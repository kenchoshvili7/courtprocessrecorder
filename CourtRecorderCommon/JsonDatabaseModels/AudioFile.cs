using DeviceController;

namespace JsonDatabaseModels
{
    public class AudioFile : BaseObject
    {
        public AudioSourceChannels? Channel { get; set; }
        public string Path { get; set; }
    }
}