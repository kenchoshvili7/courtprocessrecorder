using System;
using HelperClasses;

namespace JsonDatabaseModels
{
    public class Signature : BaseObject
    {
        public DateTime? SignTime { get; set; }
        public DateTime SessionDate { get; set; }
        public string ResponsiblePerson { get; set; }
        public string Organization { get; set; }
        public byte[] Data { get; set; }
        public string FormattedData
        {
            get { return SignatureHelper.FormatSignatureData(Data); }
        }
    }
}