﻿using System;
using System.Collections.Generic;
using AudioConfiguration;
using HelperClasses;

namespace JsonDatabaseModels
{
    public class Session : BaseObject
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string DiscNumber { get; set; }
        public string Path { get; set; }
        public TimeSpan AudioFilesDuration { get; set; }
        public bool IsSigned { get; set; }
        public SessionTemplate SessionTemplate { get; set; }
        public RSAParametersEx RsaParametersEx { get; set; }
        public string DocumentStream { get; set; }
        public List<Signature> Signatures { get; set; }
        public List<Participant> Participants { get; set; }
        public List<SessionEvent> SessionEvents { get; set; }
        public List<Channel> Channels { get; set; }
        public List<AudioFilesContainer> AudioFilesContainers { get; set; }
    }
}