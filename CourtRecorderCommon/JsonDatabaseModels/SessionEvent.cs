using System;

namespace JsonDatabaseModels
{
    public class SessionEvent : BaseObject
    {
        public DateTime Time { get; set; }
        public SessionAction SessionAction { get; set; }
        public Participant Participant { get; set; }
        public string Note { get; set; }

        public string TimeFormatted
        {
            get { return Time.ToString("HH:mm:ss"); }
        }
    }
}