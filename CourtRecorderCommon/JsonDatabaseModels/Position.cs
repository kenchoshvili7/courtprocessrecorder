namespace JsonDatabaseModels
{
    public class Position : BaseObject
    {
        public string Name { get; set; }
        public PositionType? PositionType { get; set; }
    }
}