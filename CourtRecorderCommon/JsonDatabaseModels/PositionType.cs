namespace JsonDatabaseModels
{
    public enum PositionType
    {
        Judge = 0,
        ClerkOfSession = 1,
        HeadOfSession = 2,
        Procurator = 3,
        Lawyer = 4,
        Defendant = 5,
        Victim = 6,
        Translator = 7,
        Expert = 8,
        Witness = 9,
        Specialist = 10
    }
}