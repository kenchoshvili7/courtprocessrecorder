using System.Collections.Generic;

namespace JsonDatabaseModels
{
    public class SessionsContainer : BaseObject
    {
        public List<Session> Sessions { get; set; }
    }
}