﻿using System.ComponentModel;

namespace JsonDatabaseModels
{
    public enum GlobalMode
    {
        [Description("ჩამწერი")]
        Recorder,

        [Description("რედაქტორი")]
        Editor,

        [Description("დამთვალიერებელი")]
        Viewer
    }
}