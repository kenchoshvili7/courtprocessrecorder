namespace JsonDatabaseModels
{
    public class TemplateDocument : BaseObject
    {
        public string DocumentStream { get; set; }
        public SessionTemplateType? SessionTemplateType { get; set; }
    }
}