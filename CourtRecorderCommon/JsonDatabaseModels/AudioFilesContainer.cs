using System;
using System.Collections.Generic;

namespace JsonDatabaseModels
{
    public class AudioFilesContainer : BaseObject
    {
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }
        public string Path { get; set; }
        public TimeSpan? Duration { get; set; }
        public List<AudioFile> AudioFiles { get; set; }
    }
}