﻿using System.ComponentModel;

namespace JsonDatabaseModels
{
    public enum SessionTemplateType
    {

        [Description("სისხლი")]
        Crime = 0,

        [Description("სამოქალაქო")]
        Civil = 1,
      
        [Description("ადმინისტრაციული")]
        Administrative = 2
    }
}