﻿namespace JsonDatabaseModels
{
    public class ExceptionLog : BaseObject
    {
        public string Application { get; set; }
        public string Version { get; set; }
        public string Date { get; set; }
        public string ComputerName { get; set; }
        public string UserName { get; set; }
        // ReSharper disable once InconsistentNaming
        public string OS { get; set; }
        public string Culture { get; set; }
        public string Resolution { get; set; }
        public string SystemUpTime { get; set; }
        public string AppUpTime { get; set; }
        public string TotalMemory { get; set; }
        public string AvialableMemory { get; set; }
        public string ExceptionClasses { get; set; }
        public string ExceptionMessages { get; set; }
        public string StackTraces { get; set; }
        public string LoadedModules { get; set; }
    }
}
