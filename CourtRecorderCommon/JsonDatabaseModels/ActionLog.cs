﻿namespace JsonDatabaseModels
{
    public class ActionLog : BaseObject
    {
        public string ActionType { get; set; }
        public string Data { get; set; }
        public string Date { get; set; }
        public string ComputerName { get; set; }
        public string UserName { get; set; }
        public string AppUpTime { get; set; }
    }
}