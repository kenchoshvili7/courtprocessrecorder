﻿using System.Collections.Generic;

namespace JsonDatabaseModels
{
    public class SessionTemplate : BaseObject
    {
        public string Name { get; set; }
        public List<Paragraph> Paragraphs { get; set; }
        public List<Position> Positions { get; set; }
        public List<SessionAction> SessionActions { get; set; }
        public SessionTemplateType? SessionTemplateType { get; set; }
    }
}