﻿using System;
using System.Globalization;
using System.Linq;

namespace JsonDatabaseModels
{
    public class SessionEx : Session
    {
        public SessionEx(Session session)
        {
            this.Number = session.Number;
            this.Name = session.Name;
            this.DiscNumber = session.DiscNumber;
            this.Path = session.Path;
            this.AudioFilesDuration = session.AudioFilesDuration;
            this.IsSigned = session.IsSigned;
            this.SessionTemplate = session.SessionTemplate;
            this.RsaParametersEx = session.RsaParametersEx;
            this.DocumentStream = session.DocumentStream;
            this.Signatures = session.Signatures;
            this.Participants = session.Participants;
            this.SessionEvents = session.SessionEvents;
            this.Channels = session.Channels;
            this.AudioFilesContainers = session.AudioFilesContainers;
        }

        //private DateTime? _openedOn;

        public DateTime? OpenedOn
        {
            get
            {
                var sessionEvent = SessionEvents?.Where(s => s.SessionAction?.Description != null && s.SessionAction.Description.Equals("სასამართლო სხდომის გახსნა")).OrderBy(o => o.Time).FirstOrDefault();
                return sessionEvent?.Time;
            }

            // set { _openedOn = value; }
        }

        public DateTime? ClosedOn
        {
            get { return SessionEvents?.OrderByDescending(o => o.Time).FirstOrDefault()?.Time; }
        }

        public string SessionDate
        {
            get { return OpenedOn?.ToString("dd MMMM, yyyy 'წელი'", new CultureInfo("ka-GE")); }
        }

        public string OpenedOnFormatted
        {
            get { return OpenedOn?.ToString("HH:mm:ss"); }
        }

        public string ClosedOnFormatted
        {
            get { return ClosedOn?.ToString("HH:mm:ss"); }
        }

        public string TodayFormatted
        {
            get { return DateTime.Now.ToString("dd MMMM, yyyy 'წელი'", new CultureInfo("ka-GE")); }
        }

        public string TemplateName
        {
            get { return SessionTemplate?.Name; }
        }
    }
}