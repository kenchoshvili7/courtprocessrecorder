﻿using System.Collections.Generic;
using System.IO;
using AudioConfiguration;
using CourtRecorderCommon;

namespace JsonDatabaseModels
{
    public class RecorderConfiguration : BaseObject
    {
        public string Name { get; set; }
        public int DeviceNo { get; set; }
        public string DriverName { get; set; }
        public string ArchiveDirectory { get; set; }
        public List<Channel> Channels { get; set; }
        public WaveFormatEx Format { get; set; }
        public string SkinName { get; set; }
        public GlobalMode GlobalMode { get; set; }
        public SessionTemplateType SessionTemplateType { get; set; }
        public int? HallVolumeLevel { get; set; }
        public bool? DeleteConfirmationIsOn { get; set; }
        public string CourtName { get; set; }
        public string CityName { get; set; }

        private ProgramConfiguration? programConfiguration;
        public ProgramConfiguration ProgramConfiguration
        {
            get
            {
                if (!(this.programConfiguration.HasValue || !File.Exists(AudioIO.ApplicationPath + "multichannel")))
                {
                    //this.programConfiguration = 1; todo replaced
                    this.programConfiguration = ProgramConfiguration.SingleChannel;
                }
                return this.programConfiguration.GetValueOrDefault(ProgramConfiguration.SingleChannel);
            }
            set
            {
                this.programConfiguration = new AudioConfiguration.ProgramConfiguration?(value);
            }
        }
    }
}