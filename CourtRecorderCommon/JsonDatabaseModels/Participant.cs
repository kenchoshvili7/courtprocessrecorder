namespace JsonDatabaseModels
{
    public class Participant : BaseObject
    {
        public Position Position { get; set; }
        public string Name { get; set; }

        public string ParticipantDescription
        {
            get
            {
                var positionName = string.Empty;
                if (Position != null) positionName = Position.Name;

                return positionName + ": " + Name;
            }
        }
    }
}