using System;
using Biggy.Core;
using LiteDB;

namespace JsonDatabaseModels
{
    public class BaseObject
    {
        [PrimaryKey(true)]
        [BsonId]
        public Guid UId { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}