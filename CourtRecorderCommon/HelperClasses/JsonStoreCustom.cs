﻿using System;
using Biggy.Core;
using Biggy.Data.Json;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class JsonStoreCustom<T> where T : new()
    {
        private readonly JsonDbCore _db;

        public JsonStoreCustom(JsonDbCore db)
        {
            _db = db;
            Store = _db.CreateStoreFor<T>();
        }

        public IDataStore<T> Store { get; private set; }

        public void Add(T item)
        {
            var baseObj = item as BaseObject;
            baseObj.CreatedOn = DateTime.Now;
            baseObj.UId = Guid.NewGuid();

            Store.Add(item);
        }
    }
}
