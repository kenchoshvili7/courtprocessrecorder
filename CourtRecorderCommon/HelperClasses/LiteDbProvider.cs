﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class LiteDbProvider : IDatabaseProvider
    {
        private readonly SessionsContainer _sessionsContainer;
        private readonly LiteRepositoryEx _containersDb;
        private readonly LiteRepositoryEx _sessionTemplateDb;
        private readonly LiteRepositoryEx _templateDocumentDb;

        public LiteDbProvider() { }

        public LiteDbProvider(RecorderConfiguration configuration, bool readOnlyMode = false)
        {
            var dirPath = Path.Combine(configuration.ArchiveDirectory, ConstantStrings.DatabaseName);
            CreateDir(dirPath);

            var path = Path.Combine(dirPath, $"{nameof(SessionsContainer)}.db");
            //_containersDb = new LiteRepositoryEx(path);
            _containersDb = readOnlyMode ? new LiteRepositoryEx($"Filename={path};Mode=ReadOnly") : new LiteRepositoryEx(path);

            var containers = _containersDb.Query<SessionsContainer>().ToList();

            if (!containers.Any())
            {
                _sessionsContainer = new SessionsContainer { Sessions = new List<Session>() };
                _containersDb.Insert(_sessionsContainer);
            }
            else _sessionsContainer = containers.FirstOrDefault();

            var templatesBasePath = Path.Combine(ConstantStrings.LocalDatabasePath, ConstantStrings.DatabaseName);
            CreateDir(templatesBasePath);

            var sessionTemplatePath = Path.Combine(templatesBasePath, $"{nameof(SessionTemplate)}.db");
            var templateDocumentPath = Path.Combine(templatesBasePath, $"{nameof(TemplateDocument)}.db");

            _sessionTemplateDb = new LiteRepositoryEx(sessionTemplatePath);
            _templateDocumentDb = new LiteRepositoryEx(templateDocumentPath);
        }

        private static void CreateDir(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        private LiteRepositoryEx GetSessionDb(string path)
        {
            var dirPath = Path.Combine(path, ConstantStrings.DatabaseName);
            CreateDir(dirPath);

            var dbPath = Path.Combine(dirPath, $"{nameof(Session)}.db");
            return new LiteRepositoryEx(dbPath);
        }

        public void AddSession(Session session)
        {
            var sessionDb = GetSessionDb(session.Path);

            //foreach (var containers in session.AudioFilesContainers)
            //{
            //    containers.AudioFiles.ForEach(a =>
            //    {
            //        a.UId = Guid.NewGuid();
            //        a.CreatedOn = DateTime.Now;
            //    });

            //    containers.UId = Guid.NewGuid();
            //    containers.CreatedOn = DateTime.Now;
            //}

            sessionDb.Insert(session);

            _sessionsContainer.Sessions.Add(session);
            _containersDb.Update(_sessionsContainer);
        }

        public List<Session> GetSessionsAndUpdateContainer(DateTime fromDate, DateTime toDate)
        {
            if (Global.ProgramMode == ProgramMode.CourtProcessPlayer)
                return GetSessions(fromDate, toDate);

            foreach (var session in _sessionsContainer.Sessions.Where(x => !x.IsSigned && x.CreatedOn.Date >= fromDate.Date && x.CreatedOn.Date <= toDate.Date))
            {
                try
                {
                    var reloadedSession = ReloadSession(session.Path);
                    session.IsSigned = reloadedSession.IsSigned;
                }
                catch { }
            }

            _containersDb.Update(_sessionsContainer);

            return GetSessions(fromDate, toDate);
        }

        public List<Session> GetSessions(DateTime fromDate, DateTime toDate)
        {
            var sessions = _sessionsContainer.Sessions.Where(x => x.CreatedOn.Date >= fromDate.Date && x.CreatedOn.Date <= toDate.Date).OrderByDescending(z => z.CreatedOn).ToList();

            return sessions;
        }

        public void DeleteSessionFromContainer(Session session)
        {
            var uid = ReloadSession(session.Path)?.UId ?? session.UId;
            _sessionsContainer.Sessions.RemoveAll(x => x.UId == uid);
            _containersDb.Update(_sessionsContainer);
        }

        public void AddSessionToContainer(string path)
        {
            var session = ReloadSession(path);
            if (session == null || _sessionsContainer.Sessions.Any(s => s.UId == session.UId)) return;
            session.Path = path;
            
            _sessionsContainer.Sessions.Add(new Session
            {
                UId = session.UId,
                Path = session.Path,
                CreatedOn = session.CreatedOn,
                Name = session.Name,
                Number = session.Number,
                IsSigned = session.IsSigned
            });
        }

        private static Session ReloadSession(string databasePath)
        {
            using (var sessionsDbHelper = new SessionDbHelper(databasePath))
            {
                return sessionsDbHelper.GetSession();
            }
        }

        #region Templates

        public void AddSessionTemplate(SessionTemplate sessionTemplate)
        {
            _sessionTemplateDb.Insert(sessionTemplate);
        }

        public void UpdateSessionTemplate(SessionTemplate sessionTemplate)
        {
            _sessionTemplateDb.Update(sessionTemplate);
        }

        public void DeleteSessionTemplate(SessionTemplate sessionTemplate)
        {
            var sessionTemplates = _sessionTemplateDb.Query<SessionTemplate>().ToList();
            sessionTemplates.RemoveAll(x => x.UId == sessionTemplate.UId);
            _sessionTemplateDb.Update(sessionTemplates);
        }

        public List<SessionTemplate> GetSessionTemplates()
        {
            return _sessionTemplateDb.Query<SessionTemplate>().ToList();
        }

        public TemplateDocument GetTemlateDocument(SessionTemplateType? sessionTemplateType)
        {
            return _templateDocumentDb.Query<TemplateDocument>().ToList().FirstOrDefault(s => s.SessionTemplateType == sessionTemplateType);
        }

        public void AddTemplateDocument(TemplateDocument templateDocument)
        {
            _templateDocumentDb.Insert(templateDocument);
        }

        public void ReplaceTemplateDocument(TemplateDocument templateDocument)
        {

            var oldTemplateDocument = GetTemlateDocument(templateDocument.SessionTemplateType);
            if (oldTemplateDocument == null)
            {
                AddTemplateDocument(templateDocument);
                return;
            }

            oldTemplateDocument.DocumentStream = templateDocument.DocumentStream;
            _templateDocumentDb.Update(oldTemplateDocument);
        }

        #endregion
    }
}
