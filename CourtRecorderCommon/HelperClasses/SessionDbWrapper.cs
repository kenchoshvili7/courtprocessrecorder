﻿namespace HelperClasses
{
    public class SessionDbWrapper
    {
        public static SessionDbHelper SessionDbHelper { get; private set; }

        public static SessionDbHelper Initialize(string path, bool readOnlyMode = false)
        {
            SessionDbHelper = new SessionDbHelper(path, readOnlyMode);

            return SessionDbHelper;
        }
    }
}