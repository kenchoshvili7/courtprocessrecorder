﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JsonDatabaseModels;
using LiteDB;

namespace HelperClasses
{
    public class LiteRepositoryEx : LiteRepository
    {
        public LiteRepositoryEx(LiteDatabase database, bool disposeDatabase = false) : base(database, disposeDatabase) { }

        public LiteRepositoryEx(string connectionString, BsonMapper mapper = null) : base(connectionString, mapper) { }

        public LiteRepositoryEx(ConnectionString connectionString, BsonMapper mapper = null) : base(connectionString, mapper) { }

        public LiteRepositoryEx(Stream stream, BsonMapper mapper = null, string password = null) : base(stream, mapper, password) { }

        public new BsonValue Insert<T>(T entity, string collectionName = null)
        {
            var baseObj = entity as BaseObject;
            if (baseObj != null)
            {
                baseObj.CreatedOn = DateTime.Now;
                baseObj.UId = Guid.NewGuid();
            }

            return base.Insert(entity);
        }

        public new int Insert<T>(IEnumerable<T> entities, string collectionName = null)
        {
            var enumerable = entities as IList<T> ?? entities.ToList();
            foreach (var entity in enumerable.ToList())
            {
                var baseObj = entity as BaseObject;
                if (baseObj != null)
                {
                    baseObj.CreatedOn = DateTime.Now;
                    baseObj.UId = Guid.NewGuid();
                }
            }

            return base.Insert(enumerable);
        }
    }
}
