﻿using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;

namespace HelperClasses
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class RSAParametersEx
    {
        public byte[] D { get; set; }
        public byte[] DP { get; set; }
        public byte[] DQ { get; set; }
        public byte[] Exponent { get; set; }
        public byte[] InverseQ { get; set; }
        public byte[] Modulus { get; set; }
        public byte[] P { get; set; }
        public byte[] Q { get; set; }

        public RSAParameters ToRSAParameters()
        {
            var parameters = new RSAParameters
            {
                D = D,
                DP = DP,
                DQ = DQ,
                Exponent = Exponent,
                InverseQ = InverseQ,
                Modulus = Modulus,
                P = P,
                Q = Q
            };
            return parameters;
        }

        public static RSAParametersEx FromRSAParameters(RSAParameters rsaParameters)
        {
            var parameters = new RSAParametersEx
            {
                D = rsaParameters.D,
                DP = rsaParameters.DP,
                DQ = rsaParameters.DQ,
                Exponent = rsaParameters.Exponent,
                InverseQ = rsaParameters.InverseQ,
                Modulus = rsaParameters.Modulus,
                P = rsaParameters.P,
                Q = rsaParameters.Q
            };
            return parameters;
        }
    }
}
