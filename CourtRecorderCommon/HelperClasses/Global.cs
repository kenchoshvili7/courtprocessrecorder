﻿using JsonDatabaseModels;

namespace HelperClasses
{
    public static class Global
    {
        public static ProgramMode ProgramMode { get; set; }
        public static GlobalMode GlobalMode { get; set; }
    }
}
