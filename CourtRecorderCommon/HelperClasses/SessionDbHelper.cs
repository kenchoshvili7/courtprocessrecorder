﻿using System;
using System.IO;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class SessionDbHelper : IDisposable
    {
        private readonly ISessionsDbProvider _sessionsDbProvider;
        public string SessionPath { get; }

        public SessionDbHelper(string path, bool readOnlyMode = false)
        {
            SessionPath = path;

            var jsonFilePath = Path.Combine(path, ConstantStrings.DatabaseName, "sessions.json");

            if (File.Exists(jsonFilePath)) _sessionsDbProvider = new SessionsJsonDbProvider(path);
            else _sessionsDbProvider = new SessionsLiteDbProvider(path, readOnlyMode);
        }

        public Session GetSession()
        {
            return _sessionsDbProvider.GetSession();
        }

        public Session RegenerateSessionPath(Session session, string path)
        {
            return _sessionsDbProvider.RegenerateSessionPath(session, path);
        }

        public void UpdateSession(Session session)
        {
            _sessionsDbProvider.UpdateSession(session);
        }

        public Session ReloadSession(Session session)
        {
            return _sessionsDbProvider.ReloadSession(session);
        }

        public void Dispose()
        {
            _sessionsDbProvider?.Dispose();
        }
    }
}