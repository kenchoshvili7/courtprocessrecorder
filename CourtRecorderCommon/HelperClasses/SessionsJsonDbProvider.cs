﻿using System.Linq;
using Biggy.Core;
using Biggy.Data.Json;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class SessionsJsonDbProvider : ISessionsDbProvider
    {
        private Session _session;
        private readonly IDataStore<Session> _sessionStore;

        public SessionsJsonDbProvider(string path)
        {
            var dbCore = new JsonDbCore(path, ConstantStrings.DatabaseName);
            _sessionStore = dbCore.CreateStoreFor<Session>();
            
            _session = _sessionStore.TryLoadData().FirstOrDefault();
        }

        public Session GetSession()
        {
            return _session;
        }

        public Session RegenerateSessionPath(Session session, string path)
        {
            var oldPath = session.Path;

            session.Path = path;
            foreach (var audioFilesContainer in session.AudioFilesContainers)
            {
                audioFilesContainer.Path = audioFilesContainer.Path.Replace(oldPath, path);

                foreach (var audioFile in audioFilesContainer.AudioFiles)
                    audioFile.Path = audioFile.Path.Replace(oldPath, path);
            }

            _sessionStore.Update(session);

            return session;
        }

        public void UpdateSession(Session session)
        {
            _sessionStore.Update(session);
            _session = session;
        }

        public Session ReloadSession(Session session)
        {
            if (Global.ProgramMode == ProgramMode.CourtProcessPlayer)
                return session;

            return _sessionStore.TryLoadData().FirstOrDefault();
        }

        public void Dispose()
        {
            
        }
    }
}