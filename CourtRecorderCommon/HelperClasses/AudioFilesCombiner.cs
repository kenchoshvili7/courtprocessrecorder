﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class AudioFilesCombiner
    {
        public static CombinedStream CombineAudioFiles(Session session, string sessionPath)
        {
            var audioFilesPaths = session.AudioFilesContainers.OrderBy(o => o.StartTime).SelectMany(a => a.AudioFiles.Select(x => x.Path));

            var streams = new List<Stream>();

            foreach (var path in audioFilesPaths)
                streams.Add(new FileStream(sessionPath + path, FileMode.Open, FileAccess.Read, FileShare.Read));

            using (var combinedStreams = new CombinedStream(streams.ToArray()))
            {
                return combinedStreams;
            }
        }
    }
}
