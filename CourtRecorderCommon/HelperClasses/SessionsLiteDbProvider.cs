﻿using System.IO;
using System.Linq;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class SessionsLiteDbProvider : ISessionsDbProvider
    {
        private Session _session;
        private readonly LiteRepositoryEx _sessionDb;

        public SessionsLiteDbProvider(string path, bool readOnlyMode = false)
        {
            var dirPath = Path.Combine(path, ConstantStrings.DatabaseName);
            var dbPath = Path.Combine(dirPath, $"{nameof(Session)}.db");

            _sessionDb = readOnlyMode ? new LiteRepositoryEx($"Filename={dbPath};Mode=ReadOnly") : new LiteRepositoryEx(dbPath);

            _session = _sessionDb.Query<Session>().ToList().FirstOrDefault();
        }

        public Session GetSession()
        {
            return _session;
        }

        public Session RegenerateSessionPath(Session session, string path)
        {
            var oldPath = session.Path;

            session.Path = path;
            foreach (var audioFilesContainer in session.AudioFilesContainers)
            {
                audioFilesContainer.Path = audioFilesContainer.Path.Replace(oldPath, path);

                foreach (var audioFile in audioFilesContainer.AudioFiles)
                    audioFile.Path = audioFile.Path.Replace(oldPath, path);
            }

            _sessionDb.Update(session);

            return session;
        }

        public void UpdateSession(Session session)
        {
            _sessionDb.Update(session);
            _session = session;
        }

        public Session ReloadSession(Session session)
        {
            if (Global.ProgramMode == ProgramMode.CourtProcessPlayer)
                return session;

            return _sessionDb.Query<Session>().ToList().FirstOrDefault();
        }

        public void Dispose()
        {
            _sessionDb?.Dispose();
        }
    }
}
