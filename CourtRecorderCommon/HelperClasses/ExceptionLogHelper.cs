﻿using System;
using System.IO;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class ExceptionLogHelper
    {
        public static void AddExceptionLog(Exception exception)
        {
            var path = Path.Combine(ConstantStrings.DatabasePath, ConstantStrings.DatabaseName);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var db = new LiteRepositoryEx(Path.Combine(path, $"{nameof(ExceptionLog)}.db"));

            var exceptionLog = ExceptionLogger.LogException(exception);
            db.Insert(exceptionLog);
        }
    }
}
