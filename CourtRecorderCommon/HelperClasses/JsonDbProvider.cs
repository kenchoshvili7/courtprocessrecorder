﻿using System;
using System.Collections.Generic;
using System.Linq;
using Biggy.Data.Json;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class JsonDbProvider : IDatabaseProvider
    {
        private readonly SessionsContainer _sessionsContainer;
        private readonly JsonStoreCustom<SessionsContainer> _containerCustomStore;
        private readonly JsonStoreCustom<SessionTemplate> _sessionTemplateStore;
        private readonly JsonStoreCustom<TemplateDocument> _templateDocumentStore;
        public JsonDbProvider() { }

        public JsonDbProvider(RecorderConfiguration configuration)
        {
            var dbCore = new JsonDbCore(configuration.ArchiveDirectory, ConstantStrings.DatabaseName);
            _containerCustomStore = new JsonStoreCustom<SessionsContainer>(dbCore);

            var containers = _containerCustomStore.Store.TryLoadData();
            if (!containers.Any())
            {
                _sessionsContainer = new SessionsContainer { Sessions = new List<Session>() };
                _containerCustomStore.Add(_sessionsContainer);
            }
            else _sessionsContainer = containers.FirstOrDefault();

            var templatesDbCore = new JsonDbCore(ConstantStrings.LocalDatabasePath, ConstantStrings.DatabaseName);
            _sessionTemplateStore = new JsonStoreCustom<SessionTemplate>(templatesDbCore);

            _templateDocumentStore = new JsonStoreCustom<TemplateDocument>(templatesDbCore);
        }

        private JsonStoreCustom<Session> GetSessionCustomStore(string path)
        {
            var dbCore = new JsonDbCore(path, ConstantStrings.DatabaseName);
            var sessionCustomStore = new JsonStoreCustom<Session>(dbCore);
            return sessionCustomStore;
        }

        public void AddSession(Session session)
        {
            var sessionCustomStore = GetSessionCustomStore(session.Path);

            //foreach (var containers in session.AudioFilesContainers)
            //{
            //    containers.AudioFiles.ForEach(a =>
            //    {
            //        a.UId = Guid.NewGuid();
            //        a.CreatedOn = DateTime.Now;
            //    });

            //    containers.UId = Guid.NewGuid();
            //    containers.CreatedOn = DateTime.Now;
            //}

            sessionCustomStore.Add(session);

            _sessionsContainer.Sessions.Add(session);
            _containerCustomStore.Store.Update(_sessionsContainer);
        }

        public List<Session> GetSessionsAndUpdateContainer(DateTime fromDate, DateTime toDate)
        {
            var sessions = _sessionsContainer.Sessions.Where(x => x.CreatedOn >= fromDate && x.CreatedOn <= toDate).ToList();

            if (Global.ProgramMode == ProgramMode.CourtProcessPlayer)
                return sessions;

            foreach (var session in sessions.Where(x => !x.IsSigned))
            {
                try
                {
                    var reloadedSession = ReloadSession(session.Path);
                    session.IsSigned = reloadedSession.IsSigned;
                }
                catch { }
            }

            _containerCustomStore.Store.Update(_sessionsContainer);

            return _sessionsContainer.Sessions;
        }

        public List<Session> GetSessions(DateTime fromDate, DateTime toDate)
        {
            var sessions = _sessionsContainer.Sessions.Where(x => x.CreatedOn >= fromDate && x.CreatedOn <= toDate).ToList();

            return sessions;
        }

        public void DeleteSessionFromContainer(Session session)
        {
            var uid = ReloadSession(session.Path)?.UId ?? session.UId;
            _sessionsContainer.Sessions.RemoveAll(x => x.UId == uid);
            _containerCustomStore.Store.Update(_sessionsContainer);
        }

        public void AddSessionToContainer(string path)
        {
            var session = ReloadSession(path);
            if (session == null || _sessionsContainer.Sessions.Any(s => s.UId == session.UId)) return;
            _sessionsContainer.Sessions.Add(session);
        }

        private static Session ReloadSession(string databasePath)
        {
            var sessionsDbHelper = new SessionDbHelper(databasePath);
            return sessionsDbHelper.GetSession();
        }

        #region Templates

        public void AddSessionTemplate(SessionTemplate sessionTemplate)
        {
            _sessionTemplateStore.Add(sessionTemplate);
        }

        public void UpdateSessionTemplate(SessionTemplate sessionTemplate)
        {
            _sessionTemplateStore.Store.Update(sessionTemplate);
        }

        public void DeleteSessionTemplate(SessionTemplate sessionTemplate)
        {
            _sessionTemplateStore.Store.Delete(sessionTemplate);
        }

        public List<SessionTemplate> GetSessionTemplates()
        {
            return _sessionTemplateStore.Store.TryLoadData();
        }

        public TemplateDocument GetTemlateDocument(SessionTemplateType? sessionTemplateType)
        {
            return _templateDocumentStore.Store.TryLoadData().FirstOrDefault(s => s.SessionTemplateType == sessionTemplateType);
        }

        public void AddTemplateDocument(TemplateDocument templateDocument)
        {
            _templateDocumentStore.Store.Add(templateDocument);
        }

        public void ReplaceTemplateDocument(TemplateDocument templateDocument)
        {

            var oldTemplateDocument = GetTemlateDocument(templateDocument.SessionTemplateType);
            if (oldTemplateDocument == null)
            {
                AddTemplateDocument(templateDocument);
                return;
            }

            oldTemplateDocument.DocumentStream = templateDocument.DocumentStream;
            _templateDocumentStore.Store.Update(oldTemplateDocument);
        }

        public void TempTemplatePositionUpdate(List<SessionTemplate> templates)
        {
            foreach (var template in templates)
            {
                foreach (var pos in template.Positions)
                {
                    if (pos.Name.Contains("მოსამართლე")) pos.PositionType = PositionType.Judge;
                    if (pos.Name.Contains("სხდომის მდივანი")) pos.PositionType = PositionType.ClerkOfSession;
                    if (pos.Name.Contains("სხდომის თავმჯდომარე")) pos.PositionType = PositionType.HeadOfSession;
                    if (pos.Name.Contains("ექსპერტი")) pos.PositionType = PositionType.Expert;
                    if (pos.Name.Contains("მოწმე")) pos.PositionType = PositionType.Witness;
                    if (pos.Name.Contains("სპეციალისტი")) pos.PositionType = PositionType.Specialist;
                    if (pos.Name.Contains("ბრალდებული")) pos.PositionType = PositionType.Defendant;
                    if (pos.Name.Contains("პროკურორი")) pos.PositionType = PositionType.Procurator;
                    if (pos.Name.Contains("ადვოკატი")) pos.PositionType = PositionType.Lawyer;
                    if (pos.Name.Contains("დაზარალებული")) pos.PositionType = PositionType.Victim;
                    if (pos.Name.Contains("თარჯიმანი")) pos.PositionType = PositionType.Translator;
                }

                _sessionTemplateStore.Store.Update(template);
            }
        }

        #endregion
    }
}
