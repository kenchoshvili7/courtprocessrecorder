﻿using System.IO;
using System.Linq;
using Biggy.Core;
using Biggy.Data.Json;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class ConfigurationHelper
    {
        private static RecorderConfiguration _recorderConfiguration;

        public static void CreateConfiguration(RecorderConfiguration configuration)
        {
            var db = new JsonDbCore(ConstantStrings.DatabasePath, ConstantStrings.DatabaseName);
            var configStroe = db.CreateStoreFor<RecorderConfiguration>();
            configStroe.Add(configuration);

            if (!Directory.Exists(configuration.ArchiveDirectory) && !string.IsNullOrWhiteSpace(configuration.ArchiveDirectory))
                Directory.CreateDirectory(configuration.ArchiveDirectory);
        }

        public static RecorderConfiguration GetConfiguration()
        {
            GetConfigurationWithAllChannels();

            if (_recorderConfiguration == null) return null;

            var recorderConfiguration2Return = new RecorderConfiguration
            {
                Name = _recorderConfiguration.Name,
                ArchiveDirectory = _recorderConfiguration.ArchiveDirectory,
                Channels = _recorderConfiguration.Channels,
                DeviceNo = _recorderConfiguration.DeviceNo,
                DriverName = _recorderConfiguration.DriverName,
                Format = _recorderConfiguration.Format,
                SkinName = _recorderConfiguration.SkinName,
                GlobalMode = _recorderConfiguration.GlobalMode,
                SessionTemplateType = _recorderConfiguration.SessionTemplateType,
                ProgramConfiguration = _recorderConfiguration.ProgramConfiguration,
                HallVolumeLevel = _recorderConfiguration.HallVolumeLevel,
                DeleteConfirmationIsOn = _recorderConfiguration.DeleteConfirmationIsOn,
                CourtName = _recorderConfiguration.CourtName,
                CityName = _recorderConfiguration.CityName,
                CreatedOn = _recorderConfiguration.CreatedOn,
                UId = _recorderConfiguration.UId
            };

            var channels = _recorderConfiguration.Channels.Where(c => c.IsActive).ToList();

            recorderConfiguration2Return.Channels = channels;

            return recorderConfiguration2Return;
        }

        public static RecorderConfiguration GetConfigurationWithAllChannels()
        {
            if (_recorderConfiguration != null) return _recorderConfiguration;

            var configurationDbCore = new JsonDbCore(ConstantStrings.DatabasePath, ConstantStrings.DatabaseName);
            _recorderConfiguration = new BiggyList<RecorderConfiguration>(configurationDbCore.CreateStoreFor<RecorderConfiguration>()).FirstOrDefault();

            return _recorderConfiguration;
        }

        public static void UpdateConfiguration(RecorderConfiguration configuration)
        {
            var db = new JsonDbCore(ConstantStrings.DatabasePath, ConstantStrings.DatabaseName);
            var configStroe = db.CreateStoreFor<RecorderConfiguration>();

            if (!Directory.Exists(configuration.ArchiveDirectory) && !string.IsNullOrWhiteSpace(configuration.ArchiveDirectory))
                Directory.CreateDirectory(configuration.ArchiveDirectory);

            configStroe.Update(configuration);
        }

        public static void SetSkinName(string skinName)
        {
            var db = new JsonDbCore(ConstantStrings.DatabasePath, ConstantStrings.DatabaseName);
            var configStroe = db.CreateStoreFor<RecorderConfiguration>();
            _recorderConfiguration.SkinName = skinName;
            configStroe.Update(_recorderConfiguration);
        }

        public static void SetSessionTemplateType(SessionTemplateType sessionTemplateType)
        {
            var db = new JsonDbCore(ConstantStrings.DatabasePath, ConstantStrings.DatabaseName);
            var configStroe = db.CreateStoreFor<RecorderConfiguration>();
            _recorderConfiguration.SessionTemplateType = sessionTemplateType;
            configStroe.Update(_recorderConfiguration);
        }
    }
}
