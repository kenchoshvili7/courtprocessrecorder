﻿using System;
using System.Collections.Generic;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class DatabaseHelper
    {
        private static DatabaseHelper _databaseHelper;
        private readonly IDatabaseProvider _databaseProvider;

        public DatabaseHelper()
        {
            _databaseProvider = new LiteDbProvider();
        }

        public DatabaseHelper(RecorderConfiguration configuration, bool readOnlyMode = false)
        {
            JsonDbToLiteDbConverter.ConvertSessionContainers(configuration);

            _databaseProvider = new LiteDbProvider(configuration, readOnlyMode);
        }

        //public static DatabaseHelper DatabaseHelperInstance
        //{
        //    get
        //    {
        //        if (_databaseHelper == null)
        //        {
        //            _databaseHelper = new DatabaseHelper(ConfigurationHelper.GetConfiguration());
        //        }
        //        return _databaseHelper;
        //    }
        //}
        
        public static DatabaseHelper DatabaseHelperInstance(bool readOnlyMode = false)
        {
            if (_databaseHelper != null) return _databaseHelper;
            _databaseHelper = new DatabaseHelper(ConfigurationHelper.GetConfiguration(),readOnlyMode);

            return _databaseHelper;
        }
        
        public void AddSession(Session session)
        {
            _databaseProvider.AddSession(session);
        }

        public List<Session> GetSessionsAndUpdate(DateTime fromDate, DateTime toDate)
        {
            return _databaseProvider.GetSessionsAndUpdateContainer(fromDate, toDate);
        }

        public List<Session> GetSessions(DateTime fromDate, DateTime toDate)
        {
            return _databaseProvider.GetSessions(fromDate, toDate);
        }

        public void DeleteSession(Session session)
        {
            _databaseProvider.DeleteSessionFromContainer(session);
        }

        public void AddSessionToContainer(string path)
        {
            _databaseProvider.AddSessionToContainer(path);
        }

        #region Templates

        public void AddSessionTemplate(SessionTemplate sessionTemplate)
        {
            _databaseProvider.AddSessionTemplate(sessionTemplate);
        }

        public void UpdateSessionTemplate(SessionTemplate sessionTemplate)
        {
            _databaseProvider.UpdateSessionTemplate(sessionTemplate);
        }

        public void DeleteSessionTemplate(SessionTemplate sessionTemplate)
        {
            _databaseProvider.DeleteSessionTemplate(sessionTemplate);
        }

        public List<SessionTemplate> GetSessionTemplates()
        {
            return _databaseProvider.GetSessionTemplates();
        }

        public TemplateDocument GetTemlateDocument(SessionTemplateType? sessionTemplateType)
        {
            return _databaseProvider.GetTemlateDocument(sessionTemplateType);
        }

        public void AddTemplateDocument(TemplateDocument templateDocument)
        {
            _databaseProvider.AddTemplateDocument(templateDocument);
        }

        public void ReplaceTemplateDocument(TemplateDocument templateDocument)
        {
            _databaseProvider.ReplaceTemplateDocument(templateDocument);
        }

        #endregion
    }
}
