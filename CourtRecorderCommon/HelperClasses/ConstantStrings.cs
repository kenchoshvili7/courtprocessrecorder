﻿using System;
using System.IO;
using System.Windows.Forms;

namespace HelperClasses
{
    public static class ConstantStrings
    {
        public static readonly string DatabasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CourtProcessRecorder");
        public static readonly string LocalDatabasePath = Path.GetDirectoryName(Application.ExecutablePath);
        public static readonly string LocalDatabasePathForPlayer = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Data");
        public const string DatabaseName = "Database";
        public const string ProgramStart = "პროგრამის ჩართვა";
        public const string ProgramStop = "პროგრამის დახურვა";
        public const string RecordStart = "ჩაწერის დაწყება";
        public const string RecordStop = "ჩაწერის დასრულება";
        public const string Message = "შეტყობინება";
    }
}
