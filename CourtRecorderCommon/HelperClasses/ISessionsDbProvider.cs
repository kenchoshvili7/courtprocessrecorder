﻿using System;
using JsonDatabaseModels;

namespace HelperClasses
{
    public interface ISessionsDbProvider : IDisposable
    {
        Session GetSession();

        Session RegenerateSessionPath(Session session, string path);

        void UpdateSession(Session session);

        Session ReloadSession(Session session);
    }
}