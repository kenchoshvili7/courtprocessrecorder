﻿using System;
using System.Collections.Generic;
using JsonDatabaseModels;

namespace HelperClasses
{
    public interface IDatabaseProvider
    {
        void AddSession(Session session);

        List<Session> GetSessionsAndUpdateContainer(DateTime fromDate, DateTime toDate);

        List<Session> GetSessions(DateTime fromDate, DateTime toDate);

        void DeleteSessionFromContainer(Session session);

        void AddSessionToContainer(string path);

        void AddSessionTemplate(SessionTemplate sessionTemplate);

        void UpdateSessionTemplate(SessionTemplate sessionTemplate);

        void DeleteSessionTemplate(SessionTemplate sessionTemplate);

        List<SessionTemplate> GetSessionTemplates();

        TemplateDocument GetTemlateDocument(SessionTemplateType? sessionTemplateType);

        void AddTemplateDocument(TemplateDocument templateDocument);

        void ReplaceTemplateDocument(TemplateDocument templateDocument);
    }
}