﻿using System;
using System.Security.Cryptography;
using System.Text;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class SignatureHelper
    {
        public static Tuple<Signature, Session> GenerateSignature(Session session, string sessionPath,DateTime date, DateTime? signatureDate, string responsiblePerson, string organization)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                using (var sha1 = new SHA1CryptoServiceProvider())
                {
                    if (session.RsaParametersEx == null)
                        session.RsaParametersEx = RSAParametersEx.FromRSAParameters(rsa.ExportParameters(true));
                    else
                        rsa.ImportParameters(session.RsaParametersEx.ToRSAParameters());

                    using (var s = AudioFilesCombiner.CombineAudioFiles(session, sessionPath))
                    {
                        using (var stream = new MemoryTributary())
                        {
                            s.CopyTo(stream);

                            var bytes = BitConverter.GetBytes(signatureDate?.ToBinary() ?? 0L);
                            stream.Write(bytes, 0, bytes.Length);
                            bytes = Encoding.Unicode.GetBytes(responsiblePerson);
                            stream.Write(bytes, 0, bytes.Length);
                            bytes = Encoding.Unicode.GetBytes(organization);
                            stream.Write(bytes, 0, bytes.Length);

                            stream.Position = 0;

                            var signature = new Signature
                            {
                                SignTime = signatureDate,
                                SessionDate = date,
                                ResponsiblePerson = responsiblePerson,
                                Organization = organization,
                                Data = rsa.SignData(stream, sha1)
                            };

                            return Tuple.Create(signature, session);
                        }
                    }
                }
            }
        }

        public static bool VerifySignature(Session session, string sessionPath, Signature signature, DateTime date)
        {
            if (signature.SignTime == null || signature.SignTime.Value.Date != date.Date || session.RsaParametersEx == null) return false;

            using (var rsa = new RSACryptoServiceProvider())
            {
                using (var sha1 = new SHA1CryptoServiceProvider())
                {
                    rsa.ImportParameters(session.RsaParametersEx.ToRSAParameters());
                    var str = CryptoConfig.MapNameToOID(sha1.GetType().ToString());

                    using (var s = AudioFilesCombiner.CombineAudioFiles(session, sessionPath))
                    {
                        using (var stream = new MemoryTributary())
                        {
                            s.CopyTo(stream);

                            var bytes = BitConverter.GetBytes(signature.SignTime.Value.ToBinary());
                            stream.Write(bytes, 0, bytes.Length);
                            bytes = Encoding.Unicode.GetBytes(signature.ResponsiblePerson);
                            stream.Write(bytes, 0, bytes.Length);
                            bytes = Encoding.Unicode.GetBytes(signature.Organization);
                            stream.Write(bytes, 0, bytes.Length);

                            stream.Position = 0;

                            return rsa.VerifyHash(sha1.ComputeHash(stream), str, signature.Data);
                        }
                    }
                }
            }
        }

        public static string FormatSignatureData(byte[] signatureData)
        {
            string str = null;
            if (signatureData != null)
            {
                int num = 0;
                foreach (byte num2 in signatureData)
                {
                    if ((num % 0x10) == 0)
                    {
                        string str3 = str;
                        str = str3 + num.ToString("X4") + ": " + num2.ToString("X2") + " ";
                    }
                    else
                    {
                        str = str + num2.ToString("X2") + " ";
                    }
                    num++;
                    if ((num % 0x10) == 0)
                    {
                        str = str + Environment.NewLine;
                    }
                }
            }
            return str;
        }
    }
}
