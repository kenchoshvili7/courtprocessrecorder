﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class ActionLogger
    {
        public static void LogAction(string actionType, string data = null)
        {
            if (Global.GlobalMode != GlobalMode.Recorder) return;

            var actionLog = new ActionLog
            {
                ActionType = actionType,
                Data = data,
                Date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                ComputerName = SystemInformation.ComputerName,
                UserName = SystemInformation.UserName,
                AppUpTime = (DateTime.Now - Process.GetCurrentProcess().StartTime).ToString()
            };

            var path = Path.Combine(ConstantStrings.DatabasePath, ConstantStrings.DatabaseName);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var db = new LiteRepositoryEx(Path.Combine(path, $"{nameof(ActionLog)}.db"));
            db.Insert(actionLog);
        }
    }
}
