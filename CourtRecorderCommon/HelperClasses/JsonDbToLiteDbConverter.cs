﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Biggy.Data.Json;
using JsonDatabaseModels;

namespace HelperClasses
{
    public class JsonDbToLiteDbConverter
    {
        public static void ConvertSessionContainers(RecorderConfiguration configuration)
        {
            //ConvertTemplates();
            try
            {
                var jsonFilePath = Path.Combine(configuration.ArchiveDirectory, ConstantStrings.DatabaseName, "sessionscontainers.json");
                if (!File.Exists(jsonFilePath)) return;

                var dbCore = new JsonDbCore(configuration.ArchiveDirectory, ConstantStrings.DatabaseName);
                var containerCustomStore = new JsonStoreCustom<SessionsContainer>(dbCore);
                var containers = containerCustomStore.Store.TryLoadData().FirstOrDefault();

                var path = Path.Combine(configuration.ArchiveDirectory, ConstantStrings.DatabaseName, $"{nameof(SessionsContainer)}.db");
                var containersDb = new LiteRepositoryEx(path);
                var liteDbContainers = containersDb.Query<SessionsContainer>().ToList();
                SessionsContainer sessionsContainer;

                if (!liteDbContainers.Any())
                {
                    sessionsContainer = new SessionsContainer { Sessions = new List<Session>() };
                    containersDb.Insert(sessionsContainer);
                }
                else sessionsContainer = containersDb.FirstOrDefault<SessionsContainer>();

                if (containers?.Sessions != null)
                    sessionsContainer.Sessions.AddRange(containers.Sessions);

                containersDb.Update(sessionsContainer);
                File.Delete(jsonFilePath);
            }
            catch { }
        }

        private static void ConvertTemplates()
        {
            var templatesDbCore = new JsonDbCore(ConstantStrings.LocalDatabasePath, ConstantStrings.DatabaseName);
            var sessionTemplateStore = new JsonStoreCustom<SessionTemplate>(templatesDbCore);
            var templateDocumentStore = new JsonStoreCustom<TemplateDocument>(templatesDbCore);

            var sessionTemplates = sessionTemplateStore.Store.TryLoadData();
            var templateDocuments = templateDocumentStore.Store.TryLoadData();

            var sessionTemplateDb = new LiteRepositoryEx(Path.Combine(ConstantStrings.LocalDatabasePath, ConstantStrings.DatabaseName, $"{nameof(SessionTemplate)}.db"));
            var templateDocumentDb = new LiteRepositoryEx(Path.Combine(ConstantStrings.LocalDatabasePath, ConstantStrings.DatabaseName, $"{nameof(TemplateDocument)}.db"));

            foreach (var sessionTemplate in sessionTemplates)
                sessionTemplateDb.Insert(sessionTemplate);

            foreach (var templateDocument in templateDocuments)
                templateDocumentDb.Insert(templateDocument);
        }
    }
}
