﻿using System;

namespace HelperClasses
{
    public static class ExtensionMethods
    {
        public static DateTime RoundToMilliSeconds(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Millisecond, dateTime.Kind);
        }
    }
}
