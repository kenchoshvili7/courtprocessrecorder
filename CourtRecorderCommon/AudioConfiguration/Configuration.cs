﻿using CourtRecorderCommon;

namespace AudioConfiguration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;

    [Serializable]
    public class Configuration
    {
        private string archiveDirectory;
        private int? autoDiskSpaceAllocation;
        private int? autoFragmentationInterval;
        private int? autoSaveInterval;
        private List<Channel> channels;
        private int deviceNo;
        private WaveFormatEx format;
        private long freeSpaceLimitWarn;
        private AudioConfiguration.ProgramConfiguration? programConfiguration;
        private AudioConfiguration.Quality quality;

        public Configuration()
        {
            BassRegistration.Register();
            this.Quality = AudioConfiguration.Quality.Normal;
            this.freeSpaceLimitWarn = 0x140000000L;
        }

        public static Configuration Load()
        {
            Configuration configuration2;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Configuration), new Type[] { typeof(List<Channel>), typeof(WaveFormatEx), typeof(AudioConfiguration.Quality) });
                using (FileStream stream = new FileStream(AudioIO.ApplicationPath + "configuration.xml", FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    configuration2 = (Configuration) serializer.Deserialize(stream);
                }
            }
            catch
            {
                configuration2 = new Configuration();
            }
            return configuration2;
        }

        public static string RemovePathNodeAndSaveToTemporary(string configFile)
        {
            Configuration o = null;
            XmlSerializer serializer = null;
            FileStream stream;
            try
            {
                serializer = new XmlSerializer(typeof(Configuration), new Type[] { typeof(List<Channel>), typeof(WaveFormatEx), typeof(AudioConfiguration.Quality) });
                using (stream = new FileStream(configFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    o = (Configuration) serializer.Deserialize(stream);
                }
            }
            catch
            {
                o = new Configuration();
            }
            o.archiveDirectory = null;
            o.programConfiguration = null;
            serializer = new XmlSerializer(typeof(Configuration), new Type[] { typeof(List<Channel>), typeof(WaveFormatEx), typeof(AudioConfiguration.Quality) });
            string tempFileName = Path.GetTempFileName();
            using (stream = new FileStream(tempFileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                serializer.Serialize((Stream) stream, o);
            }
            return tempFileName;
        }

        public void Save()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Configuration), new Type[] { typeof(List<Channel>), typeof(WaveFormatEx), typeof(AudioConfiguration.Quality) });
            using (FileStream stream = new FileStream(AudioIO.ApplicationPath + "configuration.xml", FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                serializer.Serialize((Stream) stream, this);
            }
        }

        public static decimal? StringToDecimal(string text)
        {
            decimal result = 0M;
            string[] strArray = text.Split(new char[] { ',', '.' });
            if (decimal.TryParse(strArray[0], out result))
            {
                if (strArray.Length > 1)
                {
                    decimal num2 = 0M;
                    if (decimal.TryParse(strArray[1], out num2))
                    {
                        result += num2 / ((decimal) Math.Pow(10.0, (double) strArray[1].Length));
                    }
                }
                return new decimal?(result);
            }
            return null;
        }

        public string ArchiveDirectory
        {
            get
            {
                if ((this.archiveDirectory != null) && this.archiveDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()))
                {
                    return this.archiveDirectory;
                }
                return (this.archiveDirectory + Path.DirectorySeparatorChar);
            }
            set
            {
                this.archiveDirectory = value;
            }
        }

        public int? AutoDiskSpaceAllocation
        {
            get
            {
                return this.autoDiskSpaceAllocation;
            }
            set
            {
                this.autoDiskSpaceAllocation = value;
            }
        }

        public int? AutoFragmentationInterval
        {
            get
            {
                return this.autoFragmentationInterval;
            }
            set
            {
                this.autoFragmentationInterval = value;
            }
        }

        public int? AutoSaveInterval
        {
            get
            {
                return this.autoSaveInterval;
            }
            set
            {
                this.autoSaveInterval = value;
            }
        }

        public List<Channel> Channels
        {
            get
            {
                if (this.channels == null)
                {
                    this.channels = new List<Channel>(6);
                }
                return this.channels;
            }
        }

        public int DeviceNo
        {
            get
            {
                return this.deviceNo;
            }
            set
            {
                this.deviceNo = value;
            }
        }

        public WaveFormatEx Format
        {
            get
            {
                return this.format;
            }
            set
            {
                this.format = value;
            }
        }

        public long FreeSpaceLimitWarn
        {
            get
            {
                return this.freeSpaceLimitWarn;
            }
            set
            {
                this.freeSpaceLimitWarn = value;
            }
        }

        public AudioConfiguration.ProgramConfiguration ProgramConfiguration
        {
            get
            {
                if (!(this.programConfiguration.HasValue || !File.Exists(AudioIO.ApplicationPath + "multichannel")))
                {
                    //this.programConfiguration = 1; todo replaced
                    this.programConfiguration = ProgramConfiguration.SingleChannel;
                }
                return this.programConfiguration.GetValueOrDefault(AudioConfiguration.ProgramConfiguration.SingleChannel);
            }
            set
            {
                this.programConfiguration = new AudioConfiguration.ProgramConfiguration?(value);
            }
        }

        public AudioConfiguration.Quality Quality
        {
            get
            {
                return this.quality;
            }
            set
            {
                this.quality = value;
            }
        }
    }
}

