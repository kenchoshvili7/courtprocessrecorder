﻿namespace AudioConfiguration
{
    using System;

    [Serializable]
    public class DeviceInfo
    {
        public int DeviceNo;
        public string Driver;
        public string Name;

        public DeviceInfo()
        {
            this.DeviceNo = 0;
        }

        public DeviceInfo(int deviceNo, string name, string driver)
        {
            this.DeviceNo = deviceNo;
            this.Name = name;
            this.Driver = driver;
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1}", this.DeviceNo, this.Name);
        }
    }
}

