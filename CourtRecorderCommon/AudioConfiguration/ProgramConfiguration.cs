﻿namespace AudioConfiguration
{
    public enum ProgramConfiguration
    {
        SingleChannel,
        MultiChannel
    }
}

