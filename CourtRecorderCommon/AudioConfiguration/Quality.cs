﻿namespace AudioConfiguration
{
    using System.ComponentModel;

    public enum Quality
    {
        [Description("მაღალი")]
        High = 0xac44,
        [Description("ნორმალური")]
        Normal = 0x5622,
        [Description("დაბალი")]
        Poor = 0x2b11
    }
}

