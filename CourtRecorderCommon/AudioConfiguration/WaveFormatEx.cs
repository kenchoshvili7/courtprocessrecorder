﻿using Un4seen.Bass;

namespace AudioConfiguration
{
    using System;
    using System.Runtime.InteropServices;
    using Un4seen.Bass.AddOn.Enc;

    [Serializable, StructLayout(LayoutKind.Sequential, Pack=2)]
    public class WaveFormatEx
    {
        public short wFormatTag;
        public short nChannels;
        public int nSamplesPerSec;
        public int nAvgBytesPerSec;
        public short nBlockAlign;
        public short wBitsPerSample;
        public short cbSize;
        public byte[] ExtraData;
        public static ACMFORMAT WaveFormatExToBassACMFORMAT(WaveFormatEx format)
        {
            ACMFORMAT acmformat = new ACMFORMAT();
            acmformat.waveformatex.cbSize = format.cbSize;
            acmformat.waveformatex.nAvgBytesPerSec = format.nAvgBytesPerSec;
            acmformat.waveformatex.nBlockAlign = format.nBlockAlign;
            acmformat.waveformatex.nChannels = format.nChannels;
            acmformat.waveformatex.nSamplesPerSec = format.nSamplesPerSec;
            acmformat.waveformatex.wBitsPerSample = format.wBitsPerSample;
            acmformat.waveformatex.wFormatTag = (WAVEFormatTag) format.wFormatTag;
            acmformat.extension = format.ExtraData;
            return acmformat;
        }

        public static WaveFormatEx BassACMFORMATToWaveFormatEx(ACMFORMAT bassFmt)
        {
            WaveFormatEx ex = new WaveFormatEx();
            ex.nAvgBytesPerSec = bassFmt.waveformatex.nAvgBytesPerSec;
            ex.nBlockAlign = bassFmt.waveformatex.nBlockAlign;
            ex.nChannels = bassFmt.waveformatex.nChannels;
            ex.nSamplesPerSec = bassFmt.waveformatex.nSamplesPerSec;
            ex.wBitsPerSample = bassFmt.waveformatex.wBitsPerSample;
            ex.wFormatTag = (short) bassFmt.waveformatex.wFormatTag;
            ex.cbSize = bassFmt.waveformatex.cbSize;
            ex.ExtraData = bassFmt.extension;
            return ex;
        }

        public override string ToString()
        {
            string str = "stereo";
            if (this.nChannels == 1)
            {
                str = "mono";
            }
            else if (this.nChannels == 3)
            {
                str = "2.1";
            }
            else if (this.nChannels == 4)
            {
                str = "quad";
            }
            else if (this.nChannels == 5)
            {
                str = "4.1";
            }
            else if (this.nChannels == 6)
            {
                str = "5.1";
            }
            else if (this.nChannels == 7)
            {
                str = "6.1";
            }
            else if (this.nChannels > 7)
            {
                str = this.nChannels.ToString() + "chans";
            }
            string str2 = "16-bit";
            if (this.wBitsPerSample == 0)
            {
                str2 = string.Format("{0}kbps", (this.nAvgBytesPerSec * 8) / 0x3e8);
            }
            else
            {
                str2 = this.wBitsPerSample.ToString() + "-bit";
                if (this.nAvgBytesPerSec > 0)
                {
                    str2 = str2 + string.Format(", {0}KB/sec.", this.nAvgBytesPerSec / 0x400);
                }
            }
            return string.Format("{0:##0.0#}kHz, {1}, {2}", ((double) this.nSamplesPerSec) / 1000.0, str2, str);
        }
    }
}

