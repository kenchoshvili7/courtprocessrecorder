﻿namespace AudioConfiguration
{
    using DeviceController;
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public class Channel
    {
        public AudioSourceChannels ChannelNo { get; set; }
        public ChannelType ChannelType { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int RecordVolume { get; set; }
        
        public Channel() { }

        public Channel(AudioSourceChannels channelNo, ChannelType channelType, string name, bool isActive)
        {
            Name = name;
            ChannelNo = channelNo;
            ChannelType = channelType;
            IsActive = isActive;
        }

        public Channel(AudioSourceChannels channelNo, string name)
        {
            Name = name;
            ChannelNo = channelNo;
            ChannelType = ChannelType.None;
            IsActive = false;
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            return ((obj is Channel) && (((Channel)obj).ChannelNo == ChannelNo));
        }

        public static Channel Empty
        {
            get
            {
                return new Channel(AudioSourceChannels.None, "NONE");
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

