﻿namespace AudioConfiguration
{
    using Un4seen.Bass;

    public static class BassRegistration
    {
        private static int ConvReg(char _c)
        {
            int num = 0x2a;
            char[] chArray = new char[] { 
                'q', '1', 'c', 'a', 'x', 'l', '-', '7', 'u', 'd', '3', 'b', '2', '9', 'n', 'g', 
                'f', 'v', 't', 'j', 'k', '5', 'm', 'e', 'o', 'p', '6', '@', 's', 'i', '8', 'h', 
                'w', '4', 'y', '_', '0', 'r', '.', 'z'
             };
            for (int i = 0; i < chArray.Length; i++)
            {
                if (_c == chArray[i])
                {
                    num = i;
                }
            }
            return num;
        }

        private static string GenerateRegistarionCode(string _regMail)
        {
            string str = "2X";
            if (_regMail.Length > 5)
            {
                char[] chArray = _regMail.ToLower().ToCharArray();
                int index = _regMail.IndexOf("@");
                str = str + ConvReg(chArray[0]).ToString() + ConvReg(chArray[1]).ToString() + ConvReg(chArray[index - 1]).ToString() + _regMail.Length.ToString() + ConvReg(chArray[index + 1]).ToString() + ConvReg(chArray[index + 2]).ToString() + ConvReg(chArray[_regMail.Length - 1]).ToString();
            }
            return str;
        }

        public static void Register()
        {
            BassNet.Registration("registered@user.com", GenerateRegistarionCode("registered@user.com"));
        }
    }
}

