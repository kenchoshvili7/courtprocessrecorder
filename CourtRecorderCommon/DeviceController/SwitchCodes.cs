﻿namespace DeviceController
{
    using System;

    [Flags]
    public enum SwitchCodes : byte
    {
        Channel1 = 8,
        Channel2 = 0x20,
        Channel3 = 2,
        Channel4 = 1,
        Channel5 = 4,
        Channel6 = 0x40,
        ChannelPC = 0x10,
        None = 0
    }
}

