﻿namespace DeviceController
{
    using System;

    [Flags]
    public enum ChannelButtons : byte
    {
        Channel1 = 0x10,
        Channel2 = 8,
        Channel3 = 4,
        Channel4 = 2,
        Channel5 = 1,
        Channel6 = 0x20,
        None = 0
    }
}

