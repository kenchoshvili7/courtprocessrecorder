﻿namespace DeviceController
{
    using System.Threading;
    using System.Timers;
    using UsbLibrary;

    public class UsbInterface : UsbHidPort
    {
        private System.Timers.Timer buttonScanner;
        private AudioSourceChannels currentStateOfChannels;
        private bool dataArrived;
        public InputReport inputReport;
        public const int MAX_PACKET_LENGTH = 0x41;
        private ChannelButtons oldButtonStates;
        private object transmissionLocked;

        public event ButtonPressedEvent ButtonPressed;

        public UsbInterface()
        {
            base.VendorId = 0x4d8;
            base.ProductId = 60;
            base.CheckDevicePresent();
            if (base.SpecifiedDevice != null)
            {
                this.inputReport = base.SpecifiedDevice.CreateInputReport();
                base.SpecifiedDevice.DataRecieved += new DataRecievedEventHandler(this.DataRecieved);
            }
            this.transmissionLocked = false;
            this.dataArrived = false;
            this.buttonScanner = new System.Timers.Timer(100.0);
            this.buttonScanner.AutoReset = true;
            this.buttonScanner.Elapsed += new ElapsedEventHandler(this.buttonScannerTimerElapsed);
            this.buttonScanner.Enabled = true;
        }

        private void buttonScannerTimerElapsed(object sender, ElapsedEventArgs e)
        {
            ChannelButtons buttonStates = this.GetButtonStates();
            this.buttonScanner.Enabled = false;
            try
            {
                buttonStates = (ChannelButtons) ((byte) (buttonStates & (ChannelButtons.Channel1 | ChannelButtons.Channel2 | ChannelButtons.Channel3 | ChannelButtons.Channel4 | ChannelButtons.Channel5)));
                if (this.oldButtonStates != buttonStates)
                {
                    if (((byte) (((byte) (this.oldButtonStates ^ buttonStates)) & 0x10)) == 0x10)
                    {
                        this.DoMasterButtonPressed(((byte) (buttonStates & ChannelButtons.Channel1)) != 0x10);
                    }
                    if (((byte) (((byte) (this.oldButtonStates ^ buttonStates)) & 8)) == 8)
                    {
                        this.DoButton2Pressed(((byte) (buttonStates & ChannelButtons.Channel2)) != 8);
                    }
                    if (((byte) (((byte) (this.oldButtonStates ^ buttonStates)) & 4)) == 4)
                    {
                        this.DoButton3Pressed(((byte) (buttonStates & ChannelButtons.Channel3)) != 4);
                    }
                    if (((byte) (((byte) (this.oldButtonStates ^ buttonStates)) & 2)) == 2)
                    {
                        this.DoButton4Pressed(((byte) (buttonStates & ChannelButtons.Channel4)) != 2);
                    }
                    if (((byte) (((byte) (this.oldButtonStates ^ buttonStates)) & 1)) == 1)
                    {
                        this.DoButton5Pressed(((byte) (buttonStates & ChannelButtons.Channel5)) != 1);
                    }
                    this.oldButtonStates = buttonStates;
                }
            }
            finally
            {
                this.buttonScanner.Enabled = true;
            }
        }

        private void DataRecieved(object sender, DataRecievedEventArgs args)
        {
            this.dataArrived = true;
            this.inputReport.Buffer = args.data;
        }

        protected void DoButton2Pressed(bool pressed)
        {
            if (this.ButtonPressed != null)
            {
                this.ButtonPressed(ChannelButtons.Channel2, pressed);
            }
        }

        protected void DoButton3Pressed(bool pressed)
        {
            if (this.ButtonPressed != null)
            {
                this.ButtonPressed(ChannelButtons.Channel3, pressed);
            }
        }

        protected void DoButton4Pressed(bool pressed)
        {
            if (this.ButtonPressed != null)
            {
                this.ButtonPressed(ChannelButtons.Channel4, pressed);
            }
        }

        protected void DoButton5Pressed(bool pressed)
        {
            if (this.ButtonPressed != null)
            {
                this.ButtonPressed(ChannelButtons.Channel5, pressed);
            }
        }

        protected void DoMasterButtonPressed(bool pressed)
        {
            if (this.ButtonPressed != null)
            {
                this.ButtonPressed(ChannelButtons.Channel1, pressed);
            }
        }

        public ChannelButtons GetButtonStates()
        {
            byte[] buff = new byte[0x41];
            byte[] buffer2 = new byte[0x41];
            buff[0] = 0;
            buff[1] = 0x12;
            lock (this.transmissionLocked)
            {
                if ((this.SendData(buff) && this.RecvData(out buffer2)) && (buffer2 != null))
                {
                    return (ChannelButtons) buffer2[2];
                }
                return ChannelButtons.None;
            }
        }

        public byte GetVolume()
        {
            byte[] buff = new byte[0x41];
            buff[0] = 0;
            buff[1] = 20;
            lock (this.transmissionLocked)
            {
                this.SendData(buff);
                if (this.RecvData(out buff))
                {
                    return buff[2];
                }
                return 0;
            }
        }

        public void Monitor(bool listen)
        {
        }

        private bool RecvData(out byte[] buff)
        {
            buff = null;
            int num = 0;
            while (!this.DataArrived && (num < 100))
            {
                Thread.Sleep(10);
                num++;
            }
            if (num < 100)
            {
                buff = this.inputReport.Buffer;
                return true;
            }
            return false;
        }

        public void SelectLineSources(AudioSourceChannels channels)
        {
            lock (this.transmissionLocked)
            {
                byte[] buff = new byte[0x41];
                buff[0] = 0;
                buff[1] = 0x13;
                SwitchCodes none = SwitchCodes.None;
                if (((byte) (channels & AudioSourceChannels.Channel1)) == 1)
                {
                    none = (SwitchCodes) ((byte) (none | SwitchCodes.Channel1));
                }
                if (((byte) (channels & AudioSourceChannels.Channel2)) == 2)
                {
                    none = (SwitchCodes) ((byte) (none | SwitchCodes.Channel2));
                }
                if (((byte) (channels & AudioSourceChannels.Channel3)) == 4)
                {
                    none = (SwitchCodes) ((byte) (none | SwitchCodes.Channel3));
                }
                if (((byte) (channels & AudioSourceChannels.Channel4)) == 8)
                {
                    none = (SwitchCodes) ((byte) (none | SwitchCodes.Channel4));
                }
                if (((byte) (channels & AudioSourceChannels.Channel5)) == 0x10)
                {
                    none = (SwitchCodes) ((byte) (none | SwitchCodes.Channel5));
                }
                if (((byte) (channels & AudioSourceChannels.ChannelPC)) == 0x20)
                {
                    none = (SwitchCodes) ((byte) (none | SwitchCodes.ChannelPC));
                }
                buff[2] = 0xff;
                if (this.SendData(buff))
                {
                    buff[1] = 0x11;
                    buff[2] = (byte) channels;
                    if (this.SendData(buff))
                    {
                        //if (((byte) (channels & ((byte) ~this.currentStateOfChannels))) == 0) todo replaced
                        if (((byte) (channels & (AudioSourceChannels)((byte) ~this.currentStateOfChannels))) == 0)
                        {
                            this.currentStateOfChannels = channels;
                        }
                        if (((byte) (channels & AudioSourceChannels.Channel5)) == 0x10)
                        {
                            Thread.Sleep(0x3e8);
                        }
                        else if ((((byte) (channels & AudioSourceChannels.ChannelPC)) == 0x20) || (((byte) 0) == 0))
                        {
                            Thread.Sleep(0x3e8);
                        }
                        else
                        {
                            Thread.Sleep(0x3e8);
                        }
                        buff[1] = 0x13;
                        buff[2] = (byte) ~none;
                        this.SendData(buff);
                    }
                }
            }
        }

        private bool SendData(byte[] buff)
        {
            lock (this.transmissionLocked)
            {
                if (base.SpecifiedDevice != null)
                {
                    base.SpecifiedDevice.SendData(buff);
                    return true;
                }
                return false;
            }
        }

        public void SetVolume(byte volume)
        {
            byte[] buff = new byte[0x41];
            buff[0] = 0;
            buff[1] = 0x10;
            buff[2] = volume;
            lock (this.transmissionLocked)
            {
                this.SendData(buff);
            }
        }

        public void UpdateLED(byte led, bool State)
        {
            byte[] buff = new byte[0x41];
            buff[0] = 0;
            buff[1] = 0x15;
            lock (this.transmissionLocked)
            {
                this.SendData(buff);
            }
        }

        private bool DataArrived
        {
            get
            {
                if (this.dataArrived)
                {
                    this.dataArrived = false;
                    return true;
                }
                return false;
            }
        }

        public delegate void ButtonPressedEvent(ChannelButtons channelButton, bool pressed);

        private enum CMD
        {
            ENABLE_PHANTOM = 0x11,
            GET_BUTTON_STATES = 0x12,
            GET_VOLUME = 20,
            SELECT_CHANNEL = 0x13,
            SET_VOLUME = 0x10,
            TOGGLE_LED = 0x15
        }
    }
}

