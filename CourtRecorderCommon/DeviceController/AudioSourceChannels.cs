﻿namespace DeviceController
{
    using System;

    [Flags]
    public enum AudioSourceChannels : byte
    {
        Channel1 = 1,
        Channel2 = 2,
        Channel3 = 4,
        Channel4 = 8,
        Channel5 = 0x10,
        Channel6 = 0x40,
        Channel7 = 0x60,
        Channel8 = 0x80,
        ChannelPC = 0x20,
        None = 0
    }
}

