﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace CourtRecorderCommon
{
    public static class User32Methods
    {
        [DllImport("user32.dll")]
        public static extern IntPtr ActivateKeyboardLayout(IntPtr hkl, uint flags);
        [DllImport("User32", CharSet=CharSet.Auto)]
        public static extern int CallWindowProc(IntPtr wndProc, IntPtr handle, uint msg, uint wParam, int lParam);
        [DllImport("User32.dll", CharSet=CharSet.Auto)]
        internal static extern int DrawText(IntPtr hdc, string lpString, int nCount, ref RECT lpRect, DrawTextFlags uFormat);
        [DllImport("user32.dll")]
        public static extern bool GetClientRect(IntPtr hWnd, IntPtr pRect);
        [DllImport("User32.dll")]
        internal static extern IntPtr GetDC(IntPtr hWnd);
        [DllImport("User32.dll")]
        internal static extern int GetSystemMetrics(int nIndex);
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowDC(IntPtr hWnd);
        [DllImport("User32", CharSet=CharSet.Auto)]
        public static extern int GetWindowLong(IntPtr handle, int nStyle);
        [DllImport("user32.dll")]
        public static extern int InvalidateRect(IntPtr hdc, IntPtr pRect, bool erase);
        [DllImport("User32")]
        public static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);
        [DllImport("User32.dll")]
        public static extern IntPtr LoadBitmap(IntPtr hInstance, int lpBitmapName);
        [DllImport("User32.dll")]
        internal static extern IntPtr LoadBitmap(IntPtr hInstance, string lpBitmapName);
        [DllImport("User32.dll")]
        internal static extern int LoadString(IntPtr hInstance, int uID, StringBuilder lpBuffer, int nBufferMax);
        [DllImport("User32", CharSet=CharSet.Auto)]
        public static extern int PostMessage(IntPtr handle, int msg, IntPtr wParam, IntPtr lParam);
        [DllImport("User32.dll")]
        internal static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);
        [DllImport("User32", CharSet=CharSet.Auto)]
        public static extern int SendMessage(IntPtr handle, int msg, IntPtr wParam, IntPtr lParam);
        [DllImport("User32", CharSet=CharSet.Auto)]
        public static extern int SendMessage(IntPtr handle, int msg, ref GETTEXTLENGTHEX wParam, IntPtr lParam);
        [DllImport("User32", CharSet=CharSet.Auto)]
        public static extern int SetWindowLong(IntPtr handle, int nStyle, int newStyle);

        public delegate int WndProc(IntPtr hwnd, uint msg, uint wParam, int lParam);
    }
}

