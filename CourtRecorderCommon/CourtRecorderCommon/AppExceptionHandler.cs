﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace CourtRecorderCommon
{
    public static class AppExceptionHandler
    {
        public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
        }

        public static void DisableExceptionHandling()
        {
            Application.ThreadException -= new ThreadExceptionEventHandler(AppExceptionHandler.OnThreadException);
        }

        public static void EnableExceptionHandling()
        {
            Application.ThreadException += new ThreadExceptionEventHandler(AppExceptionHandler.OnThreadException);
        }

        public static void OnThreadException(object sender, ThreadExceptionEventArgs e)
        {
            if (e.Exception.GetType() == typeof(EAbortException))
            {
                throw e.Exception;
            }
            try
            {
                if (e.Exception is WarningException)
                {
                    MessageBox.Show(e.Exception.Message, "გაფრთხილება", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    ExceptionDialog.Show(e.Exception);
                }
            }
            catch (Exception exception)
            {
                DialogResult oK = DialogResult.OK;
                try
                {
                    oK = MessageBox.Show(exception.Message + Environment.NewLine + Environment.NewLine + e.Exception.Message, "Fatal Error! Exception was not handled!", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
                }
                finally
                {
                    switch (oK)
                    {
                        case DialogResult.Cancel:
                        {
                            Application.Exit();
                        }
                            break; 
                    }
                }
            }
        }
    }
}

