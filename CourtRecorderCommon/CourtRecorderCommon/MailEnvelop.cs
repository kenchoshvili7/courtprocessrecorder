﻿using System;

namespace CourtRecorderCommon
{
    public class MailEnvelop
    {
        public int atts;
        public DateTime date;
        public string from;
        public string id;
        public string subject;
        public bool unread;
    }
}

