﻿using DeviceController;

namespace CourtRecorderCommon
{
    public delegate void RecordAmplitudeAvialable(float amplitude, AudioSourceChannels channels);
}