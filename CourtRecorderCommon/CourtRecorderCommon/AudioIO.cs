﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using AudioConfiguration;
using DeviceController;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.Wave;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Mix;
using Un4seen.Bass.Misc;
using Un4seen.BassAsio;

namespace CourtRecorderCommon
{
    public class AudioIO
    {
        private static Apolo16Mixer apolo16Mixer;
        private static string applicationPath;
        private static string archiveDirectory;
        //private static AudioConfiguration.Configuration configuration;
        private static RecorderConfiguration _recorderConfiguration;
        private static RecordSet currentRecordSet;
        private static bool enabled;
        private static bool initialized;
        private static bool? isRecordingPossible;
        private static bool listening;
        public static PlaybackChannel PlaybackChannel;
        private static DeviceInfo[] playbackDevices;
        private static ProgramMode? programMode;
        public static BaseRecorder Recorder;
        private static long recordStartPosition;
        private static AudioSourceChannels selectedAudioChannels = AudioSourceChannels.None;
        private static LocalSettingsManager settingManager;
        private static UsbInterface usbInterface;
        private static bool usePriorityLogic;
        private static string workingDirectory;
        internal static string workingPath;
        public static event PlaybackMeterStreamVolume PlaybackMeterStreamVolume;

        private static int Apolo16MixerDataSent(object sender, short[] data, int length)
        {
            if ((PlaybackChannel.Mixer != 0) && PlaybackChannel.Playback)
            {
                return (Bass.BASS_ChannelGetData(PlaybackChannel.Mixer, data, length * 2) / 2);
            }
            return 0;
        }

        public static byte AudioSourceChannelToIndex(AudioSourceChannels channel)
        {
            if (((byte)(channel & AudioSourceChannels.Channel1)) <= 0)
            {
                if (((byte)(channel & AudioSourceChannels.Channel2)) > 0)
                {
                    return 2;
                }
                if (((byte)(channel & AudioSourceChannels.Channel3)) > 0)
                {
                    return 3;
                }
                if (((byte)(channel & AudioSourceChannels.Channel4)) > 0)
                {
                    return 4;
                }
                if (((byte)(channel & AudioSourceChannels.Channel5)) > 0)
                {
                    return 5;
                }
                if (((byte)(channel & AudioSourceChannels.Channel6)) > 0)
                {
                    return 6;
                }
                if (((byte)(channel & AudioSourceChannels.ChannelPC)) > 0)
                {
                    return 0x10;
                }
            }
            return 1;
        }

        public static void Dispose()
        {
            if (usbInterface != null)
            {
                usbInterface.Dispose();
            }
            if (apolo16Mixer != null)
            {
                apolo16Mixer.Dispose();
            }
            if (IsInitialized(false))
            {
                if (Recorder != null)
                {
                    Recorder.Dispose();
                }
                Bass.BASS_Free();
            }
        }

        public static DeviceInfo GetPlaybackDevice()
        {
            return PlaybackChannel.Device;
        }

        public static DeviceInfo GetPlaybackDevice(int deviceNo)
        {
            foreach (var info in PlaybackDevices)
            {
                if (info.DeviceNo == deviceNo)
                {
                    return info;
                }
            }
            return null;
        }

        public static int GetPlaybackLevel(AudioSourceChannels channelNo)
        {
            var num = 0;
            if ((IsInitialized(true) && (PlaybackChannel.Mixer != 0)) && (PlaybackChannel.RecordSet != null))
            {
                PlaybackChannel.PeakLevels.TryGetValue(channelNo, out num);
            }
            return num;
        }

        public static int GetPlaybackVolume()
        {
            var volume = 100;
            if (IsInitialized(true))
            {
                volume = PlaybackChannel.Volume;
            }
            return volume;
        }

        public static bool Initialize()
        {
            if (!enabled)
            {
                return false;
            }
            if (!initialized)
            {
                BassRegistration.Register();
                initialized = true;
                PlaybackChannel = new PlaybackChannel();
                if (Configuration.Format != null)
                {
                    var dictionary = new Dictionary<int, bool>();
                    var flag = false;
                    if (Configuration.DeviceNo == 0x7ffffff1)
                    {
                        //flag = Un4seen.Bass.Bass.BASS_Init(-1, Configuration.Format.nSamplesPerSec, BASSInit.BASS_DEVICE_MONO, IntPtr.Zero, null); todo replaced
                        flag = Bass.BASS_Init(-1, Configuration.Format.nSamplesPerSec, BASSInit.BASS_DEVICE_MONO, IntPtr.Zero);
                    }
                    else
                    {
                        //flag = Un4seen.Bass.Bass.BASS_Init(Configuration.DeviceNo, Configuration.Format.nSamplesPerSec, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero, null); todo replaced
                        flag = Bass.BASS_Init(Configuration.DeviceNo, Configuration.Format.nSamplesPerSec, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
                    }
                    var flag2 = false;
                    if (ProgramMode == ProgramMode.Recording)
                    {
                        if (Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                        {
                            if ((Configuration.DeviceNo == 0x7ffffff1) && Apolo16Mixer.Connected)
                            {
                                Recorder = new Apolo16Recorder();
                                flag2 = true;
                                foreach (var channel in Configuration.Channels)
                                {
                                    flag2 &= ((Apolo16Recorder)Recorder).AddChannel(Configuration.DeviceNo, channel.ChannelNo);
                                }
                            }
                            else
                            {
                                Recorder = new MultiChannelRecorder();
                                //flag2 = Un4seen.BassAsio.BassAsio.BASS_ASIO_Init(Configuration.DeviceNo) & ((BassAsio.BASS_ASIO_GetDevice() == Configuration.DeviceNo) || BassAsio.BASS_ASIO_SetDevice(Configuration.DeviceNo)); todo replaced
                                flag2 = BassAsio.BASS_ASIO_Init(Configuration.DeviceNo, BASSASIOInit.BASS_ASIO_DEFAULT) & ((BassAsio.BASS_ASIO_GetDevice() == Configuration.DeviceNo) || BassAsio.BASS_ASIO_SetDevice(Configuration.DeviceNo));
                                foreach (var channel in Configuration.Channels)
                                {
                                    flag2 &= ((MultiChannelRecorder)Recorder).AddChannel(Configuration.DeviceNo, channel.ChannelNo);
                                }
                            }
                        }
                        else if (Configuration.ProgramConfiguration == ProgramConfiguration.SingleChannel)
                        {
                            if ((Bass.BASS_RecordGetDevice() == Configuration.DeviceNo) || Bass.BASS_RecordSetDevice(Configuration.DeviceNo))
                            {
                                flag2 = Bass.BASS_RecordGetDeviceInfo(Configuration.DeviceNo) != null;
                            }
                            if (!Bass.BASS_RecordInit(Configuration.DeviceNo))
                            {
                                MessageBox.Show("Could not initialize Recorder : " + Bass.BASS_ErrorGetCode().ToString());
                            }
                            if ((Bass.BASS_RecordGetDevice() == Configuration.DeviceNo) || Bass.BASS_RecordSetDevice(Configuration.DeviceNo))
                            {
                                Recorder = new Recorder();
                            }
                            else
                            {
                                MessageBox.Show("Could not start Recorder : " + Bass.BASS_ErrorGetCode().ToString());
                            }
                        }
                    }
                }
                if (PlaybackDevices.Length > 0)
                {
                    //int deviceNo = PlaybackDevices[0].DeviceNo; todo old
                    var deviceNo = PlaybackDevices[0].DeviceNo;
                    var s = SettingManager.Read("PlaybackDeviceNo", deviceNo.ToString());
                    int.TryParse(s, out deviceNo);
                    SetPlaybackDevice(GetPlaybackDevice(deviceNo));
                }
            }
            return true;
        }

        public static void InitializeBass()
        {
            BassRegistration.Register();
            Bass.BASS_Init(0, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
        }

        public static void CheckForDeviceCount()
        {
            if (WaveOut.DeviceCount == 0)
            {
                Bass.BASS_Init(0, Configuration.Format.nSamplesPerSec, BASSInit.BASS_DEVICE_MONO, IntPtr.Zero);
            }
        }

        public static void InitializePlaybackChannels(RecordSet recordSet)
        {
            InitializePlaybackChannels(recordSet, 0L);
        }

        public static void InitializePlaybackChannels(RecordSet recordSet, long offset)
        {
            if (IsInitialized(true) && (recordSet != null))
            {
                PlaybackChannel.RecordSet = recordSet;
                PlaybackChannel.ClearPeakLevelMeters();
                var strArray = recordSet.FileName.Split(new char[] { ':' });
                var channelPC = AudioSourceChannels.ChannelPC;
                foreach (var str in strArray)
                {
                    if (str.Contains("?"))
                    {
                        var result = 0x20;
                        int.TryParse(str.Split(new char[] { '?' })[0], out result);
                        channelPC = (AudioSourceChannels)((byte)result);
                    }
                    if (PlaybackChannel.PlaybackBASSStreams.ContainsKey(channelPC))
                    {
                        var num2 = PlaybackChannel.PlaybackBASSStreams[channelPC];
                        PlaybackChannel.PlaybackBASSStreams.Remove(channelPC);
                        if (num2 != 0)
                        {
                            BassMix.BASS_Mixer_ChannelRemove(num2);
                            Bass.BASS_StreamFree(num2);
                        }
                    }
                    var handle = Bass.BASS_StreamCreateFile(workingPath + str.Substring(str.IndexOf('?') + 1), 0L, 0L, BASSFlag.BASS_MUSIC_DECODE | BASSFlag.BASS_MIXER_NONSTOP);
                    var num4 = Bass.BASS_ChannelGetLength(handle);
                    if (handle != 0)
                    {
                        PlaybackChannel.PlaybackBASSStreams.Add(channelPC, handle);
                        PlaybackChannel.SetMuted(channelPC, PlaybackChannel.GetMuted(channelPC));
                        if (PlaybackChannel.Device.DeviceNo == 0x7ffffff1)
                        {
                            BassMix.BASS_Mixer_StreamAddChannelEx(PlaybackChannel.Mixer, handle, BASSFlag.BASS_MUSIC_DECODE | BASSFlag.BASS_MIXER_NONSTOP | BASSFlag.BASS_MIDI_NOFX, offset, 0L);
                        }
                        else
                        {
                            BassMix.BASS_Mixer_StreamAddChannelEx(PlaybackChannel.Mixer, handle, BASSFlag.BASS_MIXER_NONSTOP, offset, 0L);
                        }
                        BassMix.BASS_Mixer_ChannelPlay(handle);
                        var item = new DSP_PeakLevelMeter(handle, 0);
                        item.User = new IntPtr(handle);
                        item.UpdateTime = 0.01f;
                        item.Notification += new EventHandler(PlaybackPeakLevelMeterNotification);
                        PlaybackChannel.PeakLevels.Add(channelPC, 0);
                        PlaybackChannel.PeakLevelMeters.Add(item);
                        item.Start();
                        PlaybackChannel.Volume = PlaybackChannel.Volume;
                    }
                }
            }
        }

        public static bool IsInitialized(bool doInitialization)
        {
            if (!(initialized || !doInitialization))
            {
                Initialize();
            }
            return initialized;
        }

        public static bool IsRecordingPossible()
        {
            if (!isRecordingPossible.HasValue)
            {
                if (((ProgramMode == ProgramMode.Recording) && IsInitialized(true)) && (Configuration.Channels.Count > 0))
                {
                    isRecordingPossible = true;
                }
                else
                {
                    isRecordingPossible = false;
                }
            }
            return isRecordingPossible.Value;
        }

        public static void ListenRecorder(bool listen)
        {
            if (((ProgramMode == ProgramMode.Recording) && IsInitialized(true)) && (Configuration.Channels.Count > 0))
            {
                UsbInterface.Monitor(listen);
                if (Apolo16Mixer.Connected)
                {
                    if (listen)
                    {
                        Apolo16Mixer.MonitorSource = MonitorSource.Microphones;
                    }
                    else
                    {
                        Apolo16Mixer.MonitorSource = MonitorSource.None;
                    }
                }
                listening = listen;
            }
        }

        public static void PausePlayback()
        {
            if (IsInitialized(true) && (PlaybackChannel.Mixer != 0))
            {
                Bass.BASS_ChannelPause(PlaybackChannel.Mixer);
                PlaybackChannel.Playback = false;
            }
        }

        private static void PlaybackPeakLevelMeterNotification(object sender, EventArgs e)
        {
            var meter = sender as DSP_PeakLevelMeter;
            if (meter != null)
            {
                foreach (var pair in PlaybackChannel.PlaybackBASSStreams)
                {
                    if (pair.Value == meter.User.ToInt32())
                    {
                        PlaybackChannel.PeakLevels[pair.Key] = (meter.LevelL * 100) / 0x8000;
                        PlaybackMeterStreamVolume?.Invoke(pair.Key, (float)(meter.LevelL) / 0x8000);
                    }
                }
            }
        }

        public static void SeekPlayback(RecordSet rs, long position)
        {
            if (IsInitialized(true))
            {
                if ((PlaybackChannel.Mixer == 0) || (PlaybackChannel.CurrentDevice != PlaybackChannel.Device))
                {
                    if (PlaybackChannel.Mixer != 0)
                    {
                        for (var i = 0; i < PlaybackChannel.EQ.Length; i++)
                        {
                            if (PlaybackChannel.EQ[i] != 0)
                            {
                                Bass.BASS_ChannelRemoveFX(PlaybackChannel.Mixer, PlaybackChannel.EQ[0]);
                            }
                        }
                    }
                    if ((PlaybackChannel.CurrentDevice != null) && (PlaybackChannel.Mixer != 0))
                    {
                        Bass.BASS_SetDevice(PlaybackChannel.CurrentDevice.DeviceNo);
                        if (PlaybackChannel.Device.DeviceNo != 0x7ffffff1)
                        {
                            Bass.BASS_ChannelStop(PlaybackChannel.Mixer);
                        }
                        else
                        {
                            PlaybackChannel.Playback = false;
                        }
                        Bass.BASS_StreamFree(PlaybackChannel.Mixer);
                    }
                    PlaybackChannel.CurrentDevice = PlaybackChannel.Device;
                    if (PlaybackChannel.Device.DeviceNo == 0x7ffffff1)
                    {
                        PlaybackChannel.Mixer = BassMix.BASS_Mixer_StreamCreate(Apolo16Mixer.Frequency, 1, BASSFlag.BASS_MUSIC_DECODE | BASSFlag.BASS_MIXER_NONSTOP | BASSFlag.BASS_MIDI_NOFX);
                        Apolo16Mixer.Play();
                    }
                    else
                    {
                        Bass.BASS_SetDevice(PlaybackChannel.Device.DeviceNo);
                        PlaybackChannel.Mixer = BassMix.BASS_Mixer_StreamCreate(0xac44, 1, BASSFlag.BASS_DEFAULT);
                        Apolo16Mixer.StopPlay();
                    }
                    SetPlaybackVolume(GetPlaybackVolume());
                    var par = new BASS_DX8_PARAMEQ();
                    par.fBandwidth = 18f;
                    PlaybackChannel.EQ[0] = Bass.BASS_ChannelSetFX(PlaybackChannel.Mixer, BASSFXType.BASS_FX_DX8_PARAMEQ, 0);
                    PlaybackChannel.EQ[1] = Bass.BASS_ChannelSetFX(PlaybackChannel.Mixer, BASSFXType.BASS_FX_DX8_PARAMEQ, 0);
                    PlaybackChannel.EQ[2] = Bass.BASS_ChannelSetFX(PlaybackChannel.Mixer, BASSFXType.BASS_FX_DX8_PARAMEQ, 0);
                    PlaybackChannel.EQ[3] = Bass.BASS_ChannelSetFX(PlaybackChannel.Mixer, BASSFXType.BASS_FX_DX8_PARAMEQ, 0);
                    PlaybackChannel.EQ[4] = Bass.BASS_ChannelSetFX(PlaybackChannel.Mixer, BASSFXType.BASS_FX_DX8_PARAMEQ, 0);
                    par.fCenter = 80f;
                    par.fGain = ((float)PlaybackChannel.EQValues[0]) / 10f;
                    Bass.BASS_FXSetParameters(PlaybackChannel.EQ[0], par);
                    par.fCenter = 240f;
                    par.fGain = ((float)PlaybackChannel.EQValues[1]) / 10f;
                    Bass.BASS_FXSetParameters(PlaybackChannel.EQ[1], par);
                    par.fCenter = 875f;
                    par.fGain = ((float)PlaybackChannel.EQValues[2]) / 10f;
                    Bass.BASS_FXSetParameters(PlaybackChannel.EQ[2], par);
                    par.fCenter = 3182f;
                    par.fGain = ((float)PlaybackChannel.EQValues[3]) / 10f;
                    Bass.BASS_FXSetParameters(PlaybackChannel.EQ[3], par);
                    par.fCenter = 8000f;
                    par.fGain = ((float)PlaybackChannel.EQValues[4]) / 10f;
                    Bass.BASS_FXSetParameters(PlaybackChannel.EQ[4], par);
                }
                else if (PlaybackChannel.Device.DeviceNo != 0x7ffffff1)
                {
                    Bass.BASS_SetDevice(PlaybackChannel.Device.DeviceNo);
                }
                else
                {
                    Apolo16Mixer.Play();
                }
                Bass.BASS_ChannelLock(PlaybackChannel.Mixer, true);
                if (rs != PlaybackChannel.RecordSet)
                {
                    StopPlayback();
                    InitializePlaybackChannels(rs);
                }
                if (position > -1L)
                {
                    var recordSet = PlaybackChannel.RecordSet;
                    var pos = position;
                    if (PlaybackChannel.Device.DeviceNo == 0x7ffffff1)
                    {
                        pos -= ((long)(recordSet.RecordSetData[0].Position * (((float)Apolo16Mixer.Frequency) / 1000f))) * 4L;
                    }
                    else
                    {
                        pos -= ((long)(recordSet.RecordSetData[0].Position * 44.1f)) * 4L;
                    }
                    var flag = false;
                    foreach (var pair in PlaybackChannel.PlaybackBASSStreams)
                    {
                        var num3 = Bass.BASS_ChannelGetLength(pair.Value);
                        if ((pos < 0L) || (pos >= num3))
                        {
                            Bass.BASS_StreamFree(pair.Value);
                            BassMix.BASS_Mixer_ChannelRemove(pair.Value);
                            flag = true;
                        }
                        else
                        {
                            if (!BassMix.BASS_Mixer_ChannelSetPosition(pair.Value, pos, BASSMode.BASS_POS_BYTES))
                            {
                                var error = Bass.BASS_ErrorGetCode();
                                if ((error == BASSError.BASS_ERROR_POSITION) || (error == BASSError.BASS_OK))
                                {
                                }
                            }
                            if (PlaybackChannel.Device.DeviceNo != 0x7ffffff1)
                            {
                                BassMix.BASS_Mixer_ChannelPlay(pair.Value);
                            }
                        }
                    }
                    if (flag)
                    {
                        if (PlaybackChannel.Device.DeviceNo == 0x7ffffff1)
                        {
                            InitializePlaybackChannels(rs, ((long)(recordSet.RecordSetData[0].Position * (((float)Apolo16Mixer.Frequency) / 1000f))) * 4L);
                        }
                        else
                        {
                            InitializePlaybackChannels(rs, ((long)(recordSet.RecordSetData[0].Position * 44.1f)) * 4L);
                        }
                    }
                }
                Bass.BASS_ChannelLock(PlaybackChannel.Mixer, false);
                if (PlaybackChannel.Device.DeviceNo == 0x7ffffff1)
                {
                    Apolo16Mixer.MonitorSource = MonitorSource.None | MonitorSource.PC;
                    PlaybackChannel.Playback = true;
                }
                else
                {
                    Apolo16Mixer.MonitorSource = MonitorSource.None;
                    Bass.BASS_ChannelPlay(PlaybackChannel.Mixer, false);
                }
            }
        }

        public static void SetPlaybackDevice(DeviceInfo device)
        {
            if (device != null)
            {
                if (device.DeviceNo == 0x7ffffff1)
                {
                    PlaybackChannel.Device = device;
                    SettingManager.WriteInteger("PlaybackDeviceNo", device.DeviceNo);
                    Apolo16Mixer.DataSent += Apolo16MixerDataSent;
                    //Apolo16Mixer.Play(); //todo why it is here???
                }
                else
                {
                    BASS_DEVICEINFO bass_deviceinfo;
                    Apolo16Mixer.StopPlay();
                    if (device != null)
                    {
                        bass_deviceinfo = Bass.BASS_GetDeviceInfo(device.DeviceNo);
                    }
                    else
                    {
                        bass_deviceinfo = new BASS_DEVICEINFO();
                    }
                    if (!bass_deviceinfo.IsInitialized)
                    {
                        var error = BASSError.BASS_ERROR_UNKNOWN;
                        if ((device != null) && bass_deviceinfo.IsEnabled)
                        {
                            //if (Un4seen.Bass.Bass.BASS_Init(device.DeviceNo, 0xac44, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero, null)) todo replaced
                            if (Bass.BASS_Init(device.DeviceNo, 0xac44, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero))
                            {
                                error = BASSError.BASS_OK;
                            }
                            else
                            {
                                error = Bass.BASS_ErrorGetCode();
                            }
                        }
                        for (var i = 0; (error != BASSError.BASS_OK) && (i < PlaybackDevices.Length); i++)
                        {
                            device = null;
                            //if (Un4seen.Bass.Bass.BASS_Init(PlaybackDevices[i].DeviceNo, 0xac44, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero, null)) todo replaced
                            if (Bass.BASS_Init(PlaybackDevices[i].DeviceNo, 0xac44, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero))
                            {
                                error = BASSError.BASS_OK;
                                device = PlaybackDevices[i];
                            }
                            else
                            {
                                error = Bass.BASS_ErrorGetCode();
                            }
                        }
                    }
                    if (device == null)
                    {
                        device = new DeviceInfo(-1, "No Sound", "NONE");
                    }
                    PlaybackChannel.Device = device;
                    if (device.DeviceNo != -1)
                    {
                        Bass.BASS_SetDevice(device.DeviceNo);
                        Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_BUFFER, 200);
                        Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_UPDATEPERIOD, 20);
                        SettingManager.WriteInteger("PlaybackDeviceNo", device.DeviceNo);
                    }
                }
            }
        }

        public static void SetPlaybackEQValues(int band, int gain)
        {
            if (((IsInitialized(true) && (PlaybackChannel.Mixer != 0)) && ((band >= 0) && (band < 5))) && (PlaybackChannel.EQ[band] != 0))
            {
                var par = new BASS_DX8_PARAMEQ();
                if (Bass.BASS_FXGetParameters(PlaybackChannel.EQ[band], par))
                {
                    par.fGain = gain / 10;
                    Bass.BASS_FXSetParameters(PlaybackChannel.EQ[band], par);
                }
            }
        }

        public static void SetPlaybackPitch(float pitch)
        {
            if (IsInitialized(true) && (PlaybackChannel.Mixer != 0))
            {
                PlaybackChannel.Pitch = 1f + (pitch / 100f);
                if (PlaybackChannel.Device.DeviceNo == 0x7ffffff1)
                {
                    Bass.BASS_ChannelSetAttribute(PlaybackChannel.Mixer, BASSAttribute.BASS_ATTRIB_FREQ, Apolo16Mixer.Frequency * PlaybackChannel.Pitch);
                }
                else
                {
                    Bass.BASS_ChannelSetAttribute(PlaybackChannel.Mixer, BASSAttribute.BASS_ATTRIB_FREQ, 44100f * PlaybackChannel.Pitch);
                }
            }
        }

        public static void SetPlaybackVolume(int value)
        {
            if (IsInitialized(true))
            {
                PlaybackChannel.Volume = value;
                SettingManager.WriteInteger("VOLUME_PLAYBACK", value);
            }
        }

        public static void SetWorkingPath(string _workingPath)
        {
            if (string.IsNullOrEmpty(_workingPath))
            {
                workingPath = ApplicationPath;
            }
            else if (!_workingPath.EndsWith(@"\"))
            {
                workingPath = _workingPath + @"\";
            }
        }

        public static bool StartRecord(AudioSourceChannels channel)
        {
            var flag = false;
            if (IsInitialized(true) && (Configuration.Channels.Count > 0))
            {
                //if (string.IsNullOrEmpty(workingPath))
                //{
                //    return flag;
                //}
                //try
                //{
                //    Directory.CreateDirectory(workingPath);
                //}
                //catch
                //{
                //    return flag;
                //}

                Recorder.Start();
                var currentRecordSet = new RecordSet(string.Empty);
                recordStartPosition = 0L;
                if (Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                {
                    currentRecordSet.RecordSetData.Add(new RecordSetData(recordStartPosition, AudioSourceChannels.None));
                    recordStartPosition = Recorder.GetPosition();
                }

                //currentRecordSet = null;
                //string fileName = Recorder.Start();
                //flag = fileName != null;
                //if (flag)
                //{
                //    currentRecordSet = new RecordSet(fileName);
                //    recordStartPosition = 0L;
                //    if (Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                //    {
                //        currentRecordSet.RecordSetData.Add(new RecordSetData(recordStartPosition, AudioSourceChannels.None));
                //        recordStartPosition = Recorder.GetPosition();
                //    }
                //}
            }
            return true;
            //return flag;
        }

        public static void StopPlayback()
        {
            if (!IsInitialized(true) || (PlaybackChannel.Mixer == 0)) return;

            // added by lj
            //Apolo16Mixer.DataSent -= Apolo16MixerDataSent;
            //

            PlaybackChannel.ClearPeakLevelMeters();
            if (PlaybackChannel.RecordSet != null)
            {
                var playbackBASSStreams = PlaybackChannel.PlaybackBASSStreams;
                PlaybackChannel.PlaybackBASSStreams = new Dictionary<AudioSourceChannels, int>();
                foreach (var pair in playbackBASSStreams)
                {
                    if (pair.Value != 0)
                    {
                        BassMix.BASS_Mixer_ChannelRemove(pair.Value);
                        Bass.BASS_StreamFree(pair.Value);
                    }
                }
                playbackBASSStreams.Clear();
            }
            if (PlaybackChannel.Device.DeviceNo != 0x7ffffff1)
            {
                Bass.BASS_ChannelStop(PlaybackChannel.Mixer);
            }
            else
            {
                PlaybackChannel.Playback = false;
            }
            PlaybackChannel.RecordSet = null;
        }

        public static void StopRecord()
        {
            if (IsInitialized(false) && (Recorder != null))
            {
                var num = Recorder.Stop();
                if ((Configuration.ProgramConfiguration == ProgramConfiguration.SingleChannel) && (recordStartPosition == 0L))
                {
                    recordStartPosition = num;
                }
                //if (currentRecordSet.RecordSetData.Count > 0)
                //{
                //    currentRecordSet.RecordSetData[currentRecordSet.RecordSetData.Count - 1].Length = (num - recordStartPosition) - currentRecordSet.RecordSetData[currentRecordSet.RecordSetData.Count - 1].Position;
                //}
            }
        }

        public static Apolo16Mixer Apolo16Mixer
        {
            get
            {
                if (apolo16Mixer == null)
                {
                    apolo16Mixer = new Apolo16Mixer();
                }
                return apolo16Mixer;
            }
        }

        public static string ApplicationPath
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(applicationPath))
                    {
                        applicationPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().CodeBase.Replace("file:///", ""));
                        if (!((applicationPath == null) || applicationPath.EndsWith(@"\")))
                        {
                            applicationPath = applicationPath + @"\";
                        }
                    }
                    return applicationPath;
                }
                catch
                {
                    return @"c:\";
                }
            }
        }

        public static string ArchiveDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(archiveDirectory))
                {
                    archiveDirectory = Configuration.ArchiveDirectory;
                    if (string.IsNullOrEmpty(archiveDirectory))
                    {
                        archiveDirectory = ApplicationPath;
                    }
                }
                return archiveDirectory;
            }
            set
            {
                archiveDirectory = value;
                if (!(!string.IsNullOrEmpty(archiveDirectory) && Directory.Exists(archiveDirectory)))
                {
                    archiveDirectory = ApplicationPath;
                }
                if (!archiveDirectory.EndsWith(@"\"))
                {
                    archiveDirectory = archiveDirectory + @"\";
                }
            }
        }

        //[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public static AudioConfiguration.Configuration Configuration1
        //{
        //    get
        //    {
        //        if (configuration == null)
        //        {
        //            try
        //            {
        //                configuration = AudioConfiguration.Configuration.Load();
        //                for (int i = configuration.Channels.Count - 1; i >= 0; i--)
        //                {
        //                    if (configuration.Channels[i].ChannelType == ChannelType.None)
        //                    {
        //                        configuration.Channels.RemoveAt(i);
        //                    }
        //                }
        //            }
        //            catch (Exception exception)
        //            {
        //                EventLog.WriteEntry(AppDomain.CurrentDomain.FriendlyName, exception.ToString(), EventLogEntryType.Error, 100);
        //                MessageBox.Show(null, "შეცდომა კონფიგურაციის ჩატვირთვისას!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //                throw exception;
        //            }
        //        }
        //        return configuration;
        //    }
        //}

        public static RecorderConfiguration Configuration
        {
            get
            {
                if (_recorderConfiguration == null)
                {
                    try
                    {
                        _recorderConfiguration = ConfigurationHelper.GetConfiguration();//AudioConfiguration.Configuration.Load();
                        for (var i = _recorderConfiguration.Channels.Count - 1; i >= 0; i--)
                        {
                            if (_recorderConfiguration.Channels[i].ChannelType == ChannelType.None)
                            {
                                _recorderConfiguration.Channels.RemoveAt(i);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        EventLog.WriteEntry(AppDomain.CurrentDomain.FriendlyName, exception.ToString(), EventLogEntryType.Error, 100);
                        MessageBox.Show(null, "შეცდომა კონფიგურაციის ჩატვირთვისას!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        throw exception;
                    }
                }
                return _recorderConfiguration;
            }
        }

        public static RecordSet CurrentRecordSet
        {
            get
            {
                return currentRecordSet;
            }
        }

        public static bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

        public static DeviceInfo[] PlaybackDevices
        {
            get
            {
                if (playbackDevices == null)
                {
                    var bass_deviceinfoArray = Bass.BASS_GetDeviceInfos();
                    var list = new List<DeviceInfo>();
                    for (var i = 0; i < bass_deviceinfoArray.Length; i++)
                    {
                        if ((i != 0) && bass_deviceinfoArray[i].IsEnabled)
                        {
                            list.Add(new DeviceInfo(i, bass_deviceinfoArray[i].name, bass_deviceinfoArray[i].driver));
                        }
                    }
                    if (Apolo16Mixer.Connected)
                    {
                        list.Add(new DeviceInfo(0x7ffffff1, Apolo16Mixer.FriendlyName, Apolo16Mixer.DriverName));
                    }
                    playbackDevices = list.ToArray();
                }
                return playbackDevices;
            }
        }

        public static ProgramMode ProgramMode
        {
            get
            {
                return ProgramMode.Recording;
                if (!programMode.HasValue)
                {
                    if (File.Exists(ApplicationPath + "AudioConfigurator.exe"))
                    {
                        //programMode =  2; todo replaced
                        programMode = ProgramMode.Recording;
                    }
                    else if (File.Exists(ApplicationPath + @"Redist\WindowsXP-KB932716-v2-x86-ENU.exe"))
                    {
                        //programMode = 1; todo replaced
                        programMode = ProgramMode.Processing;
                    }
                    else
                    {
                        programMode = 0;
                    }
                }
                return programMode.Value;
            }
        }

        public static AudioSourceChannels SelectedAudioChannels
        {
            get
            {
                return selectedAudioChannels;
            }
            set
            {
                if (selectedAudioChannels != value)
                {
                    if (Configuration.ProgramConfiguration == ProgramConfiguration.SingleChannel)
                    {
                        var isRecording = false;
                        var num = 0L;
                        if (Recorder != null)
                        {
                            isRecording = Recorder.IsRecording;
                            if (isRecording)
                            {
                                num = Recorder.Pause();
                            }
                        }
                        if (ProgramMode == ProgramMode.Recording)
                        {
                            UsbInterface.SelectLineSources(value);
                            if (Recorder is Recorder)
                            {
                                ((Recorder)Recorder).Volume = ((Recorder)Recorder).GetChannelVolume(value);
                            }
                        }
                        if (isRecording)
                        {
                            if (recordStartPosition == 0L)
                            {
                                recordStartPosition = num;
                            }
                            if (currentRecordSet.RecordSetData.Count > 0)
                            {
                                currentRecordSet.RecordSetData[currentRecordSet.RecordSetData.Count - 1].Length = (num - recordStartPosition) - currentRecordSet.RecordSetData[currentRecordSet.RecordSetData.Count - 1].Position;
                            }
                            currentRecordSet.RecordSetData.Add(new RecordSetData(num - recordStartPosition, (value == AudioSourceChannels.None) ? selectedAudioChannels : value));
                            Recorder.Resume();
                        }
                        selectedAudioChannels = value;
                    }
                    else if ((Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel) && Apolo16Mixer.Connected)
                    {
                        Apolo16Mixer.Mute = (MixerChannels)((byte)(((byte)(~((Apolo16Recorder)Recorder).AudioSourceChannelToMixerChannels(value) & (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6))) | ((byte)(Apolo16Mixer.Mute & MixerChannels.Monitor))));
                        selectedAudioChannels = value;
                    }
                }
            }
        }

        public static LocalSettingsManager SettingManager
        {
            get
            {
                if (settingManager == null)
                {
                    settingManager = LocalSettingsManager.GetLocalSettingsManager();
                }
                return settingManager;
            }
        }

        public static UsbInterface UsbInterface
        {
            get
            {
                if (usbInterface == null)
                {
                    usbInterface = new UsbInterface();
                }
                return usbInterface;
            }
        }

        public static bool UsePriorityLogic
        {
            get
            {
                if (Apolo16Mixer.Connected)
                {
                    return usePriorityLogic;
                }
                return (UsbInterface.SpecifiedDevice != null);
            }
            set
            {
                usePriorityLogic = value;
            }
        }

        public static string WorkingDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(workingDirectory))
                {
                    workingDirectory = Configuration.ArchiveDirectory;
                    if (string.IsNullOrEmpty(workingDirectory))
                    {
                        workingDirectory = ApplicationPath;
                    }
                }
                return workingDirectory;
            }
            set
            {
                workingDirectory = value;
                if (!(!string.IsNullOrEmpty(workingDirectory) && Directory.Exists(workingDirectory)))
                {
                    workingDirectory = ApplicationPath;
                }
                if (!workingDirectory.EndsWith(@"\"))
                {
                    workingDirectory = workingDirectory + @"\";
                }
            }
        }
    }
}

