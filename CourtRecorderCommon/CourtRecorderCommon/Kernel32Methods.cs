﻿using System.Runtime.InteropServices;

namespace CourtRecorderCommon
{
    public static class Kernel32Methods
    {
        [DllImport("kernel32.dll")]
        public static extern int GetDriveType(string lpRootPathName);
    }
}

