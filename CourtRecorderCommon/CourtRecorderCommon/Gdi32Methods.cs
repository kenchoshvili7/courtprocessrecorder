﻿using System;
using System.Runtime.InteropServices;

namespace CourtRecorderCommon
{
    public static class Gdi32Methods
    {
        [DllImport("gdi32.dll")]
        public static extern bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, int RasterOp);
        [DllImport("Gdi32.dll")]
        internal static extern bool DeleteObject(IntPtr hObject);
        [DllImport("Gdi32.dll")]
        public static extern int GdiFlush();
        [DllImport("Gdi32.dll")]
        internal static extern int GetDeviceCaps(IntPtr hDC, int nIndex);
        [DllImport("Gdi32.dll")]
        public static extern bool GetTextMetrics(IntPtr hDC, IntPtr textMetrics);
        [DllImport("Gdi32.dll")]
        internal static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);
        [DllImport("Gdi32.dll")]
        internal static extern int SetBkMode(IntPtr hdc, int iBkMode);
        [DllImport("Gdi32.dll")]
        internal static extern int SetTextColor(IntPtr hdc, int crColor);
    }
}

