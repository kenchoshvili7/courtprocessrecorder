﻿using System;
using Un4seen.Bass;
using Un4seen.Bass.Misc;

namespace CourtRecorderCommon
{
    public class Recorder : BaseRecorder
    {
        public int AmplitudeLevel;
        public DSP_Gain DSPGain;
        public ACMEncoder Encoder;
        public int hRecorder;
        private bool? muted;
        public DSP_PeakLevelMeter PeakLevelMeter;
        private RECORDPROC recordingProcDelegate;
        private int? volume;

        public Recorder()
        {
            this.recordingProcDelegate = new RECORDPROC(this.RecordingProc);
            this.hRecorder = Un4seen.Bass.Bass.BASS_RecordStart(AudioIO.Configuration.Format.nSamplesPerSec, 1, BASSFlag.BASS_DEFAULT, this.recordingProcDelegate, IntPtr.Zero);
            if (!Un4seen.Bass.Bass.BASS_ChannelPlay(this.hRecorder, false))
            {
                throw new ApplicationException("Could not start Recorder : " + Un4seen.Bass.Bass.BASS_ErrorGetCode().ToString());
            }
            this.DSPGain = new DSP_Gain(this.hRecorder, 1);
            this.Encoder = new ACMEncoder(this.hRecorder, AudioIO.Configuration.Format);
        }

        public override bool Dispose()
        {
            if (this.hRecorder != 0)
            {
                Un4seen.Bass.Bass.BASS_ChannelStop(this.hRecorder);
            }
            this.DSPGain.Stop();
            this.DSPGain = null;
            this.Encoder.Dispose();
            this.Encoder = null;
            return base.Dispose();
        }

        public override long GetPosition()
        {
            return 0L;
        }

        public override long Pause()
        {
            Un4seen.Bass.Bass.BASS_ChannelPause(this.hRecorder);
            return Un4seen.Bass.Bass.BASS_ChannelGetPosition(this.hRecorder, BASSMode.BASS_POS_BYTES);
        }

        private unsafe bool RecordingProc(int handle, IntPtr buffer, int length, IntPtr user)
        {
            if ((length > 0) && (buffer != IntPtr.Zero))
            {
                int num = 0;
                short* numPtr = (short*) buffer;
                for (int i = 0; i < (length / 2); i++)
                {
                    if (numPtr[i] > num)
                    {
                        num = numPtr[i];
                    }
                }
                if (num > 0x8000)
                {
                    num = 0x8000;
                }
                this.AmplitudeLevel = (num * 100) / 0x8000;
            }
            return true;
        }

        public override void Resume()
        {
            Un4seen.Bass.Bass.BASS_ChannelPlay(this.hRecorder, false);
        }

        public override string Start()
        {
            string str = base.Start();
            if (!this.Encoder.Start(AudioIO.workingPath + str))
            {
                return null;
            }
            return str;
        }

        public override long Stop()
        {
            if (this.Encoder != null)
            {
                long num = Un4seen.Bass.Bass.BASS_ChannelGetPosition(this.hRecorder, BASSMode.BASS_POS_BYTES);
                this.Encoder.Pause(true);
                this.Encoder.Stop();
                return num;
            }
            return 0L;
        }

        public override bool IsRecording
        {
            get
            {
                return ((this.Encoder != null) && this.Encoder.IsActive);
            }
        }

        public bool Muted
        {
            get
            {
                if (!this.muted.HasValue)
                {
                    this.muted = new bool?(AudioIO.SettingManager.ReadBoolean("RECORDING_MUTED", false));
                }
                return this.muted.Value;
            }
            set
            {
                bool? muted = this.muted;
                bool flag = value;
                if ((muted.GetValueOrDefault() != flag) || !muted.HasValue)
                {
                    this.muted = new bool?(value);
                    if (this.muted.Value)
                    {
                        this.DSPGain.Gain = 0.0;
                    }
                    else
                    {
                        this.DSPGain.Gain = (((float) this.volume.Value) / 100f) * 1.5f;
                    }
                }
                AudioIO.SettingManager.WriteBoolean("RECORDING_MUTED", value);
            }
        }

        public int Volume
        {
            get
            {
                if (!this.volume.HasValue)
                {
                    this.volume = new int?(AudioIO.SettingManager.ReadInteger("RECORDING_VOLUME", 100));
                }
                return this.volume.Value;
            }
            set
            {
                this.volume = new int?(value);
                if (this.DSPGain != null)
                {
                    this.DSPGain.Gain = (((float) this.volume.Value) / 100f) * 1.5f;
                }
                AudioIO.SettingManager.WriteInteger("RECORDING_VOLUME", this.volume.Value);
            }
        }
    }
}

