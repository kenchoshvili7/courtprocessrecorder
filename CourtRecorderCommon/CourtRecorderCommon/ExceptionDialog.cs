﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace CourtRecorderCommon
{
    public class ExceptionDialog : BaseForm
    {
        public Label bottomLine;
        private CheckBox checkBoxDetails;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeaderProperty;
        private ColumnHeader columnHeaderValue;
        private IContainer components;
        private string DefaultAdminMail;
        private string DefaultHelpUrl;
        private string DetailsSection;
        private string generatedExcMail;
        private Label label1;
        private LinkLabel linkLabel;
        private ListView listViewOther;
        private bool loaded;
        private string mailMessageFilePath;
        private Button OkButton;
        private Panel panelDetailed;
        private RichTextBox richTextBoxGenInfo;
        private string screenShot;
        private Button sendButton;
        private TabControl tabControl;
        private TabPage tabPageGenInfo;
        private TabPage tabPageInners;
        private TabPage tabPageModules;
        private TabPage tabPageOther;
        private TabPage tabPageStack;
        private TextBox textBoxException;
        private TextBox textBoxLoadedModules;
        private TextBox textBoxStack;
        private TreeView treeViewInners;
        private TreeView treeViewStack;
        private bool windowDragged;

        public ExceptionDialog()
        {
            this.DetailsSection = "ExceptionDialog.ShowDetails";
            this.DefaultHelpUrl = "http://www.deltasystems.ge";
            this.screenShot = null;
            this.mailMessageFilePath = null;
            this.loaded = false;
            this.windowDragged = false;
            this.components = null;
            this.InitializeComponent();
            AudioIO.SettingManager.ReadFormSettings(this);
        }

        public ExceptionDialog(Exception error)
        {
            this.DetailsSection = "ExceptionDialog.ShowDetails";
            this.DefaultHelpUrl = "http://www.deltasystems.ge";
            this.screenShot = null;
            this.mailMessageFilePath = null;
            this.loaded = false;
            this.windowDragged = false;
            this.components = null;
            this.CaptureScreen();
            this.InitializeComponent();
            this.DefaultAdminMail = "court_support@deltasystems.ge";
            AudioIO.SettingManager.ReadFormSettings(this);
            this.checkBoxDetails.Checked = AudioIO.SettingManager.ReadBoolean(this.DetailsSection);
            if (!this.checkBoxDetails.Checked)
            {
                this.checkBox1_CheckedChanged(null, null);
            }
            this.textBoxException.Text = error.GetType().FullName;
            this.WriteGeneralInfo(error);
            this.WriteInnerExceptions(error);
            this.WriteStackTrace(error);
            this.WriteOtherInfo(error);
            this.WriteLoadedModules();
        }

        private void BlendForm(bool blend)
        {
            if (blend)
            {
                base.Opacity = 0.6;
            }
            else
            {
                base.Opacity = 1.0;
            }
        }

        private void CaptureScreen()
        {
            Form activeForm = Form.ActiveForm;
            if (activeForm != null)
            {
                IntPtr windowDC = User32Methods.GetWindowDC(activeForm.Handle);
                using (Graphics graphics = Graphics.FromHdc(windowDC))
                {
                    using (Image image = new Bitmap(activeForm.Width, activeForm.Height, graphics))
                    {
                        using (Graphics graphics2 = Graphics.FromImage(image))
                        {
                            IntPtr hdc = graphics2.GetHdc();
                            Gdi32Methods.BitBlt(hdc, 0, 0, activeForm.Width, activeForm.Height, windowDC, 0, 0, 0xcc0020);
                            graphics2.ReleaseHdc(hdc);
                            User32Methods.ReleaseDC(activeForm.Handle, windowDC);
                            this.screenShot = Path.GetTempPath() + "error_rpt" + new Random().Next().ToString("X") + ".jpg";
                            image.Save(this.screenShot, ImageFormat.Jpeg);
                        }
                    }
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.loaded)
            {
                this.WriteDetailStateSettings(!this.checkBoxDetails.Checked);
            }
            this.ReadDetailStateSettings(this.checkBoxDetails.Checked);
            this.panelDetailed.Visible = this.checkBoxDetails.Checked;
            this.linkLabel.Visible = !this.checkBoxDetails.Checked;
            AudioIO.SettingManager.WriteBoolean(this.DetailsSection, this.checkBoxDetails.Checked);
            this.loaded = true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ExceptionDialog_Deactivate(object sender, EventArgs e)
        {
            this.windowDragged = false;
            this.BlendForm(false);
        }

        private void ExceptionDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            AudioIO.SettingManager.WriteFormSettings(this);
            AudioIO.SettingManager.Persist();
        }

        private void ExceptionDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.WriteDetailStateSettings(this.checkBoxDetails.Checked);
        }

        private void ExceptionDialog_LocationChanged(object sender, EventArgs e)
        {
            this.BlendForm(this.windowDragged);
        }

        public string FormatTargetSite(MethodBase targetSite)
        {
            return targetSite.ToString();
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ExceptionDialog));
            this.OkButton = new Button();
            this.bottomLine = new Label();
            this.sendButton = new Button();
            this.columnHeaderProperty = new ColumnHeader();
            this.columnHeaderValue = new ColumnHeader();
            this.panelDetailed = new Panel();
            this.tabControl = new TabControl();
            this.tabPageGenInfo = new TabPage();
            this.richTextBoxGenInfo = new RichTextBox();
            this.tabPageInners = new TabPage();
            this.treeViewInners = new TreeView();
            this.tabPageStack = new TabPage();
            this.textBoxStack = new TextBox();
            this.treeViewStack = new TreeView();
            this.tabPageOther = new TabPage();
            this.listViewOther = new ListView();
            this.columnHeader1 = new ColumnHeader();
            this.columnHeader2 = new ColumnHeader();
            this.tabPageModules = new TabPage();
            this.textBoxLoadedModules = new TextBox();
            this.label1 = new Label();
            this.textBoxException = new TextBox();
            this.linkLabel = new LinkLabel();
            this.checkBoxDetails = new CheckBox();
            this.panelDetailed.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageGenInfo.SuspendLayout();
            this.tabPageInners.SuspendLayout();
            this.tabPageStack.SuspendLayout();
            this.tabPageOther.SuspendLayout();
            this.tabPageModules.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this.OkButton, "OkButton");
            this.OkButton.DialogResult = DialogResult.OK;
            this.OkButton.Name = "OkButton";
            manager.ApplyResources(this.bottomLine, "bottomLine");
            this.bottomLine.BorderStyle = BorderStyle.Fixed3D;
            this.bottomLine.Name = "bottomLine";
            manager.ApplyResources(this.sendButton, "sendButton");
            this.sendButton.Name = "sendButton";
            this.sendButton.Click += new EventHandler(this.sendButton_Click);
            manager.ApplyResources(this.columnHeaderProperty, "columnHeaderProperty");
            manager.ApplyResources(this.columnHeaderValue, "columnHeaderValue");
            manager.ApplyResources(this.panelDetailed, "panelDetailed");
            this.panelDetailed.Controls.Add(this.tabControl);
            this.panelDetailed.Controls.Add(this.label1);
            this.panelDetailed.Controls.Add(this.textBoxException);
            this.panelDetailed.Name = "panelDetailed";
            manager.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Controls.Add(this.tabPageGenInfo);
            this.tabControl.Controls.Add(this.tabPageInners);
            this.tabControl.Controls.Add(this.tabPageStack);
            this.tabControl.Controls.Add(this.tabPageOther);
            this.tabControl.Controls.Add(this.tabPageModules);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabPageGenInfo.Controls.Add(this.richTextBoxGenInfo);
            manager.ApplyResources(this.tabPageGenInfo, "tabPageGenInfo");
            this.tabPageGenInfo.Name = "tabPageGenInfo";
            manager.ApplyResources(this.richTextBoxGenInfo, "richTextBoxGenInfo");
            this.richTextBoxGenInfo.Name = "richTextBoxGenInfo";
            this.richTextBoxGenInfo.ReadOnly = true;
            this.tabPageInners.Controls.Add(this.treeViewInners);
            manager.ApplyResources(this.tabPageInners, "tabPageInners");
            this.tabPageInners.Name = "tabPageInners";
            manager.ApplyResources(this.treeViewInners, "treeViewInners");
            this.treeViewInners.Name = "treeViewInners";
            this.tabPageStack.Controls.Add(this.textBoxStack);
            this.tabPageStack.Controls.Add(this.treeViewStack);
            manager.ApplyResources(this.tabPageStack, "tabPageStack");
            this.tabPageStack.Name = "tabPageStack";
            manager.ApplyResources(this.textBoxStack, "textBoxStack");
            this.textBoxStack.Name = "textBoxStack";
            this.textBoxStack.ReadOnly = true;
            manager.ApplyResources(this.treeViewStack, "treeViewStack");
            this.treeViewStack.Name = "treeViewStack";
            this.tabPageOther.Controls.Add(this.listViewOther);
            manager.ApplyResources(this.tabPageOther, "tabPageOther");
            this.tabPageOther.Name = "tabPageOther";
            this.listViewOther.Columns.AddRange(new ColumnHeader[] { this.columnHeader1, this.columnHeader2 });
            manager.ApplyResources(this.listViewOther, "listViewOther");
            this.listViewOther.FullRowSelect = true;
            this.listViewOther.GridLines = true;
            this.listViewOther.Name = "listViewOther";
            this.listViewOther.ShowGroups = false;
            this.listViewOther.UseCompatibleStateImageBehavior = false;
            this.listViewOther.View = View.Details;
            manager.ApplyResources(this.columnHeader1, "columnHeader1");
            manager.ApplyResources(this.columnHeader2, "columnHeader2");
            this.tabPageModules.Controls.Add(this.textBoxLoadedModules);
            manager.ApplyResources(this.tabPageModules, "tabPageModules");
            this.tabPageModules.Name = "tabPageModules";
            manager.ApplyResources(this.textBoxLoadedModules, "textBoxLoadedModules");
            this.textBoxLoadedModules.Name = "textBoxLoadedModules";
            this.textBoxLoadedModules.ReadOnly = true;
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.textBoxException, "textBoxException");
            this.textBoxException.Name = "textBoxException";
            this.textBoxException.ReadOnly = true;
            manager.ApplyResources(this.linkLabel, "linkLabel");
            this.linkLabel.Name = "linkLabel";
            manager.ApplyResources(this.checkBoxDetails, "checkBoxDetails");
            this.checkBoxDetails.Name = "checkBoxDetails";
            this.checkBoxDetails.CheckedChanged += new EventHandler(this.checkBox1_CheckedChanged);
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.OkButton;
            manager.ApplyResources(this, "$this");
            base.Controls.Add(this.checkBoxDetails);
            base.Controls.Add(this.bottomLine);
            base.Controls.Add(this.OkButton);
            base.Controls.Add(this.sendButton);
            base.Controls.Add(this.panelDetailed);
            base.Controls.Add(this.linkLabel);
            base.Name = "ExceptionDialog";
            base.SizeGripStyle = SizeGripStyle.Show;
            base.Deactivate += new EventHandler(this.ExceptionDialog_Deactivate);
            base.FormClosed += new FormClosedEventHandler(this.ExceptionDialog_FormClosed);
            base.FormClosing += new FormClosingEventHandler(this.ExceptionDialog_FormClosing);
            base.LocationChanged += new EventHandler(this.ExceptionDialog_LocationChanged);
            this.panelDetailed.ResumeLayout(false);
            this.panelDetailed.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageGenInfo.ResumeLayout(false);
            this.tabPageInners.ResumeLayout(false);
            this.tabPageStack.ResumeLayout(false);
            this.tabPageStack.PerformLayout();
            this.tabPageOther.ResumeLayout(false);
            this.tabPageModules.ResumeLayout(false);
            this.tabPageModules.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public bool IsHelpLinkAvailable(string helpLink)
        {
            return ((helpLink != null) && (helpLink != ""));
        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("IExplore.exe", e.Link.LinkData.ToString());
        }

        private void ReadDetailStateSettings(bool detailed)
        {
            string[] strArray;
            string str = AudioIO.SettingManager.Read(base.Name);
            string section = detailed ? (base.Name + ".Detailed") : (base.Name + ".Simple");
            int width = base.Width;
            int height = base.Height;
            if (str.Length > 0)
            {
                strArray = str.Split(new char[] { Convert.ToChar(",") });
                base.Left = Convert.ToInt16(strArray[0]);
                base.Top = Convert.ToInt16(strArray[1]);
            }
            str = AudioIO.SettingManager.Read(section);
            if (str.Length > 0)
            {
                strArray = str.Split(new char[] { Convert.ToChar(",") });
                width = Convert.ToInt16(strArray[0]);
                height = Convert.ToInt16(strArray[1]);
            }
            if (width > 0)
            {
                base.Width = width;
            }
            if (height > 0)
            {
                base.Height = height;
            }
        }

        private void richTextBoxGenInfo_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start("IExplore.exe", e.LinkText);
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            Mapi mapi = new Mapi();
            mapi.Logon(base.Handle);
            mapi.AddRecip("Administrator", this.DefaultAdminMail, false);
            if (this.screenShot != null)
            {
                mapi.Attach(this.screenShot);
                this.mailMessageFilePath = Path.GetTempPath() + "error_rpt" + new Random().Next().ToString("X") + ".txt";
                File.WriteAllText(this.mailMessageFilePath, this.generatedExcMail, Encoding.Unicode);
                mapi.Attach(this.mailMessageFilePath);
            }
            mapi.Send("Error report", this.generatedExcMail, true);
            mapi.Logoff();
        }

        public static DialogResult Show(Exception error)
        {
            if (error == null)
            {
                error = new Exception("Null exception encountered", error);
            }
            ExceptionDialog dialog = new ExceptionDialog(error);
            return dialog.ShowDialog();
        }

        private void treeViewInners_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                Process.Start("IExplore.exe", e.Node.Tag.ToString());
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 160:
                    this.windowDragged = false;
                    this.BlendForm(false);
                    break;

                case 0xa1:
                    this.windowDragged = m.WParam.ToInt32() == 2;
                    break;
            }
            base.WndProc(ref m);
        }

        private void WriteDetailStateSettings(bool detailed)
        {
            AudioIO.SettingManager.Write(base.Name, base.Left.ToString() + "," + base.Top.ToString());
            string section = detailed ? (base.Name + ".Detailed") : (base.Name + ".Simple");
            AudioIO.SettingManager.Write(section, base.Width.ToString() + "," + base.Height.ToString());
        }

        public void WriteGeneralInfo(Exception error)
        {
            this.richTextBoxGenInfo.Clear();
            string helpLink = error.HelpLink;
            if (!this.IsHelpLinkAvailable(helpLink))
            {
                helpLink = this.DefaultHelpUrl;
            }
            this.generatedExcMail = "Exception was generated on " + DateTime.Now.ToString() + "\r\n";
            this.generatedExcMail = "Error message : \"" + error.Message + "\"\r\n\r\n";
            this.linkLabel.Text = error.Message + "\r\n\r\nFor troubleshooting follow link: ";
            int length = this.linkLabel.Text.Length;
            this.linkLabel.Text = this.linkLabel.Text + helpLink;
            this.linkLabel.Links.Add(length, this.linkLabel.Text.Length - length, helpLink);
            this.richTextBoxGenInfo.SelectedText = "The method \r\n";
            if (error.TargetSite != null)
            {
                object generatedExcMail = this.generatedExcMail;
                this.generatedExcMail = string.Concat(new object[] { generatedExcMail, "\r\nException was generated in : \"", error.TargetSite, "\"\r\n\r\n" });
                this.richTextBoxGenInfo.SelectionFont = new Font(this.richTextBoxGenInfo.SelectionFont, FontStyle.Bold);
                this.richTextBoxGenInfo.SelectedText = this.FormatTargetSite(error.TargetSite);
            }
            this.richTextBoxGenInfo.SelectionStart = this.richTextBoxGenInfo.TextLength;
            this.richTextBoxGenInfo.SelectionFont = new Font(this.richTextBoxGenInfo.SelectionFont, FontStyle.Regular);
            this.richTextBoxGenInfo.SelectedText = "\r\ngenerated exception with following message: \r\n\t\"" + error.Message + "\"";
            this.richTextBoxGenInfo.SelectedText = "\r\n\r\nFor troubleshooting follow link: " + helpLink;
        }

        public void WriteInnerExceptions(Exception error)
        {
            this.treeViewInners.BeginUpdate();
            try
            {
                this.treeViewInners.Nodes.Clear();
                Exception innerException = error.InnerException;
                TreeNode node = null;
                TreeNode node2 = null;
                TreeNode node3 = null;
                string str2 = "  ";
                if (innerException != null)
                {
                    this.generatedExcMail = this.generatedExcMail + "\r\nInner exceptions : \r\n\r\n";
                }
                while (innerException != null)
                {
                    node = new TreeNode(innerException.GetType().FullName);
                    string generatedExcMail = this.generatedExcMail;
                    this.generatedExcMail = generatedExcMail + str2 + "Exception class: " + innerException.GetType().FullName + "\r\n";
                    if (innerException.TargetSite != null)
                    {
                        object obj2 = this.generatedExcMail;
                        this.generatedExcMail = string.Concat(new object[] { obj2, str2, "Target: ", error.TargetSite, "\r\n" });
                        node.Nodes.Add(new TreeNode("Target: " + this.FormatTargetSite(innerException.TargetSite)));
                    }
                    node.Nodes.Add(new TreeNode("Message: " + innerException.Message));
                    generatedExcMail = this.generatedExcMail;
                    this.generatedExcMail = generatedExcMail + str2 + "Message: " + innerException.Message + "\r\n";
                    string helpLink = innerException.HelpLink;
                    if (!this.IsHelpLinkAvailable(helpLink))
                    {
                        helpLink = this.DefaultHelpUrl;
                    }
                    node3 = new TreeNode("Help Link: " + helpLink);
                    node3.Tag = helpLink;
                    node.Nodes.Add(node3);
                    if (node2 != null)
                    {
                        node2.Nodes.Add(node);
                    }
                    else
                    {
                        this.treeViewInners.Nodes.Add(node);
                    }
                    innerException = innerException.InnerException;
                    node2 = node;
                    str2 = str2 + "  ";
                }
                this.treeViewInners.ExpandAll();
            }
            finally
            {
                this.treeViewInners.EndUpdate();
            }
        }

        public void WriteLoadedModules()
        {
            Process currentProcess = Process.GetCurrentProcess();
            this.generatedExcMail = this.generatedExcMail + "\r\n Loaded Modules: \r\n";
            foreach (ProcessModule module in currentProcess.Modules)
            {
                string str = module.BaseAddress.ToString("X8") + " : " + module.FileVersionInfo.ToString() + "\r\n";
                this.generatedExcMail = this.generatedExcMail + str;
                this.textBoxLoadedModules.Text = this.textBoxLoadedModules.Text + str;
            }
        }

        public void WriteOtherInfo(Exception error)
        {
            this.listViewOther.BeginUpdate();
            try
            {
                System.Type type = typeof(Exception);
                this.listViewOther.Items.Clear();
                bool flag = false;
                foreach (PropertyInfo info in error.GetType().GetProperties())
                {
                    if (type.GetProperty(info.Name) == null)
                    {
                        object obj2 = info.GetValue(error, null);
                        string text = (obj2 == null) ? string.Empty : obj2.ToString();
                        this.listViewOther.Items.Add(info.Name).SubItems.Add(text);
                        if (!flag)
                        {
                            this.generatedExcMail = this.generatedExcMail + "\r\nOther information : \r\n";
                            flag = !flag;
                        }
                        string generatedExcMail = this.generatedExcMail;
                        this.generatedExcMail = generatedExcMail + "  " + info.Name + " : " + text + "\r\n";
                    }
                }
            }
            finally
            {
                this.listViewOther.EndUpdate();
            }
        }

        public void WriteStackTrace(Exception error)
        {
            if (error.StackTrace != null)
            {
                this.textBoxStack.Text = error.StackTrace.ToString();
                this.generatedExcMail = this.generatedExcMail + "\r\nStack trace: \r\n  " + this.textBoxStack.Text + "\r\n\r\n";
            }
        }
    }
}

