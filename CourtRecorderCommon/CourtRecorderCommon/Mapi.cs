﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace CourtRecorderCommon
{
    public class Mapi
    {
        private ArrayList attachs = new ArrayList();
        private int error = 0;
        private readonly string[] errors = new string[] { 
            "OK [0]", "User abort [1]", "General MAPI failure [2]", "MAPI login failure [3]", "Disk full [4]", "Insufficient memory [5]", "Access denied [6]", "-unknown- [7]", "Too many sessions [8]", "Too many files were specified [9]", "Too many recipients were specified [10]", "A specified attachment was not found [11]", "Attachment open failure [12]", "Attachment write failure [13]", "Unknown recipient [14]", "Bad recipient type [15]", 
            "No messages [16]", "Invalid message [17]", "Text too large [18]", "Invalid session [19]", "Type not supported [20]", "A recipient was specified ambiguously [21]", "Message in use [22]", "Network failure [23]", "Invalid edit fields [24]", "Invalid recipients [25]", "Not supported [26]"
         };
        private string findseed = null;
        private MapiMessage lastMsg = null;
        private StringBuilder lastMsgID = new StringBuilder(600);
        private const int MapiBCC = 3;
        private const int MapiBodyAsFile = 0x200;
        private const int MapiCC = 2;
        private const int MapiDialog = 8;
        private const int MapiEnvOnly = 0x40;
        private const int MapiExtendedUI = 0x20;
        private const int MapiForceDownload = 0x1000;
        private const int MapiGuaranteeFiFo = 0x100;
        private const int MapiLogonUI = 1;
        private const int MapiLongMsgID = 0x4000;
        private const int MapiNewSession = 2;
        private const int MapiORIG = 0;
        private const int MapiPasswordUI = 0x20000;
        private const int MapiPeek = 0x80;
        private const int MapiReceiptReq = 2;
        private const int MapiSent = 4;
        private const int MapiSuprAttach = 0x800;
        private const int MapiTO = 1;
        private const int MapiUnread = 1;
        private const int MapiUnreadOnly = 0x20;
        private MapiRecipDesc origin = new MapiRecipDesc();
        private ArrayList recpts = new ArrayList();
        private IntPtr session = IntPtr.Zero;
        private IntPtr winhandle = IntPtr.Zero;

        public void AddRecip(string name, string addr, bool cc)
        {
            MapiRecipDesc desc = new MapiRecipDesc();
            if (cc)
            {
                desc.recipClass = 2;
            }
            else
            {
                desc.recipClass = 1;
            }
            desc.name = name;
            desc.address = addr;
            this.recpts.Add(desc);
        }

        private IntPtr AllocAttachs(out int fileCount)
        {
            fileCount = 0;
            if (this.attachs == null)
            {
                return IntPtr.Zero;
            }
            if ((this.attachs.Count <= 0) || (this.attachs.Count > 100))
            {
                return IntPtr.Zero;
            }
            Type t = typeof(MapiFileDesc);
            int num = Marshal.SizeOf(t);
            IntPtr ptr = Marshal.AllocHGlobal((int) (this.attachs.Count * num));
            MapiFileDesc structure = new MapiFileDesc();
            structure.position = -1;
            int num2 = (int) ptr;
            for (int i = 0; i < this.attachs.Count; i++)
            {
                string path = this.attachs[i] as string;
                structure.name = Path.GetFileName(path);
                structure.path = path;
                Marshal.StructureToPtr(structure, (IntPtr) num2, false);
                num2 += num;
            }
            fileCount = this.attachs.Count;
            return ptr;
        }

        private IntPtr AllocOrigin()
        {
            this.origin.recipClass = 0;
            Type t = typeof(MapiRecipDesc);
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(t));
            Marshal.StructureToPtr(this.origin, ptr, false);
            return ptr;
        }

        private IntPtr AllocRecips(out int recipCount)
        {
            recipCount = 0;
            if (this.recpts.Count == 0)
            {
                return IntPtr.Zero;
            }
            Type t = typeof(MapiRecipDesc);
            int num = Marshal.SizeOf(t);
            IntPtr ptr = Marshal.AllocHGlobal((int) (this.recpts.Count * num));
            int num2 = (int) ptr;
            for (int i = 0; i < this.recpts.Count; i++)
            {
                Marshal.StructureToPtr(this.recpts[i] as MapiRecipDesc, (IntPtr) num2, false);
                num2 += num;
            }
            recipCount = this.recpts.Count;
            return ptr;
        }

        public void Attach(string filepath)
        {
            this.attachs.Add(filepath);
        }

        private void Dealloc()
        {
            int recips;
            int num3;
            Type t = typeof(MapiRecipDesc);
            int num = Marshal.SizeOf(t);
            if (this.lastMsg.originator != IntPtr.Zero)
            {
                Marshal.DestroyStructure(this.lastMsg.originator, t);
                Marshal.FreeHGlobal(this.lastMsg.originator);
            }
            if (this.lastMsg.recips != IntPtr.Zero)
            {
                recips = (int) this.lastMsg.recips;
                for (num3 = 0; num3 < this.lastMsg.recipCount; num3++)
                {
                    Marshal.DestroyStructure((IntPtr) recips, t);
                    recips += num;
                }
                Marshal.FreeHGlobal(this.lastMsg.recips);
            }
            if (this.lastMsg.files != IntPtr.Zero)
            {
                Type type2 = typeof(MapiFileDesc);
                int num4 = Marshal.SizeOf(type2);
                recips = (int) this.lastMsg.files;
                for (num3 = 0; num3 < this.lastMsg.fileCount; num3++)
                {
                    Marshal.DestroyStructure((IntPtr) recips, type2);
                    recips += num4;
                }
                Marshal.FreeHGlobal(this.lastMsg.files);
            }
        }

        public bool Delete(string id)
        {
            this.error = MAPIDeleteMail(this.session, this.winhandle, id, 0, 0);
            return (this.error == 0);
        }

        public string Error()
        {
            if (this.error <= 0x1a)
            {
                return this.errors[this.error];
            }
            return ("?unknown? [" + this.error.ToString() + "]");
        }

        private void GetAttachNames(out MailAttach[] aat)
        {
            aat = new MailAttach[this.lastMsg.fileCount];
            Type t = typeof(MapiFileDesc);
            int num = Marshal.SizeOf(t);
            MapiFileDesc structure = new MapiFileDesc();
            int files = (int) this.lastMsg.files;
            for (int i = 0; i < this.lastMsg.fileCount; i++)
            {
                Marshal.PtrToStructure((IntPtr) files, structure);
                files += num;
                aat[i] = new MailAttach();
                if (structure.flags == 0)
                {
                    aat[i].position = structure.position;
                    aat[i].name = structure.name;
                    aat[i].path = structure.path;
                }
            }
        }

        public void Logoff()
        {
            if (this.session != IntPtr.Zero)
            {
                this.error = MAPILogoff(this.session, this.winhandle, 0, 0);
                this.session = IntPtr.Zero;
            }
        }

        public bool Logon(IntPtr hwnd)
        {
            this.winhandle = hwnd;
            this.error = MAPILogon(hwnd, null, null, 0, 0, ref this.session);
            if (this.error != 0)
            {
                this.error = MAPILogon(hwnd, null, null, 1, 0, ref this.session);
            }
            return (this.error == 0);
        }

        [DllImport("MAPI32.DLL", CharSet=CharSet.Ansi)]
        private static extern int MAPIAddress(IntPtr sess, IntPtr hwnd, string caption, int editfld, string labels, int recipcount, IntPtr ptrrecips, int flg, int rsv, ref int newrec, ref IntPtr ptrnew);
        [DllImport("MAPI32.DLL", CharSet=CharSet.Ansi)]
        private static extern int MAPIDeleteMail(IntPtr sess, IntPtr hwnd, string id, int flg, int rsv);
        [DllImport("MAPI32.DLL", CharSet=CharSet.Ansi)]
        private static extern int MAPIFindNext(IntPtr sess, IntPtr hwnd, string typ, string seed, int flg, int rsv, StringBuilder id);
        [DllImport("MAPI32.DLL")]
        private static extern int MAPIFreeBuffer(IntPtr ptr);
        [DllImport("MAPI32.DLL")]
        private static extern int MAPILogoff(IntPtr sess, IntPtr hwnd, int flg, int rsv);
        [DllImport("MAPI32.DLL", CharSet=CharSet.Ansi)]
        private static extern int MAPILogon(IntPtr hwnd, string prf, string pw, int flg, int rsv, ref IntPtr sess);
        [DllImport("MAPI32.DLL", CharSet=CharSet.Ansi)]
        private static extern int MAPIReadMail(IntPtr sess, IntPtr hwnd, string id, int flg, int rsv, ref IntPtr ptrmsg);
        [DllImport("MAPI32.DLL")]
        private static extern int MAPISendMail(IntPtr sess, IntPtr hwnd, MapiMessage message, int flg, int rsv);
        public bool Next(ref MailEnvelop env)
        {
            this.error = MAPIFindNext(this.session, this.winhandle, null, this.findseed, 0x4000, 0, this.lastMsgID);
            if (this.error != 0)
            {
                return false;
            }
            this.findseed = this.lastMsgID.ToString();
            IntPtr zero = IntPtr.Zero;
            this.error = MAPIReadMail(this.session, this.winhandle, this.findseed, 0x8c0, 0, ref zero);
            if ((this.error != 0) || (zero == IntPtr.Zero))
            {
                return false;
            }
            this.lastMsg = new MapiMessage();
            Marshal.PtrToStructure(zero, this.lastMsg);
            MapiRecipDesc structure = new MapiRecipDesc();
            if (this.lastMsg.originator != IntPtr.Zero)
            {
                Marshal.PtrToStructure(this.lastMsg.originator, structure);
            }
            env.id = this.findseed;
            env.date = DateTime.ParseExact(this.lastMsg.dateReceived, "yyyy/MM/dd HH:mm", DateTimeFormatInfo.InvariantInfo);
            env.subject = this.lastMsg.subject;
            env.from = structure.name;
            env.unread = (this.lastMsg.flags & 1) != 0;
            env.atts = this.lastMsg.fileCount;
            this.error = MAPIFreeBuffer(zero);
            return (this.error == 0);
        }

        public string Read(string id, out MailAttach[] aat)
        {
            aat = null;
            IntPtr zero = IntPtr.Zero;
            this.error = MAPIReadMail(this.session, this.winhandle, id, 0x880, 0, ref zero);
            if ((this.error != 0) || (zero == IntPtr.Zero))
            {
                return null;
            }
            this.lastMsg = new MapiMessage();
            Marshal.PtrToStructure(zero, this.lastMsg);
            if (((this.lastMsg.fileCount > 0) && (this.lastMsg.fileCount < 100)) && (this.lastMsg.files != IntPtr.Zero))
            {
                this.GetAttachNames(out aat);
            }
            MAPIFreeBuffer(zero);
            return this.lastMsg.noteText;
        }

        public void Reset()
        {
            this.findseed = null;
            this.origin = new MapiRecipDesc();
            this.recpts.Clear();
            this.attachs.Clear();
            this.lastMsg = null;
        }

        private bool SaveAttachByName(string name, string savepath)
        {
            bool flag = true;
            Type t = typeof(MapiFileDesc);
            int num = Marshal.SizeOf(t);
            MapiFileDesc structure = new MapiFileDesc();
            int files = (int) this.lastMsg.files;
            for (int i = 0; i < this.lastMsg.fileCount; i++)
            {
                Marshal.PtrToStructure((IntPtr) files, structure);
                files += num;
                if ((structure.flags == 0) && (structure.name != null))
                {
                    try
                    {
                        if (name == structure.name)
                        {
                            if (File.Exists(savepath))
                            {
                                File.Delete(savepath);
                            }
                            File.Move(structure.path, savepath);
                        }
                    }
                    catch (Exception)
                    {
                        flag = false;
                        this.error = 13;
                    }
                    try
                    {
                        File.Delete(structure.path);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return flag;
        }

        public bool SaveAttachm(string id, string name, string savepath)
        {
            IntPtr zero = IntPtr.Zero;
            this.error = MAPIReadMail(this.session, this.winhandle, id, 0x80, 0, ref zero);
            if ((this.error != 0) || (zero == IntPtr.Zero))
            {
                return false;
            }
            this.lastMsg = new MapiMessage();
            Marshal.PtrToStructure(zero, this.lastMsg);
            bool flag = false;
            if (((this.lastMsg.fileCount > 0) && (this.lastMsg.fileCount < 100)) && (this.lastMsg.files != IntPtr.Zero))
            {
                flag = this.SaveAttachByName(name, savepath);
            }
            MAPIFreeBuffer(zero);
            return flag;
        }

        public bool Send(string sub, string txt, bool showDialog)
        {
            this.lastMsg = new MapiMessage();
            this.lastMsg.subject = sub;
            this.lastMsg.noteText = txt;
            this.lastMsg.originator = this.AllocOrigin();
            this.lastMsg.recips = this.AllocRecips(out this.lastMsg.recipCount);
            this.lastMsg.files = this.AllocAttachs(out this.lastMsg.fileCount);
            int flg = 0;
            if (showDialog)
            {
                flg |= 8;
            }
            this.error = MAPISendMail(this.session, this.winhandle, this.lastMsg, flg, 0);
            this.Dealloc();
            this.Reset();
            return (this.error == 0);
        }

        public void SetSender(string sname, string saddr)
        {
            this.origin.name = sname;
            this.origin.address = saddr;
        }

        public bool SingleAddress(string label, out string name, out string addr)
        {
            name = null;
            addr = null;
            int newrec = 0;
            IntPtr zero = IntPtr.Zero;
            this.error = MAPIAddress(this.session, this.winhandle, null, 1, label, 0, IntPtr.Zero, 0, 0, ref newrec, ref zero);
            if (((this.error != 0) || (newrec < 1)) || (zero == IntPtr.Zero))
            {
                return false;
            }
            MapiRecipDesc structure = new MapiRecipDesc();
            Marshal.PtrToStructure(zero, structure);
            name = structure.name;
            addr = structure.address;
            MAPIFreeBuffer(zero);
            return true;
        }
    }
}

