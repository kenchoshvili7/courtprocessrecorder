﻿using DeviceController;

namespace CourtRecorderCommon
{
    public delegate void RecordByteArrayAvialable(byte[] bytes, AudioSourceChannels channels);
}
