﻿using System.IO;

namespace CourtRecorderCommon
{
    public delegate void RecordStreamAvialableEvent(MemoryStream ms);
}