﻿namespace CourtRecorderCommon
{
    public enum ProgramMode
    {
        Playback,
        Processing,
        Recording
    }
}

