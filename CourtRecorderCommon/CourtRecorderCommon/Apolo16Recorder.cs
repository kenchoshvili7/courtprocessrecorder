﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using DeviceController;
using Un4seen.Bass;

namespace CourtRecorderCommon
{
    public class Apolo16Recorder : BaseRecorder
    {
        public Dictionary<AudioSourceChannels, int> AmplitudeLevel = new Dictionary<AudioSourceChannels, int>();
        private Dictionary<AudioSourceChannels, ACMEncoder> Encoders = new Dictionary<AudioSourceChannels, ACMEncoder>();
        private Thread encoderThread;
        private List<RecordedPacket> encodingData = new List<RecordedPacket>();
        private Dictionary<AudioSourceChannels, int> hStreams = new Dictionary<AudioSourceChannels, int>();
        private Dictionary<AudioSourceChannels, bool> Muted = new Dictionary<AudioSourceChannels, bool>();
        private bool recording;
        public double sampleRateRatio = 1.0;
        public event RecordStreamAvialableEvent RecordStreamAvialable;
        public event RecordByteArrayAvialable RecordByteArrayAvialable;
        private MemoryStream _recordStream;

        public Apolo16Recorder()
        {
            AudioIO.Apolo16Mixer.DataReceived += new AudioDataReceivedEventHandler(this.DataReceived);
            AudioIO.Apolo16Mixer.PowerSave = false;
            AudioIO.Apolo16Mixer.Frequency = AudioIO.Configuration.Format.nSamplesPerSec;
            this.encoderThread = new Thread(new ThreadStart(this.EncoderThreadProc));
            this.encoderThread.IsBackground = true;
            this.encoderThread.Priority = ThreadPriority.Highest;
            this.encoderThread.Start();
            AudioIO.Apolo16Mixer.Record();

            _recordStream = new MemoryStream();
        }

        internal bool AddChannel(int deviceNo, AudioSourceChannels channelNo)
        {
            this.sampleRateRatio = 1.0;
            int num = Un4seen.Bass.Bass.BASS_StreamCreatePush(AudioIO.Configuration.Format.nSamplesPerSec, 1, BASSFlag.BASS_MUSIC_DECODE, new IntPtr((int)channelNo));
            BASSError error = Un4seen.Bass.Bass.BASS_ErrorGetCode();
            //if ((num == 0) || (error != BASSError.BASS_OK))
            //{
            //    return false;
            //}
            this.hStreams.Add(channelNo, num);
            this.AmplitudeLevel.Add(channelNo, 0);
            this.SetChannelVolume(channelNo, this.GetChannelVolume(channelNo));
            this.Encoders.Add(channelNo, new ACMEncoder(this.hStreams[channelNo], AudioIO.Configuration.Format));
            this.Muted.Add(channelNo, this.GetChannelMuted(channelNo));
            return true;
        }

        public MixerChannels AudioSourceChannelToMixerChannels(AudioSourceChannels audioSourceChannel)
        {
            MixerChannels none = MixerChannels.None;
            if (((byte)(audioSourceChannel & AudioSourceChannels.Channel1)) == 1)
            {
                none = (MixerChannels)((byte)(none | MixerChannels.Microphone1));
            }
            if (((byte)(audioSourceChannel & AudioSourceChannels.Channel2)) == 2)
            {
                none = (MixerChannels)((byte)(none | MixerChannels.Microphone2));
            }
            if (((byte)(audioSourceChannel & AudioSourceChannels.Channel3)) == 4)
            {
                none = (MixerChannels)((byte)(none | MixerChannels.Microphone3));
            }
            if (((byte)(audioSourceChannel & AudioSourceChannels.Channel4)) == 8)
            {
                none = (MixerChannels)((byte)(none | MixerChannels.Microphone4));
            }
            if (((byte)(audioSourceChannel & AudioSourceChannels.Channel5)) == 0x10)
            {
                none = (MixerChannels)((byte)(none | MixerChannels.Microphone5));
            }
            if (((byte)(audioSourceChannel & AudioSourceChannels.Channel6)) == 0x40)
            {
                none = (MixerChannels)((byte)(none | MixerChannels.Microphone6));
            }
            if (((byte)(audioSourceChannel & AudioSourceChannels.ChannelPC)) == 0x20)
            {
                none = (MixerChannels)((byte)(none | MixerChannels.Monitor));
            }
            return none;
        }

        private void DataReceived(object sender, short[][] data, int length, int[] peaks)
        {
            if (this.recording)
            {
                lock (this.encodingData)
                {
                    this.encodingData.Add(new RecordedPacket(data, length, peaks));
                }
            }
        }

        public override bool Dispose()
        {
            AudioIO.Apolo16Mixer.StopRecord();
            AudioIO.Apolo16Mixer.PowerSave = true;
            foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
            {
                pair.Value.Stop();
                pair.Value.Dispose();
            }
            this.Encoders.Clear();
            foreach (KeyValuePair<AudioSourceChannels, int> pair2 in this.hStreams)
            {
                Un4seen.Bass.Bass.BASS_StreamFree(pair2.Value);
            }
            this.hStreams.Clear();
            this.encoderThread.Abort();
            return base.Dispose();
        }

        private void EncoderThreadProc()
        {
            while (Thread.CurrentThread.IsAlive)
            {
                List<RecordedPacket> list;
                RecordedPacket? nullable = null;
                lock ((list = this.encodingData))
                {
                    if (this.encodingData.Count > 0)
                    {
                        nullable = new RecordedPacket?(this.encodingData[0]);
                        this.encodingData.RemoveAt(0);
                    }
                }
                if (nullable.HasValue)
                {
                    int length = nullable.Value.Length * 2;
                    foreach (AudioSourceChannels channels in this.hStreams.Keys)
                    {
                        int index = AudioIO.AudioSourceChannelToIndex(channels) - 1;
                        if (index <= 5)
                        {
                            //int num3 = Un4seen.Bass.Bass.BASS_StreamPutData(this.hStreams[channels], nullable.Value.Data[index], length);
                            //num3 = Un4seen.Bass.Bass.BASS_ChannelGetData(this.hStreams[channels], nullable.Value.Data[index], num3);
                            //this.AmplitudeLevel[channels] = (nullable.Value.Peaks[index] * 100) / 0x8000;

                            var result = new byte[nullable.Value.Data[index].Length * sizeof(short)];
                            Buffer.BlockCopy(nullable.Value.Data[index], 0, result, 0, result.Length);

                            RecordByteArrayAvialable?.Invoke(result, channels);
                        }
                    }

                    //byte[] result = new byte[nullable.Value.Data[0].Length * sizeof(short)];
                    //Buffer.BlockCopy(nullable.Value.Data[0], 0, result, 0, result.Length);

                    //_recordStream.Write(result, 0, result.Length);
                    //_recordStream.Flush();
                    //if (RecordByteArrayAvialable != null) RecordByteArrayAvialable.Invoke(result);

                    //NaudioRecorder.waveFile.Write(result, 0, result.Length);
                    //NaudioRecorder.waveFile.Flush();
                    //
                    //NaudioRecorder.wri.Write(result, 0, result.Length);
                    //
                    //NaudioRecorder.MemoryStream.Write(result, 0, result.Length);

                }

                bool flag = false;
                lock ((list = this.encodingData))
                {
                    flag = this.encodingData.Count == 0;
                }
                if (flag)
                {
                    Thread.Sleep(10);
                }
            }
        }

        public bool GetChannelMuted(AudioSourceChannels audioSourceChannel)
        {
            return AudioIO.SettingManager.ReadBoolean("RECORDING_MUTED_" + audioSourceChannel.ToString(), false);
        }

        public override long GetPosition()
        {
            if (this.Encoders.Count > 0)
            {
                foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
                {
                    return (long)((int)(((double)Un4seen.Bass.Bass.BASS_ChannelGetPosition(this.hStreams[pair.Key], BASSMode.BASS_POS_BYTES)) / this.sampleRateRatio));
                }
            }
            return 0L;
        }

        public override long Pause()
        {
            throw new NotImplementedException();
        }

        public override void Resume()
        {
            throw new NotImplementedException();
        }

        public void SetChannelMuted(AudioSourceChannels audioSourceChannel, bool muted)
        {
            if (muted)
            {
                Apolo16Mixer mixer1 = AudioIO.Apolo16Mixer;
                mixer1.Mute = (MixerChannels)((byte)(mixer1.Mute | this.AudioSourceChannelToMixerChannels(audioSourceChannel)));
            }
            else
            {
                this.SetChannelVolume(audioSourceChannel, this.GetChannelVolume(audioSourceChannel));
                Apolo16Mixer mixer2 = AudioIO.Apolo16Mixer;
                //mixer2.Mute = (MixerChannels) ((byte) (mixer2.Mute & ((byte) ~this.AudioSourceChannelToMixerChannels(audioSourceChannel)))); todo old
                mixer2.Mute = (MixerChannels)((byte)((byte)mixer2.Mute & ((byte)~this.AudioSourceChannelToMixerChannels(audioSourceChannel))));
            }
            AudioIO.SettingManager.WriteBoolean("RECORDING_MUTED_" + audioSourceChannel.ToString(), muted);
        }

        public override void SetChannelVolume(AudioSourceChannels audioSourceChannel, int value)
        {
            base.SetChannelVolume(audioSourceChannel, value);
            AudioIO.Apolo16Mixer.SetVolume(this.AudioSourceChannelToMixerChannels(audioSourceChannel), (byte)(value + 0x9b));
        }

        public override string Start()
        {
            string str = null;
            //foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
            //{
            //    if (!pair.Value.Start(AudioIO.workingPath + base.Start()))
            //    {
            //        foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair2 in this.Encoders)
            //        {
            //            if (!string.IsNullOrEmpty(pair2.Value.FileName))
            //            {
            //                pair2.Value.Stop();
            //                File.Delete(pair2.Value.FileName);
            //            }
            //        }
            //        return null;
            //    }
            //    if (string.IsNullOrEmpty(str))
            //    {
            //        str = ((int)pair.Key).ToString() + "?" + Path.GetFileName(pair.Value.FileName);
            //    }
            //    else
            //    {
            //        string str3 = str;
            //        str = str3 + ":" + ((int)pair.Key).ToString() + "?" + Path.GetFileName(pair.Value.FileName);
            //    }
            //}
            //if (!string.IsNullOrEmpty(str))
            //{
            //    AudioIO.Apolo16Mixer.MonitorSource = MonitorSource.Microphones;
            //    this.recording = true;
            //}

            AudioIO.Apolo16Mixer.MonitorSource = MonitorSource.Microphones;
            this.recording = true;

            return str;
        }

        public override long Stop()
        {
            this.recording = false;
            if (this.Encoders.Count > 0)
            {
                long num = -1L;
                foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
                {
                    if (num == -1L)
                    {
                        num = Bass.BASS_ChannelGetPosition(this.hStreams[pair.Key], BASSMode.BASS_POS_BYTES);
                    }
                    pair.Value.Pause(true);
                    pair.Value.Stop();
                }
                return (long)((int)(((double)num) / this.sampleRateRatio));
            }
            return 0L;
        }

        public override bool IsRecording
        {
            get
            {
                foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
                {
                    return pair.Value.IsActive;
                }
                return false;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RecordedPacket
        {
            public short[][] Data;
            public int Length;
            public int[] Peaks;
            public RecordedPacket(short[][] data, int length, int[] peaks)
            {
                this.Data = data;
                this.Length = length;
                this.Peaks = peaks;
            }
        }
    }
}

