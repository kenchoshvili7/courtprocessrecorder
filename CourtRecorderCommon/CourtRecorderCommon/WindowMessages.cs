﻿namespace CourtRecorderCommon
{
    public static class WindowMessages
    {
        public const int WM_ACTIVATE = 6;
        public const int WM_ACTIVATEAPP = 0x1c;
        public const int WM_CHAR = 0x102;
        public const int WM_COMMAND = 0x111;
        public const int WM_COPY = 0x301;
        public const int WM_COPYDATA = 0x4a;
        public const int WM_CTLCOLORLISTBOX = 0x134;
        public const int WM_CUT = 0x300;
        public const int WM_ERASEBKGND = 20;
        public const int WM_KEYFIRST = 0x100;
        public const int WM_KEYLAST = 0x108;
        public const int WM_KEYUP = 0x101;
        public const int WM_LBUTTONDBLCLK = 0x203;
        public const int WM_LBUTTONDOWN = 0x201;
        public const int WM_LBUTTONUP = 0x202;
        public const int WM_MBUTTONDBLCLK = 0x209;
        public const int WM_MBUTTONDOWN = 0x207;
        public const int WM_MBUTTONUP = 520;
        public const int WM_MOUSEFIRST = 0x200;
        public const int WM_MOUSELAST = 0x20a;
        public const int WM_MOUSEMOVE = 0x200;
        public const int WM_MOUSEWHEEL = 0x20a;
        public const int WM_NCACTIVATE = 0x86;
        public const int WM_NCLBUTTONDOWN = 0xa1;
        public const int WM_NCMOUSEMOVE = 160;
        public const int WM_PAINT = 15;
        public const int WM_PASTE = 770;
        public const int WM_PRINT = 0x317;
        public const int WM_PRINTCLIENT = 0x318;
        public const int WM_RBUTTONDBLCLK = 0x206;
        public const int WM_RBUTTONDOWN = 0x204;
        public const int WM_RBUTTONUP = 0x205;
        public const int WM_SETFONT = 0x30;
    }
}

