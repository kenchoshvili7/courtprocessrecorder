﻿using System.Collections.Generic;
using AudioConfiguration;
using DeviceController;
using Un4seen.Bass;
using Un4seen.Bass.Misc;

namespace CourtRecorderCommon
{
    public class PlaybackChannel
    {
        public DeviceInfo CurrentDevice;
        public DeviceInfo Device = new DeviceInfo();
        public int[] EQ = new int[5];
        public int[] EQValues = new int[5];
        public int Mixer;
        public List<DSP_PeakLevelMeter> PeakLevelMeters = new List<DSP_PeakLevelMeter>();
        public Dictionary<AudioSourceChannels, int> PeakLevels = new Dictionary<AudioSourceChannels, int>();
        public float Pitch = 1f;
        private bool playback;
        public Dictionary<AudioSourceChannels, int> PlaybackBASSStreams = new Dictionary<AudioSourceChannels, int>();
        public RecordSet RecordSet;
        private int? volume;

        public PlaybackChannel()
        {
            this.Pitch = 1f;
        }

        public void ClearPeakLevelMeters()
        {
            foreach (DSP_PeakLevelMeter meter in this.PeakLevelMeters)
            {
                meter.Stop();
                meter.Dispose();
            }
            this.PeakLevelMeters.Clear();
            this.PeakLevels.Clear();
        }

        public bool GetMuted(AudioSourceChannels channel)
        {
            return AudioIO.SettingManager.ReadBoolean("PLAYBACK_MUTED_" + channel.ToString(), false);
        }

        public void SetMuted(AudioSourceChannels channel, bool muted)
        {
            int num = 0;
            if (this.PlaybackBASSStreams.TryGetValue(channel, out num))
            {
                Un4seen.Bass.Bass.BASS_ChannelSetAttribute(num, BASSAttribute.BASS_ATTRIB_VOL, muted ? 0f : (((float) this.volume.Value) / 100f));
            }
            AudioIO.SettingManager.WriteBoolean("PLAYBACK_MUTED_" + channel.ToString(), muted);
        }

        public bool CanPlayback
        {
            get
            {
                return (this.Device.DeviceNo != -1);
            }
        }

        public bool Playback
        {
            get
            {
                return this.playback;
            }
            set
            {
                this.playback = value;
                if (value)
                {
                    Apolo16Mixer mixer1 = AudioIO.Apolo16Mixer;
                    mixer1.Mute = (MixerChannels) ((byte) (((int) mixer1.Mute) & 0xbf));
                }
                else
                {
                    Apolo16Mixer mixer2 = AudioIO.Apolo16Mixer;
                    mixer2.Mute = (MixerChannels) ((byte) (mixer2.Mute | MixerChannels.Monitor));
                }
            }
        }

        public int Volume
        {
            get
            {
                if (!this.volume.HasValue)
                {
                    this.volume = new int?(AudioIO.SettingManager.ReadInteger("PLAYBACK_VOLUME", 100));
                }
                return this.volume.Value;
            }
            set
            {
                foreach (KeyValuePair<AudioSourceChannels, int> pair in this.PlaybackBASSStreams)
                {
                    if (pair.Value != 0)
                    {
                        Un4seen.Bass.Bass.BASS_ChannelSetAttribute(pair.Value, BASSAttribute.BASS_ATTRIB_VOL, ((float) value) / 100f);
                    }
                }
                this.volume = new int?(value);
                AudioIO.SettingManager.WriteInteger("PLAYBACK_VOLUME", this.volume.Value);
            }
        }
    }
}

