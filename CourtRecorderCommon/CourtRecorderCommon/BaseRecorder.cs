﻿using System.IO;
using DeviceController;

namespace CourtRecorderCommon
{
    public abstract class BaseRecorder
    {
        protected BaseRecorder()
        {
        }

        private string AddLeadingZeroes(int n)
        {
            string str = n.ToString();
            while (str.Length < 3)
            {
                str = "0" + str;
            }
            return str;
        }

        public virtual bool Dispose()
        {
            return true;
        }

        public string GenerateFileName()
        {
            string path = null;
            int n = 0;
            while (true)
            {
                path = AudioIO.workingPath + this.AddLeadingZeroes(n) + ".wav";
                if (!File.Exists(path))
                {
                    return Path.GetFileName(path);
                }
                n++;
            }
        }

        public virtual int GetChannelVolume(AudioSourceChannels audioSourceChannel)
        {
            return AudioIO.SettingManager.ReadInteger("RECORDING_VOLUME_" + audioSourceChannel.ToString(), 10);
        }

        public abstract long GetPosition();
        public abstract long Pause();
        public abstract void Resume();
        public virtual void SetChannelVolume(AudioSourceChannels audioSourceChannel, int value)
        {
            AudioIO.SettingManager.WriteInteger("RECORDING_VOLUME_" + audioSourceChannel.ToString(), value);
        }

        public virtual string Start()
        {
            return this.GenerateFileName();
        }

        public abstract long Stop();

        public abstract bool IsRecording { get; }
    }
}

