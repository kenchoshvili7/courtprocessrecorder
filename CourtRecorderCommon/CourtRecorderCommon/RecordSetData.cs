﻿using System;
using DeviceController;

namespace CourtRecorderCommon
{
    [Serializable]
    public class RecordSetData
    {
        public AudioSourceChannels Channel = AudioSourceChannels.None;
        public bool IsEncoded;
        public long Length;
        public long Position;
        public long SyncPosition;

        public RecordSetData(long position, AudioSourceChannels channel)
        {
            this.Position = position;
            this.Channel = channel;
        }
    }
}

