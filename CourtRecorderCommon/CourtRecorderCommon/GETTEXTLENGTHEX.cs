﻿using System.Runtime.InteropServices;

namespace CourtRecorderCommon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct GETTEXTLENGTHEX
    {
        public int uiFlags;
        public int uiCodePage;
    }
}

