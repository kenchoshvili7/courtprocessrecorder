﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows.Forms;
using System.Xml;

namespace CourtRecorderCommon
{
    public sealed class LocalSettingsManager
    {
        private string _fileName;
        private IsolatedStorageFile _isoStore;
        private static LocalSettingsManager _singleton;
        private XmlDocument _xml;
        private XmlDocument _xmlOriginal;

        private LocalSettingsManager(string applicationName)
        {
            try
            {
                this.InitializeConfiguration(applicationName);
            }
            catch
            {
            }
        }

        public static LocalSettingsManager GetLocalSettingsManager()
        {
            if (_singleton == null)
            {
                _singleton = new LocalSettingsManager(Application.ProductName);
            }
            return _singleton;
        }

        private void InitializeConfiguration(string applicationName)
        {
            this._fileName = applicationName + ".config";
            this._isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.Assembly | IsolatedStorageScope.User, (System.Type) null, (System.Type) null);
            string[] fileNames = this._isoStore.GetFileNames(this._fileName);
            foreach (string str in fileNames)
            {
                if (str == this._fileName)
                {
                    StreamReader txtReader = new StreamReader(new IsolatedStorageFileStream(this._fileName, FileMode.Open, this._isoStore));
                    this._xml = new XmlDocument();
                    this._xml.Load(txtReader);
                    this._xmlOriginal = new XmlDocument();
                    this._xmlOriginal.LoadXml(this._xml.OuterXml);
                    txtReader.Close();
                }
            }
        }

        public void Persist()
        {
            try
            {
                this.WriteBackConfiguration();
            }
            catch
            {
            }
            finally
            {
                _singleton = null;
            }
        }

        public string Read(string section)
        {
            return this.Read(section, string.Empty);
        }

        public string Read(string section, string defaultValue)
        {
            try
            {
                if (this._xml == null)
                {
                    return string.Empty;
                }
                XmlNode node = this._xml.SelectSingleNode("/configuration/" + section);
                if (node == null)
                {
                    return defaultValue;
                }
                return node.FirstChild.Value;
            }
            catch
            {
                return defaultValue;
            }
        }

        public bool ReadBoolean(string section)
        {
            return this.ReadBoolean(section, false);
        }

        public bool ReadBoolean(string section, bool defaultValue)
        {
            string str = this.Read(section);
            if (str.Length <= 0)
            {
                return defaultValue;
            }
            try
            {
                return bool.Parse(str);
            }
            catch
            {
                return defaultValue;
            }
        }

        public void ReadFormSettings(Form form)
        {
            this.ReadFormSettings(form.Name, form);
        }

        public void ReadFormSettings(string name, Form form)
        {
            string str = this.Read(name);
            if (str.Length > 0)
            {
                string[] strArray = str.Split(new char[] { Convert.ToChar(",") });
                form.Left = Convert.ToInt16(strArray[0]);
                form.Top = Convert.ToInt16(strArray[1]);
                int num = Convert.ToInt16(strArray[2]);
                if (num > 0)
                {
                    form.Width = num;
                }
                int num2 = Convert.ToInt16(strArray[3]);
                if (num2 > 0)
                {
                    form.Height = num2;
                }
            }
        }

        public int ReadInteger(string section)
        {
            return this.ReadInteger(section, 0);
        }

        public int ReadInteger(string section, int defaultValue)
        {
            string str = this.Read(section);
            if (str.Length <= 0)
            {
                return defaultValue;
            }
            try
            {
                return Convert.ToInt32(str);
            }
            catch
            {
                return defaultValue;
            }
        }

        public void Write(string section, string value)
        {
            try
            {
                if (this._xml == null)
                {
                    this._xml = new XmlDocument();
                    XmlNode node = this._xml.CreateElement("configuration");
                    this._xml.AppendChild(node);
                }
                XmlNode newChild = this._xml.SelectSingleNode("/configuration/" + section);
                if (newChild == null)
                {
                    newChild = this._xml.CreateElement(section);
                    this._xml.SelectSingleNode("/configuration").AppendChild(newChild);
                }
                newChild.InnerText = value;
            }
            catch
            {
            }
        }

        private void WriteBackConfiguration()
        {
            if ((this._xml != null) && ((this._xmlOriginal == null) || (this._xml.OuterXml != this._xmlOriginal.OuterXml)))
            {
                StreamWriter writer = null;
                try
                {
                    writer = new StreamWriter(new IsolatedStorageFileStream(this._fileName, FileMode.Create, this._isoStore));
                    this._xml.Save(writer);
                    writer.Flush();
                    writer.Close();
                    if (this._xmlOriginal == null)
                    {
                        this._xmlOriginal = new XmlDocument();
                    }
                    this._xmlOriginal.LoadXml(this._xml.OuterXml);
                }
                catch
                {
                }
                finally
                {
                    if (writer != null)
                    {
                        writer.Flush();
                        writer.Close();
                    }
                }
            }
        }

        public void WriteBoolean(string section, bool value)
        {
            this.Write(section, value.ToString());
        }

        public void WriteFormSettings(Form form)
        {
            this.WriteFormSettings(form.Name, form);
        }

        public void WriteFormSettings(string name, Form form)
        {
            this.Write(name + ".WindowState", ((int) form.WindowState).ToString());
            if (form.WindowState == FormWindowState.Normal)
            {
                string str = form.Left.ToString() + "," + form.Top.ToString() + "," + form.Width.ToString() + "," + form.Height.ToString();
                this.Write(name, str);
            }
        }

        public void WriteInteger(string section, int value)
        {
            this.Write(section, value.ToString());
        }
    }
}

