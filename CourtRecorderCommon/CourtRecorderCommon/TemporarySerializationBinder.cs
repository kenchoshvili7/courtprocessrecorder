﻿using System;
using System.Reflection;
using System.Runtime.Serialization;
using AudioConfiguration;

namespace CourtRecorderCommon
{
    public class TemporarySerializationBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            assemblyName = assemblyName.Replace("Court Processor Shared", "DeltaStudioCommon").Replace("Court Processor", "Court Process Recorder").Replace("DeltaStudioCommon", "CourtRecorderCommon");
            typeName = typeName.Replace("CourtProcessorShared", "DeltaStudioCommon").Replace("Court Processor Shared", "DeltaStudioCommon").Replace("CourtProcessor", "CourtProcessRecorder").Replace("Court Processor", "Court Process Recorder").Replace("DeltaStudioCommon", "CourtRecorderCommon");
            typeName = typeName.Replace("ChannelInfo", "Channel");
            if (typeName.Contains("ChannelInfo"))
            {
                return typeof(Channel);
            }
            return Type.GetType(Assembly.CreateQualifiedName(assemblyName, typeName), false);
        }
    }
}

