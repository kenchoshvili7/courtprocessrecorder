﻿using System;
using System.Collections.Generic;
using System.IO;
using DeviceController;
using Un4seen.Bass;
using Un4seen.Bass.Misc;
using Un4seen.BassAsio;

namespace CourtRecorderCommon
{
    public class MultiChannelRecorder : BaseRecorder
    {
        public Dictionary<AudioSourceChannels, int> AmplitudeLevel = new Dictionary<AudioSourceChannels, int>();
        private Dictionary<AudioSourceChannels, ACMEncoder> Encoders = new Dictionary<AudioSourceChannels, ACMEncoder>();
        private Dictionary<AudioSourceChannels, DSP_Gain> GainControllers = new Dictionary<AudioSourceChannels, DSP_Gain>();
        private Dictionary<AudioSourceChannels, BassAsioHandler> hASIOs = new Dictionary<AudioSourceChannels, BassAsioHandler>();
        private Dictionary<AudioSourceChannels, DSP_PeakLevelMeter> LevelMeters = new Dictionary<AudioSourceChannels, DSP_PeakLevelMeter>();
        private Dictionary<AudioSourceChannels, bool> Muted = new Dictionary<AudioSourceChannels, bool>();
        public double sampleRateRatio = 1.0;

        internal bool AddChannel(int deviceNo, AudioSourceChannels channelNo)
        {
            bool flag = false;
            int channel = 0;
            switch (channelNo)
            {
                case AudioSourceChannels.Channel1:
                    channel = 0;
                    break;

                case AudioSourceChannels.Channel2:
                    channel = 1;
                    break;

                case (AudioSourceChannels.Channel1 | AudioSourceChannels.Channel2):
                    return flag;

                case AudioSourceChannels.Channel3:
                    channel = 2;
                    break;

                case AudioSourceChannels.Channel4:
                    channel = 3;
                    break;

                case AudioSourceChannels.Channel5:
                    channel = 4;
                    break;

                default:
                    return flag;
            }
            flag = Un4seen.BassAsio.BassAsio.BASS_ASIO_ChannelGetInfo(true, channel) != null;
            if (flag)
            {
                this.hASIOs.Add(channelNo, new BassAsioHandler(true, deviceNo, channel, 1, BASSASIOFormat.BASS_ASIO_FORMAT_16BIT, (double)AudioIO.Configuration.Format.nSamplesPerSec));
                this.sampleRateRatio = this.hASIOs[channelNo].SampleRate / ((double)AudioIO.Configuration.Format.nSamplesPerSec);
                Console.Write(this.hASIOs[channelNo].SampleRate);
                Console.Write("   ");
                Console.Write(AudioIO.Configuration.Format.nSamplesPerSec);
                BASS_ASIO_INFO bass_asio_info = Un4seen.BassAsio.BassAsio.BASS_ASIO_GetInfo();
                //if (!this.hASIOs[channelNo].Start(bass_asio_info.bufmax)) todo replaced
                if (!this.hASIOs[channelNo].Start(bass_asio_info.bufmax, 2))
                {
                    throw new ApplicationException("Could not start multichannel Recorder: " + Un4seen.Bass.Bass.BASS_ErrorGetCode().ToString() + ", " + Un4seen.BassAsio.BassAsio.BASS_ASIO_ErrorGetCode().ToString());
                }
                this.AmplitudeLevel.Add(channelNo, 0);
                this.LevelMeters.Add(channelNo, new DSP_PeakLevelMeter(this.hASIOs[channelNo].InputChannel, 0));
                this.LevelMeters[channelNo].UpdateTime = 0.1f;
                this.LevelMeters[channelNo].Notification += new EventHandler(this.RecordingPeakLevelMeterNotification);
                this.GainControllers.Add(channelNo, new DSP_Gain(this.hASIOs[channelNo].InputChannel, 1));
                this.SetChannelVolume(channelNo, this.GetChannelVolume(channelNo));
                this.Encoders.Add(channelNo, new ACMEncoder(this.hASIOs[channelNo].InputChannel, AudioIO.Configuration.Format));
                this.Muted.Add(channelNo, this.GetChannelMuted(channelNo));
            }
            return flag;
        }

        public override bool Dispose()
        {
            foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
            {
                pair.Value.Stop();
                pair.Value.Dispose();
            }
            this.Encoders.Clear();
            foreach (KeyValuePair<AudioSourceChannels, DSP_PeakLevelMeter> pair2 in this.LevelMeters)
            {
                pair2.Value.Stop();
                pair2.Value.Dispose();
            }
            this.LevelMeters.Clear();
            foreach (KeyValuePair<AudioSourceChannels, DSP_Gain> pair3 in this.GainControllers)
            {
                pair3.Value.Stop();
                pair3.Value.Dispose();
            }
            this.GainControllers.Clear();
            foreach (KeyValuePair<AudioSourceChannels, BassAsioHandler> pair4 in this.hASIOs)
            {
                pair4.Value.Stop();
                pair4.Value.Dispose();
            }
            this.hASIOs.Clear();
            return base.Dispose();
        }

        public bool GetChannelMuted(AudioSourceChannels audioSourceChannel)
        {
            return AudioIO.SettingManager.ReadBoolean("RECORDING_MUTED_" + audioSourceChannel.ToString(), false);
        }

        public override long GetPosition()
        {
            if (this.Encoders.Count > 0)
            {
                foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
                {
                    return (long)((int)(((double)Un4seen.Bass.Bass.BASS_ChannelGetPosition(this.hASIOs[pair.Key].InputChannel, BASSMode.BASS_POS_BYTES)) / this.sampleRateRatio));
                }
            }
            return 0L;
        }

        public override long Pause()
        {
            throw new NotImplementedException();
        }

        private void RecordingPeakLevelMeterNotification(object sender, EventArgs e)
        {
            foreach (KeyValuePair<AudioSourceChannels, DSP_PeakLevelMeter> pair in this.LevelMeters)
            {
                if (pair.Value == sender)
                {
                    this.AmplitudeLevel[pair.Key] = (pair.Value.LevelL * 100) / 0x8000;
                    break;
                }
            }
        }

        public override void Resume()
        {
            throw new NotImplementedException();
        }

        public void SetChannelMuted(AudioSourceChannels audioSourceChannel, bool muted)
        {
            if (this.GainControllers.ContainsKey(audioSourceChannel))
            {
                if (muted)
                {
                    this.GainControllers[audioSourceChannel].Gain = 0.0;
                }
                else
                {
                    this.SetChannelVolume(audioSourceChannel, this.GetChannelVolume(audioSourceChannel));
                }
            }
            AudioIO.SettingManager.WriteBoolean("RECORDING_MUTED_" + audioSourceChannel.ToString(), muted);
        }

        public override void SetChannelVolume(AudioSourceChannels audioSourceChannel, int value)
        {
            if (this.GainControllers.ContainsKey(audioSourceChannel))
            {
                this.GainControllers[audioSourceChannel].Gain = (((float)value) / 100f) * 1.5f;
            }
            base.SetChannelVolume(audioSourceChannel, value);
        }

        public override string Start()
        {
            string str = null;
            foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
            {
                if (!pair.Value.Start(AudioIO.workingPath + base.Start()))
                {
                    foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair2 in this.Encoders)
                    {
                        if (!string.IsNullOrEmpty(pair2.Value.FileName))
                        {
                            pair2.Value.Stop();
                            File.Delete(pair2.Value.FileName);
                        }
                    }
                    return null;
                }
                if (string.IsNullOrEmpty(str))
                {
                    str = ((int)pair.Key).ToString() + "?" + Path.GetFileName(pair.Value.FileName);
                }
                else
                {
                    string str3 = str;
                    str = str3 + ":" + ((int)pair.Key).ToString() + "?" + Path.GetFileName(pair.Value.FileName);
                }
            }
            return str;
        }

        public override long Stop()
        {
            if (this.Encoders.Count > 0)
            {
                long num = -1L;
                foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
                {
                    if (num == -1L)
                    {
                        num = Un4seen.Bass.Bass.BASS_ChannelGetPosition(this.hASIOs[pair.Key].InputChannel, BASSMode.BASS_POS_BYTES);
                    }
                    pair.Value.Pause(true);
                    pair.Value.Stop();
                }
                return (long)((int)(((double)num) / this.sampleRateRatio));
            }
            return 0L;
        }

        public override bool IsRecording
        {
            get
            {
                foreach (KeyValuePair<AudioSourceChannels, ACMEncoder> pair in this.Encoders)
                {
                    return pair.Value.IsActive;
                }
                return false;
            }
        }
    }
}

