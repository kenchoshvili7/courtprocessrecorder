﻿using System.ComponentModel;
using System.Windows.Forms;

namespace CourtRecorderCommon
{
    public class BaseForm : Form
    {
        private IContainer components = null;

        public BaseForm()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(BaseForm));
            base.SuspendLayout();
            base.AutoScaleMode = AutoScaleMode.Inherit;
            manager.ApplyResources(this, "$this");
            base.Name = "BaseForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            base.ResumeLayout(false);
        }
    }
}

