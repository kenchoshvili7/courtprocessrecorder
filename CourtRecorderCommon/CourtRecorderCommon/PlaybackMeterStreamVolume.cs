﻿using DeviceController;

namespace CourtRecorderCommon
{
    public delegate void PlaybackMeterStreamVolume(AudioSourceChannels? channels, float maxSampleValues);
}