﻿using System;
using System.Runtime.InteropServices;
using AudioConfiguration;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Enc;

namespace CourtRecorderCommon
{
    public class ACMEncoder : IDisposable
    {
        private bool disposed;
        private string fileName;
        private int handle;
        private int hEncoder;
        private WaveFormatEx waveFormat;

        public ACMEncoder(int handle, WaveFormatEx waveFormatEx)
        {
            this.handle = handle;
            this.waveFormat = waveFormatEx;
        }

        [DllImport("bassenc.dll", CharSet = CharSet.Auto)]
        public static extern BASSActive BASS_Encode_IsActive(int handle);
        [DllImport("bassenc.dll", CharSet = CharSet.Auto)]
        public static extern bool BASS_Encode_SetPaused(int handle, [In, MarshalAs(UnmanagedType.Bool)] bool paused);
        [DllImport("bassenc.dll", CharSet = CharSet.Unicode)]
        public static extern int BASS_Encode_StartACMFile(int handle, IntPtr form, BASSEncode flags, [In, MarshalAs(UnmanagedType.LPWStr)] string file);
        [DllImport("bassenc.dll", CharSet = CharSet.Auto)]
        public static extern bool BASS_Encode_Stop(int handle);
        public virtual void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                try
                {
                    this.Stop();
                }
                catch
                {
                }
            }
            this.disposed = true;
        }

        ~ACMEncoder()
        {
            this.Dispose(false);
        }

        internal bool Pause(bool pause)
        {
            return ((this.hEncoder != 0) && BASS_Encode_SetPaused(this.hEncoder, pause));
        }

        public unsafe bool Start(string fileName)
        {
            this.fileName = fileName;
            fixed (byte* numRef = this.WaveFormatToByteArray())
            {
                //this.hEncoder = BASS_Encode_StartACMFile(this.handle, (IntPtr)numRef, BASSEncode.BASS_ENCODE_AUTOFREE | BASSEncode.BASS_UNICODE, fileName);
            }
            return true;
            //return (this.hEncoder != 0);
        }

        internal bool Stop()
        {
            if (this.hEncoder == 0)
            {
                return true;
            }
            if (BASS_Encode_Stop(this.hEncoder))
            {
                this.hEncoder = 0;
                return true;
            }
            return false;
        }

        private byte[] WaveFormatToByteArray()
        {
            int length = Marshal.SizeOf(this.waveFormat) - 4;
            byte[] destination = new byte[length + this.waveFormat.ExtraData.Length];
            IntPtr ptr = Marshal.AllocHGlobal((int)(length + 4));
            Marshal.StructureToPtr(this.waveFormat, ptr, false);
            Marshal.Copy(ptr, destination, 0, length);
            Marshal.FreeHGlobal(ptr);
            for (int i = 0; i < this.waveFormat.ExtraData.Length; i++)
            {
                destination[0x12 + i] = this.waveFormat.ExtraData[i];
            }
            return destination;
        }

        public string FileName
        {
            get
            {
                return this.fileName;
            }
        }

        public bool IsActive
        {
            get
            {
                if (this.hEncoder == 0)
                {
                    return false;
                }
                return (BASS_Encode_IsActive(this.hEncoder) != BASSActive.BASS_ACTIVE_STOPPED);
            }
        }
    }
}

