﻿namespace CourtRecorderCommon
{
    public enum DriveType
    {
        Unknown,
        NoRootDirectory,
        Removable,
        Fixed,
        Network,
        CDRom,
        Ram
    }
}

