﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Un4seen.Bass.Misc;

namespace CourtRecorderCommon
{
    [Serializable]
    public class RecordSet
    {
        public string FileName;
        [NonSerialized, XmlIgnore]
        public float? GainFactor;
        public bool IsEncoded;
        private List<RecordSetData> recordSetData;
        [NonSerialized, XmlIgnore]
        public List<WaveForm> UserData;

        public RecordSet(string fileName)
        {
            this.FileName = fileName;
        }

        public List<RecordSetData> RecordSetData
        {
            get
            {
                if (this.recordSetData == null)
                {
                    this.recordSetData = new List<RecordSetData>();
                }
                return this.recordSetData;
            }
        }
    }
}

