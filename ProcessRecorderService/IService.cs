﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ProcessRecorderService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
    [ServiceContract]

    public interface IService
    {

        [OperationContract]
        DateTime GetServerDate();

        [OperationContract]
        bool IsProsecurtorAddedOnCaseNo(string casoNO);

        [OperationContract]
        bool Insert(string courtName,string caseNO, string prosecutorID,string prosecutorName,DateTime startDate,DateTime endDate);
    }
}
