﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using CaseRandomDistributionService.HelperClasses;

namespace ProcessRecorderService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service.svc or Service.svc.cs at the Solution Explorer and start debugging.

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Service : IService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public DateTime GetServerDate()
        {
            return DateTime.Now;
        }

        public bool Insert(string courtName, string caseNO, string prosecutorID, string prosecutorName, DateTime startDate, DateTime endDate)
        {
            try
            {
                using (var db = new Db2Helper())
                {
                   
                    return db.insert(courtName, caseNO, prosecutorID, prosecutorName, startDate, endDate);
                }
            }
            catch (Exception e)
            {
                logger.Info("e.Message ---->" + e.Message + "e.InnerException---->" + e.InnerException);
                logger.Info(string.Format("caseNO:{0} prosecutorID:{1} prosecutorName:{2} startDate:{3} endDate:{4}", caseNO, prosecutorID, prosecutorName, startDate, endDate));
                return false;
            }
           




        }

        public bool IsProsecurtorAddedOnCaseNo(string casoNO)
        {
            using (var db = new Db2Helper())
            {
                return db.IsProsecurtorAddedOnCaseNo(casoNO);
            }

        }
    }
}
