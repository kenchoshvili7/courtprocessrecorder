﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using IBM.Data.DB2;

namespace CaseRandomDistributionService.HelperClasses
{

    public class Db2Helper : IDisposable
    {
        private readonly DB2Connection _con;
        public Db2Helper()
        {
            _con = new DB2Connection(ConfigurationManager.ConnectionStrings["CRDDb2Connection"].ConnectionString);
            _con.Open();
        }
        public bool insert(string courtName, string caseNO,string prosecutorID , string prosecutorName, DateTime? startDate, DateTime? endDate)
        {
            var command = new DB2Command("COURTSES.INSERT_SESSION_PROSECUTOR_ATTENDANCE", _con) { CommandType = CommandType.StoredProcedure };

            command.Parameters.Add("P_COURT_NAME ", DB2Type.VarChar).Value = courtName;
            command.Parameters.Add("P_CASE_NO ", DB2Type.VarChar).Value = caseNO;            
            command.Parameters.Add("P_PROSECUTOR_NAME ", DB2Type.VarChar).Value = prosecutorName;
            command.Parameters.Add("P_START_DATE", DB2Type.Timestamp).Value = startDate;
            command.Parameters.Add("P_END_DATE ", DB2Type.Timestamp).Value = endDate;
            command.Parameters.Add("P_PROSECUTOR_ID ", DB2Type.VarChar).Value = prosecutorID;





            command.Parameters.Add("r", DB2Type.SmallInt).Direction = ParameterDirection.Output;

            command.ExecuteNonQuery();

            var _isSucccess = Convert.ToInt32(command.Parameters["r"].Value) == 1;

            return _isSucccess;
        }


        public bool IsProsecurtorAddedOnCaseNo(string P_CASENO)
        {
            var command = new DB2Command("courtses.is_prosecutor_added_on_caseno", _con) { CommandType = CommandType.StoredProcedure };

            command.Parameters.Add("P_COURT_NAME ", DB2Type.VarChar).Value = P_CASENO;
            
            command.Parameters.Add("r", DB2Type.SmallInt).Direction = ParameterDirection.Output;

            command.ExecuteNonQuery();

            var _IsProsecurtorAddedOnCaseNo = Convert.ToInt32(command.Parameters["r"].Value) == 1;

            return _IsProsecurtorAddedOnCaseNo;
        }
        public void Dispose()
        {
            _con.Close();
            _con.Dispose();
        }
    }
}