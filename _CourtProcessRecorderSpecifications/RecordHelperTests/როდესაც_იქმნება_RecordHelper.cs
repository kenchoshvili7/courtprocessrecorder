﻿using System;
using System.IO;
using AudioConfiguration;
using Biggy.Data.Json;
using JsonDatabaseModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RecordProvider;
using Shouldly;

namespace _CourtProcessRecorderSpecifications.RecordHelperTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class როდესაც_იქმნება_RecordHelper_და_კონფიგურაცია_არის_null : TestBase
    {
        public override void Context()
        {
            _session = new Session();
        }

        private Session _session;

        [TestMethod]
        public void შეცდომა_უნდა_ამოაგდოს()
        {
            try
            {
                //new RecordHelper(_session);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("Recorder configuration may not be null!");
            }
        }
    }

    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class როდესაც_ხდება_StartRecord_ის_გამოძახება : TestBase
    {
        public override void Context()
        {
            var db = new JsonDbCore(Directory.GetCurrentDirectory() + @"\Database");
            db.TryDropTable("recorderconfigurations");
            db.TryDropTable("sessions");
            var store = db.CreateStoreFor<RecorderConfiguration>();
            //store.Add(new RecorderConfiguration { ChannelType = ChannelType.Accused, ArchiveDirectory = Directory.GetCurrentDirectory() + "\\AudioFiles", Name = "Conf" });

            _session = new Session();

            //recordHelper = new RecordHelper(_session);
        }

        private Session _session;
        private RecordHelper _recordHelper;

        [TestMethod]
        public void ბაზაში_უნდა_შეიქმნას_შესაბამისი_ჩანაწერი()
        {
            _recordHelper.StartRecord();
        }


    }
}
