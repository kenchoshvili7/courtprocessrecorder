﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _CourtProcessRecorderSpecifications
{
    [TestClass]
    public class TestBase
    {
        public TestBase()
        {
            Context();
        }

        [TestInitialize]
        public virtual void Context() { }
    }
}
