﻿namespace DriverInstaller
{
    partial class DriverInstallerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DriverInstallerForm));
            this.windowsVersionsComboBox = new System.Windows.Forms.ComboBox();
            this.installButton = new System.Windows.Forms.Button();
            this.installAutomaticallyCheckBox = new System.Windows.Forms.CheckBox();
            this.windowsVersionLabel = new System.Windows.Forms.Label();
            this.deviceComboBox = new System.Windows.Forms.ComboBox();
            this.deviceLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // windowsVersionsComboBox
            // 
            this.windowsVersionsComboBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.windowsVersionsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.windowsVersionsComboBox.Enabled = false;
            this.windowsVersionsComboBox.FormattingEnabled = true;
            this.windowsVersionsComboBox.Location = new System.Drawing.Point(-1, 115);
            this.windowsVersionsComboBox.Name = "windowsVersionsComboBox";
            this.windowsVersionsComboBox.Size = new System.Drawing.Size(292, 21);
            this.windowsVersionsComboBox.TabIndex = 0;
            // 
            // installButton
            // 
            this.installButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.installButton.Location = new System.Drawing.Point(108, 196);
            this.installButton.Name = "installButton";
            this.installButton.Size = new System.Drawing.Size(75, 23);
            this.installButton.TabIndex = 2;
            this.installButton.Text = "Install";
            this.installButton.UseVisualStyleBackColor = true;
            this.installButton.Click += new System.EventHandler(this.installButton_Click);
            // 
            // installAutomaticallyCheckBox
            // 
            this.installAutomaticallyCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.installAutomaticallyCheckBox.AutoSize = true;
            this.installAutomaticallyCheckBox.Checked = true;
            this.installAutomaticallyCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.installAutomaticallyCheckBox.Location = new System.Drawing.Point(89, 159);
            this.installAutomaticallyCheckBox.Name = "installAutomaticallyCheckBox";
            this.installAutomaticallyCheckBox.Size = new System.Drawing.Size(118, 17);
            this.installAutomaticallyCheckBox.TabIndex = 3;
            this.installAutomaticallyCheckBox.Text = "Install Automatically";
            this.installAutomaticallyCheckBox.UseVisualStyleBackColor = true;
            this.installAutomaticallyCheckBox.CheckedChanged += new System.EventHandler(this.installAutomaticallyCheckBox_CheckedChanged);
            // 
            // windowsVersionLabel
            // 
            this.windowsVersionLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.windowsVersionLabel.AutoSize = true;
            this.windowsVersionLabel.Location = new System.Drawing.Point(104, 89);
            this.windowsVersionLabel.Name = "windowsVersionLabel";
            this.windowsVersionLabel.Size = new System.Drawing.Size(89, 13);
            this.windowsVersionLabel.TabIndex = 4;
            this.windowsVersionLabel.Text = "Windows Version";
            // 
            // deviceComboBox
            // 
            this.deviceComboBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.deviceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.deviceComboBox.FormattingEnabled = true;
            this.deviceComboBox.Items.AddRange(new object[] {
            "Apolo",
            "Presonus"});
            this.deviceComboBox.Location = new System.Drawing.Point(-1, 43);
            this.deviceComboBox.Name = "deviceComboBox";
            this.deviceComboBox.Size = new System.Drawing.Size(292, 21);
            this.deviceComboBox.TabIndex = 0;
            this.deviceComboBox.SelectedIndexChanged += new System.EventHandler(this.deviceComboBox_SelectedIndexChanged);
            // 
            // deviceLabel
            // 
            this.deviceLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.deviceLabel.AutoSize = true;
            this.deviceLabel.Location = new System.Drawing.Point(128, 18);
            this.deviceLabel.Name = "deviceLabel";
            this.deviceLabel.Size = new System.Drawing.Size(41, 13);
            this.deviceLabel.TabIndex = 4;
            this.deviceLabel.Text = "Device";
            // 
            // DriverInstallerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 228);
            this.Controls.Add(this.deviceLabel);
            this.Controls.Add(this.windowsVersionLabel);
            this.Controls.Add(this.installAutomaticallyCheckBox);
            this.Controls.Add(this.installButton);
            this.Controls.Add(this.deviceComboBox);
            this.Controls.Add(this.windowsVersionsComboBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DriverInstallerForm";
            this.Text = "Driver Installer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox windowsVersionsComboBox;
        private System.Windows.Forms.Button installButton;
        private System.Windows.Forms.CheckBox installAutomaticallyCheckBox;
        private System.Windows.Forms.Label windowsVersionLabel;
        private System.Windows.Forms.ComboBox deviceComboBox;
        private System.Windows.Forms.Label deviceLabel;
    }
}

