﻿namespace DriverInstaller
{
    public class WindowsVersion
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public bool Is64Bit { get; set; }

        public string Description
        {
            get
            {
                var bit = Is64Bit ? "X64" : "X86";
                return Name + " " + bit;
            }
        }
    }
}