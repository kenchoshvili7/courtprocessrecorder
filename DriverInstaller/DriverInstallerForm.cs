﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DriverInstaller
{
    public partial class DriverInstallerForm : Form
    {
        private List<WindowsVersion> _windowsVersions;
        private WindowsVersion _currentWindowsVersion;

        public DriverInstallerForm()
        {
            InitializeComponent();

            windowsVersionsComboBox.SelectionChangeCommitted += WindowsVersionsComboBoxOnSelectionChangeCommitted;

            InitializeWindowsVersions();

            InitializeWindowsVersionComboBox();

            SelectCurrentWindowsVersion();

            deviceComboBox.SelectedIndex = 0;
        }

        private void WindowsVersionsComboBoxOnSelectionChangeCommitted(object sender, EventArgs e)
        {
            _currentWindowsVersion = windowsVersionsComboBox.SelectedItem as WindowsVersion;
        }

        private void installButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (deviceComboBox.SelectedIndex == 0)
                    DriverInstallerHelper.InstallDriver(_currentWindowsVersion);
                else
                {
                    DriverInstallerHelper.OpenPresonusInstaller();
                    Close();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(@"Error occured while installing driver, please install manually", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void installAutomaticallyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (installAutomaticallyCheckBox.Checked)
            {
                windowsVersionsComboBox.Enabled = false;
                SelectCurrentWindowsVersion();
            }
            else windowsVersionsComboBox.Enabled = true;
        }

        private void SelectCurrentWindowsVersion()
        {
            var bit = Environment.Is64BitOperatingSystem;
            var version = Environment.OSVersion.Version;
            var osVersion = version.Major + "." + version.Minor;

            _currentWindowsVersion = _windowsVersions.FirstOrDefault(w => w.Version.Contains(osVersion) && w.Is64Bit == bit);

            windowsVersionsComboBox.SelectedItem = _currentWindowsVersion;
        }

        private void InitializeWindowsVersionComboBox()
        {
            windowsVersionsComboBox.DataSource = _windowsVersions;
            windowsVersionsComboBox.DisplayMember = "Description";
        }

        private void InitializeWindowsVersions()
        {
            _windowsVersions = new List<WindowsVersion>
            {
                new WindowsVersion {Name = "Windows XP",Is64Bit = false, Version = "5.1"},
                new WindowsVersion {Name = "Windows XP",Is64Bit = true, Version = "5.1"},

                new WindowsVersion {Name = "Windows Vista",Is64Bit = false, Version = "6.0"},
                new WindowsVersion {Name = "Windows Vista",Is64Bit = true, Version = "6.0"},

                new WindowsVersion {Name = "Windows 7",Is64Bit = false, Version = "6.11"},
                new WindowsVersion {Name = "Windows 7",Is64Bit = true, Version = "6.1"},

                new WindowsVersion {Name = "Windows 8",Is64Bit = false, Version = "6.2"},
                new WindowsVersion {Name = "Windows 8",Is64Bit = true, Version = "6.2"},

                new WindowsVersion {Name = "Windows 8.1",Is64Bit = false, Version = "6.3"},
                new WindowsVersion {Name = "Windows 8.1",Is64Bit = true, Version = "6.3"},
            };

        }

        private void deviceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (deviceComboBox.SelectedIndex == 1)
            {
                installAutomaticallyCheckBox.Enabled = false;
                windowsVersionsComboBox.Enabled = false;
            }
            else
            {
                installAutomaticallyCheckBox.Enabled = true;
                windowsVersionsComboBox.Enabled = !installAutomaticallyCheckBox.Checked;
            }
        }
    }
}
