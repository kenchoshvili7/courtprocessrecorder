﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace DriverInstaller
{
    public class DriverInstallerHelper
    {
        private static bool _installed;

        public static void InstallDriver(WindowsVersion windowsVersion)
        {
            _installed = false;

            ProcessStartInfo psi;
            string arguments;

            var bit = windowsVersion.Is64Bit ? "X64" : "X86";
            var directory = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + "Drivers\\" + windowsVersion.Name + "\\" + bit;

            if (windowsVersion.Name.Contains("XP"))
            {
                psi = new ProcessStartInfo(directory + "\\" + "dpinst.exe");
                arguments = "/sw /lm";
            }
            else
            {
                psi = new ProcessStartInfo("PnPutil.exe");
                arguments = " -i -a " + "\"" + directory + "\\" + "cyusb3.inf" + "\"";
            }

            psi.UseShellExecute = false;
            psi.Arguments = arguments;
            psi.CreateNoWindow = true;

            using (var process = Process.Start(psi))
            {
                process.EnableRaisingEvents = true;
                process.Exited += ProcessOnExited;
                process.WaitForExit();
            }
        }

        private static void ProcessOnExited(object sender, EventArgs e)
        {
            if (_installed) return;

            MessageBox.Show(@"Installed successfully!");

            _installed = true;
        }

        public static void OpenPresonusInstaller()
        {
            var path = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Drivers", "PreSonus_Universal_Control.exe");
            Process.Start(path);
        }
    }
}
