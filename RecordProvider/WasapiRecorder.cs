﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using CourtRecorderCommon;
using DeviceController;
using JsonDatabaseModels;
using NAudio.CoreAudioApi;
using NAudio.Lame;
using NAudio.Wave;
using RecordProvider.HelperClasses;
using RecordProvider.Interfaces;
using WasapiCapture = NAudio.CoreAudioApi.WasapiCapture;

namespace RecordProvider
{
    public class WasapiRecorder : IRecorder
    {
        private readonly WaveFormat _format;
        //private VolumeSampleProvider _volumeProvider;
        private bool _isRecording;
        //private AudioSourceChannels _mixerChannelsAll;
        private string _sessionPath;
        private Dictionary<int, LameMP3FileWriter> _mp3FileWritersDictionary;
        //private Dictionary<int, BufferedWaveProvider> _bufferedWaveProviders;
        private string _audioFilePathForDuration;
        private readonly IWaveIn _waveIn;
        //private readonly WaveOut _waveOut;
        private readonly MMDevice _mmDevice;
        public event RecordAmplitudeAvialable RecordAmplitudeAvialable;
        private readonly Timer _amplitudeTimer;
        private bool _isTurnedOn = true;

        public WasapiRecorder()
        {
            _format = WaveFormat.CreateIeeeFloatWaveFormat(44100, 1);

            try
            {
                _mmDevice = new MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Capture, Role.Console);

                _waveIn = new WaveIn { WaveFormat = _format };

                //_waveIn = new WasapiCapture(_mmDevice)
                //{
                //    //WaveFormat = _format
                //};

                _format = _waveIn.WaveFormat;

                _waveIn.DataAvailable += WaveInOnDataAvailable;
            }
            catch { }
            
            //try { _mmDevice = new MMDeviceEnumerator().EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active)[_waveIn.DeviceNumber]; }
            //catch { }

            //_waveOut = new WaveOut();

            _amplitudeTimer = new Timer { Interval = 500, Enabled = true };
        }

        private void InitializeAudioFilesDictionary(Dictionary<AudioSourceChannels?, string> audioFiles)
        {
            //_mixerChannelsAll = AudioSourceChannels.Channel1 | AudioSourceChannels.Channel2 |
            //                    AudioSourceChannels.Channel3 | AudioSourceChannels.Channel4 |
            //                    AudioSourceChannels.Channel5 | AudioSourceChannels.Channel6 |
            //                    AudioSourceChannels.Channel7 | AudioSourceChannels.Channel8;

            _mp3FileWritersDictionary = new Dictionary<int, LameMP3FileWriter>();
            //_bufferedWaveProviders = new Dictionary<int, BufferedWaveProvider>();

            foreach (var file in audioFiles)
            {
                var channelNum = AudioSourceChannelHelper.AudioSourceChannelToNum(file.Key);

                _mp3FileWritersDictionary.Add(channelNum, new LameMP3FileWriter(_sessionPath + file.Value, _format, 16));
                //_bufferedWaveProviders.Add(channelNum, new BufferedWaveProvider(_format) { DiscardOnBufferOverflow = true });
            }

            //var mixingWaveProvider32 = new MixingWaveProvider32();
            //foreach (var buf in _bufferedWaveProviders) mixingWaveProvider32.AddInputStream(buf.Value);
            //_volumeProvider = new VolumeSampleProvider(mixingWaveProvider32.ToSampleProvider()) { Volume = 1f };
            //_waveOut.Init(new PanningSampleProvider(_volumeProvider) { Pan = 0.0f });
        }

        public void StartRecord(AudioFilesContainer audioFilesContainer, string sessionPath)
        {
            if (_isRecording) return;

            _sessionPath = sessionPath;

            _amplitudeTimer.Elapsed += AmplitudeTimerOnElapsed;

            InitializeAudioFilesDictionary(audioFilesContainer.AudioFiles.ToDictionary(c => c.Channel, p => p.Path));

            _audioFilePathForDuration = _sessionPath + audioFilesContainer.AudioFiles.FirstOrDefault()?.Path;

            //_waveOut.Play();

            _waveIn.StartRecording();

            var amp = _mmDevice.AudioMeterInformation.MasterPeakValue;
        }

        private void WaveInOnDataAvailable(object sender, WaveInEventArgs e)
        {
            //byte[] buffer = e.Buffer;

            //for (int index = 0; index < e.BytesRecorded; index += 2)
            //{
            //    short sample = (short)((buffer[index + 1] << 8) |
            //                           buffer[index]);
            //    float sample32 = sample / 32768f;

            //    var bytes = BitConverter.GetBytes(sample32);
            //    _mp3FileWritersDictionary[0].Write(bytes, 0, bytes.Length);

            //}

            _mp3FileWritersDictionary[0].Write(e.Buffer, 0, e.BytesRecorded);
            //_bufferedWaveProviders[0].AddSamples(e.Buffer, 0, e.BytesRecorded);
        }

        private void AmplitudeTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var mp3FileWriter in _mp3FileWritersDictionary)
            {
                var audioSourceChannel = AudioSourceChannelHelper.NumToAudioSourceChannel(mp3FileWriter.Key);

                var amp = _mmDevice.AudioMeterInformation.MasterPeakValue;

                RecordAmplitudeAvialable?.Invoke(amp, audioSourceChannel);
            }
        }

        public TimeSpan StopRecord()
        {
            _waveIn.StopRecording();

            _isRecording = false;

            _amplitudeTimer.Elapsed -= AmplitudeTimerOnElapsed;

            foreach (var mp3File in _mp3FileWritersDictionary)
                mp3File.Value?.Flush();

            TimeSpan duration;

            using (var audio = new AudioFileReader(_audioFilePathForDuration))
                duration = audio.TotalTime;

            return duration;
        }

        public void SetChannelVolume(AudioSourceChannels audioSourceChannel, int value)
        {
            if (_mmDevice == null) return;
            _mmDevice.AudioEndpointVolume.MasterVolumeLevelScalar = value / 100f;
        }


        public bool MuteChannel(AudioSourceChannels channels)
        {
            if (_isTurnedOn)
            {
                _mmDevice.AudioEndpointVolume.MasterVolumeLevelScalar = 0;
                _isTurnedOn = false;
            }
            else
            {
                _mmDevice.AudioEndpointVolume.MasterVolumeLevelScalar = 1;
                _isTurnedOn = true;
            }

            return _isTurnedOn;
        }

        public void SetMixerOutVolume(int value)
        {
            return;

            //not supported feature

            //var volume = 1f / 155f * (value - 100f);
            //if (_volumeProvider != null) _volumeProvider.Volume = volume;
        }
    }
}