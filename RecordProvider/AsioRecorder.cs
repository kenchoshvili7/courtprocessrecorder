﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Timers;
using CourtRecorderCommon;
using DeviceController;
using JsonDatabaseModels;
using NAudio.Lame;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using RecordProvider.HelperClasses;
using RecordProvider.Interfaces;
using Un4seen.Bass;
using Un4seen.BassAsio;

namespace RecordProvider
{
    public class AsioRecorder : IRecorder
    {
        private readonly WaveFormat _format;
        private VolumeSampleProvider _volumeProvider;

        readonly int _deviceId;
        private bool _isRecording;
        private AudioSourceChannels _mixerChannelsAll;
        private string _sessionPath;
        private Dictionary<int, LameMP3FileWriter> _mp3FileWritersDictionary;
        private Dictionary<int, BufferedWaveProvider> _bufferedWaveProviders;
        private string _audioFilePathForDuration;
        private AsioOut _asioOut;
        private MixingWaveProvider32 _mixingWaveProvider32;
        private ASIOPROC _asioProc;
        public event RecordAmplitudeAvialable RecordAmplitudeAvialable;
        private readonly Timer _amplitudeTimer;

        public AsioRecorder(int deviceId)
        {
            _deviceId = deviceId;
            _format = WaveFormat.CreateIeeeFloatWaveFormat(44100, 1);

            BassAsio.BASS_ASIO_Init(deviceId, BASSASIOInit.BASS_ASIO_THREAD);
            BassAsio.BASS_ASIO_SetRate(_format.SampleRate);

            _amplitudeTimer = new Timer { Interval = 500, Enabled = true };
        }

        private void InitializeAudioFilesDictionary(Dictionary<AudioSourceChannels?, string> audioFiles)
        {
            _asioOut = new AsioOut(_deviceId);
            _mixingWaveProvider32 = new MixingWaveProvider32();
            _asioProc = new ASIOPROC(AsioCallback);

            _mixerChannelsAll = AudioSourceChannels.Channel1 | AudioSourceChannels.Channel2 |
                                AudioSourceChannels.Channel3 | AudioSourceChannels.Channel4 |
                                AudioSourceChannels.Channel5 | AudioSourceChannels.Channel6 |
                                AudioSourceChannels.Channel7 | AudioSourceChannels.Channel8;

            _mp3FileWritersDictionary = new Dictionary<int, LameMP3FileWriter>();
            _bufferedWaveProviders = new Dictionary<int, BufferedWaveProvider>();

            foreach (var file in audioFiles)
            {
                var channelNum = AudioSourceChannelHelper.AudioSourceChannelToNum(file.Key);

                _mp3FileWritersDictionary.Add(channelNum, new LameMP3FileWriter(_sessionPath + file.Value, _format, 16));
                _bufferedWaveProviders.Add(channelNum, new BufferedWaveProvider(_format) { DiscardOnBufferOverflow = true });

                BassAsio.BASS_ASIO_ChannelEnable(true, channelNum, _asioProc, IntPtr.Zero);
                BassAsio.BASS_ASIO_ChannelSetFormat(true, channelNum, BASSASIOFormat.BASS_ASIO_FORMAT_FLOAT);
                BassAsio.BASS_ASIO_ChannelSetRate(true, channelNum, _format.SampleRate);
            }

            foreach (var buf in _bufferedWaveProviders)
            {
                _mixingWaveProvider32.AddInputStream(buf.Value);
            }

            _volumeProvider = new VolumeSampleProvider(_mixingWaveProvider32.ToSampleProvider());
            _volumeProvider.Volume = 1f;
            var panProvider = new PanningSampleProvider(_volumeProvider);
            panProvider.Pan = 0.0f; // pan 100% right
            _asioOut.Init(panProvider);

            //GC.KeepAlive(_mp3FileWritersDictionary);
        }

        public void StartRecord(AudioFilesContainer audioFilesContainer, string sessionPath)
        {
            if (_isRecording) return;

            _sessionPath = sessionPath;

            _amplitudeTimer.Elapsed += AmplitudeTimerOnElapsed;

            InitializeAudioFilesDictionary(audioFilesContainer.AudioFiles.ToDictionary(c => c.Channel, p => p.Path));

            _audioFilePathForDuration = _sessionPath + audioFilesContainer.AudioFiles.FirstOrDefault().Path;

            _isRecording = BassAsio.BASS_ASIO_Start(0, 8);

            _asioOut.Play();
        }

        private int AsioCallback(bool input, int channel, IntPtr buffer, int length, IntPtr user)
        {
            var data = new byte[length];
            Bass.BASS_ChannelGetData(user.ToInt32(), buffer, length);
            Marshal.Copy(buffer, data, 0, length);

            _bufferedWaveProviders[channel].AddSamples(data, 0, data.Length);

            _mp3FileWritersDictionary[channel].Write(data, 0, data.Length);

            return 0;
        }

        private void AmplitudeTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var mp3FileWriter in _mp3FileWritersDictionary)
            {
                var audioSourceChannel = AudioSourceChannelHelper.NumToAudioSourceChannel(mp3FileWriter.Key);
                var amplitude = BassAsio.BASS_ASIO_ChannelGetLevel(true, mp3FileWriter.Key);

                RecordAmplitudeAvialable?.Invoke(amplitude, audioSourceChannel);
            }
        }

        public TimeSpan StopRecord()
        {
            BassAsio.BASS_ASIO_Stop();
            _asioProc = null;
            _asioOut.Stop();
            _asioOut.Dispose();

            _isRecording = false;

            _amplitudeTimer.Elapsed -= AmplitudeTimerOnElapsed;

            foreach (var mp3File in _mp3FileWritersDictionary)
                mp3File.Value?.Flush();

            TimeSpan duration;

            using (var audio = new AudioFileReader(_audioFilePathForDuration))
                duration = audio.TotalTime;

            return duration;
        }

        public void SetChannelVolume(AudioSourceChannels audioSourceChannel, int value)
        {
            var channelNum = AudioSourceChannelHelper.AudioSourceChannelToNum(audioSourceChannel);
            BassAsio.BASS_ASIO_ChannelSetVolume(true, channelNum, value / 100f);
        }

        public bool MuteChannel(AudioSourceChannels channels)
        {
            var isTurnedOn = AsioProvider.ToggleMicrophone(channels);
            if (isTurnedOn)
                _mixerChannelsAll = _mixerChannelsAll ^ channels;
            else _mixerChannelsAll = _mixerChannelsAll | channels;

            return isTurnedOn;
        }

        public void SetMixerOutVolume(int value)
        {
            var volume = 1f / 155f * (value - 100f);

            if (_volumeProvider != null) _volumeProvider.Volume = volume;
        }
    }
}
