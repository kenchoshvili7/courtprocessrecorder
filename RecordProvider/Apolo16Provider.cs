﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using AudioConfiguration;
using CourtRecorderCommon;
using DeviceController;

namespace RecordProvider
{
    public class Apolo16Provider
    {
        public static void PowerOn()
        {
            AudioIO.Apolo16Mixer.PowerSave = false;
        }

        public static void PowerOff()
        {
            AudioIO.Apolo16Mixer.PowerSave = true;
        }

        public static bool ToggleMicrophone(AudioSourceChannels channel)
        {
            var isTurnedOn = AudioSourceChannelsNone[channel] == AudioSourceChannels.None;
            if (isTurnedOn)
            {
                AudioSourceChannelsNone[channel] = AudioSourceChannelsInitialized[channel];
                OnSelectChannelClicked(AudioSourceChannelsInitialized[channel]);
            }
            else
            {
                AudioSourceChannelsNone[channel] = AudioSourceChannels.None;
                OnSelectChannelClicked(AudioSourceChannels.None);
            }

            return isTurnedOn;
        }

        private static void OnSelectChannelClicked(AudioSourceChannels channels)
        {
            var none = AudioSourceChannels.None;
            if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
            {
                foreach (var channel in AudioSourceChannelsNone.Where(c => c.Value != AudioSourceChannels.None))
                    none = none | channel.Value;
            }

            if ((AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel) && AudioIO.Apolo16Mixer.Connected)
            {
                AudioIO.SelectedAudioChannels = none | channels;

                //if (this.ViewMode == Modes.Record)
                //{
                //    tag = (AudioSourceChannels)control.Tag;
                //    if (!control.Selected)
                //    {
                //        //AudioIO.SelectedAudioChannels = (AudioSourceChannels) ((byte) (none & ((byte) ~tag))); //todo uncomment this
                //    }
                //    else
                //    {
                //        AudioIO.SelectedAudioChannels = (AudioSourceChannels)((byte)(none | tag));
                //    }
                //}
            }
            //else
            //{
            //    tag = (AudioSourceChannels)control.Tag;
            //    if (!control.Selected)
            //    {
            //        AudioIO.SelectedAudioChannels = AudioSourceChannels.None;
            //    }
            //    else if (this.ViewMode == Modes.Record)
            //    {
            //        AudioIO.SelectedAudioChannels = tag;
            //    }
            //    else if (this.ViewMode == Modes.Playback)
            //    {
            //        AudioIO.SelectedAudioChannels = AudioSourceChannels.ChannelPC;
            //    }
            //}

        }

        public static void InitializeMixer()
        {
            PowerOn();
            AudioIO.Enabled = true;
            AudioIO.Initialize();
            AudioIO.CheckForDeviceCount();
            AudioIO.SetPlaybackVolume(100);
            Thread.Sleep(1000); // bad workaround but without this audio out from mixer(speaker) not working
            AudioIO.Apolo16Mixer.Mute = MixerChannels.None;
            IncreaseVolumeTo100();
            TurnOffAllMicrophones();
        }

        private static void IncreaseVolumeTo100()
        {
            new List<AudioSourceChannels>
            {
                AudioSourceChannels.Channel1,
                AudioSourceChannels.Channel2,
                AudioSourceChannels.Channel3,
                AudioSourceChannels.Channel4,
                AudioSourceChannels.Channel5,
                AudioSourceChannels.Channel6
            }.ForEach(c => AudioIO.Recorder.SetChannelVolume(c, 100));
        }

        public static void InitializeBass()
        {
            AudioIO.InitializeBass();
        }

        public static void Dispose()
        {
            AudioIO.Dispose();
        }

        public static void TurnOnAllMicrophones()
        {
            AudioIO.Apolo16Mixer.Mute = MixerChannels.None;
        }

        private static void TurnOffAllMicrophones()
        {
            AudioIO.Apolo16Mixer.Mute = MixerChannels.Microphone1 | MixerChannels.Microphone2
                 | MixerChannels.Microphone3 | MixerChannels.Microphone4
                 | MixerChannels.Microphone5 | MixerChannels.Microphone6;
        }

        //private static readonly Dictionary<int, AudioSourceChannels> AudioSourceChannelsNone = new Dictionary<int, AudioSourceChannels>
        //{
        //    {1, AudioSourceChannels.None },
        //    {2, AudioSourceChannels.None },
        //    {3, AudioSourceChannels.None },
        //    {4, AudioSourceChannels.None },
        //    {5, AudioSourceChannels.None },
        //    {6, AudioSourceChannels.None }
        //};

        //private static readonly Dictionary<int, AudioSourceChannels> AudioSourceChannelsInitialized = new Dictionary<int, AudioSourceChannels>
        //{
        //    {1, AudioSourceChannels.Channel1 },
        //    {2, AudioSourceChannels.Channel2 },
        //    {3, AudioSourceChannels.Channel3 },
        //    {4, AudioSourceChannels.Channel4 },
        //    {5, AudioSourceChannels.Channel5 },
        //    {6, AudioSourceChannels.Channel6 }
        //};

        private static readonly Dictionary<AudioSourceChannels, AudioSourceChannels> AudioSourceChannelsNone = new Dictionary<AudioSourceChannels, AudioSourceChannels>
        {
            {AudioSourceChannels.Channel1, AudioSourceChannels.Channel1 },
            {AudioSourceChannels.Channel2, AudioSourceChannels.Channel2 },
            {AudioSourceChannels.Channel3, AudioSourceChannels.Channel3 },
            {AudioSourceChannels.Channel4, AudioSourceChannels.Channel4 },
            {AudioSourceChannels.Channel5, AudioSourceChannels.Channel5 },
            {AudioSourceChannels.Channel6, AudioSourceChannels.Channel6 }
        };

        private static readonly Dictionary<AudioSourceChannels, AudioSourceChannels> AudioSourceChannelsInitialized = new Dictionary<AudioSourceChannels, AudioSourceChannels>
        {
            {AudioSourceChannels.Channel1, AudioSourceChannels.Channel1 },
            {AudioSourceChannels.Channel2, AudioSourceChannels.Channel2 },
            {AudioSourceChannels.Channel3, AudioSourceChannels.Channel3 },
            {AudioSourceChannels.Channel4, AudioSourceChannels.Channel4 },
            {AudioSourceChannels.Channel5, AudioSourceChannels.Channel5 },
            {AudioSourceChannels.Channel6, AudioSourceChannels.Channel6 }
        };
    }
}
