﻿using JsonDatabaseModels;

namespace RecordProvider
{
    public delegate void PlaybackChunkChangedEvent(AudioFilesContainer audioFilesContainer);
}
