﻿using System;
using CourtRecorderCommon;
using DeviceController;
using JsonDatabaseModels;

namespace RecordProvider.Interfaces
{
    public interface IRecorder
    {
        event RecordAmplitudeAvialable RecordAmplitudeAvialable;

        void StartRecord(AudioFilesContainer audioFilesContainer, string sessionPath);

        TimeSpan StopRecord();

        void SetChannelVolume(AudioSourceChannels audioSourceChannel, int value);

        bool MuteChannel(AudioSourceChannels channels);

        void SetMixerOutVolume(int value);
    }
}
