﻿using System;
using DeviceController;

namespace RecordProvider.Interfaces
{
    public interface IPlayer
    {
        void Play();

        void PlayInSequence();

        void PauseResume();

        void Stop();

        void Seek(TimeSpan timeSpan);

        void SetVolume(int value);

        void Dispose();

        bool MuteChannel(AudioSourceChannels audioSourceChannels);

        PlayerStatus PlayerStatus { get; set; }

        event PlaybackChunkChangedEvent PlaybackChunkChangedEvent;
        event PlaybackStopped PlaybackStopped;

        TimeSpan GetPosition();

        DateTime GetCurrentDateTime();
    }
}
