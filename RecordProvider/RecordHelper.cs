﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CourtRecorderCommon;
using DeviceController;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.Wave;
using RecordProvider.HelperClasses;
using RecordProvider.Interfaces;

namespace RecordProvider
{
    public class RecordHelper
    {
        private readonly IRecorder _recorder;
        private Session _session;
        private string _sessionPath;
        //private string _sessionDirPath;
        //private readonly DatabaseHelper _databaseHelper;
        //private SessionDbHelper _sessionDbHelper;
        private readonly RecorderConfiguration _recorderConfiguration;
        private AudioFilesContainer _audioFilesContainer;
        private TimeSpan _fromTime;
        public event RecordAmplitudeAvialable RecordAmplitudeAvialable;
        public event Apolo16MixerReseted Apolo16MixerReseted;
        private DateTime? _lastStartTime;

        public RecordHelper()
        {
            _recorderConfiguration = ConfigurationHelper.GetConfiguration();
            if (_recorderConfiguration == null)
                throw new InvalidOperationException("Recorder configuration may not be null!");
            var directory = _recorderConfiguration.ArchiveDirectory;

            if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);

            //_databaseHelper = DatabaseHelper.DatabaseHelperInstance;

            switch (_recorderConfiguration.DeviceNo)
            {
                case 0x7ffffff1:
                    _recorder = new NaudioRecorder();
                    break;
                case -1:
                    _recorder = new WasapiRecorder();
                    break;
                default:
                    _recorder = new AsioRecorder(_recorderConfiguration.DeviceNo); // todo chnage deviceno here
                    break;
            }

            AudioIO.Apolo16Mixer.Apolo16MixerReseted += Apolo16MixerOnApolo16MixerReseted;
        }

        private void Apolo16MixerOnApolo16MixerReseted(bool flag)
        {
            Apolo16MixerReseted?.Invoke(flag);
        }

        public void CreateSession(Session session)
        {
            _session = session;
            _sessionPath = SessionDbWrapper.SessionDbHelper.SessionPath;

            //_sessionDirPath = GetSessionPath();
            //if (!Directory.Exists(_sessionDirPath)) Directory.CreateDirectory(_sessionDirPath);

            _session.AudioFilesContainers = new List<AudioFilesContainer>();
            //_session.Path = _sessionDirPath;
            _session.Channels = _recorderConfiguration.Channels;

            //_databaseHelper.AddSession(_session);
            SessionDbWrapper.SessionDbHelper.UpdateSession(_session);
            //SessionDbWrapper.Initialize(session.Path);
        }

        public void OpenSession(Session session)
        {
            _sessionPath = SessionDbWrapper.SessionDbHelper.SessionPath;

            _session = SessionDbWrapper.SessionDbHelper.GetSession();

            var audioFilesContainer = _session.AudioFilesContainers.OrderByDescending(o => o.StartTime).FirstOrDefault();
            if (audioFilesContainer != null)
                _fromTime = audioFilesContainer.To;
        }

        public void StartRecord()
        {
            ReloadSession();

            var path = GenerateAudioFilesContainerPath();
            _audioFilesContainer = new AudioFilesContainer
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                StartTime = DateTime.Now,
                From = _fromTime,
                Path = path,
                AudioFiles = GenerateAudioFiles(path)
            };

            _session.AudioFilesContainers.Add(_audioFilesContainer);
            SessionDbWrapper.SessionDbHelper.UpdateSession(_session);

            _recorder.StartRecord(_audioFilesContainer, _sessionPath);

            _recorder.RecordAmplitudeAvialable += RecorderOnRecordAmplitudeAvialable;

            _lastStartTime = SessionDbWrapper.SessionDbHelper.ReloadSession(_session)
                .AudioFilesContainers.OrderByDescending(o => o.StartTime)
                .FirstOrDefault()?.StartTime;
        }

        public void StopRecord()
        {
            ReloadSession();

            var audioFilesContainer = _session.AudioFilesContainers.FirstOrDefault(a => { return _audioFilesContainer != null && a.UId == _audioFilesContainer.UId; });

            var duration = _recorder.StopRecord();
            _fromTime += duration;
            audioFilesContainer.EndTime = audioFilesContainer.StartTime + duration;
            audioFilesContainer.To = _fromTime;
            audioFilesContainer.Duration = duration;
            _session.AudioFilesDuration += duration;

            SessionDbWrapper.SessionDbHelper.UpdateSession(_session);

            _recorder.RecordAmplitudeAvialable -= RecorderOnRecordAmplitudeAvialable;
        }

        public void SetChannelVolume(AudioSourceChannels audioSourceChannel, int value)
        {
            _recorder.SetChannelVolume(audioSourceChannel, value);
        }

        public bool SetMuteChannel(AudioSourceChannels channels)
        {
            return _recorder.MuteChannel(channels);
        }

        public void SetMixerOutVolume(int value)
        {
            _recorder.SetMixerOutVolume(value);
        }

        private void RecorderOnRecordAmplitudeAvialable(float amplitude, AudioSourceChannels channels)
        {
            RecordAmplitudeAvialable?.Invoke(amplitude, channels);
        }

        public DateTime GetLastEndTime()
        {
            if (_lastStartTime == null) return DateTime.Now;
            return (DateTime)_lastStartTime;

            //var endTime = DateTime.Now;
            //var lastSession =
            //    _databaseHelper.ReloadSession(_session)
            //        .AudioFilesContainers.OrderByDescending(o => o.StartTime)
            //        .FirstOrDefault();

            //if (lastSession == null) return endTime;

            //endTime = (DateTime)lastSession.StartTime;


            //if (lastSession.EndTime == null)
            //    endTime = (DateTime)lastSession.StartTime;

            //else endTime = (DateTime)lastSession.EndTime;

            //return endTime;
        }

        private void ReloadSession()
        {
            _session = SessionDbWrapper.SessionDbHelper.ReloadSession(_session);
        }

        private string GenerateAudioFilesContainerPath()
        {
            var path = "\\AudioFiles" + "\\" + PathHelper.GetDateTimeToPath();

            var sessionDirPath = SessionDbWrapper.SessionDbHelper.SessionPath;

            if (!Directory.Exists(path)) Directory.CreateDirectory(sessionDirPath + path);

            return path;
        }

        private List<AudioFile> GenerateAudioFiles(string path)
        {
            var audioFiles = new List<AudioFile>();
            foreach (var channel in _session.Channels)
            {
                //audioFiles.Add(new AudioFile { Channel = channel.ChannelNo, Path = path + @"\" + channel.ChannelNo + ".mp3" });
                audioFiles.Add(new AudioFile { Channel = channel.ChannelNo, Path = path + @"\" + channel.Name + ".mp3" });
            }

            //audioFiles.AddRange(new List<AudioFile>
            //{
            //    new AudioFile {Channel = AudioSourceChannels.Channel1, Path =path +@"\Channel1.mp3"},
            //    new AudioFile {Channel = AudioSourceChannels.Channel2, Path =path +@"\Channel2.mp3"},
            //    new AudioFile {Channel = AudioSourceChannels.Channel3, Path =path +@"\Channel3.mp3"},
            //    new AudioFile {Channel = AudioSourceChannels.Channel4, Path =path +@"\Channel4.mp3"},
            //    new AudioFile {Channel = AudioSourceChannels.Channel5, Path =path +@"\Channel5.mp3"},
            //    new AudioFile {Channel = AudioSourceChannels.Channel6, Path =path +@"\Channel6.mp3"}
            //});

            return audioFiles;
        }
    }
}
