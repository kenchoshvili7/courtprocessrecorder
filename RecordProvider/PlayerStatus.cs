﻿namespace RecordProvider
{
    public enum PlayerStatus
    {
        Stopped,
        Playing,
        Paused
    }
}