﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeviceController;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.Wave;
using RecordProvider.HelperClasses;
using RecordProvider.Interfaces;

namespace RecordProvider
{
    public class NaudioPlayer : IPlayer
    {
        private readonly WaveOutEvent _outputDevice;
        private readonly AudioPlaybackEngine _audioPlaybackEngine;
        private int _currentChunkElement;
        private readonly int _chunkCount;
        private readonly Session _session;
        private readonly string _sessionPath;
        private readonly List<AudioFilesContainer> _audioFilesContainers;
        private Dictionary<AudioSourceChannels?, AudioFileReader> _audioFileReaders;
        private AudioFilesContainer _currentAudioFilesContainer;
        private TimeSpan _position;
        //private readonly StopWatchWithOffset _stopwatch;
        private readonly CustomTimer _customTimer;
        private Dictionary<AudioSourceChannels?, float> _audioFilesVolumes;
        private Dictionary<AudioSourceChannels?, bool> _audioFilesMuteVolumes;

        public PlayerStatus PlayerStatus { get; set; }
        public event PlaybackChunkChangedEvent PlaybackChunkChangedEvent;
        public event PlaybackStopped PlaybackStopped;

        //private readonly BufferedWaveProvider _buferedWaveProvider;

        public NaudioPlayer(Session session, string sessionPath, int deviceNumber, PlaybackSpeed playbackSpeed,
            Dictionary<AudioSourceChannels?, bool> audioFilesMuteVolumes = null)
        {
            _audioPlaybackEngine = new AudioPlaybackEngine(deviceNumber, playbackSpeed.SampleRate, 2);
            _outputDevice = _audioPlaybackEngine.OutputDevice;
            _outputDevice.Volume = 1;

            _session = session;
            _sessionPath = sessionPath;

            _audioFilesContainers = session.AudioFilesContainers.OrderBy(a => a.StartTime).ToList();
            _currentChunkElement = 0;
            _chunkCount = _audioFilesContainers.Count;

            InitializeAudioFilesVolumes(1);

            if (audioFilesMuteVolumes == null) InitializeAudioFilesMuteVolumes();
            else _audioFilesMuteVolumes = audioFilesMuteVolumes;

            InitializeAudioFiles();

            //_stopwatch = new StopWatchWithOffset(TimeSpan.Zero);
            _customTimer = new CustomTimer(playbackSpeed.Interval);

            // provides ability to play online sound in computer speakers or headphones, may be not needed

            //AudioIO.Apolo16Mixer.DataReceived += Apolo16MixerOnDataReceived;

            //var waveOut = new WaveOut();
            //_buferedWaveProvider = new BufferedWaveProvider(new WaveFormat(16000, 1));
            //waveOut.Init(_buferedWaveProvider);
            //waveOut.PlaybackStopped += WaveOutOnPlaybackStopped;
            //waveOut.Play();
        }

        //private void WaveOutOnPlaybackStopped(object sender, StoppedEventArgs e)
        //{
        //}

        //private void Apolo16MixerOnDataReceived(object sender, short[][] data, int length, int[] peaks)
        //{
        //    //var result = new byte[data[0].Length * sizeof(short)];
        //    //Buffer.BlockCopy(data[0], 0, result, 0, result.Length);

        //    //_buferedWaveProvider.AddSamples(result, 0, result.Length);
        //    // _waveOut.Play();
        //}

        public void Play()
        {
            _outputDevice.Play();
            PlayerStatus = PlayerStatus.Playing;
        }

        private void InitializeAudioFiles()
        {
            _audioFileReaders = new Dictionary<AudioSourceChannels?, AudioFileReader>();

            _currentAudioFilesContainer = _audioFilesContainers[_currentChunkElement];
            if (PlaybackChunkChangedEvent != null) PlaybackChunkChangedEvent.Invoke(_currentAudioFilesContainer);

            _audioPlaybackEngine.Mixer.RemoveAllMixerInputs();

            #region old code parralel loading audio files

            //Parallel.ForEach(_currentAudioFilesContainer.AudioFiles, audioFile =>
            //{
            //    try
            //    {
            //        var audioFileReader = _audioPlaybackEngine.PlaySoundCustom(_session.Path + audioFile.Path,
            //            audioFile.Channel);

            //        if (_audioFilesMuteVolumes[audioFile.Channel])
            //            audioFileReader.Volume = 0;
            //        else audioFileReader.Volume = _audioFilesVolumes[audioFile.Channel];

            //        _audioFileReaders.TryAdd(audioFile.Channel, audioFileReader);
            //    }
            //    catch (Exception ex)
            //    {
            //        try
            //        {
            //            ExceptionLogHelper.AddExceptionLog(ex);
            //        }|
            //        catch { }
            //    }
            //});

            #endregion

            foreach (var audioFile in _currentAudioFilesContainer.AudioFiles)
            {
                try
                {
                    var audioFileReader = _audioPlaybackEngine.PlaySoundCustom(_sessionPath + audioFile.Path,
                        audioFile.Channel);

                    if (_audioFilesMuteVolumes[audioFile.Channel])
                        audioFileReader.Volume = 0;
                    else audioFileReader.Volume = _audioFilesVolumes[audioFile.Channel];

                    _audioFileReaders.Add(audioFile.Channel, audioFileReader);
                }
                catch (Exception ex)
                {
                    try
                    {
                        ExceptionLogHelper.AddExceptionLog(ex);
                    }
                    catch { }
                }
            }

            _outputDevice.PlaybackStopped -= OutputDeviceOnPlaybackStopped;
            _outputDevice.PlaybackStopped += OutputDeviceOnPlaybackStopped;
        }

        public void PlayInSequence()
        {
            _currentChunkElement = 0;
            InitializeAudioFiles();
            Play();

            _customTimer.Start();
        }

        public void PauseResume()
        {
            if (_outputDevice == null) return;
            if (_outputDevice.PlaybackState == PlaybackState.Playing)
            {
                _outputDevice.Pause();
                PlayerStatus = PlayerStatus.Paused;
                _customTimer.Stop();
            }
            else
            {
                _outputDevice.Play();
                PlayerStatus = PlayerStatus.Playing;
                _customTimer.Start();
            }
        }

        public void Stop()
        {
            PlayerStatus = PlayerStatus.Stopped;
            _customTimer.Reset();
            _audioPlaybackEngine.Mixer.RemoveAllMixerInputs();
            _currentAudioFilesContainer = _audioFilesContainers[0];

            if (_outputDevice == null) return;
            _outputDevice.PlaybackStopped -= OutputDeviceOnPlaybackStopped;
            _outputDevice.Stop();
        }

        private void OutputDeviceOnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            if (PlayerStatus == PlayerStatus.Stopped) return;

            _currentChunkElement++;

            if (_currentChunkElement >= _chunkCount)
            {
                Stop();
                if (PlaybackChunkChangedEvent != null) PlaybackChunkChangedEvent.Invoke(_audioFilesContainers[0]);
                if (PlaybackStopped != null) PlaybackStopped.Invoke();

                return;
            }

            InitializeAudioFiles();
            Play();
        }

        public void Seek(TimeSpan timeSpan)
        {
            var previousPlayerStatus = PlayerStatus;
            var previousChunkElement = _currentChunkElement;

            var currentAudioFilesContainer = _audioFilesContainers.FirstOrDefault(a => a.From <= timeSpan && a.To > timeSpan);
            _currentChunkElement = _audioFilesContainers.IndexOf(currentAudioFilesContainer);

            if (previousChunkElement == _currentChunkElement && _audioPlaybackEngine.Mixer.MixerInputs.Any())
            {
                PlayerStatus = PlayerStatus.Stopped;
                _customTimer.Reset();

                if (_outputDevice == null) return;
                _outputDevice.PlaybackStopped -= OutputDeviceOnPlaybackStopped;
                _outputDevice.Pause();
            }
            else
                Stop();

            if (_currentChunkElement < 0) return;

            var currentAudioFilesContainerDuration = new TimeSpan();
            if (currentAudioFilesContainer != null && currentAudioFilesContainer.Duration != null)
                currentAudioFilesContainerDuration = currentAudioFilesContainer.Duration.Value;

            var durationSumBefore =
                _audioFilesContainers.Where(a => a.From <= timeSpan)
                    .Where(f => f.Duration != null)
                    .SumTimeSpan(t => t.Duration.Value) - currentAudioFilesContainerDuration;

            if (previousChunkElement == _currentChunkElement && _audioPlaybackEngine.Mixer.MixerInputs.Any())
            {
                _outputDevice.PlaybackStopped -= OutputDeviceOnPlaybackStopped;
                _outputDevice.PlaybackStopped += OutputDeviceOnPlaybackStopped;
            }
            else
                InitializeAudioFiles();

            _audioFileReaders.Values.ToList().ForEach(a => a.SetPosition(timeSpan - durationSumBefore));

            _customTimer.ElapsedTimeSpan = timeSpan;

            if (previousPlayerStatus == PlayerStatus.Playing)
            {
                _customTimer.Start();
                Play();
            }
            else
            {
                PlayerStatus = PlayerStatus.Paused;
            }
        }

        public void SetVolume(int value)
        {
            float volume = value / 100f;
#pragma warning disable 612
            if (_outputDevice != null) _outputDevice.Volume = volume;
#pragma warning restore 612

            InitializeAudioFilesVolumes(volume);
        }

        public bool MuteChannel(AudioSourceChannels audioSourceChannels)
        {
            if (_audioFileReaders[audioSourceChannels].Volume == 0)
            {
                _audioFileReaders[audioSourceChannels].Volume = _audioFilesVolumes[audioSourceChannels];
                _audioFilesMuteVolumes[audioSourceChannels] = false;

                return false;
            }

            _audioFileReaders[audioSourceChannels].Volume = 0;
            _audioFilesMuteVolumes[audioSourceChannels] = true;

            //_audioFileReaders.Values.ToList().ForEach(a => { a.Volume = 1; });

            return true;
        }

        public TimeSpan GetPosition()
        {
            //var elapsedTime = TimeSpan.FromTicks((long)(_stopwatch.ElapsedTimeSpan.Ticks * 2));
            //return elapsedTime;

            return _customTimer.ElapsedTimeSpan;


            //if (_outputDevice.PlaybackState == PlaybackState.Stopped || _currentChunkElement >= _chunkCount)
            //    return TimeSpan.Zero;

            //var audioFileReader = _audioFileReaders.FirstOrDefault();
            //if (audioFileReader == null || _currentAudioFilesContainer == null) return TimeSpan.Zero;

            //var audioFileReaderCurrentTime = TimeSpan.Zero;
            //try
            //{
            //    audioFileReaderCurrentTime = audioFileReader.CurrentTime;
            //}
            //catch
            //{
            //    // ignored
            //}

            //var currentAudioFilesContainerDuration = new TimeSpan();
            //if (_currentAudioFilesContainer.Duration != null)
            //    currentAudioFilesContainerDuration = _currentAudioFilesContainer.Duration.Value;

            //var durationSumBefore = _audioFilesContainers.Where(a => a.From <= _currentAudioFilesContainer.From).Where(f => f.Duration != null).SumTimeSpan(t => t.Duration.Value) - currentAudioFilesContainerDuration;

            //_position = durationSumBefore + audioFileReaderCurrentTime;

            //return _position;
        }

        public DateTime GetCurrentDateTime()
        {
            return (DateTime)(_currentAudioFilesContainer.StartTime + GetPosition() - _currentAudioFilesContainer.From);
        }

        private void InitializeAudioFilesVolumes(float value)
        {
            _audioFilesVolumes = new Dictionary<AudioSourceChannels?, float>();
            foreach (var channel in _session.Channels)
                _audioFilesVolumes.Add(channel.ChannelNo, value);
        }

        private void InitializeAudioFilesMuteVolumes()
        {
            _audioFilesMuteVolumes = new Dictionary<AudioSourceChannels?, bool>();
            foreach (var channel in _session.Channels)
                _audioFilesMuteVolumes.Add(channel.ChannelNo, false);
        }

        public void Dispose()
        {
            try
            {
                if (_audioFileReaders != null) _audioFileReaders.Values.ToList().ForEach(a =>
                {
                    if (a != null) a.Dispose();
                });
            }
            catch { } // todo why exception  here

            Stop();
            _outputDevice.Dispose();
            _audioPlaybackEngine.Mixer.RemoveAllMixerInputs();
            _audioPlaybackEngine.Dispose();
        }
    }
}
