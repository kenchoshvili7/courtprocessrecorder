﻿using System;
using System.Collections.Generic;
using AudioConfiguration;
using DeviceController;
using RecordProvider.HelperClasses;
using Un4seen.Bass;
using Un4seen.BassAsio;

namespace RecordProvider
{
    public class AsioProvider
    {
        public static void InitializeMixer()
        {
            BassRegistration.Register();
            Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
        }

        public static bool ToggleMicrophone(AudioSourceChannels channel)
        {
            var isTurnedOn = AudioSourceChannelsNone[channel] == AudioSourceChannels.None;
            if (isTurnedOn)
            {
                AudioSourceChannelsNone[channel] = AudioSourceChannelsInitialized[channel];
                BassAsio.BASS_ASIO_ChannelSetVolume(true, AudioSourceChannelHelper.AudioSourceChannelToNum(channel), 1f);
            }
            else
            {
                AudioSourceChannelsNone[channel] = AudioSourceChannels.None;
                BassAsio.BASS_ASIO_ChannelSetVolume(true, AudioSourceChannelHelper.AudioSourceChannelToNum(channel), 0f);
            }

            return isTurnedOn;
        }

        public static void Dispose() => BassAsio.BASS_ASIO_Free();

        private static readonly Dictionary<AudioSourceChannels, AudioSourceChannels> AudioSourceChannelsNone = new Dictionary<AudioSourceChannels, AudioSourceChannels>
        {
            {AudioSourceChannels.Channel1, AudioSourceChannels.Channel1 },
            {AudioSourceChannels.Channel2, AudioSourceChannels.Channel2 },
            {AudioSourceChannels.Channel3, AudioSourceChannels.Channel3 },
            {AudioSourceChannels.Channel4, AudioSourceChannels.Channel4 },
            {AudioSourceChannels.Channel5, AudioSourceChannels.Channel5 },
            {AudioSourceChannels.Channel6, AudioSourceChannels.Channel6 },
            {AudioSourceChannels.Channel7, AudioSourceChannels.Channel7 },
            {AudioSourceChannels.Channel8, AudioSourceChannels.Channel8 }
        };

        private static readonly Dictionary<AudioSourceChannels, AudioSourceChannels> AudioSourceChannelsInitialized = new Dictionary<AudioSourceChannels, AudioSourceChannels>
        {
            {AudioSourceChannels.Channel1, AudioSourceChannels.Channel1 },
            {AudioSourceChannels.Channel2, AudioSourceChannels.Channel2 },
            {AudioSourceChannels.Channel3, AudioSourceChannels.Channel3 },
            {AudioSourceChannels.Channel4, AudioSourceChannels.Channel4 },
            {AudioSourceChannels.Channel5, AudioSourceChannels.Channel5 },
            {AudioSourceChannels.Channel6, AudioSourceChannels.Channel6 },
            {AudioSourceChannels.Channel7, AudioSourceChannels.Channel7 },
            {AudioSourceChannels.Channel8, AudioSourceChannels.Channel8 }
        };
    }
}
