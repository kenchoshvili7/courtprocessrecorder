﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourtRecorderCommon;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using NAudio.Wave.Asio;
using ProgramMode = HelperClasses.ProgramMode;

namespace RecordProvider
{
    public class AudioDevice
    {
        public int DeviceId { get; set; }

        public string DeviceName { get; set; }

        public static List<AudioDevice> GetPlayBackDevices()
        {
            var audioDevices = new List<AudioDevice>();

            var waveOutDevices = WaveOut.DeviceCount;
            for (var waveOutDevice = 0; waveOutDevice < waveOutDevices; waveOutDevice++)
            {
                var deviceInfo = WaveOut.GetCapabilities(waveOutDevice);
                var deviceFullName = GetDeviceFullName(deviceInfo.ProductName);
                audioDevices.Add(new AudioDevice { DeviceId = waveOutDevice, DeviceName = deviceFullName });
            }

            if (Global.GlobalMode == GlobalMode.Recorder && Global.ProgramMode == ProgramMode.CourtProcessRecorder && AudioIO.Apolo16Mixer.Connected)
            {
                audioDevices.Add(new AudioDevice { DeviceId = 0x7ffffff1, DeviceName = "Apolo16 Mixer" });
            }

            return audioDevices;
        }

        public static List<AudioDevice> GetRecordingDevices()
        {
            //var waveInDevices = WaveIn.DeviceCount;
            //for (var waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
            //{
            //    var deviceInfo = WaveIn.GetCapabilities(waveInDevice);
            //    var deviceFullName = GetDeviceFullName(deviceInfo.ProductName);
            //    audioDevices.Add(new AudioDevice { DeviceId = waveInDevice, DeviceName = deviceFullName });
            //}

            var driverNames = AsioDriver.GetAsioDriverNames();

            var audioDevices = driverNames.Select((t, i) => new AudioDevice { DeviceId = i, DeviceName = t }).ToList();

            if (Global.ProgramMode == ProgramMode.CourtProcessRecorder && Global.GlobalMode == GlobalMode.Recorder && AudioIO.Apolo16Mixer.Connected)
                audioDevices.Add(new AudioDevice { DeviceId = 0x7ffffff1, DeviceName = "Apolo16 Mixer" });

            audioDevices.Add(new AudioDevice { DeviceId = -1, DeviceName = "Windows Audio" });

            return audioDevices;
        }

        private static string GetDeviceFullName(string name)
        {
            var deviceFullName = name;

            try
            {
                var enumerator = new MMDeviceEnumerator();
                var device = enumerator.EnumerateAudioEndPoints(DataFlow.All, DeviceState.All).FirstOrDefault(d => d.FriendlyName.StartsWith(name));
                if (device != null)
                    deviceFullName = device.FriendlyName;
            }
            catch { }

            return deviceFullName;
        }

        public static bool IsPlaybackDeviceConnected()
        {
            return Global.GlobalMode == GlobalMode.Recorder && AudioIO.Apolo16Mixer.Connected || WaveOut.DeviceCount > 0;
        }

        public static bool IsRecordingDeviceConnected(int deviceId)
        {
            bool isDeviceConnected;
            if (deviceId == 0x7ffffff1)
                isDeviceConnected = AudioIO.Apolo16Mixer.Connected && AudioIO.Recorder is Apolo16Recorder;
            else if (deviceId == -1)
            {
                isDeviceConnected = true;

                try { using (var waveIn = new WaveIn()) WaveIn.GetCapabilities(waveIn.DeviceNumber); }
                catch { isDeviceConnected = false; }
            }
            else
            {
                isDeviceConnected = true;
                try
                {
                    var asioDriverName = AsioDriver.GetAsioDriverNames()?[deviceId];
                    var drv = new AsioDriverExt(AsioDriver.GetAsioDriverByName(asioDriverName));
                    drv.ReleaseDriver();
                }
                catch (Exception e)
                {
                    isDeviceConnected = false;
                }
            }

            return isDeviceConnected;
        }
    }
}

