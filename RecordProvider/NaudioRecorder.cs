﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourtRecorderCommon;
using DeviceController;
using JsonDatabaseModels;
using NAudio.Lame;
using NAudio.Wave;
using RecordProvider.Interfaces;

namespace RecordProvider
{
    public class NaudioRecorder : IRecorder
    {
        private readonly WaveIn _waveSource;
        private readonly Apolo16Recorder _apolo16Recorder;
        private bool _isRecording;
        private MixerChannels _mixerChannelsAll;
        private string _sessionPath;

        private string _audioFilePathForDuration;

        //public event RecordByteArrayAvialable RecordByteArrayAvialable;
        public event RecordAmplitudeAvialable RecordAmplitudeAvialable;

        public NaudioRecorder()
        {
            _apolo16Recorder = AudioIO.Recorder as Apolo16Recorder;

            _waveSource = new WaveIn { WaveFormat = new WaveFormat(16000, 1) };
        }

        private void OnRecordByteArrayAvialable(byte[] bytes, AudioSourceChannels channels)
        {
            //if (!_isRecording) return;
            //if (!Mp3FileWritersDictionary.ContainsKey(channels)) return;

            try
            {
                _mp3FileWritersDictionary[channels]?.Write(bytes, 0, bytes.Length);
                RecordAmplitudeAvialable?.Invoke(GetAmplitude(bytes), channels);
            }
            catch
            {

            }
        }

        private Dictionary<AudioSourceChannels?, LameMP3FileWriter> _mp3FileWritersDictionary;

        private void InitializeAudioFilesDictionary(Dictionary<AudioSourceChannels?, string> audioFiles)
        {
            _mixerChannelsAll = MixerChannels.Microphone1 | MixerChannels.Microphone2
                                       | MixerChannels.Microphone3 | MixerChannels.Microphone4
                                       | MixerChannels.Microphone5 | MixerChannels.Microphone6;

            _mp3FileWritersDictionary = new Dictionary<AudioSourceChannels?, LameMP3FileWriter>();
            foreach (var file in audioFiles)
            {
                var mixerChannels = _apolo16Recorder.AudioSourceChannelToMixerChannels((AudioSourceChannels)file.Key);

                _mixerChannelsAll = _mixerChannelsAll ^ mixerChannels;

                _mp3FileWritersDictionary.Add(file.Key, new LameMP3FileWriter(_sessionPath + file.Value, _waveSource.WaveFormat, 16));
            }

            GC.KeepAlive(_mp3FileWritersDictionary);
        }


        public void StartRecord(AudioFilesContainer audioFilesContainer, string sessionPath)
        {
            if (_isRecording) return;

            if (_apolo16Recorder != null) _apolo16Recorder.RecordByteArrayAvialable += OnRecordByteArrayAvialable;

            _sessionPath = sessionPath;

            InitializeAudioFilesDictionary(audioFilesContainer.AudioFiles.ToDictionary(c => c.Channel, p => p.Path));

            AudioIO.Apolo16Mixer.Mute = _mixerChannelsAll;

            _audioFilePathForDuration = _sessionPath + audioFilesContainer.AudioFiles.FirstOrDefault().Path;

            _isRecording = AudioIO.StartRecord(AudioIO.SelectedAudioChannels);
        }

        public TimeSpan StopRecord()
        {
            _apolo16Recorder.RecordByteArrayAvialable -= OnRecordByteArrayAvialable;

            _isRecording = false;
            AudioIO.StopRecord();

            AudioIO.Apolo16Mixer.Mute = MixerChannels.Microphone1 | MixerChannels.Microphone2
                                       | MixerChannels.Microphone3 | MixerChannels.Microphone4
                                       | MixerChannels.Microphone5 | MixerChannels.Microphone6;

            foreach (var mp3File in _mp3FileWritersDictionary)
                mp3File.Value?.Flush();

            TimeSpan duration;

            using (var audio = new AudioFileReader(_audioFilePathForDuration))
                duration = audio.TotalTime;

            return duration;
        }

        public void SetChannelVolume(AudioSourceChannels audioSourceChannel, int value)
        {
            _apolo16Recorder?.SetChannelVolume(audioSourceChannel, value);
        }

        public bool MuteChannel(AudioSourceChannels channels)
        {
            var mixerChannels = _apolo16Recorder.AudioSourceChannelToMixerChannels(channels);

            var isTurnedOn = Apolo16Provider.ToggleMicrophone(channels);
            if (isTurnedOn)
                _mixerChannelsAll = _mixerChannelsAll ^ mixerChannels;
            else _mixerChannelsAll = _mixerChannelsAll | mixerChannels;

            return isTurnedOn;
        }

        public void SetMixerOutVolume(int value)
        {
            AudioIO.Apolo16Mixer.SetVolume(MixerChannels.Monitor, (byte)value);
        }

        private float GetAmplitude(byte[] bytes)
        {
            double sum = 0;
            for (var i = 0; i < bytes.Length; i = i + 2)
            {
                var sample = BitConverter.ToInt16(bytes, i) / 32768.0;
                sum += sample * sample;
            }

            var rms = (float)Math.Sqrt(sum / (bytes.Length / 2f));

            return rms;
        }
    }
}
