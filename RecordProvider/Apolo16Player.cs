﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using CourtRecorderCommon;
using DeviceController;
using JsonDatabaseModels;
using NAudio.Wave;
using RecordProvider.HelperClasses;
using RecordProvider.Interfaces;

namespace RecordProvider
{
    public class Apolo16Player : IPlayer
    {
        private readonly Timer _playbackStoppedTimer;
        private int _currentChunkElement;
        private readonly int _chunkCount;
        private double _remainingDuration;
        private long _currentPosition;
        private readonly Session _session;
        private readonly string _sessionPath;
        private readonly List<AudioFilesContainer> _audioFilesContainers;
        private bool _isPaused;
        private readonly StopWatchWithOffset _stopwatch;
        private AudioFilesContainer _currentAudioFilesContainer;

        public PlayerStatus PlayerStatus { get; set; }
        public event PlaybackChunkChangedEvent PlaybackChunkChangedEvent;
        public event PlaybackStopped PlaybackStopped;
        private PlayerStatus _previousPlayerStatus;
        private Dictionary<AudioSourceChannels?, bool> _audioFilesMuteVolumes;

        public Apolo16Player(Session sesson, string sessionPath)
        {
            _session = sesson;
            _sessionPath = sessionPath;

            var deviceNo = 0x7ffffff1;

            if (AudioIO.PlaybackChannel.Device == null || AudioIO.PlaybackChannel == null ||
                AudioIO.PlaybackChannel.Device.DeviceNo != 0x7ffffff1)
                AudioIO.SetPlaybackDevice(AudioIO.GetPlaybackDevice(deviceNo));

            _playbackStoppedTimer = new Timer();
            _playbackStoppedTimer.Elapsed += PlaybackStoppedTimerOnElapsed;
            _stopwatch = new StopWatchWithOffset(TimeSpan.Zero);

            _audioFilesContainers = sesson.AudioFilesContainers;
            _currentChunkElement = 0;
            _chunkCount = _audioFilesContainers.Count;

            InitializeAudioFilesMuteVolumes();
            SetInitialAudioFileDuration();
            InitializeAudioFiles();
        }

        public void Play()
        {
            var workingDir = _sessionPath + _currentAudioFilesContainer.Path;
            AudioIO.SetWorkingPath(workingDir);
            AudioIO.PlaybackChannel.CurrentDevice = null;

            AudioIO.PlaybackChannel.RecordSet = RecordSet;

            AudioIO.SeekPlayback(RecordSet, _currentPosition);

            foreach (var channel in _session.Channels.Select(c => c.ChannelNo))
                AudioIO.PlaybackChannel.SetMuted(channel, _audioFilesMuteVolumes[channel]);

            _playbackStoppedTimer.Start();
            PlayerStatus = PlayerStatus.Playing;
        }

        private void InitializeAudioFiles()
        {
            _playbackStoppedTimer.Interval = _remainingDuration;
            _currentAudioFilesContainer = _audioFilesContainers[_currentChunkElement];
            if (PlaybackChunkChangedEvent != null) PlaybackChunkChangedEvent.Invoke(_currentAudioFilesContainer);
        }

        private void PlaybackStoppedTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            _playbackStoppedTimer.Stop();

            _currentChunkElement++;

            if (_currentChunkElement >= _chunkCount)
            {
                Stop();
                if (PlaybackChunkChangedEvent != null) PlaybackChunkChangedEvent.Invoke(_audioFilesContainers[0]);
                if (PlaybackStopped != null) PlaybackStopped.Invoke();

                return;
            }

            SetInitialAudioFileDuration();
            InitializeAudioFiles();
            Play();
        }

        public void PlayInSequence()
        {
            _currentChunkElement = 0;
            SetInitialAudioFileDuration();
            InitializeAudioFiles();
            Play();

            _stopwatch.Start();
        }

        public void PauseResume()
        {
            if (_isPaused)
            {
                var rs = RecordSet;
                AudioIO.PlaybackChannel.RecordSet = rs;

                if (_previousPlayerStatus != PlayerStatus.Playing)
                    Play();
                else AudioIO.SeekPlayback(rs, -1);

                _playbackStoppedTimer.Start();
                _stopwatch.Start();
                _isPaused = false;
                PlayerStatus = PlayerStatus.Playing;
                return;
            }

            _playbackStoppedTimer.Stop();
            _stopwatch.Stop();
            AudioIO.PausePlayback();
            _isPaused = true;
            PlayerStatus = PlayerStatus.Paused;
        }

        public void Stop()
        {
            _playbackStoppedTimer.Stop();
            _stopwatch.Reset();
            AudioIO.StopPlayback();
            _currentAudioFilesContainer = _audioFilesContainers[0];
            _currentPosition = 0;
            _currentChunkElement = 0;
            PlayerStatus = PlayerStatus.Stopped;
            SetInitialAudioFileDuration();
        }

        public void Seek(TimeSpan timeSpan)
        {
            _previousPlayerStatus = PlayerStatus;

            Stop();

            var currentAudioFilesContainer = _audioFilesContainers.FirstOrDefault(a => a.From <= timeSpan && a.To > timeSpan);
            _currentChunkElement = _audioFilesContainers.IndexOf(currentAudioFilesContainer);

            if (_currentChunkElement < 0) return;

            var currentAudioFilesContainerDuration = new TimeSpan();
            if (currentAudioFilesContainer != null && currentAudioFilesContainer.Duration != null)
                currentAudioFilesContainerDuration = currentAudioFilesContainer.Duration.Value;

            var durationSumBefore = _audioFilesContainers.Where(a => a.From <= timeSpan).Where(f => f.Duration != null).SumTimeSpan(t => t.Duration.Value) - currentAudioFilesContainerDuration;

            var audioFilePath = _sessionPath + _audioFilesContainers[_currentChunkElement].AudioFiles.FirstOrDefault().Path;

            using (var audioFile = new Mp3FileReader(audioFilePath))
            {
                audioFile.CurrentTime = timeSpan - durationSumBefore;
                _currentPosition = audioFile.Position;
                _remainingDuration = (currentAudioFilesContainer.To - timeSpan).TotalMilliseconds; //(currentAudioFilesContainerDuration - durationSumBefore).TotalMilliseconds;
            }

            _stopwatch.ElapsedTimeSpan = timeSpan;

            InitializeAudioFiles();

            if (_previousPlayerStatus == PlayerStatus.Playing)
            {
                _stopwatch.Start();
                Play();
            }
            else
            {
                _isPaused = true;
                PlayerStatus = PlayerStatus.Paused;
            }
        }

        public void SetVolume(int value)
        {
            AudioIO.SetPlaybackVolume(value);
        }

        public bool MuteChannel(AudioSourceChannels audioSourceChannels)
        {
            var isMuted = AudioIO.PlaybackChannel.GetMuted(audioSourceChannels);
            AudioIO.PlaybackChannel.SetMuted(audioSourceChannels, !isMuted);

            _audioFilesMuteVolumes[audioSourceChannels] = !isMuted;

            return !isMuted;
        }

        public TimeSpan GetPosition()
        {
            return _stopwatch.ElapsedTimeSpan;
        }

        public DateTime GetCurrentDateTime()
        {
            return (DateTime)(_currentAudioFilesContainer.StartTime + GetPosition() - _currentAudioFilesContainer.From);
        }

        private RecordSet RecordSet
        {
            get
            {
                var str = string.Empty;

                for (var i = 0; i < _session.Channels.Count; i++)
                    str += (int)_session.Channels[i].ChannelNo + "?" + _session.Channels[i].Name + ".mp3:";

                str = str.Remove(str.Length - 1);

                //var rs = new RecordSet("1?Channel1.mp3:2?Channel2.mp3:4?Channel3.mp3:8?Channel4.mp3:16?Channel5.mp3:64?Channel6.mp3");
                var rs = new RecordSet(str);
                rs.RecordSetData.Add(new RecordSetData(0, AudioSourceChannels.None));
                return rs;
            }
        }

        private void SetInitialAudioFileDuration()
        {
            using (var audioFile = new Mp3FileReader(_sessionPath + _audioFilesContainers[_currentChunkElement].AudioFiles.FirstOrDefault().Path))
            {
                _remainingDuration = audioFile.TotalTime.TotalMilliseconds;
            }
        }

        private void InitializeAudioFilesMuteVolumes()
        {
            _audioFilesMuteVolumes = new Dictionary<AudioSourceChannels?, bool>();
            foreach (var channel in _session.Channels)
                _audioFilesMuteVolumes.Add(channel.ChannelNo, false);
        }

        public void Dispose()
        {
        }
    }
}
