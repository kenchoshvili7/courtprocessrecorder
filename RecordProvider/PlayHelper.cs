﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AudioConfiguration;
using DeviceController;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.Wave;
using RecordProvider.HelperClasses;
using RecordProvider.Interfaces;

namespace RecordProvider
{
    public class PlayHelper
    {
        private IPlayer _player;
        private readonly List<AudioFilesContainer> _audioFilesContainers;
        public event PlaybackChunkChangedEvent PlaybackChunkChangedEvent;
        public event PlaybackStopped PlaybackStopped;

        public PlayHelper(Session session, AudioDevice audioDevice, PlaybackSpeed playbackSpeed = null, Dictionary<AudioSourceChannels?, bool> audioFilesMuteVolumes = null)
        {
            if (playbackSpeed == null) playbackSpeed = PlaybackSpeed.DefaultPlaybackSpeed;

            _audioFilesContainers = session.AudioFilesContainers.OrderBy(a => a.StartTime).ToList();

            var sessionPath = SessionDbWrapper.SessionDbHelper.SessionPath;

            if (audioDevice.DeviceId == 0x7ffffff1) _player = new Apolo16Player(session, sessionPath);
            else _player = new NaudioPlayer(session, sessionPath, audioDevice.DeviceId, playbackSpeed, audioFilesMuteVolumes);

            //GenerateCombinedWaveStreams(session);

            _player.PlaybackChunkChangedEvent += PlayerOnPlaybackChunkChangedEvent;
            _player.PlaybackStopped += PlayerOnPlaybackStopped;
        }

        public void PlayAudio(AudioDevice audioDevice)
        {
            //var databaseHelper = new DatabaseHelper();

            //var session = databaseHelper.GetSession(sessionPath);
            //_audioFilesContainers = session.AudioFilesContainers.OrderBy(a => a.StartTime).ToList();

            if (!_audioFilesContainers.Any()) return;

            _player.PlayInSequence();
        }

        public void PauseResumeAudio()
        {
            if (_player != null) _player.PauseResume();
        }

        public void StopPlayback()
        {
            if (_player != null) _player.Stop();
        }

        public void SeekPlayback(TimeSpan currentTimeSpan)
        {
            _player.Seek(currentTimeSpan);
        }

        public void SetPlaybackVolume(int value)
        {
            _player.SetVolume(value);
        }

        public bool MuteChannel(AudioSourceChannels audioSourceChannels)
        {
            return _player.MuteChannel(audioSourceChannels);
        }

        public TimeSpan GetPlaybackCurrentPosition()
        {
            return _player != null ? _player.GetPosition() : TimeSpan.MinValue;
        }

        public DateTime GetCurrentDateTime()
        {
            return _player.GetCurrentDateTime();
        }

        public PlayerStatus PlayerStatus { get { return _player.PlayerStatus; } }

        public List<ChannelToWaveStream> ChannelToWaveStreams { get; set; }

        public List<AudioFilesContainer> GetAudioFilesContainers()
        {
            return _audioFilesContainers;
        }

        private void GenerateCombinedWaveStreams(Session session)
        {
            ChannelToWaveStreams = new List<ChannelToWaveStream>();

            foreach (var channel in session.Channels)
            {
                var file = _audioFilesContainers.SelectMany(a => a.AudioFiles.Where(f => f.Channel == channel.ChannelNo));
                var paths = file.Select(f => f.Path);
                if (!paths.Any()) continue;

                var channelToWaveStream = new ChannelToWaveStream { Channel = channel, WaveStream = Combine(paths) };
                ChannelToWaveStreams.Add(channelToWaveStream);
            }

            //var values = Enum.GetValues(typeof(AudioSourceChannels));
            //foreach (AudioSourceChannels audioSourceChannel in values)
            //{
            //    if (audioSourceChannel == AudioSourceChannels.None || audioSourceChannel == AudioSourceChannels.ChannelPC) continue;

            //    var file = _audioFilesContainers.SelectMany(a => a.AudioFiles.Where(f => f.Channel == audioSourceChannel));
            //    var paths = file.Select(f => f.Path);
            //    if (!paths.Any()) continue;
            //    CombinedWaveStreams.Add(Combine(paths));
            //}
        }

        public WaveStream Combine(IEnumerable<string> inputFiles)
        {
            Stream output = new MemoryStream();

            foreach (var file in inputFiles)
            {
                using (var reader = new Mp3FileReader(file))
                {
                    if ((output.Position == 0) && (reader.Id3v2Tag != null))
                        output.Write(reader.Id3v2Tag.RawData, 0, reader.Id3v2Tag.RawData.Length);

                    Mp3Frame frame;
                    while ((frame = reader.ReadNextFrame()) != null)
                        output.Write(frame.RawData, 0, frame.RawData.Length);
                }
            }

            output.Position = 0;

            return new Mp3FileReader(output);
        }

        private void PlayerOnPlaybackChunkChangedEvent(AudioFilesContainer audioFilesContainer)
        {
            if (PlaybackChunkChangedEvent != null) PlaybackChunkChangedEvent.Invoke(audioFilesContainer);
        }

        private void PlayerOnPlaybackStopped()
        {
            if (PlaybackStopped != null) PlaybackStopped.Invoke();
        }

        public void Dispose()
        {
            //ChannelToWaveStreams.ForEach(x =>
            //{
            //    if (x.WaveStream != null) x.WaveStream.Dispose();
            //});
            _player.PlaybackChunkChangedEvent -= PlayerOnPlaybackChunkChangedEvent;
            _player.PlaybackStopped -= PlayerOnPlaybackStopped;

            _player.Dispose();
            _player = null;
        }
    }

    public class ChannelToWaveStream
    {
        public Channel Channel { get; set; }
        public WaveStream WaveStream { get; set; }
    }
}