﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RecordProvider.HelperClasses
{
    public static class ExtensionMethods
    {
        public static TimeSpan SumTimeSpan<TSource>(this IEnumerable<TSource> source, Func<TSource, TimeSpan> selector)
        {
            return source.Select(selector).Aggregate(TimeSpan.Zero, (t1, t2) => t1 + t2);
        }

        public static T[,] ToSquare2D<T>(this T[] array, int size)
        {
            var buffer = new T[(int)Math.Ceiling((double)array.Length / size), size];
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    buffer[i, j] = array[i + j];
                }
            }
            return buffer;
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(this T[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }
    }
}
