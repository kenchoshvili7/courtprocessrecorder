﻿using System;
using System.Timers;

namespace RecordProvider.HelperClasses
{
    public class CustomTimer
    {
        private readonly Timer _timer;
        private TimeSpan _elapsedTime;
        private int _currentInterval;

        public CustomTimer(int interval = 1000)
        {
            _elapsedTime = TimeSpan.Zero;

            _currentInterval = interval;

            _timer = new Timer(500);
            _timer.Elapsed += TimerOnElapsed;
        }

        public void Start()
        {
            //_elapsedTime = new TimeSpan(0, 0, 0, 0, _currentInterval);

            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Reset()
        {
            _elapsedTime = TimeSpan.Zero;
            _timer.Stop();
        }

        public void SetInterval(int interval)
        {
            _currentInterval = interval;
        }

        public TimeSpan ElapsedTimeSpan
        {
            get
            {
                return _elapsedTime;
            }
            set
            {
                _elapsedTime = value;
            }
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            _elapsedTime = _elapsedTime.Add(new TimeSpan(0, 0, 0, 0, _currentInterval / 2));
        }

    }
}
