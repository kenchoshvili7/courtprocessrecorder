﻿using CourtRecorderCommon;
using DeviceController;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;

namespace RecordProvider.HelperClasses
{
    public class MeteringSampleProviderWrapper
    {
        private readonly AudioSourceChannels? _channels;
        private readonly MeteringSampleProvider _meter;

        public static event PlaybackMeterStreamVolume PlaybackMeterStreamVolume;

        public MeteringSampleProviderWrapper(ISampleProvider input, AudioSourceChannels? channels)
        {
            _channels = channels;
            _meter = new MeteringSampleProvider(input);
            _meter.StreamVolume += MeterOnStreamVolume;
        }

        public MeteringSampleProvider Meter { get { return _meter; } }

        private void MeterOnStreamVolume(object sender, StreamVolumeEventArgs e)
        {
            PlaybackMeterStreamVolume?.Invoke(_channels, e.MaxSampleValues[0]);
        }
    }
}
