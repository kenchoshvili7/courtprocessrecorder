﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeviceController;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;

namespace RecordProvider.HelperClasses
{
    class AudioPlaybackEngine : IDisposable
    {
        private readonly IWavePlayer _outputDevice;
        private readonly MixingSampleProvider _mixer;

        public AudioPlaybackEngine(int deviceNumber, int sampleRate = 44100, int channelCount = 2)
        {
            _outputDevice = new WaveOutEvent();
            OutputDevice.DeviceNumber = deviceNumber;
            _mixer = new MixingSampleProvider(WaveFormat.CreateIeeeFloatWaveFormat(sampleRate, channelCount))
            {
                ReadFully = false //when true PlaybackStopped event not firing
            };

            _outputDevice.Init(_mixer);
            //outputDevice.Play();
        }

        public WaveOutEvent OutputDevice
        {
            get { return _outputDevice as WaveOutEvent; }
        }

        public MixingSampleProvider Mixer
        {
            get { return _mixer; }
        }

        public AudioFileReader PlaySound(string fileName)
        {
            AudioFileReader input = null;
            try
            {
                input = new AudioFileReader(fileName);
                AddMixerInput(new AutoDisposeFileReader(input));
            }

            catch { }

            return input;
        }

        // custom method to provide audio amplitude
        public AudioFileReader PlaySoundCustom(string fileName, AudioSourceChannels? channels)
        {
            AudioFileReader input = null;
            try
            {
                input = new AudioFileReader(fileName);
                AddMixerInputCustom(new AutoDisposeFileReader(input), channels);
            }

            catch(Exception ex)
            {

            }

            return input;
        }

        private ISampleProvider ConvertToRightChannelCount(ISampleProvider input)
        {
            if (input.WaveFormat.Channels == _mixer.WaveFormat.Channels)
            {
                return input;
            }
            if (input.WaveFormat.Channels == 1 && _mixer.WaveFormat.Channels == 2)
            {
                return new MonoToStereoSampleProvider(input);
            }
            throw new NotImplementedException("Not yet implemented this channel count conversion");
        }

        public void PlaySound(CachedSound sound)
        {
            AddMixerInput(new CachedSoundSampleProvider(sound));
        }

        private void AddMixerInput(ISampleProvider input)
        {
            _mixer.AddMixerInput(ConvertToRightChannelCount(input));
        }

        // custom method to provide audio amplitude
        private void AddMixerInputCustom(ISampleProvider input, AudioSourceChannels? channels)
        {
            var meter = new MeteringSampleProviderWrapper(input, channels);
            _mixer.AddMixerInput(ConvertToRightChannelCount(meter.Meter));
        }

        public void Dispose()
        {
            _outputDevice.Dispose();
        }

        //public static readonly AudioPlaybackEngine Instance = new AudioPlaybackEngine(sampleRate: 44100, channelCount: 2);
    }

    class CachedSound
    {
        public float[] AudioData { get; private set; }
        public WaveFormat WaveFormat { get; private set; }
        public CachedSound(string audioFileName)
        {
            using (var audioFileReader = new AudioFileReader(audioFileName))
            {
                // TODO: could add resampling in here if required
                WaveFormat = audioFileReader.WaveFormat;
                var wholeFile = new List<float>((int)(audioFileReader.Length / 4));
                var readBuffer = new float[audioFileReader.WaveFormat.SampleRate * audioFileReader.WaveFormat.Channels];
                int samplesRead;
                while ((samplesRead = audioFileReader.Read(readBuffer, 0, readBuffer.Length)) > 0)
                {
                    wholeFile.AddRange(readBuffer.Take(samplesRead));
                }
                AudioData = wholeFile.ToArray();
            }
        }
    }

    class CachedSoundSampleProvider : ISampleProvider
    {
        private readonly CachedSound cachedSound;
        private long position;

        public CachedSoundSampleProvider(CachedSound cachedSound)
        {
            this.cachedSound = cachedSound;
        }

        public int Read(float[] buffer, int offset, int count)
        {
            var availableSamples = cachedSound.AudioData.Length - position;
            var samplesToCopy = Math.Min(availableSamples, count);
            Array.Copy(cachedSound.AudioData, position, buffer, offset, samplesToCopy);
            position += samplesToCopy;
            return (int)samplesToCopy;
        }

        public WaveFormat WaveFormat { get { return cachedSound.WaveFormat; } }
    }

    class AutoDisposeFileReader : ISampleProvider
    {
        private readonly AudioFileReader reader;
        private bool isDisposed;
        public AutoDisposeFileReader(AudioFileReader reader)
        {
            this.reader = reader;
            this.WaveFormat = reader.WaveFormat;
        }

        public int Read(float[] buffer, int offset, int count)
        {
            if (isDisposed)
                return 0;
            int read = reader.Read(buffer, offset, count);
            if (read == 0)
            {
                reader.Dispose();
                isDisposed = true;
            }
            return read;
        }

        public WaveFormat WaveFormat { get; private set; }
    }
}
