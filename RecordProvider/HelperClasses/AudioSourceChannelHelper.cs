﻿using DeviceController;

namespace RecordProvider.HelperClasses
{
    public class AudioSourceChannelHelper
    {
        public static int AudioSourceChannelToNum(AudioSourceChannels? channels)
        {
            switch (channels)
            {
                case AudioSourceChannels.Channel1: return 0;
                case AudioSourceChannels.Channel2: return 1;
                case AudioSourceChannels.Channel3: return 2;
                case AudioSourceChannels.Channel4: return 3;
                case AudioSourceChannels.Channel5: return 4;
                case AudioSourceChannels.Channel6: return 5;
                case AudioSourceChannels.Channel7: return 6;
                case AudioSourceChannels.Channel8: return 7;
                default: return -1;                       
            }
        }

        public static AudioSourceChannels NumToAudioSourceChannel(int num)
        {
            switch (num)
            {
                case 0: return AudioSourceChannels.Channel1;
                case 1: return AudioSourceChannels.Channel2;
                case 2: return AudioSourceChannels.Channel3;
                case 3: return AudioSourceChannels.Channel4;
                case 4: return AudioSourceChannels.Channel5;
                case 5: return AudioSourceChannels.Channel6;
                case 6: return AudioSourceChannels.Channel7;
                case 7: return AudioSourceChannels.Channel8;
                default: return AudioSourceChannels.None;
            }
        }
    }
}
