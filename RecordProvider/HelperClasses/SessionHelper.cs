﻿using System;
using System.Linq;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.Wave;

namespace RecordProvider.HelperClasses
{
    public class SessionHelper
    {
        public static void UpdateAudioFilesMissingDurations(Session session, string sessionPath)
        {
            var audioFilesWithMissingDuration =
                session.AudioFilesContainers.Where(a => a.Duration == null || a.Duration.Value == TimeSpan.Zero)
                    .ToList();

            if (!audioFilesWithMissingDuration.Any()) return;

            foreach (var audioFilesContainer in audioFilesWithMissingDuration)
            {
                var duration = TimeSpan.Zero;

                try
                {
                    using (var audioFile = new Mp3FileReader(sessionPath + audioFilesContainer.AudioFiles.FirstOrDefault()?.Path))
                        duration = audioFile.TotalTime;
                }
                catch (Exception ex)
                {
                    ExceptionLogHelper.AddExceptionLog(ex);
                }

                audioFilesContainer.Duration = duration;
                audioFilesContainer.To = audioFilesContainer.From + duration;
                audioFilesContainer.EndTime = audioFilesContainer.StartTime + duration;

                session.AudioFilesDuration += duration;
            }

            SessionDbWrapper.SessionDbHelper.UpdateSession(session);
        }
    }
}
