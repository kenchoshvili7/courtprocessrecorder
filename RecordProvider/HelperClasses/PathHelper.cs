﻿using System;
using System.IO;
using HelperClasses;

namespace RecordProvider.HelperClasses
{
    public class PathHelper
    {
        public static string CreateSessionPath(string sessionNumber)
        {
            var sessionDirPath = GetSessionPath(sessionNumber);
            if (!Directory.Exists(sessionDirPath)) Directory.CreateDirectory(sessionDirPath);

            return sessionDirPath;
        }

        private static string GetSessionPath(string sessionNumber)
        {
            var recorderConfiguration = ConfigurationHelper.GetConfiguration();

            return Path.Combine(recorderConfiguration.ArchiveDirectory, sessionNumber, sessionNumber + "_" + GetDateTimeToPath());
        }

        public static string GetDateTimeToPath()
        {
            return DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Hour + "_" +
                   DateTime.Now.Minute + "_" + DateTime.Now.Second;
        }
    }
}
