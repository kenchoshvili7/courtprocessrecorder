﻿using System.Collections.Generic;

namespace RecordProvider.HelperClasses
{
    public class PlaybackSpeed
    {
        public string Description { get; set; }
        public int SampleRate { get; set; }
        public int Interval { get; set; }

        public static PlaybackSpeed DefaultPlaybackSpeed => new PlaybackSpeed { Description = "ნორმალური", SampleRate = 16000, Interval = 1000 };

        public static List<PlaybackSpeed> GetPlaybackSpeeds
        {
            get
            {
                var playbackSpeeds = new List<PlaybackSpeed>
                {
                    new PlaybackSpeed {Description = "ნელი", SampleRate = 12000, Interval = 750},
                    new PlaybackSpeed {Description = "ნორმალური", SampleRate = 16000, Interval = 1000},
                    new PlaybackSpeed {Description = "სწრაფი",  SampleRate = 24000, Interval = 1500 },
                    new PlaybackSpeed {Description = "უფრო სწრაფი", SampleRate = 32000, Interval = 2000}
                };

                return playbackSpeeds;
            }
        }
    }
}