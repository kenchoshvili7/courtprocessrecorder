﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AudioConfiguration;
using CourtProcessPlayer.HelperClasses;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DeviceController;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.Gui;
using RecordProvider;
using RecordProvider.HelperClasses;
using Timer = System.Windows.Forms.Timer;

namespace CourtProcessRecorder.UserControls
{
    public partial class RecorderUserControl : XtraUserControl
    {
        private readonly RecorderConfiguration _configuration;
        private readonly RecordHelper _recordHelper;
        private bool _isRecording;
        private Session _currentSession;
        private readonly List<VolumeMeter> _volumeMeters;
        private readonly List<TrackBarControl> _volumeTrackBars;
        private readonly Timer _currentTimeTimer = new Timer();
        private readonly StopWatchWithOffset _currentTimeStopWatch;
        private LabelControl _currentTimelbl;
        private List<Channel> _channels;
        private DateTime _resetedOn;
        private TrackBarControl _playbackVolumeTrackBar;
        //private readonly Apolo16Recorder _apolo16Recorder;

        public RecorderUserControl()
        {
            InitializeComponent();

            _volumeMeters = new List<VolumeMeter>();
            _volumeTrackBars = new List<TrackBarControl>();

            _currentTimeTimer.Interval = 1000;
            _currentTimeStopWatch = new StopWatchWithOffset(TimeSpan.Zero);


            //return;
            // if in design mode do not execute code below
            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime || DesignMode) return;

            if (Global.GlobalMode != GlobalMode.Recorder) return;

            _configuration = ConfigurationHelper.GetConfiguration();
            _recordHelper = new RecordHelper();

            //_apolo16Recorder = AudioIO.Recorder as Apolo16Recorder;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _currentTimelbl = FormsHelper.FindControlByTypeAndName<LabelControl>(this, "currentTimelbl");
        }

        private void RecordHelperOnRecordAmplitudeAvialable(float amplitude, AudioSourceChannels channels)
        {
            var volumeMeter = _volumeMeters.FirstOrDefault(v => ((Channel)v.Tag).ChannelNo == channels);
            if (volumeMeter != null) volumeMeter.Amplitude = amplitude;
        }

        private void Apolo16MixerOnApolo16MixerReseted(bool flag)
        {
            if (_resetedOn == DateTime.MinValue && flag)
            {
                _resetedOn = DateTime.Now;
                return;
            }

            var timeElapsed = (DateTime.Now - _resetedOn).TotalSeconds;
            if (timeElapsed < 30 && flag) return;
            _resetedOn = DateTime.MaxValue;

            const string message = "ჩამწერ მოწყობილობასთან გაწყდა კავშირი, გთხოვთ გადატვირთოთ პროგრამა!";
            XtraMessageBox.Show(message, "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
            _resetedOn = DateTime.Now;

            ActionLogger.LogAction(ConstantStrings.Message, $"{message} SessionPath: {SessionDbWrapper.SessionDbHelper.SessionPath}");
        }

        public void InitializeRecorderControl(Session session)
        {
            _currentSession = session;
            _recordHelper.CreateSession(session);

            _channels = _currentSession.Channels;

            RemoveCustomControls();
            AddCustomControls();

            SetRecordingVolumes();

            SetHallVolumeLevel();
        }

        public void OpenSession(Session session)
        {
            _currentSession = session;

            _recordHelper.OpenSession(session);

            _channels = _currentSession.Channels;

            RemoveCustomControls();
            AddCustomControls();

            SetRecordingVolumes();

            SetHallVolumeLevel();
        }

        public Session CurrentSession { get { return _currentSession; } }

        private void AddCustomControls()
        {
            var count = _channels.Count + 1;

            var height = (this.Height - count * 10) / count;
            var size = new Size(120, height);

            recorderTableLayoutPanel.RowStyles.Clear();
            recorderTableLayoutPanel.RowCount = count;

            for (var i = 0; i < _channels.Count; i++)
            {
                var channel = _channels[i];

                recorderTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, height));

                var volumeMeter = new VolumeMeter();
                volumeMeter.Tag = channel;
                volumeMeter.Amplitude = 0F;
                volumeMeter.ForeColor = Color.FromArgb(0, 191, 255);
                volumeMeter.MaxDb = 0F;
                volumeMeter.MinDb = -60F;
                volumeMeter.Name = channel.Name + "volumeMeter";
                volumeMeter.AutoSize = true;
                volumeMeter.Dock = DockStyle.Fill;
                volumeMeter.Orientation = Orientation.Horizontal;

                recorderTableLayoutPanel.Controls.Add(volumeMeter, 1, i);

                _volumeMeters.Add(volumeMeter);

                var label = new LabelControl();
                label.Text = channel.Name;
                label.Font = new Font("Arial", 10, FontStyle.Bold);
                label.ForeColor = Color.Black;
                label.BackColor = Color.FromArgb(40, 220, 20, 60);
                label.Tag = channel;
                label.AutoSizeMode = LabelAutoSizeMode.None;
                label.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
                label.Appearance.TextOptions.VAlignment = VertAlignment.Center;
                label.Size = size;
                label.Click += LabelOnClick;
                label.Dock = DockStyle.Fill;

                volumeMeter.Controls.Add(label);

                var volumeTrackBar = new TrackBarControl();
                volumeTrackBar.Dock = DockStyle.Fill;
                volumeTrackBar.Properties.Orientation = Orientation.Vertical;
                volumeTrackBar.Properties.Maximum = 100;
                volumeTrackBar.Tag = channel;
                volumeTrackBar.AutoSize = true;
                volumeTrackBar.MouseUp += VolumeTrackBarOnMouseUp;
                volumeTrackBar.Properties.TickStyle = TickStyle.None;

                _volumeTrackBars.Add(volumeTrackBar);

                recorderTableLayoutPanel.Controls.Add(volumeTrackBar, 0, i);
            }

            recorderTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, height));

            _playbackVolumeTrackBar = new TrackBarControl();

            _playbackVolumeTrackBar.Properties.Maximum = 255;
            _playbackVolumeTrackBar.Properties.Minimum = 100;
            _playbackVolumeTrackBar.Properties.SmallChange = 32;
            _playbackVolumeTrackBar.Properties.LargeChange = 32;

            _playbackVolumeTrackBar.Dock = DockStyle.Fill;
            _playbackVolumeTrackBar.EditValue = 255;
            _playbackVolumeTrackBar.Location = new Point(0, 0);
            _playbackVolumeTrackBar.Size = size;
            _playbackVolumeTrackBar.AutoSize = true;
            _playbackVolumeTrackBar.TabIndex = 5;
            _playbackVolumeTrackBar.ToolTip = @"ხმა დარბაზში";

            _playbackVolumeTrackBar.Properties.LabelAppearance.Options.UseTextOptions = true;
            _playbackVolumeTrackBar.Properties.LabelAppearance.TextOptions.HAlignment = HorzAlignment.Center;
            _playbackVolumeTrackBar.MouseUp += PlaybackVolumeTrackBarOnMouseUp;

            recorderTableLayoutPanel.Controls.Add(_playbackVolumeTrackBar, 1, _channels.Count);
        }

        private void RemoveCustomControls()
        {
            foreach (var volumeMeter in _volumeMeters)
                recorderTableLayoutPanel.Controls.Remove(volumeMeter);

            _volumeMeters.Clear();
            _volumeTrackBars.Clear();

            recorderTableLayoutPanel.Controls.Remove(_playbackVolumeTrackBar);
        }

        private void LabelOnClick(object sender, EventArgs e)
        {
            var control = sender as Control;
            var channel = control.Tag as Channel ?? new Channel();
            var isTurnedOn = _recordHelper.SetMuteChannel(channel.ChannelNo);
            control.BackColor = isTurnedOn ? Color.FromArgb(40, 220, 20, 60) : Color.FromArgb(100, 128, 128, 128);
        }

        private void SetRecordingVolumes()
        {
            foreach (var channel in _configuration.Channels)
            {
                var trackBarControl = _volumeTrackBars.FirstOrDefault(v => (v.Tag as Channel).ChannelNo == channel.ChannelNo);
                if (trackBarControl != null) trackBarControl.EditValue = channel.RecordVolume;
                _recordHelper.SetChannelVolume(channel.ChannelNo, channel.RecordVolume);
            }
        }

        private void VolumeTrackBarOnMouseUp(object sender, MouseEventArgs e)
        {
            var trackBar = sender as TrackBarControl;
            var channel = trackBar.Tag as Channel ?? new Channel();
            _recordHelper.SetChannelVolume(channel.ChannelNo, (int)trackBar.EditValue);
        }

        private void PlaybackVolumeTrackBarOnMouseUp(object sender, MouseEventArgs e)
        {
            var trackBar = sender as TrackBarControl;
            if (trackBar != null)
            {
                _recordHelper.SetMixerOutVolume((int)trackBar.EditValue);
            }
        }

        public void AddSession(Session session)
        {
            _recordHelper.CreateSession(session);
        }

        public bool Record()
        {
            if (_isRecording)
            {
                _currentTimeTimer.Stop();
                _currentTimeStopWatch.Reset();
                _currentTimeStopWatch.Stop();

                _recordHelper.StopRecord();
                _isRecording = false;

                return _isRecording;
            }

            _recordHelper.StartRecord();
            _isRecording = true;

            _currentTimeStopWatch.Start();
            _currentTimeTimer.Start();

            return _isRecording;
        }

        public void StartRecord()
        {
            _recordHelper.StartRecord();
            _isRecording = true;

            _recordHelper.RecordAmplitudeAvialable += RecordHelperOnRecordAmplitudeAvialable;

            _recordHelper.Apolo16MixerReseted += Apolo16MixerOnApolo16MixerReseted;

            _currentTimeTimer.Tick += CurrentTimeTimerOnTick;

            _currentTimeStopWatch.Start();
            _currentTimeTimer.Start();

            ActionLogger.LogAction(ConstantStrings.RecordStart);
        }

        public void StopRecord()
        {
            _currentTimeTimer.Stop();
            _currentTimeStopWatch.Reset();
            _currentTimeStopWatch.Stop();

            _currentTimeTimer.Tick -= CurrentTimeTimerOnTick;

            _recordHelper.RecordAmplitudeAvialable -= RecordHelperOnRecordAmplitudeAvialable;

            _recordHelper.Apolo16MixerReseted -= Apolo16MixerOnApolo16MixerReseted;

            _recordHelper.StopRecord();
            _isRecording = false;

            ActionLogger.LogAction(ConstantStrings.RecordStop);
        }

        public DateTime GetCurrentTime()
        {
            var lastEndTime = _recordHelper.GetLastEndTime();
            return lastEndTime + _currentTimeStopWatch.ElapsedTimeSpan;
        }

        private void SetHallVolumeLevel()
        {
            var volume = _configuration.HallVolumeLevel ?? 255;
            _playbackVolumeTrackBar.EditValue = volume;
            _recordHelper.SetMixerOutVolume(volume);
        }

        private void CurrentTimeTimerOnTick(object sender, EventArgs e)
        {
            _currentTimeTimer.Stop();

            _currentTimelbl.Text = GetCurrentTime().ToString("HH:mm:ss");

            _currentTimeTimer.Start();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var parms = base.CreateParams;
                parms.Style &= ~0x02000000;  // Turn off WS_CLIPCHILDREN
                return parms;
            }
        }
    }
}
