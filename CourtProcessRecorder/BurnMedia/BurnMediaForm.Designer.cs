﻿namespace CourtProcessRecorder.BurnMedia
{
    partial class BurnMediaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BurnMediaForm));
            this.devicesComboBox = new System.Windows.Forms.ComboBox();
            this.backgroundBurnWorker = new System.ComponentModel.BackgroundWorker();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonBurn = new System.Windows.Forms.Button();
            this.checkBoxEject = new System.Windows.Forms.CheckBox();
            this.textBoxLabel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundFormatWorker = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.closeMediaCheckBox = new System.Windows.Forms.CheckBox();
            this.speedComboBox = new System.Windows.Forms.ComboBox();
            this.speedLabel = new System.Windows.Forms.Label();
            this.avialableDiskSize = new System.Windows.Forms.Label();
            this.progressBarControl = new DevExpress.XtraEditors.ProgressBarControl();
            this.totalMediaSizeValue = new System.Windows.Forms.Label();
            this.totalMediaSizeText = new System.Windows.Forms.Label();
            this.avialableDiskSizeLabel = new System.Windows.Forms.Label();
            this.warningLabel = new System.Windows.Forms.Label();
            this.statusProgressBar = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusProgressBar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // devicesComboBox
            // 
            this.devicesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.devicesComboBox.FormattingEnabled = true;
            this.devicesComboBox.Location = new System.Drawing.Point(172, 22);
            this.devicesComboBox.Name = "devicesComboBox";
            this.devicesComboBox.Size = new System.Drawing.Size(202, 21);
            this.devicesComboBox.TabIndex = 1;
            this.devicesComboBox.SelectedIndexChanged += new System.EventHandler(this.devicesComboBox_SelectedIndexChanged);
            this.devicesComboBox.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.devicesComboBox_Format);
            // 
            // backgroundBurnWorker
            // 
            this.backgroundBurnWorker.WorkerReportsProgress = true;
            this.backgroundBurnWorker.WorkerSupportsCancellation = true;
            this.backgroundBurnWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundBurnWorker_DoWork);
            this.backgroundBurnWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundBurnWorker_ProgressChanged);
            this.backgroundBurnWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundBurnWorker_RunWorkerCompleted);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "All Files (*.*)|*.*";
            // 
            // buttonBurn
            // 
            this.buttonBurn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.buttonBurn.Location = new System.Drawing.Point(12, 291);
            this.buttonBurn.Name = "buttonBurn";
            this.buttonBurn.Size = new System.Drawing.Size(88, 30);
            this.buttonBurn.TabIndex = 6;
            this.buttonBurn.Text = "&ჩაწერა";
            this.buttonBurn.UseVisualStyleBackColor = true;
            this.buttonBurn.Click += new System.EventHandler(this.buttonBurn_Click);
            // 
            // checkBoxEject
            // 
            this.checkBoxEject.AutoSize = true;
            this.checkBoxEject.Checked = true;
            this.checkBoxEject.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxEject.Font = new System.Drawing.Font("Tahoma", 10F);
            this.checkBoxEject.Location = new System.Drawing.Point(268, 297);
            this.checkBoxEject.Name = "checkBoxEject";
            this.checkBoxEject.Size = new System.Drawing.Size(144, 21);
            this.checkBoxEject.TabIndex = 7;
            this.checkBoxEject.Text = "Eject when finished";
            this.checkBoxEject.UseVisualStyleBackColor = true;
            // 
            // textBoxLabel
            // 
            this.textBoxLabel.Location = new System.Drawing.Point(172, 64);
            this.textBoxLabel.Name = "textBoxLabel";
            this.textBoxLabel.ReadOnly = true;
            this.textBoxLabel.Size = new System.Drawing.Size(202, 21);
            this.textBoxLabel.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label2.Location = new System.Drawing.Point(30, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "დისკის დასახელება:";
            // 
            // backgroundFormatWorker
            // 
            this.backgroundFormatWorker.WorkerReportsProgress = true;
            this.backgroundFormatWorker.WorkerSupportsCancellation = true;
            this.backgroundFormatWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundFormatWorker_DoWork);
            this.backgroundFormatWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundFormatWorker_ProgressChanged);
            this.backgroundFormatWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundFormatWorker_RunWorkerCompleted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label1.Location = new System.Drawing.Point(30, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "ჩამწერის არჩევა:";
            // 
            // closeMediaCheckBox
            // 
            this.closeMediaCheckBox.AutoSize = true;
            this.closeMediaCheckBox.Checked = true;
            this.closeMediaCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.closeMediaCheckBox.Font = new System.Drawing.Font("Tahoma", 10F);
            this.closeMediaCheckBox.Location = new System.Drawing.Point(127, 297);
            this.closeMediaCheckBox.Name = "closeMediaCheckBox";
            this.closeMediaCheckBox.Size = new System.Drawing.Size(131, 21);
            this.closeMediaCheckBox.TabIndex = 7;
            this.closeMediaCheckBox.Text = "დისკის დახურვა";
            this.closeMediaCheckBox.UseVisualStyleBackColor = true;
            // 
            // speedComboBox
            // 
            this.speedComboBox.DisplayMember = "Value";
            this.speedComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.speedComboBox.Enabled = false;
            this.speedComboBox.FormattingEnabled = true;
            this.speedComboBox.Location = new System.Drawing.Point(172, 109);
            this.speedComboBox.Name = "speedComboBox";
            this.speedComboBox.Size = new System.Drawing.Size(202, 21);
            this.speedComboBox.TabIndex = 1;
            this.speedComboBox.ValueMember = "Value";
            this.speedComboBox.SelectedValueChanged += new System.EventHandler(this.speedComboBox_SelectedIndexChanged);
            // 
            // speedLabel
            // 
            this.speedLabel.AutoSize = true;
            this.speedLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.speedLabel.Location = new System.Drawing.Point(30, 110);
            this.speedLabel.Name = "speedLabel";
            this.speedLabel.Size = new System.Drawing.Size(102, 17);
            this.speedLabel.TabIndex = 4;
            this.speedLabel.Text = "სიჩქარე (KB/s):";
            // 
            // avialableDiskSize
            // 
            this.avialableDiskSize.AutoSize = true;
            this.avialableDiskSize.Font = new System.Drawing.Font("Tahoma", 10F);
            this.avialableDiskSize.Location = new System.Drawing.Point(185, 189);
            this.avialableDiskSize.Name = "avialableDiskSize";
            this.avialableDiskSize.Size = new System.Drawing.Size(38, 17);
            this.avialableDiskSize.TabIndex = 9;
            this.avialableDiskSize.Text = "0 MB";
            // 
            // progressBarControl
            // 
            this.progressBarControl.Location = new System.Drawing.Point(33, 248);
            this.progressBarControl.Name = "progressBarControl";
            this.progressBarControl.Properties.EndColor = System.Drawing.Color.Red;
            this.progressBarControl.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.progressBarControl.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressBarControl.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.progressBarControl.Properties.ShowTitle = true;
            this.progressBarControl.Properties.StartColor = System.Drawing.Color.Green;
            this.progressBarControl.Properties.Step = 1;
            this.progressBarControl.Size = new System.Drawing.Size(341, 21);
            this.progressBarControl.TabIndex = 10;
            // 
            // totalMediaSizeValue
            // 
            this.totalMediaSizeValue.AutoSize = true;
            this.totalMediaSizeValue.Font = new System.Drawing.Font("Tahoma", 10F);
            this.totalMediaSizeValue.Location = new System.Drawing.Point(185, 153);
            this.totalMediaSizeValue.Name = "totalMediaSizeValue";
            this.totalMediaSizeValue.Size = new System.Drawing.Size(38, 17);
            this.totalMediaSizeValue.TabIndex = 11;
            this.totalMediaSizeValue.Text = "0 MB";
            // 
            // totalMediaSizeText
            // 
            this.totalMediaSizeText.AutoSize = true;
            this.totalMediaSizeText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.totalMediaSizeText.Location = new System.Drawing.Point(30, 153);
            this.totalMediaSizeText.Name = "totalMediaSizeText";
            this.totalMediaSizeText.Size = new System.Drawing.Size(107, 17);
            this.totalMediaSizeText.TabIndex = 11;
            this.totalMediaSizeText.Text = "ფაილების ზომა";
            // 
            // avialableDiskSizeLabel
            // 
            this.avialableDiskSizeLabel.AutoSize = true;
            this.avialableDiskSizeLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.avialableDiskSizeLabel.Location = new System.Drawing.Point(30, 189);
            this.avialableDiskSizeLabel.Name = "avialableDiskSizeLabel";
            this.avialableDiskSizeLabel.Size = new System.Drawing.Size(147, 17);
            this.avialableDiskSizeLabel.TabIndex = 11;
            this.avialableDiskSizeLabel.Text = "თავისუფალი ადგილი";
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = true;
            this.warningLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.warningLabel.ForeColor = System.Drawing.Color.Red;
            this.warningLabel.Location = new System.Drawing.Point(118, 219);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(200, 17);
            this.warningLabel.TabIndex = 11;
            this.warningLabel.Text = "არასაკმარისი ადგილი დისკზე!";
            this.warningLabel.Visible = false;
            // 
            // statusProgressBar
            // 
            this.statusProgressBar.Location = new System.Drawing.Point(12, 340);
            this.statusProgressBar.Name = "statusProgressBar";
            this.statusProgressBar.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.statusProgressBar.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.statusProgressBar.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.statusProgressBar.Properties.ShowTitle = true;
            this.statusProgressBar.Properties.Step = 1;
            this.statusProgressBar.Size = new System.Drawing.Size(400, 30);
            this.statusProgressBar.TabIndex = 12;
            // 
            // BurnMediaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 383);
            this.Controls.Add(this.statusProgressBar);
            this.Controls.Add(this.warningLabel);
            this.Controls.Add(this.avialableDiskSizeLabel);
            this.Controls.Add(this.totalMediaSizeText);
            this.Controls.Add(this.totalMediaSizeValue);
            this.Controls.Add(this.progressBarControl);
            this.Controls.Add(this.avialableDiskSize);
            this.Controls.Add(this.speedLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.closeMediaCheckBox);
            this.Controls.Add(this.checkBoxEject);
            this.Controls.Add(this.buttonBurn);
            this.Controls.Add(this.textBoxLabel);
            this.Controls.Add(this.speedComboBox);
            this.Controls.Add(this.devicesComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BurnMediaForm";
            this.Text = "დისკზე ჩაწერა";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusProgressBar.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox devicesComboBox;
        private System.ComponentModel.BackgroundWorker backgroundBurnWorker;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.CheckBox checkBoxEject;
        private System.Windows.Forms.TextBox textBoxLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonBurn;
        private System.ComponentModel.BackgroundWorker backgroundFormatWorker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox closeMediaCheckBox;
        private System.Windows.Forms.ComboBox speedComboBox;
        private System.Windows.Forms.Label speedLabel;
        private System.Windows.Forms.Label avialableDiskSize;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl;
        private System.Windows.Forms.Label totalMediaSizeValue;
        private System.Windows.Forms.Label totalMediaSizeText;
        private System.Windows.Forms.Label avialableDiskSizeLabel;
        private System.Windows.Forms.Label warningLabel;
        private DevExpress.XtraEditors.ProgressBarControl statusProgressBar;
    }
}

