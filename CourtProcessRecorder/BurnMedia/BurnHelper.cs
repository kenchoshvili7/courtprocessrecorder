﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using IMAPI2.Interop;

namespace CourtProcessRecorder.BurnMedia
{
    public class BurnHelper
    {
        static List<IDiscRecorder2> _recorders;

        public static IMAPI_MEDIA_PHYSICAL_TYPE CurrentPhysicalMediaType(IDiscRecorder2 rec)
        {
            MsftDiscFormat2Data mi = new MsftDiscFormat2Data();
            try
            {

                mi.Recorder = rec;
                return mi.CurrentPhysicalMediaType;
            }
            catch
            {

                return IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_UNKNOWN;
            }
            finally
            {
                Marshal.FinalReleaseComObject(mi);
            }

        }

        public static string GetMediaStateDescription(IDiscRecorder2 rec)
        {
            StringBuilder sb = null;
            MsftDiscFormat2Data mi = new MsftDiscFormat2Data();
            try
            {

                mi.Recorder = rec;
                // Get the media status properties for the media to for diagnostic purposes

                sb = new StringBuilder("Media type:");
                for (int i = 1; i <= (int)mi.CurrentMediaStatus; i <<= 1)
                {

                    if ((((int)mi.CurrentMediaStatus) & i) == i)
                    {
                        sb.Append(EnumHelper.GetDescription(((IMAPI_FORMAT2_DATA_MEDIA_STATE)i)));
                        sb.Append('|');
                    }
                }
                sb.Remove(sb.Length - 1, 1);
                return sb.ToString();
            }
            catch
            {
                return "No Media or the selected drive is not a recorder.";
            }
            finally
            {
                Marshal.FinalReleaseComObject(mi);
                sb.Length = 0;
            }

        }

        public static IList<KeyValuePair<IWriteSpeedDescriptor, string>> GetSpeedDescriptors(IDiscRecorder2 rec)
        {

            List<KeyValuePair<IWriteSpeedDescriptor, string>> list = null;
            MsftDiscFormat2Data mi = new MsftDiscFormat2Data();
            try
            {

                mi.Recorder = rec;
                //uint[] speeds = mi.SupportedWriteSpeeds;
                Array spds = mi.SupportedWriteSpeedDescriptors;
                System.Collections.IEnumerator enm = spds.GetEnumerator();
                list = new List<KeyValuePair<IWriteSpeedDescriptor, string>>(spds.GetLength(0));
                while (enm.MoveNext())
                {
                    IWriteSpeedDescriptor spd = enm.Current as IWriteSpeedDescriptor;
                    if (spd != null)
                        list.Add(new KeyValuePair<IWriteSpeedDescriptor, string>(spd, spd.WriteSpeed.ToString()));
                }
            }
            catch
            {

                return null;
            }
            finally
            {
                Marshal.FinalReleaseComObject(mi);
            }
            return (list.Count > 0) ? list : null;

        }
    }
}
