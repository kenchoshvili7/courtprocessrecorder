﻿namespace CourtProcessRecorder
{
    partial class DigitalSignDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.printSimleButton = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.responsiblePersonLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.sessionDatelbl = new DevExpress.XtraEditors.LabelControl();
            this.sessionDateLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.signatureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.responsiblePersonTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.checkSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.warningTextLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.organizationLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.createdOnLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.organizationTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.createdOnTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.closeSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.signatureDataMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionDateLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signatureBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.responsiblePersonTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.organizationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.createdOnTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signatureDataMemoEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // printSimleButton
            // 
            this.printSimleButton.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.printSimleButton.Appearance.Options.UseFont = true;
            this.printSimleButton.Location = new System.Drawing.Point(272, 379);
            this.printSimleButton.Name = "printSimleButton";
            this.printSimleButton.Size = new System.Drawing.Size(75, 23);
            this.printSimleButton.TabIndex = 0;
            this.printSimleButton.Text = "ბეჭდვა";
            this.printSimleButton.Click += new System.EventHandler(this.printSimleButton_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(0, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(0, 13);
            this.labelControl1.TabIndex = 3;
            // 
            // responsiblePersonLabelControl
            // 
            this.responsiblePersonLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.responsiblePersonLabelControl.Location = new System.Drawing.Point(18, 56);
            this.responsiblePersonLabelControl.Name = "responsiblePersonLabelControl";
            this.responsiblePersonLabelControl.Size = new System.Drawing.Size(126, 17);
            this.responsiblePersonLabelControl.TabIndex = 2;
            this.responsiblePersonLabelControl.Text = "პასუხისმგებელი პირი";
            // 
            // sessionDatelbl
            // 
            this.sessionDatelbl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionDatelbl.Location = new System.Drawing.Point(18, 18);
            this.sessionDatelbl.Name = "sessionDatelbl";
            this.sessionDatelbl.Size = new System.Drawing.Size(101, 17);
            this.sessionDatelbl.TabIndex = 2;
            this.sessionDatelbl.Text = "სხდომის თარიღი";
            // 
            // sessionDateLookUpEdit
            // 
            this.sessionDateLookUpEdit.Location = new System.Drawing.Point(179, 15);
            this.sessionDateLookUpEdit.Name = "sessionDateLookUpEdit";
            this.sessionDateLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionDateLookUpEdit.Properties.Appearance.Options.UseFont = true;
            this.sessionDateLookUpEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionDateLookUpEdit.Properties.AppearanceDropDown.Options.UseFont = true;
            this.sessionDateLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sessionDateLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SessionDate", "Session Date", 84, DevExpress.Utils.FormatType.DateTime, "dd.MM.yyyy", true, DevExpress.Utils.HorzAlignment.Near)});
            this.sessionDateLookUpEdit.Properties.DataSource = this.signatureBindingSource;
            this.sessionDateLookUpEdit.Properties.DisplayFormat.FormatString = "dd.MM.yyyy";
            this.sessionDateLookUpEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.sessionDateLookUpEdit.Properties.DisplayMember = "SessionDate";
            this.sessionDateLookUpEdit.Properties.DropDownRows = 4;
            this.sessionDateLookUpEdit.Properties.ShowHeader = false;
            this.sessionDateLookUpEdit.Size = new System.Drawing.Size(292, 22);
            this.sessionDateLookUpEdit.TabIndex = 5;
            // 
            // signatureBindingSource
            // 
            this.signatureBindingSource.DataSource = typeof(JsonDatabaseModels.Signature);
            // 
            // responsiblePersonTextEdit
            // 
            this.responsiblePersonTextEdit.Location = new System.Drawing.Point(179, 53);
            this.responsiblePersonTextEdit.Name = "responsiblePersonTextEdit";
            this.responsiblePersonTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.responsiblePersonTextEdit.Properties.Appearance.Options.UseFont = true;
            this.responsiblePersonTextEdit.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 10F);
            this.responsiblePersonTextEdit.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.responsiblePersonTextEdit.Properties.ReadOnly = true;
            this.responsiblePersonTextEdit.Size = new System.Drawing.Size(292, 22);
            this.responsiblePersonTextEdit.TabIndex = 6;
            // 
            // checkSimpleButton
            // 
            this.checkSimpleButton.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.checkSimpleButton.Appearance.Options.UseFont = true;
            this.checkSimpleButton.Location = new System.Drawing.Point(169, 379);
            this.checkSimpleButton.Name = "checkSimpleButton";
            this.checkSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.checkSimpleButton.TabIndex = 7;
            this.checkSimpleButton.Text = "შემოწმება";
            this.checkSimpleButton.Click += new System.EventHandler(this.checkSimpleButton_Click);
            // 
            // warningTextLabelControl
            // 
            this.warningTextLabelControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.warningTextLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.warningTextLabelControl.Appearance.ForeColor = System.Drawing.Color.Red;
            this.warningTextLabelControl.Location = new System.Drawing.Point(-84, 395);
            this.warningTextLabelControl.Name = "warningTextLabelControl";
            this.warningTextLabelControl.Size = new System.Drawing.Size(0, 16);
            this.warningTextLabelControl.TabIndex = 8;
            // 
            // organizationLabelControl
            // 
            this.organizationLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.organizationLabelControl.Location = new System.Drawing.Point(18, 95);
            this.organizationLabelControl.Name = "organizationLabelControl";
            this.organizationLabelControl.Size = new System.Drawing.Size(75, 17);
            this.organizationLabelControl.TabIndex = 2;
            this.organizationLabelControl.Text = "ორგანიზაცია";
            // 
            // createdOnLabelControl
            // 
            this.createdOnLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.createdOnLabelControl.Location = new System.Drawing.Point(18, 131);
            this.createdOnLabelControl.Name = "createdOnLabelControl";
            this.createdOnLabelControl.Size = new System.Drawing.Size(74, 17);
            this.createdOnLabelControl.TabIndex = 2;
            this.createdOnLabelControl.Text = "შექმნის დრო";
            // 
            // organizationTextEdit
            // 
            this.organizationTextEdit.Location = new System.Drawing.Point(179, 92);
            this.organizationTextEdit.Name = "organizationTextEdit";
            this.organizationTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.organizationTextEdit.Properties.Appearance.Options.UseFont = true;
            this.organizationTextEdit.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 10F);
            this.organizationTextEdit.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.organizationTextEdit.Properties.ReadOnly = true;
            this.organizationTextEdit.Size = new System.Drawing.Size(292, 22);
            this.organizationTextEdit.TabIndex = 6;
            // 
            // createdOnTextEdit
            // 
            this.createdOnTextEdit.Location = new System.Drawing.Point(179, 128);
            this.createdOnTextEdit.Name = "createdOnTextEdit";
            this.createdOnTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.createdOnTextEdit.Properties.Appearance.Options.UseFont = true;
            this.createdOnTextEdit.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 10F);
            this.createdOnTextEdit.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.createdOnTextEdit.Properties.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.createdOnTextEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.createdOnTextEdit.Properties.EditFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.createdOnTextEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.createdOnTextEdit.Properties.ReadOnly = true;
            this.createdOnTextEdit.Size = new System.Drawing.Size(292, 22);
            this.createdOnTextEdit.TabIndex = 6;
            // 
            // closeSimpleButton
            // 
            this.closeSimpleButton.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.closeSimpleButton.Appearance.Options.UseFont = true;
            this.closeSimpleButton.Location = new System.Drawing.Point(370, 379);
            this.closeSimpleButton.Name = "closeSimpleButton";
            this.closeSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.closeSimpleButton.TabIndex = 0;
            this.closeSimpleButton.Text = "დახურვა";
            this.closeSimpleButton.Click += new System.EventHandler(this.closeSimpleButton_Click);
            // 
            // signatureDataMemoEdit
            // 
            this.signatureDataMemoEdit.Location = new System.Drawing.Point(18, 168);
            this.signatureDataMemoEdit.Name = "signatureDataMemoEdit";
            this.signatureDataMemoEdit.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 10F);
            this.signatureDataMemoEdit.Properties.Appearance.Options.UseFont = true;
            this.signatureDataMemoEdit.Properties.ReadOnly = true;
            this.signatureDataMemoEdit.Size = new System.Drawing.Size(455, 194);
            this.signatureDataMemoEdit.TabIndex = 9;
            // 
            // DigitalSignDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 415);
            this.Controls.Add(this.signatureDataMemoEdit);
            this.Controls.Add(this.warningTextLabelControl);
            this.Controls.Add(this.checkSimpleButton);
            this.Controls.Add(this.createdOnTextEdit);
            this.Controls.Add(this.organizationTextEdit);
            this.Controls.Add(this.responsiblePersonTextEdit);
            this.Controls.Add(this.sessionDateLookUpEdit);
            this.Controls.Add(this.sessionDatelbl);
            this.Controls.Add(this.createdOnLabelControl);
            this.Controls.Add(this.organizationLabelControl);
            this.Controls.Add(this.responsiblePersonLabelControl);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.closeSimpleButton);
            this.Controls.Add(this.printSimleButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DigitalSignDetailForm";
            this.Text = "ხელმოწერა";
            ((System.ComponentModel.ISupportInitialize)(this.sessionDateLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signatureBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.responsiblePersonTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.organizationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.createdOnTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signatureDataMemoEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton printSimleButton;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl responsiblePersonLabelControl;
        private DevExpress.XtraEditors.LabelControl sessionDatelbl;
        private DevExpress.XtraEditors.LookUpEdit sessionDateLookUpEdit;
        private DevExpress.XtraEditors.TextEdit responsiblePersonTextEdit;
        private DevExpress.XtraEditors.SimpleButton checkSimpleButton;
        private DevExpress.XtraEditors.LabelControl warningTextLabelControl;
        private DevExpress.XtraEditors.LabelControl organizationLabelControl;
        private DevExpress.XtraEditors.LabelControl createdOnLabelControl;
        private DevExpress.XtraEditors.TextEdit organizationTextEdit;
        private DevExpress.XtraEditors.TextEdit createdOnTextEdit;
        private DevExpress.XtraEditors.SimpleButton closeSimpleButton;
        private DevExpress.XtraEditors.MemoEdit signatureDataMemoEdit;
        private System.Windows.Forms.BindingSource signatureBindingSource;
    }
}