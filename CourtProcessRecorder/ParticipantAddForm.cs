﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using HelperClasses;
using JsonDatabaseModels;

namespace CourtProcessRecorder
{
    public partial class ParticipantAddForm : XtraForm
    {
        private List<Participant> _participants;
        private SessionTemplate _sessionTemplate;
        private readonly DatabaseHelper _databaseHelper;
        //private SessionDbHelper _sessionDbHelper;

        public ParticipantAddForm()
        {
            InitializeComponent();

            _databaseHelper = DatabaseHelper.DatabaseHelperInstance();

            positionNameTextEdit.KeyPress += OnEnterPress;

            positionsGridView.RowClick += PositionsGridViewOnRowClick;
            participantsGridView.RowClick += ParticipantsGridViewOnRowClick;

            positionsGridView.RowCellStyle += OnRowCellStyle;
            participantsGridView.RowCellStyle += OnRowCellStyle;
        }

        private void OnRowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            var view = sender as GridView;
            if (view != null && e.RowHandle != view.FocusedRowHandle) return;
            e.Appearance.BackColor = Color.DarkBlue;
            e.Appearance.ForeColor = Color.White;
        }

        public List<Participant> Participants { get { return _participants; } }

        //public void InitializePositions(List<Position> positions, List<Participant> participants)
        public void InitializePositions(SessionTemplate sessionTemplate, List<Participant> participants)
        {
            _sessionTemplate = sessionTemplate;

            _participants = participants ?? new List<Participant>();

            positionsGridControl.DataSource = sessionTemplate.Positions;
            participantsGridControl.DataSource = participants;

            positionNameTextEdit.Focus();
        }

        private void ParticipantsGridViewOnRowClick(object sender, RowClickEventArgs e)
        {
            var participant = participantsGridView.GetFocusedRow() as Participant;
            if (participant == null) return;
            positionNameTextEdit.Text = participant.Name;

            positionNameTextEdit.Focus();
        }

        private void PositionsGridViewOnRowClick(object sender, RowClickEventArgs e)
        {
            var position = positionsGridView.GetFocusedRow() as Position;
            if (position == null) return;
            positionNameTextEdit.Text = string.Empty;

            positionNameTextEdit.Focus();
        }

        private void addParticipantSimpleButton_Click(object sender, EventArgs e)
        {
            var position = positionsGridView.GetFocusedRow() as Position;
            if (position == null) return;

            if (string.IsNullOrWhiteSpace(positionNameTextEdit.Text))
            {
                XtraMessageBox.Show("სახელი, გვარი არ შეიძლება იყოს ცარიელი!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var newParticipant = new Participant
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                Position = position,
                Name = positionNameTextEdit.Text
            };

            if (Participants.Any(p => p.Name == newParticipant.Name && p.Position.Name == newParticipant.Position.Name))
            {
                XtraMessageBox.Show("ასეთი მონაწილე უკვე არსებობს!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _participants.Add(newParticipant);

            RefreshParticipantsGridAndFocus();
        }

        private void deleteParticipantSimpleButton2_Click(object sender, EventArgs e)
        {
            var participant = participantsGridView.GetFocusedRow() as Participant;
            if (participant == null) return;

            _participants.RemoveAll(p => p.Name == participant.Name && p.Position.Name == participant.Position.Name);

            RefreshParticipantsGridAndFocus();
        }

        private void renameParticipantSimpleButton_Click(object sender, EventArgs e)
        {
            var participant = participantsGridView.GetFocusedRow() as Participant;
            if (participant == null) return;

            _participants.FirstOrDefault(p => p.UId == participant.UId).Name = positionNameTextEdit.Text;

            RefreshParticipantsGridAndFocus();
        }

        private void newPositionSimpleButton_Click(object sender, EventArgs e)
        {
            if (_sessionTemplate == null) return;

            var addPosition = new AddPosition
            {
                StartPosition = FormStartPosition.CenterParent,
                Text = @"პოზიციის დამატება"
            };

            if (addPosition.ShowDialog() != DialogResult.OK) return;

            _sessionTemplate.Positions.Add(new Position
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                Name = addPosition.PositionName
            });

            _databaseHelper.UpdateSessionTemplate(_sessionTemplate);

            _sessionTemplate = _databaseHelper.GetSessionTemplates().FirstOrDefault(t => t.UId == _sessionTemplate.UId);

            positionsGridControl.DataSource = _sessionTemplate?.Positions;
        }

        private void importParticipantsButton_Click(object sender, EventArgs e)
        {
            if (importParticipantsFolderBrowser.ShowDialog() != DialogResult.OK) return;
            var path = importParticipantsFolderBrowser.SelectedPath;

            Session session;
            using (var sessionDbHelper = new SessionDbHelper(path))
            {
                session = sessionDbHelper.GetSession();
            }

            if (session?.Participants == null) return;

            foreach (var participant in session.Participants)
            {
                if (_participants.Any(p => p.ParticipantDescription.Equals(participant.ParticipantDescription))) continue;
                _participants.Add(participant);
            }

            RefreshParticipantsGridAndFocus();
        }

        private void okSimpleButton_Click(object sender, EventArgs e)
        {
            ExecuteAction();
        }

        private void OnEnterPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                ExecuteAction();
        }

        private void ExecuteAction()
        {
            var participants = participantsGridControl.DataSource as List<Participant>;

            _participants = participants?.OrderBy(o => o.CreatedOn).ToList();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void RefreshParticipantsGridAndFocus()
        {
            participantsGridControl.DataSource = _participants;
            participantsGridControl.RefreshDataSource();
            positionNameTextEdit.Focus();
        }

        private void cancelSimpleButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}