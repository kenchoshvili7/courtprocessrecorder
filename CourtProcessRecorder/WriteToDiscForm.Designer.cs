﻿namespace CourtProcessRecorder
{
    partial class WriteToDiscForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.discNumberLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.discNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.discNumberTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // discNumberLabelControl
            // 
            this.discNumberLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.discNumberLabelControl.Location = new System.Drawing.Point(12, 17);
            this.discNumberLabelControl.Name = "discNumberLabelControl";
            this.discNumberLabelControl.Size = new System.Drawing.Size(43, 17);
            this.discNumberLabelControl.TabIndex = 0;
            this.discNumberLabelControl.Text = "ნომერი";
            // 
            // discNumberTextEdit
            // 
            this.discNumberTextEdit.Location = new System.Drawing.Point(12, 40);
            this.discNumberTextEdit.Name = "discNumberTextEdit";
            this.discNumberTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.discNumberTextEdit.Properties.Appearance.Options.UseFont = true;
            this.discNumberTextEdit.Size = new System.Drawing.Size(269, 22);
            this.discNumberTextEdit.TabIndex = 1;
            // 
            // okSimpleButton
            // 
            this.okSimpleButton.Location = new System.Drawing.Point(12, 83);
            this.okSimpleButton.Name = "okSimpleButton";
            this.okSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.okSimpleButton.TabIndex = 3;
            this.okSimpleButton.Text = "OK";
            this.okSimpleButton.Click += new System.EventHandler(this.okSimpleButton_Click);
            // 
            // cancelSimpleButton
            // 
            this.cancelSimpleButton.Location = new System.Drawing.Point(206, 83);
            this.cancelSimpleButton.Name = "cancelSimpleButton";
            this.cancelSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.cancelSimpleButton.TabIndex = 3;
            this.cancelSimpleButton.Text = "გაუქმება";
            this.cancelSimpleButton.Click += new System.EventHandler(this.cancelSimpleButton_Click);
            // 
            // WriteToDiscForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 112);
            this.Controls.Add(this.cancelSimpleButton);
            this.Controls.Add(this.okSimpleButton);
            this.Controls.Add(this.discNumberTextEdit);
            this.Controls.Add(this.discNumberLabelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "WriteToDiscForm";
            this.Text = "დისკზე ჩაწერა";
            ((System.ComponentModel.ISupportInitialize)(this.discNumberTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl discNumberLabelControl;
        private DevExpress.XtraEditors.TextEdit discNumberTextEdit;
        private DevExpress.XtraEditors.SimpleButton okSimpleButton;
        private DevExpress.XtraEditors.SimpleButton cancelSimpleButton;
    }
}