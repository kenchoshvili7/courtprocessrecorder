﻿using CourtProcessRecorder.UserControls;

namespace CourtProcessRecorder
{
    partial class SessionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionsForm));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.skinRibbonGallery = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.newSession = new DevExpress.XtraBars.BarButtonItem();
            this.openSession = new DevExpress.XtraBars.BarButtonItem();
            this.deleteSession = new DevExpress.XtraBars.BarButtonItem();
            this.writeToDiscBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.selectItemsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.addToContainerButton = new DevExpress.XtraBars.BarButtonItem();
            this.fromDateBarEditItem = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.toDateBarEditItem = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.MainPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.MainGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.containerGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.writeToDiscRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.filterRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.sessionEventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sessionActionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.participantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.participantBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.audioDeviceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sessionsGridControl = new DevExpress.XtraGrid.GridControl();
            this.sessionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sessionsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIsSigned = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.openSessionFolderBrowserDialog = new Ookii.Dialogs.VistaFolderBrowserDialog();
            this.generateCprBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionActionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.skinRibbonGallery,
            this.newSession,
            this.openSession,
            this.deleteSession,
            this.writeToDiscBarButtonItem,
            this.selectItemsBarButtonItem,
            this.addToContainerButton,
            this.fromDateBarEditItem,
            this.toDateBarEditItem,
            this.generateCprBarButtonItem});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 10;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.MainPage});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2});
            this.ribbon.Size = new System.Drawing.Size(1190, 146);
            // 
            // skinRibbonGallery
            // 
            this.skinRibbonGallery.Caption = "Skins";
            this.skinRibbonGallery.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.skinRibbonGallery.Id = 1;
            this.skinRibbonGallery.Name = "skinRibbonGallery";
            // 
            // newSession
            // 
            this.newSession.Caption = "ახალი";
            this.newSession.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.newSession.Id = 1;
            this.newSession.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("newSession.ImageOptions.Image")));
            this.newSession.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("newSession.ImageOptions.LargeImage")));
            this.newSession.Name = "newSession";
            this.newSession.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.newSession_ItemClick);
            // 
            // openSession
            // 
            this.openSession.Caption = "გახსნა";
            this.openSession.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.openSession.Id = 2;
            this.openSession.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("openSession.ImageOptions.Image")));
            this.openSession.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("openSession.ImageOptions.LargeImage")));
            this.openSession.Name = "openSession";
            this.openSession.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.openSession_ItemClick);
            // 
            // deleteSession
            // 
            this.deleteSession.Caption = "ამოშლა სიიდან";
            this.deleteSession.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.deleteSession.Id = 3;
            this.deleteSession.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("deleteSession.ImageOptions.Image")));
            this.deleteSession.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("deleteSession.ImageOptions.LargeImage")));
            this.deleteSession.Name = "deleteSession";
            this.deleteSession.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteSession_ItemClick);
            // 
            // writeToDiscBarButtonItem
            // 
            this.writeToDiscBarButtonItem.Caption = "დისკზე ჩაწერა";
            this.writeToDiscBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.writeToDiscBarButtonItem.Id = 4;
            this.writeToDiscBarButtonItem.ImageOptions.LargeImage = global::CourtProcessRecorder.Properties.Resources.burn;
            this.writeToDiscBarButtonItem.Name = "writeToDiscBarButtonItem";
            this.writeToDiscBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.writeToDiscBarButtonItem_ItemClick);
            // 
            // selectItemsBarButtonItem
            // 
            this.selectItemsBarButtonItem.Caption = "სხდომების მონიშვნა";
            this.selectItemsBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.selectItemsBarButtonItem.Id = 5;
            this.selectItemsBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("selectItemsBarButtonItem.ImageOptions.Image")));
            this.selectItemsBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("selectItemsBarButtonItem.ImageOptions.LargeImage")));
            this.selectItemsBarButtonItem.Name = "selectItemsBarButtonItem";
            this.selectItemsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.selectItemsBarButtonItem_ItemClick);
            // 
            // addToContainerButton
            // 
            this.addToContainerButton.Caption = "დამატება სიაში";
            this.addToContainerButton.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.addToContainerButton.Id = 6;
            this.addToContainerButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("addToContainerButton.ImageOptions.Image")));
            this.addToContainerButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("addToContainerButton.ImageOptions.LargeImage")));
            this.addToContainerButton.Name = "addToContainerButton";
            this.addToContainerButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addToContainerButton_ItemClick);
            // 
            // fromDateBarEditItem
            // 
            this.fromDateBarEditItem.Edit = this.repositoryItemDateEdit1;
            this.fromDateBarEditItem.EditWidth = 150;
            this.fromDateBarEditItem.Id = 7;
            this.fromDateBarEditItem.Name = "fromDateBarEditItem";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // toDateBarEditItem
            // 
            this.toDateBarEditItem.Edit = this.repositoryItemDateEdit2;
            this.toDateBarEditItem.EditWidth = 150;
            this.toDateBarEditItem.Id = 8;
            this.toDateBarEditItem.Name = "toDateBarEditItem";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // MainPage
            // 
            this.MainPage.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.MainPage.Appearance.Options.UseFont = true;
            this.MainPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.MainGroup,
            this.containerGroup,
            this.writeToDiscRibbonPageGroup,
            this.filterRibbonPageGroup});
            this.MainPage.Name = "MainPage";
            this.MainPage.Text = "მთავარი";
            // 
            // MainGroup
            // 
            this.MainGroup.ItemLinks.Add(this.newSession);
            this.MainGroup.ItemLinks.Add(this.openSession);
            this.MainGroup.Name = "MainGroup";
            this.MainGroup.Text = "ძირითადი";
            // 
            // containerGroup
            // 
            this.containerGroup.ItemLinks.Add(this.addToContainerButton);
            this.containerGroup.ItemLinks.Add(this.deleteSession);
            this.containerGroup.Name = "containerGroup";
            this.containerGroup.Text = "სია";
            // 
            // writeToDiscRibbonPageGroup
            // 
            this.writeToDiscRibbonPageGroup.ItemLinks.Add(this.selectItemsBarButtonItem);
            this.writeToDiscRibbonPageGroup.ItemLinks.Add(this.writeToDiscBarButtonItem);
            this.writeToDiscRibbonPageGroup.ItemLinks.Add(this.generateCprBarButtonItem);
            this.writeToDiscRibbonPageGroup.Name = "writeToDiscRibbonPageGroup";
            this.writeToDiscRibbonPageGroup.Text = "დისკზე ჩაწერა";
            // 
            // filterRibbonPageGroup
            // 
            this.filterRibbonPageGroup.ItemLinks.Add(this.fromDateBarEditItem);
            this.filterRibbonPageGroup.ItemLinks.Add(this.toDateBarEditItem);
            this.filterRibbonPageGroup.Name = "filterRibbonPageGroup";
            this.filterRibbonPageGroup.Text = "ფილტრი";
            // 
            // sessionEventBindingSource
            // 
            this.sessionEventBindingSource.DataSource = typeof(JsonDatabaseModels.SessionEvent);
            // 
            // sessionActionBindingSource
            // 
            this.sessionActionBindingSource.DataSource = typeof(JsonDatabaseModels.SessionAction);
            // 
            // participantBindingSource
            // 
            this.participantBindingSource.DataSource = typeof(JsonDatabaseModels.Participant);
            // 
            // participantBindingSource1
            // 
            this.participantBindingSource1.DataSource = typeof(JsonDatabaseModels.Participant);
            // 
            // audioDeviceBindingSource
            // 
            this.audioDeviceBindingSource.DataSource = typeof(RecordProvider.AudioDevice);
            // 
            // sessionsGridControl
            // 
            this.sessionsGridControl.DataSource = this.sessionBindingSource;
            this.sessionsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sessionsGridControl.Location = new System.Drawing.Point(0, 146);
            this.sessionsGridControl.MainView = this.sessionsGridView;
            this.sessionsGridControl.MenuManager = this.ribbon;
            this.sessionsGridControl.Name = "sessionsGridControl";
            this.sessionsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit});
            this.sessionsGridControl.Size = new System.Drawing.Size(1190, 749);
            this.sessionsGridControl.TabIndex = 2;
            this.sessionsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.sessionsGridView});
            // 
            // sessionBindingSource
            // 
            this.sessionBindingSource.DataSource = typeof(JsonDatabaseModels.Session);
            // 
            // sessionsGridView
            // 
            this.sessionsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIsSigned,
            this.colNumber,
            this.colName,
            this.colPath,
            this.colCreatedOn});
            this.sessionsGridView.GridControl = this.sessionsGridControl;
            this.sessionsGridView.GroupCount = 1;
            this.sessionsGridView.Name = "sessionsGridView";
            this.sessionsGridView.OptionsBehavior.AutoExpandAllGroups = true;
            this.sessionsGridView.OptionsBehavior.Editable = false;
            this.sessionsGridView.OptionsDetail.EnableMasterViewMode = false;
            this.sessionsGridView.OptionsMenu.EnableColumnMenu = false;
            this.sessionsGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.sessionsGridView.OptionsSelection.EnableAppearanceHideSelection = false;
            this.sessionsGridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.sessionsGridView.OptionsSelection.ResetSelectionClickOutsideCheckboxSelector = true;
            this.sessionsGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.sessionsGridView.OptionsView.ShowAutoFilterRow = true;
            this.sessionsGridView.OptionsView.ShowGroupedColumns = true;
            this.sessionsGridView.OptionsView.ShowGroupPanel = false;
            this.sessionsGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCreatedOn, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colIsSigned
            // 
            this.colIsSigned.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colIsSigned.AppearanceCell.Options.UseFont = true;
            this.colIsSigned.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colIsSigned.AppearanceHeader.Options.UseFont = true;
            this.colIsSigned.Caption = "ხელმოწერა";
            this.colIsSigned.ColumnEdit = this.repositoryItemCheckEdit;
            this.colIsSigned.FieldName = "IsSigned";
            this.colIsSigned.Name = "colIsSigned";
            this.colIsSigned.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colIsSigned.OptionsColumn.ShowCaption = false;
            this.colIsSigned.Visible = true;
            this.colIsSigned.VisibleIndex = 0;
            this.colIsSigned.Width = 50;
            // 
            // repositoryItemCheckEdit
            // 
            this.repositoryItemCheckEdit.AutoHeight = false;
            this.repositoryItemCheckEdit.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.repositoryItemCheckEdit.ImageOptions.ImageChecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit.ImageOptions.ImageChecked")));
            this.repositoryItemCheckEdit.Name = "repositoryItemCheckEdit";
            // 
            // colNumber
            // 
            this.colNumber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colNumber.AppearanceCell.Options.UseFont = true;
            this.colNumber.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colNumber.AppearanceHeader.Options.UseFont = true;
            this.colNumber.Caption = "სხდომის N";
            this.colNumber.FieldName = "Number";
            this.colNumber.Name = "colNumber";
            this.colNumber.OptionsColumn.AllowEdit = false;
            this.colNumber.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.colNumber.OptionsColumn.AllowMove = false;
            this.colNumber.Visible = true;
            this.colNumber.VisibleIndex = 1;
            this.colNumber.Width = 273;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colName.AppearanceCell.Options.UseFont = true;
            this.colName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colName.AppearanceHeader.Options.UseFont = true;
            this.colName.Caption = "დასახელება";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colName.OptionsColumn.AllowMove = false;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 2;
            this.colName.Width = 273;
            // 
            // colPath
            // 
            this.colPath.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPath.AppearanceCell.Options.UseFont = true;
            this.colPath.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPath.AppearanceHeader.Options.UseFont = true;
            this.colPath.Caption = "მისამართი";
            this.colPath.FieldName = "Path";
            this.colPath.Name = "colPath";
            this.colPath.OptionsColumn.AllowEdit = false;
            this.colPath.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colPath.OptionsColumn.AllowMove = false;
            this.colPath.Visible = true;
            this.colPath.VisibleIndex = 4;
            this.colPath.Width = 305;
            // 
            // colCreatedOn
            // 
            this.colCreatedOn.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colCreatedOn.AppearanceCell.Options.UseFont = true;
            this.colCreatedOn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colCreatedOn.AppearanceHeader.Options.UseFont = true;
            this.colCreatedOn.Caption = "თარიღი";
            this.colCreatedOn.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colCreatedOn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCreatedOn.FieldName = "CreatedOn";
            this.colCreatedOn.Name = "colCreatedOn";
            this.colCreatedOn.OptionsColumn.AllowEdit = false;
            this.colCreatedOn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colCreatedOn.OptionsColumn.AllowMove = false;
            this.colCreatedOn.Visible = true;
            this.colCreatedOn.VisibleIndex = 3;
            this.colCreatedOn.Width = 273;
            // 
            // generateCprBarButtonItem
            // 
            this.generateCprBarButtonItem.Caption = "ფაილის გენერაცია";
            this.generateCprBarButtonItem.Id = 9;
            this.generateCprBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("generateCprBarButtonItem.ImageOptions.Image")));
            this.generateCprBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("generateCprBarButtonItem.ImageOptions.LargeImage")));
            this.generateCprBarButtonItem.Name = "generateCprBarButtonItem";
            this.generateCprBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.generateCprBarButtonItem_ItemClick);
            // 
            // SessionsForm
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.True;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1190, 895);
            this.ControlBox = false;
            this.Controls.Add(this.sessionsGridControl);
            this.Controls.Add(this.ribbon);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SessionsForm";
            this.Ribbon = this.ribbon;
            this.Text = "Court Process Recorder";
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionActionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage MainPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup MainGroup;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGallery;
        private System.Windows.Forms.BindingSource sessionEventBindingSource;
        private System.Windows.Forms.BindingSource participantBindingSource;
        private System.Windows.Forms.BindingSource sessionActionBindingSource;
        private System.Windows.Forms.BindingSource participantBindingSource1;
        private System.Windows.Forms.BindingSource audioDeviceBindingSource;
        private DevExpress.XtraBars.BarButtonItem newSession;
        private DevExpress.XtraBars.BarButtonItem openSession;
        private DevExpress.XtraGrid.GridControl sessionsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView sessionsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colPath;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedOn;
        private DevExpress.XtraBars.BarButtonItem deleteSession;
        private Ookii.Dialogs.VistaFolderBrowserDialog openSessionFolderBrowserDialog;
        private System.Windows.Forms.BindingSource sessionBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSigned;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit;
        private DevExpress.XtraBars.BarButtonItem writeToDiscBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup writeToDiscRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem selectItemsBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem addToContainerButton;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup containerGroup;
        private DevExpress.XtraBars.BarEditItem fromDateBarEditItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraBars.BarEditItem toDateBarEditItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup filterRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem generateCprBarButtonItem;
    }
}