using CourtProcessRecorder.CustomRibbonControls;
using CourtProcessRecorder.UserControls;

namespace CourtProcessRecorder
{
    partial class Default
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Default));
            this.ribbon = new CourtProcessRecorder.CustomRibbonControls.CustomRibbonControl();
            this.skinRibbonGallery = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.configuration = new DevExpress.XtraBars.BarButtonItem();
            this.templateEditBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.parametersPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.MainGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.SkinGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.sessionEventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sessionActionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.participantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.participantBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.audioDeviceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionActionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ApplicationIcon = global::CourtProcessRecorder.Properties.Resources.Greater_coat_of_arms_of_Georgia_svg;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.skinRibbonGallery,
            this.configuration,
            this.templateEditBarButtonItem});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 2;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.parametersPage});
            this.ribbon.Size = new System.Drawing.Size(1190, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // skinRibbonGallery
            // 
            this.skinRibbonGallery.Caption = "Skins";
            this.skinRibbonGallery.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            // 
            // 
            // 
            this.skinRibbonGallery.Gallery.ItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.skinRibbonGallery_Gallery_ItemClick);
            this.skinRibbonGallery.Id = 1;
            this.skinRibbonGallery.Name = "skinRibbonGallery";
            // 
            // configuration
            // 
            this.configuration.Caption = "კონფიგურაცია";
            this.configuration.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.configuration.Glyph = ((System.Drawing.Image)(resources.GetObject("configuration.Glyph")));
            this.configuration.Id = 2;
            this.configuration.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("configuration.LargeGlyph")));
            this.configuration.Name = "configuration";
            this.configuration.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.configuration_ItemClick);
            // 
            // templateEditBarButtonItem
            // 
            this.templateEditBarButtonItem.Caption = "შაბლონების რედაქტირება";
            this.templateEditBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.templateEditBarButtonItem.Glyph = ((System.Drawing.Image)(resources.GetObject("templateEditBarButtonItem.Glyph")));
            this.templateEditBarButtonItem.Id = 1;
            this.templateEditBarButtonItem.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("templateEditBarButtonItem.LargeGlyph")));
            this.templateEditBarButtonItem.Name = "templateEditBarButtonItem";
            this.templateEditBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.templateEditBarButtonItem_ItemClick);
            // 
            // parametersPage
            // 
            this.parametersPage.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.parametersPage.Appearance.Options.UseFont = true;
            this.parametersPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.MainGroup,
            this.SkinGroup});
            this.parametersPage.Name = "parametersPage";
            this.parametersPage.Text = "პარამეტრები";
            // 
            // MainGroup
            // 
            this.MainGroup.ItemLinks.Add(this.templateEditBarButtonItem);
            this.MainGroup.ItemLinks.Add(this.configuration);
            this.MainGroup.Name = "MainGroup";
            this.MainGroup.Text = "ძირითადი";
            // 
            // SkinGroup
            // 
            this.SkinGroup.ItemLinks.Add(this.skinRibbonGallery);
            this.SkinGroup.Name = "SkinGroup";
            this.SkinGroup.Text = "Skins";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 868);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1190, 31);
            // 
            // sessionEventBindingSource
            // 
            this.sessionEventBindingSource.DataSource = typeof(JsonDatabaseModels.SessionEvent);
            // 
            // sessionActionBindingSource
            // 
            this.sessionActionBindingSource.DataSource = typeof(JsonDatabaseModels.SessionAction);
            // 
            // participantBindingSource
            // 
            this.participantBindingSource.DataSource = typeof(JsonDatabaseModels.Participant);
            // 
            // participantBindingSource1
            // 
            this.participantBindingSource1.DataSource = typeof(JsonDatabaseModels.Participant);
            // 
            // audioDeviceBindingSource
            // 
            this.audioDeviceBindingSource.DataSource = typeof(RecordProvider.AudioDevice);
            // 
            // Default
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.True;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1190, 899);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Default";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Court Process Recorder";
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionActionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomRibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage parametersPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup MainGroup;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGallery;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup SkinGroup;
        private DevExpress.XtraBars.BarButtonItem configuration;
        private System.Windows.Forms.BindingSource sessionEventBindingSource;
        private System.Windows.Forms.BindingSource participantBindingSource;
        private System.Windows.Forms.BindingSource sessionActionBindingSource;
        private System.Windows.Forms.BindingSource participantBindingSource1;
        private System.Windows.Forms.BindingSource audioDeviceBindingSource;
        private DevExpress.XtraBars.BarButtonItem templateEditBarButtonItem;
    }
}