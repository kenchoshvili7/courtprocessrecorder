﻿using CourtProcessRecorder.CustomRibbonControls;
using CourtProcessRecorder.UserControls;

namespace CourtProcessRecorder
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ribbon = new CourtProcessRecorder.CustomRibbonControls.CustomRibbonControl();
            this.skinRibbonGallery = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.templatesLookupEdit = new DevExpress.XtraBars.BarEditItem();
            this.templatesRepositoryItemLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sessionTemplateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.addParticipantBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.deleteParticipantBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.deleteSessionActionBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.writeToDiscBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.printBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.signBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.secondsBeforeBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.secondsAfterBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.addSessionActionBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.closeBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.questionAnswerbarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.sessionTemplateTypeBarEditItem = new DevExpress.XtraBars.BarEditItem();
            this.sessionTemplateTypeImageComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.generateCprBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.MainPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.MainGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.participantRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.sessionEventRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.actionsRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.exitRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemTimeSpanEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sessionEventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.actionsTreeList = new DevExpress.XtraTreeList.TreeList();
            this.colDescription = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colUId1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colCreatedOn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.sessionActionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.participantsGridControl = new DevExpress.XtraGrid.GridControl();
            this.participantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.participantsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParticipantDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.participantBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.recorderUserControl = new CourtProcessRecorder.UserControls.RecorderUserControl();
            this.playerUserControl = new CourtProcessPlayer.PlayerUserControl();
            this.sessionEventUserControl = new CourtProcessPlayer.SessionEventUserControl();
            this.mediaControlPanel = new DevExpress.XtraEditors.PanelControl();
            this.mediaControllerButtonsPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.seekSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.fastForwardImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.seekTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.playbackSpeedLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.playbackSpeedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.playbackDeviceLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.audioDeviceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.currentTimelbl = new DevExpress.XtraEditors.LabelControl();
            this.playPausebtn = new DevExpress.XtraEditors.SimpleButton();
            this.playPauseImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.stopbtn = new DevExpress.XtraEditors.SimpleButton();
            this.stopImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.recordbtn = new DevExpress.XtraEditors.SimpleButton();
            this.recordImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.recorderPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.sessionEventPanel = new DevExpress.XtraEditors.PanelControl();
            this.descriptionPanel = new DevExpress.XtraEditors.PanelControl();
            this.mainPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.mainSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.topSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.bottomSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.participantAudioFilesContainersXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.participantsTab = new DevExpress.XtraTab.XtraTabPage();
            this.audioFilesContainersTab = new DevExpress.XtraTab.XtraTabPage();
            this.audioFilesContainersUserControl1 = new CourtProcessPlayer.AudioFilesContainersUserControl();
            this.paragraphsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templatesRepositoryItemLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTemplateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTemplateTypeImageComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeSpanEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.actionsTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionActionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaControlPanel)).BeginInit();
            this.mediaControlPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mediaControllerButtonsPanelControl)).BeginInit();
            this.mediaControllerButtonsPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fastForwardImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seekTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackSpeedLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackSpeedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackDeviceLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playPauseImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recordImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recorderPanelControl)).BeginInit();
            this.recorderPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventPanel)).BeginInit();
            this.sessionEventPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.descriptionPanel)).BeginInit();
            this.descriptionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPanelControl)).BeginInit();
            this.mainPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainerControl)).BeginInit();
            this.mainSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.topSplitContainerControl)).BeginInit();
            this.topSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottomSplitContainerControl)).BeginInit();
            this.bottomSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.participantAudioFilesContainersXtraTabControl)).BeginInit();
            this.participantAudioFilesContainersXtraTabControl.SuspendLayout();
            this.participantsTab.SuspendLayout();
            this.audioFilesContainersTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.paragraphsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.repositoryItemTextEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTextEdit1.EditFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTextEdit1.Mask.EditMask = "dd.MM.yyyy HH:mm:ss";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.skinRibbonGallery,
            this.templatesLookupEdit,
            this.addParticipantBarButtonItem,
            this.deleteParticipantBarButtonItem,
            this.deleteSessionActionBarButtonItem,
            this.writeToDiscBarButtonItem,
            this.printBarButtonItem,
            this.signBarButtonItem,
            this.secondsBeforeBarButtonItem,
            this.secondsAfterBarButtonItem,
            this.addSessionActionBarButtonItem,
            this.closeBarButtonItem,
            this.questionAnswerbarButtonItem,
            this.sessionTemplateTypeBarEditItem,
            this.generateCprBarButtonItem});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 19;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.MainPage});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeSpanEdit1,
            this.templatesRepositoryItemLookUpEdit,
            this.repositoryItemGridLookUpEdit1,
            this.sessionTemplateTypeImageComboBox});
            this.ribbon.Size = new System.Drawing.Size(1190, 146);
            // 
            // skinRibbonGallery
            // 
            this.skinRibbonGallery.Caption = "Skins";
            this.skinRibbonGallery.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.skinRibbonGallery.Id = 1;
            this.skinRibbonGallery.Name = "skinRibbonGallery";
            // 
            // templatesLookupEdit
            // 
            this.templatesLookupEdit.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.templatesLookupEdit.Edit = this.templatesRepositoryItemLookUpEdit;
            this.templatesLookupEdit.EditWidth = 350;
            this.templatesLookupEdit.Id = 2;
            this.templatesLookupEdit.Name = "templatesLookupEdit";
            // 
            // templatesRepositoryItemLookUpEdit
            // 
            this.templatesRepositoryItemLookUpEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.templatesRepositoryItemLookUpEdit.Appearance.Options.UseFont = true;
            this.templatesRepositoryItemLookUpEdit.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.templatesRepositoryItemLookUpEdit.AppearanceDropDown.Options.UseFont = true;
            this.templatesRepositoryItemLookUpEdit.AutoHeight = false;
            this.templatesRepositoryItemLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.templatesRepositoryItemLookUpEdit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.templatesRepositoryItemLookUpEdit.DataSource = this.sessionTemplateBindingSource;
            this.templatesRepositoryItemLookUpEdit.DisplayMember = "Name";
            this.templatesRepositoryItemLookUpEdit.DropDownRows = 10;
            this.templatesRepositoryItemLookUpEdit.Name = "templatesRepositoryItemLookUpEdit";
            this.templatesRepositoryItemLookUpEdit.ShowHeader = false;
            // 
            // sessionTemplateBindingSource
            // 
            this.sessionTemplateBindingSource.DataSource = typeof(JsonDatabaseModels.SessionTemplate);
            // 
            // addParticipantBarButtonItem
            // 
            this.addParticipantBarButtonItem.Caption = "მონაწილეების განსაზღვრა";
            this.addParticipantBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.addParticipantBarButtonItem.Id = 1;
            this.addParticipantBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("addParticipantBarButtonItem.ImageOptions.Image")));
            this.addParticipantBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("addParticipantBarButtonItem.ImageOptions.LargeImage")));
            this.addParticipantBarButtonItem.Name = "addParticipantBarButtonItem";
            this.addParticipantBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addParticipantBarButtonItem_ItemClick);
            // 
            // deleteParticipantBarButtonItem
            // 
            this.deleteParticipantBarButtonItem.Caption = "მონაწილის წაშლა";
            this.deleteParticipantBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.deleteParticipantBarButtonItem.Id = 2;
            this.deleteParticipantBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("deleteParticipantBarButtonItem.ImageOptions.Image")));
            this.deleteParticipantBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("deleteParticipantBarButtonItem.ImageOptions.LargeImage")));
            this.deleteParticipantBarButtonItem.Name = "deleteParticipantBarButtonItem";
            this.deleteParticipantBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteParticipantBarButtonItem_ItemClick);
            // 
            // deleteSessionActionBarButtonItem
            // 
            this.deleteSessionActionBarButtonItem.Caption = "მოვლენის წაშლა";
            this.deleteSessionActionBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.deleteSessionActionBarButtonItem.Id = 4;
            this.deleteSessionActionBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("deleteSessionActionBarButtonItem.ImageOptions.Image")));
            this.deleteSessionActionBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("deleteSessionActionBarButtonItem.ImageOptions.LargeImage")));
            this.deleteSessionActionBarButtonItem.Name = "deleteSessionActionBarButtonItem";
            this.deleteSessionActionBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteSessionActionBarButtonItem_ItemClick);
            // 
            // writeToDiscBarButtonItem
            // 
            this.writeToDiscBarButtonItem.Caption = "დისკზე ჩაწერა";
            this.writeToDiscBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.writeToDiscBarButtonItem.Id = 6;
            this.writeToDiscBarButtonItem.ImageOptions.LargeImage = global::CourtProcessRecorder.Properties.Resources.burn;
            this.writeToDiscBarButtonItem.Name = "writeToDiscBarButtonItem";
            this.writeToDiscBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.writeToDiscBarButtonItem_ItemClick);
            // 
            // printBarButtonItem
            // 
            this.printBarButtonItem.Caption = "სხდომის ოქმი";
            this.printBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.printBarButtonItem.Id = 7;
            this.printBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("printBarButtonItem.ImageOptions.Image")));
            this.printBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("printBarButtonItem.ImageOptions.LargeImage")));
            this.printBarButtonItem.Name = "printBarButtonItem";
            this.printBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.printBarButtonItem_ItemClick);
            // 
            // signBarButtonItem
            // 
            this.signBarButtonItem.Caption = "ხელმოწერა";
            this.signBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.signBarButtonItem.Id = 8;
            this.signBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("signBarButtonItem.ImageOptions.Image")));
            this.signBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("signBarButtonItem.ImageOptions.LargeImage")));
            this.signBarButtonItem.Name = "signBarButtonItem";
            this.signBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.signBarButtonItem_ItemClick);
            // 
            // secondsBeforeBarButtonItem
            // 
            this.secondsBeforeBarButtonItem.Caption = "1 წამით უკან";
            this.secondsBeforeBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.secondsBeforeBarButtonItem.Id = 9;
            this.secondsBeforeBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("secondsBeforeBarButtonItem.ImageOptions.Image")));
            this.secondsBeforeBarButtonItem.Name = "secondsBeforeBarButtonItem";
            this.secondsBeforeBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.secondsBeforeBarButtonItem_ItemClick);
            // 
            // secondsAfterBarButtonItem
            // 
            this.secondsAfterBarButtonItem.Caption = "1 წამით წინ";
            this.secondsAfterBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.secondsAfterBarButtonItem.Id = 10;
            this.secondsAfterBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("secondsAfterBarButtonItem.ImageOptions.Image")));
            this.secondsAfterBarButtonItem.Name = "secondsAfterBarButtonItem";
            this.secondsAfterBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.secondsAfterBarButtonItem_ItemClick);
            // 
            // addSessionActionBarButtonItem
            // 
            this.addSessionActionBarButtonItem.Caption = "მოვლენის დამატება";
            this.addSessionActionBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.addSessionActionBarButtonItem.Id = 11;
            this.addSessionActionBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("addSessionActionBarButtonItem.ImageOptions.Image")));
            this.addSessionActionBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("addSessionActionBarButtonItem.ImageOptions.LargeImage")));
            this.addSessionActionBarButtonItem.Name = "addSessionActionBarButtonItem";
            this.addSessionActionBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addSessionActionBarButtonItem_ItemClick);
            // 
            // closeBarButtonItem
            // 
            this.closeBarButtonItem.Caption = "სხდომის დახურვა";
            this.closeBarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.closeBarButtonItem.Id = 13;
            this.closeBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("closeBarButtonItem.ImageOptions.Image")));
            this.closeBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("closeBarButtonItem.ImageOptions.LargeImage")));
            this.closeBarButtonItem.Name = "closeBarButtonItem";
            this.closeBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.closeBarButtonItem_ItemClick);
            // 
            // questionAnswerbarButtonItem
            // 
            this.questionAnswerbarButtonItem.Caption = "კითხვა/პასუხი";
            this.questionAnswerbarButtonItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.questionAnswerbarButtonItem.Id = 15;
            this.questionAnswerbarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("questionAnswerbarButtonItem.ImageOptions.Image")));
            this.questionAnswerbarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("questionAnswerbarButtonItem.ImageOptions.LargeImage")));
            this.questionAnswerbarButtonItem.Name = "questionAnswerbarButtonItem";
            this.questionAnswerbarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.questionAnswerbarButtonItem_ItemClick);
            // 
            // sessionTemplateTypeBarEditItem
            // 
            this.sessionTemplateTypeBarEditItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.sessionTemplateTypeBarEditItem.Edit = this.sessionTemplateTypeImageComboBox;
            this.sessionTemplateTypeBarEditItem.EditWidth = 350;
            this.sessionTemplateTypeBarEditItem.Id = 17;
            this.sessionTemplateTypeBarEditItem.Name = "sessionTemplateTypeBarEditItem";
            // 
            // sessionTemplateTypeImageComboBox
            // 
            this.sessionTemplateTypeImageComboBox.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionTemplateTypeImageComboBox.Appearance.Options.UseFont = true;
            this.sessionTemplateTypeImageComboBox.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionTemplateTypeImageComboBox.AppearanceDropDown.Options.UseFont = true;
            this.sessionTemplateTypeImageComboBox.AutoHeight = false;
            this.sessionTemplateTypeImageComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sessionTemplateTypeImageComboBox.Name = "sessionTemplateTypeImageComboBox";
            // 
            // generateCprBarButtonItem
            // 
            this.generateCprBarButtonItem.Caption = "ფაილის გენერაცია";
            this.generateCprBarButtonItem.Id = 18;
            this.generateCprBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("generateCprBarButtonItem.ImageOptions.Image")));
            this.generateCprBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("generateCprBarButtonItem.ImageOptions.LargeImage")));
            this.generateCprBarButtonItem.Name = "generateCprBarButtonItem";
            this.generateCprBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.generateCprBarButtonItem_ItemClick);
            // 
            // MainPage
            // 
            this.MainPage.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.MainPage.Appearance.Options.UseFont = true;
            this.MainPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.MainGroup,
            this.participantRibbonPageGroup,
            this.sessionEventRibbonPageGroup,
            this.actionsRibbonPageGroup,
            this.exitRibbonPageGroup});
            this.MainPage.Name = "MainPage";
            this.MainPage.Text = "მთავარი";
            // 
            // MainGroup
            // 
            this.MainGroup.ItemLinks.Add(this.sessionTemplateTypeBarEditItem);
            this.MainGroup.ItemLinks.Add(this.templatesLookupEdit);
            this.MainGroup.Name = "MainGroup";
            this.MainGroup.ShowCaptionButton = false;
            this.MainGroup.Text = "შაბლონები";
            // 
            // participantRibbonPageGroup
            // 
            this.participantRibbonPageGroup.AllowTextClipping = false;
            this.participantRibbonPageGroup.ItemLinks.Add(this.addParticipantBarButtonItem);
            this.participantRibbonPageGroup.Name = "participantRibbonPageGroup";
            this.participantRibbonPageGroup.ShowCaptionButton = false;
            this.participantRibbonPageGroup.Text = "მონაწილე";
            // 
            // sessionEventRibbonPageGroup
            // 
            this.sessionEventRibbonPageGroup.ItemLinks.Add(this.addSessionActionBarButtonItem);
            this.sessionEventRibbonPageGroup.ItemLinks.Add(this.deleteSessionActionBarButtonItem);
            this.sessionEventRibbonPageGroup.ItemLinks.Add(this.secondsBeforeBarButtonItem);
            this.sessionEventRibbonPageGroup.ItemLinks.Add(this.secondsAfterBarButtonItem);
            this.sessionEventRibbonPageGroup.ItemLinks.Add(this.questionAnswerbarButtonItem);
            this.sessionEventRibbonPageGroup.Name = "sessionEventRibbonPageGroup";
            this.sessionEventRibbonPageGroup.ShowCaptionButton = false;
            this.sessionEventRibbonPageGroup.Text = "მოვლენა";
            // 
            // actionsRibbonPageGroup
            // 
            this.actionsRibbonPageGroup.ItemLinks.Add(this.writeToDiscBarButtonItem);
            this.actionsRibbonPageGroup.ItemLinks.Add(this.printBarButtonItem);
            this.actionsRibbonPageGroup.ItemLinks.Add(this.signBarButtonItem);
            this.actionsRibbonPageGroup.ItemLinks.Add(this.generateCprBarButtonItem);
            this.actionsRibbonPageGroup.Name = "actionsRibbonPageGroup";
            this.actionsRibbonPageGroup.ShowCaptionButton = false;
            this.actionsRibbonPageGroup.Text = "მოქმედებები";
            // 
            // exitRibbonPageGroup
            // 
            this.exitRibbonPageGroup.ItemLinks.Add(this.closeBarButtonItem);
            this.exitRibbonPageGroup.Name = "exitRibbonPageGroup";
            this.exitRibbonPageGroup.ShowCaptionButton = false;
            this.exitRibbonPageGroup.Tag = "AlignRight";
            this.exitRibbonPageGroup.Text = "დახურვა";
            // 
            // repositoryItemTimeSpanEdit1
            // 
            this.repositoryItemTimeSpanEdit1.AutoHeight = false;
            this.repositoryItemTimeSpanEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeSpanEdit1.Mask.EditMask = "dd.HH:mm:ss";
            this.repositoryItemTimeSpanEdit1.Name = "repositoryItemTimeSpanEdit1";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.PopupView = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // sessionEventBindingSource
            // 
            this.sessionEventBindingSource.DataSource = typeof(JsonDatabaseModels.SessionEvent);
            // 
            // actionsTreeList
            // 
            this.actionsTreeList.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.actionsTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colDescription,
            this.colUId1,
            this.colCreatedOn1});
            this.actionsTreeList.DataSource = this.sessionActionBindingSource;
            this.actionsTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionsTreeList.Location = new System.Drawing.Point(0, 0);
            this.actionsTreeList.Name = "actionsTreeList";
            this.actionsTreeList.OptionsCustomization.AllowColumnMoving = false;
            this.actionsTreeList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.actionsTreeList.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.actionsTreeList.Size = new System.Drawing.Size(259, 370);
            this.actionsTreeList.TabIndex = 5;
            // 
            // colDescription
            // 
            this.colDescription.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colDescription.AppearanceCell.Options.UseFont = true;
            this.colDescription.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colDescription.AppearanceHeader.Options.UseFont = true;
            this.colDescription.Caption = "შაბლონები";
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 300;
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 300;
            // 
            // colUId1
            // 
            this.colUId1.FieldName = "UId";
            this.colUId1.Name = "colUId1";
            this.colUId1.Width = 124;
            // 
            // colCreatedOn1
            // 
            this.colCreatedOn1.FieldName = "CreatedOn";
            this.colCreatedOn1.Name = "colCreatedOn1";
            this.colCreatedOn1.Width = 123;
            // 
            // sessionActionBindingSource
            // 
            this.sessionActionBindingSource.DataSource = typeof(JsonDatabaseModels.SessionAction);
            // 
            // participantsGridControl
            // 
            this.participantsGridControl.DataSource = this.participantBindingSource;
            this.participantsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.participantsGridControl.Location = new System.Drawing.Point(0, 0);
            this.participantsGridControl.MainView = this.participantsGridView;
            this.participantsGridControl.MenuManager = this.ribbon;
            this.participantsGridControl.Name = "participantsGridControl";
            this.participantsGridControl.Size = new System.Drawing.Size(254, 342);
            this.participantsGridControl.TabIndex = 6;
            this.participantsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.participantsGridView});
            // 
            // participantBindingSource
            // 
            this.participantBindingSource.DataSource = typeof(JsonDatabaseModels.Participant);
            // 
            // participantsGridView
            // 
            this.participantsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPosition,
            this.colName,
            this.colParticipantDescription,
            this.colUId,
            this.colCreatedOn});
            this.participantsGridView.GridControl = this.participantsGridControl;
            this.participantsGridView.Name = "participantsGridView";
            this.participantsGridView.OptionsCustomization.AllowColumnMoving = false;
            this.participantsGridView.OptionsCustomization.AllowColumnResizing = false;
            this.participantsGridView.OptionsCustomization.AllowGroup = false;
            this.participantsGridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.participantsGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.participantsGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.participantsGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colPosition
            // 
            this.colPosition.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPosition.AppearanceCell.Options.UseFont = true;
            this.colPosition.FieldName = "Position";
            this.colPosition.Name = "colPosition";
            this.colPosition.Width = 74;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colName.AppearanceCell.Options.UseFont = true;
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.Width = 74;
            // 
            // colParticipantDescription
            // 
            this.colParticipantDescription.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colParticipantDescription.AppearanceCell.Options.UseFont = true;
            this.colParticipantDescription.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colParticipantDescription.AppearanceHeader.Options.UseFont = true;
            this.colParticipantDescription.Caption = "მონაწილე";
            this.colParticipantDescription.FieldName = "ParticipantDescription";
            this.colParticipantDescription.Name = "colParticipantDescription";
            this.colParticipantDescription.OptionsColumn.AllowEdit = false;
            this.colParticipantDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colParticipantDescription.OptionsColumn.ReadOnly = true;
            this.colParticipantDescription.Visible = true;
            this.colParticipantDescription.VisibleIndex = 0;
            this.colParticipantDescription.Width = 128;
            // 
            // colUId
            // 
            this.colUId.FieldName = "UId";
            this.colUId.Name = "colUId";
            this.colUId.Width = 47;
            // 
            // colCreatedOn
            // 
            this.colCreatedOn.FieldName = "CreatedOn";
            this.colCreatedOn.Name = "colCreatedOn";
            this.colCreatedOn.Width = 48;
            // 
            // participantBindingSource1
            // 
            this.participantBindingSource1.DataSource = typeof(JsonDatabaseModels.Participant);
            // 
            // recorderUserControl
            // 
            this.recorderUserControl.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.recorderUserControl.Appearance.Options.UseBackColor = true;
            this.recorderUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recorderUserControl.Location = new System.Drawing.Point(0, 0);
            this.recorderUserControl.Name = "recorderUserControl";
            this.recorderUserControl.Size = new System.Drawing.Size(921, 332);
            this.recorderUserControl.TabIndex = 12;
            // 
            // playerUserControl
            // 
            this.playerUserControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playerUserControl.Location = new System.Drawing.Point(0, 31);
            this.playerUserControl.Name = "playerUserControl";
            this.playerUserControl.Size = new System.Drawing.Size(925, 343);
            this.playerUserControl.TabIndex = 18;
            // 
            // sessionEventUserControl
            // 
            this.sessionEventUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sessionEventUserControl.Location = new System.Drawing.Point(0, 0);
            this.sessionEventUserControl.Name = "sessionEventUserControl";
            this.sessionEventUserControl.Size = new System.Drawing.Size(926, 370);
            this.sessionEventUserControl.TabIndex = 20;
            // 
            // mediaControlPanel
            // 
            this.mediaControlPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.mediaControlPanel.Controls.Add(this.mediaControllerButtonsPanelControl);
            this.mediaControlPanel.Controls.Add(this.recorderPanelControl);
            this.mediaControlPanel.Controls.Add(this.playerUserControl);
            this.mediaControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mediaControlPanel.Location = new System.Drawing.Point(0, 0);
            this.mediaControlPanel.Name = "mediaControlPanel";
            this.mediaControlPanel.Size = new System.Drawing.Size(925, 374);
            this.mediaControlPanel.TabIndex = 23;
            // 
            // mediaControllerButtonsPanelControl
            // 
            this.mediaControllerButtonsPanelControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mediaControllerButtonsPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.mediaControllerButtonsPanelControl.Controls.Add(this.seekSimpleButton);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.seekTextEdit);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.playbackSpeedLookUpEdit);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.playbackDeviceLookUpEdit);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.currentTimelbl);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.playPausebtn);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.stopbtn);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.recordbtn);
            this.mediaControllerButtonsPanelControl.Location = new System.Drawing.Point(4, 2);
            this.mediaControllerButtonsPanelControl.Name = "mediaControllerButtonsPanelControl";
            this.mediaControllerButtonsPanelControl.Size = new System.Drawing.Size(921, 29);
            this.mediaControllerButtonsPanelControl.TabIndex = 19;
            // 
            // seekSimpleButton
            // 
            this.seekSimpleButton.ImageOptions.ImageIndex = 0;
            this.seekSimpleButton.ImageOptions.ImageList = this.fastForwardImageCollection;
            this.seekSimpleButton.Location = new System.Drawing.Point(140, 2);
            this.seekSimpleButton.Name = "seekSimpleButton";
            this.seekSimpleButton.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.seekSimpleButton.Size = new System.Drawing.Size(80, 25);
            this.seekSimpleButton.TabIndex = 22;
            this.seekSimpleButton.Text = "გადახვევა";
            this.seekSimpleButton.Click += new System.EventHandler(this.seekSimpleButton_Click);
            // 
            // fastForwardImageCollection
            // 
            this.fastForwardImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("fastForwardImageCollection.ImageStream")));
            this.fastForwardImageCollection.Images.SetKeyName(0, "fastforward.png");
            // 
            // seekTextEdit
            // 
            this.seekTextEdit.EditValue = "";
            this.seekTextEdit.Location = new System.Drawing.Point(5, 4);
            this.seekTextEdit.MenuManager = this.ribbon;
            this.seekTextEdit.Name = "seekTextEdit";
            this.seekTextEdit.Size = new System.Drawing.Size(130, 20);
            this.seekTextEdit.TabIndex = 21;
            this.seekTextEdit.ToolTip = "გადახვევის დრო";
            // 
            // playbackSpeedLookUpEdit
            // 
            this.playbackSpeedLookUpEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackSpeedLookUpEdit.Location = new System.Drawing.Point(546, 4);
            this.playbackSpeedLookUpEdit.Name = "playbackSpeedLookUpEdit";
            this.playbackSpeedLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.playbackSpeedLookUpEdit.Properties.Appearance.Options.UseFont = true;
            this.playbackSpeedLookUpEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.playbackSpeedLookUpEdit.Properties.AppearanceDropDown.Options.UseFont = true;
            this.playbackSpeedLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.playbackSpeedLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.playbackSpeedLookUpEdit.Properties.DataSource = this.playbackSpeedBindingSource;
            this.playbackSpeedLookUpEdit.Properties.DisplayMember = "Description";
            this.playbackSpeedLookUpEdit.Properties.DropDownRows = 4;
            this.playbackSpeedLookUpEdit.Properties.ShowHeader = false;
            this.playbackSpeedLookUpEdit.Properties.ValueMember = "SampleRate";
            this.playbackSpeedLookUpEdit.Size = new System.Drawing.Size(100, 22);
            this.playbackSpeedLookUpEdit.TabIndex = 20;
            this.playbackSpeedLookUpEdit.ToolTip = "მოსმენის სიჩქარე";
            // 
            // playbackSpeedBindingSource
            // 
            this.playbackSpeedBindingSource.DataSource = typeof(RecordProvider.HelperClasses.PlaybackSpeed);
            // 
            // playbackDeviceLookUpEdit
            // 
            this.playbackDeviceLookUpEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.playbackDeviceLookUpEdit.Location = new System.Drawing.Point(235, 4);
            this.playbackDeviceLookUpEdit.MenuManager = this.ribbon;
            this.playbackDeviceLookUpEdit.Name = "playbackDeviceLookUpEdit";
            this.playbackDeviceLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.playbackDeviceLookUpEdit.Properties.Appearance.Options.UseFont = true;
            this.playbackDeviceLookUpEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.playbackDeviceLookUpEdit.Properties.AppearanceDropDown.Options.UseFont = true;
            this.playbackDeviceLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.playbackDeviceLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DeviceName", "Device Name", 72, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.playbackDeviceLookUpEdit.Properties.DataSource = this.audioDeviceBindingSource;
            this.playbackDeviceLookUpEdit.Properties.DisplayMember = "DeviceName";
            this.playbackDeviceLookUpEdit.Properties.DropDownRows = 3;
            this.playbackDeviceLookUpEdit.Properties.ShowHeader = false;
            this.playbackDeviceLookUpEdit.Properties.ValueMember = "DeviceId";
            this.playbackDeviceLookUpEdit.Size = new System.Drawing.Size(108, 22);
            this.playbackDeviceLookUpEdit.TabIndex = 20;
            this.playbackDeviceLookUpEdit.ToolTip = "მოსასმენი მოწყობილობა";
            // 
            // audioDeviceBindingSource
            // 
            this.audioDeviceBindingSource.DataSource = typeof(RecordProvider.AudioDevice);
            // 
            // currentTimelbl
            // 
            this.currentTimelbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.currentTimelbl.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.currentTimelbl.Appearance.Options.UseFont = true;
            this.currentTimelbl.Location = new System.Drawing.Point(410, 4);
            this.currentTimelbl.Name = "currentTimelbl";
            this.currentTimelbl.Size = new System.Drawing.Size(72, 19);
            this.currentTimelbl.TabIndex = 19;
            this.currentTimelbl.Text = "00:00:00";
            // 
            // playPausebtn
            // 
            this.playPausebtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playPausebtn.ImageOptions.ImageIndex = 0;
            this.playPausebtn.ImageOptions.ImageList = this.playPauseImageCollection;
            this.playPausebtn.Location = new System.Drawing.Point(656, 2);
            this.playPausebtn.Name = "playPausebtn";
            this.playPausebtn.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.playPausebtn.Size = new System.Drawing.Size(80, 25);
            this.playPausebtn.TabIndex = 18;
            this.playPausebtn.Text = "მოსმენა";
            this.playPausebtn.Click += new System.EventHandler(this.playPausebtn_Click);
            // 
            // playPauseImageCollection
            // 
            this.playPauseImageCollection.ImageSize = new System.Drawing.Size(20, 20);
            this.playPauseImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("playPauseImageCollection.ImageStream")));
            this.playPauseImageCollection.Images.SetKeyName(0, "play.png");
            this.playPauseImageCollection.Images.SetKeyName(1, "pause.png");
            // 
            // stopbtn
            // 
            this.stopbtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stopbtn.ImageOptions.ImageIndex = 0;
            this.stopbtn.ImageOptions.ImageList = this.stopImageCollection;
            this.stopbtn.Location = new System.Drawing.Point(746, 2);
            this.stopbtn.Name = "stopbtn";
            this.stopbtn.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.stopbtn.Size = new System.Drawing.Size(80, 25);
            this.stopbtn.TabIndex = 17;
            this.stopbtn.Text = "გაჩერება";
            this.stopbtn.Click += new System.EventHandler(this.stopbtn_Click);
            // 
            // stopImageCollection
            // 
            this.stopImageCollection.ImageSize = new System.Drawing.Size(20, 20);
            this.stopImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("stopImageCollection.ImageStream")));
            this.stopImageCollection.Images.SetKeyName(0, "stop1.png");
            // 
            // recordbtn
            // 
            this.recordbtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.recordbtn.ImageOptions.ImageIndex = 0;
            this.recordbtn.ImageOptions.ImageList = this.recordImageCollection;
            this.recordbtn.Location = new System.Drawing.Point(836, 2);
            this.recordbtn.Name = "recordbtn";
            this.recordbtn.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.recordbtn.Size = new System.Drawing.Size(80, 25);
            this.recordbtn.TabIndex = 16;
            this.recordbtn.Text = "ჩაწერა";
            this.recordbtn.Click += new System.EventHandler(this.recordbtn_Click);
            // 
            // recordImageCollection
            // 
            this.recordImageCollection.ImageSize = new System.Drawing.Size(20, 20);
            this.recordImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("recordImageCollection.ImageStream")));
            this.recordImageCollection.Images.SetKeyName(0, "startrecord2.png");
            this.recordImageCollection.Images.SetKeyName(1, "stoprecord.png");
            // 
            // recorderPanelControl
            // 
            this.recorderPanelControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.recorderPanelControl.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.recorderPanelControl.Appearance.Options.UseBackColor = true;
            this.recorderPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.recorderPanelControl.Controls.Add(this.recorderUserControl);
            this.recorderPanelControl.Location = new System.Drawing.Point(4, 37);
            this.recorderPanelControl.Name = "recorderPanelControl";
            this.recorderPanelControl.Size = new System.Drawing.Size(921, 332);
            this.recorderPanelControl.TabIndex = 20;
            // 
            // sessionEventPanel
            // 
            this.sessionEventPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.sessionEventPanel.Controls.Add(this.sessionEventUserControl);
            this.sessionEventPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sessionEventPanel.Location = new System.Drawing.Point(0, 0);
            this.sessionEventPanel.Name = "sessionEventPanel";
            this.sessionEventPanel.Size = new System.Drawing.Size(926, 370);
            this.sessionEventPanel.TabIndex = 22;
            // 
            // descriptionPanel
            // 
            this.descriptionPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.descriptionPanel.Controls.Add(this.actionsTreeList);
            this.descriptionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descriptionPanel.Location = new System.Drawing.Point(0, 0);
            this.descriptionPanel.Name = "descriptionPanel";
            this.descriptionPanel.Size = new System.Drawing.Size(259, 370);
            this.descriptionPanel.TabIndex = 21;
            // 
            // mainPanelControl
            // 
            this.mainPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.mainPanelControl.Controls.Add(this.mainSplitContainerControl);
            this.mainPanelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanelControl.Location = new System.Drawing.Point(0, 146);
            this.mainPanelControl.Name = "mainPanelControl";
            this.mainPanelControl.Size = new System.Drawing.Size(1190, 749);
            this.mainPanelControl.TabIndex = 29;
            // 
            // mainSplitContainerControl
            // 
            this.mainSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainerControl.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.mainSplitContainerControl.Horizontal = false;
            this.mainSplitContainerControl.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainerControl.Name = "mainSplitContainerControl";
            this.mainSplitContainerControl.Panel1.Controls.Add(this.topSplitContainerControl);
            this.mainSplitContainerControl.Panel1.Text = "topSplitterPanel";
            this.mainSplitContainerControl.Panel2.Controls.Add(this.bottomSplitContainerControl);
            this.mainSplitContainerControl.Panel2.Text = "mediaControlPanel";
            this.mainSplitContainerControl.Size = new System.Drawing.Size(1190, 749);
            this.mainSplitContainerControl.SplitterPosition = 374;
            this.mainSplitContainerControl.TabIndex = 29;
            this.mainSplitContainerControl.Text = "splitContainerControl1";
            // 
            // topSplitContainerControl
            // 
            this.topSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topSplitContainerControl.Location = new System.Drawing.Point(0, 0);
            this.topSplitContainerControl.Name = "topSplitContainerControl";
            this.topSplitContainerControl.Panel1.Controls.Add(this.descriptionPanel);
            this.topSplitContainerControl.Panel1.Text = "templatePanel";
            this.topSplitContainerControl.Panel2.Controls.Add(this.sessionEventPanel);
            this.topSplitContainerControl.Panel2.Text = "eventPanel";
            this.topSplitContainerControl.Size = new System.Drawing.Size(1190, 370);
            this.topSplitContainerControl.SplitterPosition = 259;
            this.topSplitContainerControl.TabIndex = 27;
            this.topSplitContainerControl.Text = "splitContainerControl1";
            // 
            // bottomSplitContainerControl
            // 
            this.bottomSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomSplitContainerControl.Location = new System.Drawing.Point(0, 0);
            this.bottomSplitContainerControl.Name = "bottomSplitContainerControl";
            this.bottomSplitContainerControl.Panel1.Controls.Add(this.participantAudioFilesContainersXtraTabControl);
            this.bottomSplitContainerControl.Panel1.Text = "participantAndAudioFilesPanel";
            this.bottomSplitContainerControl.Panel2.Controls.Add(this.mediaControlPanel);
            this.bottomSplitContainerControl.Panel2.Text = "Panel2";
            this.bottomSplitContainerControl.Size = new System.Drawing.Size(1190, 374);
            this.bottomSplitContainerControl.SplitterPosition = 260;
            this.bottomSplitContainerControl.TabIndex = 28;
            this.bottomSplitContainerControl.Text = "splitContainerControl1";
            // 
            // participantAudioFilesContainersXtraTabControl
            // 
            this.participantAudioFilesContainersXtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.participantAudioFilesContainersXtraTabControl.Location = new System.Drawing.Point(0, 0);
            this.participantAudioFilesContainersXtraTabControl.Name = "participantAudioFilesContainersXtraTabControl";
            this.participantAudioFilesContainersXtraTabControl.SelectedTabPage = this.participantsTab;
            this.participantAudioFilesContainersXtraTabControl.Size = new System.Drawing.Size(260, 374);
            this.participantAudioFilesContainersXtraTabControl.TabIndex = 21;
            this.participantAudioFilesContainersXtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.participantsTab,
            this.audioFilesContainersTab});
            // 
            // participantsTab
            // 
            this.participantsTab.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.participantsTab.Appearance.Header.Options.UseFont = true;
            this.participantsTab.Appearance.Header.Options.UseTextOptions = true;
            this.participantsTab.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.participantsTab.Controls.Add(this.participantsGridControl);
            this.participantsTab.Name = "participantsTab";
            this.participantsTab.Size = new System.Drawing.Size(254, 342);
            this.participantsTab.TabPageWidth = 106;
            this.participantsTab.Text = "მონაწილეები";
            // 
            // audioFilesContainersTab
            // 
            this.audioFilesContainersTab.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.audioFilesContainersTab.Appearance.Header.Options.UseFont = true;
            this.audioFilesContainersTab.Appearance.Header.Options.UseTextOptions = true;
            this.audioFilesContainersTab.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.audioFilesContainersTab.Controls.Add(this.audioFilesContainersUserControl1);
            this.audioFilesContainersTab.Name = "audioFilesContainersTab";
            this.audioFilesContainersTab.Size = new System.Drawing.Size(254, 342);
            this.audioFilesContainersTab.TabPageWidth = 107;
            this.audioFilesContainersTab.Text = "დროები";
            // 
            // audioFilesContainersUserControl1
            // 
            this.audioFilesContainersUserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.audioFilesContainersUserControl1.Location = new System.Drawing.Point(0, 0);
            this.audioFilesContainersUserControl1.Name = "audioFilesContainersUserControl1";
            this.audioFilesContainersUserControl1.Size = new System.Drawing.Size(254, 342);
            this.audioFilesContainersUserControl1.TabIndex = 21;
            // 
            // paragraphsBindingSource
            // 
            this.paragraphsBindingSource.DataMember = "Paragraphs";
            this.paragraphsBindingSource.DataSource = this.sessionTemplateBindingSource;
            // 
            // Main
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.True;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1190, 895);
            this.ControlBox = false;
            this.Controls.Add(this.mainPanelControl);
            this.Controls.Add(this.ribbon);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Ribbon = this.ribbon;
            this.Text = "Court Process Recorder";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainMouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templatesRepositoryItemLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTemplateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTemplateTypeImageComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeSpanEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.actionsTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionActionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaControlPanel)).EndInit();
            this.mediaControlPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mediaControllerButtonsPanelControl)).EndInit();
            this.mediaControllerButtonsPanelControl.ResumeLayout(false);
            this.mediaControllerButtonsPanelControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fastForwardImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seekTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackSpeedLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackSpeedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackDeviceLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playPauseImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recordImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recorderPanelControl)).EndInit();
            this.recorderPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventPanel)).EndInit();
            this.sessionEventPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.descriptionPanel)).EndInit();
            this.descriptionPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainPanelControl)).EndInit();
            this.mainPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainerControl)).EndInit();
            this.mainSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.topSplitContainerControl)).EndInit();
            this.topSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bottomSplitContainerControl)).EndInit();
            this.bottomSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.participantAudioFilesContainersXtraTabControl)).EndInit();
            this.participantAudioFilesContainersXtraTabControl.ResumeLayout(false);
            this.participantsTab.ResumeLayout(false);
            this.audioFilesContainersTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.paragraphsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomRibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage MainPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup MainGroup;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGallery;
        private DevExpress.XtraTreeList.TreeList actionsTreeList;
        private DevExpress.XtraGrid.GridControl participantsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView participantsGridView;
        private System.Windows.Forms.BindingSource sessionEventBindingSource;
        private System.Windows.Forms.BindingSource participantBindingSource;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDescription;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colUId1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCreatedOn1;
        private System.Windows.Forms.BindingSource sessionActionBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colParticipantDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colUId;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedOn;
        private System.Windows.Forms.BindingSource participantBindingSource1;
        private UserControls.RecorderUserControl recorderUserControl;
        private CourtProcessPlayer.PlayerUserControl playerUserControl;
        private CourtProcessPlayer.SessionEventUserControl sessionEventUserControl;
        private DevExpress.XtraEditors.PanelControl mediaControlPanel;
        private DevExpress.XtraEditors.PanelControl descriptionPanel;
        private DevExpress.XtraEditors.PanelControl sessionEventPanel;
        private DevExpress.XtraEditors.PanelControl mediaControllerButtonsPanelControl;
        private DevExpress.XtraEditors.LabelControl currentTimelbl;
        private DevExpress.XtraEditors.SimpleButton playPausebtn;
        private DevExpress.XtraEditors.SimpleButton stopbtn;
        private DevExpress.XtraEditors.SimpleButton recordbtn;
        private DevExpress.XtraEditors.LookUpEdit playbackDeviceLookUpEdit;
        private System.Windows.Forms.BindingSource audioDeviceBindingSource;
        private DevExpress.XtraEditors.PanelControl recorderPanelControl;
        private DevExpress.XtraEditors.PanelControl mainPanelControl;
        private DevExpress.XtraBars.BarEditItem templatesLookupEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit templatesRepositoryItemLookUpEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit repositoryItemTimeSpanEdit1;
        private System.Windows.Forms.BindingSource sessionTemplateBindingSource;
        private System.Windows.Forms.BindingSource paragraphsBindingSource;
        private DevExpress.XtraBars.BarButtonItem addParticipantBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem deleteParticipantBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup sessionEventRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem deleteSessionActionBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem writeToDiscBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem printBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem signBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem secondsBeforeBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem secondsAfterBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem addSessionActionBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup actionsRibbonPageGroup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup participantRibbonPageGroup;
        private DevExpress.Utils.ImageCollection recordImageCollection;
        private DevExpress.Utils.ImageCollection stopImageCollection;
        private DevExpress.Utils.ImageCollection playPauseImageCollection;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup exitRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem closeBarButtonItem;
        private DevExpress.XtraEditors.LookUpEdit playbackSpeedLookUpEdit;
        private System.Windows.Forms.BindingSource playbackSpeedBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraTab.XtraTabControl participantAudioFilesContainersXtraTabControl;
        private DevExpress.XtraTab.XtraTabPage participantsTab;
        private DevExpress.XtraTab.XtraTabPage audioFilesContainersTab;
        private CourtProcessPlayer.AudioFilesContainersUserControl audioFilesContainersUserControl1;
        private DevExpress.XtraEditors.TextEdit seekTextEdit;
        private DevExpress.XtraEditors.SimpleButton seekSimpleButton;
        private DevExpress.Utils.ImageCollection fastForwardImageCollection;
        private DevExpress.XtraEditors.SplitContainerControl topSplitContainerControl;
        private DevExpress.XtraEditors.SplitContainerControl mainSplitContainerControl;
        private DevExpress.XtraEditors.SplitContainerControl bottomSplitContainerControl;
        private DevExpress.XtraBars.BarButtonItem questionAnswerbarButtonItem;
        private DevExpress.XtraBars.BarEditItem sessionTemplateTypeBarEditItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox sessionTemplateTypeImageComboBox;
        private DevExpress.XtraBars.BarButtonItem generateCprBarButtonItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}