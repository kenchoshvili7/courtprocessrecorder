﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CourtProcessRecorder.ProsecutorServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ProsecutorServiceReference.IService")]
    public interface IService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetServerDate", ReplyAction="http://tempuri.org/IService/GetServerDateResponse")]
        System.DateTime GetServerDate();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetServerDate", ReplyAction="http://tempuri.org/IService/GetServerDateResponse")]
        System.Threading.Tasks.Task<System.DateTime> GetServerDateAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/IsProsecurtorAddedOnCaseNo", ReplyAction="http://tempuri.org/IService/IsProsecurtorAddedOnCaseNoResponse")]
        bool IsProsecurtorAddedOnCaseNo(string casoNO);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/IsProsecurtorAddedOnCaseNo", ReplyAction="http://tempuri.org/IService/IsProsecurtorAddedOnCaseNoResponse")]
        System.Threading.Tasks.Task<bool> IsProsecurtorAddedOnCaseNoAsync(string casoNO);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/Insert", ReplyAction="http://tempuri.org/IService/InsertResponse")]
        bool Insert(string courtName, string caseNO, string prosecutorID, string prosecutorName, System.DateTime startDate, System.DateTime endDate);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/Insert", ReplyAction="http://tempuri.org/IService/InsertResponse")]
        System.Threading.Tasks.Task<bool> InsertAsync(string courtName, string caseNO, string prosecutorID, string prosecutorName, System.DateTime startDate, System.DateTime endDate);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceChannel : CourtProcessRecorder.ProsecutorServiceReference.IService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceClient : System.ServiceModel.ClientBase<CourtProcessRecorder.ProsecutorServiceReference.IService>, CourtProcessRecorder.ProsecutorServiceReference.IService {
        
        public ServiceClient() {
        }
        
        public ServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.DateTime GetServerDate() {
            return base.Channel.GetServerDate();
        }
        
        public System.Threading.Tasks.Task<System.DateTime> GetServerDateAsync() {
            return base.Channel.GetServerDateAsync();
        }
        
        public bool IsProsecurtorAddedOnCaseNo(string casoNO) {
            return base.Channel.IsProsecurtorAddedOnCaseNo(casoNO);
        }
        
        public System.Threading.Tasks.Task<bool> IsProsecurtorAddedOnCaseNoAsync(string casoNO) {
            return base.Channel.IsProsecurtorAddedOnCaseNoAsync(casoNO);
        }
        
        public bool Insert(string courtName, string caseNO, string prosecutorID, string prosecutorName, System.DateTime startDate, System.DateTime endDate) {
            return base.Channel.Insert(courtName, caseNO, prosecutorID, prosecutorName, startDate, endDate);
        }
        
        public System.Threading.Tasks.Task<bool> InsertAsync(string courtName, string caseNO, string prosecutorID, string prosecutorName, System.DateTime startDate, System.DateTime endDate) {
            return base.Channel.InsertAsync(courtName, caseNO, prosecutorID, prosecutorName, startDate, endDate);
        }
    }
}
