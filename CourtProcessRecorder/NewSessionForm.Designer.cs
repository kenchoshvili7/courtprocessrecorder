﻿namespace CourtProcessRecorder
{
    partial class NewSessionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sessionNumberLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.sessionNameLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.sessionDateLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.sessionNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sessionDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sessionNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.sessionNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionNameTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // sessionNumberLabelControl
            // 
            this.sessionNumberLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionNumberLabelControl.Location = new System.Drawing.Point(12, 12);
            this.sessionNumberLabelControl.Name = "sessionNumberLabelControl";
            this.sessionNumberLabelControl.Size = new System.Drawing.Size(63, 17);
            this.sessionNumberLabelControl.TabIndex = 0;
            this.sessionNumberLabelControl.Text = "სხდომის N";
            // 
            // sessionNameLabelControl
            // 
            this.sessionNameLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionNameLabelControl.Location = new System.Drawing.Point(12, 84);
            this.sessionNameLabelControl.Name = "sessionNameLabelControl";
            this.sessionNameLabelControl.Size = new System.Drawing.Size(120, 17);
            this.sessionNameLabelControl.TabIndex = 0;
            this.sessionNameLabelControl.Text = "სხდომის დასახელება";
            // 
            // sessionDateLabelControl
            // 
            this.sessionDateLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionDateLabelControl.Location = new System.Drawing.Point(207, 12);
            this.sessionDateLabelControl.Name = "sessionDateLabelControl";
            this.sessionDateLabelControl.Size = new System.Drawing.Size(101, 17);
            this.sessionDateLabelControl.TabIndex = 0;
            this.sessionDateLabelControl.Text = "სხდომის თარიღი";
            // 
            // sessionNumberTextEdit
            // 
            this.sessionNumberTextEdit.Location = new System.Drawing.Point(12, 40);
            this.sessionNumberTextEdit.Name = "sessionNumberTextEdit";
            this.sessionNumberTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionNumberTextEdit.Properties.Appearance.Options.UseFont = true;
            this.sessionNumberTextEdit.Size = new System.Drawing.Size(150, 22);
            this.sessionNumberTextEdit.TabIndex = 1;
            // 
            // sessionDateTextEdit
            // 
            this.sessionDateTextEdit.Enabled = false;
            this.sessionDateTextEdit.Location = new System.Drawing.Point(208, 40);
            this.sessionDateTextEdit.Name = "sessionDateTextEdit";
            this.sessionDateTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionDateTextEdit.Properties.Appearance.Options.UseFont = true;
            this.sessionDateTextEdit.Size = new System.Drawing.Size(100, 22);
            this.sessionDateTextEdit.TabIndex = 1;
            // 
            // sessionNameTextEdit
            // 
            this.sessionNameTextEdit.Location = new System.Drawing.Point(12, 107);
            this.sessionNameTextEdit.Name = "sessionNameTextEdit";
            this.sessionNameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionNameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.sessionNameTextEdit.Size = new System.Drawing.Size(296, 22);
            this.sessionNameTextEdit.TabIndex = 2;
            // 
            // okSimpleButton
            // 
            this.okSimpleButton.Location = new System.Drawing.Point(31, 140);
            this.okSimpleButton.Name = "okSimpleButton";
            this.okSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.okSimpleButton.TabIndex = 3;
            this.okSimpleButton.Text = "OK";
            this.okSimpleButton.Click += new System.EventHandler(this.okSimpleButton_Click);
            // 
            // cancelSimpleButton
            // 
            this.cancelSimpleButton.Location = new System.Drawing.Point(207, 140);
            this.cancelSimpleButton.Name = "cancelSimpleButton";
            this.cancelSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.cancelSimpleButton.TabIndex = 3;
            this.cancelSimpleButton.Text = "გაუქმება";
            this.cancelSimpleButton.Click += new System.EventHandler(this.cancelSimpleButton_Click);
            // 
            // NewSessionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 170);
            this.Controls.Add(this.cancelSimpleButton);
            this.Controls.Add(this.okSimpleButton);
            this.Controls.Add(this.sessionNameTextEdit);
            this.Controls.Add(this.sessionDateTextEdit);
            this.Controls.Add(this.sessionNumberTextEdit);
            this.Controls.Add(this.sessionNameLabelControl);
            this.Controls.Add(this.sessionDateLabelControl);
            this.Controls.Add(this.sessionNumberLabelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "NewSessionForm";
            this.Text = "ახალი სხდომა";
            ((System.ComponentModel.ISupportInitialize)(this.sessionNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionNameTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl sessionNumberLabelControl;
        private DevExpress.XtraEditors.LabelControl sessionNameLabelControl;
        private DevExpress.XtraEditors.LabelControl sessionDateLabelControl;
        private DevExpress.XtraEditors.TextEdit sessionNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit sessionDateTextEdit;
        private DevExpress.XtraEditors.TextEdit sessionNameTextEdit;
        private DevExpress.XtraEditors.SimpleButton okSimpleButton;
        private DevExpress.XtraEditors.SimpleButton cancelSimpleButton;
    }
}