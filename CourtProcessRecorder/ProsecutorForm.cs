﻿using CourtProcessRecorder.HelperClasses;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourtProcessRecorder
{
    public partial class ProsecutorForm : Form
    {

        private List<ProsecutorModel> _list = new List<ProsecutorModel>();
        private List<string> _selectedProsecutor = new List<string>();

        public ProsecutorForm()
        {

            InitializeComponent();
            FillComboBox("");
        }

        private void RemoveItemsFromProsecutorIDs()
        {
            var items = checkedComboBoxEdit1.Properties.GetItems();

            foreach (var item in items)
            {
                CheckedListBoxItem f = (CheckedListBoxItem)item;
                if (f.CheckState == CheckState.Unchecked && _selectedProsecutor.Any(o => o == (string)f.Value))
                {
                    _selectedProsecutor.Remove((string)f.Value);

                }
            }

        }

        private void UpdateSelectedProsecutorIDs()
        {
            var items = checkedComboBoxEdit1.Properties.GetItems();
            foreach (var item in items)
            {
                CheckedListBoxItem f = (CheckedListBoxItem)item;
                if (f.CheckState == CheckState.Checked)
                {
                    if (!_selectedProsecutor.Any(s => s == (string)f.Value))
                    {
                        _selectedProsecutor.Add((string)f.Value);
                    }

                }
            }
        }

        private void FillComboBox(string srchtxt)
        {
            var list = getProsecutors(srchtxt);

            checkedComboBoxEdit1.Properties.Items.Clear();
            foreach (var item in list)
            {

                CheckState chedkedState = CheckState.Unchecked;
                if (_selectedProsecutor.Any() && _selectedProsecutor.Any(o => o == item.idprosecutors))
                {
                    chedkedState = CheckState.Checked;
                }

                checkedComboBoxEdit1.Properties.Items.Add(
                    new CheckedListBoxItem(item.idprosecutors, item.fullname, chedkedState, true));
            }
        }

        private void checkedComboBoxEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckedComboBoxEdit edit = sender as CheckedComboBoxEdit;
            if (!edit.IsPopupOpen)
            {
                edit.ShowPopup();
                SendKeys.Send(e.KeyChar.ToString());
            }
        }

        private List<ProsecutorModel> getProsecutors(string srchtxt)
        {
            if (_list != null && _list.Any())
            {
                if (!string.IsNullOrEmpty(srchtxt))
                {
                    return _list.Where(o => o.fullname.Contains(srchtxt)).ToList();
                }
                else
                {
                    return _list;
                }
            }

            _list = getProsecutorsFromServer();
            return _list;



        }


        private List<ProsecutorModel> getProsecutorsFromServer()
        {
            List<ProsecutorModel> result = new List<ProsecutorModel>();
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ProsecutorServiceUrl"]);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                response.Close();
                response.Dispose();
                result = JsonConvert.DeserializeObject<List<ProsecutorModel>>(content);
            }

            catch (Exception e)
            {
                return new List<ProsecutorModel>();

            }
           
           
       
            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {


            UpdateSelectedProsecutorIDs();
            var selectedItems = _selectedProsecutor;

            try
            {
                //var selectedItems = checkedComboBoxEdit1.Properties.GetItems().GetCheckedValues();

                ProsecutorServiceReference.ServiceClient client = new ProsecutorServiceReference.ServiceClient();
                ProsecutorInfo.Prosecutors = new List<ProsecutorInfoModel>();


                if (InternetConnectionHelper.CheckForInternetConnection())
                {
                    DateTime startDate = client.GetServerDate();
                    if (selectedItems.Count == 0 && checkBox1.Checked == false)
                    {
                        string message = "პროკურორის არჩევა სავალდებულოა";
                        XtraMessageBox.Show(message, "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (!checkBox1.Checked)
                    {

                        foreach (var value in selectedItems)
                        {
                            var list = getProsecutors("");
                            var prosecurtor = list.Where(s => s.idprosecutors == (string)value).FirstOrDefault();
                            string fullName = string.Format("{0} {1}", prosecurtor.user_name, prosecurtor.user_lastname);


                            //client.Insert("CourtName", ProsecutorInfo.CaseNo, value.ToString(), fullName, DateTime.Now, DateTime.Now);
                            ProsecutorInfo.Prosecutors.Add(new ProsecutorInfoModel()
                            {
                                CourtName = "CourtName"
                                ,
                                CaseNo = ProsecutorInfo.CaseNo
                                ,
                                ProsecutorID = value.ToString()
                                ,
                                FullName = fullName
                                ,
                                StartDate = startDate
                            });

                        }
                    }



                    this.DialogResult = DialogResult.Yes;

                }
            }
            catch(Exception exc)
            {

            }
            finally
            {

                if (selectedItems.Count != 0 || checkBox1.Checked)
                {
                    this.Close();
                }


            }


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var textbox = (TextBox)sender;
            var text = textbox.Text;

            UpdateSelectedProsecutorIDs();

            //if (!string.IsNullOrEmpty(text))
            //{
            FillComboBox(text);

            //}


        }

        private void Form_Load(object sender, EventArgs e)
        {

            //checkedComboBoxEdit1.Properties.IncrementalSearch = true;
            //checkedComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            //checkedComboBoxEdit1.Properties.ShowButtons = false;

        }

        private void checkedComboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {

            RemoveItemsFromProsecutorIDs();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox checkbox = (CheckBox)sender;
            if (checkbox.Checked)
            {
                textBox1.Enabled = false;
                checkedComboBoxEdit1.ReadOnly = true;

            }
            else
            {
                textBox1.Enabled = true;
                checkedComboBoxEdit1.ReadOnly = false;
            }

        }



    }


    public class ProsecutorModel
    {
        public string idprosecutors { get; set; }
        public string user_name { get; set; }

        public string user_lastname { get; set; }

        public string personal_number { get; set; }


        public string fullname { get { return string.Format("{0} {1} {2}", user_name, user_lastname, personal_number); } }

    }


    public class ProsecutorInfoModel
    {

        public string CourtName { get; set; }
        public string CaseNo { get; set; }
        public string ProsecutorID { get; set; }
        public string FullName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }

    public static class ProsecutorInfo
    {


        public static string CourtName { get; set; }
        public static string CaseNo { get; set; }
        public static List<ProsecutorInfoModel> Prosecutors { get; set; }
        public static DateTime StartDate { get; set; }
        public static DateTime EndDate { get; set; }
    }

}
