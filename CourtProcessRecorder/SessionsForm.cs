﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourtProcessRecorder.CustomRibbonControls;
using CourtProcessRecorder.HelperClasses;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using HelperClasses;
using JsonDatabaseModels;
using RecordProvider;
using RecordProvider.HelperClasses;

namespace CourtProcessRecorder
{
    public partial class SessionsForm : RibbonForm
    {
        private readonly DatabaseHelper _databaseHelper;
        private Main _mainForm;
        private DateTime _fromDate;
        private DateTime _toDate;

        public SessionsForm()
        {
            InitializeComponent();

            IsMdiContainer = false;
            ribbon.MdiMergeStyle = RibbonMdiMergeStyle.Always;

            this.WindowState = FormWindowState.Maximized;

            _toDate = DateTime.Now;
            _fromDate = _toDate.AddMonths(-1);

            fromDateBarEditItem.EditValue = _fromDate;
            toDateBarEditItem.EditValue = _toDate;

            fromDateBarEditItem.EditValueChanged += DateBarEditItemOnEditValueChanged;
            toDateBarEditItem.EditValueChanged += DateBarEditItemOnEditValueChanged;

            _databaseHelper = Global.GlobalMode == GlobalMode.Viewer ? DatabaseHelper.DatabaseHelperInstance(true) : DatabaseHelper.DatabaseHelperInstance();

            FillGrid();

            sessionsGridView.DoubleClick += SessionsGridViewOnDoubleClick;

            this.Shown += OnShown;

            this.KeyPreview = true;
            this.KeyDown += OnKeyDown;

            SetGlobalMode();
        }

        private void SetGlobalMode()
        {
            switch (Global.GlobalMode)
            {
                case GlobalMode.Viewer:
                    writeToDiscRibbonPageGroup.Visible = false;
                    newSession.Visibility = BarItemVisibility.Never;
                    containerGroup.Visible = false;
                    break;
                case GlobalMode.Editor:
                    newSession.Visibility = BarItemVisibility.Never;

                    break;
                case GlobalMode.Recorder:
                    break;
            }
        }

        private void OnShown(object sender, EventArgs e)
        {
            Activated += OnActivated;

            ribbon.Select();
            ribbon.SelectedPage = ribbon.Pages[0];
        }

        private void OnActivated(object sender, EventArgs eventArgs)
        {
            this.WindowState = FormWindowState.Maximized;

            FillGrid();

            //sessionsGridControl.RefreshDataSource();

            var customRibbon = this.Parent?.Parent?.Controls.OfType<CustomRibbonControl>().FirstOrDefault();
            if (customRibbon != null)
            {
                customRibbon.Pages[0].Groups[1].Visible = true;
                customRibbon.Pages[0].Visible = true;
            }
        }

        private void DateBarEditItemOnEditValueChanged(object sender, EventArgs e)
        {
            _fromDate = (DateTime)fromDateBarEditItem.EditValue;
            _toDate = (DateTime)toDateBarEditItem.EditValue;

            FillGrid();
        }

        private async void FillGrid()
        {
            //sessionsGridControl.DataSource  = Global.GlobalMode == GlobalMode.Viewer ? _databaseHelper.GetSessions(_fromDate, _toDate) : _databaseHelper.GetSessionsAndUpdate(_fromDate, _toDate);

            sessionsGridControl.DataSource = await Task.Run(() => Global.GlobalMode == GlobalMode.Viewer ? _databaseHelper.GetSessions(_fromDate, _toDate) : _databaseHelper.GetSessionsAndUpdate(_fromDate, _toDate));
        }

        private void SessionsGridViewOnDoubleClick(object sender, EventArgs e)
        {
            setProsecutorInfo();
            OpenOnClick();
        }

        private void  setProsecutorInfo()
        {
            GridView gridView = sessionsGridControl.FocusedView as GridView;
            object selectedRowData = gridView.GetRow(gridView.FocusedRowHandle);

            var CaseNo = selectedRowData.GetType().GetProperty("Number").GetValue(selectedRowData, null);
            ProsecutorInfo.CaseNo = (string)CaseNo;
        }


        private void OpenOnClick()
        {
            if (!(sessionsGridView.GetFocusedRow() is Session session)) return;

            OpenSession(session.Path);
        }

        private void newSession_ItemClick(object sender, ItemClickEventArgs e)
        {
            var newSessionForm = new NewSessionForm { StartPosition = FormStartPosition.CenterParent };

            if (newSessionForm.ShowDialog() != DialogResult.OK) return;
            var newsession = new Session
            {
                Number = newSessionForm.SessionNumber,
                Name = newSessionForm.SessionName
            };

            CreateSession(newsession);
        }

        private void openSession_ItemClick(object sender, ItemClickEventArgs e)
        {

            //// get templates from old program

            //var templates = new List<Template>();
            //foreach (string str in Directory.GetFiles(@"C:\Users\l.jalaghonia\Desktop\new recorder templates"))
            //{
            //    if (Path.GetExtension(str).ToUpper() == ".CLPT")
            //    {
            //        Template item = Template.LoadFromFile(str);
            //        if (item != null)
            //        {
            //            templates.Add(item);
            //        }
            //    }
            //}


            //foreach (var template in templates)
            //{
            //    var tem = new SessionTemplate()
            //    {
            //        Name = template.Name
            //    };

            //    tem.SessionActions = new List<SessionAction>();
            //    foreach (var caseStage in template.CaseStages)
            //    {
            //        tem.SessionActions.Add(new SessionAction() { UId = Guid.NewGuid(), Description = caseStage.Key });
            //    }

            //    tem.Paragraphs = new List<Paragraph>();
            //    foreach (var paragraph in template.Paragraphs)
            //    {
            //        tem.Paragraphs.Add(new Paragraph() { UId = Guid.NewGuid(), Name = paragraph.Name });
            //    }

            //    tem.Positions = new List<Position>();
            //    foreach (var personType in template.PersonTypes)
            //    {
            //        tem.Positions.Add(new Position() { UId = Guid.NewGuid(), Name = personType });
            //    }

            //    _databaseHelper.AddSessionTemplate(tem);
            //}

            //_databaseHelper.TempTemplatePositionUpdate(_databaseHelper.GetSessionTemplates());

            ////

            if (openSessionFolderBrowserDialog.ShowDialog() != DialogResult.OK) return;
            var path = openSessionFolderBrowserDialog.SelectedPath;
            OpenSession(path);
        }

        private void addToContainerButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (openSessionFolderBrowserDialog.ShowDialog() != DialogResult.OK) return;
            var path = openSessionFolderBrowserDialog.SelectedPath;

            _databaseHelper.AddSessionToContainer(path);
            FillGrid();
            //sessionsGridControl.RefreshDataSource();
        }

        private void deleteSession_ItemClick(object sender, ItemClickEventArgs e)
        {
            DeleteSession();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                OpenOnClick();
            //if (e.KeyCode == Keys.Delete)
            //    DeleteSession();
        }

        private void CreateSession(Session session)
        {
            var match = Regex.IsMatch(session.Number, @"^(?! )[0-9a-zA-Z-\p{L}]*$");

            if (!match)
            {
                XtraMessageBox.Show("სხდომის ნომერში დაშვებული სიმბოლოებია: ასოები, ციფრები და ტირე!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //if (_databaseHelper.GetSessions().Any(s => s.Number == session.Number))
            //{
            //    XtraMessageBox.Show("მითითებული სხდომის ნომრით სხდომა უკვე არსებობს!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

            var sessionPath = PathHelper.CreateSessionPath(session.Number);
            session.Path = sessionPath;
            _databaseHelper.AddSession(session);

            _databaseHelper.AddSessionToContainer(sessionPath);

            _mainForm = new Main
            {
                IsMdiContainer = false,
                Text = session.Number + @"_" + session.Name,
                MdiParent = this.ParentForm
            };

            _mainForm.CreateNewSession(session);

            _mainForm.Show();
        }

        private void OpenSession(string path)
        {


            if (!AudioDevice.IsPlaybackDeviceConnected())
            {
                const string message = "ვერ მოიძებნა აუდიო მოწყობილობა!";
                XtraMessageBox.Show(message, "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ActionLogger.LogAction(ConstantStrings.Message, $"{message} SessionPath: {path}");
                return;
            }

            var name = Path.GetFileName(path);

            _mainForm = new Main
            {
                IsMdiContainer = false,
                Text = name,
                MdiParent = this.ParentForm
            };

            Session session;
            if (Global.GlobalMode == GlobalMode.Viewer)
            {
                using (var sessionDbHelper = new SessionDbHelper(path, true))
                    session = sessionDbHelper.GetSession();
            }
            else
            {
                using (var sessionDbHelper = new SessionDbHelper(path))
                    session = sessionDbHelper.GetSession();
            }

            if (Global.GlobalMode != GlobalMode.Recorder && (session?.AudioFilesContainers == null || !session.AudioFilesContainers.Any()))
            {
                XtraMessageBox.Show("სხდომა არ შეიცავს ფაილებს!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (WriteToDiscHelper.CheckIfSessionInUse(session))
            {
                XtraMessageBox.Show("მიმდინარეობს სხდომის დისკზე ჩაწერა!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var isOpened = _mainForm.OpenSession(path);
            if (!isOpened)
            {
                XtraMessageBox.Show("სხდომა ვერ ჩაიტვირთა!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _mainForm.Show();
        }

        private void DeleteSession()
        {
            //var focusedRowValue = sessionsGridView.GetFocusedRow();

            //if (!(focusedRowValue is Session session)) return;

            var sessions2Delete = new List<Session>();

            foreach (var index in sessionsGridView.GetSelectedRows().Where(i => i >= 0))
            {
                var ses = sessionsGridView.GetRow(index) as Session;
                if (ses == null) continue;
                sessions2Delete.Add(ses);
            }

            var result = XtraMessageBox.Show("ნამდვილად გსურთ ამოშლა?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                foreach (var ses in sessions2Delete) _databaseHelper.DeleteSession(ses);
            }

            //sessionsGridControl.RefreshDataSource();
            FillGrid();

            sessionsGridView.OptionsSelection.MultiSelect = false;
        }

        private void selectItemsBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            sessionsGridView.OptionsSelection.MultiSelect = !sessionsGridView.OptionsSelection.MultiSelect;
        }

        private void writeToDiscBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            //var selectedSessions = new List<Session>();
            var sessionPaths = new List<string>();

            foreach (var index in sessionsGridView.GetSelectedRows().Where(i => i >= 0))
            {
                var sessionPath = (sessionsGridView.GetRow(index) as Session)?.Path;

                sessionPaths.Add(sessionPath);
                //using (var sessionDbHelper = new SessionDbHelper(sessionPath))
                //{
                //    var session = sessionDbHelper.GetSession();
                //    session.Path = sessionPath;
                //    selectedSessions.Add(session);
                //}
            }

            if (!sessionPaths.Any()) return;

            if (!ReportHelper.CheckForSessionReportExists(sessionPaths)) return;

            WriteToDiscHelper.LaunchDialog(sessionPaths);

            sessionsGridView.OptionsSelection.MultiSelect = false;
        }

        private void generateCprBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            var focusedRowValue = sessionsGridView.GetFocusedRow();

            if (!(focusedRowValue is Session session)) return;

            if (!ReportHelper.CheckForSessionReportExists(new List<string> { session.Path })) return;

            GenerateCprHelper.CreateCprFile(session.Path);
        }

        public void DisposeSessionForm()
        {
            if (_mainForm == null) return;
            _mainForm.DisposeMain();
            _mainForm.Dispose();
        }
    }
}