﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using HelperClasses;

namespace CourtProcessRecorder
{
    static class Program
    {
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var culture = new CultureInfo("en-US")
            {
                DateTimeFormat =
                {
                    ShortDatePattern = "dd.MM.yyyy",
                    LongDatePattern = "dd.MM.yyyy",
                    LongTimePattern = "HH:mm:ss"
                },
                NumberFormat =
                {
                    CurrencySymbol = "",
                    NumberDecimalDigits = 2,
                    NumberDecimalSeparator = ".",
                    CurrencyDecimalSeparator = ".",
                    PercentDecimalSeparator = ".",
                    PercentDecimalDigits = 10,
                    NumberGroupSeparator = ""
                }
            };
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            if (!IsWin7OrHigher())
            {
                XtraMessageBox.Show("მხარდაჭერილი ოპერაციული სისტემის ვერსიაა Windows 7 და ზემოთ!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }

            if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Length > 1)
            {
                var proc = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).FirstOrDefault();

                if (proc != null)
                {
                    SetForegroundWindow(proc.MainWindowHandle);
                }

                return;
            }

            FormatInfo.AlwaysUseThreadFormat = true;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += ApplicationOnThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

            InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(new System.Globalization.CultureInfo("ka-GE"));

            BonusSkins.Register();
            SkinManager.EnableFormSkins();

            var skinName = ConfigurationHelper.GetConfiguration()?.SkinName;

            var skin = string.IsNullOrWhiteSpace(skinName) ? "DevExpress Style" : skinName;

            UserLookAndFeel.Default.SetSkinStyle(skin);

            Application.Run(new Default());
        }

        private static void ApplicationOnThreadException(object sender, ThreadExceptionEventArgs e)
        {
            HandleException(e.Exception);
        }

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;
            HandleException(exception);
        }

        private static void HandleException(Exception ex)
        {
            if (ex.Message.Contains("Parameter is not valid.")) return;

            var exType = ex.GetType();

            ExceptionLogHelper.AddExceptionLog(ex);

            if (exType == typeof(UnauthorizedAccessException))
            {
                XtraMessageBox.Show("მითითებულ მისამართზე წვდომა შეზღუდულია!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (exType == typeof(DirectoryNotFoundException) || exType == typeof(IOException))
            {
                XtraMessageBox.Show("მითითებული მისამართი ვერ მოიძებნა!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            XtraMessageBox.Show("პროგრამაში მოხდა შეცდომა გთხოვთ დაუკავშირდეთ ადმინისტრატორს!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static bool IsWin7OrHigher()
        {
            var os = Environment.OSVersion;
            return (os.Version.Major > 6) || ((os.Version.Major == 6) && (os.Version.Minor >= 1));
        }
    }
}
