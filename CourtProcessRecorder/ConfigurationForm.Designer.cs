﻿namespace CourtProcessRecorder
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.saveConfiguration = new DevExpress.XtraEditors.SimpleButton();
            this.configurationFolderLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.recordingDevicelbl = new DevExpress.XtraEditors.LabelControl();
            this.channelConfigurationGridControl = new DevExpress.XtraGrid.GridControl();
            this.channelBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.channelConfigurationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colChannelNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChannelType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.recordVolumeTrackBar = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.channelConfigurationHelperBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.recordingDeviceLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.audioDeviceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.configurationFolderTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.configurationBrowseSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.configurationDirectoryFolderBrowserDialog = new Ookii.Dialogs.VistaFolderBrowserDialog();
            this.warningTextLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.globalModeComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.globalModeLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.hallVolumeLevelTrackBar = new DevExpress.XtraEditors.TrackBarControl();
            this.hallVolumeLevelLabel = new DevExpress.XtraEditors.LabelControl();
            this.deleteConfirmationCheck = new DevExpress.XtraEditors.CheckEdit();
            this.courtNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.courtNameLabel = new DevExpress.XtraEditors.LabelControl();
            this.cityNameLabel = new DevExpress.XtraEditors.LabelControl();
            this.cityNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recordVolumeTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationHelperBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recordingDeviceLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.configurationFolderTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.globalModeComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hallVolumeLevelTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hallVolumeLevelTrackBar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteConfirmationCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.courtNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityNameTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // saveConfiguration
            // 
            this.saveConfiguration.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.saveConfiguration.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.saveConfiguration.Appearance.Options.UseFont = true;
            this.saveConfiguration.Location = new System.Drawing.Point(301, 581);
            this.saveConfiguration.Name = "saveConfiguration";
            this.saveConfiguration.Size = new System.Drawing.Size(75, 23);
            this.saveConfiguration.TabIndex = 8;
            this.saveConfiguration.Text = "შენახვა";
            this.saveConfiguration.Click += new System.EventHandler(this.saveConfiguration_Click);
            // 
            // configurationFolderLabelControl
            // 
            this.configurationFolderLabelControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.configurationFolderLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.configurationFolderLabelControl.Appearance.Options.UseFont = true;
            this.configurationFolderLabelControl.Location = new System.Drawing.Point(12, 68);
            this.configurationFolderLabelControl.Name = "configurationFolderLabelControl";
            this.configurationFolderLabelControl.Size = new System.Drawing.Size(119, 17);
            this.configurationFolderLabelControl.TabIndex = 2;
            this.configurationFolderLabelControl.Text = "არქივის მისამართი";
            // 
            // recordingDevicelbl
            // 
            this.recordingDevicelbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.recordingDevicelbl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.recordingDevicelbl.Appearance.Options.UseFont = true;
            this.recordingDevicelbl.Location = new System.Drawing.Point(405, 8);
            this.recordingDevicelbl.Name = "recordingDevicelbl";
            this.recordingDevicelbl.Size = new System.Drawing.Size(142, 17);
            this.recordingDevicelbl.TabIndex = 2;
            this.recordingDevicelbl.Text = "ჩამწერი მოწყობილობა";
            // 
            // channelConfigurationGridControl
            // 
            this.channelConfigurationGridControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.channelConfigurationGridControl.DataSource = this.channelBindingSource2;
            this.channelConfigurationGridControl.Location = new System.Drawing.Point(10, 267);
            this.channelConfigurationGridControl.MainView = this.channelConfigurationGridView;
            this.channelConfigurationGridControl.Name = "channelConfigurationGridControl";
            this.channelConfigurationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.recordVolumeTrackBar});
            this.channelConfigurationGridControl.Size = new System.Drawing.Size(654, 257);
            this.channelConfigurationGridControl.TabIndex = 7;
            this.channelConfigurationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.channelConfigurationGridView});
            // 
            // channelBindingSource2
            // 
            this.channelBindingSource2.DataSource = typeof(AudioConfiguration.Channel);
            // 
            // channelConfigurationGridView
            // 
            this.channelConfigurationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colChannelNo,
            this.colChannelType,
            this.colName,
            this.colIsActive,
            this.colRecordVolume});
            this.channelConfigurationGridView.GridControl = this.channelConfigurationGridControl;
            this.channelConfigurationGridView.Name = "channelConfigurationGridView";
            this.channelConfigurationGridView.OptionsCustomization.AllowColumnMoving = false;
            this.channelConfigurationGridView.OptionsCustomization.AllowFilter = false;
            this.channelConfigurationGridView.OptionsCustomization.AllowGroup = false;
            this.channelConfigurationGridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.channelConfigurationGridView.OptionsCustomization.AllowSort = false;
            this.channelConfigurationGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.channelConfigurationGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colChannelNo
            // 
            this.colChannelNo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colChannelNo.AppearanceCell.Options.UseFont = true;
            this.colChannelNo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colChannelNo.AppearanceHeader.Options.UseFont = true;
            this.colChannelNo.Caption = "არხის ნომერი";
            this.colChannelNo.FieldName = "ChannelNo";
            this.colChannelNo.Name = "colChannelNo";
            this.colChannelNo.OptionsColumn.AllowEdit = false;
            this.colChannelNo.Visible = true;
            this.colChannelNo.VisibleIndex = 0;
            // 
            // colChannelType
            // 
            this.colChannelType.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colChannelType.AppearanceCell.Options.UseFont = true;
            this.colChannelType.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colChannelType.AppearanceHeader.Options.UseFont = true;
            this.colChannelType.Caption = "არხის ტიპი";
            this.colChannelType.FieldName = "ChannelType";
            this.colChannelType.Name = "colChannelType";
            this.colChannelType.Visible = true;
            this.colChannelType.VisibleIndex = 1;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colName.AppearanceCell.Options.UseFont = true;
            this.colName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colName.AppearanceHeader.Options.UseFont = true;
            this.colName.Caption = "დასახელება";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 2;
            // 
            // colIsActive
            // 
            this.colIsActive.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colIsActive.AppearanceCell.Options.UseFont = true;
            this.colIsActive.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colIsActive.AppearanceHeader.Options.UseFont = true;
            this.colIsActive.Caption = "არის აქტიური";
            this.colIsActive.FieldName = "IsActive";
            this.colIsActive.Name = "colIsActive";
            this.colIsActive.Visible = true;
            this.colIsActive.VisibleIndex = 3;
            // 
            // colRecordVolume
            // 
            this.colRecordVolume.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colRecordVolume.AppearanceCell.Options.UseFont = true;
            this.colRecordVolume.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colRecordVolume.AppearanceHeader.Options.UseFont = true;
            this.colRecordVolume.Caption = "ხმის დონე";
            this.colRecordVolume.ColumnEdit = this.recordVolumeTrackBar;
            this.colRecordVolume.FieldName = "RecordVolume";
            this.colRecordVolume.Name = "colRecordVolume";
            this.colRecordVolume.Visible = true;
            this.colRecordVolume.VisibleIndex = 4;
            // 
            // recordVolumeTrackBar
            // 
            this.recordVolumeTrackBar.LabelAppearance.Options.UseTextOptions = true;
            this.recordVolumeTrackBar.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.recordVolumeTrackBar.Maximum = 100;
            this.recordVolumeTrackBar.Name = "recordVolumeTrackBar";
            // 
            // channelConfigurationHelperBindingSource
            // 
            this.channelConfigurationHelperBindingSource.DataSource = typeof(CourtProcessRecorder.HelperClasses.ChannelConfigurationHelper);
            // 
            // recordingDeviceLookUpEdit
            // 
            this.recordingDeviceLookUpEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.recordingDeviceLookUpEdit.Location = new System.Drawing.Point(405, 31);
            this.recordingDeviceLookUpEdit.Name = "recordingDeviceLookUpEdit";
            this.recordingDeviceLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.recordingDeviceLookUpEdit.Properties.Appearance.Options.UseFont = true;
            this.recordingDeviceLookUpEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.recordingDeviceLookUpEdit.Properties.AppearanceDropDown.Options.UseFont = true;
            this.recordingDeviceLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.recordingDeviceLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DeviceName", "Device Name", 72, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.recordingDeviceLookUpEdit.Properties.DataSource = this.audioDeviceBindingSource;
            this.recordingDeviceLookUpEdit.Properties.DisplayMember = "DeviceName";
            this.recordingDeviceLookUpEdit.Properties.ShowHeader = false;
            this.recordingDeviceLookUpEdit.Size = new System.Drawing.Size(259, 22);
            this.recordingDeviceLookUpEdit.TabIndex = 1;
            // 
            // audioDeviceBindingSource
            // 
            this.audioDeviceBindingSource.DataSource = typeof(RecordProvider.AudioDevice);
            // 
            // configurationFolderTextEdit
            // 
            this.configurationFolderTextEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.configurationFolderTextEdit.Location = new System.Drawing.Point(10, 91);
            this.configurationFolderTextEdit.Name = "configurationFolderTextEdit";
            this.configurationFolderTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.configurationFolderTextEdit.Properties.Appearance.Options.UseFont = true;
            this.configurationFolderTextEdit.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 10F);
            this.configurationFolderTextEdit.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.configurationFolderTextEdit.Properties.ReadOnly = true;
            this.configurationFolderTextEdit.Size = new System.Drawing.Size(569, 22);
            this.configurationFolderTextEdit.TabIndex = 2;
            // 
            // configurationBrowseSimpleButton
            // 
            this.configurationBrowseSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.configurationBrowseSimpleButton.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.configurationBrowseSimpleButton.Appearance.Options.UseFont = true;
            this.configurationBrowseSimpleButton.Location = new System.Drawing.Point(589, 90);
            this.configurationBrowseSimpleButton.Name = "configurationBrowseSimpleButton";
            this.configurationBrowseSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.configurationBrowseSimpleButton.TabIndex = 7;
            this.configurationBrowseSimpleButton.Text = "არჩევა";
            this.configurationBrowseSimpleButton.Click += new System.EventHandler(this.configurationBrowseSimpleButton_Click);
            // 
            // warningTextLabelControl
            // 
            this.warningTextLabelControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.warningTextLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.warningTextLabelControl.Appearance.ForeColor = System.Drawing.Color.Red;
            this.warningTextLabelControl.Appearance.Options.UseFont = true;
            this.warningTextLabelControl.Appearance.Options.UseForeColor = true;
            this.warningTextLabelControl.Location = new System.Drawing.Point(12, 543);
            this.warningTextLabelControl.Name = "warningTextLabelControl";
            this.warningTextLabelControl.Size = new System.Drawing.Size(0, 16);
            this.warningTextLabelControl.TabIndex = 8;
            // 
            // globalModeComboBoxEdit
            // 
            this.globalModeComboBoxEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.globalModeComboBoxEdit.Location = new System.Drawing.Point(10, 31);
            this.globalModeComboBoxEdit.Name = "globalModeComboBoxEdit";
            this.globalModeComboBoxEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.globalModeComboBoxEdit.Properties.Appearance.Options.UseFont = true;
            this.globalModeComboBoxEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.globalModeComboBoxEdit.Properties.AppearanceDropDown.Options.UseFont = true;
            this.globalModeComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.globalModeComboBoxEdit.Properties.DropDownRows = 3;
            this.globalModeComboBoxEdit.Size = new System.Drawing.Size(259, 22);
            this.globalModeComboBoxEdit.TabIndex = 0;
            // 
            // globalModeLabelControl
            // 
            this.globalModeLabelControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.globalModeLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.globalModeLabelControl.Appearance.Options.UseFont = true;
            this.globalModeLabelControl.Location = new System.Drawing.Point(10, 8);
            this.globalModeLabelControl.Name = "globalModeLabelControl";
            this.globalModeLabelControl.Size = new System.Drawing.Size(113, 17);
            this.globalModeLabelControl.TabIndex = 2;
            this.globalModeLabelControl.Text = "პროგრამის რეჟიმი";
            // 
            // hallVolumeLevelTrackBar
            // 
            this.hallVolumeLevelTrackBar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.hallVolumeLevelTrackBar.EditValue = 255;
            this.hallVolumeLevelTrackBar.Location = new System.Drawing.Point(10, 219);
            this.hallVolumeLevelTrackBar.Name = "hallVolumeLevelTrackBar";
            this.hallVolumeLevelTrackBar.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.hallVolumeLevelTrackBar.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.hallVolumeLevelTrackBar.Properties.LargeChange = 32;
            this.hallVolumeLevelTrackBar.Properties.Maximum = 255;
            this.hallVolumeLevelTrackBar.Properties.SmallChange = 32;
            this.hallVolumeLevelTrackBar.Size = new System.Drawing.Size(259, 45);
            this.hallVolumeLevelTrackBar.TabIndex = 5;
            this.hallVolumeLevelTrackBar.ToolTip = "ხმა დარბაზში";
            this.hallVolumeLevelTrackBar.Value = 255;
            // 
            // hallVolumeLevelLabel
            // 
            this.hallVolumeLevelLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.hallVolumeLevelLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.hallVolumeLevelLabel.Appearance.Options.UseFont = true;
            this.hallVolumeLevelLabel.Location = new System.Drawing.Point(12, 196);
            this.hallVolumeLevelLabel.Name = "hallVolumeLevelLabel";
            this.hallVolumeLevelLabel.Size = new System.Drawing.Size(87, 17);
            this.hallVolumeLevelLabel.TabIndex = 2;
            this.hallVolumeLevelLabel.Text = "ხმა დარბაზში";
            // 
            // deleteConfirmationCheck
            // 
            this.deleteConfirmationCheck.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.deleteConfirmationCheck.EditValue = true;
            this.deleteConfirmationCheck.Location = new System.Drawing.Point(405, 217);
            this.deleteConfirmationCheck.Name = "deleteConfirmationCheck";
            this.deleteConfirmationCheck.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.deleteConfirmationCheck.Properties.Appearance.Options.UseFont = true;
            this.deleteConfirmationCheck.Properties.Caption = "დასტური წაშლაზე";
            this.deleteConfirmationCheck.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.deleteConfirmationCheck.Size = new System.Drawing.Size(156, 21);
            this.deleteConfirmationCheck.TabIndex = 6;
            // 
            // courtNameTextEdit
            // 
            this.courtNameTextEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.courtNameTextEdit.Location = new System.Drawing.Point(10, 151);
            this.courtNameTextEdit.Name = "courtNameTextEdit";
            this.courtNameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.courtNameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.courtNameTextEdit.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 10F);
            this.courtNameTextEdit.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.courtNameTextEdit.Size = new System.Drawing.Size(259, 22);
            this.courtNameTextEdit.TabIndex = 3;
            // 
            // courtNameLabel
            // 
            this.courtNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.courtNameLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.courtNameLabel.Appearance.Options.UseFont = true;
            this.courtNameLabel.Location = new System.Drawing.Point(10, 128);
            this.courtNameLabel.Name = "courtNameLabel";
            this.courtNameLabel.Size = new System.Drawing.Size(167, 17);
            this.courtNameLabel.TabIndex = 2;
            this.courtNameLabel.Text = "სასამართლოს დასახელება";
            // 
            // cityNameLabel
            // 
            this.cityNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cityNameLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cityNameLabel.Appearance.Options.UseFont = true;
            this.cityNameLabel.Location = new System.Drawing.Point(405, 127);
            this.cityNameLabel.Name = "cityNameLabel";
            this.cityNameLabel.Size = new System.Drawing.Size(130, 17);
            this.cityNameLabel.TabIndex = 2;
            this.cityNameLabel.Text = "ქალაქის დასახელება";
            // 
            // cityNameTextEdit
            // 
            this.cityNameTextEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cityNameTextEdit.Location = new System.Drawing.Point(405, 150);
            this.cityNameTextEdit.Name = "cityNameTextEdit";
            this.cityNameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cityNameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.cityNameTextEdit.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cityNameTextEdit.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cityNameTextEdit.Size = new System.Drawing.Size(259, 22);
            this.cityNameTextEdit.TabIndex = 4;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(672, 608);
            this.Controls.Add(this.deleteConfirmationCheck);
            this.Controls.Add(this.hallVolumeLevelTrackBar);
            this.Controls.Add(this.globalModeComboBoxEdit);
            this.Controls.Add(this.warningTextLabelControl);
            this.Controls.Add(this.configurationBrowseSimpleButton);
            this.Controls.Add(this.cityNameTextEdit);
            this.Controls.Add(this.courtNameTextEdit);
            this.Controls.Add(this.configurationFolderTextEdit);
            this.Controls.Add(this.recordingDeviceLookUpEdit);
            this.Controls.Add(this.channelConfigurationGridControl);
            this.Controls.Add(this.globalModeLabelControl);
            this.Controls.Add(this.cityNameLabel);
            this.Controls.Add(this.recordingDevicelbl);
            this.Controls.Add(this.courtNameLabel);
            this.Controls.Add(this.hallVolumeLevelLabel);
            this.Controls.Add(this.configurationFolderLabelControl);
            this.Controls.Add(this.saveConfiguration);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(678, 532);
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recordVolumeTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationHelperBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recordingDeviceLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.configurationFolderTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.globalModeComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hallVolumeLevelTrackBar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hallVolumeLevelTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteConfirmationCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.courtNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityNameTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton saveConfiguration;
        private DevExpress.XtraEditors.LabelControl configurationFolderLabelControl;
        private DevExpress.XtraEditors.LabelControl recordingDevicelbl;
        private DevExpress.XtraGrid.GridControl channelConfigurationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView channelConfigurationGridView;
        private System.Windows.Forms.BindingSource channelConfigurationHelperBindingSource;
        private System.Windows.Forms.BindingSource channelBindingSource2;
        private DevExpress.XtraGrid.Columns.GridColumn colChannelNo;
        private DevExpress.XtraGrid.Columns.GridColumn colChannelType;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colIsActive;
        private DevExpress.XtraEditors.LookUpEdit recordingDeviceLookUpEdit;
        private DevExpress.XtraEditors.TextEdit configurationFolderTextEdit;
        private DevExpress.XtraEditors.SimpleButton configurationBrowseSimpleButton;
        private Ookii.Dialogs.VistaFolderBrowserDialog configurationDirectoryFolderBrowserDialog;
        private System.Windows.Forms.BindingSource audioDeviceBindingSource;
        private DevExpress.XtraEditors.LabelControl warningTextLabelControl;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordVolume;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar recordVolumeTrackBar;
        private DevExpress.XtraEditors.ImageComboBoxEdit globalModeComboBoxEdit;
        private DevExpress.XtraEditors.LabelControl globalModeLabelControl;
        private DevExpress.XtraEditors.TrackBarControl hallVolumeLevelTrackBar;
        private DevExpress.XtraEditors.LabelControl hallVolumeLevelLabel;
        private DevExpress.XtraEditors.CheckEdit deleteConfirmationCheck;
        private DevExpress.XtraEditors.TextEdit courtNameTextEdit;
        private DevExpress.XtraEditors.LabelControl courtNameLabel;
        private DevExpress.XtraEditors.LabelControl cityNameLabel;
        private DevExpress.XtraEditors.TextEdit cityNameTextEdit;
    }
}