﻿namespace CourtProcessRecorder
{
    partial class DigitalSignForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.signerLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.organizationLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.signerTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.organizationTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.signerTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.organizationTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // signerLabelControl
            // 
            this.signerLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.signerLabelControl.Location = new System.Drawing.Point(12, 12);
            this.signerLabelControl.Name = "signerLabelControl";
            this.signerLabelControl.Size = new System.Drawing.Size(72, 17);
            this.signerLabelControl.TabIndex = 0;
            this.signerLabelControl.Text = "ხელმომწერი";
            // 
            // organizationLabelControl
            // 
            this.organizationLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.organizationLabelControl.Location = new System.Drawing.Point(12, 84);
            this.organizationLabelControl.Name = "organizationLabelControl";
            this.organizationLabelControl.Size = new System.Drawing.Size(75, 17);
            this.organizationLabelControl.TabIndex = 0;
            this.organizationLabelControl.Text = "ორგანიზაცია";
            // 
            // signerTextEdit
            // 
            this.signerTextEdit.Location = new System.Drawing.Point(12, 40);
            this.signerTextEdit.Name = "signerTextEdit";
            this.signerTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.signerTextEdit.Properties.Appearance.Options.UseFont = true;
            this.signerTextEdit.Size = new System.Drawing.Size(296, 22);
            this.signerTextEdit.TabIndex = 1;
            // 
            // organizationTextEdit
            // 
            this.organizationTextEdit.Location = new System.Drawing.Point(12, 107);
            this.organizationTextEdit.Name = "organizationTextEdit";
            this.organizationTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.organizationTextEdit.Properties.Appearance.Options.UseFont = true;
            this.organizationTextEdit.Size = new System.Drawing.Size(296, 22);
            this.organizationTextEdit.TabIndex = 2;
            // 
            // okSimpleButton
            // 
            this.okSimpleButton.Location = new System.Drawing.Point(31, 140);
            this.okSimpleButton.Name = "okSimpleButton";
            this.okSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.okSimpleButton.TabIndex = 3;
            this.okSimpleButton.Text = "OK";
            this.okSimpleButton.Click += new System.EventHandler(this.okSimpleButton_Click);
            // 
            // cancelSimpleButton
            // 
            this.cancelSimpleButton.Location = new System.Drawing.Point(207, 140);
            this.cancelSimpleButton.Name = "cancelSimpleButton";
            this.cancelSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.cancelSimpleButton.TabIndex = 3;
            this.cancelSimpleButton.Text = "გაუქმება";
            this.cancelSimpleButton.Click += new System.EventHandler(this.cancelSimpleButton_Click);
            // 
            // DigitalSignForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 170);
            this.Controls.Add(this.cancelSimpleButton);
            this.Controls.Add(this.okSimpleButton);
            this.Controls.Add(this.organizationTextEdit);
            this.Controls.Add(this.signerTextEdit);
            this.Controls.Add(this.organizationLabelControl);
            this.Controls.Add(this.signerLabelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DigitalSignForm";
            this.Text = "ხელმოწერა";
            ((System.ComponentModel.ISupportInitialize)(this.signerTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.organizationTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl signerLabelControl;
        private DevExpress.XtraEditors.LabelControl organizationLabelControl;
        private DevExpress.XtraEditors.TextEdit signerTextEdit;
        private DevExpress.XtraEditors.TextEdit organizationTextEdit;
        private DevExpress.XtraEditors.SimpleButton okSimpleButton;
        private DevExpress.XtraEditors.SimpleButton cancelSimpleButton;
    }
}