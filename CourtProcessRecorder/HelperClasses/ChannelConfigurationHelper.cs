﻿using AudioConfiguration;

namespace CourtProcessRecorder.HelperClasses
{
    public class ChannelConfigurationHelper
    {
        public string ChannelNumber { get; set; }
        public ChannelType ChannelType { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}