﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CourtProcessRecorder.Reports;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using HelperClasses;
using JsonDatabaseModels;

namespace CourtProcessRecorder.HelperClasses
{
    public class ReportHelper
    {
        private static SessionReportRichTextForm _sessionReportRichTextForm;

        public static void GenerateSessionTemplate(Session session)
        {
            if (_sessionReportRichTextForm != null && !_sessionReportRichTextForm.IsDisposed)
            {
                _sessionReportRichTextForm.BringToFront();
                return;
            }

            _sessionReportRichTextForm = new SessionReportRichTextForm();
            var isValid = _sessionReportRichTextForm.LoadSessionReport(session);
            if (isValid) _sessionReportRichTextForm.Show();
        }

        public static bool CheckForSessionReportExists(List<string> sessionPaths)
        {
            foreach (var sessionPath in sessionPaths)
            {
                using (var sessionDbHelper = new SessionDbHelper(sessionPath))
                {
                    var session = sessionDbHelper.GetSession();
                    if (string.IsNullOrWhiteSpace(session.DocumentStream))
                    {
                        XtraMessageBox.Show($"სხდომაზე ნომრით : {session.Number} სხდომის ოქმი არ არის შენახული, გთხოვთ შეინახოთ", "შეცდომა!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        return false;
                    }
                }
            }

            return true;
        }

        public static bool CheckForSessionReportExists(Session session)
        {
            if (!CheckForSessionReportOpened()) return false;

            if (!string.IsNullOrWhiteSpace(session.DocumentStream)) return true;

            var result = XtraMessageBox.Show("სხდომის ოქმი არ არის შენახული, გსურთ ავტომატური შენახვა?", "შეცდომა!", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            if (result != DialogResult.Yes) return false;

            using (var sessionReportRichTextForm = new SessionReportRichTextForm())
            {
                var isValid = sessionReportRichTextForm.LoadSessionReport(session);
                if (isValid)
                {
                    sessionReportRichTextForm.LoadRichEditControlDocument();
                    sessionReportRichTextForm.Save();
                }
            }

            return true;
        }

        public static bool CheckForSessionReportOpened()
        {
            if (_sessionReportRichTextForm != null && !_sessionReportRichTextForm.IsDisposed)
            {
                XtraMessageBox.Show("სხდომის ოქმი გახსნილია, გთხოვთ დახუროთ!", "შეცდომა!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }

            return true;
        }

        public static void GenerateSessionReport(Session session)
        {
            var report = new SessionReport { DataSource = GenerateSessionReportDataSet(session).Tables[0] };

            var y = 0;
            if (session.Participants != null)
            {
                foreach (var participant in session.Participants)
                {
                    if (participant.Position.PositionType == PositionType.Judge
                        || participant.Position.PositionType == PositionType.HeadOfSession
                        || participant.Position.PositionType == PositionType.ClerkOfSession)
                        continue;

                    var label = new XRLabel();
                    label.AutoWidth = true;
                    label.Font = new System.Drawing.Font("Sylfaen", 12F);
                    label.LocationFloat = new DevExpress.Utils.PointFloat(0, y);
                    label.Name = participant.Name + participant.Position.Name;
                    label.Padding = new PaddingInfo(2, 2, 0, 0, 100F);
                    label.SizeF = new System.Drawing.SizeF(600F, 32F);
                    label.StylePriority.UseFont = false;
                    label.StylePriority.UseTextAlignment = false;
                    label.Text = participant.ParticipantDescription;
                    label.TextAlignment = TextAlignment.MiddleCenter;

                    report.AddLabelsToParticipantsXrPanel(label);

                    y += 40;
                }

                var judge = session.Participants.FirstOrDefault(p => p.Position.PositionType == PositionType.Judge)?.Name;
                var clerkOfSession = session.Participants.FirstOrDefault(p => p.Position.PositionType == PositionType.ClerkOfSession)?.Name;
                var headOfSession = session.Participants.FirstOrDefault(p => p.Position.PositionType == PositionType.HeadOfSession)?.Name;

                report.InitializeAdditionalValues(judge, clerkOfSession, headOfSession);
            }

            report.MoveFooterPanel(y);

            var documentsPath = Path.Combine(Directory.GetCurrentDirectory(), "Documents");

            if (!Directory.Exists(documentsPath)) Directory.CreateDirectory(documentsPath);

            var reportPath = Path.Combine(documentsPath, "სხდომის ოქმი_" + session.Number + "_" + session.Name + "_" + Guid.NewGuid() + ".rtf");
            report.ExportToRtf(reportPath);
            Process.Start(reportPath);
        }

        private static DataSet GenerateSessionReportDataSet(Session session)
        {
            var dataSet = new DataSet("SessionDataSet");
            var dataTable = new DataTable();
            dataSet.Tables.Add(dataTable);

            foreach (var propInfo in typeof(Session).GetProperties())
            {
                var colType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                dataTable.Columns.Add(propInfo.Name, colType);
            }

            var row = dataTable.NewRow();

            foreach (var propInfo in typeof(Session).GetProperties())
            {
                row[propInfo.Name] = propInfo.GetValue(session, null) ?? DBNull.Value;
            }

            dataTable.Rows.Add(row);

            return dataSet;
        }
    }
}
