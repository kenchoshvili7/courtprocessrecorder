﻿using System.IO;
using JsonDatabaseModels;

namespace CourtProcessRecorder.HelperClasses
{
    public class PermissionHelper
    {
        public static bool HasPermissionOnDir(string path, GlobalMode globalMode)
        {
            return globalMode == GlobalMode.Viewer ? HasReadPermissionOnDir(path) : IsDirectoryWritable(path);
        }

        private static bool HasReadPermissionOnDir(string path)
        {
            return Directory.Exists(path);
        }

        public static bool IsDirectoryWritable(string dirPath)
        {
            try
            {
                using (var fs = File.Create(Path.Combine(dirPath, Path.GetRandomFileName()), 1, FileOptions.DeleteOnClose))
                { }
                return true;
            }
            catch
            { return false; }
        }
    }
}
