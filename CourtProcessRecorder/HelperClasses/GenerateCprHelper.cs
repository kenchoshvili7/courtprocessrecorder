﻿using System.IO;
using System.IO.Compression;
using System.Windows.Forms;

namespace CourtProcessRecorder.HelperClasses
{
    public class GenerateCprHelper
    {
        public static void CreateCprFile(string path)
        {
            var saveFileDialog = new SaveFileDialog
            {
                DefaultExt = "*.cpr",
                Filter = @".cpr Files (*.cpr)|*.cpr",
                FileName = $"სხდომის ოქმი_{Path.GetFileName(path)}"
            };

            var result = saveFileDialog.ShowDialog();

            if(result!= DialogResult.OK) return;

            if (string.IsNullOrWhiteSpace(saveFileDialog.FileName)) return;

            if(File.Exists(saveFileDialog.FileName)) File.Delete(saveFileDialog.FileName);

            ZipFile.CreateFromDirectory(path, saveFileDialog.FileName);
        }
    }
}
