﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CourtProcessRecorder.BurnMedia;
using DevExpress.XtraEditors;
using HelperClasses;
using JsonDatabaseModels;

namespace CourtProcessRecorder.HelperClasses
{
    public class WriteToDiscHelper
    {
        private static readonly List<Session> SessionsToWrite = new List<Session>();
        private static bool _inWriteMode;
        public static event WriteToDiscFinished WriteToDiscFinished;

        public static void LaunchDialog(List<string> sessionPaths)
        {
            if (_inWriteMode)
            {
                XtraMessageBox.Show("მიმდინარეობს სხდომის დისკზე ჩაწერა!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WriteToDiscFinished?.Invoke();

                return;
            }

            string discNumber;
            string number;

            using (var sessionDbHelper = new SessionDbHelper(sessionPaths.FirstOrDefault()))
            {
                var ses = sessionDbHelper.GetSession();
                discNumber = ses.DiscNumber;
                number = ses.Number;
            }

            var writeToDiscForm = new WriteToDiscForm
            {
                StartPosition = FormStartPosition.CenterParent,
                DiscNumber = discNumber
            };

            if (writeToDiscForm.ShowDialog() != DialogResult.OK)
            {
                WriteToDiscFinished?.Invoke();
                return;
            }

            //SessionsToWrite.AddRange(sessions);
            _inWriteMode = true;

            var burnMediaForm = new BurnMediaForm();
            burnMediaForm.FormClosed += BurnMediaFormOnFormClosed;

            burnMediaForm.SetDiscLabel(number);

            var courtProcessPlayerFiles = Path.Combine(Directory.GetCurrentDirectory(), @"CourtProcessPlayer");

            foreach (var sessionPath in sessionPaths)
            {
                using (var sessionDbHelper = new SessionDbHelper(sessionPath))
                {
                    var session = sessionDbHelper.GetSession();
                    session.DiscNumber = writeToDiscForm.DiscNumber;
                    sessionDbHelper.UpdateSession(session);
                    SessionsToWrite.Add(session);
                }

                burnMediaForm.AddFolder(sessionPath);
                var rootDir = Path.GetFileName(sessionPath);

                foreach (var file in Directory.GetFiles(courtProcessPlayerFiles))
                {
                    if (file.Contains("bass.dll"))
                    {
                        burnMediaForm.AddFileToFolder(file, Path.Combine(rootDir, "Data"));
                        continue;
                    }
                    burnMediaForm.AddFileToFolder(file, rootDir);
                }
            }

            //HideUnHideSessionFolders(sessions, true);

            burnMediaForm.Show();

            //HideUnHideSessionFolders(sessions, false);
        }

        public static void LaunchDialog(string sessionPath)
        {
            LaunchDialog(new List<string> { sessionPath });
        }

        public static bool CheckIfSessionInUse(Session session)
        {
            return SessionsToWrite.Any(s => s.UId == session.UId);
        }

        private static void BurnMediaFormOnFormClosed(object sender, FormClosedEventArgs e)
        {
            SessionsToWrite.Clear();
            _inWriteMode = false;

            WriteToDiscFinished?.Invoke();
        }

        private static void HideUnHideSessionFolders(IEnumerable<Session> sessions, bool hide)
        {
            try
            {
                foreach (var session in sessions)
                {
                    foreach (var directory in Directory.GetDirectories(session.Path))
                    {
                        var dir = new DirectoryInfo(directory);
                        if (hide) dir.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                        else dir.Attributes = dir.Attributes ^ FileAttributes.Hidden;
                    }
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}
