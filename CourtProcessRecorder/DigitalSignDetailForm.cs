﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using CourtProcessRecorder.Reports;
using DevExpress.XtraEditors;
using HelperClasses;
using JsonDatabaseModels;

namespace CourtProcessRecorder
{
    public partial class DigitalSignDetailForm : XtraForm
    {
        private Signature _currentSignature;
        private Session _currentSession;

        public DigitalSignDetailForm()
        {
            InitializeComponent();
        }

        public void InitializeDigitalSignDetailForm(Session session)
        {
            _currentSession = session;

            var orderedSignatures = session.Signatures.OrderBy(s => s.SignTime);
            sessionDateLookUpEdit.Properties.DataSource = orderedSignatures;

            _currentSignature = orderedSignatures.FirstOrDefault();
            sessionDateLookUpEdit.EditValue = _currentSignature;
            LoadControlValues();

            sessionDateLookUpEdit.EditValueChanged += SessionDateLookUpEditOnEditValueChanged;
        }

        private void LoadControlValues()
        {
            responsiblePersonTextEdit.Text = _currentSignature.ResponsiblePerson;
            organizationTextEdit.Text = _currentSignature.Organization;
            createdOnTextEdit.Text = _currentSignature.SignTime.Value.ToString();
            signatureDataMemoEdit.Text = _currentSignature.FormattedData;
        }

        private void SessionDateLookUpEditOnEditValueChanged(object sender, EventArgs e)
        {
            var lookUpEdit = sender as LookUpEdit;
            _currentSignature = lookUpEdit.GetSelectedDataRow() as Signature;

            LoadControlValues();
        }

        private void checkSimpleButton_Click(object sender, EventArgs e)
        {
            var isValid = SignatureHelper.VerifySignature(_currentSession, SessionDbWrapper.SessionDbHelper.SessionPath, _currentSignature, _currentSignature.SignTime.Value);

            if (isValid) XtraMessageBox.Show("მონაცემთა ხელმოწერის შემოწმება წარმატებით დასრულდა!", "ხელმოწერის შემოწმება", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else XtraMessageBox.Show("მონაცემთა ხელმოწერის შემოწმება წარუმატებლად დასრულდა!", "ხელმოწერის შემოწმება", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void printSimleButton_Click(object sender, EventArgs e)
        {
            var reprot = new SignatureReport();
            var dataSet = GenerateDataSet();
            reprot.DataSource = dataSet.Tables[0];

            var signatureReportPrintFrom = new SignatureReportPrintPreviewForm();
            signatureReportPrintFrom.InitializeDocumentViewerDataSource(reprot);
            signatureReportPrintFrom.ShowDialog();

            //var path = Directory.GetCurrentDirectory() + "\\ხელმოწერა_" + Guid.NewGuid() + ".rtf";
            //reprot.ExportToRtf(path);
            //Process.Start(path);
        }

        private void closeSimpleButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private DataSet GenerateDataSet()
        {
            var dataSet = new DataSet("SignatureDataSet");
            var dataTable = new DataTable();
            dataSet.Tables.Add(dataTable);

            foreach (var propInfo in typeof(Signature).GetProperties())
            {
                var colType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                dataTable.Columns.Add(propInfo.Name, colType);
            }

            var row = dataTable.NewRow();

            foreach (var propInfo in typeof(Signature).GetProperties())
            {
                row[propInfo.Name] = propInfo.GetValue(_currentSignature, null) ?? DBNull.Value;
            }

            dataTable.Rows.Add(row);

            return dataSet;
        }
    }
}

