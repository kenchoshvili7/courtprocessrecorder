﻿namespace CourtProcessRecorder
{
    partial class Recorder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_RecorderOnOff = new System.Windows.Forms.Button();
            this.bt_Mic1 = new System.Windows.Forms.Button();
            this.bt_Mic2 = new System.Windows.Forms.Button();
            this.bt_Mic3 = new System.Windows.Forms.Button();
            this.bt_Mic4 = new System.Windows.Forms.Button();
            this.bt_Mic5 = new System.Windows.Forms.Button();
            this.bt_Mic6 = new System.Windows.Forms.Button();
            this.bt_Record = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.MixerPower = new System.Windows.Forms.CheckBox();
            this.Initialize = new System.Windows.Forms.Button();
            this.Stop = new System.Windows.Forms.Button();
            this.Play = new System.Windows.Forms.Button();
            this.Pause = new System.Windows.Forms.Button();
            this.PathTextbox = new System.Windows.Forms.TextBox();
            this.PathButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SessionNameTextbox = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.CurrentPosition = new System.Windows.Forms.Label();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_RecorderOnOff
            // 
            this.bt_RecorderOnOff.BackColor = System.Drawing.Color.Red;
            this.bt_RecorderOnOff.ForeColor = System.Drawing.Color.White;
            this.bt_RecorderOnOff.Location = new System.Drawing.Point(12, 86);
            this.bt_RecorderOnOff.Name = "bt_RecorderOnOff";
            this.bt_RecorderOnOff.Size = new System.Drawing.Size(132, 23);
            this.bt_RecorderOnOff.TabIndex = 0;
            this.bt_RecorderOnOff.Text = "ჩართვა/გამორთვა";
            this.bt_RecorderOnOff.UseVisualStyleBackColor = false;
            this.bt_RecorderOnOff.Click += new System.EventHandler(this.bt_RecorderOnOff_Click);
            // 
            // bt_Mic1
            // 
            this.bt_Mic1.BackColor = System.Drawing.Color.Green;
            this.bt_Mic1.ForeColor = System.Drawing.Color.White;
            this.bt_Mic1.Location = new System.Drawing.Point(13, 261);
            this.bt_Mic1.Name = "bt_Mic1";
            this.bt_Mic1.Size = new System.Drawing.Size(131, 23);
            this.bt_Mic1.TabIndex = 1;
            this.bt_Mic1.Text = "mic 1";
            this.bt_Mic1.UseVisualStyleBackColor = false;
            this.bt_Mic1.Click += new System.EventHandler(this.bt_Mic1_Click);
            // 
            // bt_Mic2
            // 
            this.bt_Mic2.BackColor = System.Drawing.Color.Green;
            this.bt_Mic2.ForeColor = System.Drawing.Color.White;
            this.bt_Mic2.Location = new System.Drawing.Point(13, 304);
            this.bt_Mic2.Name = "bt_Mic2";
            this.bt_Mic2.Size = new System.Drawing.Size(131, 23);
            this.bt_Mic2.TabIndex = 2;
            this.bt_Mic2.Text = "mic 2";
            this.bt_Mic2.UseVisualStyleBackColor = false;
            this.bt_Mic2.Click += new System.EventHandler(this.bt_Mic2_Click);
            // 
            // bt_Mic3
            // 
            this.bt_Mic3.BackColor = System.Drawing.Color.Green;
            this.bt_Mic3.ForeColor = System.Drawing.Color.White;
            this.bt_Mic3.Location = new System.Drawing.Point(13, 347);
            this.bt_Mic3.Name = "bt_Mic3";
            this.bt_Mic3.Size = new System.Drawing.Size(131, 23);
            this.bt_Mic3.TabIndex = 3;
            this.bt_Mic3.Text = "mic 3";
            this.bt_Mic3.UseVisualStyleBackColor = false;
            this.bt_Mic3.Click += new System.EventHandler(this.bt_Mic3_Click);
            // 
            // bt_Mic4
            // 
            this.bt_Mic4.BackColor = System.Drawing.Color.Green;
            this.bt_Mic4.ForeColor = System.Drawing.Color.White;
            this.bt_Mic4.Location = new System.Drawing.Point(12, 393);
            this.bt_Mic4.Name = "bt_Mic4";
            this.bt_Mic4.Size = new System.Drawing.Size(131, 23);
            this.bt_Mic4.TabIndex = 4;
            this.bt_Mic4.Text = "mic 4";
            this.bt_Mic4.UseVisualStyleBackColor = false;
            this.bt_Mic4.Click += new System.EventHandler(this.bt_Mic4_Click);
            // 
            // bt_Mic5
            // 
            this.bt_Mic5.BackColor = System.Drawing.Color.Green;
            this.bt_Mic5.ForeColor = System.Drawing.Color.White;
            this.bt_Mic5.Location = new System.Drawing.Point(13, 433);
            this.bt_Mic5.Name = "bt_Mic5";
            this.bt_Mic5.Size = new System.Drawing.Size(131, 23);
            this.bt_Mic5.TabIndex = 5;
            this.bt_Mic5.Text = "mic 5";
            this.bt_Mic5.UseVisualStyleBackColor = false;
            this.bt_Mic5.Click += new System.EventHandler(this.bt_Mic5_Click);
            // 
            // bt_Mic6
            // 
            this.bt_Mic6.BackColor = System.Drawing.Color.Green;
            this.bt_Mic6.ForeColor = System.Drawing.Color.White;
            this.bt_Mic6.Location = new System.Drawing.Point(12, 476);
            this.bt_Mic6.Name = "bt_Mic6";
            this.bt_Mic6.Size = new System.Drawing.Size(131, 23);
            this.bt_Mic6.TabIndex = 6;
            this.bt_Mic6.Text = "mic 6";
            this.bt_Mic6.UseVisualStyleBackColor = false;
            this.bt_Mic6.Click += new System.EventHandler(this.bt_Mic6_Click);
            // 
            // bt_Record
            // 
            this.bt_Record.BackColor = System.Drawing.Color.Green;
            this.bt_Record.ForeColor = System.Drawing.Color.White;
            this.bt_Record.Location = new System.Drawing.Point(12, 140);
            this.bt_Record.Name = "bt_Record";
            this.bt_Record.Size = new System.Drawing.Size(131, 23);
            this.bt_Record.TabIndex = 7;
            this.bt_Record.Text = "ჩაწერა";
            this.bt_Record.UseVisualStyleBackColor = false;
            this.bt_Record.Click += new System.EventHandler(this.bt_Record_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(265, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(231, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Court Process Recorder";
            // 
            // MixerPower
            // 
            this.MixerPower.AutoSize = true;
            this.MixerPower.Checked = true;
            this.MixerPower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MixerPower.Location = new System.Drawing.Point(34, 204);
            this.MixerPower.Name = "MixerPower";
            this.MixerPower.Size = new System.Drawing.Size(76, 17);
            this.MixerPower.TabIndex = 9;
            this.MixerPower.Text = "ჩართულია";
            this.MixerPower.UseVisualStyleBackColor = true;
            // 
            // Initialize
            // 
            this.Initialize.Location = new System.Drawing.Point(656, 113);
            this.Initialize.Name = "Initialize";
            this.Initialize.Size = new System.Drawing.Size(75, 23);
            this.Initialize.TabIndex = 10;
            this.Initialize.Text = "New Session";
            this.Initialize.UseVisualStyleBackColor = true;
            this.Initialize.Click += new System.EventHandler(this.button1_Click);
            // 
            // Stop
            // 
            this.Stop.Location = new System.Drawing.Point(633, 232);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(75, 23);
            this.Stop.TabIndex = 11;
            this.Stop.Text = "Stop";
            this.Stop.UseVisualStyleBackColor = true;
            this.Stop.Click += new System.EventHandler(this.button2_Click);
            // 
            // Play
            // 
            this.Play.Location = new System.Drawing.Point(356, 232);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(75, 23);
            this.Play.TabIndex = 12;
            this.Play.Text = "Play";
            this.Play.UseVisualStyleBackColor = true;
            this.Play.Click += new System.EventHandler(this.Play_Click);
            // 
            // Pause
            // 
            this.Pause.Location = new System.Drawing.Point(498, 232);
            this.Pause.Name = "Pause";
            this.Pause.Size = new System.Drawing.Size(75, 23);
            this.Pause.TabIndex = 13;
            this.Pause.Text = "Pause";
            this.Pause.UseVisualStyleBackColor = true;
            this.Pause.Click += new System.EventHandler(this.Pause_Click);
            // 
            // PathTextbox
            // 
            this.PathTextbox.Location = new System.Drawing.Point(498, 77);
            this.PathTextbox.Name = "PathTextbox";
            this.PathTextbox.Size = new System.Drawing.Size(131, 21);
            this.PathTextbox.TabIndex = 14;
            // 
            // PathButton
            // 
            this.PathButton.Location = new System.Drawing.Point(656, 77);
            this.PathButton.Name = "PathButton";
            this.PathButton.Size = new System.Drawing.Size(75, 21);
            this.PathButton.TabIndex = 15;
            this.PathButton.Text = "InitializeDir";
            this.PathButton.UseVisualStyleBackColor = true;
            this.PathButton.Click += new System.EventHandler(this.PathButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(353, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Configuration Directory";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(358, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Session Name";
            // 
            // SessionNameTextbox
            // 
            this.SessionNameTextbox.Location = new System.Drawing.Point(498, 113);
            this.SessionNameTextbox.Name = "SessionNameTextbox";
            this.SessionNameTextbox.Size = new System.Drawing.Size(131, 21);
            this.SessionNameTextbox.TabIndex = 19;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(274, 261);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(461, 155);
            this.dataGridView1.TabIndex = 20;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(270, 434);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(461, 42);
            this.trackBar1.TabIndex = 21;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // CurrentPosition
            // 
            this.CurrentPosition.AutoSize = true;
            this.CurrentPosition.Location = new System.Drawing.Point(473, 478);
            this.CurrentPosition.Name = "CurrentPosition";
            this.CurrentPosition.Size = new System.Drawing.Size(81, 13);
            this.CurrentPosition.TabIndex = 22;
            this.CurrentPosition.Text = "CurrentPosition";
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(498, 155);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(233, 21);
            this.comboBox1.TabIndex = 25;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(498, 197);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(233, 21);
            this.comboBox2.TabIndex = 25;
            this.comboBox2.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(361, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Playback Device";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(361, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Recorder Device";
            // 
            // Recorder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 688);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.CurrentPosition);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.SessionNameTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PathButton);
            this.Controls.Add(this.PathTextbox);
            this.Controls.Add(this.Pause);
            this.Controls.Add(this.Play);
            this.Controls.Add(this.Stop);
            this.Controls.Add(this.Initialize);
            this.Controls.Add(this.MixerPower);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bt_Record);
            this.Controls.Add(this.bt_Mic6);
            this.Controls.Add(this.bt_Mic5);
            this.Controls.Add(this.bt_Mic4);
            this.Controls.Add(this.bt_Mic3);
            this.Controls.Add(this.bt_Mic2);
            this.Controls.Add(this.bt_Mic1);
            this.Controls.Add(this.bt_RecorderOnOff);
            this.Name = "Recorder";
            this.Text = "CourtProcessRecorder";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_RecorderOnOff;
        private System.Windows.Forms.Button bt_Mic1;
        private System.Windows.Forms.Button bt_Mic2;
        private System.Windows.Forms.Button bt_Mic3;
        private System.Windows.Forms.Button bt_Mic4;
        private System.Windows.Forms.Button bt_Mic5;
        private System.Windows.Forms.Button bt_Mic6;
        private System.Windows.Forms.Button bt_Record;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox MixerPower;
        private System.Windows.Forms.Button Initialize;
        private System.Windows.Forms.Button Stop;
        private System.Windows.Forms.Button Play;
        private System.Windows.Forms.Button Pause;
        private System.Windows.Forms.TextBox PathTextbox;
        private System.Windows.Forms.Button PathButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SessionNameTextbox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label CurrentPosition;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox2;
    }
}

