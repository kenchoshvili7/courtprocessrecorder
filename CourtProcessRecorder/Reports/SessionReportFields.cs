﻿using System.Collections.Generic;

namespace CourtProcessRecorder.Reports
{
    public class SessionReportField
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }

        public string DisplayName
        {
            get { return FieldValue.Trim('[', ']'); }
        }

        public static List<SessionReportField> GetSessionReportFields()
        {
            return new List<SessionReportField>
            {
                new SessionReportField { FieldName = "Number", FieldValue = "[ნომერი]" },
                new SessionReportField { FieldName = "Name", FieldValue = "[დასახელება]" },
                new SessionReportField { FieldName = "DiscNumber", FieldValue = "[დისკის ნომერი]" },
                new SessionReportField { FieldName = "SessionDate", FieldValue = "[სხდომის თარიღი]" },
                new SessionReportField { FieldName = "OpenedOnFormatted", FieldValue = "[გახსნის დრო]" },
                new SessionReportField { FieldName = "ClosedOnFormatted", FieldValue = "[დახურვის დრო]" },
                new SessionReportField { FieldName = "TodayFormatted", FieldValue = "[მიმდინარე თარიღი]" },
                new SessionReportField { FieldName = "TemplateName", FieldValue = "[შაბლონის დასახელება]" },
                new SessionReportField { FieldName = "Participants", FieldValue = "[ყველა მონაწილე]" },
                new SessionReportField { FieldName = "RemainingParticipants", FieldValue = "[დანარჩენი მონაწილეები]" },
                new SessionReportField { FieldName = "SessionEvents", FieldValue = "[მოვლენები]" },
                new SessionReportField { FieldName = "CourtName", FieldValue = "[სასამართლოს დასახელება]" },
                new SessionReportField { FieldName = "CityName", FieldValue = "[ქალაქის დასახელება]" }
                };
        }
    }
}