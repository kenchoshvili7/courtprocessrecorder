﻿namespace CourtProcessRecorder.Reports
{
    partial class SignatureReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.digitalSignValue = new DevExpress.XtraReports.UI.XRLabel();
            this.signatureCreatedOnValue = new DevExpress.XtraReports.UI.XRLabel();
            this.organizationValue = new DevExpress.XtraReports.UI.XRLabel();
            this.sessionDateValue = new DevExpress.XtraReports.UI.XRLabel();
            this.responsiblePersonValue = new DevExpress.XtraReports.UI.XRLabel();
            this.digitalSignXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.signatureCreatedOnXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.sessionDateXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.organizationXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.responsiblePersonXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.bindingSource1 = new System.Windows.Forms.BindingSource();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.digitalSignValue,
            this.signatureCreatedOnValue,
            this.organizationValue,
            this.sessionDateValue,
            this.responsiblePersonValue,
            this.digitalSignXrLabel,
            this.signatureCreatedOnXrLabel,
            this.sessionDateXrLabel,
            this.organizationXrLabel,
            this.responsiblePersonXrLabel});
            this.Detail.HeightF = 500F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // digitalSignValue
            // 
            this.digitalSignValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FormattedData")});
            this.digitalSignValue.Font = new System.Drawing.Font("Courier New", 12F);
            this.digitalSignValue.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 232.25F);
            this.digitalSignValue.Multiline = true;
            this.digitalSignValue.Name = "digitalSignValue";
            this.digitalSignValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.digitalSignValue.SizeF = new System.Drawing.SizeF(591.2499F, 257.75F);
            this.digitalSignValue.StylePriority.UseFont = false;
            this.digitalSignValue.StylePriority.UseTextAlignment = false;
            this.digitalSignValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // signatureCreatedOnValue
            // 
            this.signatureCreatedOnValue.AutoWidth = true;
            this.signatureCreatedOnValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SignTime")});
            this.signatureCreatedOnValue.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.signatureCreatedOnValue.LocationFloat = new DevExpress.Utils.PointFloat(294.7916F, 99.74998F);
            this.signatureCreatedOnValue.Name = "signatureCreatedOnValue";
            this.signatureCreatedOnValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.signatureCreatedOnValue.SizeF = new System.Drawing.SizeF(215.625F, 25.08333F);
            this.signatureCreatedOnValue.StylePriority.UseFont = false;
            this.signatureCreatedOnValue.StylePriority.UseTextAlignment = false;
            this.signatureCreatedOnValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // organizationValue
            // 
            this.organizationValue.AutoWidth = true;
            this.organizationValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Organization")});
            this.organizationValue.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.organizationValue.LocationFloat = new DevExpress.Utils.PointFloat(294.7916F, 57.99999F);
            this.organizationValue.Name = "organizationValue";
            this.organizationValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.organizationValue.SizeF = new System.Drawing.SizeF(215.625F, 25.08333F);
            this.organizationValue.StylePriority.UseFont = false;
            this.organizationValue.StylePriority.UseTextAlignment = false;
            this.organizationValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // sessionDateValue
            // 
            this.sessionDateValue.AutoWidth = true;
            this.sessionDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SessionDate", "{0:dd.MM.yyyy}")});
            this.sessionDateValue.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.sessionDateValue.LocationFloat = new DevExpress.Utils.PointFloat(294.7916F, 141.3333F);
            this.sessionDateValue.Name = "sessionDateValue";
            this.sessionDateValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionDateValue.SizeF = new System.Drawing.SizeF(215.6249F, 25.08333F);
            this.sessionDateValue.StylePriority.UseFont = false;
            this.sessionDateValue.StylePriority.UseTextAlignment = false;
            this.sessionDateValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // responsiblePersonValue
            // 
            this.responsiblePersonValue.AutoWidth = true;
            this.responsiblePersonValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResponsiblePerson")});
            this.responsiblePersonValue.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.responsiblePersonValue.LocationFloat = new DevExpress.Utils.PointFloat(294.7917F, 20.49998F);
            this.responsiblePersonValue.Name = "responsiblePersonValue";
            this.responsiblePersonValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.responsiblePersonValue.SizeF = new System.Drawing.SizeF(215.6249F, 25.08333F);
            this.responsiblePersonValue.StylePriority.UseFont = false;
            this.responsiblePersonValue.StylePriority.UseTextAlignment = false;
            this.responsiblePersonValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // digitalSignXrLabel
            // 
            this.digitalSignXrLabel.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.digitalSignXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 189.3333F);
            this.digitalSignXrLabel.Multiline = true;
            this.digitalSignXrLabel.Name = "digitalSignXrLabel";
            this.digitalSignXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.digitalSignXrLabel.SizeF = new System.Drawing.SizeF(192.7084F, 24.04169F);
            this.digitalSignXrLabel.StylePriority.UseFont = false;
            this.digitalSignXrLabel.StylePriority.UseTextAlignment = false;
            this.digitalSignXrLabel.Text = "ციფრული ხელმოწერა:";
            this.digitalSignXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // signatureCreatedOnXrLabel
            // 
            this.signatureCreatedOnXrLabel.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.signatureCreatedOnXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 99.74998F);
            this.signatureCreatedOnXrLabel.Name = "signatureCreatedOnXrLabel";
            this.signatureCreatedOnXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.signatureCreatedOnXrLabel.SizeF = new System.Drawing.SizeF(228.125F, 25.08334F);
            this.signatureCreatedOnXrLabel.StylePriority.UseFont = false;
            this.signatureCreatedOnXrLabel.StylePriority.UseTextAlignment = false;
            this.signatureCreatedOnXrLabel.Text = "შექმნის დრო:";
            this.signatureCreatedOnXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // sessionDateXrLabel
            // 
            this.sessionDateXrLabel.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.sessionDateXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 141.3333F);
            this.sessionDateXrLabel.Name = "sessionDateXrLabel";
            this.sessionDateXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionDateXrLabel.SizeF = new System.Drawing.SizeF(228.125F, 25.08331F);
            this.sessionDateXrLabel.StylePriority.UseFont = false;
            this.sessionDateXrLabel.StylePriority.UseTextAlignment = false;
            this.sessionDateXrLabel.Text = "სხდომის თარიღი:";
            this.sessionDateXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // organizationXrLabel
            // 
            this.organizationXrLabel.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.organizationXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 57.99999F);
            this.organizationXrLabel.Name = "organizationXrLabel";
            this.organizationXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.organizationXrLabel.SizeF = new System.Drawing.SizeF(228.125F, 25.08333F);
            this.organizationXrLabel.StylePriority.UseFont = false;
            this.organizationXrLabel.StylePriority.UseTextAlignment = false;
            this.organizationXrLabel.Text = "ორგანიზაცია:";
            this.organizationXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // responsiblePersonXrLabel
            // 
            this.responsiblePersonXrLabel.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.responsiblePersonXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 20.49998F);
            this.responsiblePersonXrLabel.Name = "responsiblePersonXrLabel";
            this.responsiblePersonXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.responsiblePersonXrLabel.SizeF = new System.Drawing.SizeF(228.125F, 25.08333F);
            this.responsiblePersonXrLabel.StylePriority.UseFont = false;
            this.responsiblePersonXrLabel.StylePriority.UseTextAlignment = false;
            this.responsiblePersonXrLabel.Text = "პასუხისმგებელი პირი:";
            this.responsiblePersonXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 16.66667F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 48.95833F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(JsonDatabaseModels.Signature);
            // 
            // SignatureReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.DataSource = this.bindingSource1;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 17, 49);
            this.Version = "14.2";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel responsiblePersonXrLabel;
        private DevExpress.XtraReports.UI.XRLabel digitalSignXrLabel;
        private DevExpress.XtraReports.UI.XRLabel signatureCreatedOnXrLabel;
        private DevExpress.XtraReports.UI.XRLabel sessionDateXrLabel;
        private DevExpress.XtraReports.UI.XRLabel organizationXrLabel;
        private DevExpress.XtraReports.UI.XRLabel organizationValue;
        private DevExpress.XtraReports.UI.XRLabel sessionDateValue;
        private DevExpress.XtraReports.UI.XRLabel responsiblePersonValue;
        private DevExpress.XtraReports.UI.XRLabel signatureCreatedOnValue;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraReports.UI.XRLabel digitalSignValue;
    }
}
