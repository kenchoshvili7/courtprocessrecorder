﻿using DevExpress.XtraEditors;

namespace CourtProcessRecorder.Reports
{
    public partial class SignatureReportPrintPreviewForm : XtraForm
    {
        public SignatureReportPrintPreviewForm()
        {
            InitializeComponent();
        }

        public void InitializeDocumentViewerDataSource(object signatureReport)
        {
            documentViewer.DocumentSource = signatureReport;
        }
    }
}
