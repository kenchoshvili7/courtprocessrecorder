﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using HelperClasses;
using JsonDatabaseModels;

namespace CourtProcessRecorder.Reports
{
    public partial class SessionReportRichTextForm : XtraForm
    {
        private Session _session;

        public SessionReportRichTextForm()
        {
            InitializeComponent();
        }

        public void LoadRichEditControlDocument()
        {
            ReplaceAllFields();
            ChangeDocumentFontToSylfaen();
        }

        private void RichEditControlOnDocumentLoaded(object sender, EventArgs eventArgs)
        {
            LoadRichEditControlDocument();
        }

        public bool LoadSessionReport(Session session)
        {
            _session = session;

            if (_session.Signatures != null && _session.Signatures.Any())
            {
                generateNewDocumentBarButtonItem.Enabled = false;
                saveBarButtonItem.Enabled = false;
                richEditControl.ReadOnly = true;
            }

            if (!string.IsNullOrEmpty(session.DocumentStream))
            {
                var existingMs = new MemoryStream(Convert.FromBase64String(session.DocumentStream)) { Position = 0 };
                richEditControl.LoadDocument(existingMs, DocumentFormat.Rtf);

                SetGlobalMode();

                return true;
            }

            var documentStream = DatabaseHelper.DatabaseHelperInstance().GetTemlateDocument(session.SessionTemplate?.SessionTemplateType)?.DocumentStream;

            if (documentStream == null)
            {
                XtraMessageBox.Show("შაბლონის დოკუმენტი ვერ მოიძებნა!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }

            richEditControl.DocumentLoaded += RichEditControlOnDocumentLoaded;

            var ms = new MemoryStream(Convert.FromBase64String(documentStream)) { Position = 0 };

            richEditControl.LoadDocument(ms, DocumentFormat.Rtf);

            SetGlobalMode();

            return true;
        }

        public void LoadSessionReport(MemoryStream ms, Session session)
        {
            _session = session;

            ms.Position = 0;

            richEditControl.LoadDocument(ms, DocumentFormat.Rtf);
        }

        public void LoadSessionReport(string path)
        {
            richEditControl.LoadDocument(path, DocumentFormat.Rtf);
        }

        private void SetGlobalMode()
        {
            switch (Global.GlobalMode)
            {
                case GlobalMode.Viewer:
                    generateNewDocumentBarButtonItem.Enabled = false;
                    saveBarButtonItem.Enabled = false;
                    richEditControl.ReadOnly = true;
                    break;
                case GlobalMode.Editor:
                    break;
                case GlobalMode.Recorder:
                    break;
            }
        }

        private void ReplaceAllFields()
        {
            var ranges = richEditControl.Document.FindAll(new Regex(@"\[(.*?)\]", RegexOptions.IgnoreCase));

            var insertedParticipants = new List<Participant>();

            foreach (var range in ranges)
            {
                var text = richEditControl.Document.GetText(range);

                var searchString = text.Trim('[', ']');

                var participant = _session.Participants?.FirstOrDefault(p => p.Position?.Name != null && p.Position.Name.Contains(searchString));
                if (participant != null)
                {
                    insertedParticipants.Add(participant);

                    var participantName = participant.Name;

                    richEditControl.Document.Replace(range, participantName);
                }
                else
                {
                    var sessionReportField = SessionReportField.GetSessionReportFields().FirstOrDefault(s => s.FieldValue == text);
                    if (sessionReportField == null) continue;

                    var sessionEx = new SessionEx(_session);

                    var propName = sessionReportField.FieldName;
                    var value = text;
                    var propVal = sessionEx.GetType().GetProperty(propName)?.GetValue(sessionEx, null);
                    if (propVal != null)
                    {
                        if (propVal.GetType().IsGenericType) continue;

                        value = propVal.ToString();
                    }

                    richEditControl.Document.Replace(range, value);
                }
            }

            var courtNameRange = richEditControl.Document.FindAll("[სასამართლოს დასახელება]", SearchOptions.None)?.FirstOrDefault();
            if (courtNameRange != null)
            {
                var courtName = ConfigurationHelper.GetConfiguration()?.CourtName;

                richEditControl.Document.Replace(courtNameRange, string.IsNullOrWhiteSpace(courtName) ? "თბილისის საქალაქო სასამართლო" : courtName);
            }

            var cityNameRange = richEditControl.Document.FindAll("[ქალაქის დასახელება]", SearchOptions.None)?.FirstOrDefault();
            if (courtNameRange != null)
            {
                var cityName = ConfigurationHelper.GetConfiguration()?.CityName;

                richEditControl.Document.Replace(cityNameRange, string.IsNullOrWhiteSpace(cityName) ? "თბილისი" : cityName);
            }

            if (_session.Participants != null)
            {
                var participantsRange = richEditControl.Document.FindAll("[ყველა მონაწილე]", SearchOptions.None)?.FirstOrDefault();
                if (participantsRange != null)
                {
                    var participants = string.Empty;

                    foreach (var sessionParticipant in _session.Participants.Where(p => p.Position != null
                                                                                        && p.Position.PositionType != PositionType.Judge
                                                                                        && p.Position.PositionType != PositionType.ClerkOfSession))
                    {
                        if (string.IsNullOrEmpty(participants))
                            participants = sessionParticipant.Position.Name + ": " + sessionParticipant.Name;
                        else
                            participants = participants + Environment.NewLine + sessionParticipant.Position.Name + ": " + sessionParticipant.Name;
                    }

                    richEditControl.Document.Replace(participantsRange, participants);
                }

                var remainingParticipantsRange = richEditControl.Document.FindAll("[დანარჩენი მონაწილეები]", SearchOptions.None)?.FirstOrDefault();
                if (remainingParticipantsRange != null)
                {
                    var remainingParticipants = string.Empty;

                    foreach (var sessionParticipant in _session.Participants.Except(insertedParticipants))
                    {
                        if (string.IsNullOrEmpty(remainingParticipants))
                            remainingParticipants = sessionParticipant.Position.Name + ": " + sessionParticipant.Name;
                        else
                            remainingParticipants = remainingParticipants + Environment.NewLine +
                                                    sessionParticipant.Position.Name + ": " + sessionParticipant.Name;
                    }

                    richEditControl.Document.Replace(remainingParticipantsRange, remainingParticipants);
                }
            }

            if (_session.SessionEvents == null || !_session.SessionEvents.Any()) return;

            var sessinEventsRange = richEditControl.Document.FindAll("[მოვლენები]", SearchOptions.None)?.FirstOrDefault();
            if (sessinEventsRange == null) return;

            richEditControl.Document.Replace(sessinEventsRange, string.Empty);

            #region old with table

            //var table = richEditControl.Document.Tables.Create(sessinEventsRange.Start, _session.SessionEvents.Count + 1, 4);
            //table.TableLayout = TableLayoutType.Autofit;
            //table.PreferredWidthType = WidthType.Auto;

            //table.ForEachCell((cell, i, j) =>
            //{
            //    cell.Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //    cell.Borders.Top.LineStyle = TableBorderLineStyle.None;
            //    cell.Borders.Right.LineStyle = TableBorderLineStyle.None;
            //    cell.Borders.Left.LineStyle = TableBorderLineStyle.None;
            //});

            //richEditControl.Document.InsertHtmlText(table[0, 0].Range.Start, "<b>დრო</b>");
            //richEditControl.Document.InsertHtmlText(table[0, 1].Range.Start, "<b>მოქმედება</b>");
            //richEditControl.Document.InsertHtmlText(table[0, 2].Range.Start, "<b>მონაწილე</b>");
            //richEditControl.Document.InsertHtmlText(table[0, 3].Range.Start, "<b>შენიშვნა</b>");

            //for (var i = 0; i < _session.SessionEvents.Count; i++)
            //{
            //    var sessionEvent = _session.SessionEvents[i];
            //    richEditControl.Document.InsertText(table[i + 1, 0].Range.Start, sessionEvent.Time.ToString());
            //    richEditControl.Document.InsertText(table[i + 1, 1].Range.Start, sessionEvent.SessionAction?.Description);
            //    richEditControl.Document.InsertText(table[i + 1, 2].Range.Start, sessionEvent.Participant?.ParticipantDescription);
            //    richEditControl.Document.InsertText(table[i + 1, 3].Range.Start, sessionEvent.Note);

            //    richEditControl.Document.InsertText(sessinEventsRange.Start, sessionEvent.Time.ToString());

            //}

            #endregion

            //var sessionEventsText = "<table>";
            var sessionEventsText = string.Empty;

            foreach (var sessionEvent in _session.SessionEvents.OrderBy(o => o.Time))
            {
                var actionDescription = string.Empty;

                if (sessionEvent.SessionAction?.Description != null)
                {
                    actionDescription = sessionEvent.SessionAction.Description;

                    if (actionDescription.Contains("<") || actionDescription.Contains(">"))
                    {
                        continue;
                    }
                    actionDescription = HttpUtility.HtmlEncode(sessionEvent.SessionAction?.Description);
                }

                var participantDescription = sessionEvent.Participant?.ParticipantDescription;

                var actionText = string.IsNullOrWhiteSpace(actionDescription) ? string.Empty : $"{actionDescription}";
                var participantDescriptionText = string.IsNullOrWhiteSpace(participantDescription) ? string.Empty : $"{participantDescription}";

                //sessionEventsText += "<tr>" +
                //                     "<td height='35'> " +
                //                     $"{sessionEvent.TimeFormatted}: " +
                //                     $"{actionText}" +
                //                     $" {participantDescriptionText}" +
                //                     $" {sessionEvent.Note}" +
                //                     "</td>" +
                //                     "</tr>";

                sessionEventsText += $"{Environment.NewLine}" +
                                     $"{sessionEvent.TimeFormatted}: " +
                                     $"{actionText}" +
                                     $" {participantDescriptionText}" +
                                     $" {sessionEvent.Note}" +
                                     $"{Environment.NewLine}";
            }

            //sessionEventsText = $"{sessionEventsText}</table>";
            //richEditControl.Document.InsertHtmlText(sessinEventsRange.Start, sessionEventsText);

            sessionEventsText = $"{sessionEventsText}";

            richEditControl.Document.InsertText(sessinEventsRange.Start, sessionEventsText);
        }

        private void generateNewDocumentBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var result = XtraMessageBox.Show("არსებული დოკუმენტი წაიშლება! ნამდვილად გსურთ გენერაცია?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result != DialogResult.Yes) return;

            _session.DocumentStream = null;
            richEditControl.DocumentLoaded -= RichEditControlOnDocumentLoaded;

            LoadSessionReport(_session);
        }

        private void saveBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
            //new SessionDbHelper(_session.Path).UpdateSession(_session);
        }

        public void Save()
        {
            var ms = new MemoryStream();

            richEditControl.Document.SaveDocument(ms, DocumentFormat.Rtf);

            ms.Position = 0;

            var base64 = Convert.ToBase64String(ms.ToArray());
            _session.DocumentStream = base64;

            SessionDbWrapper.SessionDbHelper.UpdateSession(_session);
        }

        private void ChangeDocumentFontToSylfaen()
        {
            richEditControl.SelectAll();

            var selection = richEditControl.Document.Selection;

            var document = selection.BeginUpdateDocument();
            var characters = document.BeginUpdateCharacters(selection);
            characters.FontName = "Sylfaen";
            document.EndUpdateCharacters(characters);
            selection.EndUpdateDocument(document);

            var myStart = richEditControl.Document.CreatePosition(0);
            var myRange = richEditControl.Document.CreateRange(myStart, 0);

            richEditControl.Document.Selection = myRange;
        }
    }
}
