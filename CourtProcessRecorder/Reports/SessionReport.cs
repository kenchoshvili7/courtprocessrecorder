﻿using System.Drawing;
using DevExpress.XtraReports.UI;

namespace CourtProcessRecorder.Reports
{
    public partial class SessionReport : XtraReport
    {
        public SessionReport()
        {
            InitializeComponent();
        }

        public void AddLabelsToParticipantsXrPanel(XRLabel label)
        {
            participantsXrPanel.Controls.Add(label);
        }

        public void InitializeAdditionalValues(string judge, string clerkOfSession, string headOfSession)
        {
            judgeValue.Text = judge;
            clerkOfSessionValue.Text = clerkOfSession;
            clerkValue.Text = clerkOfSession;
            headOfSessionValue.Text = headOfSession;
        }

        public void MoveFooterPanel(int y)
        {
            var point = new Point(footerXrPanel.Location.X, participantsXrPanel.Location.Y + y);
            footerXrPanel.Location = point;
        }

    }
}
