﻿namespace CourtProcessRecorder.Reports
{
    partial class SessionReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.participantsXrPanel = new DevExpress.XtraReports.UI.XRPanel();
            this.footerXrPanel = new DevExpress.XtraReports.UI.XRPanel();
            this.sessionRecordedWithXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.clerkCheckedTechnicXrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.diskNumberXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.diskNumberValue = new DevExpress.XtraReports.UI.XRLabel();
            this.sessionClosedOnXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.headOfSessionXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.clerkOfSessionXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.sessionClosedOnValue = new DevExpress.XtraReports.UI.XRLabel();
            this.headOfSessionValue = new DevExpress.XtraReports.UI.XRLabel();
            this.clerkOfSessionValue = new DevExpress.XtraReports.UI.XRLabel();
            this.sessionClosedOnHourXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.courtSessionOpenedOnValue = new DevExpress.XtraReports.UI.XRLabel();
            this.clerkValue = new DevExpress.XtraReports.UI.XRLabel();
            this.judgeValue = new DevExpress.XtraReports.UI.XRLabel();
            this.courtSessionAttendedXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.onHourXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.courtSessionOpenedOnXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.clerkXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.judgeXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.courtSessionReportXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.sessionNumberValue = new DevExpress.XtraReports.UI.XRLabel();
            this.sessionDateXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.coutNameXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.courtRaionXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.sessionNumberXrLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.todayCalculatedField = new DevExpress.XtraReports.UI.CalculatedField();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.participantsXrPanel,
            this.footerXrPanel,
            this.courtSessionOpenedOnValue,
            this.clerkValue,
            this.judgeValue,
            this.courtSessionAttendedXrLabel,
            this.onHourXrLabel,
            this.courtSessionOpenedOnXrLabel,
            this.clerkXrLabel,
            this.judgeXrLabel});
            this.Detail.HeightF = 835.8334F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // participantsXrPanel
            // 
            this.participantsXrPanel.KeepTogether = false;
            this.participantsXrPanel.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 177.0833F);
            this.participantsXrPanel.Name = "participantsXrPanel";
            this.participantsXrPanel.SizeF = new System.Drawing.SizeF(650F, 192.7083F);
            // 
            // footerXrPanel
            // 
            this.footerXrPanel.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.footerXrPanel.CanGrow = false;
            this.footerXrPanel.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.sessionRecordedWithXrLabel,
            this.clerkCheckedTechnicXrLabel1,
            this.diskNumberXrLabel,
            this.diskNumberValue,
            this.sessionClosedOnXrLabel,
            this.headOfSessionXrLabel,
            this.clerkOfSessionXrLabel,
            this.sessionClosedOnValue,
            this.headOfSessionValue,
            this.clerkOfSessionValue,
            this.sessionClosedOnHourXrLabel});
            this.footerXrPanel.KeepTogether = false;
            this.footerXrPanel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 389.5835F);
            this.footerXrPanel.Name = "footerXrPanel";
            this.footerXrPanel.SizeF = new System.Drawing.SizeF(649.9999F, 446.2499F);
            // 
            // sessionRecordedWithXrLabel
            // 
            this.sessionRecordedWithXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.sessionRecordedWithXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 29.7917F);
            this.sessionRecordedWithXrLabel.Multiline = true;
            this.sessionRecordedWithXrLabel.Name = "sessionRecordedWithXrLabel";
            this.sessionRecordedWithXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionRecordedWithXrLabel.SizeF = new System.Drawing.SizeF(609.3751F, 58.41669F);
            this.sessionRecordedWithXrLabel.StylePriority.UseFont = false;
            this.sessionRecordedWithXrLabel.StylePriority.UseTextAlignment = false;
            this.sessionRecordedWithXrLabel.Text = "2.  სხდომის ოქმის ჩაწერა ხორციელდება სასამართლო პროცესის ფიქსაციის ტექნიკური სისტ" +
    "ემის  საშუალებით.";
            this.sessionRecordedWithXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // clerkCheckedTechnicXrLabel1
            // 
            this.clerkCheckedTechnicXrLabel1.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.clerkCheckedTechnicXrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 112.3335F);
            this.clerkCheckedTechnicXrLabel1.Multiline = true;
            this.clerkCheckedTechnicXrLabel1.Name = "clerkCheckedTechnicXrLabel1";
            this.clerkCheckedTechnicXrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.clerkCheckedTechnicXrLabel1.SizeF = new System.Drawing.SizeF(609.3751F, 58.41669F);
            this.clerkCheckedTechnicXrLabel1.StylePriority.UseFont = false;
            this.clerkCheckedTechnicXrLabel1.StylePriority.UseTextAlignment = false;
            this.clerkCheckedTechnicXrLabel1.Text = "სასამართლო სხდომის დაწყების წინ სხდომის მდივანმა მა შეამოწმა სისტემის ტექნიკური მ" +
    "დგომარეობა.";
            this.clerkCheckedTechnicXrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // diskNumberXrLabel
            // 
            this.diskNumberXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.diskNumberXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 211.2918F);
            this.diskNumberXrLabel.Multiline = true;
            this.diskNumberXrLabel.Name = "diskNumberXrLabel";
            this.diskNumberXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.diskNumberXrLabel.SizeF = new System.Drawing.SizeF(315.625F, 24.04169F);
            this.diskNumberXrLabel.StylePriority.UseFont = false;
            this.diskNumberXrLabel.StylePriority.UseTextAlignment = false;
            this.diskNumberXrLabel.Text = "ჩაწერა ხორციელდება კომპაკ-დისკზე №";
            this.diskNumberXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // diskNumberValue
            // 
            this.diskNumberValue.AutoWidth = true;
            this.diskNumberValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DiscNumber")});
            this.diskNumberValue.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.diskNumberValue.LocationFloat = new DevExpress.Utils.PointFloat(346.875F, 210.2501F);
            this.diskNumberValue.Name = "diskNumberValue";
            this.diskNumberValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.diskNumberValue.SizeF = new System.Drawing.SizeF(127.0833F, 25.08331F);
            this.diskNumberValue.StylePriority.UseFont = false;
            this.diskNumberValue.StylePriority.UseTextAlignment = false;
            this.diskNumberValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sessionClosedOnXrLabel
            // 
            this.sessionClosedOnXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.sessionClosedOnXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 262.9583F);
            this.sessionClosedOnXrLabel.Multiline = true;
            this.sessionClosedOnXrLabel.Name = "sessionClosedOnXrLabel";
            this.sessionClosedOnXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionClosedOnXrLabel.SizeF = new System.Drawing.SizeF(381.25F, 24.04169F);
            this.sessionClosedOnXrLabel.StylePriority.UseFont = false;
            this.sessionClosedOnXrLabel.StylePriority.UseTextAlignment = false;
            this.sessionClosedOnXrLabel.Text = "სასამართლო სხდომა გამოცხადდა დახურულად";
            this.sessionClosedOnXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // headOfSessionXrLabel
            // 
            this.headOfSessionXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.headOfSessionXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 327.5418F);
            this.headOfSessionXrLabel.Multiline = true;
            this.headOfSessionXrLabel.Name = "headOfSessionXrLabel";
            this.headOfSessionXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.headOfSessionXrLabel.SizeF = new System.Drawing.SizeF(192.7084F, 24.04169F);
            this.headOfSessionXrLabel.StylePriority.UseFont = false;
            this.headOfSessionXrLabel.StylePriority.UseTextAlignment = false;
            this.headOfSessionXrLabel.Text = "სხდომის თავმჯდომარე";
            this.headOfSessionXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // clerkOfSessionXrLabel
            // 
            this.clerkOfSessionXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.clerkOfSessionXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 386.9166F);
            this.clerkOfSessionXrLabel.Multiline = true;
            this.clerkOfSessionXrLabel.Name = "clerkOfSessionXrLabel";
            this.clerkOfSessionXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.clerkOfSessionXrLabel.SizeF = new System.Drawing.SizeF(170.8334F, 24.04169F);
            this.clerkOfSessionXrLabel.StylePriority.UseFont = false;
            this.clerkOfSessionXrLabel.StylePriority.UseTextAlignment = false;
            this.clerkOfSessionXrLabel.Text = "სხდომის მდივანი";
            this.clerkOfSessionXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // sessionClosedOnValue
            // 
            this.sessionClosedOnValue.AutoWidth = true;
            this.sessionClosedOnValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ClosedOn", "{0:HH:mm}")});
            this.sessionClosedOnValue.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.sessionClosedOnValue.LocationFloat = new DevExpress.Utils.PointFloat(421.25F, 261.9167F);
            this.sessionClosedOnValue.Name = "sessionClosedOnValue";
            this.sessionClosedOnValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionClosedOnValue.SizeF = new System.Drawing.SizeF(86.45831F, 25.08333F);
            this.sessionClosedOnValue.StylePriority.UseFont = false;
            this.sessionClosedOnValue.StylePriority.UseTextAlignment = false;
            this.sessionClosedOnValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // headOfSessionValue
            // 
            this.headOfSessionValue.AutoWidth = true;
            this.headOfSessionValue.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.headOfSessionValue.LocationFloat = new DevExpress.Utils.PointFloat(268.75F, 326.5002F);
            this.headOfSessionValue.Name = "headOfSessionValue";
            this.headOfSessionValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.headOfSessionValue.SizeF = new System.Drawing.SizeF(277.0834F, 25.08331F);
            this.headOfSessionValue.StylePriority.UseFont = false;
            this.headOfSessionValue.StylePriority.UseTextAlignment = false;
            this.headOfSessionValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // clerkOfSessionValue
            // 
            this.clerkOfSessionValue.AutoWidth = true;
            this.clerkOfSessionValue.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.clerkOfSessionValue.LocationFloat = new DevExpress.Utils.PointFloat(258.3333F, 385.875F);
            this.clerkOfSessionValue.Name = "clerkOfSessionValue";
            this.clerkOfSessionValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.clerkOfSessionValue.SizeF = new System.Drawing.SizeF(287.5001F, 25.08331F);
            this.clerkOfSessionValue.StylePriority.UseFont = false;
            this.clerkOfSessionValue.StylePriority.UseTextAlignment = false;
            this.clerkOfSessionValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // sessionClosedOnHourXrLabel
            // 
            this.sessionClosedOnHourXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.sessionClosedOnHourXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(534.375F, 262.9583F);
            this.sessionClosedOnHourXrLabel.Multiline = true;
            this.sessionClosedOnHourXrLabel.Name = "sessionClosedOnHourXrLabel";
            this.sessionClosedOnHourXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionClosedOnHourXrLabel.SizeF = new System.Drawing.SizeF(63.54169F, 24.04169F);
            this.sessionClosedOnHourXrLabel.StylePriority.UseFont = false;
            this.sessionClosedOnHourXrLabel.StylePriority.UseTextAlignment = false;
            this.sessionClosedOnHourXrLabel.Text = "საათზე.";
            this.sessionClosedOnHourXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // courtSessionOpenedOnValue
            // 
            this.courtSessionOpenedOnValue.AutoWidth = true;
            this.courtSessionOpenedOnValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "OpenedOn", "{0:HH:mm}")});
            this.courtSessionOpenedOnValue.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.courtSessionOpenedOnValue.LocationFloat = new DevExpress.Utils.PointFloat(268.75F, 91.33333F);
            this.courtSessionOpenedOnValue.Name = "courtSessionOpenedOnValue";
            this.courtSessionOpenedOnValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.courtSessionOpenedOnValue.SizeF = new System.Drawing.SizeF(86.45831F, 25.08333F);
            this.courtSessionOpenedOnValue.StylePriority.UseFont = false;
            this.courtSessionOpenedOnValue.StylePriority.UseTextAlignment = false;
            this.courtSessionOpenedOnValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // clerkValue
            // 
            this.clerkValue.AutoWidth = true;
            this.clerkValue.CanShrink = true;
            this.clerkValue.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.clerkValue.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 38.20832F);
            this.clerkValue.Name = "clerkValue";
            this.clerkValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.clerkValue.SizeF = new System.Drawing.SizeF(250F, 25.08333F);
            this.clerkValue.StylePriority.UseFont = false;
            this.clerkValue.StylePriority.UseTextAlignment = false;
            this.clerkValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // judgeValue
            // 
            this.judgeValue.AutoWidth = true;
            this.judgeValue.CanShrink = true;
            this.judgeValue.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.judgeValue.LocationFloat = new DevExpress.Utils.PointFloat(290.625F, 10.00001F);
            this.judgeValue.Name = "judgeValue";
            this.judgeValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.judgeValue.SizeF = new System.Drawing.SizeF(281.2501F, 25.08333F);
            this.judgeValue.StylePriority.UseFont = false;
            this.judgeValue.StylePriority.UseTextAlignment = false;
            this.judgeValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // courtSessionAttendedXrLabel
            // 
            this.courtSessionAttendedXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.courtSessionAttendedXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 138.2916F);
            this.courtSessionAttendedXrLabel.Name = "courtSessionAttendedXrLabel";
            this.courtSessionAttendedXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.courtSessionAttendedXrLabel.SizeF = new System.Drawing.SizeF(350.0001F, 25.08333F);
            this.courtSessionAttendedXrLabel.StylePriority.UseFont = false;
            this.courtSessionAttendedXrLabel.StylePriority.UseTextAlignment = false;
            this.courtSessionAttendedXrLabel.Text = "1.  სასამართლო სხდომაზე გამოცხადდნენ:";
            this.courtSessionAttendedXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // onHourXrLabel
            // 
            this.onHourXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.onHourXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(416.0416F, 91.33333F);
            this.onHourXrLabel.Name = "onHourXrLabel";
            this.onHourXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.onHourXrLabel.SizeF = new System.Drawing.SizeF(91.66666F, 25.08331F);
            this.onHourXrLabel.StylePriority.UseFont = false;
            this.onHourXrLabel.StylePriority.UseTextAlignment = false;
            this.onHourXrLabel.Text = "საათზე";
            this.onHourXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // courtSessionOpenedOnXrLabel
            // 
            this.courtSessionOpenedOnXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.courtSessionOpenedOnXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 91.33333F);
            this.courtSessionOpenedOnXrLabel.Name = "courtSessionOpenedOnXrLabel";
            this.courtSessionOpenedOnXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.courtSessionOpenedOnXrLabel.SizeF = new System.Drawing.SizeF(250F, 25.08333F);
            this.courtSessionOpenedOnXrLabel.StylePriority.UseFont = false;
            this.courtSessionOpenedOnXrLabel.StylePriority.UseTextAlignment = false;
            this.courtSessionOpenedOnXrLabel.Text = "სასამართლო სხდომა გაიხსნა";
            this.courtSessionOpenedOnXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // clerkXrLabel
            // 
            this.clerkXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F);
            this.clerkXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(290.625F, 38.20832F);
            this.clerkXrLabel.Name = "clerkXrLabel";
            this.clerkXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.clerkXrLabel.SizeF = new System.Drawing.SizeF(152.0833F, 25.08333F);
            this.clerkXrLabel.StylePriority.UseFont = false;
            this.clerkXrLabel.StylePriority.UseTextAlignment = false;
            this.clerkXrLabel.Text = "ს მდივნობით";
            this.clerkXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // judgeXrLabel
            // 
            this.judgeXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.judgeXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.judgeXrLabel.Name = "judgeXrLabel";
            this.judgeXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.judgeXrLabel.SizeF = new System.Drawing.SizeF(152.0833F, 25.08333F);
            this.judgeXrLabel.StylePriority.UseFont = false;
            this.judgeXrLabel.StylePriority.UseTextAlignment = false;
            this.judgeXrLabel.Text = "მოსამართლე:";
            this.judgeXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 17.70833F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 2.708435F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // courtSessionReportXrLabel
            // 
            this.courtSessionReportXrLabel.Font = new System.Drawing.Font("Sylfaen", 15F, System.Drawing.FontStyle.Bold);
            this.courtSessionReportXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(154.1667F, 10.00001F);
            this.courtSessionReportXrLabel.Multiline = true;
            this.courtSessionReportXrLabel.Name = "courtSessionReportXrLabel";
            this.courtSessionReportXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.courtSessionReportXrLabel.SizeF = new System.Drawing.SizeF(334.3749F, 58.41666F);
            this.courtSessionReportXrLabel.StylePriority.UseFont = false;
            this.courtSessionReportXrLabel.StylePriority.UseTextAlignment = false;
            this.courtSessionReportXrLabel.Text = "ს ა ს ა მ ა რ თ ლ ო  ს ხ დ ო მ ი ს  ო ქ მ ი";
            this.courtSessionReportXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.sessionNumberValue,
            this.sessionDateXrLabel,
            this.coutNameXrLabel,
            this.courtRaionXrLabel,
            this.sessionNumberXrLabel,
            this.courtSessionReportXrLabel});
            this.ReportHeader.HeightF = 212.1667F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // sessionNumberValue
            // 
            this.sessionNumberValue.AutoWidth = true;
            this.sessionNumberValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Number")});
            this.sessionNumberValue.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.sessionNumberValue.LocationFloat = new DevExpress.Utils.PointFloat(319.7917F, 80.91666F);
            this.sessionNumberValue.Name = "sessionNumberValue";
            this.sessionNumberValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionNumberValue.SizeF = new System.Drawing.SizeF(107.2917F, 24.04165F);
            this.sessionNumberValue.StylePriority.UseFont = false;
            this.sessionNumberValue.StylePriority.UseTextAlignment = false;
            this.sessionNumberValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sessionDateXrLabel
            // 
            this.sessionDateXrLabel.AutoWidth = true;
            this.sessionDateXrLabel.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "todayCalculatedField", "{0:yyyy \'წლის\' dd MM, dddd}")});
            this.sessionDateXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.sessionDateXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(319.7917F, 120.875F);
            this.sessionDateXrLabel.Name = "sessionDateXrLabel";
            this.sessionDateXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionDateXrLabel.SizeF = new System.Drawing.SizeF(309.375F, 31.25001F);
            this.sessionDateXrLabel.StylePriority.UseFont = false;
            this.sessionDateXrLabel.StylePriority.UseTextAlignment = false;
            this.sessionDateXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // coutNameXrLabel
            // 
            this.coutNameXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.coutNameXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(154.1666F, 158.3334F);
            this.coutNameXrLabel.Name = "coutNameXrLabel";
            this.coutNameXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.coutNameXrLabel.SizeF = new System.Drawing.SizeF(334.375F, 43.83333F);
            this.coutNameXrLabel.StylePriority.UseFont = false;
            this.coutNameXrLabel.Text = "თბილისის საქალაქო სასამართლო";
            this.coutNameXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // courtRaionXrLabel
            // 
            this.courtRaionXrLabel.CanGrow = false;
            this.courtRaionXrLabel.CanShrink = true;
            this.courtRaionXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.courtRaionXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(79.79177F, 120.875F);
            this.courtRaionXrLabel.Name = "courtRaionXrLabel";
            this.courtRaionXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.courtRaionXrLabel.SizeF = new System.Drawing.SizeF(144.7917F, 31.25002F);
            this.courtRaionXrLabel.StylePriority.UseBorderDashStyle = false;
            this.courtRaionXrLabel.StylePriority.UseBorderWidth = false;
            this.courtRaionXrLabel.StylePriority.UseFont = false;
            this.courtRaionXrLabel.StylePriority.UseTextAlignment = false;
            this.courtRaionXrLabel.Text = "ქ. თბილისი";
            this.courtRaionXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // sessionNumberXrLabel
            // 
            this.sessionNumberXrLabel.Font = new System.Drawing.Font("Sylfaen", 12F, System.Drawing.FontStyle.Bold);
            this.sessionNumberXrLabel.LocationFloat = new DevExpress.Utils.PointFloat(203.125F, 80.91666F);
            this.sessionNumberXrLabel.Name = "sessionNumberXrLabel";
            this.sessionNumberXrLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.sessionNumberXrLabel.SizeF = new System.Drawing.SizeF(77.08334F, 24.04165F);
            this.sessionNumberXrLabel.StylePriority.UseFont = false;
            this.sessionNumberXrLabel.StylePriority.UseTextAlignment = false;
            this.sessionNumberXrLabel.Text = "საქმე №";
            this.sessionNumberXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // todayCalculatedField
            // 
            this.todayCalculatedField.Expression = "Now()";
            this.todayCalculatedField.Name = "todayCalculatedField";
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(JsonDatabaseModels.Session);
            // 
            // SessionReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.todayCalculatedField});
            this.DataSource = this.bindingSource1;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 18, 3);
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Version = "14.2";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel courtSessionReportXrLabel;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel clerkXrLabel;
        private DevExpress.XtraReports.UI.XRLabel judgeXrLabel;
        private DevExpress.XtraReports.UI.XRLabel coutNameXrLabel;
        private DevExpress.XtraReports.UI.XRLabel courtRaionXrLabel;
        private DevExpress.XtraReports.UI.XRLabel sessionNumberXrLabel;
        private DevExpress.XtraReports.UI.XRLabel clerkOfSessionXrLabel;
        private DevExpress.XtraReports.UI.XRLabel headOfSessionXrLabel;
        private DevExpress.XtraReports.UI.XRLabel sessionClosedOnHourXrLabel;
        private DevExpress.XtraReports.UI.XRLabel sessionClosedOnXrLabel;
        private DevExpress.XtraReports.UI.XRLabel diskNumberXrLabel;
        private DevExpress.XtraReports.UI.XRLabel clerkCheckedTechnicXrLabel1;
        private DevExpress.XtraReports.UI.XRLabel courtSessionAttendedXrLabel;
        private DevExpress.XtraReports.UI.XRLabel onHourXrLabel;
        private DevExpress.XtraReports.UI.XRLabel courtSessionOpenedOnXrLabel;
        private DevExpress.XtraReports.UI.XRLabel sessionDateXrLabel;
        private DevExpress.XtraReports.UI.XRLabel sessionNumberValue;
        private DevExpress.XtraReports.UI.XRLabel courtSessionOpenedOnValue;
        private DevExpress.XtraReports.UI.XRLabel clerkValue;
        private DevExpress.XtraReports.UI.XRLabel judgeValue;
        private DevExpress.XtraReports.UI.XRLabel sessionClosedOnValue;
        private DevExpress.XtraReports.UI.XRLabel headOfSessionValue;
        private DevExpress.XtraReports.UI.XRLabel diskNumberValue;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraReports.UI.XRLabel clerkOfSessionValue;
        private DevExpress.XtraReports.UI.CalculatedField todayCalculatedField;
        private DevExpress.XtraReports.UI.XRPanel footerXrPanel;
        private DevExpress.XtraReports.UI.XRLabel sessionRecordedWithXrLabel;
        private DevExpress.XtraReports.UI.XRPanel participantsXrPanel;
    }
}
