﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CourtProcessRecorder
{
    public partial class WriteToDiscForm : XtraForm
    {
        private string _sessionName;

        public WriteToDiscForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            discNumberTextEdit.Text = DiscNumber;
        }

        public string DiscNumber { get; set; }

        public string SessionName { get { return _sessionName; } }

        private void okSimpleButton_Click(object sender, EventArgs e)
        {
            DiscNumber = discNumberTextEdit.Text;

            //if (string.IsNullOrWhiteSpace(DiscNumber))
            //{
            //    XtraMessageBox.Show("დისკის ნომერი არ შეიძლება იყოს ცარიელი!", "გაფრთხილება", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //    return;
            //}

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void cancelSimpleButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}