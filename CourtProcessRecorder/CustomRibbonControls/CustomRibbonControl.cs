﻿// Developer Express Code Central Example:
// How to align a RibbonPageGroup to the right side of a page?
// 
// This example demonstrates how to create a custom RibbonControl that displays a
// group with the "AlignRight" tag assigned at the right side.
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E2869

using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.ViewInfo;

namespace CourtProcessRecorder.CustomRibbonControls
{
    [System.ComponentModel.DesignerCategory("")]
    public class CustomRibbonControl : RibbonControl
    {

        protected override RibbonViewInfo CreateViewInfo()
        {
            return new CustomRibbonViewInfo(this);
        }
    }
}
