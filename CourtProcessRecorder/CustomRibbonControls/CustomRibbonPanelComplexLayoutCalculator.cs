﻿using System.Drawing;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.ViewInfo;

namespace CourtProcessRecorder.CustomRibbonControls
{
    public class CustomRibbonPanelComplexLayoutCalculator : RibbonPanelComplexLayoutCalculator
    {
        public CustomRibbonPanelComplexLayoutCalculator(RibbonPanelViewInfo panelInfo)
            : base(panelInfo)
        {
 
        }

        public override void UpdatePanelLayout()
        {
            var xPos = ContentBounds.Left - PanelInfo.PanelScrollOffset;
            var rAx = 0;
            for (var i = 0; i < PanelInfo.Groups.Count; i++)
            {
                var pageGroup = PanelInfo.Groups[i].PageGroup;

                var temp = (PanelInfo.Groups[i] as CustomRibbonPageGroupViewInfo)?.NewCalcViewInfo(new Rectangle(xPos, ContentBounds.Top, PanelInfo.Groups[i].PrecalculatedWidth, ContentBounds.Height), rAx);
                if (temp== null) continue;
                
                if (object.Equals(pageGroup.Tag, "AlignRight"))
                {
                    rAx = temp.Value;
                    continue;
                }
             
                xPos += PanelInfo.Groups[i].PrecalculatedWidth + PanelInfo.DefaultIndentBetweenGroups;
            }
        }
    }
}
