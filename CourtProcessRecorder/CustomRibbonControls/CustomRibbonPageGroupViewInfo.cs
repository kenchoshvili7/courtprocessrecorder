﻿// Developer Express Code Central Example:
// How to align a RibbonPageGroup to the right side of a page?
// 
// This example demonstrates how to create a custom RibbonControl that displays a
// group with the "AlignRight" tag assigned at the right side.
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E2869

using System.Drawing;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.ViewInfo;

namespace CourtProcessRecorder.CustomRibbonControls
{
    public class CustomRibbonPageGroupViewInfo : RibbonPageGroupViewInfo
    {
        public CustomRibbonPageGroupViewInfo(RibbonViewInfo viewInfo, RibbonPageGroup group)
            : base(viewInfo, group)
        {

        }


        public int NewCalcViewInfo(Rectangle bounds, int rAX)
        {
            if (object.Equals(PageGroup.Tag, "AlignRight"))
            {

                int offset = rAX == 0 ? ViewInfo.Bounds.Right - bounds.Right : rAX - bounds.Right;
                if (offset > 5)
                    bounds.Offset(offset, 0);
            }

            CalcViewInfo(bounds);

            return bounds.X;
        }

        public override void CalcViewInfo(Rectangle bounds)
        {
            

            base.CalcViewInfo(bounds);
        }
    }
}
