﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AudioConfiguration;
using DevExpress.XtraEditors;
using DeviceController;
using HelperClasses;
using JsonDatabaseModels;
using RecordProvider;

namespace CourtProcessRecorder
{
    public partial class ConfigurationForm : XtraForm
    {
        private RecorderConfiguration _configuration;
        private string _archiveDirectory;

        public ConfigurationForm()
        {
            InitializeComponent();

            globalModeComboBoxEdit.Properties.Items.AddEnum(typeof(GlobalMode));

            LoadConfiguarion();

            var recordingDevices = AudioDevice.GetRecordingDevices();
            recordingDeviceLookUpEdit.Properties.DataSource = recordingDevices;
            recordingDeviceLookUpEdit.EditValue = recordingDevices.FirstOrDefault(d => d.DeviceId == _configuration.DeviceNo);
            //recordingDeviceLookUpEdit.EditValue = recordingDevices.FirstOrDefault(d => d.DeviceId == 2147483633); // old 

            configurationFolderTextEdit.Text = _archiveDirectory;

            //globalModeComboBoxEdit.EditValue = GlobalMode.Editor;
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            if (this.Parent != null)
                WindowState = FormWindowState.Maximized;
        }

        private void LoadConfiguarion()
        {
            _configuration = ConfigurationHelper.GetConfigurationWithAllChannels();

            if (_configuration == null)
            {
                _configuration = new RecorderConfiguration
                {
                    UId = Guid.NewGuid(),
                    CreatedOn = DateTime.Now,
                    Name = "DefaultConfig",
                    Format = new WaveFormatEx
                    {
                        wFormatTag = 1,
                        nChannels = 1,
                        nSamplesPerSec = 16000,
                        nAvgBytesPerSec = 32000,
                        nBlockAlign = 2,
                        wBitsPerSample = 16,
                        cbSize = 0,
                        ExtraData = new byte[0]
                    },
                    Channels = new List<Channel>
                    {
                        new Channel {ChannelNo = AudioSourceChannels.Channel1, RecordVolume = 100},
                        new Channel {ChannelNo = AudioSourceChannels.Channel2, RecordVolume = 100},
                        new Channel {ChannelNo = AudioSourceChannels.Channel3, RecordVolume = 100},
                        new Channel {ChannelNo = AudioSourceChannels.Channel4, RecordVolume = 100},
                        new Channel {ChannelNo = AudioSourceChannels.Channel5, RecordVolume = 100},
                        new Channel {ChannelNo = AudioSourceChannels.Channel6, RecordVolume = 100},
                        new Channel {ChannelNo = AudioSourceChannels.Channel7, RecordVolume = 100},
                        new Channel {ChannelNo = AudioSourceChannels.Channel8, RecordVolume = 100}
                    },
                    ProgramConfiguration = ProgramConfiguration.MultiChannel,
                    GlobalMode = GlobalMode.Recorder,
                    DeleteConfirmationIsOn = true,
                    HallVolumeLevel = 255
                };

                ConfigurationHelper.CreateConfiguration(_configuration);
            }
            else _archiveDirectory = _configuration.ArchiveDirectory;

            channelConfigurationGridControl.DataSource = _configuration.Channels;
            channelConfigurationGridControl.RefreshDataSource();

            globalModeComboBoxEdit.EditValue = _configuration.GlobalMode;
            hallVolumeLevelTrackBar.EditValue = _configuration.HallVolumeLevel ?? 255;

            if (_configuration.DeleteConfirmationIsOn == null) _configuration.DeleteConfirmationIsOn = true;
            deleteConfirmationCheck.Checked = (bool)_configuration.DeleteConfirmationIsOn;

            courtNameTextEdit.Text = _configuration.CourtName;
            cityNameTextEdit.Text = _configuration.CityName;
        }

        private void saveConfiguration_Click(object sender, EventArgs e)
        {
            _configuration.ArchiveDirectory = _archiveDirectory;
            _configuration.HallVolumeLevel = (int)hallVolumeLevelTrackBar.EditValue;
            _configuration.DeleteConfirmationIsOn = deleteConfirmationCheck.Checked;
            _configuration.CourtName = courtNameTextEdit.Text;
            _configuration.CityName = cityNameTextEdit.Text;

            var audioDevice = recordingDeviceLookUpEdit.EditValue as AudioDevice;
            if (audioDevice != null)
            {
                _configuration.DeviceNo = audioDevice.DeviceId;
                _configuration.DriverName = audioDevice.DeviceName;
            }

            if (_configuration.Channels.Any(c => c.IsActive && string.IsNullOrWhiteSpace(c.Name)))
            {
                XtraMessageBox.Show("დასახელება არ შეიძლება იყოს ცარიელი!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            foreach (var channel in _configuration.Channels.Where(ch => ch.IsActive))
            {
                if (_configuration.Channels.Count(c => c.IsActive && c.Name.Equals(channel.Name)) <= 1) continue;
                XtraMessageBox.Show("დასახელებები უნდა იყოს განსხვავებული!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            _configuration.GlobalMode = (GlobalMode)Enum.Parse(typeof(GlobalMode), globalModeComboBoxEdit.EditValue.ToString());

            ConfigurationHelper.UpdateConfiguration(_configuration);

            DialogResult = DialogResult.OK;

            warningTextLabelControl.Text = @"კონფიგურაციის ასახვისთვის საჭიროა პროგრამის გადატვირთვა";
        }

        private void configurationBrowseSimpleButton_Click(object sender, EventArgs e)
        {
            if (configurationDirectoryFolderBrowserDialog.ShowDialog() != DialogResult.OK) return;
            _archiveDirectory = configurationDirectoryFolderBrowserDialog.SelectedPath;
            configurationFolderTextEdit.Text = _archiveDirectory;
        }
    }
}

