﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CourtProcessRecorder
{
    public partial class AddPosition : XtraForm
    {
        private string _positionName;

        public AddPosition()
        {
            InitializeComponent();

            nameTextEdit.Text = PositionName;

            nameTextEdit.KeyPress += OnEnterPress;
        }

        public string PositionName { get { return _positionName; } }

        public void SetPositionName(string positionName)
        {
            nameTextEdit.Text = positionName;
        }

        private void okSimpleButton_Click(object sender, EventArgs e)
        {
            ExecuteAction();
        }

        private void OnEnterPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                ExecuteAction();
        }

        private void ExecuteAction()
        {
            _positionName = nameTextEdit.Text;

            if (string.IsNullOrWhiteSpace(PositionName))
            {
                XtraMessageBox.Show("დასახელება არ შეიძლება იყოს ცარიელი!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void cancelSimpleButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
