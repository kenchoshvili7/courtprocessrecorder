﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JsonDatabaseModels;

namespace CourtProcessRecorder.TemplateEdit
{
    public partial class AddRenameTemplate : XtraForm
    {
        public string TemplateName { get; private set; }
        public SessionTemplateType SessionTemplateType { get; private set; }

        public AddRenameTemplate()
        {
            InitializeComponent();

            nameTextEdit.Text = TemplateName;

            sessionTemplateTypeComboBox.Properties.Items.AddEnum(typeof(SessionTemplateType));
        }

        public void Initialize(string templateName, SessionTemplateType? sessionTemplateType)
        {
            nameTextEdit.Text = templateName;
            sessionTemplateTypeComboBox.EditValue = sessionTemplateType;
        }

        private void okSimpleButton_Click(object sender, EventArgs e)
        {
            TemplateName = nameTextEdit.Text;
            SessionTemplateType = (SessionTemplateType)sessionTemplateTypeComboBox.EditValue;

            if (string.IsNullOrWhiteSpace(TemplateName))
            {
                XtraMessageBox.Show("დასახელება არ შეიძლება იყოს ცარიელი!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void cancelSimpleButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
