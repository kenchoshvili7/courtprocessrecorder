﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CourtProcessRecorder.Reports;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using HelperClasses;
using JsonDatabaseModels;

namespace CourtProcessRecorder.TemplateEdit
{
    public partial class TemplateEditForm : RibbonForm
    {
        private readonly DatabaseHelper _databaseHelper;
        SessionTemplate _currentTemplate;

        public TemplateEditForm()
        {
            InitializeComponent();

            IsMdiContainer = false;
            ribbon.MdiMergeStyle = RibbonMdiMergeStyle.Always;

            Activated += OnActivated;
            this.WindowState = FormWindowState.Maximized;

            _databaseHelper = DatabaseHelper.DatabaseHelperInstance();

            this.Shown += OnShown;

            this.KeyPreview = true;
            this.KeyDown += OnKeyDown;

            //LoadTemplates();

            actionsGridView.CellValueChanged += ActionsGridViewOnCellValueChanged;
            positionsGridView.CellValueChanged += PositionsGridViewOnCellValueChanged;

            templatesRepositoryItemLookUpEdit.EditValueChanged += TemplatesRepositoryItemLookUpEditOnEditValueChanged;

            templateRichEditControl.DocumentLoaded += RichEditControlOnDocumentLoaded;
            positionsGridView.DoubleClick += PositionsGridViewOnDoubleClick;
            fieldsGridView.DoubleClick += FieldsGridViewOnDoubleClick;

            fieldsGridControl.DataSource = SessionReportField.GetSessionReportFields();

            var sessionTemplateType = ConfigurationHelper.GetConfiguration().SessionTemplateType;
            sessionTemplateTypeComboBox.Items.AddEnum(typeof(SessionTemplateType));
            sessionTemplateTypeBarEditItem.EditValue = sessionTemplateType;
            sessionTemplateTypeBarEditItem.EditValueChanged += SessionTemplateTypeBarEditItemOnEditValueChanged;

            LoadTemplates(sessionTemplateType);

            LoadTemplateDocument(sessionTemplateType);
        }

        private void OnShown(object sender, EventArgs e)
        {
            ribbon.Select();
            ribbon.SelectedPage = ribbon.Pages[0];
        }

        private void OnActivated(object sender, EventArgs eventArgs)
        {
            this.WindowState = FormWindowState.Maximized;

            //sessionsGridControl.DataSource = _databaseHelper.GetSessions();
            //
            //sessionsGridControl.RefreshDataSource();
        }

        private void LoadTemplates(SessionTemplateType? sessionTemplateType = null)
        {
            var templates = sessionTemplateType == null ? _databaseHelper.GetSessionTemplates()
                                                                   : _databaseHelper.GetSessionTemplates().Where(t => t.SessionTemplateType == sessionTemplateType).ToList();

            templatesRepositoryItemLookUpEdit.DataSource = templates;

            _currentTemplate = templates.FirstOrDefault();
            templatesLookupEdit.EditValue = _currentTemplate;

            RefreshDataSource();
        }

        private void TemplatesRepositoryItemLookUpEditOnEditValueChanged(object sender, EventArgs e)
        {
            var lookUpEdit = sender as LookUpEdit;
            _currentTemplate = lookUpEdit.GetSelectedDataRow() as SessionTemplate;

            RefreshDataSource();
        }

        private void SessionTemplateTypeBarEditItemOnEditValueChanged(object sender, EventArgs eventArgs)
        {
            var sessionTemplateType = (SessionTemplateType)sessionTemplateTypeBarEditItem.EditValue;
            LoadTemplates(sessionTemplateType);
            LoadTemplateDocument(sessionTemplateType);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) { }
        }

        private void addTemplateBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            var addRenameTemplate = new AddRenameTemplate
            {
                StartPosition = FormStartPosition.CenterParent,
                Text = @"შაბლონის დამატება"
            };

            if (addRenameTemplate.ShowDialog() != DialogResult.OK) return;

            var template = new SessionTemplate
            {
                Name = addRenameTemplate.TemplateName,
                SessionTemplateType = addRenameTemplate.SessionTemplateType,
                SessionActions = new List<SessionAction>(),
                Positions = new List<Position>(),
                CreatedOn = DateTime.Now,
                UId = Guid.NewGuid()
            };

            _databaseHelper.AddSessionTemplate(template);

            var templates = _databaseHelper.GetSessionTemplates();

            _currentTemplate = templates.FirstOrDefault(t => t.UId == template.UId);

            templatesRepositoryItemLookUpEdit.DataSource = templates;
            templatesLookupEdit.EditValue = _currentTemplate;

            RefreshDataSource();
        }

        private void renameTemplateBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_currentTemplate == null) return;

            var addRenameTemplate = new AddRenameTemplate
            {
                StartPosition = FormStartPosition.CenterParent,
                Text = @"შაბლონის რედაქტირება",
            };

            addRenameTemplate.Initialize(_currentTemplate.Name, _currentTemplate.SessionTemplateType);

            if (addRenameTemplate.ShowDialog() != DialogResult.OK) return;

            _currentTemplate.Name = addRenameTemplate.TemplateName;
            _currentTemplate.SessionTemplateType = addRenameTemplate.SessionTemplateType;

            _databaseHelper.UpdateSessionTemplate(_currentTemplate);

            var templates = _databaseHelper.GetSessionTemplates();

            _currentTemplate = templates.FirstOrDefault(t => t.UId == _currentTemplate.UId);

            templatesRepositoryItemLookUpEdit.DataSource = templates;
            templatesLookupEdit.EditValue = _currentTemplate;

            RefreshDataSource();
        }

        private void deleteTemplateBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_currentTemplate == null) return;

            var result = XtraMessageBox.Show("ნამდვილად გსურთ წაშლა?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result != DialogResult.Yes) return;

            _databaseHelper.DeleteSessionTemplate(_currentTemplate);

            var templates = _databaseHelper.GetSessionTemplates();
            _currentTemplate = templates.FirstOrDefault();

            templatesRepositoryItemLookUpEdit.DataSource = templates;
            templatesLookupEdit.EditValue = _currentTemplate;

            RefreshDataSource();
        }

        private void addActionBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_currentTemplate == null) return;

            _currentTemplate.SessionActions.Add(new SessionAction
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now
            });

            _databaseHelper.UpdateSessionTemplate(_currentTemplate);

            _currentTemplate = _databaseHelper.GetSessionTemplates().FirstOrDefault(t => t.UId == _currentTemplate.UId);

            RefreshDataSource();
        }

        private void deleteActionBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_currentTemplate == null || !_currentTemplate.SessionActions.Any()) return;

            var result = XtraMessageBox.Show("ნამდვილად გსურთ წაშლა?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result != DialogResult.Yes) return;

            var focusedRowValue = actionsGridView.GetFocusedRow();
            if (focusedRowValue == null) return;

            var sessionAction = focusedRowValue as SessionAction;

            _currentTemplate.SessionActions.RemoveAll(a => a.UId == sessionAction.UId);

            _databaseHelper.UpdateSessionTemplate(_currentTemplate);

            _currentTemplate = _databaseHelper.GetSessionTemplates().FirstOrDefault(t => t.UId == _currentTemplate.UId);

            RefreshDataSource();
        }

        private void addPositionBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_currentTemplate == null) return;

            _currentTemplate.Positions.Add(new Position
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now
            });

            _databaseHelper.UpdateSessionTemplate(_currentTemplate);

            _currentTemplate = _databaseHelper.GetSessionTemplates().FirstOrDefault(t => t.UId == _currentTemplate.UId);

            RefreshDataSource();
        }

        private void deletePositionBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_currentTemplate == null || !_currentTemplate.Positions.Any()) return;

            var result = XtraMessageBox.Show("ნამდვილად გსურთ წაშლა?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result != DialogResult.Yes) return;

            var focusedRowValue = positionsGridView.GetFocusedRow();
            if (focusedRowValue == null) return;

            var position = focusedRowValue as Position;

            _currentTemplate.Positions.RemoveAll(a => a.UId == position.UId);

            _databaseHelper.UpdateSessionTemplate(_currentTemplate);

            _currentTemplate = _databaseHelper.GetSessionTemplates().FirstOrDefault(t => t.UId == _currentTemplate.UId);

            RefreshDataSource();
        }

        private void ActionsGridViewOnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            var focusedRowValue = actionsGridView.GetFocusedRow();
            if (focusedRowValue == null) return;

            var selectedSessionAction = focusedRowValue as SessionAction;

            var sessionAction = _currentTemplate.SessionActions.FirstOrDefault(s => s.UId == selectedSessionAction.UId);

            sessionAction.Description = selectedSessionAction.Description;

            _databaseHelper.UpdateSessionTemplate(_currentTemplate);

            _currentTemplate = _databaseHelper.GetSessionTemplates().FirstOrDefault(t => t.UId == _currentTemplate.UId);

            RefreshDataSource();
        }

        private void PositionsGridViewOnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            var focusedRowValue = positionsGridView.GetFocusedRow();
            if (focusedRowValue == null) return;

            var selectedPosition = focusedRowValue as Position;

            var position = _currentTemplate.Positions.FirstOrDefault(p => p.UId == selectedPosition.UId);

            position.Name = selectedPosition.Name;

            _databaseHelper.UpdateSessionTemplate(_currentTemplate);

            _currentTemplate = _databaseHelper.GetSessionTemplates().FirstOrDefault(t => t.UId == _currentTemplate.UId);

            RefreshDataSource();
        }

        private void RefreshDataSource()
        {
            actionsGridControl.DataSource = _currentTemplate?.SessionActions;
            positionsGridControl.DataSource = _currentTemplate?.Positions;
        }

        private void RichEditControlOnDocumentLoaded(object sender, EventArgs eventArgs)
        {
            templateRichEditControl.SelectAll();

            var selection = templateRichEditControl.Document.Selection;

            var document = selection.BeginUpdateDocument();
            var characters = document.BeginUpdateCharacters(selection);
            characters.FontName = "Sylfaen";
            document.EndUpdateCharacters(characters);
            selection.EndUpdateDocument(document);

            var myStart = templateRichEditControl.Document.CreatePosition(0);
            var myRange = templateRichEditControl.Document.CreateRange(myStart, 0);

            templateRichEditControl.Document.Selection = myRange;
        }

        private void LoadTemplateDocument(SessionTemplateType? sessionTemplateType = null)
        {
            //var templatePath = Path.Combine(ConstantStrings.DatabasePath, ConstantStrings.DatabaseName, "Template.rtf");

            //var buffer = File.ReadAllBytes(templatePath);

            //var base64 = Convert.ToBase64String(buffer);

            //DatabaseHelper.DatabaseHelperInstance.ReplaceTemplateDocument(new TemplateDocument { DocumentStream = base64 });

            var documentStream = DatabaseHelper.DatabaseHelperInstance().GetTemlateDocument(sessionTemplateType)?.DocumentStream;
            if (documentStream == null) return;

            var ms = new MemoryStream(Convert.FromBase64String(documentStream)) { Position = 0 };

            templateRichEditControl.LoadDocument(ms, DocumentFormat.Rtf);
        }

        private void PositionsGridViewOnDoubleClick(object sender, EventArgs eventArgs)
        {
            var gridView = sender as GridView;
            if (gridView == null) return;
            var position = gridView.GetFocusedRow() as Position;

            if (position == null) return;

            var text = "[" + position.Name + "]";

            InsertTextAtCurrentPos(text);
        }

        private void FieldsGridViewOnDoubleClick(object sender, EventArgs eventArgs)
        {
            var gridView = sender as GridView;
            if (gridView == null) return;
            var sessionReportField = gridView.GetFocusedRow() as SessionReportField;

            InsertTextAtCurrentPos(sessionReportField?.FieldValue);
        }

        private void InsertTextAtCurrentPos(string text)
        {
            var pos = templateRichEditControl.Document.CaretPosition;
            var doc = pos.BeginUpdateDocument();
            doc.InsertText(pos, text);
            pos.EndUpdateDocument(doc);
        }

        private void saveBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            var ms = new MemoryStream();

            templateRichEditControl.Document.SaveDocument(ms, DocumentFormat.Rtf);

            ms.Position = 0;

            var base64 = Convert.ToBase64String(ms.ToArray());

            var sessionTemplateType = (SessionTemplateType)sessionTemplateTypeBarEditItem.EditValue;

            DatabaseHelper.DatabaseHelperInstance().ReplaceTemplateDocument(new TemplateDocument { DocumentStream = base64, SessionTemplateType = sessionTemplateType });
        }
    }
}