﻿namespace CourtProcessRecorder.TemplateEdit
{
    partial class AddRenameTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.nameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.sessionTemplateTypeLabel = new DevExpress.XtraEditors.LabelControl();
            this.sessionTemplateTypeComboBox = new DevExpress.XtraEditors.ImageComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.nameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTemplateTypeComboBox.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabelControl
            // 
            this.nameLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.nameLabelControl.Location = new System.Drawing.Point(12, 11);
            this.nameLabelControl.Name = "nameLabelControl";
            this.nameLabelControl.Size = new System.Drawing.Size(66, 17);
            this.nameLabelControl.TabIndex = 0;
            this.nameLabelControl.Text = "დასახელება";
            // 
            // nameTextEdit
            // 
            this.nameTextEdit.Location = new System.Drawing.Point(12, 34);
            this.nameTextEdit.Name = "nameTextEdit";
            this.nameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.nameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.nameTextEdit.Size = new System.Drawing.Size(326, 22);
            this.nameTextEdit.TabIndex = 1;
            // 
            // okSimpleButton
            // 
            this.okSimpleButton.Location = new System.Drawing.Point(12, 137);
            this.okSimpleButton.Name = "okSimpleButton";
            this.okSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.okSimpleButton.TabIndex = 2;
            this.okSimpleButton.Text = "OK";
            this.okSimpleButton.Click += new System.EventHandler(this.okSimpleButton_Click);
            // 
            // cancelSimpleButton
            // 
            this.cancelSimpleButton.Location = new System.Drawing.Point(263, 137);
            this.cancelSimpleButton.Name = "cancelSimpleButton";
            this.cancelSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.cancelSimpleButton.TabIndex = 3;
            this.cancelSimpleButton.Text = "გაუქმება";
            this.cancelSimpleButton.Click += new System.EventHandler(this.cancelSimpleButton_Click);
            // 
            // sessionTemplateTypeLabel
            // 
            this.sessionTemplateTypeLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionTemplateTypeLabel.Location = new System.Drawing.Point(12, 72);
            this.sessionTemplateTypeLabel.Name = "sessionTemplateTypeLabel";
            this.sessionTemplateTypeLabel.Size = new System.Drawing.Size(29, 17);
            this.sessionTemplateTypeLabel.TabIndex = 0;
            this.sessionTemplateTypeLabel.Text = "ტიპი";
            // 
            // sessionTemplateTypeComboBox
            // 
            this.sessionTemplateTypeComboBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sessionTemplateTypeComboBox.Location = new System.Drawing.Point(12, 95);
            this.sessionTemplateTypeComboBox.Name = "sessionTemplateTypeComboBox";
            this.sessionTemplateTypeComboBox.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionTemplateTypeComboBox.Properties.Appearance.Options.UseFont = true;
            this.sessionTemplateTypeComboBox.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sessionTemplateTypeComboBox.Properties.AppearanceDropDown.Options.UseFont = true;
            this.sessionTemplateTypeComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sessionTemplateTypeComboBox.Properties.DropDownRows = 3;
            this.sessionTemplateTypeComboBox.Size = new System.Drawing.Size(326, 22);
            this.sessionTemplateTypeComboBox.TabIndex = 12;
            // 
            // AddRenameTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 172);
            this.Controls.Add(this.sessionTemplateTypeComboBox);
            this.Controls.Add(this.cancelSimpleButton);
            this.Controls.Add(this.okSimpleButton);
            this.Controls.Add(this.nameTextEdit);
            this.Controls.Add(this.sessionTemplateTypeLabel);
            this.Controls.Add(this.nameLabelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddRenameTemplate";
            ((System.ComponentModel.ISupportInitialize)(this.nameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTemplateTypeComboBox.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl nameLabelControl;
        private DevExpress.XtraEditors.TextEdit nameTextEdit;
        private DevExpress.XtraEditors.SimpleButton okSimpleButton;
        private DevExpress.XtraEditors.SimpleButton cancelSimpleButton;
        private DevExpress.XtraEditors.LabelControl sessionTemplateTypeLabel;
        private DevExpress.XtraEditors.ImageComboBoxEdit sessionTemplateTypeComboBox;
    }
}