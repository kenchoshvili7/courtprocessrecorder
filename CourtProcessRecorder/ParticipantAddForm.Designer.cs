﻿namespace CourtProcessRecorder
{
    partial class ParticipantAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParticipantAddForm));
            this.positionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.positionNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.participantNameLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.participantsGridControl = new DevExpress.XtraGrid.GridControl();
            this.participantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.participantsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colParticipantDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.positionsGridControl = new DevExpress.XtraGrid.GridControl();
            this.positionsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.addParticipantSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.deleteParticipantSimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.renameParticipantSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.leftPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.positionsPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.buttonsPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.importParticipantsButton = new DevExpress.XtraEditors.SimpleButton();
            this.newPositionSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.footerPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.importParticipantsFolderBrowser = new Ookii.Dialogs.VistaFolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.positionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftPanelControl)).BeginInit();
            this.leftPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.positionsPanelControl)).BeginInit();
            this.positionsPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonsPanelControl)).BeginInit();
            this.buttonsPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.footerPanelControl)).BeginInit();
            this.footerPanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // positionBindingSource
            // 
            this.positionBindingSource.DataSource = typeof(JsonDatabaseModels.Position);
            // 
            // positionNameTextEdit
            // 
            this.positionNameTextEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.positionNameTextEdit.Location = new System.Drawing.Point(1, 22);
            this.positionNameTextEdit.Name = "positionNameTextEdit";
            this.positionNameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.positionNameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.positionNameTextEdit.Size = new System.Drawing.Size(343, 22);
            this.positionNameTextEdit.TabIndex = 2;
            // 
            // participantNameLabelControl
            // 
            this.participantNameLabelControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.participantNameLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.participantNameLabelControl.Location = new System.Drawing.Point(5, 1);
            this.participantNameLabelControl.Name = "participantNameLabelControl";
            this.participantNameLabelControl.Size = new System.Drawing.Size(82, 17);
            this.participantNameLabelControl.TabIndex = 1;
            this.participantNameLabelControl.Text = "სახელი, გვარი";
            // 
            // okSimpleButton
            // 
            this.okSimpleButton.Location = new System.Drawing.Point(5, 5);
            this.okSimpleButton.Name = "okSimpleButton";
            this.okSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.okSimpleButton.TabIndex = 3;
            this.okSimpleButton.Text = "OK";
            this.okSimpleButton.Click += new System.EventHandler(this.okSimpleButton_Click);
            // 
            // cancelSimpleButton
            // 
            this.cancelSimpleButton.Location = new System.Drawing.Point(677, 5);
            this.cancelSimpleButton.Name = "cancelSimpleButton";
            this.cancelSimpleButton.Size = new System.Drawing.Size(75, 23);
            this.cancelSimpleButton.TabIndex = 3;
            this.cancelSimpleButton.Text = "გაუქმება";
            this.cancelSimpleButton.Click += new System.EventHandler(this.cancelSimpleButton_Click);
            // 
            // participantsGridControl
            // 
            this.participantsGridControl.DataSource = this.participantBindingSource;
            this.participantsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.participantsGridControl.Location = new System.Drawing.Point(0, 0);
            this.participantsGridControl.MainView = this.participantsGridView;
            this.participantsGridControl.Name = "participantsGridControl";
            this.participantsGridControl.Size = new System.Drawing.Size(303, 445);
            this.participantsGridControl.TabIndex = 4;
            this.participantsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.participantsGridView});
            // 
            // participantBindingSource
            // 
            this.participantBindingSource.DataSource = typeof(JsonDatabaseModels.Participant);
            // 
            // participantsGridView
            // 
            this.participantsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colParticipantDescription});
            this.participantsGridView.GridControl = this.participantsGridControl;
            this.participantsGridView.Name = "participantsGridView";
            this.participantsGridView.OptionsBehavior.Editable = false;
            this.participantsGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.participantsGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colParticipantDescription
            // 
            this.colParticipantDescription.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colParticipantDescription.AppearanceCell.Options.UseFont = true;
            this.colParticipantDescription.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colParticipantDescription.AppearanceHeader.Options.UseFont = true;
            this.colParticipantDescription.Caption = "მონაწილეები";
            this.colParticipantDescription.FieldName = "ParticipantDescription";
            this.colParticipantDescription.Name = "colParticipantDescription";
            this.colParticipantDescription.OptionsColumn.ReadOnly = true;
            this.colParticipantDescription.Visible = true;
            this.colParticipantDescription.VisibleIndex = 0;
            // 
            // positionsGridControl
            // 
            this.positionsGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.positionsGridControl.DataSource = this.positionBindingSource;
            this.positionsGridControl.Location = new System.Drawing.Point(0, 50);
            this.positionsGridControl.MainView = this.positionsGridView;
            this.positionsGridControl.Name = "positionsGridControl";
            this.positionsGridControl.Size = new System.Drawing.Size(349, 397);
            this.positionsGridControl.TabIndex = 4;
            this.positionsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.positionsGridView});
            // 
            // positionsGridView
            // 
            this.positionsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName});
            this.positionsGridView.GridControl = this.positionsGridControl;
            this.positionsGridView.Name = "positionsGridView";
            this.positionsGridView.OptionsBehavior.Editable = false;
            this.positionsGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.positionsGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colName.AppearanceCell.Options.UseFont = true;
            this.colName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colName.AppearanceHeader.Options.UseFont = true;
            this.colName.Caption = "პოზიციები";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // addParticipantSimpleButton
            // 
            this.addParticipantSimpleButton.Image = ((System.Drawing.Image)(resources.GetObject("addParticipantSimpleButton.Image")));
            this.addParticipantSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.addParticipantSimpleButton.Location = new System.Drawing.Point(11, 105);
            this.addParticipantSimpleButton.Name = "addParticipantSimpleButton";
            this.addParticipantSimpleButton.Size = new System.Drawing.Size(80, 65);
            this.addParticipantSimpleButton.TabIndex = 3;
            this.addParticipantSimpleButton.Text = "დამატება";
            this.addParticipantSimpleButton.Click += new System.EventHandler(this.addParticipantSimpleButton_Click);
            // 
            // deleteParticipantSimpleButton2
            // 
            this.deleteParticipantSimpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("deleteParticipantSimpleButton2.Image")));
            this.deleteParticipantSimpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.deleteParticipantSimpleButton2.Location = new System.Drawing.Point(11, 183);
            this.deleteParticipantSimpleButton2.Name = "deleteParticipantSimpleButton2";
            this.deleteParticipantSimpleButton2.Size = new System.Drawing.Size(80, 65);
            this.deleteParticipantSimpleButton2.TabIndex = 3;
            this.deleteParticipantSimpleButton2.Text = "წაშლა";
            this.deleteParticipantSimpleButton2.Click += new System.EventHandler(this.deleteParticipantSimpleButton2_Click);
            // 
            // renameParticipantSimpleButton
            // 
            this.renameParticipantSimpleButton.Image = ((System.Drawing.Image)(resources.GetObject("renameParticipantSimpleButton.Image")));
            this.renameParticipantSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.renameParticipantSimpleButton.Location = new System.Drawing.Point(11, 27);
            this.renameParticipantSimpleButton.Name = "renameParticipantSimpleButton";
            this.renameParticipantSimpleButton.Size = new System.Drawing.Size(80, 65);
            this.renameParticipantSimpleButton.TabIndex = 3;
            this.renameParticipantSimpleButton.Text = "შეცვლა";
            this.renameParticipantSimpleButton.Click += new System.EventHandler(this.renameParticipantSimpleButton_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.participantsGridControl);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.leftPanelControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(757, 445);
            this.splitContainerControl1.SplitterPosition = 449;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // leftPanelControl
            // 
            this.leftPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.leftPanelControl.Controls.Add(this.positionsPanelControl);
            this.leftPanelControl.Controls.Add(this.buttonsPanelControl);
            this.leftPanelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftPanelControl.Location = new System.Drawing.Point(0, 0);
            this.leftPanelControl.Name = "leftPanelControl";
            this.leftPanelControl.Size = new System.Drawing.Size(449, 445);
            this.leftPanelControl.TabIndex = 7;
            // 
            // positionsPanelControl
            // 
            this.positionsPanelControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.positionsPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.positionsPanelControl.Controls.Add(this.positionsGridControl);
            this.positionsPanelControl.Controls.Add(this.positionNameTextEdit);
            this.positionsPanelControl.Controls.Add(this.participantNameLabelControl);
            this.positionsPanelControl.Location = new System.Drawing.Point(0, 0);
            this.positionsPanelControl.Name = "positionsPanelControl";
            this.positionsPanelControl.Size = new System.Drawing.Size(347, 445);
            this.positionsPanelControl.TabIndex = 6;
            // 
            // buttonsPanelControl
            // 
            this.buttonsPanelControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonsPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.buttonsPanelControl.Controls.Add(this.deleteParticipantSimpleButton2);
            this.buttonsPanelControl.Controls.Add(this.renameParticipantSimpleButton);
            this.buttonsPanelControl.Controls.Add(this.importParticipantsButton);
            this.buttonsPanelControl.Controls.Add(this.newPositionSimpleButton);
            this.buttonsPanelControl.Controls.Add(this.addParticipantSimpleButton);
            this.buttonsPanelControl.Location = new System.Drawing.Point(350, -2);
            this.buttonsPanelControl.Name = "buttonsPanelControl";
            this.buttonsPanelControl.Size = new System.Drawing.Size(101, 449);
            this.buttonsPanelControl.TabIndex = 5;
            // 
            // importParticipantsButton
            // 
            this.importParticipantsButton.Image = ((System.Drawing.Image)(resources.GetObject("importParticipantsButton.Image")));
            this.importParticipantsButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.importParticipantsButton.Location = new System.Drawing.Point(11, 261);
            this.importParticipantsButton.Name = "importParticipantsButton";
            this.importParticipantsButton.Size = new System.Drawing.Size(80, 65);
            this.importParticipantsButton.TabIndex = 3;
            this.importParticipantsButton.Text = "იმპორტი";
            this.importParticipantsButton.ToolTip = "მონაწილეების იმპორტი სხვა სხდომიდან";
            this.importParticipantsButton.Click += new System.EventHandler(this.importParticipantsButton_Click);
            // 
            // newPositionSimpleButton
            // 
            this.newPositionSimpleButton.Image = ((System.Drawing.Image)(resources.GetObject("newPositionSimpleButton.Image")));
            this.newPositionSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.newPositionSimpleButton.Location = new System.Drawing.Point(11, 339);
            this.newPositionSimpleButton.Name = "newPositionSimpleButton";
            this.newPositionSimpleButton.Size = new System.Drawing.Size(80, 65);
            this.newPositionSimpleButton.TabIndex = 3;
            this.newPositionSimpleButton.Text = "ახალი პოზიცია";
            this.newPositionSimpleButton.Click += new System.EventHandler(this.newPositionSimpleButton_Click);
            // 
            // footerPanelControl
            // 
            this.footerPanelControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.footerPanelControl.Controls.Add(this.cancelSimpleButton);
            this.footerPanelControl.Controls.Add(this.okSimpleButton);
            this.footerPanelControl.Location = new System.Drawing.Point(0, 446);
            this.footerPanelControl.Name = "footerPanelControl";
            this.footerPanelControl.Size = new System.Drawing.Size(757, 33);
            this.footerPanelControl.TabIndex = 6;
            // 
            // ParticipantAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 478);
            this.Controls.Add(this.footerPanelControl);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ParticipantAddForm";
            this.Text = "მონაწილეების განსაზღვრა";
            ((System.ComponentModel.ISupportInitialize)(this.positionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leftPanelControl)).EndInit();
            this.leftPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.positionsPanelControl)).EndInit();
            this.positionsPanelControl.ResumeLayout(false);
            this.positionsPanelControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonsPanelControl)).EndInit();
            this.buttonsPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.footerPanelControl)).EndInit();
            this.footerPanelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource positionBindingSource;
        private DevExpress.XtraEditors.TextEdit positionNameTextEdit;
        private DevExpress.XtraEditors.LabelControl participantNameLabelControl;
        private DevExpress.XtraEditors.SimpleButton okSimpleButton;
        private DevExpress.XtraEditors.SimpleButton cancelSimpleButton;
        private DevExpress.XtraGrid.GridControl participantsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView participantsGridView;
        private DevExpress.XtraGrid.GridControl positionsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView positionsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private System.Windows.Forms.BindingSource participantBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colParticipantDescription;
        private DevExpress.XtraEditors.SimpleButton addParticipantSimpleButton;
        private DevExpress.XtraEditors.SimpleButton deleteParticipantSimpleButton2;
        private DevExpress.XtraEditors.SimpleButton renameParticipantSimpleButton;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.PanelControl footerPanelControl;
        private DevExpress.XtraEditors.PanelControl positionsPanelControl;
        private DevExpress.XtraEditors.PanelControl buttonsPanelControl;
        private DevExpress.XtraEditors.PanelControl leftPanelControl;
        private DevExpress.XtraEditors.SimpleButton newPositionSimpleButton;
        private DevExpress.XtraEditors.SimpleButton importParticipantsButton;
        private Ookii.Dialogs.VistaFolderBrowserDialog importParticipantsFolderBrowser;
    }
}