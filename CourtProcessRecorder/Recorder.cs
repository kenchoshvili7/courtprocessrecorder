﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AudioConfiguration;
using CourtRecorderCommon;
using DeviceController;
using HelperClasses;
using JsonDatabaseModels;
using RecordProvider;
using RecordProvider.HelperClasses;

namespace CourtProcessRecorder
{
    public partial class Recorder : DevExpress.XtraEditors.XtraForm
    {
        private List<Session> Sessions;
        //private Timer _timer = new Timer(1000);
        private System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();
        private readonly DatabaseHelper _databaseHelper;
        private SessionDbHelper _sessionDbHelper;
        
        AudioDevice playbackAudioDevice;
        AudioDevice recordingAudioDevice;

        public Recorder()
        {
            InitializeComponent();

            MixerPower.Enabled = false;

            InitializeMixer();

            //AudioIO.Apolo16Mixer.ButtonsStateChanged += Apolo16MixerOnButtonsStateChanged;
            //AudioIO.UsbInterface.ButtonPressed += UsbInterfaceOnButtonPressed;
            //AudioIO.Apolo16Mixer.ButtonsStateChanged += new DeviceController.ButtonPressedEvent(this.UsbInterfaceButtonPressedNonSync2);

            //AudioIO.Apolo16Mixer.Reseted += Apolo16MixerOnReseted;

            Sessions = new List<Session>();

            _recordHelper = new RecordHelper();

            _databaseHelper = new DatabaseHelper(ConfigurationHelper.GetConfiguration());
            dataGridView1.DataSource = typeof(List<Session>);
            //dataGridView1.DataSource = _databaseHelper.GetSessionsAndUpdate();

            comboBox1.DataSource = AudioDevice.GetPlayBackDevices();
            comboBox1.DisplayMember = "DeviceName";
            comboBox1.ValueMember = "DeviceId";

            comboBox2.DataSource = AudioDevice.GetRecordingDevices();
            comboBox2.DisplayMember = "DeviceName";
            comboBox2.ValueMember = "DeviceId";

            _timer.Interval = 1000;
            _timer.Tick += TimerOnTick;
        }

        void Apolo16MixerOnReseted(object sender, EventArgs eventArgs)
        {

        }

        private bool _mixerPowered = true;
        private bool _isRecording = false;

        private void bt_Mic1_Click(object sender, EventArgs e)
        {
            var isTurnedOff = Apolo16Provider.ToggleMicrophone(AudioSourceChannels.Channel1);
            bt_Mic1.BackColor = isTurnedOff ? Color.Red : Color.Green;
        }

        private void bt_Mic2_Click(object sender, EventArgs e)
        {
            var isTurnedOff = Apolo16Provider.ToggleMicrophone(AudioSourceChannels.Channel2);
            bt_Mic2.BackColor = isTurnedOff ? Color.Red : Color.Green;
        }

        private void bt_Mic3_Click(object sender, EventArgs e)
        {
            var isTurnedOff = Apolo16Provider.ToggleMicrophone(AudioSourceChannels.Channel3);
            bt_Mic3.BackColor = isTurnedOff ? Color.Red : Color.Green;
        }

        private void bt_Mic4_Click(object sender, EventArgs e)
        {
            var isTurnedOff = Apolo16Provider.ToggleMicrophone(AudioSourceChannels.Channel4);
            bt_Mic4.BackColor = isTurnedOff ? Color.Red : Color.Green;
        }

        private void bt_Mic5_Click(object sender, EventArgs e)
        {
            var isTurnedOff = Apolo16Provider.ToggleMicrophone(AudioSourceChannels.Channel5);
            bt_Mic5.BackColor = isTurnedOff ? Color.Red : Color.Green;
        }

        private void bt_Mic6_Click(object sender, EventArgs e)
        {
            var isTurnedOff = Apolo16Provider.ToggleMicrophone(AudioSourceChannels.Channel6);
            bt_Mic6.BackColor = isTurnedOff ? Color.Red : Color.Green;
        }

        private void bt_Record_Click(object sender, EventArgs e)
        {
            Record();
        }

        private void bt_RecorderOnOff_Click(object sender, EventArgs e)
        {
            OnOff();
        }

        private void InitializeMixer()
        {
            AudioIO.Enabled = true;
            AudioIO.Initialize();
            AudioIO.Apolo16Mixer.Mute = MixerChannels.None;
            AudioIO.SetPlaybackVolume(100);
            IncreaseVolumeTo100();
        }

        private void Record()
        {
            try
            {
                if (_isRecording)
                {
                    _recordHelper.StopRecord();

                    _isRecording = false;
                    bt_Record.BackColor = Color.Green;
                }
                else
                {
                    _recordHelper.StartRecord();

                    _isRecording = true;
                    bt_Record.BackColor = Color.Red;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private bool OnOff()
        {
            if (_mixerPowered)
            {
                Apolo16Provider.PowerOn();
                _mixerPowered = false;
                MixerPower.Checked = false;
                bt_RecorderOnOff.BackColor = Color.Green;

                return true;
            }

            Apolo16Provider.PowerOff();
            _mixerPowered = true;
            MixerPower.Checked = true;
            bt_RecorderOnOff.BackColor = Color.Red;

            return false;
        }

        private void IncreaseVolumeTo100()
        {
            new List<AudioSourceChannels>
            {
                AudioSourceChannels.Channel1,
                AudioSourceChannels.Channel2,
                AudioSourceChannels.Channel3,
                AudioSourceChannels.Channel4,
                AudioSourceChannels.Channel5,
                AudioSourceChannels.Channel6
            }.ForEach(c => AudioIO.Recorder.SetChannelVolume(c, 100));
        }

        public delegate void ButtonPressedEvent(ChannelButtons channelButton, bool pressed);

        RecordHelper _recordHelper;
        private PlayHelper _playHelper;

        private void button1_Click(object sender, EventArgs e)
        {
            var nn = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            var newSession = new Session { Name = SessionNameTextbox.Text };
            Sessions.Add(newSession);

            _recordHelper.CreateSession(newSession);
            dataGridView1.DataSource = typeof(List<Session>);
            //dataGridView1.DataSource = _databaseHelper.GetSessionsAndUpdate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _playHelper.StopPlayback();
        }

        private void Play_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows[0].Cells.Count < 1) return;

            var sessionPath = dataGridView1.SelectedRows[0].Cells["Path"].Value;
            //var duration = dataGridView1.SelectedRows[0].Cells["AudioFilesDuration"].Value;
            _sessionDbHelper = new SessionDbHelper(sessionPath.ToString());
            var session = _sessionDbHelper.GetSession();
            _playHelper = new PlayHelper(session, playbackAudioDevice);

            var durationSum = session.AudioFilesContainers.OrderBy(a => a.StartTime).Where(x => x.Duration != null).SumTimeSpan(d => d.Duration.Value);
            trackBar1.Maximum = durationSum.Seconds;
            _playHelper.PlayAudio(playbackAudioDevice);

            //waveViewer1.SamplesPerPixel = 500;
            //waveViewer1.LineColor = Pens.Red;
            ////waveViewer1.WaveStream = _playHelper.CombinedWaveStreams[0];

            //waveViewer2.SamplesPerPixel = 500;
            //waveViewer2.WaveStream = _playHelper.CombinedWaveStreams[2];


            //new Mp3FileReader(@"D:\AudioTest\1_2016_10_12_16_31_43\AudioFiles\2016_10_12_16_31_46\Channel1.mp3");

            _timer.Start();

        }

        private void Pause_Click(object sender, EventArgs e)
        {
            _playHelper.PauseResumeAudio();
        }

        private void PathButton_Click(object sender, EventArgs e)
        {
            ConfigurationHelper.CreateConfiguration(new RecorderConfiguration
            {
                Name = "TempConfig",
                ArchiveDirectory = PathTextbox.Text,
                Format = new WaveFormatEx
                {
                    wFormatTag = 1,
                    nChannels = 1,
                    nSamplesPerSec = 16000,
                    nAvgBytesPerSec = 32000,
                    nBlockAlign = 2,
                    wBitsPerSample = 16,
                    cbSize = 0,
                    ExtraData = new byte[0]
                },
                DeviceNo = 2147483633,
                Channels = new List<Channel>
                {
                    new Channel{ChannelNo = AudioSourceChannels.Channel1,Name = "მოსამართლე",ChannelType = ChannelType.Judge},
                    new Channel{ChannelNo = AudioSourceChannels.Channel2,Name = "მხარე1",ChannelType = ChannelType.Side},
                    new Channel{ChannelNo = AudioSourceChannels.Channel3,Name = "მხარე2",ChannelType = ChannelType.Side},
                    new Channel{ChannelNo = AudioSourceChannels.Channel4,Name = "აპელანტი",ChannelType = ChannelType.Side},
                    new Channel{ChannelNo = AudioSourceChannels.Channel5,Name = "ბრალდებული",ChannelType = ChannelType.Accused},
                    new Channel{ChannelNo = AudioSourceChannels.Channel6,Name = "asd",ChannelType = ChannelType.Judge}
                },
                ProgramConfiguration = ProgramConfiguration.MultiChannel
            });
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            _timer.Stop();
            var currentTimeSpan = new TimeSpan(0, 0, trackBar1.Value);
            _playHelper.SeekPlayback(currentTimeSpan);
            _timer.Start();
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            var pos = _playHelper.GetPlaybackCurrentPosition();
            CurrentPosition.Text = pos.ToString();
            if (pos.Seconds > trackBar1.Maximum) return;
            trackBar1.Value = pos.Seconds;
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            playbackAudioDevice = comboBox1.SelectedItem as AudioDevice;
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            recordingAudioDevice = comboBox2.SelectedItem as AudioDevice;
        }
    }
}
