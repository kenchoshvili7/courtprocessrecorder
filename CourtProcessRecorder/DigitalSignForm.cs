﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CourtProcessRecorder
{
    public partial class DigitalSignForm : XtraForm
    {
        private string _responsiblePerson;
        private string _organization;

        public DigitalSignForm()
        {
            InitializeComponent();
            }

        public string ResponsiblePerson { get { return _responsiblePerson; } }

        public string Organization { get { return _organization; } }

        private void okSimpleButton_Click(object sender, EventArgs e)
        {
            _responsiblePerson = signerTextEdit.Text;
            _organization = organizationTextEdit.Text;

            if (string.IsNullOrWhiteSpace(_responsiblePerson) || string.IsNullOrWhiteSpace(_organization))
            {
                XtraMessageBox.Show("ხელმომწერი ან ორგანიზაცია არ შეიძლება იყოს ცარიელი!", "გაფრთხილება", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void cancelSimpleButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}