using System;
using System.ComponentModel;
using System.Windows.Forms;
using CourtProcessRecorder.HelperClasses;
using CourtProcessRecorder.TemplateEdit;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using HelperClasses;
using JsonDatabaseModels;
using RecordProvider;
using ProgramMode = HelperClasses.ProgramMode;

namespace CourtProcessRecorder
{
    public partial class Default : RibbonForm
    {
        private readonly SessionsForm _sessionForm;
        private ConfigurationForm _configurationForm;
        private TemplateEditForm _templateEditForm;
        readonly RecorderConfiguration _config;

        public Default()
        {
            var isValid = CheckForConfiguration();

            if (!isValid)
            {
                Environment.Exit(0);
                return;
            }

            _config = ConfigurationHelper.GetConfiguration();

            Global.GlobalMode = _config.GlobalMode;

            if (Global.GlobalMode == GlobalMode.Recorder)
            {
                if (_config.DeviceNo == 0x7ffffff1) Apolo16Provider.InitializeMixer();
                else AsioProvider.InitializeMixer();
            }
            else Apolo16Provider.InitializeBass();

            Global.ProgramMode = ProgramMode.CourtProcessRecorder;

            InitializeComponent();

            Closing += OnClosing;

            IsMdiContainer = true;
            ribbon.MdiMergeStyle = RibbonMdiMergeStyle.Always;

            SetGlobalMode();

            _sessionForm = new SessionsForm
            {
                Text = @"სხდომები",
                MdiParent = this
            };

            ribbon.Merge += RibbonOnMerge;
            ribbon.MergeRibbon(_sessionForm.Ribbon);

            _sessionForm.Show();

            ActionLogger.LogAction(ConstantStrings.ProgramStart);
        }

        private void SetGlobalMode()
        {
            switch (Global.GlobalMode)
            {
                case GlobalMode.Viewer:
                    templateEditBarButtonItem.Visibility = BarItemVisibility.Never;
                    break;
                case GlobalMode.Editor:

                    break;
                case GlobalMode.Recorder:
                    break;
            }
        }

        private void RibbonOnMerge(object sender, RibbonMergeEventArgs e)
        {
            ribbon.Pages[0].MergeOrder = 100;
            ribbon.MergedRibbon.Pages[0].MergeOrder = 10;

            ribbon.SelectedPage = e.MergedChild.Pages[0];
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            var result = XtraMessageBox.Show("ნამდვილად გსურთ დახურვა?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                _sessionForm.DisposeSessionForm();

                if (_config.DeviceNo == 0x7ffffff1) Apolo16Provider.Dispose();
                else AsioProvider.Dispose();

                ActionLogger.LogAction(ConstantStrings.ProgramStop);
            }
            else e.Cancel = true;
        }

        //private void CreateDocumentManager()
        //{
        //    var mdiManager = new DocumentManager { MdiParent = this };
        //}

        private void configuration_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_configurationForm != null && _configurationForm.Visible) return;

            _configurationForm = new ConfigurationForm
            {
                Text = @"კონფიგურაცია",
                WindowState = FormWindowState.Maximized,
                MdiParent = this
            };

            _configurationForm.Show();
        }

        private void templateEditBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_templateEditForm != null && _templateEditForm.Visible) return;

            _templateEditForm = new TemplateEditForm
            {
                Text = @"შაბლონების რედაქტირება",
                WindowState = FormWindowState.Maximized,
                MdiParent = this
            };

            _templateEditForm.Show();
        }

        private void skinRibbonGallery_Gallery_ItemClick(object sender, GalleryItemClickEventArgs e)
        {
            var selectedSkin = e.Item.Caption;
            ConfigurationHelper.SetSkinName(selectedSkin);
        }

        private bool CheckForConfiguration()
        {
            var recorderConfiguration = ConfigurationHelper.GetConfigurationWithAllChannels();

            if (string.IsNullOrWhiteSpace(recorderConfiguration?.ArchiveDirectory))
            {
                XtraMessageBox.Show("ვერ მოიძებნა კონფიგურაცია, გთხოვთ დააკონფიგურიროთ პროგრამა", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!PermissionHelper.HasPermissionOnDir(recorderConfiguration.ArchiveDirectory, recorderConfiguration.GlobalMode))
            {
                XtraMessageBox.Show("არქივის მისამართი მიუწვდომელია!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else return true;

            var configurationForm = new ConfigurationForm
            {
                Text = @"კონფიგურაცია"
            };

            if (configurationForm.ShowDialog() == DialogResult.OK)
            {
                Application.Restart();
                return false;
            }

            Environment.Exit(0);
            return false;
        }
    }
}