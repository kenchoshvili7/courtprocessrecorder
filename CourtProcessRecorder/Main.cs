﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CourtProcessRecorder.CustomRibbonControls;
using CourtProcessRecorder.HelperClasses;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList;
using HelperClasses;
using JsonDatabaseModels;
using RecordProvider;
using RecordProvider.HelperClasses;
using CourtProcessRecorder.ProsecutorServiceReference;
using System.Net;

namespace CourtProcessRecorder
{
    public partial class Main : RibbonForm
    {
        private int isProsecutorsSelected = 0;
        private readonly DatabaseHelper _databaseHelper;
        //private SessionDbHelper _sessionDbHelper;
        private SessionTemplate _currentTemplate;
        private bool _isRecording;
        private readonly RecorderConfiguration _configuration;
        private string _sessionPath;

        public Main()
        {
            InitializeComponent();

            IsMdiContainer = false;

            _databaseHelper = DatabaseHelper.DatabaseHelperInstance();

            _configuration = ConfigurationHelper.GetConfiguration();

            playerUserControl.PlaybackStopped += PlayerUserControlOnPlaybackStopped;

            participantsGridView.RowClick += ParticipantsGridViewOnClick;
            actionsTreeList.MouseClick += ActionsTreeListOnClick;

            seekTextEdit.KeyDown += SeekTextEditOnKeyDown;
        }

        private Mode _mode { get; set; }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            WindowState = FormWindowState.Maximized;

            LoadTemplates();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            WindowState = FormWindowState.Maximized;

            var audioDevices = AudioDevice.GetPlayBackDevices();

            playbackDeviceLookUpEdit.Properties.DataSource = audioDevices;

            playbackDeviceLookUpEdit.EditValue = audioDevices.Count == 1 ? audioDevices.FirstOrDefault()?.DeviceId : 0;

            playbackDeviceLookUpEdit.EditValueChanged += PlaybackDeviceLookUpEditOnEditValueChanged;
            playbackDeviceLookUpEdit.Properties.MouseDown += PlaybackDeviceLookUpEditOnMouseDown;

            playbackSpeedLookUpEdit.Properties.DataSource = PlaybackSpeed.GetPlaybackSpeeds;
            playbackSpeedLookUpEdit.EditValue = 16000;
            playbackSpeedLookUpEdit.EditValueChanged += PlaybackSpeedLookUpEditOnEditValueChanged;

            participantsGridControl.DataSource = _currentSession.Participants;

            templatesRepositoryItemLookUpEdit.EditValueChanged += TemplatesRepositoryItemLookUpEditOnEditValueChanged;

            sessionTemplateTypeImageComboBox.Items.AddEnum(typeof(SessionTemplateType));
            sessionTemplateTypeBarEditItem.EditValueChanged += SessionTemplateTypeBarEditItemOnEditValueChanged;

            SetGlobalMode();
        }

        private void SetGlobalMode()
        {
            switch (Global.GlobalMode)
            {
                case GlobalMode.Viewer:
                    participantsGridView.RowClick -= ParticipantsGridViewOnClick;
                    actionsTreeList.MouseClick -= ActionsTreeListOnClick;

                    var grid = sessionEventUserControl.Controls.Find("eventsGridControl", true).FirstOrDefault() as GridControl;
                    var eventsGrid = grid.MainView as GridView;
                    eventsGrid.OptionsBehavior.Editable = false;

                    templatesLookupEdit.Enabled = false;
                    sessionTemplateTypeBarEditItem.Enabled = false;

                    participantRibbonPageGroup.Visible = false;
                    sessionEventRibbonPageGroup.Visible = false;
                    writeToDiscBarButtonItem.Visibility = BarItemVisibility.Never;
                    recordbtn.Visible = false;

                    break;
                case GlobalMode.Editor:
                    recordbtn.Visible = false;
                    break;
                case GlobalMode.Recorder:
                    break;
            }
        }

        private Session _currentSession;

        public void CreateNewSession(Session session)
        {
            _currentSession = session;
            _sessionPath = session.Path;

            SessionDbWrapper.Initialize(_sessionPath);

            recorderUserControl.InitializeRecorderControl(_currentSession);

            sessionEventUserControl.InitializeSession(_currentSession);

            ToggleMode(false);

            recordbtn.ImageIndex = 0;
            recordbtn.Text = @"ჩაწერა";
        }

        public bool OpenSession(string path)
        {
            if (Global.GlobalMode == GlobalMode.Viewer)
                SessionDbWrapper.Initialize(path, true);
            else SessionDbWrapper.Initialize(path);

            _sessionPath = path;

            _currentSession = SessionDbWrapper.SessionDbHelper.GetSession();
            if (_currentSession == null) return false;

            //if (_currentSession.Path != path)
            //    _currentSession = SessionDbWrapper.SessionDbHelper.RegenerateSessionPath(_currentSession, path);

            if (Global.GlobalMode != GlobalMode.Viewer) SessionHelper.UpdateAudioFilesMissingDurations(_currentSession, _sessionPath);

            if (Global.GlobalMode == GlobalMode.Recorder) recorderUserControl.OpenSession(_currentSession);

            var mode = _currentSession.AudioFilesContainers != null && _currentSession.AudioFilesContainers.Any();

            ToggleMode(mode);

            recordbtn.ImageIndex = 0;
            recordbtn.Text = @"ჩაწერა";

            sessionEventUserControl.InitializeSession(_currentSession);

            if (_currentSession.IsSigned)
                OpenSignedMode();

            return true;
        }

        private void OpenSignedMode()
        {
            participantsGridView.RowClick -= ParticipantsGridViewOnClick;
            actionsTreeList.MouseClick -= ActionsTreeListOnClick;

            var grid = sessionEventUserControl.Controls.Find("eventsGridControl", true).FirstOrDefault() as GridControl;
            var eventsGrid = grid.MainView as GridView;
            eventsGrid.OptionsBehavior.Editable = false;

            templatesLookupEdit.Enabled = false;
            sessionTemplateTypeBarEditItem.Enabled = false;
            participantRibbonPageGroup.Enabled = false;
            sessionEventRibbonPageGroup.Enabled = false;
            recordbtn.Enabled = false;
        }

        private void ToggleMode(bool player)
        {
            var customRibbon = Parent.Parent.Controls.OfType<CustomRibbonControl>().FirstOrDefault();

            if (player)
            {
                _mode = Mode.Player;

                recorderPanelControl.Hide();
                recorderUserControl.Hide();

                playPausebtn.Enabled = true;
                stopbtn.Enabled = true;
                recordbtn.ImageIndex = 0;
                recordbtn.Text = @"ჩაწერა";
                seekTextEdit.Enabled = true;
                seekSimpleButton.Enabled = true;
                playbackDeviceLookUpEdit.Enabled = true;
                playbackSpeedLookUpEdit.Enabled = true;
                participantAudioFilesContainersXtraTabControl.TabPages[1].PageVisible = true;

                seekTextEdit.EditValue = _currentSession.AudioFilesContainers.OrderBy(o => o.StartTime).FirstOrDefault()?.StartTime;

                playerUserControl.InitializePlayerControl(_currentSession);

                actionsRibbonPageGroup.Enabled = true;

                playerUserControl.Show();

                if (customRibbon != null) customRibbon.Pages[0].Groups[1].Visible = true;
            }
            else
            {
                _mode = Mode.Recorder;

                playPausebtn.Enabled = false;
                stopbtn.Enabled = false;
                recordbtn.ImageIndex = 1;
                recordbtn.Text = @"გაჩერება";
                seekTextEdit.Enabled = false;
                seekSimpleButton.Enabled = false;
                playbackDeviceLookUpEdit.Enabled = false;
                playbackSpeedLookUpEdit.Enabled = false;
                participantAudioFilesContainersXtraTabControl.TabPages[1].PageVisible = false;

                playerUserControl.Hide();
                recorderPanelControl.Show();
                recorderUserControl.Show();
                actionsRibbonPageGroup.Enabled = false;
                if (customRibbon != null) customRibbon.Pages[0].Groups[1].Visible = false;
            }
        }

        private void TogglePlayerButton()
        {
            if (playerUserControl.PlayerStatus == PlayerStatus.Playing)
            {
                playPausebtn.Text = @"პაუზა";
                playPausebtn.ImageIndex = 1;

                recordbtn.Enabled = false;
                stopbtn.Enabled = true;
            }
            else
            {
                playPausebtn.Text = @"მოსმენა";
                playPausebtn.ImageIndex = 0;

                recordbtn.Enabled = !_currentSession.IsSigned;
                stopbtn.Enabled = false;
            }
        }

        private void playPausebtn_Click(object sender, EventArgs e)
        {
            playerUserControl.PlayPause();

            TogglePlayerButton();
        }

        private void stopbtn_Click(object sender, EventArgs e)
        {
            playerUserControl.Stop();

            TogglePlayerButton();
        }

        private void recordbtn_Click(object sender, EventArgs e)
        {


            if (_isRecording)
            {
                InsertProsecutors();
                recorderUserControl.StopRecord();
                _isRecording = false;
            }
            else
            {
                if (!AudioDevice.IsRecordingDeviceConnected(_configuration.DeviceNo))
                {
                    var message = "ჩამწერი მოწყობილობა მიუწვდომელია!";
                    XtraMessageBox.Show(message, "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    ActionLogger.LogAction(ConstantStrings.Message, $"{message} SessionPath: {_sessionPath}");

                    return;
                }


                var isConnection = InternetConnectionHelper.CheckForInternetConnection();
                var sessionTemplateType = (SessionTemplateType)sessionTemplateTypeBarEditItem.EditValue;
                if (isConnection && sessionTemplateType == SessionTemplateType.Crime)
                {
                    var dialogResult =  showProsecutorForm();
                    if (dialogResult != DialogResult.Yes)
                        return;
                }


                playerUserControl.DisposePlayer();

                recorderUserControl.StartRecord();
                _isRecording = true;
            }

            ReloadSession();

            var lastAudioFilesContainers = _currentSession.AudioFilesContainers.OrderByDescending(o => o.StartTime).FirstOrDefault();

            string description;
            var time = DateTime.Now;

            if (_isRecording)
            {
                description = "<<<ჩაწერის დაწყება>>>";

                if (lastAudioFilesContainers?.StartTime != null)
                    time = lastAudioFilesContainers.StartTime.Value;
            }
            else
            {
                description = "<<<ჩაწერის დასრულება>>>";

                if (lastAudioFilesContainers?.EndTime != null)
                    time = lastAudioFilesContainers.EndTime.Value;
            }

            var sessionEvent = new SessionEvent
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                Time = time,
                SessionAction = new SessionAction { Description = description }
            };

            sessionEventUserControl.AddEvent(_currentSession, sessionEvent);

            ToggleMode(!_isRecording);

            playPausebtn.Text = @"მოსმენა";
            playPausebtn.ImageIndex = 0;

            //ToggleMode(!record);
        }

        private void seekSimpleButton_Click(object sender, EventArgs e)
        {
            Seek();
        }


        private void InsertProsecutors()
        {
            if (InternetConnectionHelper.CheckForInternetConnection())
            {
                try
                {
                    ProsecutorServiceReference.ServiceClient client = new ProsecutorServiceReference.ServiceClient();
                    DateTime endDate = client.GetServerDate();
                    if (ProsecutorInfo.Prosecutors != null && ProsecutorInfo.Prosecutors.Any())
                    {
                        foreach (var i in ProsecutorInfo.Prosecutors)
                        {
                            client.Insert(i.CourtName, i.CaseNo, i.ProsecutorID, i.FullName, i.StartDate, endDate);
                        }
                    }
                }
                catch (Exception e)
                {


                }
                finally
                {
                    ProsecutorInfo.Prosecutors = null;
                }

            }
            else
            {

            }

        }


        private DialogResult showProsecutorForm()
        {
            //ProsecutorServiceReference.ServiceClient client = new ProsecutorServiceReference.ServiceClient();
            //var isProsecurtorAddedOnCaseNo = client.IsProsecurtorAddedOnCaseNo(ProsecutorInfo.CaseNo);
            //if (isProsecutorsSelected == 0 && !isProsecurtorAddedOnCaseNo)
            //{
            //    ProsecutorForm form1 = new ProsecutorForm();
            //    form1.ShowDialog();
            //}          
            //isProsecutorsSelected++;

            ProsecutorForm form1 = new ProsecutorForm();
            return form1.ShowDialog();
           


        }

        private void SeekTextEditOnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                Seek();
        }

        private void Seek()
        {
            var time = DateTime.Parse(seekTextEdit.Text);
            playerUserControl.SeekFromOutside(time, false);
        }

        private void PlaybackDeviceLookUpEditOnEditValueChanged(object sender, EventArgs e)
        {
            var audioDevice = playbackDeviceLookUpEdit.GetSelectedDataRow() as AudioDevice;
            playerUserControl.PlaybackDeviceLookUpEditOnEditValueChanged(audioDevice);

            if (audioDevice.DeviceId == 0x7ffffff1) playbackSpeedLookUpEdit.Enabled = false;
            else playbackSpeedLookUpEdit.Enabled = true;

            playbackSpeedLookUpEdit.EditValue = 16000;

            TogglePlayerButton();
        }

        private void PlaybackDeviceLookUpEditOnMouseDown(object sender, EventArgs e)
        {
            playbackDeviceLookUpEdit.Properties.DataSource = AudioDevice.GetPlayBackDevices();
        }

        private void PlaybackSpeedLookUpEditOnEditValueChanged(object sender, EventArgs eventArgs)
        {
            var playbackSpeed = playbackSpeedLookUpEdit.GetSelectedDataRow() as PlaybackSpeed;
            playerUserControl.ChangePlaybackSpeed(playbackSpeed);
        }

        private void ActionsTreeListOnClick(object sender, EventArgs e)
        {
            var tree = sender as TreeList;
            if (tree == null) return;
            var focusedNode = tree.FocusedNode;

            if (focusedNode == null) return;

            var column = tree.Columns.FirstOrDefault(c => c.FieldName == "Description");
            var value = focusedNode.GetValue(column);

            var sessionEvent = new SessionEvent
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                Time = GetCurrentTime(),
                SessionAction = new SessionAction { Description = value?.ToString() }
            };

            if ((ModifierKeys & Keys.Control) != 0)
            {
                sessionEventUserControl.UpdateEvent(_currentSession, sessionEvent);
                return;
            }

            sessionEventUserControl.AddEvent(_currentSession, sessionEvent);
        }

        private void ParticipantsGridViewOnClick(object sender, EventArgs e)
        {
            var gridView = sender as GridView;
            var participant = gridView.GetFocusedRow() as Participant;

            var sessionEvent = new SessionEvent
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                Time = GetCurrentTime(),
                Participant = participant
            };

            if ((ModifierKeys & Keys.Control) != 0)
            {
                sessionEventUserControl.UpdateEvent(_currentSession, sessionEvent);
                return;
            }

            sessionEventUserControl.AddEvent(_currentSession, sessionEvent);
        }

        private void addParticipantBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReloadSession();

            var participantAddForm = new ParticipantAddForm();
            participantAddForm.StartPosition = FormStartPosition.CenterParent;

            if (_currentTemplate == null) return;

            participantAddForm.InitializePositions(_currentTemplate, _currentSession.Participants);

            if (participantAddForm.ShowDialog() != DialogResult.OK) return;

            ReloadSession();

            _currentSession.Participants = participantAddForm.Participants;

            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            ReloadParticipantsDataSource();
        }

        private void deleteParticipantBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            var participant = participantsGridView.GetFocusedRow() as Participant;
            if (participant == null) return;

            var result = XtraMessageBox.Show("ნამდვილად გსურთ წაშლა?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result != DialogResult.Yes) return;

            ReloadSession();

            _currentSession.Participants.RemoveAll(p => p.UId == participant.UId);
            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);
            ReloadParticipantsDataSource();
        }

        private void addSessionActionBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            var sessionEvent = new SessionEvent
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                Time = GetCurrentTime()
            };

            sessionEventUserControl.AddEvent(_currentSession, sessionEvent);
        }

        private void deleteSessionActionBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_configuration.DeleteConfirmationIsOn == null || (bool)_configuration.DeleteConfirmationIsOn)
            {
                var result = XtraMessageBox.Show("ნამდვილად გსურთ წაშლა?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result != DialogResult.Yes) return;
            }

            ReloadSession();
            sessionEventUserControl.RemoveEvents(_currentSession);
        }

        private void secondsBeforeBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            sessionEventUserControl.SecondsBefore();
        }

        private void secondsAfterBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            sessionEventUserControl.SecondsAfter();
        }

        private void writeToDiscBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReloadSession();

            if (!ReportHelper.CheckForSessionReportExists(_currentSession)) return;

            SessionDbWrapper.SessionDbHelper.Dispose();

            WriteToDiscHelper.WriteToDiscFinished += WriteToDiscHelperOnWriteToDiscFinished;

            EnableDisableInWriteMode(false);
            WriteToDiscHelper.LaunchDialog(_sessionPath);
        }

        private void WriteToDiscHelperOnWriteToDiscFinished()
        {
            WriteToDiscHelper.WriteToDiscFinished -= WriteToDiscHelperOnWriteToDiscFinished;
            SessionDbWrapper.Initialize(_sessionPath);

            EnableDisableInWriteMode(true);
        }

        private void EnableDisableInWriteMode(bool isDisabled)
        {
            var customRibbon = Parent?.Parent?.Controls.OfType<CustomRibbonControl>().FirstOrDefault();
            if (customRibbon != null) customRibbon.Pages[0].Visible = isDisabled;

            mainPanelControl.Enabled = isDisabled;
            MainGroup.Enabled = isDisabled;
            participantRibbonPageGroup.Enabled = isDisabled && !_currentSession.IsSigned;
            sessionEventRibbonPageGroup.Enabled = isDisabled && !_currentSession.IsSigned;
            actionsRibbonPageGroup.Enabled = isDisabled;
        }

        private void printBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReloadSession();
            ReportHelper.GenerateSessionTemplate(_currentSession);
        }

        private void signBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReloadSession();

            var audioFilesContainer = _currentSession.AudioFilesContainers.FirstOrDefault();
            if (audioFilesContainer == null || !_currentSession.AudioFilesContainers.Any() || !audioFilesContainer.AudioFiles.Any())
            {
                XtraMessageBox.Show("შეუძლებელია ხელმოწერა რადგან სხდომა არ შეიცავს აუდიო ფაილებს!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (_currentSession.Signatures == null || !_currentSession.Signatures.Any())
                _currentSession.Signatures = new List<Signature>();
            else
            {
                OpenDigitalSignDetailForm();

                return;
            }

            if (Global.GlobalMode == GlobalMode.Viewer) return;

            if (!ReportHelper.CheckForSessionReportExists(_currentSession)) return;

            var result = XtraMessageBox.Show("ნამდვილად გსურთ ხელმოწერა " + DateTime.Now.ToString("dd.MM.yyyy") + " თარიღით ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result != DialogResult.Yes) return;

            var digitalSignForm = new DigitalSignForm { StartPosition = FormStartPosition.CenterParent };

            if (digitalSignForm.ShowDialog() != DialogResult.OK) return;

            //var signatureDates = _currentSession.SessionEvents.Select(x => x.Time.Date).Distinct();
            //foreach (var date in signatureDates)
            //{
            //if (_currentSession.Signatures.Any(s => s.SessionDate.Date == date)) continue;

            var signatureSession = SignatureHelper.GenerateSignature(_currentSession, _sessionPath, _currentSession.CreatedOn, DateTime.Now.RoundToMilliSeconds(), digitalSignForm.ResponsiblePerson, digitalSignForm.Organization);

            var signature = signatureSession.Item1;
            _currentSession = signatureSession.Item2;

            signature.UId = Guid.NewGuid();
            signature.CreatedOn = DateTime.Now;

            _currentSession.Signatures.Add(signature);

            _currentSession.IsSigned = true;

            OpenSignedMode();

            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            OpenDigitalSignDetailForm();
        }

        private void OpenDigitalSignDetailForm()
        {
            ReloadSession();

            var digitalSignDetailForm = new DigitalSignDetailForm { StartPosition = FormStartPosition.CenterParent };
            digitalSignDetailForm.InitializeDigitalSignDetailForm(_currentSession);
            digitalSignDetailForm.ShowDialog();
        }

        private void closeBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            var result = XtraMessageBox.Show("ნამდვილად გსურთ დახურვა?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result != DialogResult.Yes) return;

            if (!ReportHelper.CheckForSessionReportOpened()) return;

            DisposeMain();
            Close();
        }

    
        private void LoadTemplates()
        {
            ReloadSession();

            SessionTemplateType? sessionTemplateType = _currentSession?.SessionTemplate?.SessionTemplateType ?? _configuration.SessionTemplateType;

            if (_currentSession?.SessionTemplate != null && _currentSession?.SessionTemplate?.SessionTemplateType == null)
                sessionTemplateType = null;

            var templates = sessionTemplateType == null ? _databaseHelper.GetSessionTemplates()
                                                       : _databaseHelper.GetSessionTemplates().Where(t => t.SessionTemplateType == sessionTemplateType).ToList();

            templatesRepositoryItemLookUpEdit.DataSource = templates;

            var firstTemplate = templates.FirstOrDefault();
            _currentTemplate = templates.FirstOrDefault(t => t.UId == _currentSession?.SessionTemplate?.UId) ?? firstTemplate;

            sessionTemplateTypeBarEditItem.EditValue = sessionTemplateType;

            if (_currentSession.SessionTemplate == null)
            {
                _currentSession.SessionTemplate = firstTemplate;
                SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);
            }

            templatesLookupEdit.EditValue = _currentTemplate;
            actionsTreeList.DataSource = _currentTemplate?.SessionActions;
        }

        private void TemplatesRepositoryItemLookUpEditOnEditValueChanged(object sender, EventArgs eventArgs)
        {
            ReloadSession();

            var lookUpEdit = sender as LookUpEdit;
            _currentTemplate = lookUpEdit.GetSelectedDataRow() as SessionTemplate;

            _currentSession.SessionTemplate = _databaseHelper.GetSessionTemplates().FirstOrDefault(t => t.UId == _currentTemplate.UId);
            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            actionsTreeList.DataSource = _currentTemplate.SessionActions;
        }




        private void SessionTemplateTypeBarEditItemOnEditValueChanged(object sender, EventArgs eventArgs)
        {
            var sessionTemplateType = (SessionTemplateType)sessionTemplateTypeBarEditItem.EditValue;

            ReloadSession();

            var templates = _databaseHelper.GetSessionTemplates().Where(t => t.SessionTemplateType == sessionTemplateType).ToList();

            templatesRepositoryItemLookUpEdit.DataSource = templates;

            var firstTemplate = templates.FirstOrDefault();
            _currentTemplate = templates.FirstOrDefault(t => t.UId == _currentSession?.SessionTemplate?.UId) ?? firstTemplate;

            sessionTemplateTypeBarEditItem.EditValue = sessionTemplateType;

            _currentSession.SessionTemplate = _currentTemplate;
            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            templatesLookupEdit.EditValue = _currentTemplate;
            actionsTreeList.DataSource = _currentTemplate?.SessionActions;

            ConfigurationHelper.SetSessionTemplateType(sessionTemplateType);
        }

        private void questionAnswerbarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReloadSession();

            var sessionEvents = _currentSession.SessionEvents?.Where(p => p.Participant != null).OrderByDescending(o => o.Time).ThenByDescending(c => c.CreatedOn);
            if (sessionEvents == null || sessionEvents.Count() < 2) return;

            var participant = sessionEvents.ElementAt(1).Participant;

            var sessionEvent = new SessionEvent
            {
                UId = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                Time = GetCurrentTime(),
                Participant = participant
            };

            sessionEventUserControl.AddEvent(_currentSession, sessionEvent);
        }

        private void generateCprBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!ReportHelper.CheckForSessionReportExists(_currentSession)) return;

            SessionDbWrapper.SessionDbHelper.Dispose();

            EnableDisableInWriteMode(false);

            GenerateCprHelper.CreateCprFile(_sessionPath);

            SessionDbWrapper.Initialize(_sessionPath);

            EnableDisableInWriteMode(true);
        }

        private DateTime GetCurrentTime()
        {
            return _mode == Mode.Player ? playerUserControl.GetCurrentDateTime() : recorderUserControl.GetCurrentTime();
        }

        private void PlayerUserControlOnPlaybackStopped()
        {
            if (InvokeRequired)
                Invoke(new MethodInvoker(TogglePlayerButton));
            else
                TogglePlayerButton();
        }

        private void ReloadParticipantsDataSource()
        {
            var focusedRowHandle = participantsGridView.FocusedRowHandle;

            participantsGridControl.DataSource = _currentSession.Participants;

            participantsGridView.MakeRowVisible(focusedRowHandle);
        }

        private void ReloadSession()
        {
            _currentSession = SessionDbWrapper.SessionDbHelper.ReloadSession(_currentSession);
        }

        private void MainMouseDown(object sender, MouseEventArgs e)
        {
            var dxMouseEventArgs = e as DevExpress.Utils.DXMouseEventArgs;
            if (dxMouseEventArgs != null) dxMouseEventArgs.Handled = true;
        }

        public void DisposeMain()
        {
            try
            {
                recorderUserControl?.StopRecord();
            }
            catch { }

            try
            {
                playerUserControl?.DisposePlayer();
            }
            catch { }

            SessionDbWrapper.SessionDbHelper?.Dispose();
        }
    }
}