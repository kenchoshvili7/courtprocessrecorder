﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CourtProcessRecorder
{
    public partial class NewSessionForm : XtraForm
    {
        private string _sessionNumber;
        private string _sessionName;

        public NewSessionForm()
        {
            InitializeComponent();

            sessionDateTextEdit.Text = DateTime.Now.ToString("dd.MM.yyyy");

            sessionNumberTextEdit.KeyPress += OnEnterPress;
            sessionNameTextEdit.KeyPress += OnEnterPress;
        }

        public string SessionNumber { get { return _sessionNumber; } }

        public string SessionName { get { return _sessionName; } }

        private void okSimpleButton_Click(object sender, EventArgs e)
        {
            ProsecutorInfo.CaseNo = sessionNumberTextEdit.Text;
            ExecuteAction();
        }

        private void OnEnterPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                ExecuteAction();
        }

        private void ExecuteAction()
        {


            _sessionNumber = sessionNumberTextEdit.Text;
            _sessionName = sessionNameTextEdit.Text;

            ProsecutorInfo.CaseNo = _sessionNumber;

            if (string.IsNullOrWhiteSpace(_sessionNumber))
            {
                XtraMessageBox.Show("სხდომის ნომერი არ შეიძლება იყოს ცარიელი!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void cancelSimpleButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}