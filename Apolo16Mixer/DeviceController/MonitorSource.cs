﻿namespace DeviceController
{
    using System;

    [Flags]
    public enum MonitorSource : byte
    {
        Microphones = 1,
        None = 0,
        PC = 2
    }
}

