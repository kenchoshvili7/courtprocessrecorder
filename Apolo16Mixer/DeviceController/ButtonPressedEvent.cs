﻿namespace DeviceController
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ButtonPressedEvent(MixerChannels channelButton, bool pressed);
}

