﻿namespace DeviceController
{
    using System;

    [Flags]
    public enum MixerChannels : byte
    {
        Microphone1 = 1,
        Microphone2 = 2,
        Microphone3 = 4,
        Microphone4 = 8,
        Microphone5 = 0x10,
        Microphone6 = 0x20,
        Monitor = 0x40,
        None = 0
    }
}

