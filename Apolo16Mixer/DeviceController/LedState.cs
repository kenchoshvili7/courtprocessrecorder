﻿namespace DeviceController
{
    using System;

    public enum LedState : byte
    {
        Off = 0,
        On = 1
    }
}

