﻿namespace DeviceController
{
    using System;

    public enum MixerSamplingRate : byte
    {
        _11025hz = 1,
        _12000hz = 2,
        _16000hz = 3,
        _22050hz = 4,
        _24000hz = 5,
        _32000hz = 6,
        _36000hz = 7,
        _44100hz = 8,
        _48000hz = 9,
        _8000hz = 0,
        Syncronized = 0x80
    }
}

