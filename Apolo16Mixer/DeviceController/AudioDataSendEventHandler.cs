﻿namespace DeviceController
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate int AudioDataSendEventHandler(object sender, short[] data, int length);
}

