﻿namespace DeviceController
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void AudioDataReceivedEventHandler(object sender, short[][] data, int length, int[] peaks);
}

