﻿namespace DeviceController
{
    using CyUSB;
    using System;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;

    public class Apolo16Mixer
    {
        private bool _MICsMutedChanged;
        private bool _MICVolumesChanged;
        private bool _MonitorMuteChanged;
        private bool _MonitorVolumeChanged;
        private MixerChannels _mute = (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6 | MixerChannels.Monitor);
        private bool _powerSaveChanged;
        private MixerChannels buttonStates;
        public const int DeviceNo = 0x7ffffff1;
        private bool disposing;
        private const int FIRMWARE_VERSION = 0x101;
        private bool FirmwareUpdated;
        private bool FirmwareUpdateFailed = false;
        private const int IN_XFERSIZE = 0x1f8;
        private CyBulkEndPoint in1Endpoint = null;
        public CyBulkEndPoint inEndpoint = null;
        private Thread inXferThread;
        private LedState ledState;
        private bool ledStateChanged;
        public const int MICROPHONE_CHANNELS = 6;
        private byte[] MICVolumes = new byte[] { 0xd7, 0xd7, 0xd7, 0xd7, 0xd7, 0xd7 };
        private MixerSamplingRate mixerSamplingRate;
        private MonitorSource monitorSource = MonitorSource.None;
        private byte MonitorVolume = 0xff;
        private const int OUT_XFERSIZE = 0x200;
        private CyBulkEndPoint out1Endpoint = null;
        public CyBulkEndPoint outEndpoint = null;
        private Thread outXferThread;
        private bool playing;
        private bool powerSave;
        private bool recording;
        private CyFX2Device usbDevice = null;
        private USBDeviceList usbDevices = null;

        public event EventHandler Attached;

        public event ButtonPressedEvent ButtonsStateChanged;

        public event AudioDataReceivedEventHandler DataReceived;

        public event AudioDataSendEventHandler DataSent;

        public event EventHandler Removed;

        public event Apolo16MixerReseted Apolo16MixerReseted;

        public Apolo16Mixer()
        {
            usbDevices = new USBDeviceList(1);
            usbDevices.DeviceAttached += new EventHandler(DeviceAttached);
            usbDevices.DeviceRemoved += new EventHandler(DeviceRemoved);
            monitorSource = MonitorSource.None | MonitorSource.PC;
            ledState = LedState.On;
            SetDevice();
        }

        private byte ChannelToIndex(MixerChannels channel)
        {
            if (((byte)(channel & MixerChannels.Microphone1)) <= 0)
            {
                if (((byte)(channel & MixerChannels.Microphone2)) > 0)
                {
                    return 2;
                }
                if (((byte)(channel & MixerChannels.Microphone3)) > 0)
                {
                    return 3;
                }
                if (((byte)(channel & MixerChannels.Microphone4)) > 0)
                {
                    return 4;
                }
                if (((byte)(channel & MixerChannels.Microphone5)) > 0)
                {
                    return 5;
                }
                if (((byte)(channel & MixerChannels.Microphone6)) > 0)
                {
                    return 6;
                }
                if (((byte)(channel & MixerChannels.Monitor)) > 0)
                {
                    return 0x10;
                }
            }
            return 1;
        }

        protected void DeviceAttached(object sender, EventArgs e)
        {
            if ((!FirmwareUpdated && SetDevice()) && (Attached != null))
            {
                Attached(this, new EventArgs());
            }
        }

        protected void DeviceRemoved(object sender, EventArgs e)
        {
            if (!FirmwareUpdated)
            {
                SetDevice();
                if (Removed != null)
                {
                    Removed(this, new EventArgs());
                }
            }
        }

        public virtual void Dispose()
        {
            disposing = true;
            playing = false;
            recording = false;
            Thread.Sleep(100);
            if (usbDevice != null)
            {
                PowerSave = true;
                usbDevice = null;
                SetDevice();
            }
            if (usbDevices != null)
            {
                usbDevices.Dispose();
                usbDevices = null;
            }
        }

        protected void GetControlData()
        {
            if (Connected && (in1Endpoint != null))
            {
                byte[] buf = new byte[0x40];
                int length = buf.Length;
                int num2 = 0;
                in1Endpoint.XferData(ref buf, ref length);
                if (length == 15)
                {
                    monitorSource = (MonitorSource)buf[num2++];
                    mixerSamplingRate = (MixerSamplingRate)((byte)(buf[num2++] & 0x7f));
                    MICVolumes[0] = buf[num2++];
                    MICVolumes[1] = buf[num2++];
                    MICVolumes[2] = buf[num2++];
                    MICVolumes[3] = buf[num2++];
                    MICVolumes[4] = buf[num2++];
                    MICVolumes[5] = buf[num2++];
                    MonitorVolume = buf[num2++];
                    _mute = (MixerChannels)buf[num2++];
                    powerSave = buf[num2++] == 1;
                }
            }
        }

        public byte GetVolume(MixerChannels channel)
        {
            if (((byte)(channel & (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6))) != 0)
            {
                return MICVolumes[ChannelToIndex(channel) - 1];
            }
            if (((byte)(channel & MixerChannels.Monitor)) != 0)
            {
                return MonitorVolume;
            }
            return 0;
        }

        protected void inXferThreadProc()
        {
            bool flag = true;
            int index = 0;
            int num3 = 0;
            byte[] buf = new byte[0x200];
            int num4 = 0x1f40;
            short[][] data = new short[6][];
            int[] peaks = new int[6];
            int num5 = 0;
            while (num5 < 6)
            {
                data[num5] = new short[num4 + 1];
                num5++;
            }
            MixerChannels none = MixerChannels.None;
            while (recording && flag)
            {
                int length = buf.Length;
                flag = inEndpoint.XferData(ref buf, ref length);
                if (!flag)
                {
                    flag = inEndpoint.Reset();
                    OnReseted(flag);
                    Console.Write("RESET");
                }
                else
                {
                    buttonStates = (MixerChannels)((byte)(buf[0x1f8] & 0x7f));
                }
                for (num5 = 0; flag & (num5 < (length - 1)); num5 += 2)
                {
                    int num6 = (short)((buf[num5 + 1] << 8) | buf[num5]);
                    data[index][num3] = (short)num6;
                    if (num6 < 0)
                    {
                        num6 = -num6;
                    }
                    if (peaks[index] < num6)
                    {
                        peaks[index] = num6;
                    }
                    if (++index == 6)
                    {
                        index = 0;
                        if (++num3 == num4)
                        {
                            if (DataReceived != null)
                            {
                                DataReceived(this, data, num3, peaks);
                            }
                            num3 = 0;
                            while (num3 < 6)
                            {
                                peaks[num3] = 0;
                                num3++;
                            }
                            num3 = 0;
                        }
                    }
                }
                MixerChannels channelButton = (MixerChannels)((byte)(((byte)(none & (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6))) ^ ((byte)(buttonStates & (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6)))));
                none = buttonStates;
                if ((channelButton != MixerChannels.None) && (ButtonsStateChanged != null))
                {
                    ButtonsStateChanged(channelButton, ((byte)(buttonStates & channelButton)) == (byte)channelButton);
                }
            }
            recording = false;
        }

        protected void outXferThreadProc()
        {
            bool flag = true;
            int num2 = 0;
            byte[] buf = new byte[0x200];
            byte[] buffer2 = new byte[0x200];
            short[] data = new short[buf.Length * 5];
            int index = 0;
            while (index < buffer2.Length)
            {
                buffer2[index] = 0;
                index++;
            }
            while (playing && flag)
            {
                if (MonitorSource != (MonitorSource.None | MonitorSource.PC))
                {
                    Thread.Sleep(10);
                }
                else
                {
                    int length;
                    num2 = 0;
                    if ((DataSent != null) & flag)
                    {
                        num2 = DataSent(this, data, data.Length);
                    }
                    if (num2 == 0)
                    {
                        length = buffer2.Length;
                        flag = outEndpoint.XferData(ref buffer2, ref length);
                        if (!flag)
                        {
                            flag = outEndpoint.Reset();
                        }
                    }
                    else
                    {
                        int num3 = 0;
                        while ((playing && flag) && (num3 < num2))
                        {
                            for (index = 0; index < buf.Length; index += 2)
                            {
                                if (num3 >= num2)
                                {
                                    byte num6;
                                    buf[index] = (byte)(num6 = 0);
                                    buf[index + 1] = num6;
                                }
                                else
                                {
                                    short num5 = data[num3++];
                                    buf[index + 1] = (byte)((num5 & 0xff00) >> 8);
                                    buf[index] = (byte)(num5 & 0xff);
                                }
                            }
                            length = buf.Length;
                            flag = outEndpoint.XferData(ref buf, ref length);
                            if (!flag)
                            {
                                flag = outEndpoint.Reset();
                            }
                        }
                    }
                }
            }
            playing = false;
        }

        public bool Play()
        {
            if (!playing)
            {
                StopPlay();
                if (!Connected)
                {
                    return false;
                }
                MonitorSource = MonitorSource.None | MonitorSource.PC;
                playing = true;
                outXferThread = new Thread(new ThreadStart(outXferThreadProc));
                outXferThread.IsBackground = true;
                outXferThread.Start();
            }
            return true;
        }

        [DllImport("kernel32.dll")]
        public static extern int QueryPerformanceCounter(out long x);
        [DllImport("kernel32.dll")]
        public static extern int QueryPerformanceFrequency(out long x);
        public bool ReConnect()
        {
            return (Connected && usbDevice.ReConnect());
        }

        public bool Record()
        {
            StopRecord();
            if (!Connected)
            {
                return false;
            }
            MonitorSource = MonitorSource.Microphones;
            recording = true;
            inXferThread = new Thread(new ThreadStart(inXferThreadProc));
            inXferThread.IsBackground = true;
            inXferThread.Priority = ThreadPriority.Highest;
            inXferThread.Start();
            Thread.Sleep(10);
            if (!recording)
            {
                return false;
            }
            return true;
        }

        public bool Reset()
        {
            return (Connected && usbDevice.Reset());
        }

        protected void SendControlData()
        {
            if (Connected && (out1Endpoint != null))
            {
                byte[] buf = new byte[0x40];
                int len = 0;
                buf[len++] = (byte)monitorSource;
                buf[len++] = (byte)((ledStateChanged ? ((LedState)0x80) : LedState.Off) | ledState);
                buf[len++] = (byte)mixerSamplingRate;
                buf[len++] = _MICVolumesChanged ? ((byte)0xff) : ((byte)0);
                buf[len++] = MICVolumes[0];
                buf[len++] = MICVolumes[1];
                buf[len++] = MICVolumes[2];
                buf[len++] = MICVolumes[3];
                buf[len++] = MICVolumes[4];
                buf[len++] = MICVolumes[5];
                buf[len++] = _MonitorVolumeChanged ? ((byte)0xff) : ((byte)0);
                buf[len++] = MonitorVolume;
                buf[len++] = (byte)((byte)(_mute & (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4
                                        | MixerChannels.Microphone5 | MixerChannels.Microphone6)) | (byte)(_MICsMutedChanged ? 0x80 : (byte)MixerChannels.None));
                buf[len++] = (byte)(((((byte)(_mute & MixerChannels.Monitor)) != 0) ? 1 : 0) | (_MonitorMuteChanged ? 0x80 : 0));
                buf[len++] = (byte)((_powerSaveChanged ? 0x80 : 0) | (powerSave ? 1 : 0));
                out1Endpoint.XferData(ref buf, ref len);
            }
            mixerSamplingRate = (MixerSamplingRate)((byte)(mixerSamplingRate & ((MixerSamplingRate)0x7f)));
            _MICVolumesChanged = false;
            _MonitorVolumeChanged = false;
            _MICsMutedChanged = false;
            _MonitorMuteChanged = false;
            _powerSaveChanged = false;
        }

        protected bool SetDevice()
        {
            CyFX2Device cyDevice = null;
            if (!((usbDevices == null) || disposing))
            {
                cyDevice = usbDevices[0x4b4, 0x1004] as CyFX2Device;
            }
            if (cyDevice != null)
            {
                if (cyDevice.BcdDevice != 0x101)
                {
                    UpdateFirmware(cyDevice);
                    return false;
                }
                usbDevice = cyDevice;
                outEndpoint = usbDevice.EndPointOf(2) as CyBulkEndPoint;
                inEndpoint = usbDevice.EndPointOf(0x86) as CyBulkEndPoint;
                out1Endpoint = usbDevice.EndPointOf(1) as CyBulkEndPoint;
                in1Endpoint = usbDevice.EndPointOf(0x81) as CyBulkEndPoint;
                outEndpoint.Reset();
                inEndpoint.Reset();
                out1Endpoint.Reset();
                in1Endpoint.Reset();
                outEndpoint.TimeOut = 100;
                inEndpoint.TimeOut = 100;
                out1Endpoint.TimeOut = 100;
                in1Endpoint.TimeOut = 100;
                GetControlData();
                Mute = MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6 | MixerChannels.Monitor;
                powerSave = true;
                PowerSave = false;
                return true;
            }
            if (!disposing)
            {
                cyDevice = usbDevices[0x4b4, 0x8613] as CyFX2Device;
                if (cyDevice != null)
                {
                    UpdateFirmware(cyDevice);
                }
            }
            StopRecord();
            StopPlay();
            outEndpoint = null;
            inEndpoint = null;
            out1Endpoint = null;
            in1Endpoint = null;
            return false;
        }

        public void SetVolume(MixerChannels channel, byte volume)
        {
            byte monitorVolume;
            if (((byte)(channel & (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6))) != 0)
            {
                monitorVolume = MICVolumes[ChannelToIndex(channel) - 1];
                MICVolumes[ChannelToIndex(channel) - 1] = volume;
                _MICVolumesChanged = true;
                if (monitorVolume != volume)
                {
                    SendControlData();
                }
            }
            else if (((byte)(channel & MixerChannels.Monitor)) != 0)
            {
                monitorVolume = MonitorVolume;
                MonitorVolume = volume;
                _MonitorVolumeChanged = true;
                if (monitorVolume != volume)
                {
                    SendControlData();
                }
            }
        }

        public void StopPlay()
        {
            playing = false;
            if (outXferThread != null)
            {
                Thread.Sleep(500);
                if (outXferThread.IsAlive)
                {
                    outXferThread = null;
                }
                Mute = (MixerChannels)((byte)(Mute | MixerChannels.Monitor));
                MonitorSource = MonitorSource.None;
            }
        }

        public void StopRecord()
        {
            recording = false;
            if (inXferThread != null)
            {
                Thread.Sleep(500);
                if (inXferThread.IsAlive)
                {
                    inXferThread = null;
                }
            }
        }

        internal bool UpdateFirmware()
        {
            bool flag = UpdateFirmware(usbDevice);
            SetDevice();
            return flag;
        }

        internal bool UpdateFirmware(CyFX2Device cyDevice)
        {
            if (!FirmwareUpdateFailed && (cyDevice != null))
            {
                if (cyDevice.LoadEEPROM("firmware.iic", true))
                {
                    FirmwareUpdated = true;
                    cyDevice.Reset();
                    cyDevice.ReConnect();
                    MessageBox.Show("მოწყობილობის პროგრამა განახლდა წარმატებით. გთხოვთ გამორთოთ მოწყობილობა კვებიდან, გამორთოთ USB მავთული, შეართეთ თავიდან და შემდგომ დააჭირეთ OK ღილაკს!", "მოწყობილობის პროგრამის განახლება", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Application.Restart();
                    return true;
                }
                MessageBox.Show("Firmware update failed!", "Firmware update", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                FirmwareUpdateFailed = true;
            }
            return false;
        }

        public MixerChannels ButtonStates
        {
            get
            {
                return buttonStates;
            }
        }

        public bool Connected
        {
            get
            {
                return (usbDevice != null);
            }
        }

        public string DriverName
        {
            get
            {
                return (Connected ? usbDevice.DriverName : null);
            }
        }

        public int Frequency
        {
            get
            {
                switch (((MixerSamplingRate)((byte)(mixerSamplingRate & ((MixerSamplingRate)0x7f)))))
                {
                    case MixerSamplingRate._8000hz:
                        return 0x1f40;

                    case MixerSamplingRate._11025hz:
                        return 0x2b11;

                    case MixerSamplingRate._12000hz:
                        return 0x2ee0;

                    case MixerSamplingRate._16000hz:
                        return 0x3e80;

                    case MixerSamplingRate._22050hz:
                        return 0x5622;

                    case MixerSamplingRate._24000hz:
                        return 0x5dc0;

                    case MixerSamplingRate._32000hz:
                        return 0x7d00;

                    case MixerSamplingRate._36000hz:
                        return 0x8ca0;

                    case MixerSamplingRate._44100hz:
                        return 0xac44;

                    case MixerSamplingRate._48000hz:
                        return 0xbb80;
                }
                return 0x3e80;
            }
            set
            {
                switch (value)
                {
                    case 0x2ee0:
                        MixerSamplingRate = MixerSamplingRate._12000hz;
                        break;

                    case 0x3e80:
                        MixerSamplingRate = MixerSamplingRate._16000hz;
                        break;

                    case 0x5622:
                        MixerSamplingRate = MixerSamplingRate._22050hz;
                        break;

                    case 0x1f40:
                        MixerSamplingRate = MixerSamplingRate._8000hz;
                        break;

                    case 0x2b11:
                        MixerSamplingRate = MixerSamplingRate._11025hz;
                        break;

                    case 0x5dc0:
                        MixerSamplingRate = MixerSamplingRate._24000hz;
                        break;

                    case 0x7d00:
                        MixerSamplingRate = MixerSamplingRate._32000hz;
                        break;

                    case 0x8ca0:
                        MixerSamplingRate = MixerSamplingRate._36000hz;
                        break;

                    case 0xac44:
                        MixerSamplingRate = MixerSamplingRate._44100hz;
                        break;

                    case 0xbb80:
                        MixerSamplingRate = MixerSamplingRate._48000hz;
                        break;
                }
                MixerSamplingRate = MixerSamplingRate._16000hz;
            }
        }

        public string FriendlyName
        {
            get
            {
                return (Connected ? string.Concat(new string[5]) : null);
            }
        }

        public LedState LedState
        {
            get
            {
                if (usbDevice == null)
                {
                    return LedState.Off;
                }
                return ledState;
            }
            set
            {
                if (ledState != value)
                {
                    ledState = value;
                    ledStateChanged = true;
                    SendControlData();
                }
            }
        }

        public MixerSamplingRate MixerSamplingRate
        {
            get
            {
                return (MixerSamplingRate)((byte)(mixerSamplingRate & ((MixerSamplingRate)0x7f)));
            }
            set
            {
                if (mixerSamplingRate != value)
                {
                    mixerSamplingRate = (MixerSamplingRate)((byte)(value | MixerSamplingRate.Syncronized));
                    SendControlData();
                }
            }
        }

        public MonitorSource MonitorSource
        {
            get
            {
                if (Connected)
                {
                    return monitorSource;
                }
                return MonitorSource.None;
            }
            set
            {
                if (monitorSource != value)
                {
                    monitorSource = value;
                    SendControlData();
                }
            }
        }

        public MixerChannels Mute
        {
            get
            {
                return _mute;
            }
            set
            {
                MixerChannels channels = (MixerChannels)((byte)(_mute & (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6)));
                MixerChannels channels2 = (MixerChannels)((byte)(value & (MixerChannels.Microphone1 | MixerChannels.Microphone2 | MixerChannels.Microphone3 | MixerChannels.Microphone4 | MixerChannels.Microphone5 | MixerChannels.Microphone6)));
                if (((byte)(channels ^ channels2)) != 0)
                {
                    _MICsMutedChanged = true;
                }
                if (((byte)(((byte)(_mute & MixerChannels.Monitor)) ^ ((byte)(value & MixerChannels.Monitor)))) != 0)
                {
                    _MonitorMuteChanged = true;
                }
                if (_mute != value)
                {
                    _mute = value;
                    SendControlData();
                }
            }
        }

        public bool PowerSave
        {
            get
            {
                return powerSave;
            }
            set
            {
                if (value != powerSave)
                {
                    powerSave = value;
                    _powerSaveChanged = true;
                    SendControlData();
                }
            }
        }

        protected virtual void OnReseted(bool flag)
        {
            Apolo16MixerReseted?.Invoke(flag);
        }
    }
}

