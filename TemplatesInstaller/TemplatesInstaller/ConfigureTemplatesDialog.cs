﻿namespace TemplatesInstaller
{
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class ConfigureTemplatesDialog : BaseForm
    {
        private RadioButton btnAppeal;
        private Button btnCancel;
        private RadioButton btnFirstInstance;
        private Button btnOk;
        private RadioButton btnSupreme;
        private IContainer components = null;
        private GroupBox groupBox1;

        public ConfigureTemplatesDialog()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.btnCancel = new Button();
            this.btnOk = new Button();
            this.groupBox1 = new GroupBox();
            this.btnSupreme = new RadioButton();
            this.btnFirstInstance = new RadioButton();
            this.btnAppeal = new RadioButton();
            this.groupBox1.SuspendLayout();
            base.SuspendLayout();
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Font = new Font("Tahoma", 8.25f);
            this.btnCancel.Location = new Point(240, 0x7d);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x4b, 0x17);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnOk.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnOk.DialogResult = DialogResult.OK;
            this.btnOk.Font = new Font("Tahoma", 8.25f);
            this.btnOk.Location = new Point(0x9f, 0x7d);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new Size(0x4b, 0x17);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.groupBox1.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.groupBox1.Controls.Add(this.btnSupreme);
            this.groupBox1.Controls.Add(this.btnFirstInstance);
            this.groupBox1.Controls.Add(this.btnAppeal);
            this.groupBox1.Location = new Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(0x12f, 0x6b);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ინსტანციები";
            this.btnSupreme.AutoSize = true;
            this.btnSupreme.Location = new Point(6, 0x4a);
            this.btnSupreme.Name = "btnSupreme";
            this.btnSupreme.Size = new Size(0x9b, 20);
            this.btnSupreme.TabIndex = 2;
            this.btnSupreme.Text = "უზენაესი სასამართლო";
            this.btnSupreme.UseVisualStyleBackColor = true;
            this.btnFirstInstance.AutoSize = true;
            this.btnFirstInstance.Location = new Point(6, 0x30);
            this.btnFirstInstance.Name = "btnFirstInstance";
            this.btnFirstInstance.Size = new Size(0xde, 20);
            this.btnFirstInstance.TabIndex = 1;
            this.btnFirstInstance.Text = "პირველი ინსტანციის სასამართლო";
            this.btnFirstInstance.UseVisualStyleBackColor = true;
            this.btnAppeal.AutoSize = true;
            this.btnAppeal.Checked = true;
            this.btnAppeal.Location = new Point(6, 0x16);
            this.btnAppeal.Name = "btnAppeal";
            this.btnAppeal.Size = new Size(0xa8, 20);
            this.btnAppeal.TabIndex = 0;
            this.btnAppeal.TabStop = true;
            this.btnAppeal.Text = "სააპელაციო სასამართლო";
            this.btnAppeal.UseVisualStyleBackColor = true;
            base.AcceptButton = this.btnOk;
            base.AutoScaleDimensions = new SizeF(7f, 16f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x147, 160);
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.btnOk);
            base.Controls.Add(this.btnCancel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.Margin = new Padding(3, 4, 3, 4);
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ConfigureTemplatesDialog";
            this.Text = "აირჩიეთ ინსტანციის ტიპი";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            base.ResumeLayout(false);
        }

        public TemplatesInstaller.InstanceType InstanceType
        {
            get
            {
                if (this.btnFirstInstance.Checked)
                {
                    return TemplatesInstaller.InstanceType.FirstInstane;
                }
                if (this.btnSupreme.Checked)
                {
                    return TemplatesInstaller.InstanceType.Supreme;
                }
                return TemplatesInstaller.InstanceType.Appeal;
            }
        }
    }
}

