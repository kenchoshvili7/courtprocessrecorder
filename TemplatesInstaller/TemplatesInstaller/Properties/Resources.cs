﻿namespace TemplatesInstaller.Properties
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Resources;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode]
    internal class Resources
    {
        private static CultureInfo resourceCulture;
        private static System.Resources.ResourceManager resourceMan;

        internal Resources()
        {
        }

        internal static byte[] Appeal0
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("Appeal0", resourceCulture);
            }
        }

        internal static byte[] Appeal1
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("Appeal1", resourceCulture);
            }
        }

        internal static byte[] Appeal2
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("Appeal2", resourceCulture);
            }
        }

        internal static byte[] Appeal3
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("Appeal3", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static byte[] FirstInstance0
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance0", resourceCulture);
            }
        }

        internal static byte[] FirstInstance1
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance1", resourceCulture);
            }
        }

        internal static byte[] FirstInstance2
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance2", resourceCulture);
            }
        }

        internal static byte[] FirstInstance3
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance3", resourceCulture);
            }
        }

        internal static byte[] FirstInstance4
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance4", resourceCulture);
            }
        }

        internal static byte[] FirstInstance5
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance5", resourceCulture);
            }
        }

        internal static byte[] FirstInstance6
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance6", resourceCulture);
            }
        }

        internal static byte[] FirstInstance7
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance7", resourceCulture);
            }
        }

        internal static byte[] FirstInstance8
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance8", resourceCulture);
            }
        }

        internal static byte[] FirstInstance9
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("FirstInstance9", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager manager = new System.Resources.ResourceManager("TemplatesInstaller.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }

        internal static byte[] Supreme0
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("Supreme0", resourceCulture);
            }
        }

        internal static byte[] Supreme1
        {
            get
            {
                return (byte[]) ResourceManager.GetObject("Supreme1", resourceCulture);
            }
        }
    }
}

