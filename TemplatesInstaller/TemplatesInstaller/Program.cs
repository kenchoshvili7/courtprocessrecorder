﻿namespace TemplatesInstaller
{
    using CourtRecorderCommon;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Forms;
    using TemplatesInstaller.Properties;

    internal class Program
    {
        private static List<ResTemplateInfo> templateItems;

        private static void Main(string[] args)
        {
            if (!Directory.Exists(AudioIO.ApplicationPath + "Templates"))
            {
                using (ConfigureTemplatesDialog dialog = new ConfigureTemplatesDialog())
                {
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        foreach (ResTemplateInfo info in TemplateItems)
                        {
                            if (info.TemplateType == dialog.InstanceType)
                            {
                                string path = AudioIO.ApplicationPath + @"Templates\";
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }
                                File.WriteAllBytes(path + info.TemplateName, info.Template);
                            }
                        }
                    }
                    else
                    {
                        Process.GetCurrentProcess().Kill();
                    }
                }
            }
        }

        private static List<ResTemplateInfo> TemplateItems
        {
            get
            {
                if (templateItems == null)
                {
                    templateItems = new List<ResTemplateInfo>();
                    templateItems.Add(new ResTemplateInfo("Appeal0", "ადმინისტრაციულ საქმეთა პალატა (სხდომის ოქმი).clpt", InstanceType.Appeal, Resources.Appeal0));
                    templateItems.Add(new ResTemplateInfo("Appeal1", "საგამოძიებო კოლეგია(სხდომის ოქმი).clpt", InstanceType.Appeal, Resources.Appeal1));
                    templateItems.Add(new ResTemplateInfo("Appeal2", "სამოქალაქო საქმეთა პალატა(სხდომის ოქმი).clpt", InstanceType.Appeal, Resources.Appeal2));
                    templateItems.Add(new ResTemplateInfo("Appeal3", "სისხლის სამართლის საქმეთა პალატა(სხდომის ოქმი).clpt", InstanceType.Appeal, Resources.Appeal3));
                    templateItems.Add(new ResTemplateInfo("FirstInstance0", "ადმინისტრაციული განმწესრიგებელი სხდომის ოქმი.clpt", InstanceType.FirstInstane, Resources.FirstInstance0));
                    templateItems.Add(new ResTemplateInfo("FirstInstance1", "ადმინისტრაციული სამართალდარღვევა.clpt", InstanceType.FirstInstane, Resources.FirstInstance1));
                    templateItems.Add(new ResTemplateInfo("FirstInstance2", "აღკვეთის ღონისძიება.clpt", InstanceType.FirstInstane, Resources.FirstInstance2));
                    templateItems.Add(new ResTemplateInfo("FirstInstance3", "ოპერატიული.clpt", InstanceType.FirstInstane, Resources.FirstInstance3));
                    templateItems.Add(new ResTemplateInfo("FirstInstance4", "ს.ს. საქმის განხილვა  სასამართლო გამოძიების შეკვეცის შემთხვევაში (სხდომის ოქმი).clpt", InstanceType.FirstInstane, Resources.FirstInstance4));
                    templateItems.Add(new ResTemplateInfo("FirstInstance5", "სამოქალაქო საქმე სასამართლოს მთავარი სხდომის ოქმი.clpt", InstanceType.FirstInstane, Resources.FirstInstance5));
                    templateItems.Add(new ResTemplateInfo("FirstInstance6", "სამოქალაქო საქმე სასამართლოს მოსამზადებელი სხდომის ოქმი.clpt", InstanceType.FirstInstane, Resources.FirstInstance6));
                    templateItems.Add(new ResTemplateInfo("FirstInstance7", "სისხლის სამართლის საქმეთა სხდომის ოქმი (არსებითი განხილვა).clpt", InstanceType.FirstInstane, Resources.FirstInstance7));
                    templateItems.Add(new ResTemplateInfo("FirstInstance8", "სს საქმეთა სხდომის ოქმი (არსებითი განხილვასრული სასამართლო გამოძიება).clpt", InstanceType.FirstInstane, Resources.FirstInstance8));
                    templateItems.Add(new ResTemplateInfo("FirstInstance9", "სხდომის ოქმი სასამართლო გამოძიების არსებითი განხილვა (სრული სასამართლო გამოძიება).clpt", InstanceType.FirstInstane, Resources.FirstInstance9));
                    templateItems.Add(new ResTemplateInfo("Supreme0", "ადმინისტრაციულ საქმეთა პალატის სხდომის ოქმი.clpt", InstanceType.Supreme, Resources.Supreme0));
                    templateItems.Add(new ResTemplateInfo("Supreme1", "სამოქალაქო, სამეწარმეო და გაკოტრების საქმეთა პალატის სხდომის ოქმი.clpt", InstanceType.Supreme, Resources.Supreme1));
                }
                return templateItems;
            }
        }

        private class ResTemplateInfo
        {
            public string ResourceName;
            public byte[] Template;
            public string TemplateName;
            public InstanceType TemplateType;

            public ResTemplateInfo(string resourceName, string templateName, InstanceType templateType, byte[] template)
            {
                this.ResourceName = resourceName;
                this.TemplateName = templateName;
                this.TemplateType = templateType;
                this.Template = template;
            }
        }
    }
}

