﻿// Assembly TemplatesInstaller, Version 1.0.0.0

[assembly: System.Reflection.AssemblyCompany("Delta Systems")]
[assembly: System.Reflection.AssemblyProduct("Court Process Recorder Configuration Helper")]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9 Delta Systems 2008")]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Reflection.AssemblyDescription("Delta Systems Template Installer")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Reflection.AssemblyTitle("Delta Systems Template Installer")]
[assembly: System.Runtime.InteropServices.Guid("616bff85-421f-48d0-9ac0-479d50a29da5")]
[assembly: System.Reflection.AssemblyFileVersion("1.0.0.0")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.DisableOptimizations | System.Diagnostics.DebuggableAttribute.DebuggingModes.EnableEditAndContinue | System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | System.Diagnostics.DebuggableAttribute.DebuggingModes.Default)]

