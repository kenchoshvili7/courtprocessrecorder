﻿namespace AudioConfigurator
{
    using AudioConfiguration;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public class UpDownControl : DomainUpDown
    {
        private bool editable;
        public const int ES_NUMBER = 0x2000;
        public const int GWL_STYLE = -16;
        private decimal maxValue;
        private decimal minValue;
        private TextBox textBox;

        public UpDownControl()
        {
            this.Text = "";
            this.editable = true;
        }

        public override void DownButton()
        {
            decimal? nullable;
            if (!base.ReadOnly && (((nullable = this.Decimal).GetValueOrDefault() > this.MinValue) && nullable.HasValue))
            {
                base.DownButton();
                if (base.Items.Count == 0)
                {
                    this.Value -= 1;
                }
            }
        }

        [DllImport("user32")]
        public static extern int GetWindowLong(IntPtr hwnd, int nIndex);
        protected override void OnTextBoxKeyDown(object source, KeyEventArgs e)
        {
            if ((((e.KeyData & Keys.Control) == Keys.Control) && (e.KeyCode == Keys.V)) || (((e.KeyData & Keys.Shift) == Keys.Shift) && (e.KeyCode == Keys.Insert)))
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
            if (!this.editable)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                this.Editable = this.editable;
            }
            base.OnTextBoxKeyDown(source, e);
        }

        protected override void OnTextBoxTextChanged(object source, EventArgs e)
        {
            if (this.textBox == null)
            {
                this.textBox = (TextBox) source;
                this.textBox.MaxLength = 3;
                int windowLong = GetWindowLong(this.textBox.Handle, -16);
                SetWindowLong(this.textBox.Handle, -16, windowLong | 0x2000);
            }
            base.OnTextBoxTextChanged(source, e);
        }

        [DllImport("user32")]
        public static extern int SetWindowLong(IntPtr hwnd, int nIndex, int dwNewLong);
        public override void UpButton()
        {
            decimal? nullable;
            if (!base.ReadOnly && (((nullable = this.Decimal).GetValueOrDefault() < this.MaxValue) && nullable.HasValue))
            {
                base.UpButton();
                if (base.Items.Count == 0)
                {
                    this.Value += 1;
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public decimal? Decimal
        {
            get
            {
                decimal? nullable = Configuration.StringToDecimal(this.Text);
                decimal? nullable3 = nullable;
                decimal minValue = this.minValue;
                if ((nullable3.GetValueOrDefault() < minValue) && nullable3.HasValue)
                {
                    return new decimal?(this.minValue);
                }
                nullable3 = nullable;
                minValue = this.maxValue;
                if ((nullable3.GetValueOrDefault() > minValue) && nullable3.HasValue)
                {
                    return new decimal?(this.maxValue);
                }
                return nullable;
            }
            set
            {
                if (!value.HasValue)
                {
                    this.Text = null;
                }
                else
                {
                    this.Text = value.Value.ToString("0.00").Replace(",", ".");
                }
            }
        }

        public bool Editable
        {
            get
            {
                return this.editable;
            }
            set
            {
                this.editable = value;
                if (this.textBox != null)
                {
                    this.textBox.ReadOnly = true;
                    this.textBox.BackColor = SystemColors.Window;
                }
            }
        }

        public decimal MaxValue
        {
            get
            {
                return this.maxValue;
            }
            set
            {
                this.maxValue = value;
            }
        }

        public decimal MinValue
        {
            get
            {
                return this.minValue;
            }
            set
            {
                this.minValue = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int? Value
        {
            get
            {
                int result = 0;
                if (int.TryParse(this.Text, out result))
                {
                    if (result < this.minValue)
                    {
                        return new int?((int) this.minValue);
                    }
                    if (result > this.maxValue)
                    {
                        return new int?((int) this.maxValue);
                    }
                    return new int?(result);
                }
                return null;
            }
            set
            {
                if (!value.HasValue)
                {
                    this.Text = null;
                }
                else
                {
                    this.Text = value.ToString();
                }
            }
        }
    }
}

