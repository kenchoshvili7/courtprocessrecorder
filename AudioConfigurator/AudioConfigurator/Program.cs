﻿using CourtRecorderCommon;

namespace AudioConfigurator
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Forms;

    internal static class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            Process process = null;
            if (!Directory.Exists(AudioIO.ApplicationPath + "Templates"))
            {
                process = Process.Start(AudioIO.ApplicationPath + "TemplatesInstaller.exe");
            }
            if (process != null)
            {
                process.WaitForExit();
            }
            AppExceptionHandler.EnableExceptionHandling();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ConfigurationDialog());

            AppExceptionHandler.DisableExceptionHandling();
        }
    }
}

