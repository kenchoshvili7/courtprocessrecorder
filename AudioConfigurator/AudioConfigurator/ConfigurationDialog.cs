﻿using CourtRecorderCommon;

namespace AudioConfigurator
{
    using AudioConfiguration;
    using DeviceController;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;
    using Un4seen.Bass;
    using Un4seen.Bass.AddOn.Enc;
    using Un4seen.BassAsio;

    public class ConfigurationDialog : Form
    {
        private DeviceController.Apolo16Mixer apolo16Mixer;
        private Button btnBrowseFolder;
        private Button btnCancel;
        private Button btnChooseFormat;
        private Button btnReset;
        private Button btnSave;
        private ComboBox cbDevice;
        private ComboBox cbQuality;
        private IContainer components = null;
        private AudioConfiguration.Configuration configuration;
        private DataGridView dataGridView;
        private List<DeviceInfo> deviceChannels = new List<DeviceInfo>();
        private TextBox edtDirectory;
        private TextBox edtFreeSpaceWarn;
        private TextBox eFormat;
        private GroupBox groupBox1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private DataGridViewTextBoxColumn NAME;
        private DataGridViewTextBoxColumn NO;
        private DataGridViewComboBoxColumn TYPE;

        public ConfigurationDialog()
        {
            this.InitializeComponent();
            BassRegistration.Register();
            if (File.Exists(AudioIO.ApplicationPath + "multichannel"))
            {
                this.Configuration.ProgramConfiguration = ProgramConfiguration.MultiChannel;
            }
        }

        public void btnBrowseFolderClick(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.SelectedPath = this.edtDirectory.Text;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.edtDirectory.Text = dialog.SelectedPath;
                }
            }
        }

        private void btnCancelClick(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnChooseFormatClick(object sender, EventArgs e)
        {
            try
            {
                if ((this.cbDevice.SelectedItem is DeviceInfo) && (this.cbQuality.SelectedItem != null))
                {
                    BASSError error;
                    ACMFORMAT acmformat;
                    WaveFormatEx ex;
                    DeviceInfo selectedItem = this.cbDevice.SelectedItem as DeviceInfo;
                    if (this.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                    {
                        if (selectedItem.DeviceNo == 0x7ffffff1)
                        {
                            int handle = Un4seen.Bass.Bass.BASS_StreamCreateDummy(this.Apolo16Mixer.Frequency, 1, BASSFlag.BASS_DEFAULT, IntPtr.Zero);
                            error = Un4seen.Bass.Bass.BASS_ErrorGetCode();
                            if ((handle == 0) || ((error != BASSError.BASS_OK) && (error != BASSError.BASS_ERROR_ALREADY)))
                            {
                                throw new ApplicationException("შეცდომა ჩამწერი მოწყობილობის არჩევისას ! " + error.ToString());
                            }

                            acmformat = BassEnc.BASS_Encode_GetACMFormat(handle, null, BASSACMFormat.BASS_ACM_CHANS | BASSACMFormat.BASS_ACM_RATE, WAVEFormatTag.UNKNOWN);
                            if (acmformat != null)
                            {
                                ex = WaveFormatEx.BassACMFORMATToWaveFormatEx(acmformat);
                                this.eFormat.Text = ex.ToString();
                                this.eFormat.Tag = ex;
                            }
                            Un4seen.Bass.Bass.BASS_StreamFree(handle);
                        }
                        else
                        {
                            //Un4seen.BassAsio.BassAsio.BASS_ASIO_Init(selectedItem.DeviceNo); todo old
                            Un4seen.BassAsio.BassAsio.BASS_ASIO_Init(selectedItem.DeviceNo, BASSASIOInit.BASS_ASIO_DEFAULT);
                            error = Un4seen.BassAsio.BassAsio.BASS_ASIO_ErrorGetCode();
                            if ((error != BASSError.BASS_OK) && (error != BASSError.BASS_ERROR_ALREADY))
                            {
                                throw new ApplicationException("შეცდომა ჩამწერი მოწყობილობის არჩევისას ! " + error.ToString());
                            }
                            using (BassAsioHandler handler = new BassAsioHandler(true, selectedItem.DeviceNo, 0, 1, BASSASIOFormat.BASS_ASIO_FORMAT_16BIT, (double)((int)this.cbQuality.SelectedItem)))
                            {
                                if (!Un4seen.BassAsio.BassAsio.BASS_ASIO_ChannelSetRate(true, 0, (double)((int)this.cbQuality.SelectedItem)))
                                {
                                    error = Un4seen.BassAsio.BassAsio.BASS_ASIO_ErrorGetCode();
                                    throw new ApplicationException("შეცდომა ჩამწერი მოწყობილობას არ აქვს დაბალი ხარისხით ჩაწერის ფუნქცია! " + error.ToString());
                                }
                                acmformat = BassEnc.BASS_Encode_GetACMFormat(handler.InputChannel, null, BASSACMFormat.BASS_ACM_NONE, WAVEFormatTag.UNKNOWN);
                                if (acmformat != null)
                                {
                                    ex = WaveFormatEx.BassACMFORMATToWaveFormatEx(acmformat);
                                    this.eFormat.Text = ex.ToString();
                                    this.eFormat.Tag = ex;
                                }
                            }
                        }
                    }
                    else
                    {
                        Un4seen.Bass.Bass.BASS_RecordInit(selectedItem.DeviceNo);
                        error = Un4seen.Bass.Bass.BASS_ErrorGetCode();
                        if ((error != BASSError.BASS_OK) && (error != BASSError.BASS_ERROR_ALREADY))
                        {
                            throw new ApplicationException("შეცდომა ჩამწერი მოწყობილობის არჩევისას ! " + error.ToString());
                        }
                        int num = Un4seen.Bass.Bass.BASS_RecordStart((int)this.cbQuality.SelectedItem, 1, BASSFlag.BASS_DEFAULT, null, IntPtr.Zero);
                        if (num == 0)
                        {
                            error = Un4seen.Bass.Bass.BASS_ErrorGetCode();
                            throw new ApplicationException("შეცდომა ჩამწერი მოწყობილობის არჩევისას ! " + error.ToString());
                        }
                        acmformat = BassEnc.BASS_Encode_GetACMFormat(num, null, BASSACMFormat.BASS_ACM_CHANS | BASSACMFormat.BASS_ACM_RATE, WAVEFormatTag.UNKNOWN);
                        if (acmformat != null)
                        {
                            ex = WaveFormatEx.BassACMFORMATToWaveFormatEx(acmformat);
                            this.eFormat.Text = ex.ToString();
                            this.eFormat.Tag = ex;
                        }
                    }
                }
            }
            finally
            {
                Un4seen.Bass.Bass.BASS_RecordFree();
            }
        }

        private void btnResetClick(object sender, EventArgs e)
        {
            this.ReloadConfiguration();
        }

        private void btnSaveClick(object sender, EventArgs e)
        {
            if (this.cbDevice.SelectedItem == null)
            {
                MessageBox.Show(this, "მიუთითედ ჩამწერი მოწყობილობა!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.cbDevice.Focus();
            }
            else if (this.cbQuality.SelectedItem == null)
            {
                MessageBox.Show(this, "მიუთითედ ჩაწერის ხარისხი!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.cbQuality.Focus();
            }
            else if ((this.eFormat.Tag == null) || string.IsNullOrEmpty(this.eFormat.Text))
            {
                MessageBox.Show(this, "მიუთითედ ჩაწერის ფორმატი!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.eFormat.Focus();
            }
            else if (!(!string.IsNullOrEmpty(this.edtDirectory.Text) && Directory.Exists(this.edtDirectory.Text)))
            {
                MessageBox.Show(this, "არქივის მისამართი არ არის სწორად მითითებული!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.edtDirectory.Focus();
            }
            else
            {
                long result = 0x7fffffffffffffffL;
                if (!(!string.IsNullOrEmpty(this.edtFreeSpaceWarn.Text) && long.TryParse(this.edtFreeSpaceWarn.Text, out result)))
                {
                    MessageBox.Show(this, "არასწორი რიცხვის ფორმატი!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.edtFreeSpaceWarn.Focus();
                }
                else
                {
                    foreach (DataGridViewRow row in (IEnumerable)this.dataGridView.Rows)
                    {
                        if (string.IsNullOrEmpty((string)row.Cells[2].Value))
                        {
                            this.dataGridView.Focus();
                            row.Cells[2].Selected = true;
                            MessageBox.Show(this, "არხის დასახელება არ არის მითითებული!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            return;
                        }
                    }
                    this.SaveConfiguration();
                }
            }
        }

        private void cbDeviceSelectedIndexChanged(object sender, EventArgs e)
        {
            this.eFormat.Tag = null;
            this.eFormat.Text = "";
        }

        private void cbQualitySelectedIndexChanged(object sender, EventArgs e)
        {
            this.eFormat.Tag = null;
            this.eFormat.Text = "";
        }

        private BASSError DetectChannelInfos()
        {
            try
            {
                int num2;
                int num = 0;
                if (this.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                {
                    Console.WriteLine("Apolo16Mixer.Connected: " + this.Apolo16Mixer.Connected.ToString());
                    //if (this.Apolo16Mixer.Connected && Un4seen.Bass.Bass.BASS_Init(-1, this.Apolo16Mixer.Frequency, BASSInit.BASS_DEVICE_MONO, base.Handle, null)) todo old
                    if (this.Apolo16Mixer.Connected && Un4seen.Bass.Bass.BASS_Init(-1, this.Apolo16Mixer.Frequency, BASSInit.BASS_DEVICE_MONO, base.Handle))
                    {
                        this.deviceChannels.Add(new DeviceInfo(0x7ffffff1, this.Apolo16Mixer.FriendlyName, this.Apolo16Mixer.DriverName));
                    }
                    Console.WriteLine("deviceChannels.Count: " + this.deviceChannels.Count.ToString());
                    num = Un4seen.BassAsio.BassAsio.BASS_ASIO_GetDeviceCount();
                    Console.WriteLine("deviceCount: " + num.ToString());
                    if (num > 0)
                    {
                        BASS_ASIO_DEVICEINFO[] bass_asio_deviceinfoArray = Un4seen.BassAsio.BassAsio.BASS_ASIO_GetDeviceInfos();
                        for (num2 = 0; num2 < num; num2++)
                        {
                            //if (Un4seen.BassAsio.BassAsio.BASS_ASIO_Init(num2) && Un4seen.Bass.Bass.BASS_Init(num2, 0x5622, BASSInit.BASS_DEVICE_MONO, base.Handle, null)) todo old
                            //if (Un4seen.BassAsio.BassAsio.BASS_ASIO_Init(num2) && Un4seen.Bass.Bass.BASS_Init(num2, 0x5622, BASSInit.BASS_DEVICE_MONO, base.Handle)) todo old
                            if (BassAsio.BASS_ASIO_Init(num2, BASSASIOInit.BASS_ASIO_DEFAULT) && Bass.BASS_Init(num2, 0x5622, BASSInit.BASS_DEVICE_MONO, base.Handle))
                            {
                                if ((Un4seen.BassAsio.BassAsio.BASS_ASIO_GetDevice() == num2) || Un4seen.BassAsio.BassAsio.BASS_ASIO_SetDevice(num2))
                                {
                                    this.deviceChannels.Add(new DeviceInfo(num2, bass_asio_deviceinfoArray[num2].name, bass_asio_deviceinfoArray[num2].driver));
                                }
                                else
                                {
                                    BASSError error = Un4seen.BassAsio.BassAsio.BASS_ASIO_ErrorGetCode();
                                    Un4seen.BassAsio.BassAsio.BASS_ASIO_Free();
                                    break;
                                }
                            }
                        }
                    }
                    return BASSError.BASS_OK;
                }
                num = Un4seen.Bass.Bass.BASS_RecordGetDeviceCount();
                if (num > 0)
                {
                    BASS_DEVICEINFO[] bass_deviceinfoArray = Un4seen.Bass.Bass.BASS_RecordGetDeviceInfos();
                    for (num2 = 0; num2 < num; num2++)
                    {
                        if (Un4seen.Bass.Bass.BASS_RecordInit(num2) && ((Un4seen.Bass.Bass.BASS_RecordGetDevice() == num2) || Un4seen.Bass.Bass.BASS_RecordSetDevice(num2)))
                        {
                            BASS_RECORDINFO bass_recordinfo = Un4seen.Bass.Bass.BASS_RecordGetInfo();
                            for (int i = 0; i < bass_recordinfo.inputs; i++)
                            {
                                if ((Un4seen.Bass.Bass.BASS_RecordGetInputType(i) & BASSInputType.BASS_INPUT_TYPE_LINE) > BASSInputType.BASS_INPUT_TYPE_UNDEF)
                                {
                                    this.deviceChannels.Add(new DeviceInfo(num2, bass_deviceinfoArray[num2].name, bass_deviceinfoArray[num2].driver));
                                    break;
                                }
                            }
                        }
                        else
                        {
                            return Un4seen.Bass.Bass.BASS_ErrorGetCode();
                        }
                    }
                }
            }
            finally
            {
                Un4seen.Bass.Bass.BASS_RecordFree();
            }
            return BASSError.BASS_OK;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.groupBox1 = new GroupBox();
            this.btnBrowseFolder = new Button();
            this.edtDirectory = new TextBox();
            this.label5 = new Label();
            this.btnReset = new Button();
            this.btnCancel = new Button();
            this.btnSave = new Button();
            this.cbDevice = new ComboBox();
            this.label1 = new Label();
            this.label2 = new Label();
            this.cbQuality = new ComboBox();
            this.label3 = new Label();
            this.dataGridView = new DataGridView();
            this.NO = new DataGridViewTextBoxColumn();
            this.TYPE = new DataGridViewComboBoxColumn();
            this.NAME = new DataGridViewTextBoxColumn();
            this.eFormat = new TextBox();
            this.btnChooseFormat = new Button();
            this.label4 = new Label();
            this.edtFreeSpaceWarn = new TextBox();
            this.groupBox1.SuspendLayout();
            ((ISupportInitialize)this.dataGridView).BeginInit();
            base.SuspendLayout();
            this.groupBox1.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.groupBox1.Controls.Add(this.edtFreeSpaceWarn);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnBrowseFolder);
            this.groupBox1.Controls.Add(this.edtDirectory);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new Point(12, 0xe9);
            this.groupBox1.Margin = new Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new Padding(3, 4, 3, 4);
            this.groupBox1.Size = new Size(0x279, 0x77);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ზოგადი პარამეტრები";
            this.btnBrowseFolder.Location = new Point(0x250, 0x17);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new Size(0x23, 0x17);
            this.btnBrowseFolder.TabIndex = 2;
            this.btnBrowseFolder.Text = "...";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            this.btnBrowseFolder.Click += new EventHandler(this.btnBrowseFolderClick);
            this.edtDirectory.Location = new Point(0x61, 0x17);
            this.edtDirectory.Name = "edtDirectory";
            this.edtDirectory.ReadOnly = true;
            this.edtDirectory.Size = new Size(0x1e9, 0x17);
            this.edtDirectory.TabIndex = 1;
            this.label5.AutoSize = true;
            this.label5.Location = new Point(6, 0x1a);
            this.label5.Name = "label5";
            this.label5.Size = new Size(0x2f, 0x10);
            this.label5.TabIndex = 0;
            this.label5.Text = "არქივი";
            this.btnReset.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnReset.Location = new Point(0x1e9, 0x167);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new Size(0x4b, 0x17);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "ხელახალა";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new EventHandler(this.btnResetClick);
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Location = new Point(570, 0x167);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x4b, 0x17);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "გაუქმება";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new EventHandler(this.btnCancelClick);
            this.btnSave.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnSave.Location = new Point(0x198, 0x167);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new Size(0x4b, 0x17);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "შენახვა";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new EventHandler(this.btnSaveClick);
            this.cbDevice.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbDevice.Location = new Point(12, 0x1b);
            this.cbDevice.Name = "cbDevice";
            this.cbDevice.Size = new Size(0x10b, 0x18);
            this.cbDevice.TabIndex = 0;
            this.cbDevice.SelectedIndexChanged += new EventHandler(this.cbDeviceSelectedIndexChanged);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x88, 0x10);
            this.label1.TabIndex = 3;
            this.label1.Text = "ჩამწერი მოწყობილობა";
            this.label2.AutoSize = true;
            this.label2.Location = new Point(0x11d, 9);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x36, 0x10);
            this.label2.TabIndex = 3;
            this.label2.Text = "ხარისხი";
            this.cbQuality.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbQuality.Location = new Point(0x11d, 0x1b);
            this.cbQuality.Name = "cbQuality";
            this.cbQuality.Size = new Size(0x65, 0x18);
            this.cbQuality.TabIndex = 1;
            this.cbQuality.SelectedIndexChanged += new EventHandler(this.cbQualitySelectedIndexChanged);
            this.label3.AutoSize = true;
            this.label3.Location = new Point(0x185, 9);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x3e, 0x10);
            this.label3.TabIndex = 3;
            this.label3.Text = "ფორმატი";
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new DataGridViewColumn[] { this.NO, this.TYPE, this.NAME });
            this.dataGridView.Location = new Point(12, 0x3a);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.ScrollBars = ScrollBars.None;
            this.dataGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
            this.dataGridView.Size = new Size(0x279, 0xa2);
            this.dataGridView.TabIndex = 4;
            this.NO.FillWeight = 20f;
            this.NO.HeaderText = "№";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.NO.ToolTipText = "არხის №";
            this.TYPE.DataPropertyName = "ChannelType";
            this.TYPE.FillWeight = 30f;
            this.TYPE.HeaderText = "ტიპი";
            this.TYPE.MaxDropDownItems = 4;
            this.TYPE.Name = "TYPE";
            this.TYPE.ToolTipText = "არხის ტიპი";
            this.NAME.HeaderText = "დასახელება";
            this.NAME.Name = "NAME";
            this.NAME.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.eFormat.Location = new Point(0x188, 0x1b);
            this.eFormat.Name = "eFormat";
            this.eFormat.ReadOnly = true;
            this.eFormat.Size = new Size(0xd4, 0x17);
            this.eFormat.TabIndex = 2;
            this.btnChooseFormat.Location = new Point(610, 0x1b);
            this.btnChooseFormat.Name = "btnChooseFormat";
            this.btnChooseFormat.Size = new Size(0x23, 0x17);
            this.btnChooseFormat.TabIndex = 3;
            this.btnChooseFormat.Text = "...";
            this.btnChooseFormat.UseVisualStyleBackColor = true;
            this.btnChooseFormat.Click += new EventHandler(this.btnChooseFormatClick);
            this.label4.AutoSize = true;
            this.label4.Location = new Point(6, 0x37);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0xfc, 0x10);
            this.label4.TabIndex = 3;
            this.label4.Text = "გაფრთხილება თავისუფალი ადგილზე (MB)";
            this.edtFreeSpaceWarn.Location = new Point(0x220, 0x34);
            this.edtFreeSpaceWarn.Name = "edtFreeSpaceWarn";
            this.edtFreeSpaceWarn.Size = new Size(0x53, 0x17);
            this.edtFreeSpaceWarn.TabIndex = 4;
            this.edtFreeSpaceWarn.TextAlign = HorizontalAlignment.Right;
            base.AcceptButton = this.btnSave;
            base.AutoScaleDimensions = new SizeF(7f, 16f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x291, 0x18a);
            base.Controls.Add(this.btnChooseFormat);
            base.Controls.Add(this.cbQuality);
            base.Controls.Add(this.eFormat);
            base.Controls.Add(this.cbDevice);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.btnSave);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.btnReset);
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.dataGridView);
            this.Font = new Font("Sylfaen", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.Margin = new Padding(3, 4, 3, 4);
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ConfigurationDialog";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "კონფიგურაცია";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((ISupportInitialize)this.dataGridView).EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            BASSError error = this.DetectChannelInfos();
            Console.Write("BASSError error = DetectChannelInfos();");
            Console.WriteLine(error);
            this.cbQuality.Items.AddRange(new object[] { Quality.Poor, Quality.Normal, Quality.High });
            this.cbQuality.SelectedIndex = 1;
            this.cbDevice.DataSource = this.deviceChannels;
            this.TYPE.ValueType = typeof(ChannelType);
            this.TYPE.Items.Add(ChannelType.None);
            this.TYPE.Items.Add(ChannelType.Judge);
            this.TYPE.Items.Add(ChannelType.Side);
            this.TYPE.Items.Add(ChannelType.Accused);
            this.ReloadConfiguration();
        }

        public void ReloadConfiguration()
        {
            this.dataGridView.Rows.Clear();
            if (File.Exists("configuration.xml"))
            {
                this.configuration = AudioConfiguration.Configuration.Load();
                this.edtDirectory.Text = this.Configuration.ArchiveDirectory;
                foreach (Channel channel in this.Configuration.Channels)
                {
                    this.dataGridView.Rows.Add(new object[] { channel.ChannelNo, channel.ChannelType, channel.Name });
                }
            }
            else
            {
                this.configuration = new AudioConfiguration.Configuration();
                int? nullable = null;
                this.Configuration.AutoSaveInterval = nullable;
                nullable = null;
                this.Configuration.AutoDiskSpaceAllocation = nullable;
                this.Configuration.AutoFragmentationInterval = null;
                this.Configuration.DeviceNo = 0;
                this.Configuration.Quality = Quality.Normal;
                this.Configuration.FreeSpaceLimitWarn = 0x7fffffffffffffffL;
                this.edtDirectory.Text = null;
                this.dataGridView.Rows.Add(new object[] { AudioSourceChannels.Channel1, ChannelType.Judge, "მოსამართლე" });
                this.dataGridView.Rows.Add(new object[] { AudioSourceChannels.Channel2, ChannelType.Side, "მხარე1" });
                this.dataGridView.Rows.Add(new object[] { AudioSourceChannels.Channel3, ChannelType.Side, "მხარე2" });
                this.dataGridView.Rows.Add(new object[] { AudioSourceChannels.Channel4, ChannelType.Side, "აპელანტი" });
                this.dataGridView.Rows.Add(new object[] { AudioSourceChannels.Channel5, ChannelType.Accused, "ბრალდებული" });
                this.dataGridView.Rows.Add(new object[] { AudioSourceChannels.Channel6, ChannelType.None, "" });
            }
            DeviceInfo info = this.deviceChannels.Find(delegate (DeviceInfo i)
            {
                return i.DeviceNo == this.Configuration.DeviceNo;
            });
            if (info == null)
            {
                this.cbDevice.SelectedIndex = -1;
            }
            else
            {
                this.cbDevice.SelectedItem = info;
            }
            this.cbQuality.SelectedItem = this.Configuration.Quality;
            if (this.Configuration.Format == null)
            {
                this.eFormat.Text = null;
            }
            else
            {
                this.eFormat.Text = this.Configuration.Format.ToString();
            }
            this.eFormat.Tag = this.Configuration.Format;
            if (this.Configuration.FreeSpaceLimitWarn == 0x7fffffffffffffffL)
            {
                this.edtFreeSpaceWarn.Text = null;
            }
            else
            {
                this.edtFreeSpaceWarn.Text = ((this.Configuration.FreeSpaceLimitWarn / 0x400L) / 0x400L).ToString();
            }
        }

        public void SaveConfiguration()
        {
            this.Configuration.ArchiveDirectory = this.edtDirectory.Text;
            this.Configuration.DeviceNo = (this.cbDevice.SelectedItem as DeviceInfo).DeviceNo;
            this.Configuration.Format = (WaveFormatEx)this.eFormat.Tag;
            this.Configuration.Quality = (Quality)this.cbQuality.SelectedItem;
            long result = 0x7fffffffffffffffL;
            if (string.IsNullOrEmpty(this.edtFreeSpaceWarn.Text) || long.TryParse(this.edtFreeSpaceWarn.Text, out result))
            {
                this.Configuration.FreeSpaceLimitWarn = (result * 0x400L) * 0x400L;
            }
            this.Configuration.Channels.Clear();
            foreach (DataGridViewRow row in (IEnumerable)this.dataGridView.Rows)
            {
                //this.Configuration.Channels.Add(new Channel((AudioSourceChannels)row.Cells[0].Value, (ChannelType)row.Cells[1].Value, (string)row.Cells[2].Value));
            }
            this.Configuration.Save();
        }

        public DeviceController.Apolo16Mixer Apolo16Mixer
        {
            get
            {
                if (this.apolo16Mixer == null)
                {
                    this.apolo16Mixer = new DeviceController.Apolo16Mixer();
                }
                return this.apolo16Mixer;
            }
        }

        public AudioConfiguration.Configuration Configuration
        {
            get
            {
                if (this.configuration == null)
                {
                    try
                    {
                        this.configuration = AudioConfiguration.Configuration.Load();
                    }
                    catch
                    {
                        this.configuration = new AudioConfiguration.Configuration();
                    }
                }
                return this.configuration;
            }
        }
    }
}

