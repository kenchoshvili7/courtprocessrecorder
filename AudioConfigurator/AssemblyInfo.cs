﻿// Assembly AudioConfigurator, Version 1.0.0.23503

[assembly: System.Reflection.AssemblyTitle("Court Process Recorder Audio Channel Configurator")]
[assembly: System.Reflection.AssemblyDescription("Court Process Recorder Audio Channel Configurator")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyCompany("Court")]
[assembly: System.Reflection.AssemblyProduct("Court Process Recorder Audio Channel Configurator")]
[assembly: System.Reflection.AssemblyCopyright("")]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Runtime.InteropServices.Guid("ae743ea1-b1c7-4474-9b6b-3c6d4149bdbd")]
[assembly: System.Reflection.AssemblyFileVersion("1.0.0")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.DisableOptimizations | System.Diagnostics.DebuggableAttribute.DebuggingModes.EnableEditAndContinue | System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | System.Diagnostics.DebuggableAttribute.DebuggingModes.Default)]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]

