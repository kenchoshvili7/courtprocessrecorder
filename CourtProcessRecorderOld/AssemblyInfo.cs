﻿// Assembly Court Process Recorder, Version 1.0.0.23504

[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Runtime.InteropServices.Guid("6893d5c3-0cc9-426a-b6dd-0b5afaa15aa9")]
[assembly: System.Reflection.AssemblyProduct("Court Process Recorder")]
[assembly: System.Runtime.InteropServices.ComVisible(true)]
[assembly: System.Reflection.AssemblyFileVersion("1.0.0.23")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.DisableOptimizations | System.Diagnostics.DebuggableAttribute.DebuggingModes.EnableEditAndContinue | System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | System.Diagnostics.DebuggableAttribute.DebuggingModes.Default)]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9 Delta Systems 2008")]
[assembly: System.Reflection.AssemblyTitle("Court Process Recorder")]
[assembly: System.Reflection.AssemblyDescription("")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyCompany("Delta Systems")]
[assembly: System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.RequestMinimum, SkipVerification=true)]

