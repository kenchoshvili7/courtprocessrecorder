﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class ManagePersonsDialog : BaseForm
    {
        private ToolStripButton btnAdd;
        private Button btnCancel;
        private ToolStripButton btnImport;
        private Button btnOK;
        private ToolStripButton btnRemove;
        private ToolStripButton btnRename;
        private ToolStripSeparator btnSeparator1;
        private ToolStripSeparator btnSeparator2;
        private IContainer components = null;
        private ListBox listPersons;
        private ListBox listPersonTypes;
        private CourtProcessRecorder.DataTypes.Persons persons;
        private TextBox tbPersonName;
        private ToolStrip toolStripMain;

        public ManagePersonsDialog()
        {
            this.InitializeComponent();
            this.listPersonTypes.Items.AddRange(MainForm.CaseDocument.Templates.FindByName(MainForm.CaseDocument.TrialTemplates[MainForm.CurrentDate]).PersonTypes.ToArray());
            this.listPersons.Items.AddRange(this.Persons.ToArray());
            this.UpdateControls();
        }

        private void btnAddClick(object sender, EventArgs e)
        {
            if (this.listPersonTypes.SelectedItems.Count > 0)
            {
                if (this.Persons.FindByTypeAndName((string) this.listPersonTypes.SelectedItem, this.tbPersonName.Text) != null)
                {
                    throw new WarningException("ასეთი მონაწილე უკვე არსებობს!");
                }
                Person item = new Person(this.tbPersonName.Text, (string) this.listPersonTypes.SelectedItem);
                this.Persons.Add(item);
                int num = this.listPersons.Items.Add(item);
                this.listPersons.SelectedIndex = num;
            }
        }

        private void btnImportClick(object sender, EventArgs e)
        {
            using (OpenCaseDocumentDialog dialog = new OpenCaseDocumentDialog(false))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    using (ImportPersonsDialog dialog2 = new ImportPersonsDialog(dialog.CaseDocument))
                    {
                        if (dialog2.ShowDialog() == DialogResult.OK)
                        {
                            foreach (Person person in dialog2.Persons)
                            {
                                if (this.Persons.FindByTypeAndName(person.Type, person.Name) == null)
                                {
                                    this.Persons.Add(person);
                                }
                            }
                            this.listPersons.BeginUpdate();
                            try
                            {
                                this.listPersons.Items.Clear();
                                this.listPersons.Items.AddRange(this.Persons.ToArray());
                            }
                            finally
                            {
                                this.listPersons.EndUpdate();
                            }
                        }
                    }
                }
            }
        }

        private void btnRemoveClick(object sender, EventArgs e)
        {
            for (int i = this.listPersons.SelectedItems.Count - 1; i >= 0; i--)
            {
                this.Persons.Remove((Person) this.listPersons.SelectedItems[i]);
                this.listPersons.Items.RemoveAt(this.listPersons.SelectedIndices[i]);
            }
        }

        private void btnRenameClick(object sender, EventArgs e)
        {
            if (this.listPersons.SelectedItems.Count > 0)
            {
                Person item = (Person) this.listPersons.SelectedItems[0];
                if (this.Persons.FindByTypeAndName(item.Type, this.tbPersonName.Text) != null)
                {
                    throw new WarningException("ასეთი მონაწილე უკვე არსებობს!");
                }
                this.listPersons.BeginUpdate();
                try
                {
                    int selectedIndex = this.listPersons.SelectedIndex;
                    item.Name = this.tbPersonName.Text;
                    this.listPersons.Items.RemoveAt(selectedIndex);
                    this.listPersons.Items.Insert(selectedIndex, item);
                    this.listPersons.SelectedIndex = selectedIndex;
                }
                finally
                {
                    this.listPersons.EndUpdate();
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ManagePersonsDialog));
            this.listPersons = new ListBox();
            this.tbPersonName = new TextBox();
            this.listPersonTypes = new ListBox();
            this.btnOK = new Button();
            this.btnCancel = new Button();
            this.toolStripMain = new ToolStrip();
            this.btnRename = new ToolStripButton();
            this.btnSeparator1 = new ToolStripSeparator();
            this.btnAdd = new ToolStripButton();
            this.btnRemove = new ToolStripButton();
            this.btnSeparator2 = new ToolStripSeparator();
            this.btnImport = new ToolStripButton();
            this.toolStripMain.SuspendLayout();
            base.SuspendLayout();
            this.listPersons.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.listPersons.ItemHeight = 0x10;
            this.listPersons.Location = new Point(12, 12);
            this.listPersons.Name = "listPersons";
            this.listPersons.Size = new Size(0x148, 0x134);
            this.listPersons.TabIndex = 3;
            this.listPersons.SelectedIndexChanged += new EventHandler(this.listPersonsSelectedIndexChanged);
            this.tbPersonName.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.tbPersonName.Location = new Point(0x15a, 12);
            this.tbPersonName.Name = "tbPersonName";
            this.tbPersonName.Size = new Size(0x11b, 0x17);
            this.tbPersonName.TabIndex = 0;
            this.tbPersonName.TextChanged += new EventHandler(this.tbPersonNameTextChanged);
            this.listPersonTypes.Anchor = AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top;
            this.listPersonTypes.ItemHeight = 0x10;
            this.listPersonTypes.Location = new Point(0x15a, 0x2c);
            this.listPersonTypes.Name = "listPersonTypes";
            this.listPersonTypes.Size = new Size(0x11b, 0x114);
            this.listPersonTypes.TabIndex = 1;
            this.listPersonTypes.SelectedIndexChanged += new EventHandler(this.listPersonTypesSelectedIndexChanged);
            this.btnOK.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnOK.DialogResult = DialogResult.OK;
            this.btnOK.Location = new Point(0x219, 0x146);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new Size(0x4b, 0x17);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Location = new Point(0x26a, 0x146);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x4b, 0x17);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "გაუქმება";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.toolStripMain.Anchor = AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top;
            this.toolStripMain.AutoSize = false;
            this.toolStripMain.Dock = DockStyle.None;
            this.toolStripMain.Font = new Font("Sylfaen", 9f);
            this.toolStripMain.GripStyle = ToolStripGripStyle.Hidden;
            this.toolStripMain.ImageScalingSize = new Size(0x20, 0x20);
            this.toolStripMain.Items.AddRange(new ToolStripItem[] { this.btnRename, this.btnSeparator1, this.btnAdd, this.btnRemove, this.btnSeparator2, this.btnImport });
            this.toolStripMain.LayoutStyle = ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.toolStripMain.Location = new Point(0x278, 12);
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.Size = new Size(0x40, 0x133);
            this.toolStripMain.TabIndex = 2;
            this.toolStripMain.Text = "toolStrip1";
            this.btnRename.AutoSize = false;
            this.btnRename.Image = (Image) manager.GetObject("btnRename.Image");
            this.btnRename.ImageTransparentColor = Color.Magenta;
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new Size(0x40, 0x40);
            this.btnRename.Text = "შეცვლა";
            this.btnRename.TextImageRelation = TextImageRelation.ImageAboveText;
            this.btnRename.Click += new EventHandler(this.btnRenameClick);
            this.btnSeparator1.AutoSize = false;
            this.btnSeparator1.Name = "btnSeparator1";
            this.btnSeparator1.Size = new Size(0x40, 0x10);
            this.btnAdd.AutoSize = false;
            this.btnAdd.Image = (Image) manager.GetObject("btnAdd.Image");
            this.btnAdd.ImageTransparentColor = Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new Size(0x40, 0x40);
            this.btnAdd.Text = "დამატება";
            this.btnAdd.TextImageRelation = TextImageRelation.ImageAboveText;
            this.btnAdd.Click += new EventHandler(this.btnAddClick);
            this.btnRemove.AutoSize = false;
            this.btnRemove.Image = (Image) manager.GetObject("btnRemove.Image");
            this.btnRemove.ImageTransparentColor = Color.Magenta;
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new Size(0x40, 0x40);
            this.btnRemove.Text = "წაშლა";
            this.btnRemove.TextImageRelation = TextImageRelation.ImageAboveText;
            this.btnRemove.Click += new EventHandler(this.btnRemoveClick);
            this.btnSeparator2.AutoSize = false;
            this.btnSeparator2.Name = "btnSeparator2";
            this.btnSeparator2.Size = new Size(0x40, 0x10);
            this.btnImport.AutoSize = false;
            this.btnImport.Image = (Image) manager.GetObject("btnImport.Image");
            this.btnImport.ImageTransparentColor = Color.Magenta;
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new Size(0x40, 0x40);
            this.btnImport.Text = "იმპორტი";
            this.btnImport.TextImageRelation = TextImageRelation.ImageAboveText;
            this.btnImport.Click += new EventHandler(this.btnImportClick);
            base.AcceptButton = this.btnOK;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x2c1, 0x164);
            base.Controls.Add(this.toolStripMain);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.btnOK);
            base.Controls.Add(this.tbPersonName);
            base.Controls.Add(this.listPersonTypes);
            base.Controls.Add(this.listPersons);
            this.MinimumSize = new Size(0x23b, 390);
            base.Name = "ManagePersonsDialog";
            this.Text = "მონაწილეების განსაზღვრა";
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void listPersonsSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listPersons.SelectedItems.Count > 0)
            {
                this.tbPersonName.Text = ((Person) this.listPersons.SelectedItems[0]).Name;
            }
            else
            {
                this.tbPersonName.Text = "";
            }
            this.UpdateControls();
            this.tbPersonName.Focus();
        }

        private void listPersonTypesSelectedIndexChanged(object sender, EventArgs e)
        {
            this.tbPersonName.Text = "";
            this.UpdateControls();
            this.tbPersonName.Focus();
        }

        private void tbPersonNameTextChanged(object sender, EventArgs e)
        {
            this.UpdateControls();
        }

        private void UpdateControls()
        {
            this.btnAdd.Enabled = !string.IsNullOrEmpty(this.tbPersonName.Text) && (this.listPersonTypes.SelectedItems.Count > 0);
            this.btnRemove.Enabled = this.listPersons.SelectedItems.Count > 0;
            this.btnRename.Enabled = this.listPersons.SelectedItems.Count > 0;
        }

        public CourtProcessRecorder.DataTypes.Persons Persons
        {
            get
            {
                if (this.persons == null)
                {
                    if (MainForm.CaseDocument.TrialPersons.ContainsKey(MainForm.CurrentDate))
                    {
                        this.persons = (CourtProcessRecorder.DataTypes.Persons) MainForm.CaseDocument.TrialPersons[MainForm.CurrentDate].Clone();
                    }
                    else
                    {
                        this.persons = new CourtProcessRecorder.DataTypes.Persons();
                    }
                }
                return this.persons;
            }
        }
    }
}

