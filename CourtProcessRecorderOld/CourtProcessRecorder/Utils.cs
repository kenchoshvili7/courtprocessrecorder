﻿namespace CourtProcessRecorder
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    public static class Utils
    {
        public const uint FILE_ATTRIBUTE_NORMAL = 0x80;
        private const long SECTOR_SIZE = 0x800L;
        public const uint STGM_DELETEONRELEASE = 0x4000000;
        public const uint STGM_READ = 0;
        public const uint STGM_SHARE_DENY_NONE = 0x40;
        public const uint STGM_SHARE_DENY_WRITE = 0x20;

        public static string FormatFileSize(long fileSize)
        {
            string str;
            float num = fileSize;
            if (num >= 1.073742E+09f)
            {
                num /= 1.073742E+09f;
                str = " GB";
                return (num.ToString("n2") + str);
            }
            if (num >= 1048576f)
            {
                num /= 1048576f;
                str = " MB";
                return (num.ToString("n2") + str);
            }
            if (num > 1024f)
            {
                num /= 1024f;
                str = " KB";
                return (num.ToString("n2") + str);
            }
            str = " B";
            return (fileSize.ToString() + str);
        }

        public static long GetFreeSpace(string path)
        {
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo info in drives)
            {
                if (Path.GetPathRoot(path) == info.RootDirectory.Name)
                {
                    return info.AvailableFreeSpace;
                }
            }
            return 0x7fffffffffffffffL;
        }

        [DllImport("shlwapi.dll", EntryPoint="SHCreateStreamOnFileW", CharSet=CharSet.Unicode, ExactSpelling=true, PreserveSig=false)]
        public static extern void SHCreateStreamOnFile(string fileName, uint mode, ref IStream stream);
    }
}

