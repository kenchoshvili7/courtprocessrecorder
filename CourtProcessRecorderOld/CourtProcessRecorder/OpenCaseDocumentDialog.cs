﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using CourtProcessRecorder.Properties;
    using CourtRecorderCommon;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    public class OpenCaseDocumentDialog : BaseForm
    {
        private ToolStripButton btnBrowse;
        private Button btnCancel;
        private ToolStripButton btnFind;
        private ToolStripButton btnGotoArchive;
        private Button btnOK;
        private Button btnSignatures;
        private ColumnHeader chCaseNo;
        private ColumnHeader chFirstTrialDate;
        private ColumnHeader chLastTrialDate;
        private ColumnHeader chName;
        private ColumnHeader chSize;
        private ColumnHeader chType;
        private IContainer components = null;
        private bool continueCurrentDayTrial;
        private TextBox edtArchiveDirectory;
        private RichTextBox edtPreviewOrder;
        private ListViewItem hoverLVI = null;
        private ImageList imageList;
        private ListView listDocuments;
        private int lockItemChange;
        private ToolStrip toolbarMain;
        private ToolTip toolTip;

        public OpenCaseDocumentDialog(bool continueCurrentDayTrial)
        {
            if (!MainForm.MainFormInstance.Visible)
            {
                base.Icon = MainForm.MainFormInstance.Icon;
                base.ShowInTaskbar = true;
            }
            this.continueCurrentDayTrial = continueCurrentDayTrial;
            this.InitializeComponent();
            this.edtArchiveDirectory.Text = AudioIO.WorkingDirectory;
            this.edtPreviewOrder.BackColor = SystemColors.Window;
            this.UpdateGotoArchiveDirectoryVisibility();
        }

        private void btnBrowseClick(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.SelectedPath = this.edtArchiveDirectory.Text;
                if ((sender == this.btnGotoArchive) || (dialog.ShowDialog() == DialogResult.OK))
                {
                    this.edtArchiveDirectory.Text = dialog.SelectedPath;
                    AudioIO.WorkingDirectory = dialog.SelectedPath;
                    this.UpdateGotoArchiveDirectoryVisibility();
                    this.ReloadDocuments(this.continueCurrentDayTrial, null, null, null, null);
                }
            }
        }

        private void btnFindClick(object sender, EventArgs e)
        {
            using (NewTrialDialog dialog = new NewTrialDialog(NewTrialDialog.DialogType.Search))
            {
                dialog.Text = "ძებნა";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    CourtProcessRecorder.DataTypes.CaseDocument caseDocument = dialog.CaseDocument;
                    if (caseDocument != null)
                    {
                        this.ReloadDocuments(this.continueCurrentDayTrial, caseDocument.TrialNo, caseDocument.TrialName, new DateTime?(caseDocument.TrialDate), caseDocument.CDSerialNo);
                    }
                    else
                    {
                        this.ReloadDocuments(this.continueCurrentDayTrial, dialog.TrialNo, dialog.TrialName, new DateTime?(dialog.TrialDate), dialog.CDSerialNo);
                    }
                }
            }
        }

        private void btnGotoArchiveClick(object sender, EventArgs e)
        {
            this.edtArchiveDirectory.Text = AudioIO.ArchiveDirectory;
            this.btnBrowseClick(sender, e);
        }

        private void btnSignaturesClick(object sender, EventArgs e)
        {
            if (this.listDocuments.SelectedItems.Count > 0)
            {
                SignatureDialog.ShowDialog(this.CaseDocument, null);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(OpenCaseDocumentDialog));
            this.edtPreviewOrder = new RichTextBox();
            this.listDocuments = new ListView();
            this.chCaseNo = new ColumnHeader();
            this.chFirstTrialDate = new ColumnHeader();
            this.chLastTrialDate = new ColumnHeader();
            this.chName = new ColumnHeader();
            this.chType = new ColumnHeader();
            this.chSize = new ColumnHeader();
            this.imageList = new ImageList(this.components);
            this.edtArchiveDirectory = new TextBox();
            this.toolbarMain = new ToolStrip();
            this.btnBrowse = new ToolStripButton();
            this.btnFind = new ToolStripButton();
            this.btnGotoArchive = new ToolStripButton();
            this.btnCancel = new Button();
            this.btnOK = new Button();
            this.btnSignatures = new Button();
            this.toolTip = new ToolTip(this.components);
            this.toolbarMain.SuspendLayout();
            base.SuspendLayout();
            this.edtPreviewOrder.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.edtPreviewOrder.Location = new Point(12, 12);
            this.edtPreviewOrder.Name = "edtPreviewOrder";
            this.edtPreviewOrder.ReadOnly = true;
            this.edtPreviewOrder.Size = new Size(0x132, 0x19f);
            this.edtPreviewOrder.TabIndex = 6;
            this.edtPreviewOrder.Text = "";
            this.edtPreviewOrder.WordWrap = false;
            this.listDocuments.Anchor = AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top;
            this.listDocuments.Columns.AddRange(new ColumnHeader[] { this.chCaseNo, this.chFirstTrialDate, this.chLastTrialDate, this.chName, this.chType, this.chSize });
            this.listDocuments.FullRowSelect = true;
            this.listDocuments.GridLines = true;
            this.listDocuments.HideSelection = false;
            this.listDocuments.LabelWrap = false;
            this.listDocuments.Location = new Point(0x144, 0x27);
            this.listDocuments.MultiSelect = false;
            this.listDocuments.Name = "listDocuments";
            this.listDocuments.ShowGroups = false;
            this.listDocuments.ShowItemToolTips = true;
            this.listDocuments.Size = new Size(0x1e5, 0x167);
            this.listDocuments.SmallImageList = this.imageList;
            this.listDocuments.Sorting = SortOrder.Ascending;
            this.listDocuments.TabIndex = 2;
            this.listDocuments.UseCompatibleStateImageBehavior = false;
            this.listDocuments.View = View.Details;
            this.listDocuments.ItemActivate += new EventHandler(this.listDocumentsItemActivate);
            this.listDocuments.SelectedIndexChanged += new EventHandler(this.listDocumentsSelectedIndexChanged);
            this.listDocuments.ColumnClick += new ColumnClickEventHandler(this.listDocumentsColumnClick);
            this.listDocuments.MouseMove += new MouseEventHandler(this.listDocuments_MouseMove);
            this.chCaseNo.Text = "საქმის №";
            this.chCaseNo.Width = 0x52;
            this.chFirstTrialDate.Text = "დაწყების თარიღი";
            this.chFirstTrialDate.Width = 120;
            this.chLastTrialDate.Text = "ბოლო სხდომის თარიღი";
            this.chLastTrialDate.Width = 160;
            this.chName.Text = "სახელი";
            this.chName.Width = 150;
            this.chType.Text = "ტიპი";
            this.chType.Width = 150;
            this.chSize.Text = "ზომა";
            this.chSize.Width = 120;
            this.imageList.ImageStream = (ImageListStreamer) manager.GetObject("imageList.ImageStream");
            this.imageList.TransparentColor = Color.Magenta;
            this.imageList.Images.SetKeyName(0, "lock.bmp");
            this.imageList.Images.SetKeyName(1, "lock16_d.bmp");
            this.edtArchiveDirectory.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.edtArchiveDirectory.Location = new Point(0x144, 12);
            this.edtArchiveDirectory.Name = "edtArchiveDirectory";
            this.edtArchiveDirectory.ReadOnly = true;
            this.edtArchiveDirectory.Size = new Size(0x18a, 0x17);
            this.edtArchiveDirectory.TabIndex = 0;
            this.toolbarMain.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.toolbarMain.AutoSize = false;
            this.toolbarMain.Dock = DockStyle.None;
            this.toolbarMain.GripStyle = ToolStripGripStyle.Hidden;
            this.toolbarMain.ImageScalingSize = new Size(0x18, 0x18);
            this.toolbarMain.Items.AddRange(new ToolStripItem[] { this.btnBrowse, this.btnFind, this.btnGotoArchive });
            this.toolbarMain.Location = new Point(0x2d3, 0);
            this.toolbarMain.Name = "toolbarMain";
            this.toolbarMain.RenderMode = ToolStripRenderMode.System;
            this.toolbarMain.Size = new Size(0x56, 0x24);
            this.toolbarMain.TabIndex = 1;
            this.toolbarMain.Text = "toolStrip1";
            this.btnBrowse.AutoSize = false;
            this.btnBrowse.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnBrowse.Image = Resources.open_project24_h;
            this.btnBrowse.ImageTransparentColor = Color.Magenta;
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new Size(0x1c, 0x1c);
            this.btnBrowse.Text = "შერჩევა";
            this.btnBrowse.Click += new EventHandler(this.btnBrowseClick);
            this.btnFind.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnFind.Image = Resources.find_nextb24_h;
            this.btnFind.ImageTransparentColor = Color.Magenta;
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new Size(0x1c, 0x21);
            this.btnFind.Text = "ძებნა";
            this.btnFind.Click += new EventHandler(this.btnFindClick);
            this.btnGotoArchive.AutoSize = false;
            this.btnGotoArchive.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnGotoArchive.Image = Resources.new_project24_h;
            this.btnGotoArchive.ImageTransparentColor = Color.Magenta;
            this.btnGotoArchive.Name = "btnGotoArchive";
            this.btnGotoArchive.Size = new Size(0x1c, 0x1c);
            this.btnGotoArchive.Text = "არქივში დაბრუნება";
            this.btnGotoArchive.ToolTipText = "არქივში დაბრუნება";
            this.btnGotoArchive.Click += new EventHandler(this.btnGotoArchiveClick);
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Location = new Point(0x2d3, 0x194);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x56, 0x17);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "გაუქმება";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnOK.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnOK.DialogResult = DialogResult.OK;
            this.btnOK.Enabled = false;
            this.btnOK.Location = new Point(0x278, 0x194);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new Size(0x56, 0x17);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnSignatures.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnSignatures.Enabled = false;
            this.btnSignatures.Location = new Point(0x201, 0x194);
            this.btnSignatures.Name = "btnSignatures";
            this.btnSignatures.Size = new Size(0x73, 0x17);
            this.btnSignatures.TabIndex = 3;
            this.btnSignatures.Text = "ხელმოწერები...";
            this.btnSignatures.UseVisualStyleBackColor = true;
            this.btnSignatures.Click += new EventHandler(this.btnSignaturesClick);
            base.AcceptButton = this.btnOK;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x335, 0x1b4);
            base.Controls.Add(this.listDocuments);
            base.Controls.Add(this.btnSignatures);
            base.Controls.Add(this.btnOK);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.toolbarMain);
            base.Controls.Add(this.edtArchiveDirectory);
            base.Controls.Add(this.edtPreviewOrder);
            this.MinimumSize = new Size(580, 470);
            base.Name = "OpenCaseDocumentDialog";
            this.Text = "სხდომის გახსნა";
            this.toolbarMain.ResumeLayout(false);
            this.toolbarMain.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void listDocuments_MouseMove(object sender, MouseEventArgs e)
        {
            string str = null;
            ListViewItem itemAt = this.listDocuments.GetItemAt(e.X, e.Y);
            if (this.hoverLVI != itemAt)
            {
                this.hoverLVI = itemAt;
                if ((itemAt != null) && ((itemAt.Tag != null) && (itemAt.Tag is CourtProcessRecorder.DataTypes.CaseDocument)))
                {
                    CourtProcessRecorder.DataTypes.CaseDocument tag = itemAt.Tag as CourtProcessRecorder.DataTypes.CaseDocument;
                    foreach (DateTime time in tag._TrialDates)
                    {
                        CourtProcessRecorder.DataTypes.CaseDocument.SignatureData data;
                        str = str + string.Format("{0}:  ზომა: {1}, ", time.ToString("dd/MM/yyyy"), Utils.FormatFileSize(tag.CalculateSize(new DateTime?(time))));
                        if (tag.Signature.TryGetValue(time, out data))
                        {
                            str = str + string.Format("ხელმომწერი: {0}, {1}", data.Organization, data.ResponsiblePerson);
                            if (data.SignTime.HasValue)
                            {
                                str = str + string.Format(" ({0})", data.SignTime.Value.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                        }
                        else
                        {
                            str = str + "არა რის ხელმოწერილი";
                        }
                        str = str + Environment.NewLine;
                    }
                }
                this.toolTip.Active = false;
                if (!string.IsNullOrEmpty(str))
                {
                    Point point = this.listDocuments.PointToClient(Control.MousePosition);
                    this.toolTip.Active = true;
                    this.toolTip.Show(str, this.listDocuments, point.X + 10, point.Y + 50, 5);
                    this.toolTip.Show(str, this.listDocuments, point.X + 10, point.Y + 50, 0x1388);
                    Console.WriteLine(itemAt.Text);
                }
            }
        }

        private void listDocumentsColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (this.listDocuments.Columns[e.Column].Tag == null)
            {
                this.listDocuments.Columns[e.Column].Tag = true;
            }
            else
            {
                this.listDocuments.Columns[e.Column].Tag = !((bool) this.listDocuments.Columns[e.Column].Tag);
            }
            this.listDocuments.ListViewItemSorter = new ListViewItemComparer(e.Column, (bool) this.listDocuments.Columns[e.Column].Tag);
        }

        private void listDocumentsItemActivate(object sender, EventArgs e)
        {
            if (this.listDocuments.SelectedItems.Count > 0)
            {
                this.btnOK.PerformClick();
            }
        }

        private void listDocumentsSelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnOK.Enabled = this.listDocuments.SelectedItems.Count > 0;
            this.btnSignatures.Enabled = this.listDocuments.SelectedItems.Count > 0;
            if (this.lockItemChange == 0)
            {
                if (this.listDocuments.SelectedItems.Count > 0)
                {
                    CourtProcessRecorder.DataTypes.CaseDocument tag = (CourtProcessRecorder.DataTypes.CaseDocument) this.listDocuments.SelectedItems[0].Tag;
                    if (tag.Events.Count > 0)
                    {
                        this.edtPreviewOrder.Rtf = tag.GenerateOrderRtf(tag.Events[tag.Events.Count - 1].Time.Date);
                    }
                    else
                    {
                        this.edtPreviewOrder.Rtf = "დოკუმენტი ცარიელია";
                    }
                }
                else
                {
                    this.edtPreviewOrder.Rtf = null;
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.ReloadDocuments(this.continueCurrentDayTrial, null, null, null, null);
        }

        private void ReloadDocuments(bool filterByCurrentDay, string trialNo, string trialName, DateTime? trialDate, string cdSerialNo)
        {
            this.listDocuments.Items.Clear();
            this.edtPreviewOrder.Clear();
            this.lockItemChange++;
            if (filterByCurrentDay)
            {
                trialDate = new DateTime?(DateTime.Today);
            }
            try
            {
                foreach (string str in Directory.GetFiles(AudioIO.WorkingDirectory, "*.cpd"))
                {
                    try
                    {
                        DateTime? nullable;
                        CourtProcessRecorder.DataTypes.CaseDocument document = CourtProcessRecorder.DataTypes.CaseDocument.Open(str);
                        if ((!((string.IsNullOrEmpty(trialNo) || string.IsNullOrEmpty(document.TrialNo)) || document.TrialNo.Contains(trialNo)) || !((string.IsNullOrEmpty(trialName) || string.IsNullOrEmpty(document.TrialName)) || document.TrialName.Contains(trialName))) || !((string.IsNullOrEmpty(cdSerialNo) || string.IsNullOrEmpty(document.CDSerialNo)) || document.CDSerialNo.Contains(cdSerialNo)))
                        {
                            continue;
                        }
                        if (trialDate.HasValue)
                        {
                            bool flag = false;
                            foreach (Event event2 in document.Events)
                            {
                                DateTime date = event2.Time.Date;
                                nullable = trialDate;
                                if (nullable.HasValue && (date == nullable.GetValueOrDefault()))
                                {
                                    if (!filterByCurrentDay || !document.Signature.ContainsKey(trialDate.Value))
                                    {
                                        flag = true;
                                    }
                                    break;
                                }
                            }
                            if (!flag)
                            {
                                continue;
                            }
                        }
                        FileInfo info = new FileInfo(str);
                        nullable = null;
                        long fileSize = info.Length + document.CalculateSize(nullable);
                        ListViewItem item = new ListViewItem(new string[] { document.TrialNo, document.TrialDate.ToShortDateString(), document.Events[document.Events.Count - 1].Time.Date.ToShortDateString(), document.TrialName, document.TrialTemplates[document.Events[document.Events.Count - 1].Time.Date], Utils.FormatFileSize(fileSize) + ((document.RecordCount() == 0) ? "" : (", " + document.RecordCount().ToString() + " ჩანაწერი")) });
                        item.SubItems[1].Tag = document.TrialDate;
                        item.SubItems[2].Tag = document.Events[document.Events.Count - 1].Time.Date;
                        item.SubItems[5].Tag = fileSize;
                        item.Tag = document;
                        int num2 = -1;
                        foreach (DateTime time in document._TrialDates)
                        {
                            if (document.Signature.ContainsKey(time))
                            {
                                num2++;
                            }
                        }
                        if (num2 > -1)
                        {
                            if (num2 == (document._TrialDates.Length - 1))
                            {
                                num2 = 0;
                            }
                            else
                            {
                                num2 = 1;
                            }
                        }
                        item.ImageIndex = num2;
                        this.listDocuments.Items.Add(item);
                    }
                    catch (Exception exception1)
                    {
                        Console.WriteLine(exception1.ToString());
                    }
                }
            }
            catch (Exception exception2)
            {
                Console.WriteLine(exception2.ToString());
            }
            this.lockItemChange--;
            if (this.listDocuments.Items.Count > 0)
            {
                this.listDocuments.Items[0].Selected = true;
            }
        }

        private void UpdateGotoArchiveDirectoryVisibility()
        {
            if ((AudioIO.ProgramMode == ProgramMode.Playback) || (AudioIO.ArchiveDirectory == AudioIO.WorkingDirectory))
            {
                this.btnGotoArchive.Visible = false;
                this.edtArchiveDirectory.Width = 0x18a + this.btnGotoArchive.Width;
                this.toolbarMain.Left = 0x2d3 + this.btnGotoArchive.Width;
            }
            else
            {
                this.btnGotoArchive.Visible = true;
                this.edtArchiveDirectory.Width = 0x18a;
                this.toolbarMain.Left = 0x2d3;
            }
        }

        public CourtProcessRecorder.DataTypes.CaseDocument CaseDocument
        {
            get
            {
                if (this.listDocuments.SelectedItems.Count > 0)
                {
                    return (CourtProcessRecorder.DataTypes.CaseDocument) this.listDocuments.SelectedItems[0].Tag;
                }
                return null;
            }
        }

        private class ListViewItemComparer : IComparer
        {
            private int col;
            private int direction;

            public ListViewItemComparer()
            {
                this.col = 0;
                this.direction = 1;
            }

            public ListViewItemComparer(int column, bool direction)
            {
                this.col = column;
                if (direction)
                {
                    this.direction = 1;
                }
                else
                {
                    this.direction = -1;
                }
            }

            public int Compare(object x, object y)
            {
                if (((this.col == 0) || (this.col == 3)) || (this.col == 4))
                {
                    return (string.Compare(((ListViewItem) x).SubItems[this.col].Text, ((ListViewItem) y).SubItems[this.col].Text) * this.direction);
                }
                if ((this.col == 1) || (this.col == 2))
                {
                    return (DateTime.Compare((DateTime) ((ListViewItem) x).SubItems[this.col].Tag, (DateTime) ((ListViewItem) y).SubItems[this.col].Tag) * this.direction);
                }
                if (this.col == 5)
                {
                    long tag = (long) ((ListViewItem) x).SubItems[this.col].Tag;
                    long num2 = (long) ((ListViewItem) y).SubItems[this.col].Tag;
                    if (tag == num2)
                    {
                        return 0;
                    }
                    if (tag > num2)
                    {
                        return this.direction;
                    }
                    if (tag < num2)
                    {
                        return (-1 * this.direction);
                    }
                }
                return 0;
            }
        }
    }
}

