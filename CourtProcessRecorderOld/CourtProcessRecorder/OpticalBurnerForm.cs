﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.Controls;
    using CourtProcessRecorder.DataTypes;
    using CourtRecorderCommon;
    using IMAPI2;
    using IMAPI2.Interop;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;
    using System.Windows.Forms;

    public class OpticalBurnerForm : BaseForm
    {
        private FileInfo __fileInfo;
        private BackgroundWorker backgroundWorker;
        private Button btnBurn;
        private Button btnCancelClose;
        private bool burning;
        private BurningInfo burningInfo;
        private ComboBox cbDevices;
        private CheckBox cbStoreInstallationFiles;
        private ComboBox cbTrialDate;
        private string CDSerialNo;
        private CheckBox checkBoxCloseDisk;
        private ComboBox comboBoxWriteSpeed;
        private IContainer components = null;
        private DriveDetector diskDetector;
        private Dictionary<FileInfo, string> filesToWrite;
        private Label label1;
        private Label label11;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label labelBytesWritten;
        private Label labelBytesWrittenDescrip;
        private Label labelDiskFreeSize;
        private Label labelDiskSize;
        private Label labelDiskType;
        private Label labelElapsedTime;
        private Label labelElapsedTimeDescrip;
        private Label labelProgress;
        private Label labelRequiredSize;
        private Label labelSerialNo;
        private Label labelUsedBufferSize;
        private Label labelUsedBufferSizeDescrip;
        private Label labelVolumeName;
        private Label labelWriteSpeed;
        private Label labelWriteSpeedDescr;
        private Label labeRemainingTime;
        private Label labeRemainingTimeDescrip;
        private ProgressBar progressBar;
        public long SectorSize = 0x800L;

        public OpticalBurnerForm(string cdSerialNo)
        {
            this.InitializeComponent();
            this.CDSerialNo = cdSerialNo;
        }

        private void backgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            BurnConfiguration argument = e.Argument as BurnConfiguration;
            MsftDiscFormat2Data data = (MsftDiscFormat2Data)new MsftDiscFormat2DataClass();
            MsftDiscRecorder2 discRecorder = (MsftDiscRecorder2)new MsftDiscRecorder2Class();
            discRecorder.InitializeDiscRecorder(argument.DiscRecorder.ActiveDiscRecorder);
            discRecorder.AcquireExclusiveAccess(true, AppDomain.CurrentDomain.FriendlyName);
            try
            {
                data.Recorder = discRecorder;
                data.ClientName = AppDomain.CurrentDomain.FriendlyName;
                data.ForceMediaToBeClosed = argument.ForceMediaToBeClosed;
                data.SetWriteSpeed(argument.WriteSpeedDescriptor.WriteSpeed, argument.WriteSpeedDescriptor.RotationTypeIsPureCAV);
                data.BufferUnderrunFreeDisabled = false;
                this.burningInfo.CurrentWriteSpeed = data.CurrentWriteSpeed;
                object[] multisessionInterfaces = null;
                if (!data.MediaHeuristicallyBlank)
                {
                    multisessionInterfaces = data.MultisessionInterfaces;
                }
                System.Runtime.InteropServices.ComTypes.IStream stream = this.CreateMediaFileSystem(discRecorder, multisessionInterfaces);
                if (stream == null)
                {
                    e.Result = -1;
                    e.Cancel = true;
                }
                else
                {
                    data.Update += new DiscFormat2Data_EventHandler(this.WriteDiscDataUpdate);
                    try
                    {
                        data.Write(stream);
                        e.Result = 0;
                    }
                    catch (COMException exception)
                    {
                        e.Result = exception.ErrorCode;
                        this.labelProgress.Text = "დისკის ჩაწერის დროს დაფიქსირდა შეცდომა '" + ((ErrorCodes)exception.ErrorCode).ToString() + "'!";
                        this.labelProgress.ForeColor = Color.Red;
                        MessageBox.Show(exception.Message, this.labelProgress.Text, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    data.Update -= new DiscFormat2Data_EventHandler(this.WriteDiscDataUpdate);
                    discRecorder.EjectMedia();
                }
            }
            finally
            {
                discRecorder.ReleaseExclusiveAccess();
            }
        }

        private void backgroundWorkerProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BurningInfo userState = (BurningInfo)e.UserState;
            this.labelProgress.ForeColor = SystemColors.WindowText;
            if (userState.BurnStage == BurnStage.InitializingFileSystem)
            {
                this.labelProgress.Text = userState.ProgressText;
                this.progressBar.Value = e.ProgressPercentage;
            }
            else if (userState.BurnStage == BurnStage.WritingToTheDisc)
            {
                switch (userState.CurrentAction)
                {
                    case IMAPI_FORMAT2_DATA_WRITE_ACTION.IMAPI_FORMAT2_DATA_WRITE_ACTION_VALIDATING_MEDIA:
                        this.labelProgress.Text = "მიმდინარეობს ციფრული მატარებელის შემოწმება...";
                        break;

                    case IMAPI_FORMAT2_DATA_WRITE_ACTION.IMAPI_FORMAT2_DATA_WRITE_ACTION_FORMATTING_MEDIA:
                        this.labelProgress.Text = "მიმდინარეობს  ციფრული მატარებელის ფორმატირება...";
                        break;

                    case IMAPI_FORMAT2_DATA_WRITE_ACTION.IMAPI_FORMAT2_DATA_WRITE_ACTION_INITIALIZING_HARDWARE:
                        this.labelProgress.Text = "მიმდინარეობს  ჩამწერის ინიციალიზირება...";
                        break;

                    case IMAPI_FORMAT2_DATA_WRITE_ACTION.IMAPI_FORMAT2_DATA_WRITE_ACTION_CALIBRATING_POWER:
                        this.labelProgress.Text = "მიმდინარეობს  ჩამწერის კალიბრაცია...";
                        break;

                    case IMAPI_FORMAT2_DATA_WRITE_ACTION.IMAPI_FORMAT2_DATA_WRITE_ACTION_WRITING_DATA:
                        {
                            long num = userState.LastWrittenLba - userState.StartLba;
                            int num2 = 0;
                            if ((num > 0L) && (userState.SectorCount > 0))
                            {
                                num2 = (int)((num * 100L) / ((long)userState.SectorCount));
                            }
                            this.labelProgress.Text = string.Format("მიმდინარეობს ინფორმაციის ჩაწერა: {0}%", num2);
                            this.progressBar.Value = num2;
                            break;
                        }
                    case IMAPI_FORMAT2_DATA_WRITE_ACTION.IMAPI_FORMAT2_DATA_WRITE_ACTION_FINALIZATION:
                        this.labelProgress.Text = "ჩაწერის ფინალიზაცია...";
                        break;

                    case IMAPI_FORMAT2_DATA_WRITE_ACTION.IMAPI_FORMAT2_DATA_WRITE_ACTION_COMPLETED:
                        this.labelProgress.Text = "დისკის ჩაწერა დასრულდა წარმატებით!";
                        break;
                }
                this.UpdateLabels();
            }
        }

        private void backgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.burning = false;
            this.btnCancelClose.Text = "დახურვა";
            this.UpdateLabels();
            this.progressBar.Value = 0;
            if (e.Cancelled)
            {
                this.labelProgress.Text = "დისკის ჩაწერა შეწყვეტილია!";
            }
            else if (((int)e.Result) == 0)
            {
                this.labelProgress.Text = "დისკის ჩაწერა დასრულდა წარმატებით!";
                this.labelProgress.ForeColor = SystemColors.WindowText;
            }
            else
            {
                this.labelProgress.Text = "დისკის ჩაწერის დროს დაფიქსირდა შეცდომა!";
                this.labelProgress.ForeColor = Color.Red;
            }
            this.cbDevices.Enabled = true;
            this.cbTrialDate.Enabled = true;
            this.cbStoreInstallationFiles.Enabled = true;
        }

        private void btnBurnClick(object sender, EventArgs e)
        {
            this.burningInfo = new BurningInfo();
            BurnConfiguration argument = new BurnConfiguration();
            argument.DiscRecorder = this.cbDevices.SelectedItem as IDiscRecorder2;
            if (argument.DiscRecorder == null)
            {
                MessageBox.Show(this, "აირჩიეთ ჩამწერი მოწყობილობა!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                argument.WriteSpeedDescriptor = this.comboBoxWriteSpeed.SelectedItem as IWriteSpeedDescriptor;
                if (argument.WriteSpeedDescriptor == null)
                {
                    MessageBox.Show(this, "აირჩიეთ ჩაწერის სიჩწარე!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    argument.ForceMediaToBeClosed = this.checkBoxCloseDisk.Checked;
                    this.burning = true;
                    this.btnCancelClose.Enabled = true;
                    this.btnBurn.Enabled = false;
                    this.cbDevices.Enabled = false;
                    this.cbTrialDate.Enabled = false;
                    this.cbStoreInstallationFiles.Enabled = false;
                    this.btnCancelClose.Text = "გაუქმება";
                    this.backgroundWorker.RunWorkerAsync(argument);
                    this.UpdateLabels();
                }
            }
        }

        private void btnCancelCloseClick(object sender, EventArgs e)
        {
            if (this.burning)
            {
                this.btnCancelClose.Enabled = false;
                this.backgroundWorker.CancelAsync();
            }
            else
            {
                base.DialogResult = DialogResult.Cancel;
                base.Close();
            }
        }

        private void cbStoreInstallationFilesCheckedChanged(object sender, EventArgs e)
        {
            this.UpdateLabels();
        }

        private void cbTrialDateSelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateLabels();
        }

        private void comboBoxDevicesFormat(object sender, ListControlConvertEventArgs e)
        {
            IDiscRecorder2 listItem = (IDiscRecorder2)e.ListItem;
            string str = string.Empty;
            foreach (string str2 in listItem.VolumePathNames)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    str = str + "," + str2;
                }
                else
                {
                    str = str + str2;
                }
            }
            e.Value = string.Format("{0} [{1}]", listItem.ProductId, str);
        }

        private void comboBoxDevicesSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbDevices.SelectedIndex > -1)
            {
                IDiscRecorder2 selectedItem = (IDiscRecorder2)this.cbDevices.SelectedItem;
                IDiscFormat2Data data = (MsftDiscFormat2Data)new MsftDiscFormat2DataClass();
                if (!data.IsRecorderSupported(selectedItem))
                {
                    MessageBox.Show("ეს ჩამწერი ვერ უზრუნველყოფს დისკის ჩაწერას! აირჩიეთ სხვა ჩამწერი!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.cbDevices.SelectedIndex = -1;
                }
                else
                {
                    IDiscRecorder2 recorder2 = (IDiscRecorder2)this.cbDevices.SelectedItem;
                    foreach (string str in recorder2.VolumePathNames)
                    {
                        this.diskDetector.EnableQueryRemove(str);
                    }
                    this.UpdateLabels();
                }
            }
        }

        private void comboBoxWriteSpeedFormat(object sender, ListControlConvertEventArgs e)
        {
            IWriteSpeedDescriptor listItem = (IWriteSpeedDescriptor)e.ListItem;
            e.Value = this.FormatWriteSpeed(listItem.WriteSpeed);
        }

        private System.Runtime.InteropServices.ComTypes.IStream CreateMediaFileSystem(IDiscRecorder2 discRecorder, object[] multisessionInterfaces)
        {
            System.Runtime.InteropServices.ComTypes.IStream imageStream;
            MsftFileSystemImage image = (MsftFileSystemImage)new MsftFileSystemImageClass();
            image.ChooseImageDefaults(discRecorder);
            image.FileSystemsToCreate = FsiFileSystems.FsiFileSystemJoliet | FsiFileSystems.FsiFileSystemISO9660;
            image.VolumeName = this.labelVolumeName.Text;
            try
            {
                image.StageFiles = true;
            }
            catch
            {
            }
            image.Update += new DFileSystemImage_EventHandler(this.FileSystemImageUpdate);
            try
            {
                if (multisessionInterfaces != null)
                {
                    image.MultisessionInterfaces = multisessionInterfaces;
                    image.ImportFileSystem();
                }
                IFsiDirectoryItem root = image.Root;
                foreach (KeyValuePair<FileInfo, string> pair in this.FilesToWrite)
                {
                    this.__fileInfo = pair.Key;
                    if (this.backgroundWorker.CancellationPending)
                    {
                        return null;
                    }
                    System.Runtime.InteropServices.ComTypes.IStream stream = null;
                    Utils.SHCreateStreamOnFile(pair.Key.FullName, 0x20, ref stream);
                    if (stream != null)
                    {
                        string directoryName = Path.GetDirectoryName(pair.Value);
                        try
                        {
                            FsiItemType type = image.Exists(@"\" + directoryName);
                            if (!(string.IsNullOrEmpty(directoryName) || (type != FsiItemType.FsiItemNotFound)))
                            {
                                root.AddDirectory(directoryName);
                            }
                            root.AddFile(pair.Value, stream);
                        }
                        catch
                        {
                        }
                    }
                }
                imageStream = image.CreateResultImage().ImageStream;
            }
            finally
            {
                image.Update -= new DFileSystemImage_EventHandler(this.FileSystemImageUpdate);
                this.backgroundWorker.ReportProgress(0, this.burningInfo);
            }
            return imageStream;
        }

        private void diskDetectorDeviceArrived(object sender, DriveDetectorEventArgs e)
        {
            e.HookQueryRemove = true;
            //base.BeginInvoke(delegate { this.UpdateLabels(); });
            base.BeginInvoke(new MethodInvoker(UpdateLabels));
        }

        private void diskDetectorDeviceRemoved(object sender, DriveDetectorEventArgs e)
        {
            e.HookQueryRemove = true;
            //base.BeginInvoke(delegate { this.UpdateLabels(); });
            base.BeginInvoke(new MethodInvoker(UpdateLabels));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
            if (this.diskDetector != null)
            {
                this.diskDetector.Dispose();
                this.diskDetector = null;
            }
        }

        private void FileSystemImageUpdate([In, MarshalAs(UnmanagedType.IDispatch)] object sender, [In, MarshalAs(UnmanagedType.BStr)] string currentFile, [In] int copiedSectors, [In] int totalSectors)
        {
            int percentProgress = 0;
            if ((copiedSectors > 0) && (totalSectors > 0))
            {
                percentProgress = (copiedSectors * 100) / totalSectors;
            }
            if (this.__fileInfo != null)
            {
                this.burningInfo.ProgressText = "ემატება ფაილი \"" + this.__fileInfo.Name + "\" ...";
                this.burningInfo.BurnStage = BurnStage.InitializingFileSystem;
                this.backgroundWorker.ReportProgress(percentProgress, this.burningInfo);
            }
        }

        private string FormatDiskType(IMAPI_MEDIA_PHYSICAL_TYPE mediaType)
        {
            switch (mediaType)
            {
                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_CDROM:
                    return "CD-ROM";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_CDR:
                    return "CD-R";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_CDRW:
                    return "CD-RW";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDROM:
                    return "DVD ROM";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDRAM:
                    return "DVD-RAM";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDPLUSR:
                    return "DVD+R";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDPLUSRW:
                    return "DVD+RW";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDPLUSR_DUALLAYER:
                    return "DVD+R Dual Layer";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDDASHR:
                    return "DVD-R";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDDASHRW:
                    return "DVD-RW";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDDASHR_DUALLAYER:
                    return "DVD-R Dual Layer";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DISK:
                    return "random-access writes";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_DVDPLUSRW_DUALLAYER:
                    return "DVD+RW DL";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_HDDVDROM:
                    return "HD DVD-ROM";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_HDDVDR:
                    return "HD DVD-R";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_HDDVDRAM:
                    return "HD DVD-RAM";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_BDROM:
                    return "Blu-ray DVD (BD-ROM)";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_BDR:
                    return "Blu-ray media";

                case IMAPI_MEDIA_PHYSICAL_TYPE.IMAPI_MEDIA_TYPE_BDRE:
                    return "Blu-ray Rewritable media";
            }
            return "UNKNOWN";
        }

        private string FormatWriteSpeed(int sectorsPerSecond)
        {
            long fileSize = sectorsPerSecond * this.SectorSize;
            return string.Format("{0}x ({1}/Sec)", fileSize / 0x25800L, Utils.FormatFileSize(fileSize));
        }

        private void InitializeComponent()
        {
            this.btnBurn = new Button();
            this.btnCancelClose = new Button();
            this.progressBar = new ProgressBar();
            this.labelProgress = new Label();
            this.cbDevices = new ComboBox();
            this.label1 = new Label();
            this.label2 = new Label();
            this.labelDiskType = new Label();
            this.label3 = new Label();
            this.labelDiskSize = new Label();
            this.label4 = new Label();
            this.labelDiskFreeSize = new Label();
            this.label5 = new Label();
            this.labelRequiredSize = new Label();
            this.label6 = new Label();
            this.labelVolumeName = new Label();
            this.labelElapsedTimeDescrip = new Label();
            this.labelElapsedTime = new Label();
            this.labeRemainingTimeDescrip = new Label();
            this.labeRemainingTime = new Label();
            this.labelBytesWrittenDescrip = new Label();
            this.labelBytesWritten = new Label();
            this.labelUsedBufferSizeDescrip = new Label();
            this.labelUsedBufferSize = new Label();
            this.label11 = new Label();
            this.labelSerialNo = new Label();
            this.backgroundWorker = new BackgroundWorker();
            this.checkBoxCloseDisk = new CheckBox();
            this.comboBoxWriteSpeed = new ComboBox();
            this.labelWriteSpeedDescr = new Label();
            this.labelWriteSpeed = new Label();
            this.cbStoreInstallationFiles = new CheckBox();
            this.label7 = new Label();
            this.cbTrialDate = new ComboBox();
            base.SuspendLayout();
            this.btnBurn.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnBurn.Enabled = false;
            this.btnBurn.Location = new Point(0x130, 0x11b);
            this.btnBurn.Name = "btnBurn";
            this.btnBurn.Size = new Size(0x4b, 0x17);
            this.btnBurn.TabIndex = 5;
            this.btnBurn.Text = "ჩაწერა";
            this.btnBurn.UseVisualStyleBackColor = true;
            this.btnBurn.Click += new EventHandler(this.btnBurnClick);
            this.btnCancelClose.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCancelClose.DialogResult = DialogResult.Cancel;
            this.btnCancelClose.Location = new Point(0x181, 0x11b);
            this.btnCancelClose.Name = "btnCancelClose";
            this.btnCancelClose.Size = new Size(0x4b, 0x17);
            this.btnCancelClose.TabIndex = 6;
            this.btnCancelClose.Text = "დახურვა";
            this.btnCancelClose.UseVisualStyleBackColor = true;
            this.btnCancelClose.Click += new EventHandler(this.btnCancelCloseClick);
            this.progressBar.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.progressBar.Location = new Point(12, 0xed);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new Size(0x1c0, 0x17);
            this.progressBar.TabIndex = 2;
            this.labelProgress.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.labelProgress.Location = new Point(12, 0x107);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new Size(0x1c0, 0x11);
            this.labelProgress.TabIndex = 3;
            this.labelProgress.Text = "მოქმედება";
            this.cbDevices.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbDevices.Font = new Font("Microsoft Sans Serif", 8.25f);
            this.cbDevices.FormattingEnabled = true;
            this.cbDevices.Location = new Point(0x9a, 0x29);
            this.cbDevices.Name = "cbDevices";
            this.cbDevices.Size = new Size(0x132, 0x15);
            this.cbDevices.TabIndex = 1;
            this.cbDevices.SelectedIndexChanged += new EventHandler(this.comboBoxDevicesSelectedIndexChanged);
            this.cbDevices.Format += new ListControlConvertEventHandler(this.comboBoxDevicesFormat);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(12, 0x2b);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x88, 0x10);
            this.label1.TabIndex = 5;
            this.label1.Text = "ჩამწერი მოწყობილობა";
            this.label2.AutoSize = true;
            this.label2.Location = new Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x51, 0x10);
            this.label2.TabIndex = 6;
            this.label2.Text = "დისკის ტიპი";
            this.labelDiskType.AutoSize = true;
            this.labelDiskType.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelDiskType.Location = new Point(0x89, 0x52);
            this.labelDiskType.Name = "labelDiskType";
            this.labelDiskType.Size = new Size(0x34, 13);
            this.labelDiskType.TabIndex = 6;
            this.labelDiskType.Text = "DiskType";
            this.label3.AutoSize = true;
            this.label3.Location = new Point(12, 0x6a);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x6c, 0x10);
            this.label3.TabIndex = 6;
            this.label3.Text = "დისკის ტევადობა";
            this.labelDiskSize.AutoSize = true;
            this.labelDiskSize.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelDiskSize.Location = new Point(0x89, 0x6c);
            this.labelDiskSize.Name = "labelDiskSize";
            this.labelDiskSize.Size = new Size(0x30, 13);
            this.labelDiskSize.TabIndex = 6;
            this.labelDiskSize.Text = "DiskSize";
            this.label4.AutoSize = true;
            this.label4.Location = new Point(12, 0x84);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x77, 0x10);
            this.label4.TabIndex = 6;
            this.label4.Text = "ცარიელი ტევადობა";
            this.labelDiskFreeSize.AutoSize = true;
            this.labelDiskFreeSize.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelDiskFreeSize.Location = new Point(0x89, 0x85);
            this.labelDiskFreeSize.Name = "labelDiskFreeSize";
            this.labelDiskFreeSize.Size = new Size(0x45, 13);
            this.labelDiskFreeSize.TabIndex = 6;
            this.labelDiskFreeSize.Text = "DiskFreeSize";
            this.label5.AutoSize = true;
            this.label5.Location = new Point(12, 0x9e);
            this.label5.Name = "label5";
            this.label5.Size = new Size(110, 0x10);
            this.label5.TabIndex = 6;
            this.label5.Text = "საჭირო ტევადობა";
            this.labelRequiredSize.AutoSize = true;
            this.labelRequiredSize.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelRequiredSize.Location = new Point(0x89, 0x9e);
            this.labelRequiredSize.Name = "labelRequiredSize";
            this.labelRequiredSize.Size = new Size(70, 13);
            this.labelRequiredSize.TabIndex = 6;
            this.labelRequiredSize.Text = "RequiredSize";
            this.label6.AutoSize = true;
            this.label6.Location = new Point(12, 0xb7);
            this.label6.Name = "label6";
            this.label6.Size = new Size(0x76, 0x10);
            this.label6.TabIndex = 6;
            this.label6.Text = "დისკის დასახელება";
            this.labelVolumeName.AutoSize = true;
            this.labelVolumeName.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelVolumeName.Location = new Point(0x89, 0xb6);
            this.labelVolumeName.Name = "labelVolumeName";
            this.labelVolumeName.Size = new Size(70, 13);
            this.labelVolumeName.TabIndex = 6;
            this.labelVolumeName.Text = "VolumeName";
            this.labelElapsedTimeDescrip.AutoSize = true;
            this.labelElapsedTimeDescrip.Location = new Point(0xea, 0x6b);
            this.labelElapsedTimeDescrip.Name = "labelElapsedTimeDescrip";
            this.labelElapsedTimeDescrip.Size = new Size(0x5c, 0x10);
            this.labelElapsedTimeDescrip.TabIndex = 6;
            this.labelElapsedTimeDescrip.Text = "გავლილი დრო";
            this.labelElapsedTimeDescrip.Visible = false;
            this.labelElapsedTime.AutoSize = true;
            this.labelElapsedTime.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelElapsedTime.Location = new Point(370, 0x6b);
            this.labelElapsedTime.Name = "labelElapsedTime";
            this.labelElapsedTime.Size = new Size(0x44, 13);
            this.labelElapsedTime.TabIndex = 6;
            this.labelElapsedTime.Text = "ElapsedTime";
            this.labelElapsedTime.Visible = false;
            this.labeRemainingTimeDescrip.AutoSize = true;
            this.labeRemainingTimeDescrip.Location = new Point(0xea, 0x85);
            this.labeRemainingTimeDescrip.Name = "labeRemainingTimeDescrip";
            this.labeRemainingTimeDescrip.Size = new Size(0x67, 0x10);
            this.labeRemainingTimeDescrip.TabIndex = 6;
            this.labeRemainingTimeDescrip.Text = "სავარაუდო დრო";
            this.labeRemainingTimeDescrip.Visible = false;
            this.labeRemainingTime.AutoSize = true;
            this.labeRemainingTime.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labeRemainingTime.Location = new Point(370, 0x85);
            this.labeRemainingTime.Name = "labeRemainingTime";
            this.labeRemainingTime.Size = new Size(80, 13);
            this.labeRemainingTime.TabIndex = 6;
            this.labeRemainingTime.Text = "RemainingTime";
            this.labeRemainingTime.Visible = false;
            this.labelBytesWrittenDescrip.AutoSize = true;
            this.labelBytesWrittenDescrip.Location = new Point(0xea, 0x9e);
            this.labelBytesWrittenDescrip.Name = "labelBytesWrittenDescrip";
            this.labelBytesWrittenDescrip.Size = new Size(70, 0x10);
            this.labelBytesWrittenDescrip.TabIndex = 6;
            this.labelBytesWrittenDescrip.Text = "ჩაწერილია";
            this.labelBytesWrittenDescrip.Visible = false;
            this.labelBytesWritten.AutoSize = true;
            this.labelBytesWritten.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelBytesWritten.Location = new Point(370, 0x9e);
            this.labelBytesWritten.Name = "labelBytesWritten";
            this.labelBytesWritten.Size = new Size(0x43, 13);
            this.labelBytesWritten.TabIndex = 6;
            this.labelBytesWritten.Text = "BytesWritten";
            this.labelBytesWritten.Visible = false;
            this.labelUsedBufferSizeDescrip.AutoSize = true;
            this.labelUsedBufferSizeDescrip.Location = new Point(0xea, 0xb7);
            this.labelUsedBufferSizeDescrip.Name = "labelUsedBufferSizeDescrip";
            this.labelUsedBufferSizeDescrip.Size = new Size(130, 0x10);
            this.labelUsedBufferSizeDescrip.TabIndex = 6;
            this.labelUsedBufferSizeDescrip.Text = "ბუფერის დატვირთვა";
            this.labelUsedBufferSizeDescrip.Visible = false;
            this.labelUsedBufferSize.AutoSize = true;
            this.labelUsedBufferSize.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelUsedBufferSize.Location = new Point(370, 0xb7);
            this.labelUsedBufferSize.Name = "labelUsedBufferSize";
            this.labelUsedBufferSize.Size = new Size(80, 13);
            this.labelUsedBufferSize.TabIndex = 6;
            this.labelUsedBufferSize.Text = "UsedBufferSize";
            this.labelUsedBufferSize.Visible = false;
            this.label11.AutoSize = true;
            this.label11.Location = new Point(12, 0xcf);
            this.label11.Name = "label11";
            this.label11.Size = new Size(0x79, 0x10);
            this.label11.TabIndex = 6;
            this.label11.Text = "დისკის სერიული №";
            this.labelSerialNo.AutoSize = true;
            this.labelSerialNo.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelSerialNo.Location = new Point(0x89, 0xcf);
            this.labelSerialNo.Name = "labelSerialNo";
            this.labelSerialNo.Size = new Size(0x2f, 13);
            this.labelSerialNo.TabIndex = 6;
            this.labelSerialNo.Text = "SerialNo";
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new DoWorkEventHandler(this.backgroundWorkerDoWork);
            this.backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.backgroundWorkerRunWorkerCompleted);
            this.backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(this.backgroundWorkerProgressChanged);
            this.checkBoxCloseDisk.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
            this.checkBoxCloseDisk.AutoSize = true;
            this.checkBoxCloseDisk.Location = new Point(12, 0x11e);
            this.checkBoxCloseDisk.Name = "checkBoxCloseDisk";
            this.checkBoxCloseDisk.Size = new Size(0x79, 20);
            this.checkBoxCloseDisk.TabIndex = 4;
            this.checkBoxCloseDisk.Text = "დისკის დახურვა";
            this.checkBoxCloseDisk.UseVisualStyleBackColor = true;
            this.comboBoxWriteSpeed.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxWriteSpeed.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.comboBoxWriteSpeed.FormattingEnabled = true;
            this.comboBoxWriteSpeed.Location = new Point(340, 0x4e);
            this.comboBoxWriteSpeed.Name = "comboBoxWriteSpeed";
            this.comboBoxWriteSpeed.Size = new Size(120, 0x15);
            this.comboBoxWriteSpeed.TabIndex = 2;
            this.comboBoxWriteSpeed.Format += new ListControlConvertEventHandler(this.comboBoxWriteSpeedFormat);
            this.labelWriteSpeedDescr.AutoSize = true;
            this.labelWriteSpeedDescr.Location = new Point(0xea, 0x51);
            this.labelWriteSpeedDescr.Name = "labelWriteSpeedDescr";
            this.labelWriteSpeedDescr.Size = new Size(100, 0x10);
            this.labelWriteSpeedDescr.TabIndex = 6;
            this.labelWriteSpeedDescr.Text = "ჩაწერის სიჩქარე";
            this.labelWriteSpeed.AutoSize = true;
            this.labelWriteSpeed.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelWriteSpeed.Location = new Point(370, 0x51);
            this.labelWriteSpeed.Name = "labelWriteSpeed";
            this.labelWriteSpeed.Size = new Size(0x26, 13);
            this.labelWriteSpeed.TabIndex = 6;
            this.labelWriteSpeed.Text = "Speed";
            this.labelWriteSpeed.Visible = false;
            this.cbStoreInstallationFiles.AutoSize = true;
            this.cbStoreInstallationFiles.CheckAlign = ContentAlignment.MiddleRight;
            this.cbStoreInstallationFiles.Checked = true;
            this.cbStoreInstallationFiles.CheckState = CheckState.Checked;
            this.cbStoreInstallationFiles.Location = new Point(0x107, 0xce);
            this.cbStoreInstallationFiles.Name = "cbStoreInstallationFiles";
            this.cbStoreInstallationFiles.Size = new Size(0xc5, 20);
            this.cbStoreInstallationFiles.TabIndex = 3;
            this.cbStoreInstallationFiles.Text = "საინსტალაციო პაკეტის ჩაწერა";
            this.cbStoreInstallationFiles.UseVisualStyleBackColor = true;
            this.cbStoreInstallationFiles.CheckedChanged += new EventHandler(this.cbStoreInstallationFilesCheckedChanged);
            this.label7.AutoSize = true;
            this.label7.Location = new Point(12, 14);
            this.label7.Name = "label7";
            this.label7.Size = new Size(0x6c, 0x10);
            this.label7.TabIndex = 5;
            this.label7.Text = "სხდომის თარიღი";
            this.cbTrialDate.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbTrialDate.Font = new Font("Microsoft Sans Serif", 8.25f);
            this.cbTrialDate.FormattingEnabled = true;
            this.cbTrialDate.Location = new Point(0x9a, 12);
            this.cbTrialDate.Name = "cbTrialDate";
            this.cbTrialDate.Size = new Size(0x132, 0x15);
            this.cbTrialDate.TabIndex = 0;
            base.AutoScaleDimensions = new SizeF(7f, 16f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this.btnCancelClose;
            base.ClientSize = new Size(0x1d8, 0x13e);
            base.Controls.Add(this.cbStoreInstallationFiles);
            base.Controls.Add(this.checkBoxCloseDisk);
            base.Controls.Add(this.labelSerialNo);
            base.Controls.Add(this.labelVolumeName);
            base.Controls.Add(this.labelRequiredSize);
            base.Controls.Add(this.label11);
            base.Controls.Add(this.labelDiskFreeSize);
            base.Controls.Add(this.label6);
            base.Controls.Add(this.labelDiskSize);
            base.Controls.Add(this.label5);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.labelWriteSpeed);
            base.Controls.Add(this.labelUsedBufferSize);
            base.Controls.Add(this.labelBytesWritten);
            base.Controls.Add(this.labelWriteSpeedDescr);
            base.Controls.Add(this.labelUsedBufferSizeDescrip);
            base.Controls.Add(this.labeRemainingTime);
            base.Controls.Add(this.labelBytesWrittenDescrip);
            base.Controls.Add(this.labelElapsedTime);
            base.Controls.Add(this.labeRemainingTimeDescrip);
            base.Controls.Add(this.labelDiskType);
            base.Controls.Add(this.labelElapsedTimeDescrip);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label7);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.comboBoxWriteSpeed);
            base.Controls.Add(this.cbTrialDate);
            base.Controls.Add(this.cbDevices);
            base.Controls.Add(this.labelProgress);
            base.Controls.Add(this.progressBar);
            base.Controls.Add(this.btnCancelClose);
            base.Controls.Add(this.btnBurn);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.Margin = new Padding(3, 4, 3, 4);
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "OpticalBurnerForm";
            this.Text = "დისკის ჩაწერა";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            e.Cancel = this.burning;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.diskDetector = new DriveDetector(this);
            this.diskDetector.DeviceArrived += new DriveDetectorEventHandler(this.diskDetectorDeviceArrived);
            this.diskDetector.DeviceRemoved += new DriveDetectorEventHandler(this.diskDetectorDeviceRemoved);
            this.cbTrialDate.BeginUpdate();
            this.cbTrialDate.Items.Add("(ყველა)");
            foreach (DateTime time in MainForm.CaseDocument._TrialDates)
            {
                this.cbTrialDate.Items.Add(new DateSigned(time, MainForm.CaseDocument.Signature.ContainsKey(time)));
            }
            this.cbTrialDate.EndUpdate();
            this.cbTrialDate.SelectedItem = new DateSigned(MainForm.CurrentDate, MainForm.CaseDocument.Signature.ContainsKey(MainForm.CurrentDate));
            if ((this.cbTrialDate.SelectedIndex == -1) && (this.cbTrialDate.Items.Count > 0))
            {
                this.cbTrialDate.SelectedIndex = 0;
            }
            this.cbTrialDate.SelectedIndexChanged += new EventHandler(this.cbTrialDateSelectedIndexChanged);
            MsftDiscMaster2 master = (MsftDiscMaster2)new MsftDiscMaster2Class();
            if (!master.IsSupportedEnvironment)
            {
                MessageBox.Show(Form.ActiveForm, "შესაბამისი დრაივერები (IMAPI2) ან ჩამწერი მოწყობილობა არ არის დაყენებული კომპიუტერში! მიმართეთ სისტემურ ადმინისტრატორს!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                base.DialogResult = DialogResult.Abort;
                base.Close();
            }
            else
            {
                foreach (string str in master)
                {
                    MsftDiscRecorder2 item = (MsftDiscRecorder2)new MsftDiscRecorder2Class();
                    try
                    {
                        item.InitializeDiscRecorder(str);
                        this.cbDevices.Items.Add(item);
                    }
                    catch
                    {
                        if (item != null)
                        {
                            item = null;
                        }
                    }
                }
                if (this.cbDevices.Items.Count > 0)
                {
                    this.cbDevices.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show(Form.ActiveForm, "ჩამწერი მოწყობილობა არ მოიძებნა!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    base.DialogResult = DialogResult.Abort;
                    base.Close();
                    return;
                }
                if (this.cbDevices.SelectedIndex == -1)
                {
                    this.UpdateLabels();
                }
            }
        }

        private void UpdateLabels()
        {
            DateTime now = DateTime.Now;
            this.labelVolumeName.Text = MainForm.CaseDocument.TrialNo;
            this.labelSerialNo.Text = this.CDSerialNo;
            string str = "-";
            bool flag = false;
            if (!this.burning)
            {
                string str2 = "-";
                string str3 = "-";
                string str4 = "-";
                this.filesToWrite = null;
                this.labelRequiredSize.ForeColor = this.labelDiskFreeSize.ForeColor = SystemColors.WindowText;
                this.cbStoreInstallationFiles.Visible = true;
                IDiscRecorder2 selectedItem = this.cbDevices.SelectedItem as IDiscRecorder2;
                if (selectedItem != null)
                {
                    MsftDiscFormat2Data data = (MsftDiscFormat2Data)new MsftDiscFormat2DataClass();
                    data.Recorder = selectedItem;
                    try
                    {
                        IMAPI_FORMAT2_DATA_MEDIA_STATE currentMediaStatus = data.CurrentMediaStatus;
                        IMAPI_MEDIA_PHYSICAL_TYPE currentPhysicalMediaType = data.CurrentPhysicalMediaType;
                        if ((currentMediaStatus & IMAPI_FORMAT2_DATA_MEDIA_STATE.IMAPI_FORMAT2_DATA_MEDIA_STATE_FINALIZED) == IMAPI_FORMAT2_DATA_MEDIA_STATE.IMAPI_FORMAT2_DATA_MEDIA_STATE_FINALIZED)
                        {
                            this.labelProgress.Text = "დევს დახურული " + this.FormatDiskType(currentPhysicalMediaType) + " დისკი, ჩაწერა შეუძლებელია!";
                            this.labelProgress.ForeColor = Color.Red;
                        }
                        else if (((currentMediaStatus & IMAPI_FORMAT2_DATA_MEDIA_STATE.IMAPI_FORMAT2_DATA_MEDIA_STATE_BLANK) == IMAPI_FORMAT2_DATA_MEDIA_STATE.IMAPI_FORMAT2_DATA_MEDIA_STATE_BLANK) || data.MediaHeuristicallyBlank)
                        {
                            this.labelProgress.Text = "დევს ცარიელი " + this.FormatDiskType(currentPhysicalMediaType) + " დისკი";
                            this.labelProgress.ForeColor = SystemColors.WindowText;
                            flag = true;
                        }
                        else if (((currentMediaStatus & IMAPI_FORMAT2_DATA_MEDIA_STATE.IMAPI_FORMAT2_DATA_MEDIA_STATE_APPENDABLE) == IMAPI_FORMAT2_DATA_MEDIA_STATE.IMAPI_FORMAT2_DATA_MEDIA_STATE_APPENDABLE) || ((currentMediaStatus & IMAPI_FORMAT2_DATA_MEDIA_STATE.IMAPI_FORMAT2_DATA_MEDIA_STATE_FINAL_SESSION) == IMAPI_FORMAT2_DATA_MEDIA_STATE.IMAPI_FORMAT2_DATA_MEDIA_STATE_FINAL_SESSION))
                        {
                            this.labelProgress.Text = "დევს არაცარიელი " + this.FormatDiskType(currentPhysicalMediaType) + " დისკი, მოხდება ინფორმაციის დამატება!";
                            this.labelProgress.ForeColor = SystemColors.WindowText;
                            flag = true;
                        }
                        if (!data.IsCurrentMediaSupported(selectedItem))
                        {
                            this.labelProgress.Text = "ამ ჩამწერზე ჩაწერა შეუძლებელია! აირჩიეთ სხვა ჩამწერი!";
                            this.labelProgress.ForeColor = Color.Red;
                            flag = false;
                        }
                        else
                        {
                            MsftFileSystemImage image = (MsftFileSystemImage)new MsftFileSystemImageClass();
                            image.ChooseImageDefaultsForMediaType(currentPhysicalMediaType);
                            long fileSize = data.TotalSectorsOnMedia * this.SectorSize;
                            str2 = this.FormatDiskType(currentPhysicalMediaType);
                            str3 = Utils.FormatFileSize(fileSize);
                            if (!data.MediaHeuristicallyBlank)
                            {
                                foreach (object obj2 in data.MultisessionInterfaces)
                                {
                                    IMultisessionSequential sequential = obj2 as IMultisessionSequential;
                                    if (!((sequential == null) || sequential.IsSupportedOnCurrentMediaState))
                                    {
                                    }
                                }
                                image.MultisessionInterfaces = data.MultisessionInterfaces;
                                if ((image.IdentifyFileSystemsOnDisc(selectedItem) & (FsiFileSystems.FsiFileSystemUnknown | FsiFileSystems.FsiFileSystemUDF)) == FsiFileSystems.FsiFileSystemNone)
                                {
                                    image.ImportFileSystem();
                                }
                                fileSize = data.FreeSectorsOnMedia * this.SectorSize;
                                str4 = Utils.FormatFileSize(fileSize);
                            }
                            else
                            {
                                str4 = str3;
                            }
                            if (this.SizeRequired > fileSize)
                            {
                                this.labelRequiredSize.ForeColor = this.labelDiskFreeSize.ForeColor = Color.Red;
                                flag = false;
                            }
                            else
                            {
                                flag = true;
                            }
                        }
                        this.comboBoxWriteSpeed.BeginUpdate();
                        try
                        {
                            this.comboBoxWriteSpeed.Items.Clear();
                            if (flag)
                            {
                                object[] supportedWriteSpeedDescriptors = data.SupportedWriteSpeedDescriptors;
                                int currentWriteSpeed = data.CurrentWriteSpeed;
                                if (supportedWriteSpeedDescriptors.Length > 0)
                                {
                                    this.comboBoxWriteSpeed.Items.AddRange(supportedWriteSpeedDescriptors);
                                    int num3 = 0;
                                    foreach (IWriteSpeedDescriptor descriptor in supportedWriteSpeedDescriptors)
                                    {
                                        if (descriptor.WriteSpeed == currentWriteSpeed)
                                        {
                                            this.comboBoxWriteSpeed.SelectedIndex = num3;
                                        }
                                    }
                                    if (this.comboBoxWriteSpeed.SelectedIndex == -1)
                                    {
                                        this.comboBoxWriteSpeed.SelectedIndex = 0;
                                    }
                                }
                                else
                                {
                                    this.labelProgress.Text = "ვერ მოხერხდა დისკის ჩაწერის სიჩქარის ამოცნობა!, ჩაწერა შეუძლებელია";
                                    this.labelProgress.ForeColor = Color.Red;
                                    flag = false;
                                }
                            }
                            else
                            {
                                flag = false;
                            }
                            this.comboBoxWriteSpeed.Visible = flag;
                        }
                        finally
                        {
                            this.comboBoxWriteSpeed.EndUpdate();
                        }
                    }
                    catch (COMException exception)
                    {
                        flag = false;
                        if (exception.ErrorCode == -1062600190)
                        {
                            this.labelProgress.Text = "დისკი არ დევს";
                            this.labelProgress.ForeColor = Color.Red;
                        }
                        else
                        {
                            this.labelProgress.Text = "IMAPI2 შეცდომა '" + ((ErrorCodes)exception.ErrorCode).ToString() + "': " + exception.Message;
                            this.labelProgress.ForeColor = Color.Red;
                        }
                    }
                }
                else
                {
                    this.labelProgress.Text = "აირჩიეთ ჩამწერი!";
                    this.labelProgress.ForeColor = Color.Red;
                }
                this.btnBurn.Enabled = flag;
                this.labelDiskType.Text = str2;
                this.labelDiskSize.Text = str3;
                this.labelDiskFreeSize.Text = str4;
                this.labelRequiredSize.Text = Utils.FormatFileSize(this.SizeRequired);
            }
            else
            {
                this.labelWriteSpeed.Text = this.FormatWriteSpeed(this.burningInfo.CurrentWriteSpeed);
                this.labelElapsedTime.Text = DateTime.MinValue.AddSeconds((double)this.burningInfo.ElapsedTime).ToLongTimeString();
                this.labeRemainingTime.Text = DateTime.MinValue.AddSeconds((double)this.burningInfo.RemainingTime).ToLongTimeString();
                this.labelBytesWritten.Text = Utils.FormatFileSize(this.burningInfo.SectorCount * this.SectorSize);
                if (this.burningInfo.TotalSystemBuffer != 0)
                {
                    this.labelUsedBufferSize.Text = (((this.burningInfo.UsedSystemBuffer * 100) / this.burningInfo.TotalSystemBuffer)).ToString() + "%";
                }
                else
                {
                    this.labelUsedBufferSize.Text = "-";
                }
            }
            this.labelElapsedTime.Visible = this.labelElapsedTimeDescrip.Visible = this.burning;
            this.labeRemainingTime.Visible = this.labeRemainingTimeDescrip.Visible = this.burning;
            this.labelBytesWritten.Visible = this.labelBytesWrittenDescrip.Visible = this.burning;
            this.labelUsedBufferSize.Visible = this.labelUsedBufferSizeDescrip.Visible = this.burning;
            this.comboBoxWriteSpeed.Visible = !this.burning && flag;
            this.labelWriteSpeed.Visible = this.burning;
            if (!(this.burning || flag))
            {
                this.comboBoxWriteSpeed.Items.Clear();
                this.labelWriteSpeed.Text = str;
                this.labelWriteSpeed.Visible = true;
            }
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (this.diskDetector != null)
            {
                this.diskDetector.WndProc(ref m);
            }
        }

        private void WriteDiscDataUpdate([In, MarshalAs(UnmanagedType.IDispatch)] object sender, [In, MarshalAs(UnmanagedType.IDispatch)] object progress)
        {
            if (this.backgroundWorker.CancellationPending)
            {
                ((IDiscFormat2Data)sender).CancelWrite();
            }
            else
            {
                IDiscFormat2DataEventArgs args = (IDiscFormat2DataEventArgs)progress;
                this.burningInfo.BurnStage = BurnStage.WritingToTheDisc;
                this.burningInfo.ElapsedTime = args.ElapsedTime;
                this.burningInfo.RemainingTime = args.RemainingTime;
                this.burningInfo.TotalTime = args.TotalTime;
                this.burningInfo.CurrentAction = args.CurrentAction;
                this.burningInfo.StartLba = args.StartLba;
                this.burningInfo.SectorCount = args.SectorCount;
                this.burningInfo.LastReadLba = args.LastReadLba;
                this.burningInfo.LastWrittenLba = args.LastWrittenLba;
                this.burningInfo.TotalSystemBuffer = args.TotalSystemBuffer;
                this.burningInfo.UsedSystemBuffer = args.UsedSystemBuffer;
                this.burningInfo.FreeSystemBuffer = args.FreeSystemBuffer;
                this.backgroundWorker.ReportProgress(0, this.burningInfo);
            }
        }

        public Dictionary<FileInfo, string> FilesToWrite
        {
            get
            {
                if (this.filesToWrite == null)
                {
                    this.filesToWrite = new Dictionary<FileInfo, string>();
                    if ((this.cbDevices.SelectedIndex > -1) && (this.cbTrialDate.SelectedIndex > -1))
                    {
                        string str = ((MsftDiscRecorder2)this.cbDevices.SelectedItem).VolumePathNames[0].ToString();
                        CaseDocument caseDocument = MainForm.CaseDocument;
                        this.filesToWrite.Add(new FileInfo(caseDocument.GetFileName()), Path.GetFileName(caseDocument.GetFileName()));
                        string directoryName = Path.GetDirectoryName(caseDocument.GetFileName());
                        bool flag = false;
                        if (!string.IsNullOrEmpty(directoryName))
                        {
                            if (!directoryName.EndsWith(@"\"))
                            {
                                directoryName = directoryName + @"\";
                            }
                            if (!((str == null) || str.EndsWith(@"\")))
                            {
                                str = str + @"\";
                            }
                            foreach (Event event2 in caseDocument.Events)
                            {
                                if (((this.cbTrialDate.SelectedIndex == 0) || (event2.Time.Date == ((DateSigned)this.cbTrialDate.SelectedItem).Date)) && (event2.RecordSet != null))
                                {
                                    string[] strArray = event2.RecordSet.FileName.Split(new char[] { ':' });
                                    foreach (string str3 in strArray)
                                    {
                                        string str4 = str3.Substring(str3.IndexOf('?') + 1);
                                        try
                                        {
                                            if (!(flag || File.Exists(str + caseDocument.TrialNo + @"\" + str4)))
                                            {
                                                this.filesToWrite.Add(new FileInfo(directoryName + caseDocument.TrialNo + @"\" + str4), caseDocument.TrialNo + @"\" + str4);
                                            }
                                        }
                                        catch
                                        {
                                            flag = true;
                                            this.filesToWrite.Add(new FileInfo(directoryName + caseDocument.TrialNo + str4), caseDocument.TrialNo + @"\" + str4);
                                        }
                                    }
                                }
                            }
                        }
                        if (!(!flag && File.Exists(str + "Court Process Recorder.exe")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "Court Process Recorder.exe"), "Court Process Recorder.exe");
                        }
                        if (!(!flag && File.Exists(str + "Bass.Net.dll")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "Bass.Net.dll"), "Bass.Net.dll");
                        }
                        if (!(!flag && File.Exists(str + "bass.dll")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "bass.dll"), "bass.dll");
                        }
                        if (!(!flag && File.Exists(str + "bass_fx.dll")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "bass_fx.dll"), "bass_fx.dll");
                        }
                        if (!(!flag && File.Exists(str + "bassenc.dll")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "bassenc.dll"), "bassenc.dll");
                        }
                        if (!(!flag && File.Exists(str + "bassmix.dll")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "bassmix.dll"), "bassmix.dll");
                        }
                        if (!(!flag && File.Exists(str + "CourtRecorderCommon.dll")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "CourtRecorderCommon.dll"), "CourtRecorderCommon.dll");
                        }
                        if (!(!flag && File.Exists(str + "Apolo16Mixer.dll")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "Apolo16Mixer.dll"), "Apolo16Mixer.dll");
                        }
                        if (!(!flag && File.Exists(str + "CyUSB.dll")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + "CyUSB.dll"), "CyUSB.dll");
                        }
                        if (!(!flag && File.Exists(str + "autorun.exe")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + @"redist\launcher.exe"), "autorun.exe");
                        }
                        if (!(!flag && File.Exists(str + "autorun.inf")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + @"redist\autorun" + (this.cbStoreInstallationFiles.Checked ? "_user" : "")), "autorun.inf");
                        }
                        if (this.cbStoreInstallationFiles.Checked && !(!flag && File.Exists(str + @"redist\net_fx2.exe")))
                        {
                            this.filesToWrite.Add(new FileInfo(AudioIO.ApplicationPath + @"Redist\net_fx2.exe"), @"redist\net_fx2.exe");
                        }
                    }
                }
                return this.filesToWrite;
            }
        }

        private long SizeRequired
        {
            get
            {
                long num = 0L;
                foreach (KeyValuePair<FileInfo, string> pair in this.FilesToWrite)
                {
                    num += ((pair.Key.Length / this.SectorSize) + 1L) * this.SectorSize;
                }
                return num;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct DateSigned
        {
            public DateTime Date;
            public bool Signed;
            public DateSigned(DateTime date, bool signed)
            {
                this.Date = date;
                this.Signed = signed;
            }

            public override string ToString()
            {
                return (this.Date.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern) + (this.Signed ? "" : " (არ არის ხელმოწერილი)"));
            }
        }
    }
}

