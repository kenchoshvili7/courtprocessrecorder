﻿namespace CourtProcessRecorder.Controls
{
    using System;

    public class DriveDetectorEventArgs : EventArgs
    {
        public bool Cancel = false;
        public string Drive = "";
        public bool HookQueryRemove = false;
    }
}

