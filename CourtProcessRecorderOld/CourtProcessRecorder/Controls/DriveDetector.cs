﻿namespace CourtProcessRecorder.Controls
{
    using Microsoft.Win32.SafeHandles;
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;

    internal class DriveDetector : IDisposable
    {
        private const int BROADCAST_QUERY_DENY = 0x424d5144;
        private const int DBT_DEVICEARRIVAL = 0x8000;
        private const int DBT_DEVICEQUERYREMOVE = 0x8001;
        private const int DBT_DEVICEREMOVECOMPLETE = 0x8004;
        private const int DBT_DEVTYP_DEVICEINTERFACE = 5;
        private const int DBT_DEVTYP_HANDLE = 6;
        private const int DBT_DEVTYP_VOLUME = 2;
        private string mCurrentDrive;
        private IntPtr mDeviceNotifyHandle;
        private IntPtr mDirHandle = IntPtr.Zero;
        private string mFileToOpen;
        private IntPtr mRecipientHandle;
        private const int WM_DEVICECHANGE = 0x219;

        public event DriveDetectorEventHandler DeviceArrived;

        public event DriveDetectorEventHandler DeviceRemoved;

        public event DriveDetectorEventHandler QueryRemove;

        public DriveDetector(Control control)
        {
            this.Init(control, null);
        }

        public void DisableQueryRemove()
        {
            if (this.mDeviceNotifyHandle != IntPtr.Zero)
            {
                this.RegisterForDeviceChange(false, null);
            }
        }

        public void Dispose()
        {
            this.RegisterForDeviceChange(false, null);
        }

        private static char DriveMaskToLetter(int mask)
        {
            string str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int num = 0;
            int num2 = mask / 2;
            while (num2 != 0)
            {
                num2 /= 2;
                num++;
            }
            if (num < str.Length)
            {
                return str[num];
            }
            return '?';
        }

        public bool EnableQueryRemove(string fileOnDrive)
        {
            if ((fileOnDrive == null) || (fileOnDrive.Length == 0))
            {
                throw new ArgumentException("Drive path must be supplied to register for Query remove.");
            }
            if ((fileOnDrive.Length == 2) && (fileOnDrive[1] == ':'))
            {
                fileOnDrive = fileOnDrive + '\\';
            }
            if (this.mDeviceNotifyHandle != IntPtr.Zero)
            {
                this.RegisterForDeviceChange(false, null);
            }
            if (!((Path.GetFileName(fileOnDrive).Length != 0) && File.Exists(fileOnDrive)))
            {
                this.mFileToOpen = null;
            }
            else
            {
                this.mFileToOpen = fileOnDrive;
            }
            this.RegisterQuery(Path.GetPathRoot(fileOnDrive));
            if (this.mDeviceNotifyHandle == IntPtr.Zero)
            {
                return false;
            }
            return true;
        }

        private void Init(Control control, string fileToOpen)
        {
            this.mFileToOpen = fileToOpen;
            this.mDeviceNotifyHandle = IntPtr.Zero;
            this.mRecipientHandle = control.Handle;
            this.mDirHandle = IntPtr.Zero;
            this.mCurrentDrive = "";
        }

        private void RegisterForDeviceChange(string dirPath)
        {
            IntPtr ptr = Native.OpenDirectory(dirPath);
            if (ptr == IntPtr.Zero)
            {
                this.mDeviceNotifyHandle = IntPtr.Zero;
            }
            else
            {
                this.mDirHandle = ptr;
                DEV_BROADCAST_HANDLE structure = new DEV_BROADCAST_HANDLE();
                structure.dbch_devicetype = 6;
                structure.dbch_reserved = 0;
                structure.dbch_nameoffset = 0L;
                structure.dbch_handle = ptr;
                structure.dbch_hdevnotify = IntPtr.Zero;
                int cb = Marshal.SizeOf(structure);
                structure.dbch_size = cb;
                IntPtr ptr2 = Marshal.AllocHGlobal(cb);
                Marshal.StructureToPtr(structure, ptr2, true);
                this.mDeviceNotifyHandle = Native.RegisterDeviceNotification(this.mRecipientHandle, ptr2, 0);
            }
        }

        private void RegisterForDeviceChange(bool register, SafeFileHandle fileHandle)
        {
            if (register)
            {
                DEV_BROADCAST_HANDLE structure = new DEV_BROADCAST_HANDLE();
                structure.dbch_devicetype = 6;
                structure.dbch_reserved = 0;
                structure.dbch_nameoffset = 0L;
                structure.dbch_handle = fileHandle.DangerousGetHandle();
                structure.dbch_hdevnotify = IntPtr.Zero;
                int cb = Marshal.SizeOf(structure);
                structure.dbch_size = cb;
                IntPtr ptr = Marshal.AllocHGlobal(cb);
                Marshal.StructureToPtr(structure, ptr, true);
                this.mDeviceNotifyHandle = Native.RegisterDeviceNotification(this.mRecipientHandle, ptr, 0);
            }
            else
            {
                if (this.mDirHandle != IntPtr.Zero)
                {
                    Native.CloseDirectoryHandle(this.mDirHandle);
                }
                if (this.mDeviceNotifyHandle != IntPtr.Zero)
                {
                    Native.UnregisterDeviceNotification(this.mDeviceNotifyHandle);
                }
                this.mDeviceNotifyHandle = IntPtr.Zero;
                this.mDirHandle = IntPtr.Zero;
                this.mCurrentDrive = "";
            }
        }

        private void RegisterQuery(string drive)
        {
            bool flag = true;
            if (this.mFileToOpen != null)
            {
                if (this.mFileToOpen.Contains(":"))
                {
                    string str = this.mFileToOpen.Substring(3);
                    string pathRoot = Path.GetPathRoot(drive);
                    this.mFileToOpen = Path.Combine(pathRoot, str);
                }
                else
                {
                    this.mFileToOpen = Path.Combine(drive, this.mFileToOpen);
                }
            }
            if (flag)
            {
                this.RegisterForDeviceChange(drive);
                this.mCurrentDrive = drive;
            }
        }

        public void WndProc(ref Message m)
        {
            if (m.Msg == 0x219)
            {
                char ch;
                DEV_BROADCAST_VOLUME dev_broadcast_volume;
                DriveDetectorEventArgs args;
                switch (m.WParam.ToInt32())
                {
                    case 0x8000:
                        if (Marshal.ReadInt32(m.LParam, 4) == 2)
                        {
                            dev_broadcast_volume = (DEV_BROADCAST_VOLUME) Marshal.PtrToStructure(m.LParam, typeof(DEV_BROADCAST_VOLUME));
                            ch = DriveMaskToLetter(dev_broadcast_volume.dbcv_unitmask);
                            DriveDetectorEventHandler deviceArrived = this.DeviceArrived;
                            if (deviceArrived != null)
                            {
                                args = new DriveDetectorEventArgs();
                                args.Drive = ch + @":\";
                                deviceArrived(this, args);
                                if (args.HookQueryRemove)
                                {
                                    if (this.mDeviceNotifyHandle != IntPtr.Zero)
                                    {
                                        this.RegisterForDeviceChange(false, null);
                                    }
                                    this.RegisterQuery(ch + @":\");
                                }
                            }
                        }
                        break;

                    case 0x8001:
                        if (Marshal.ReadInt32(m.LParam, 4) == 6)
                        {
                            DriveDetectorEventHandler queryRemove = this.QueryRemove;
                            if (queryRemove != null)
                            {
                                args = new DriveDetectorEventArgs();
                                args.Drive = this.mCurrentDrive;
                                queryRemove(this, args);
                                if (!args.Cancel)
                                {
                                    this.RegisterForDeviceChange(false, null);
                                    break;
                                }
                                m.Result = (IntPtr) 0x424d5144;
                            }
                        }
                        break;

                    case 0x8004:
                        if ((Marshal.ReadInt32(m.LParam, 4) == 2) && (Marshal.ReadInt32(m.LParam, 4) == 2))
                        {
                            dev_broadcast_volume = (DEV_BROADCAST_VOLUME) Marshal.PtrToStructure(m.LParam, typeof(DEV_BROADCAST_VOLUME));
                            ch = DriveMaskToLetter(dev_broadcast_volume.dbcv_unitmask);
                            DriveDetectorEventHandler deviceRemoved = this.DeviceRemoved;
                            if (deviceRemoved != null)
                            {
                                args = new DriveDetectorEventArgs();
                                args.Drive = ch + @":\";
                                deviceRemoved(this, args);
                            }
                        }
                        break;
                }
            }
        }

        public string HookedDrive
        {
            get
            {
                return this.mCurrentDrive;
            }
        }

        public bool IsQueryHooked
        {
            get
            {
                if (this.mDeviceNotifyHandle == IntPtr.Zero)
                {
                    return false;
                }
                return true;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DEV_BROADCAST_HANDLE
        {
            public int dbch_size;
            public int dbch_devicetype;
            public int dbch_reserved;
            public IntPtr dbch_handle;
            public IntPtr dbch_hdevnotify;
            public Guid dbch_eventguid;
            public long dbch_nameoffset;
            public byte dbch_data;
            public byte dbch_data1;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DEV_BROADCAST_VOLUME
        {
            public int dbcv_size;
            public int dbcv_devicetype;
            public int dbcv_reserved;
            public int dbcv_unitmask;
        }

        private class Native
        {
            private const uint FILE_ATTRIBUTE_NORMAL = 0x80;
            private const uint FILE_FLAG_BACKUP_SEMANTICS = 0x2000000;
            private const uint FILE_SHARE_READ = 1;
            private const uint FILE_SHARE_WRITE = 2;
            private const uint GENERIC_READ = 0x80000000;
            private static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);
            private const uint OPEN_EXISTING = 3;

            public static bool CloseDirectoryHandle(IntPtr handle)
            {
                return CloseHandle(handle);
            }

            [DllImport("kernel32", SetLastError=true)]
            private static extern bool CloseHandle(IntPtr hObject);
            [DllImport("kernel32", SetLastError=true)]
            private static extern IntPtr CreateFile(string FileName, uint DesiredAccess, uint ShareMode, uint SecurityAttributes, uint CreationDisposition, uint FlagsAndAttributes, int hTemplateFile);
            public static IntPtr OpenDirectory(string dirPath)
            {
                IntPtr ptr = CreateFile(dirPath, 0x80000000, 3, 0, 3, 0x2000080, 0);
                if (ptr == INVALID_HANDLE_VALUE)
                {
                    return IntPtr.Zero;
                }
                return ptr;
            }

            [DllImport("user32.dll", CharSet=CharSet.Auto)]
            public static extern IntPtr RegisterDeviceNotification(IntPtr hRecipient, IntPtr NotificationFilter, uint Flags);
            [DllImport("user32.dll", CharSet=CharSet.Auto)]
            public static extern uint UnregisterDeviceNotification(IntPtr hHandle);
        }
    }
}

