﻿namespace CourtProcessRecorder.Controls
{
    using System;

    public class CtrlClickEventArgs : EventArgs
    {
        private System.DateTime dateTime;

        public CtrlClickEventArgs(System.DateTime dateTime)
        {
            this.dateTime = dateTime;
        }

        public System.DateTime DateTime
        {
            get
            {
                return this.dateTime;
            }
        }
    }
}

