﻿using CourtRecorderCommon;

namespace CourtProcessRecorder.Controls
{
    using AudioConfiguration;
    using CourtProcessRecorder;
    using CourtProcessRecorder.DataTypes;
    using DeviceController;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;
    using Un4seen.Bass;
    using Un4seen.Bass.Misc;

    public class PlayBackControl : UserControl
    {
        private Image cachedImage;
        private CheckBox cbEnableEQ;
        private ComboBox cbPresets;
        private IContainer components = null;
        private DateTime currentDate;
        private DateTime currentTime;
        private DateTime? endTime;
        private int fileCount;
        private int filesProcessed;
        private DateTime frameEndTime;
        private bool frameShouldUpdated;
        private DateTime frameStartTime;
        private DateTime lastTickTime;
        private bool lockSeek;
        private ContextMenuStrip mnuEQ;
        private ToolStripMenuItem mnuiEq;
        private ToolStripMenuItem mnuiPitch;
        private Panel pBlockTracker;
        private Panel pEqualizer;
        private Event playingEvent;
        public CourtProcessRecorder.Controls.PlayState PlayState;
        private bool positionChanged;
        private string processedFileName;
        private Panel pWaveForm;
        private Dictionary<Event, RecordSet> recordSet = new Dictionary<Event, RecordSet>();
        private bool renderingInProgress;
        private DateTime? startTime;
        private TrackBar tbEQ0;
        private TrackBar tbEQ1;
        private TrackBar tbEQ2;
        private TrackBar tbEQ3;
        private TrackBar tbEQ4;
        private TrackBar tbPitch;
        private TrackBar tbPosition;
        private System.Windows.Forms.Timer timer;
        public System.Windows.Forms.Timer timerPositionUpdater;
        private List<Channel> usedChannels = new List<Channel>();

        public event CtrlClickEventHandler CtrlClickEvent;

        public event EventHandler OnStopped;

        public event EventHandler UpdateTime;

        public PlayBackControl()
        {
            this.InitializeComponent();
            this.PlayState = CourtProcessRecorder.Controls.PlayState.Idle;
            this.frameShouldUpdated = true;
            this.cbPresets.SelectedIndex = 3;
            this.ShowEqualizer(false);
            this.ShowPitchControl(false);
        }

        public void AbortRendering()
        {
            CaseDocument caseDocument = MainForm.CaseDocument;
            if (caseDocument != null)
            {
                foreach (Event event2 in caseDocument.Events)
                {
                    if ((((byte)(event2.EventType & EventType.RecordStart)) == 1) && (event2.RecordSet.UserData != null))
                    {
                        foreach (WaveForm form in event2.RecordSet.UserData)
                        {
                            if ((form != null) && form.IsRenderingInProgress)
                            {
                                form.RenderStop();
                            }
                        }
                        event2.RecordSet.UserData.Clear();
                        event2.RecordSet.UserData = null;
                    }
                }
            }
        }

        private void BuildPreview()
        {
            this.startTime = null;
            this.endTime = null;
            this.FrameStartTime = this.StartTime;
            this.FrameEndTime = this.EndTime;
            this.currentTime = this.FrameStartTime;
            this.tbPosition.Minimum = 0;
            this.tbPosition.Maximum = this.FrameLengthInMS;
        }

        private void cbEnableEQCheckStateChanged(object sender, EventArgs e)
        {
            AudioIO.SetPlaybackEQValues(0, this.cbEnableEQ.Checked ? this.tbEQ0.Value : 0);
            AudioIO.SetPlaybackEQValues(1, this.cbEnableEQ.Checked ? this.tbEQ1.Value : 0);
            AudioIO.SetPlaybackEQValues(2, this.cbEnableEQ.Checked ? this.tbEQ2.Value : 0);
            AudioIO.SetPlaybackEQValues(3, this.cbEnableEQ.Checked ? this.tbEQ3.Value : 0);
            AudioIO.SetPlaybackEQValues(4, this.cbEnableEQ.Checked ? this.tbEQ4.Value : 0);
        }

        private void cbPresetsSelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.cbPresets.SelectedIndex)
            {
                case 0:
                    this.tbEQ0.Value = -150;
                    this.tbEQ1.Value = -130;
                    this.tbEQ2.Value = 150;
                    this.tbEQ3.Value = 140;
                    this.tbEQ4.Value = 110;
                    break;

                case 1:
                    this.tbEQ0.Value = 150;
                    this.tbEQ1.Value = -130;
                    this.tbEQ2.Value = 150;
                    this.tbEQ3.Value = 140;
                    this.tbEQ4.Value = 110;
                    break;

                case 2:
                    this.tbEQ0.Value = 150;
                    this.tbEQ1.Value = 150;
                    this.tbEQ2.Value = 150;
                    this.tbEQ3.Value = 150;
                    this.tbEQ4.Value = 150;
                    break;

                case 3:
                    this.tbEQ0.Value = 0;
                    this.tbEQ1.Value = 0;
                    this.tbEQ2.Value = 0;
                    this.tbEQ3.Value = 0;
                    this.tbEQ4.Value = 0;
                    break;
            }
            if (this.cbEnableEQ.Checked)
            {
                this.cbEnableEQCheckStateChanged(null, null);
            }
        }

        private WaveForm CreateWaveForm(string fileName)
        {
            if (!(base.Handle != IntPtr.Zero))
            {
                return null;
            }
            Job job = null;
            WaveForm wf = new WaveForm(fileName, new WAVEFORMPROC(this.WaveFormCallback), this);
            wf.DrawWaveForm = WaveForm.WAVEFORMDRAWTYPE.Mono;
            wf.DrawMarker = WaveForm.MARKERDRAWTYPE.NamePositionAlternate;
            wf.DrawEnvelope = false;
            wf.DrawVolume = WaveForm.VOLUMEDRAWTYPE.Solid;
            wf.FrameResolution = 0.001f;
            wf.ColorBackground = Color.FromArgb(230, 0xff, 230);
            wf.ColorLeft = Color.FromArgb(0xff, 0, 0xff, 0);
            wf.ColorLeft2 = Color.FromArgb(0xff, 0, 0, 0);
            wf.ColorBeat = Color.LightSkyBlue;
            wf.ColorVolume = Color.Black;
            bool background = true;
            switch (Kernel32Methods.GetDriveType(Directory.GetDirectoryRoot(fileName)))
            {
                case 1:
                case 2:
                case 5:
                case 6:
                    background = false;
                    break;

                default:
                    background = false;
                    break;
            }
            if (background)
            {
                this.processedFileName = null;
                wf.RenderStart(background, BASSFlag.BASS_FX_BPM_MULT2);
            }
            else
            {
                this.processedFileName = fileName;
                WaitForProcessForm.StatusText1 = string.Format("ფაილის გახსნა ({0}/{1}) 0.00%", this.filesProcessed, this.fileCount);
                WaitForProcessForm.StatusText2 = this.processedFileName;
                if (job == null)
                {
                    job = delegate
                    {
                        return wf.RenderStart(background, BASSFlag.BASS_FX_BPM_MULT2);
                    };
                }
                WaitForProcessForm.WaitFor(job);
            }
            return wf;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void DrawTimelineMarkups(Graphics graphics, float x, bool upDown)
        {
            PointF[] tfArray;
            float num = 2.5f;
            if (upDown)
            {
                tfArray = new PointF[] { new PointF(x - num, 0f), new PointF(x + num, 0f), new PointF(x, num * 2f), new PointF(x - num, 0f) };
                graphics.DrawPolygon(Pens.Black, tfArray);
            }
            else
            {
                tfArray = new PointF[] { new PointF(x - num, num * 2f), new PointF(x + num, num * 2f), new PointF(x, 0f), new PointF(x - num, num * 2f) };
                graphics.DrawPolygon(Pens.Black, tfArray);
            }
        }

        private int GetChannelIndex(AudioSourceChannels channel)
        {
            int num = 0;
            AudioControl parent = base.Parent as AudioControl;
            if ((parent != null) && parent.Controls.ContainsKey("mixerControl"))
            {
                MixerControl control2 = (MixerControl)parent.Controls["mixerControl"];
                foreach (Control control3 in control2.Controls)
                {
                    if (control3 is VolumeControl)
                    {
                        if (((AudioSourceChannels)((VolumeControl)control3).Tag) == channel)
                        {
                            return num;
                        }
                        num++;
                    }
                }
            }
            return num;
        }

        private long GetPositionFromWaveForm(out RecordSet recordSets, out Event e)
        {
            if (!(this.HasData() && !this.renderingInProgress))
            {
                recordSets = null;
                e = null;
                return -1L;
            }
            foreach (KeyValuePair<Event, RecordSet> pair in this.recordSet)
            {
                foreach (RecordSetData data in pair.Value.RecordSetData)
                {
                    if ((pair.Value.UserData != null) && (pair.Value.UserData.Count > 0))
                    {
                        WaveForm form = pair.Value.UserData[0];
                        if ((form != null) && (form.Wave != null))
                        {
                            DateTime time = pair.Key.Time.AddMilliseconds((double)((data.Position / ((long)form.Wave.bpf)) * form.Wave.chans));
                            DateTime time2 = time.AddMilliseconds((double)((data.Length / ((long)form.Wave.bpf)) * form.Wave.chans));
                            if (((time >= this.currentTime) || (time2 > this.currentTime)) && ((time <= this.FrameEndTime) || (time2 <= this.FrameEndTime)))
                            {
                                recordSets = pair.Value;
                                e = pair.Key;
                                TimeSpan span = (TimeSpan)(this.currentTime - pair.Key.Time);
                                return (long)(((int)span.TotalMilliseconds) * form.Wave.bpf);
                            }
                        }
                    }
                }
            }
            recordSets = null;
            e = null;
            return -1L;
        }

        private DateTime GetTimeFromWaveForm(int point)
        {
            DateTime frameStartTime = this.FrameStartTime;
            if (!(this.HasData() && !this.renderingInProgress))
            {
                return frameStartTime;
            }
            TimeSpan span = (TimeSpan)(this.FrameEndTime - this.FrameStartTime);
            return frameStartTime.AddMilliseconds((span.TotalMilliseconds * point) / ((double)this.pWaveForm.Width));
        }

        public bool HasData()
        {
            if (MainForm.CaseDocument != null)
            {
                foreach (Event event2 in MainForm.CaseDocument.Events)
                {
                    if ((event2.Time.Date == this.CurrentDate) && (event2.RecordSet != null))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            this.tbPosition = new TrackBar();
            this.pBlockTracker = new Panel();
            this.pWaveForm = new Panel();
            this.mnuEQ = new ContextMenuStrip(this.components);
            this.mnuiEq = new ToolStripMenuItem();
            this.mnuiPitch = new ToolStripMenuItem();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.timerPositionUpdater = new System.Windows.Forms.Timer(this.components);
            this.pEqualizer = new Panel();
            this.cbEnableEQ = new CheckBox();
            this.cbPresets = new ComboBox();
            this.tbEQ4 = new TrackBar();
            this.tbEQ3 = new TrackBar();
            this.tbEQ1 = new TrackBar();
            this.tbEQ2 = new TrackBar();
            this.tbEQ0 = new TrackBar();
            this.tbPitch = new TrackBar();
            this.tbPosition.BeginInit();
            this.mnuEQ.SuspendLayout();
            this.pEqualizer.SuspendLayout();
            this.tbEQ4.BeginInit();
            this.tbEQ3.BeginInit();
            this.tbEQ1.BeginInit();
            this.tbEQ2.BeginInit();
            this.tbEQ0.BeginInit();
            this.tbPitch.BeginInit();
            base.SuspendLayout();
            this.tbPosition.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.tbPosition.AutoSize = false;
            this.tbPosition.LargeChange = 50;
            this.tbPosition.Location = new Point(0x48, 0x7c);
            this.tbPosition.Name = "tbPosition";
            this.tbPosition.Size = new Size(0xec, 0x17);
            this.tbPosition.TabIndex = 3;
            this.tbPosition.TickStyle = TickStyle.None;
            this.tbPosition.Scroll += new EventHandler(this.tbPositionScroll);
            this.pBlockTracker.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.pBlockTracker.Location = new Point(0x4e, 0x93);
            this.pBlockTracker.Name = "pBlockTracker";
            this.pBlockTracker.Size = new Size(0xdf, 0x10);
            this.pBlockTracker.TabIndex = 4;
            this.pBlockTracker.Paint += new PaintEventHandler(this.pBlockTrackerPaint);
            this.pWaveForm.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.pWaveForm.ContextMenuStrip = this.mnuEQ;
            this.pWaveForm.Location = new Point(0x4e, 0);
            this.pWaveForm.Name = "pWaveForm";
            this.pWaveForm.Size = new Size(0xde, 0x7f);
            this.pWaveForm.TabIndex = 2;
            this.pWaveForm.Paint += new PaintEventHandler(this.pWaveFormPaint);
            this.pWaveForm.MouseDown += new MouseEventHandler(this.pWaveFormMouseDown);
            this.pWaveForm.MouseHover += new EventHandler(this.pWaveFormMouseHover);
            this.mnuEQ.Font = new Font("Sylfaen", 8.25f);
            this.mnuEQ.Items.AddRange(new ToolStripItem[] { this.mnuiEq, this.mnuiPitch });
            this.mnuEQ.Name = "mnuEQ";
            this.mnuEQ.Size = new Size(0x94, 0x30);
            this.mnuiEq.CheckOnClick = true;
            this.mnuiEq.Name = "mnuiEq";
            this.mnuiEq.Size = new Size(0x93, 0x16);
            this.mnuiEq.Text = "ექვალაიზერი";
            this.mnuiEq.ToolTipText = "ექვალაიზერი";
            this.mnuiEq.Click += new EventHandler(this.mnuiEqClick);
            this.mnuiPitch.CheckOnClick = true;
            this.mnuiPitch.Name = "mnuiPitch";
            this.mnuiPitch.Size = new Size(0x93, 0x16);
            this.mnuiPitch.Text = "სიჩქარე";
            this.mnuiPitch.ToolTipText = "სიჩქარე";
            this.mnuiPitch.Click += new EventHandler(this.mnuiPitchClick);
            this.timer.Interval = 0x3e8;
            this.timer.Tick += new EventHandler(this.timerTick);
            this.timerPositionUpdater.Enabled = true;
            this.timerPositionUpdater.Interval = 500;
            this.timerPositionUpdater.Tick += new EventHandler(this.timerPositionUpdater_Tick);
            this.pEqualizer.Controls.Add(this.cbEnableEQ);
            this.pEqualizer.Controls.Add(this.cbPresets);
            this.pEqualizer.Controls.Add(this.tbEQ4);
            this.pEqualizer.Controls.Add(this.tbEQ3);
            this.pEqualizer.Controls.Add(this.tbEQ1);
            this.pEqualizer.Controls.Add(this.tbEQ2);
            this.pEqualizer.Controls.Add(this.tbEQ0);
            this.pEqualizer.Dock = DockStyle.Left;
            this.pEqualizer.Location = new Point(0x19, 0);
            this.pEqualizer.Name = "pEqualizer";
            this.pEqualizer.Size = new Size(0x36, 0xa3);
            this.pEqualizer.TabIndex = 1;
            this.cbEnableEQ.AutoSize = true;
            this.cbEnableEQ.Location = new Point(0x13, 0x90);
            this.cbEnableEQ.Name = "cbEnableEQ";
            this.cbEnableEQ.Size = new Size(15, 14);
            this.cbEnableEQ.TabIndex = 6;
            this.cbEnableEQ.UseVisualStyleBackColor = true;
            this.cbEnableEQ.CheckStateChanged += new EventHandler(this.cbEnableEQCheckStateChanged);
            this.cbPresets.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbPresets.DropDownWidth = 200;
            this.cbPresets.FormattingEnabled = true;
            this.cbPresets.Items.AddRange(new object[] { "სტანდარტული", "ხმა", "დაბალი სიხშირის ამოღება", "შერჩევითი" });
            this.cbPresets.Location = new Point(3, 0x75);
            this.cbPresets.Name = "cbPresets";
            this.cbPresets.Size = new Size(0x30, 0x15);
            this.cbPresets.TabIndex = 5;
            this.cbPresets.SelectedIndexChanged += new EventHandler(this.cbPresetsSelectedIndexChanged);
            this.tbEQ4.AutoSize = false;
            this.tbEQ4.Location = new Point(-6, 0x5d);
            this.tbEQ4.Maximum = 150;
            this.tbEQ4.Minimum = -150;
            this.tbEQ4.Name = "tbEQ4";
            this.tbEQ4.Size = new Size(0x41, 0x17);
            this.tbEQ4.TabIndex = 4;
            this.tbEQ4.Tag = 4;
            this.tbEQ4.TickStyle = TickStyle.None;
            this.tbEQ4.Scroll += new EventHandler(this.tbEQScroll);
            this.tbEQ3.AutoSize = false;
            this.tbEQ3.Location = new Point(-6, 0x45);
            this.tbEQ3.Maximum = 150;
            this.tbEQ3.Minimum = -150;
            this.tbEQ3.Name = "tbEQ3";
            this.tbEQ3.Size = new Size(0x41, 0x17);
            this.tbEQ3.TabIndex = 3;
            this.tbEQ3.Tag = 3;
            this.tbEQ3.TickStyle = TickStyle.None;
            this.tbEQ3.Scroll += new EventHandler(this.tbEQScroll);
            this.tbEQ1.AutoSize = false;
            this.tbEQ1.Location = new Point(-6, 0x17);
            this.tbEQ1.Maximum = 150;
            this.tbEQ1.Minimum = -150;
            this.tbEQ1.Name = "tbEQ1";
            this.tbEQ1.Size = new Size(0x41, 0x17);
            this.tbEQ1.TabIndex = 1;
            this.tbEQ1.Tag = 1;
            this.tbEQ1.TickStyle = TickStyle.None;
            this.tbEQ1.Scroll += new EventHandler(this.tbEQScroll);
            this.tbEQ2.AutoSize = false;
            this.tbEQ2.Location = new Point(-6, 0x2e);
            this.tbEQ2.Maximum = 150;
            this.tbEQ2.Minimum = -150;
            this.tbEQ2.Name = "tbEQ2";
            this.tbEQ2.Size = new Size(0x41, 0x17);
            this.tbEQ2.TabIndex = 2;
            this.tbEQ2.Tag = 2;
            this.tbEQ2.TickStyle = TickStyle.None;
            this.tbEQ2.Scroll += new EventHandler(this.tbEQScroll);
            this.tbEQ0.AutoSize = false;
            this.tbEQ0.Location = new Point(-6, 0);
            this.tbEQ0.Maximum = 150;
            this.tbEQ0.Minimum = -150;
            this.tbEQ0.Name = "tbEQ0";
            this.tbEQ0.Size = new Size(0x41, 0x17);
            this.tbEQ0.TabIndex = 0;
            this.tbEQ0.Tag = 0;
            this.tbEQ0.TickStyle = TickStyle.None;
            this.tbEQ0.Scroll += new EventHandler(this.tbEQScroll);
            this.tbPitch.AutoSize = false;
            this.tbPitch.Dock = DockStyle.Left;
            this.tbPitch.Location = new Point(0, 0);
            this.tbPitch.Maximum = 50;
            this.tbPitch.Minimum = -50;
            this.tbPitch.Name = "tbPitch";
            this.tbPitch.Orientation = Orientation.Vertical;
            this.tbPitch.Size = new Size(0x19, 0xa3);
            this.tbPitch.TabIndex = 0;
            this.tbPitch.Tag = 0;
            this.tbPitch.TickStyle = TickStyle.None;
            this.tbPitch.Scroll += new EventHandler(this.tbPitchScroll);
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.Controls.Add(this.pWaveForm);
            base.Controls.Add(this.pBlockTracker);
            base.Controls.Add(this.pEqualizer);
            base.Controls.Add(this.tbPitch);
            base.Controls.Add(this.tbPosition);
            this.DoubleBuffered = true;
            base.Name = "PlayBackControl";
            base.Size = new Size(0x12d, 0xa3);
            this.tbPosition.EndInit();
            this.mnuEQ.ResumeLayout(false);
            this.pEqualizer.ResumeLayout(false);
            this.pEqualizer.PerformLayout();
            this.tbEQ4.EndInit();
            this.tbEQ3.EndInit();
            this.tbEQ1.EndInit();
            this.tbEQ2.EndInit();
            this.tbEQ0.EndInit();
            this.tbPitch.EndInit();
            base.ResumeLayout(false);
        }

        private void mnuiEqClick(object sender, EventArgs e)
        {
            this.ShowEqualizer(this.mnuiEq.Checked);
        }

        private void mnuiPitchClick(object sender, EventArgs e)
        {
            this.ShowPitchControl(this.mnuiPitch.Checked);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
            if (this.HasData() && !this.renderingInProgress)
            {
                if ((e.Delta > 0) && (this.FrameLengthInMS > 0xbb8))
                {
                    this.FrameEndTime = this.FrameStartTime.AddMilliseconds((double)Math.Max(0xbb8, this.FrameLengthInMS / 2));
                    this.frameShouldUpdated = true;
                }
                if ((e.Delta < 0) && ((this.FrameEndTime - this.FrameStartTime) < (this.EndTime - this.StartTime)))
                {
                    this.FrameEndTime = this.FrameStartTime.AddMilliseconds((double)Math.Max(0xbb8, this.FrameLengthInMS * 2));
                    this.frameShouldUpdated = true;
                }
                if (this.FrameEndTime > this.EndTime)
                {
                    this.FrameEndTime = this.EndTime;
                    this.frameShouldUpdated = true;
                }
                if (this.FrameStartTime < this.StartTime)
                {
                    this.FrameStartTime = this.StartTime;
                    this.frameShouldUpdated = true;
                }
                this.WaveFormRepaint();
                this.pBlockTracker.Invalidate();
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.frameShouldUpdated = true;
            if (this.HasData() && !this.renderingInProgress)
            {
                this.WaveFormRepaint();
                this.pBlockTracker.Invalidate();
            }
        }

        public void Pause()
        {
            this.PlayState = CourtProcessRecorder.Controls.PlayState.Paused;
            this.timer.Enabled = false;
            AudioIO.PausePlayback();
            this.positionChanged = false;
        }

        private void pBlockTrackerPaint(object sender, PaintEventArgs e)
        {
            if ((this.HasData() && (this.EndTime != this.StartTime)) && !this.renderingInProgress)
            {
                TimeSpan span = (TimeSpan)(this.FrameStartTime - this.StartTime);
                span = (TimeSpan)(this.EndTime - this.StartTime);
                float x = (float)(this.pBlockTracker.Width * (span.TotalMilliseconds / span.TotalMilliseconds));
                span = (TimeSpan)(this.FrameEndTime - this.StartTime);
                span = (TimeSpan)(this.EndTime - this.StartTime);
                float num2 = (float)(this.pBlockTracker.Width * (span.TotalMilliseconds / span.TotalMilliseconds));
                e.Graphics.FillRectangle(Brushes.Pink, new RectangleF(x, 0f, num2 - x, (float)this.pBlockTracker.Height));
                using (Font font = new Font("Microsoft Sans Serif", 7f, FontStyle.Regular))
                {
                    float num3 = 1.5f;
                    string text = this.StartTime.ToLongTimeString();
                    SizeF ef = e.Graphics.MeasureString(text, font);
                    ef.Width *= num3;
                    int num4 = 0;
                    int num5 = (int)(((float)this.pBlockTracker.Width) / ef.Width);
                    float num6 = 0f;
                    while (num6 <= this.pBlockTracker.Width)
                    {
                        span = (TimeSpan)(this.EndTime - this.StartTime);
                        text = this.StartTime.AddMilliseconds((span.TotalMilliseconds / ((double)num5)) * num4).ToLongTimeString();
                        float num7 = num6 - ((ef.Width / num3) / 2f);
                        if (num7 < 0f)
                        {
                            num7 = 0f;
                        }
                        if (num7 > (this.pBlockTracker.Width - (ef.Width / num3)))
                        {
                            num7 = this.pBlockTracker.Width - (ef.Width / num3);
                            this.DrawTimelineMarkups(e.Graphics, (float)(this.pBlockTracker.Width - 2), false);
                        }
                        else
                        {
                            this.DrawTimelineMarkups(e.Graphics, num6, false);
                        }
                        e.Graphics.DrawString(text, font, Brushes.Black, new PointF(num7, 5f));
                        num6 += this.pBlockTracker.Width / num5;
                        num4++;
                    }
                }
            }
        }

        public void Play()
        {
            this.timer.Enabled = true;
            if (this.UpdateTime != null)
            {
                this.UpdateTime(this, new EventArgs());
            }
            AudioIO.StopPlayback();
            this.UpdatePlaybackPosition();
            this.PlayState = CourtProcessRecorder.Controls.PlayState.Playing;
        }

        private void pWaveFormMouseDown(object sender, MouseEventArgs e)
        {
            if (((this.HasData() && !this.renderingInProgress) && ((e.Button == MouseButtons.Left) && ((Control.ModifierKeys & Keys.Control) == Keys.Control))) && (this.CtrlClickEvent != null))
            {
                this.CtrlClickEvent(this, new CtrlClickEventArgs(this.GetTimeFromWaveForm(e.X)));
            }
        }

        private void pWaveFormMouseHover(object sender, EventArgs e)
        {
            this.pWaveForm.Select();
        }

        private void pWaveFormPaint(object sender, PaintEventArgs e)
        {
            this.WaveFormRepaint(e.Graphics);
        }

        public void SeekToEvent(Event evt)
        {
            this.SeekToEvent(evt.Time);
        }

        public void SeekToEvent(DateTime dt)
        {
            if ((this.HasData() && !this.renderingInProgress) && !this.lockSeek)
            {
                this.lockSeek = true;
                try
                {
                    this.Time = dt;
                    TimeSpan span = (TimeSpan)(this.currentTime - this.StartTime);
                    this.tbPosition.Value = Math.Min(this.tbPosition.Maximum, (int)span.TotalMilliseconds);
                    this.positionChanged = true;
                    if (this.UpdateTime != null)
                    {
                        this.UpdateTime(this, new EventArgs());
                    }
                    if (this.PlayState == CourtProcessRecorder.Controls.PlayState.Playing)
                    {
                        RecordSet set;
                        long positionFromWaveForm = this.GetPositionFromWaveForm(out set, out this.playingEvent);
                        if ((positionFromWaveForm > -1L) && (set != null))
                        {
                            AudioIO.SeekPlayback(set, positionFromWaveForm);
                        }
                        else
                        {
                            AudioIO.StopPlayback();
                        }
                    }
                }
                finally
                {
                    this.lockSeek = false;
                }
            }
        }

        public void ShowEqualizer(bool show)
        {
            this.pEqualizer.Visible = show;
            this.UpdateFXControls();
        }

        private void ShowPitchControl(bool show)
        {
            this.tbPitch.Visible = show;
            this.UpdateFXControls();
        }

        public void Stop()
        {
            if (this.timer.Enabled)
            {
                this.PlayState = CourtProcessRecorder.Controls.PlayState.Idle;
                this.positionChanged = true;
                this.timer.Enabled = false;
                AudioIO.StopPlayback();
                this.playingEvent = null;
                if (this.OnStopped != null)
                {
                    this.OnStopped(this, new EventArgs());
                }
            }
        }

        private void tbEQScroll(object sender, EventArgs e)
        {
            TrackBar bar = sender as TrackBar;
            if (bar != null)
            {
                AudioIO.SetPlaybackEQValues((int)bar.Tag, this.cbEnableEQ.Checked ? bar.Value : 0);
            }
        }

        private void tbPitchScroll(object sender, EventArgs e)
        {
            AudioIO.SetPlaybackPitch((float)this.tbPitch.Value);
        }

        private void tbPositionScroll(object sender, EventArgs e)
        {
            if (this.HasData() && !this.renderingInProgress)
            {
                this.lockSeek = true;
                try
                {
                    this.Time = this.StartTime.AddMilliseconds((double)this.tbPosition.Value);
                    this.positionChanged = true;
                    if (this.UpdateTime != null)
                    {
                        this.UpdateTime(sender, e);
                    }
                    if (this.PlayState == CourtProcessRecorder.Controls.PlayState.Playing)
                    {
                        RecordSet set;
                        long positionFromWaveForm = this.GetPositionFromWaveForm(out set, out this.playingEvent);
                        if ((positionFromWaveForm > -1L) && (set != null))
                        {
                            AudioIO.SeekPlayback(set, positionFromWaveForm);
                        }
                        else
                        {
                            AudioIO.StopPlayback();
                        }
                    }
                }
                finally
                {
                    this.lockSeek = false;
                }
            }
        }

        private void timerPositionUpdater_Tick(object sender, EventArgs e)
        {
            if (this.HasData() && !this.renderingInProgress)
            {
                CaseDocument caseDocument = MainForm.CaseDocument;
                DateTime now = DateTime.Now;
                if ((this.PlayState == CourtProcessRecorder.Controls.PlayState.Playing) && (caseDocument != null))
                {
                    TimeSpan span = (TimeSpan)(now - this.lastTickTime);
                    this.currentTime = this.currentTime.AddMilliseconds((double)(span.Milliseconds * AudioIO.PlaybackChannel.Pitch));
                    span = (TimeSpan)(this.currentTime - this.StartTime);
                    this.tbPosition.Value = Math.Min(this.tbPosition.Maximum, (int)span.TotalMilliseconds);
                    if (this.currentTime > this.EndTime)
                    {
                        this.Stop();
                    }
                    else
                    {
                        this.Time = this.currentTime;
                    }
                    foreach (Event event2 in caseDocument.Events)
                    {
                        if (event2.Time.Date == this.CurrentDate)
                        {
                            span = (TimeSpan)(event2.Time - this.currentTime);
                            if (Math.Floor(span.TotalSeconds) > 0.0)
                            {
                                break;
                            }
                            span = (TimeSpan)(event2.Time - this.Time);
                            if (Math.Floor(span.TotalSeconds) == 0.0)
                            {
                                if (this.playingEvent != event2)
                                {
                                    if (((byte)(event2.EventType & EventType.RecordStart)) == 1)
                                    {
                                        AudioIO.SeekPlayback(event2.RecordSet, 0L);
                                    }
                                    if (((byte)(event2.EventType & EventType.RecordEnd)) == 2)
                                    {
                                        AudioIO.StopPlayback();
                                    }
                                }
                                this.playingEvent = event2;
                                break;
                            }
                        }
                    }
                    if (this.UpdateTime != null)
                    {
                        this.UpdateTime(this, e);
                    }
                }
                this.lastTickTime = DateTime.Now;
            }
        }

        private void timerTick(object sender, EventArgs e)
        {
            if (this.HasData() && !this.renderingInProgress)
            {
                CaseDocument caseDocument = MainForm.CaseDocument;
                if (caseDocument == null)
                {
                    this.Stop();
                }
                else
                {
                    foreach (Event event2 in caseDocument.Events)
                    {
                        if (event2.Time.Date == this.CurrentDate)
                        {
                            TimeSpan span = (TimeSpan)(event2.Time - this.currentTime);
                            if (Math.Floor(span.TotalSeconds) > 0.0)
                            {
                                break;
                            }
                            span = (TimeSpan)(event2.Time - this.Time);
                            if (Math.Floor(span.TotalSeconds) == 0.0)
                            {
                                if (((byte)(event2.EventType & EventType.RecordStart)) == 1)
                                {
                                    AudioIO.SeekPlayback(event2.RecordSet, 0L);
                                }
                                if (((byte)(event2.EventType & EventType.RecordEnd)) == 2)
                                {
                                    AudioIO.StopPlayback();
                                }
                            }
                        }
                    }
                    if (this.UpdateTime != null)
                    {
                        this.UpdateTime(this, e);
                    }
                }
            }
        }

        public void UpdateChannelData()
        {
            this.positionChanged = true;
            base.SuspendLayout();
            try
            {
                for (int i = base.Controls.Count - 1; i >= 0; i--)
                {
                    if (base.Controls[i] is PictureBox)
                    {
                        base.Controls[i].Dispose();
                    }
                }
                CaseDocument caseDocument = MainForm.CaseDocument;
                this.fileCount = 0;
                this.filesProcessed = 0;
                if (caseDocument != null)
                {
                    string[] strArray;
                    this.recordSet.Clear();
                    this.usedChannels.Clear();
                    this.renderingInProgress = true;
                    this.frameShouldUpdated = true;
                    foreach (Event event2 in caseDocument.Events)
                    {
                        if ((event2.Time.Date == this.currentDate) && (((byte)(event2.EventType & EventType.RecordStart)) == 1))
                        {
                            strArray = event2.RecordSet.FileName.Split(new char[] { ':' });
                            if (strArray.Length > 0)
                            {
                                this.fileCount += strArray.Length;
                            }
                        }
                    }
                    foreach (Event event2 in caseDocument.Events)
                    {
                        if ((event2.Time.Date == this.currentDate) && (((byte)(event2.EventType & EventType.RecordStart)) == 1))
                        {
                            if (event2.RecordSet.UserData == null)
                            {
                                event2.RecordSet.UserData = new List<WaveForm>();
                                strArray = event2.RecordSet.FileName.Split(new char[] { ':' });
                                if (strArray.Length > 0)
                                {
                                    foreach (string str in strArray)
                                    {
                                        string path = AudioIO.WorkingDirectory + MainForm.CaseDocument.TrialNo + @"\" + Path.GetFileName(str.Substring(str.IndexOf('?') + 1));
                                        if (File.Exists(path))
                                        {
                                            event2.RecordSet.UserData.Add(this.CreateWaveForm(path));
                                        }
                                        else
                                        {
                                            event2.RecordSet.UserData.Add(null);
                                        }
                                        this.filesProcessed++;
                                    }
                                }
                            }
                            this.recordSet.Add(event2, event2.RecordSet);
                        }
                    }
                    this.renderingInProgress = false;
                    this.BuildPreview();
                    this.WaveFormRepaint();
                    this.pBlockTracker.Invalidate();
                }
            }
            finally
            {
                base.ResumeLayout();
                base.PerformLayout();
            }
        }

        private void UpdateFXControls()
        {
            this.pBlockTracker.Left = this.pWaveForm.Left = (this.pEqualizer.Visible ? this.pEqualizer.Width : 0) + (this.tbPitch.Visible ? this.tbPitch.Width : 0);
            this.pBlockTracker.Width = this.pWaveForm.Width = base.Width - this.pWaveForm.Left;
            this.tbPosition.Left = this.pWaveForm.Left - 6;
            this.tbPosition.Width = this.pWaveForm.Width + 12;
            this.OnResize(new EventArgs());
        }

        private void UpdatePlaybackPosition()
        {
            Event event2 = null;
            foreach (Event event3 in MainForm.CaseDocument.Events)
            {
                if (((event3.Time.Date == this.CurrentDate) && (event3.Time <= this.currentTime)) && ((((byte)(event3.EventType & EventType.RecordStart)) == 1) || (((byte)(event3.EventType & EventType.RecordEnd)) == 2)))
                {
                    event2 = event3;
                }
            }
            if (event2 != null)
            {
                if (((byte)(event2.EventType & EventType.RecordStart)) == 1)
                {
                    if (!this.positionChanged)
                    {
                        AudioIO.SeekPlayback(event2.RecordSet, -1L);
                    }
                    else
                    {
                        RecordSet set;
                        long positionFromWaveForm = this.GetPositionFromWaveForm(out set, out this.playingEvent);
                        if ((positionFromWaveForm > -1L) && (set != null))
                        {
                            AudioIO.SeekPlayback(set, positionFromWaveForm);
                        }
                        else
                        {
                            AudioIO.SeekPlayback(event2.RecordSet, 0L);
                        }
                    }
                }
                if (((byte)(event2.EventType & EventType.RecordEnd)) == 2)
                {
                    AudioIO.StopPlayback();
                }
            }
        }

        private void WaveFormCallback(int framesDone, int framesTotal, TimeSpan elapsedTime, bool finished)
        {
            if (this.processedFileName != null)
            {
                WaitForProcessForm.StatusText1 = string.Format("ფაილის გახსნა ({0}/{1}) {2}%", this.filesProcessed, this.fileCount, (((framesDone * 1.0) / ((double)framesTotal)) * 100.0).ToString("0.00"));
                WaitForProcessForm.StatusText2 = this.processedFileName;
            }
        }

        protected void WaveFormRepaint()
        {
            if (this.HasData() && !this.renderingInProgress)
            {
                using (Graphics graphics = this.pWaveForm.CreateGraphics())
                {
                    this.WaveFormRepaint(graphics);
                }
            }
        }

        protected void WaveFormRepaint(Graphics g)
        {
            if ((this.HasData() && (this.EndTime != this.StartTime)) && !this.renderingInProgress)
            {
                CaseDocument caseDocument = MainForm.CaseDocument;
                if (caseDocument != null)
                {
                    TimeSpan span;
                    if (this.frameShouldUpdated)
                    {
                        if (this.cachedImage != null)
                        {
                            this.cachedImage.Dispose();
                        }
                        this.cachedImage = new Bitmap(this.pWaveForm.Width, this.pWaveForm.Height);
                        using (Graphics graphics = Graphics.FromImage(this.cachedImage))
                        {
                            graphics.Clear(this.pWaveForm.BackColor);
                            foreach (KeyValuePair<Event, RecordSet> pair in this.recordSet)
                            {
                                foreach (RecordSetData data in pair.Value.RecordSetData)
                                {
                                    if (pair.Value.UserData != null)
                                    {
                                        if (!pair.Value.GainFactor.HasValue)
                                        {
                                            float? nullable = null;
                                            foreach (WaveForm form in pair.Value.UserData)
                                            {
                                                if ((form != null) && (form.Wave != null))
                                                {
                                                    //float num = form.GetNormalizationGain(-1, -1) * 1.5f; todo old
                                                    float peak = 0;
                                                    float num = form.GetNormalizationGain(-1, -1, ref peak) * 1.5f;
                                                    nullable = new float?(Math.Min(nullable.GetValueOrDefault(num), num));
                                                }
                                            }
                                            pair.Value.GainFactor = nullable;
                                            foreach (WaveForm form in pair.Value.UserData)
                                            {
                                                if ((form != null) && (form.Wave != null))
                                                {
                                                    form.GainFactor = nullable.GetValueOrDefault(1f);
                                                }
                                            }
                                        }
                                        int num2 = (caseDocument.ProgramConfiguration == ProgramConfiguration.SingleChannel) ? this.GetChannelIndex(data.Channel) : 0;
                                        foreach (WaveForm form in pair.Value.UserData)
                                        {
                                            DateTime frameStartTime;
                                            DateTime frameEndTime;
                                            int totalMilliseconds;
                                            int num4;
                                            Rectangle rectangle;
                                            if ((form != null) && (form.Wave != null))
                                            {
                                                frameStartTime = pair.Key.Time.AddMilliseconds((double)((data.Position / ((long)form.Wave.bpf)) * form.Wave.chans));
                                                frameEndTime = frameStartTime.AddMilliseconds((double)((data.Length / ((long)form.Wave.bpf)) * form.Wave.chans));
                                                if (((frameStartTime >= this.FrameStartTime) || (frameEndTime > this.FrameStartTime)) && ((frameStartTime <= this.FrameEndTime) || (frameEndTime <= this.FrameEndTime)))
                                                {
                                                    if (frameStartTime < this.FrameStartTime)
                                                    {
                                                        frameStartTime = this.FrameStartTime;
                                                    }
                                                    if (frameEndTime > this.FrameEndTime)
                                                    {
                                                        frameEndTime = this.FrameEndTime;
                                                    }
                                                    span = (TimeSpan)(frameStartTime - pair.Key.Time);
                                                    totalMilliseconds = (int)span.TotalMilliseconds;
                                                    span = (TimeSpan)(frameEndTime - pair.Key.Time);
                                                    num4 = (int)span.TotalMilliseconds;
                                                    this.pWaveForm.BorderStyle = BorderStyle.Fixed3D;
                                                    rectangle = new Rectangle(0, 0, this.pWaveForm.Width, 0x2a);
                                                    rectangle.Width = (this.FrameLengthInMS == 0) ? 0 : ((this.pWaveForm.Width * (num4 - totalMilliseconds)) / this.FrameLengthInMS);
                                                    if (rectangle.Width > 0)
                                                    {
                                                        span = (TimeSpan)(frameStartTime - this.FrameStartTime);
                                                        rectangle.X = (int)(this.pWaveForm.Width * (span.TotalMilliseconds / ((double)this.FrameLengthInMS)));
                                                        if (rectangle.Width <= 3)
                                                        {
                                                            graphics.FillRectangle(new SolidBrush(Color.FromArgb(230, 0xff, 230)), rectangle.X, num2 * rectangle.Height, rectangle.Width, rectangle.Height);
                                                        }
                                                        else
                                                        {
                                                            using (Bitmap bitmap = form.CreateBitmap(rectangle.Width, rectangle.Height, totalMilliseconds, num4, false))
                                                            {
                                                                if (bitmap != null)
                                                                {
                                                                    graphics.DrawImage(bitmap, rectangle.X, num2 * rectangle.Height);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                using (new SolidBrush(Color.Red))
                                                {
                                                    frameStartTime = pair.Key.Time.AddMilliseconds((double)(((float)data.Position) / 44f));
                                                    frameEndTime = frameStartTime.AddMilliseconds((double)(((float)data.Length) / 44f));
                                                    if (((frameStartTime >= this.FrameStartTime) || (frameEndTime > this.FrameStartTime)) && ((frameStartTime <= this.FrameEndTime) || (frameEndTime <= this.FrameEndTime)))
                                                    {
                                                        if (frameStartTime < this.FrameStartTime)
                                                        {
                                                            frameStartTime = this.FrameStartTime;
                                                        }
                                                        if (frameEndTime > this.FrameEndTime)
                                                        {
                                                            frameEndTime = this.FrameEndTime;
                                                        }
                                                        span = (TimeSpan)(frameStartTime - pair.Key.Time);
                                                        totalMilliseconds = (int)span.TotalMilliseconds;
                                                        span = (TimeSpan)(frameEndTime - pair.Key.Time);
                                                        num4 = (int)span.TotalMilliseconds;
                                                        rectangle = new Rectangle(0, 0, this.pWaveForm.Width, 0x2a);
                                                        rectangle.Width = (this.FrameLengthInMS == 0) ? 0 : ((this.pWaveForm.Width * (num4 - totalMilliseconds)) / this.FrameLengthInMS);
                                                        if (rectangle.Width > 0)
                                                        {
                                                            using (Brush brush2 = new SolidBrush(Color.Red))
                                                            {
                                                                span = (TimeSpan)(frameStartTime - this.FrameStartTime);
                                                                rectangle.X = (int)(this.pWaveForm.Width * (span.TotalMilliseconds / ((double)this.FrameLengthInMS)));
                                                                rectangle.Y = num2 * rectangle.Height;
                                                                graphics.FillRectangle(brush2, rectangle);
                                                                StringFormat format = new StringFormat();
                                                                format.Alignment = StringAlignment.Center;
                                                                format.LineAlignment = StringAlignment.Center;
                                                                graphics.DrawString("ჩანაწერი არ მოიძებნა ან ვერ მოხერხდა გახსნა!", this.Font, new SolidBrush(Color.Black), rectangle, format);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (caseDocument.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                                            {
                                                num2++;
                                            }
                                        }
                                    }
                                }
                            }
                            graphics.SetClip(new Rectangle(0, 0, this.pWaveForm.Width, this.pWaveForm.Height));
                            using (Font font = new Font("Microsoft Sans Serif", 7f, FontStyle.Regular))
                            {
                                float num5 = 1.5f;
                                string text = this.FrameStartTime.ToLongTimeString();
                                SizeF ef = g.MeasureString(text, font);
                                ef.Width *= num5;
                                int num6 = 0;
                                float num7 = (int)(((float)this.pBlockTracker.Width) / ef.Width);
                                float x = 0f;
                                while (x <= this.pBlockTracker.Width)
                                {
                                    text = this.FrameStartTime.AddMilliseconds((double)((((float)this.FrameLengthInMS) / num7) * num6)).ToLongTimeString();
                                    float num9 = x - ((ef.Width / num5) / 2f);
                                    if (num9 < 0f)
                                    {
                                        num9 = 0f;
                                    }
                                    if (num9 > (this.pBlockTracker.Width - (ef.Width / num5)))
                                    {
                                        num9 = this.pBlockTracker.Width - (ef.Width / num5);
                                        this.DrawTimelineMarkups(graphics, (float)(this.pBlockTracker.Width - 2), true);
                                    }
                                    else
                                    {
                                        this.DrawTimelineMarkups(graphics, x, true);
                                    }
                                    graphics.DrawString(text, font, Brushes.Black, new PointF(num9, 5f));
                                    x += ((float)this.pBlockTracker.Width) / num7;
                                    num6++;
                                }
                            }
                        }
                        this.frameShouldUpdated = false;
                    }
                    g.DrawImage(this.cachedImage, 0, 0);
                    int num10 = (this.FrameLengthInMS == 0) ? 0 : ((int)((this.pWaveForm.Width * (span = (TimeSpan)(this.currentTime - this.FrameStartTime)).TotalMilliseconds) / ((double)this.FrameLengthInMS)));
                    g.DrawLine(Pens.Red, num10, 0, num10, this.pWaveForm.Height);
                }
            }
        }

        public DateTime CurrentDate
        {
            get
            {
                return this.currentDate;
            }
            set
            {
                if (this.currentDate != value)
                {
                    this.currentDate = value;
                }
            }
        }

        private DateTime EndTime
        {
            get
            {
                if (!this.endTime.HasValue)
                {
                    this.endTime = new DateTime?(DateTime.MinValue);
                    CaseDocument caseDocument = MainForm.CaseDocument;
                    if (caseDocument != null)
                    {
                        foreach (Event event2 in caseDocument.Events)
                        {
                            if (event2.Time.Date == this.CurrentDate)
                            {
                                DateTime? endTime = this.endTime;
                                DateTime time = event2.Time;
                                if (endTime.HasValue ? (endTime.GetValueOrDefault() < time) : false)
                                {
                                    this.endTime = new DateTime?(event2.Time);
                                }
                            }
                        }
                    }
                    if (this.endTime == DateTime.MinValue)
                    {
                        this.endTime = new DateTime?(DateTime.Now);
                    }
                }
                return this.endTime.Value;
            }
        }

        private DateTime FrameEndTime
        {
            get
            {
                return this.frameEndTime;
            }
            set
            {
                if ((value < this.StartTime) || (value > this.EndTime))
                {
                    value = this.EndTime;
                }
                if (this.frameEndTime != value)
                {
                    this.frameEndTime = value;
                    this.frameShouldUpdated = true;
                }
            }
        }

        private int FrameLengthInMS
        {
            get
            {
                TimeSpan span = (TimeSpan)(this.frameEndTime - this.frameStartTime);
                int totalMilliseconds = (int)span.TotalMilliseconds;
                if (totalMilliseconds < 0)
                {
                    totalMilliseconds = 0;
                }
                return totalMilliseconds;
            }
        }

        private DateTime FrameStartTime
        {
            get
            {
                return this.frameStartTime;
            }
            set
            {
                if (value < this.StartTime)
                {
                    value = this.StartTime;
                }
                if (this.frameStartTime != value)
                {
                    this.frameStartTime = value;
                    this.frameShouldUpdated = true;
                }
            }
        }

        private DateTime StartTime
        {
            get
            {
                if (!this.startTime.HasValue)
                {
                    this.startTime = new DateTime?(DateTime.MaxValue);
                    CaseDocument caseDocument = MainForm.CaseDocument;
                    if (caseDocument != null)
                    {
                        foreach (Event event2 in caseDocument.Events)
                        {
                            if (event2.Time.Date == this.CurrentDate)
                            {
                                DateTime? startTime = this.startTime;
                                DateTime time = event2.Time;
                                if (startTime.HasValue ? (startTime.GetValueOrDefault() > time) : false)
                                {
                                    this.startTime = new DateTime?(event2.Time);
                                }
                            }
                        }
                    }
                    if (this.startTime == DateTime.MaxValue)
                    {
                        this.startTime = new DateTime?(DateTime.Now);
                    }
                }
                return this.startTime.Value;
            }
        }

        public DateTime Time
        {
            get
            {
                return this.currentTime;
            }
            set
            {
                if (this.currentTime != value)
                {
                    this.positionChanged = true;
                }
                this.currentTime = value;
                long num = Math.Max(this.FrameLengthInMS, 0xbb8);
                if (this.currentTime < this.FrameStartTime)
                {
                    this.FrameStartTime = this.currentTime;
                }
                else if (this.currentTime > this.FrameEndTime)
                {
                    this.FrameStartTime = this.currentTime;
                }
                DateTime time = this.FrameStartTime.AddMilliseconds((double)(num + 1L));
                if (time.Ticks > this.EndTime.Ticks)
                {
                    this.FrameEndTime = this.EndTime;
                }
                else
                {
                    this.FrameEndTime = time;
                }
                this.WaveFormRepaint();
                this.pBlockTracker.Invalidate();
            }
        }
    }
}

