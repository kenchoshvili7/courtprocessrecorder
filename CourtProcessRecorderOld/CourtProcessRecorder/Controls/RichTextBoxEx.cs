﻿namespace CourtProcessRecorder.Controls
{
    using CourtRecorderCommon;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public class RichTextBoxEx : RichTextBox
    {
        public const uint CFE_AUTOCOLOR = 0x40000000;
        public const uint CFE_BOLD = 1;
        public const uint CFE_ITALIC = 2;
        public const uint CFE_LINK = 0x20;
        public const uint CFE_PROTECTED = 0x10;
        public const uint CFE_STRIKEOUT = 8;
        public const uint CFE_SUBSCRIPT = 0x10000;
        public const uint CFE_SUPERSCRIPT = 0x20000;
        public const uint CFE_UNDERLINE = 4;
        public const int CFM_ALLCAPS = 0x80;
        public const int CFM_ANIMATION = 0x40000;
        public const int CFM_BACKCOLOR = 0x4000000;
        public const uint CFM_BOLD = 1;
        public const uint CFM_CHARSET = 0x8000000;
        public const uint CFM_COLOR = 0x40000000;
        public const int CFM_DISABLED = 0x2000;
        public const int CFM_EMBOSS = 0x800;
        public const uint CFM_FACE = 0x20000000;
        public const int CFM_HIDDEN = 0x100;
        public const int CFM_IMPRINT = 0x1000;
        public const uint CFM_ITALIC = 2;
        public const int CFM_KERNING = 0x100000;
        public const int CFM_LCID = 0x2000000;
        public const uint CFM_LINK = 0x20;
        public const uint CFM_OFFSET = 0x10000000;
        public const int CFM_OUTLINE = 0x200;
        public const uint CFM_PROTECTED = 0x10;
        public const int CFM_REVAUTHOR = 0x8000;
        public const int CFM_REVISED = 0x4000;
        public const int CFM_SHADOW = 0x400;
        public const uint CFM_SIZE = 0x80000000;
        public const int CFM_SMALLCAPS = 0x40;
        public const int CFM_SPACING = 0x200000;
        public const uint CFM_STRIKEOUT = 8;
        public const int CFM_STYLE = 0x80000;
        public const uint CFM_SUBSCRIPT = 0x30000;
        public const uint CFM_SUPERSCRIPT = 0x30000;
        public const uint CFM_UNDERLINE = 4;
        public const int CFM_UNDERLINETYPE = 0x800000;
        public const int CFM_WEIGHT = 0x400000;
        public const byte CFU_UNDERLINE = 1;
        public const byte CFU_UNDERLINEDASH = 5;
        public const byte CFU_UNDERLINEDASHDOT = 6;
        public const byte CFU_UNDERLINEDASHDOTDOT = 7;
        public const byte CFU_UNDERLINEDOTTED = 4;
        public const byte CFU_UNDERLINEDOUBLE = 3;
        public const byte CFU_UNDERLINEHAIRLINE = 10;
        public const byte CFU_UNDERLINENONE = 0;
        public const byte CFU_UNDERLINETHICK = 9;
        public const byte CFU_UNDERLINEWAVE = 8;
        public const byte CFU_UNDERLINEWORD = 2;
        private const int EM_FORMATRANGE = 0x439;
        private const int EM_GETCHARFORMAT = 0x43a;
        private const int EM_GETPARAFORMAT = 0x43d;
        private const int EM_GETTEXTLENGTHEX = 0x45f;
        private const int EM_SETCHARFORMAT = 0x444;
        private const int EM_SETEVENTMASK = 0x431;
        private const int EM_SETPARAFORMAT = 0x447;
        private const int EM_SETTYPOGRAPHYOPTIONS = 0x4ca;
        public const short FW_BLACK = 900;
        public const short FW_BOLD = 700;
        public const short FW_DEMIBOLD = 600;
        public const short FW_DONTCARE = 0;
        public const short FW_EXTRABOLD = 800;
        public const short FW_EXTRALIGHT = 200;
        public const short FW_HEAVY = 900;
        public const short FW_LIGHT = 300;
        public const short FW_MEDIUM = 500;
        public const short FW_NORMAL = 400;
        public const short FW_REGULAR = 400;
        public const short FW_SEMIBOLD = 600;
        public const short FW_THIN = 100;
        public const short FW_ULTRABOLD = 800;
        public const short FW_ULTRALIGHT = 200;
        private char keyChar;
        public const int LF_FACESIZE = 0x20;
        private bool lockSelectionChangedEvent;
        private int oldEventMask = 0;
        public const ushort PFA_CENTER = 3;
        public const ushort PFA_LEFT = 1;
        public const ushort PFA_RIGHT = 2;
        public const uint PFM_ALIGNMENT = 8;
        public const uint PFM_NUMBERING = 0x20;
        public const uint PFM_OFFSET = 4;
        public const uint PFM_OFFSETINDENT = 0x80000000;
        public const uint PFM_RIGHTINDENT = 2;
        public const uint PFM_STARTINDENT = 1;
        public const uint PFM_TABSTOPS = 0x10;
        public const ushort PFN_BULLET = 1;
        private List<ProtectedDataInfo> protectedData = new List<ProtectedDataInfo>();
        private KeyEventArgs savedKeyEventArgs;
        private const int SCF_ALL = 4;
        private const int SCF_SELECTION = 1;
        private const int SCF_WORD = 2;
        private const int TO_ADVANCEDTYPOGRAPHY = 1;
        private int updating = 0;
        private const int WM_CHAR = 0x102;
        private const int WM_KEYDOWN = 0x100;
        private const int WM_KEYUP = 0x101;
        private const int WM_SETREDRAW = 11;
        private const int WM_USER = 0x400;

        public void ApplyStyle(string font, float? size, bool? bold, bool? italic, bool? underline, bool? strikeout)
        {
            this.BeginUpdate();
            this.lockSelectionChangedEvent = true;
            int selectionStart = base.SelectionStart;
            int selectionLength = this.SelectionLength;
            try
            {
                int num3 = selectionStart;
                while (num3 < Math.Min(this.GetTextLength(), selectionStart + selectionLength))
                {
                    this.SelectionLength = 1;
                    Font selectionFont = base.SelectionFont;
                    if (selectionFont != null)
                    {
                        FontStyle style = selectionFont.Style;
                        if (bold.HasValue)
                        {
                            if (bold.Value)
                            {
                                style |= FontStyle.Bold;
                            }
                            else
                            {
                                style &= ~FontStyle.Bold;
                            }
                        }
                        if (italic.HasValue)
                        {
                            if (italic.Value)
                            {
                                style |= FontStyle.Italic;
                            }
                            else
                            {
                                style &= ~FontStyle.Italic;
                            }
                        }
                        if (underline.HasValue)
                        {
                            if (underline.Value)
                            {
                                style |= FontStyle.Underline;
                            }
                            else
                            {
                                style &= ~FontStyle.Underline;
                            }
                        }
                        if (strikeout.HasValue)
                        {
                            if (strikeout.Value)
                            {
                                style |= FontStyle.Strikeout;
                            }
                            else
                            {
                                style &= ~FontStyle.Strikeout;
                            }
                        }
                        base.SelectionFont = new Font(string.IsNullOrEmpty(font) ? selectionFont.Name : font, !size.HasValue ? selectionFont.Size : size.Value, style);
                    }
                    num3++;
                    base.SelectionStart = num3;
                }
            }
            finally
            {
                base.SelectionStart = selectionStart;
                this.SelectionLength = selectionLength;
                this.lockSelectionChangedEvent = false;
                this.EndUpdate();
            }
        }

        public void BeginUpdate()
        {
            this.updating++;
            if (this.updating <= 1)
            {
                this.oldEventMask = SendMessage(new HandleRef(this, base.Handle), 0x431, 0, 0);
                SendMessage(new HandleRef(this, base.Handle), 11, 0, 0);
            }
        }

        public void DoKeyDown(KeyEventArgs e, char keyChar)
        {
            if (e != null)
            {
                Message m = new Message();
                m.Msg = 0x102;
                m.LParam = new IntPtr((int) e.KeyCode);
                m.WParam = new IntPtr(keyChar);
                m.HWnd = base.Handle;
                base.WndProc(ref m);
            }
        }

        public void EndUpdate()
        {
            this.updating--;
            if (this.updating <= 0)
            {
                SendMessage(new HandleRef(this, base.Handle), 11, 1, 0);
                SendMessage(new HandleRef(this, base.Handle), 0x431, 0, this.oldEventMask);
                this.Refresh();
            }
        }

        private Color GetColor(int crColor)
        {
            byte red = (byte) crColor;
            byte green = (byte) (crColor >> 8);
            byte blue = (byte) (crColor >> 0x10);
            return Color.FromArgb(red, green, blue);
        }

        private int GetCOLORREF(Color color)
        {
            int r = color.R;
            int g = color.G;
            int b = color.B;
            return this.GetCOLORREF(r, g, b);
        }

        private int GetCOLORREF(int r, int g, int b)
        {
            int num = r;
            int num2 = g << 8;
            int num3 = b << 0x10;
            return ((num | num2) | num3);
        }

        protected ProtectedDataInfo? GetSelectionProtectedLocation(int pos)
        {
            base.SelectionStart = pos;
            this.SelectionLength = 1;
            if (base.SelectionProtected)
            {
                ProtectedDataInfo info = new ProtectedDataInfo();
                while (base.SelectionProtected && (base.SelectionStart > 0))
                {
                    base.SelectionStart--;
                    this.SelectionLength = 1;
                }
                if (!base.SelectionProtected)
                {
                    base.SelectionStart++;
                }
                info.StartPos = base.SelectionStart;
                base.SelectionStart = pos;
                while (base.SelectionProtected && (base.SelectionStart < this.GetTextLength()))
                {
                    base.SelectionStart++;
                    this.SelectionLength = 1;
                }
                if (!base.SelectionProtected)
                {
                    base.SelectionStart--;
                }
                info.Length = (base.SelectionStart - info.StartPos) + 1;
                return new ProtectedDataInfo?(info);
            }
            return null;
        }

        public int GetTextLength()
        {
            CourtRecorderCommon.GETTEXTLENGTHEX wParam = new CourtRecorderCommon.GETTEXTLENGTHEX();
            wParam.uiFlags = 0;
            wParam.uiCodePage = 0x4b0;
            return User32Methods.SendMessage(base.Handle, 0x45f, ref wParam, IntPtr.Zero);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            SendMessage(new HandleRef(this, base.Handle), 0x4ca, 1, 1);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            this.savedKeyEventArgs = e;
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            this.keyChar = e.KeyChar;
            base.OnKeyPress(e);
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            this.savedKeyEventArgs = null;
        }

        protected override void OnProtected(EventArgs e)
        {
            this.RemoveProtectedSelection();
            base.OnProtected(e);
        }

        protected override void OnSelectionChanged(EventArgs e)
        {
            if (!this.lockSelectionChangedEvent)
            {
                base.OnSelectionChanged(e);
            }
        }

        public void RemoveProtectedSelection()
        {
            int selectionStart = base.SelectionStart;
            int num2 = selectionStart;
            int selectionLength = this.SelectionLength;
            this.BeginUpdate();
            this.lockSelectionChangedEvent = true;
            try
            {
                if (selectionStart > 0)
                {
                    base.SelectionStart = selectionStart - 1;
                    this.SelectionLength = 1;
                    while (base.SelectionProtected && (base.SelectionStart > 0))
                    {
                        base.SelectionStart--;
                        this.SelectionLength = 1;
                    }
                    num2 = base.SelectionStart + (base.SelectionProtected ? 0 : 1);
                }
                base.SelectionStart = selectionStart + selectionLength;
                while (base.SelectionProtected)
                {
                    base.SelectionStart++;
                    this.SelectionLength = 1;
                }
                selectionStart = base.SelectionStart - (base.SelectionProtected ? 0 : 1);
                base.SelectionStart = num2;
                this.SelectionLength = (selectionStart - num2) + 1;
                base.SelectionProtected = false;
                this.SelectedText = "";
                base.SelectionStart = num2;
                this.DoKeyDown(this.savedKeyEventArgs, this.keyChar);
                this.savedKeyEventArgs = null;
            }
            finally
            {
                this.lockSelectionChangedEvent = false;
                this.EndUpdate();
            }
        }

        public void RestoreProtectedData()
        {
            if (this.protectedData.Count > 0)
            {
                this.lockSelectionChangedEvent = true;
                this.BeginUpdate();
                try
                {
                    int selectionStart = base.SelectionStart;
                    int selectionLength = this.SelectionLength;
                    foreach (ProtectedDataInfo info in this.protectedData)
                    {
                        base.SelectionStart = info.StartPos;
                        this.SelectionLength = info.Length;
                        base.SelectionProtected = true;
                    }
                    base.SelectionStart = selectionStart;
                    this.SelectionLength = selectionLength;
                    this.protectedData.Clear();
                }
                finally
                {
                    this.lockSelectionChangedEvent = false;
                    this.EndUpdate();
                }
            }
        }

        public void SaveSelectionProtectedData()
        {
            if (this.SelectionLength > 0)
            {
                this.lockSelectionChangedEvent = true;
                this.BeginUpdate();
                try
                {
                    this.protectedData.Clear();
                    int selectionStart = base.SelectionStart;
                    int selectionLength = this.SelectionLength;
                    int pos = selectionStart;
                    while (pos < Math.Min(this.GetTextLength(), selectionStart + selectionLength))
                    {
                        ProtectedDataInfo? selectionProtectedLocation = this.GetSelectionProtectedLocation(pos);
                        if (selectionProtectedLocation.HasValue)
                        {
                            pos = (selectionProtectedLocation.Value.StartPos + selectionProtectedLocation.Value.Length) + 1;
                            this.protectedData.Add(selectionProtectedLocation.Value);
                            base.SelectionStart = selectionProtectedLocation.Value.StartPos;
                            this.SelectionLength = selectionProtectedLocation.Value.Length;
                            base.SelectionProtected = false;
                        }
                        else
                        {
                            pos++;
                        }
                    }
                    base.SelectionStart = selectionStart;
                    this.SelectionLength = selectionLength;
                }
                finally
                {
                    this.lockSelectionChangedEvent = false;
                    this.EndUpdate();
                }
            }
        }

        [DllImport("user32", CharSet=CharSet.Auto)]
        private static extern int SendMessage(HandleRef hWnd, int msg, int wParam, int lParam);
        [DllImport("user32", CharSet=CharSet.Auto)]
        private static extern int SendMessage(HandleRef hWnd, int msg, int wParam, ref CHARFORMAT lp);
        [DllImport("user32", CharSet=CharSet.Auto)]
        private static extern int SendMessage(HandleRef hWnd, int msg, int wParam, ref PARAFORMAT lp);

        public CHARFORMAT CharFormat
        {
            get
            {
                CHARFORMAT structure = new CHARFORMAT();
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x43a, 1, ref structure);
                return structure;
            }
            set
            {
                CHARFORMAT structure = value;
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x444, 1, ref structure);
            }
        }

        public CHARFORMAT DefaultCharFormat
        {
            get
            {
                CHARFORMAT structure = new CHARFORMAT();
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x43a, 4, ref structure);
                return structure;
            }
            set
            {
                CHARFORMAT structure = value;
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x444, 4, ref structure);
            }
        }

        public PARAFORMAT DefaultParaFormat
        {
            get
            {
                PARAFORMAT structure = new PARAFORMAT();
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x43d, 4, ref structure);
                return structure;
            }
            set
            {
                PARAFORMAT structure = value;
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x447, 4, ref structure);
            }
        }

        public PARAFORMAT ParaFormat
        {
            get
            {
                PARAFORMAT structure = new PARAFORMAT();
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x43d, 1, ref structure);
                return structure;
            }
            set
            {
                PARAFORMAT structure = value;
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x447, 1, ref structure);
            }
        }

        public TextAlign SelectionAlignment
        {
            get
            {
                PARAFORMAT structure = new PARAFORMAT();
                structure.cbSize = Marshal.SizeOf(structure);
                SendMessage(new HandleRef(this, base.Handle), 0x43d, 1, ref structure);
                if ((structure.dwMask & 8) == 0)
                {
                    return TextAlign.Left;
                }
                return (TextAlign) structure.wAlignment;
            }
            set
            {
                PARAFORMAT structure = new PARAFORMAT();
                structure.cbSize = Marshal.SizeOf(structure);
                structure.dwMask = 8;
                structure.wAlignment = (short) value;
                SendMessage(new HandleRef(this, base.Handle), 0x447, 1, ref structure);
            }
        }

        public bool SelectionSubScript
        {
            get
            {
                return ((this.CharFormat.dwEffects & 0x10000) == 0x10000);
            }
            set
            {
                CHARFORMAT charFormat = this.CharFormat;
                if (value)
                {
                    charFormat.dwMask |= 0x30000;
                    charFormat.dwEffects = 0x10000 | (charFormat.dwEffects & 0xfffdffff);
                }
                else
                {
                    charFormat.dwEffects &= 0xfffeffff;
                }
                this.CharFormat = charFormat;
            }
        }

        public bool SelectionSuperScript
        {
            get
            {
                return ((this.CharFormat.dwEffects & 0x20000) == 0x20000);
            }
            set
            {
                CHARFORMAT charFormat = this.CharFormat;
                if (value)
                {
                    charFormat.dwMask |= 0x30000;
                    charFormat.dwEffects = 0x20000 | (charFormat.dwEffects & 0xfffeffff);
                }
                else
                {
                    charFormat.dwEffects &= 0xfffdffff;
                }
                this.CharFormat = charFormat;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CHARFORMAT
        {
            public int cbSize;
            public uint dwMask;
            public uint dwEffects;
            public int yHeight;
            public int yOffset;
            public int crTextColor;
            public byte bCharSet;
            public byte bPitchAndFamily;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst=0x20)]
            public char[] szFaceName;
            public short wWeight;
            public short sSpacing;
            public int crBackColor;
            public uint lcid;
            public uint dwReserved;
            public short sStyle;
            public short wKerning;
            public byte bUnderlineType;
            public byte bAnimation;
            public byte bRevAuthor;
            public byte bReserved1;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PARAFORMAT
        {
            public int cbSize;
            public uint dwMask;
            public short wNumbering;
            public short wReserved;
            public int dxStartIndent;
            public int dxRightIndent;
            public int dxOffset;
            public short wAlignment;
            public short cTabCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst=0x20)]
            public int[] rgxTabs;
            public int dySpaceBefore;
            public int dySpaceAfter;
            public int dyLineSpacing;
            public short sStyle;
            public byte bLineSpacingRule;
            public byte bOutlineLevel;
            public short wShadingWeight;
            public short wShadingStyle;
            public short wNumberingStart;
            public short wNumberingStyle;
            public short wNumberingTab;
            public short wBorderSpace;
            public short wBorderWidth;
            public short wBorders;
        }

        [StructLayout(LayoutKind.Sequential)]
        protected struct ProtectedDataInfo
        {
            public int StartPos;
            public int Length;
        }

        public enum TextAlign
        {
            Center = 3,
            Justify = 4,
            Left = 1,
            Right = 2
        }
    }
}

