﻿using CourtRecorderCommon;

namespace CourtProcessRecorder.Controls
{
    using AudioConfiguration;
    using CourtProcessRecorder;
    using DeviceController;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Threading;
    using System.Windows.Forms;

    public class VolumeControl : UserControl
    {
        private ToolStripButton btnChannelName;
        private IContainer components = null;
        public const int ControlHeight = 0x2a;
        private Bitmap image;
        private int level;
        private bool lockVolumeChange;
        private TrackBar tbRecVolume;
        private ToolStrip toolStrip;
        private Modes viewMode;

        public event EventHandler OnSelectClicked;

        public VolumeControl()
        {
            this.InitializeComponent();
        }

        private void btnChannelNameClick(object sender, EventArgs e)
        {
            this.UpdateControl();
            if (this.OnSelectClicked != null)
            {
                this.OnSelectClicked(this, new EventArgs());
            }
            if ((this.ViewMode == Modes.Playback) && (MainForm.CaseDocument.ProgramConfiguration == ProgramConfiguration.MultiChannel))
            {
                this.Selected = this.btnChannelName.Checked;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
            this.image.Dispose();
        }

        private void DrawAmplitude()
        {
            this.btnChannelName.Image = null;
            Thread.Sleep(10);
            using (Graphics graphics = Graphics.FromImage(this.image))
            {
                Thread.Sleep(10);
                graphics.Clear(Color.Transparent);
                Thread.Sleep(10);
                Rectangle rect = new Rectangle(0, 5, (int) Math.Round((double) (this.image.Width * (((double) this.level) / 100.0))), this.image.Height - 15);
                Thread.Sleep(10);
                using (Brush brush = new SolidBrush(Color.FromArgb(0xff, 0, 0xff, 0)))
                {
                    graphics.FillRectangle(brush, rect);
                }
                Thread.Sleep(10);
            }
            Thread.Sleep(10);
            this.btnChannelName.Image = this.image;
            Thread.Sleep(10);
        }

        private void InitializeComponent()
        {
            this.toolStrip = new ToolStrip();
            this.btnChannelName = new ToolStripButton();
            this.tbRecVolume = new TrackBar();
            this.toolStrip.SuspendLayout();
            this.tbRecVolume.BeginInit();
            base.SuspendLayout();
            this.toolStrip.AutoSize = false;
            this.toolStrip.CanOverflow = false;
            this.toolStrip.Dock = DockStyle.None;
            this.toolStrip.GripMargin = new Padding(0);
            this.toolStrip.GripStyle = ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new ToolStripItem[] { this.btnChannelName });
            this.toolStrip.LayoutStyle = ToolStripLayoutStyle.Table;
            this.toolStrip.Location = new Point(13, -3);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new Size(120, 0x2a);
            this.toolStrip.TabIndex = 1;
            this.btnChannelName.AutoSize = false;
            this.btnChannelName.CheckOnClick = true;
            this.btnChannelName.Font = new Font("Sylfaen", 8f);
            this.btnChannelName.ImageAlign = ContentAlignment.MiddleLeft;
            this.btnChannelName.ImageScaling = ToolStripItemImageScaling.None;
            this.btnChannelName.ImageTransparentColor = Color.Magenta;
            this.btnChannelName.Name = "btnChannelName";
            this.btnChannelName.Size = new Size(0x17, 0x2e);
            this.btnChannelName.TextImageRelation = TextImageRelation.Overlay;
            this.btnChannelName.Click += new EventHandler(this.btnChannelNameClick);
            this.tbRecVolume.AutoSize = false;
            this.tbRecVolume.Location = new Point(-2, -4);
            this.tbRecVolume.Maximum = 100;
            this.tbRecVolume.Name = "tbRecVolume";
            this.tbRecVolume.Orientation = Orientation.Vertical;
            this.tbRecVolume.Size = new Size(0x15, 0x33);
            this.tbRecVolume.TabIndex = 2;
            this.tbRecVolume.Value = 5;
            this.tbRecVolume.Visible = false;
            this.tbRecVolume.ValueChanged += new EventHandler(this.tbRecVolumeValueChanged);
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.Controls.Add(this.toolStrip);
            base.Controls.Add(this.tbRecVolume);
            this.DoubleBuffered = true;
            base.Name = "VolumeControl";
            base.Size = new Size(0x85, 0x2a);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.tbRecVolume.EndInit();
            base.ResumeLayout(false);
        }

        private void tbRecVolumeValueChanged(object sender, EventArgs e)
        {
            if (!this.lockVolumeChange)
            {
                AudioIO.Recorder.SetChannelVolume((AudioSourceChannels) base.Tag, this.tbRecVolume.Value);
                if ((AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.SingleChannel) && this.btnChannelName.Checked)
                {
                    ((Recorder) AudioIO.Recorder).Volume = this.tbRecVolume.Value;
                }
            }
        }

        private void UpdateControl()
        {
            if (this.btnChannelName.Checked)
            {
                if (this.ViewMode == Modes.Record)
                {
                    this.btnChannelName.ForeColor = Color.Red;
                    this.btnChannelName.Font = new Font(this.btnChannelName.Font.Name, 9f, FontStyle.Bold);
                    this.btnChannelName.Text = MainForm.CaseDocument.GetChannelName(MainForm.CurrentDate, (AudioSourceChannels) base.Tag) + Environment.NewLine + "(იწერება)";
                    if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                    {
                        if (AudioIO.Apolo16Mixer.Connected)
                        {
                            ((Apolo16Recorder) AudioIO.Recorder).SetChannelMuted((AudioSourceChannels) base.Tag, false);
                        }
                        else
                        {
                            ((MultiChannelRecorder) AudioIO.Recorder).SetChannelMuted((AudioSourceChannels) base.Tag, false);
                        }
                    }
                }
                else if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                {
                    this.btnChannelName.ForeColor = Color.LightGray;
                    this.btnChannelName.Font = new Font(this.btnChannelName.Font.Name, 9f, FontStyle.Italic | FontStyle.Bold);
                }
            }
            else
            {
                this.btnChannelName.ForeColor = Color.Black;
                if (this.ViewMode == Modes.Record)
                {
                    this.btnChannelName.Font = new Font(this.btnChannelName.Font.Name, 9f, FontStyle.Regular);
                    this.btnChannelName.Text = MainForm.CaseDocument.GetChannelName(MainForm.CurrentDate, (AudioSourceChannels) base.Tag);
                    this.Level = 0;
                    if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                    {
                        if (AudioIO.Apolo16Mixer.Connected)
                        {
                            ((Apolo16Recorder) AudioIO.Recorder).SetChannelMuted((AudioSourceChannels) base.Tag, true);
                        }
                        else
                        {
                            ((MultiChannelRecorder) AudioIO.Recorder).SetChannelMuted((AudioSourceChannels) base.Tag, true);
                        }
                    }
                }
                else if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                {
                    this.btnChannelName.Font = new Font(this.btnChannelName.Font.Name, 9f, FontStyle.Regular);
                }
            }
        }

        public string ChannelName
        {
            get
            {
                return this.btnChannelName.Text;
            }
            set
            {
                this.btnChannelName.Text = value;
                this.btnChannelName.ToolTipText = value;
            }
        }

        public int Level
        {
            get
            {
                return this.level;
            }
            set
            {
                if (this.level != value)
                {
                    this.level = value;
                    this.DrawAmplitude();
                }
            }
        }

        public bool Selected
        {
            get
            {
                return this.btnChannelName.Checked;
            }
            set
            {
                this.btnChannelName.Checked = value;
                this.UpdateControl();
                if (this.ViewMode == Modes.Playback)
                {
                    if (this.btnChannelName.Checked)
                    {
                        this.Level = 0;
                    }
                }
                else if (!this.btnChannelName.Checked)
                {
                    this.Level = 0;
                }
                if ((this.ViewMode == Modes.Playback) && (MainForm.CaseDocument.ProgramConfiguration == ProgramConfiguration.MultiChannel))
                {
                    AudioIO.PlaybackChannel.SetMuted((AudioSourceChannels) base.Tag, this.btnChannelName.Checked);
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Modes ViewMode
        {
            get
            {
                return this.viewMode;
            }
            set
            {
                this.viewMode = value;
                if (this.viewMode == Modes.Record)
                {
                    this.tbRecVolume.Visible = true;
                    this.toolStrip.Location = new Point(13, -3);
                    this.toolStrip.Size = new Size(base.Width - 13, base.Height + 3);
                    this.lockVolumeChange = true;
                    try
                    {
                        this.tbRecVolume.Value = AudioIO.Recorder.GetChannelVolume((AudioSourceChannels) base.Tag);
                    }
                    finally
                    {
                        this.lockVolumeChange = false;
                    }
                    this.btnChannelName.Width = this.toolStrip.Width;
                    if (this.image != null)
                    {
                        this.image.Dispose();
                    }
                    this.image = new Bitmap(this.btnChannelName.Width, this.btnChannelName.Height, PixelFormat.Format32bppArgb);
                    this.btnChannelName.Image = this.image;
                }
                else
                {
                    this.tbRecVolume.Visible = false;
                    this.toolStrip.Location = new Point(0, -3);
                    this.toolStrip.Size = new Size(base.Width, base.Height + 3);
                    this.btnChannelName.Width = this.toolStrip.Width;
                    if (this.image != null)
                    {
                        this.image.Dispose();
                    }
                    this.image = new Bitmap(this.btnChannelName.Width, this.btnChannelName.Height, PixelFormat.Format32bppArgb);
                    this.btnChannelName.Image = this.image;
                }
            }
        }
    }
}

