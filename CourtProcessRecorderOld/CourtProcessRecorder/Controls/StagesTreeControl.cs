﻿namespace CourtProcessRecorder.Controls
{
    using CourtProcessRecorder.DataTypes;
    using CourtProcessRecorder.Properties;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;

    public class StagesTreeControl : TreeView
    {
        private IContainer components;
        private ImageList images;
        private const int paragraphedImage = 0;
        private const int unparagraphedImage = 1;

        public event GetDefaultParagraphNameEventHanlder OnGetDefaultParagraphNameEventHanlder;

        public StagesTreeControl()
        {
            this.InitializeComponent();
            base.ImageIndex = 0;
            base.SelectedImageIndex = 0;
        }

        public void AddNewItem()
        {
            if (base.LabelEdit)
            {
                EventHandler method = null;
                TreeNode node;
                base.BeginUpdate();
                try
                {
                    if ((base.SelectedNode != null) && base.SelectedNode.IsEditing)
                    {
                        base.SelectedNode.EndEdit(false);
                    }
                    if (base.SelectedNode != null)
                    {
                        if (base.SelectedNode.Parent != null)
                        {
                            node = base.SelectedNode.Parent.Nodes.Insert(base.SelectedNode.Index + 1, "");
                        }
                        else
                        {
                            node = base.Nodes.Insert(base.SelectedNode.Index + 1, "");
                        }
                    }
                    else
                    {
                        node = base.Nodes.Add("");
                    }
                }
                finally
                {
                    base.EndUpdate();
                }
                if (node != null)
                {
                    node.ImageIndex = 1;
                    node.SelectedImageIndex = 1;
                    node.Tag = null;
                    if (method == null)
                    {
                        method = delegate {
                            try
                            {
                                node.BeginEdit();
                            }
                            catch
                            {
                            }
                        };
                    }
                    base.BeginInvoke(method);
                }
            }
        }

        public void BuildNodesFromDataTable(Dictionary<string, CaseStage> stages)
        {
            base.BeginUpdate();
            try
            {
                base.Nodes.Clear();
                if (stages != null)
                {
                    foreach (KeyValuePair<string, CaseStage> pair in stages)
                    {
                        TreeNode node;
                        int num = base.Nodes.IndexOfKey(pair.Value.ParentStage);
                        if (num > -1)
                        {
                            node = base.Nodes[num].Nodes.Add(pair.Key, pair.Key);
                        }
                        else
                        {
                            node = base.Nodes.Add(pair.Key, pair.Key);
                        }
                        node.Tag = pair.Value.ParagraphName;
                        node.SelectedImageIndex = node.ImageIndex = ((pair.Value.ParagraphName != null) && ParagraphData.IsCustom(pair.Value.ParagraphName)) ? 0 : 1;
                    }
                    base.ExpandAll();
                }
            }
            finally
            {
                base.EndUpdate();
            }
        }

        public void BuildStagesFromNodes(Dictionary<string, CaseStage> stages)
        {
            stages.Clear();
            foreach (TreeNode node in base.Nodes)
            {
                string paragraphName = null;
                if (node.Tag != null)
                {
                    paragraphName = (string) node.Tag;
                }
                stages.Add(node.Name, new CaseStage(paragraphName, null));
                foreach (TreeNode node2 in node.Nodes)
                {
                    paragraphName = null;
                    if (node2.Tag != null)
                    {
                        paragraphName = (string) node2.Tag;
                    }
                    stages.Add(node2.Name, new CaseStage(paragraphName, node.Name));
                }
            }
        }

        public void DecreaseSelectedItemLevel()
        {
            if ((base.LabelEdit && (base.SelectedNode != null)) && (base.SelectedNode.Level < 1))
            {
                base.BeginUpdate();
                try
                {
                    if (base.SelectedNode.PrevNode != null)
                    {
                        TreeNode selectedNode = base.SelectedNode;
                        TreeNode prevNode = selectedNode.PrevNode;
                        selectedNode.Remove();
                        prevNode.Nodes.Add(selectedNode);
                        while (selectedNode.Nodes.Count > 0)
                        {
                            TreeNode node = selectedNode.Nodes[0];
                            node.Remove();
                            prevNode.Nodes.Add(node);
                        }
                        prevNode.ExpandAll();
                        base.SelectedNode = selectedNode;
                    }
                }
                finally
                {
                    base.EndUpdate();
                }
            }
        }

        public void DeleteSelectedItem()
        {
            if (base.LabelEdit && (base.SelectedNode != null))
            {
                base.BeginUpdate();
                try
                {
                    base.SelectedNode.Remove();
                }
                finally
                {
                    base.EndUpdate();
                }
            }
        }

        protected virtual string GetDefaultParagraphName(int level)
        {
            if (this.OnGetDefaultParagraphNameEventHanlder != null)
            {
                return this.OnGetDefaultParagraphNameEventHanlder(level);
            }
            if (level == 0)
            {
                return Resources.ParagraphStage;
            }
            return Resources.ParagraphActionPersonNote;
        }

        public void IncreaseSelectedItemLevel()
        {
            if (base.LabelEdit && (base.SelectedNode != null))
            {
                base.BeginUpdate();
                try
                {
                    if (base.SelectedNode.Level > 0)
                    {
                        TreeNode selectedNode = base.SelectedNode;
                        TreeNode parent = selectedNode.Parent;
                        int index = selectedNode.Index;
                        selectedNode.Remove();
                        base.Nodes.Insert(parent.Index + 1, selectedNode);
                        while (parent.Nodes.Count > index)
                        {
                            TreeNode node = parent.Nodes[index];
                            node.Remove();
                            selectedNode.Nodes.Add(node);
                        }
                        selectedNode.ExpandAll();
                        base.SelectedNode = selectedNode;
                    }
                }
                finally
                {
                    base.EndUpdate();
                }
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(StagesTreeControl));
            this.images = new ImageList(this.components);
            base.SuspendLayout();
            this.images.ImageStream = (ImageListStreamer) manager.GetObject("images.ImageStream");
            this.images.TransparentColor = Color.Fuchsia;
            this.images.Images.SetKeyName(0, "show_symbols16_h.bmp");
            this.images.Images.SetKeyName(1, "new16.bmp");
            base.LineColor = Color.Black;
            base.ResumeLayout(false);
        }

        public void MoveSelectedItemDown()
        {
            if (base.LabelEdit && (base.SelectedNode != null))
            {
                base.BeginUpdate();
                try
                {
                    if (base.SelectedNode.NextNode != null)
                    {
                        int index = base.SelectedNode.Index;
                        TreeNode nextNode = base.SelectedNode.NextNode;
                        TreeNodeCollection nodes = (base.SelectedNode.Parent == null) ? base.Nodes : base.SelectedNode.Parent.Nodes;
                        if (nodes != null)
                        {
                            nextNode.Remove();
                            nodes.Insert(index, nextNode);
                        }
                    }
                }
                finally
                {
                    base.EndUpdate();
                }
            }
        }

        public void MoveSelectedItemUp()
        {
            if (base.LabelEdit && (base.SelectedNode != null))
            {
                base.BeginUpdate();
                try
                {
                    if (base.SelectedNode.PrevNode != null)
                    {
                        int index = base.SelectedNode.Index;
                        TreeNode prevNode = base.SelectedNode.PrevNode;
                        TreeNodeCollection nodes = (base.SelectedNode.Parent == null) ? base.Nodes : base.SelectedNode.Parent.Nodes;
                        if (nodes != null)
                        {
                            prevNode.Remove();
                            nodes.Insert(index, prevNode);
                        }
                    }
                }
                finally
                {
                    base.EndUpdate();
                }
            }
        }

        protected override void OnAfterLabelEdit(NodeLabelEditEventArgs e)
        {
            EventHandler method = null;
            string label = e.Label;
            if (!((label != null) || string.IsNullOrEmpty(e.Node.Name)))
            {
                e.CancelEdit = true;
                base.OnAfterLabelEdit(e);
            }
            else if (string.IsNullOrEmpty(label))
            {
                e.Node.Remove();
            }
            else
            {
                if (base.Nodes.Find(label, true).Length > 0)
                {
                    if (string.IsNullOrEmpty(e.Node.Name))
                    {
                        e.Node.Remove();
                    }
                    else
                    {
                        e.CancelEdit = true;
                    }
                    if (method == null)
                    {
                        method = delegate {
                            MessageBox.Show(this, "ასეთი სახელი უკვე არსებობს!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        };
                    }
                    base.BeginInvoke(method);
                }
                else
                {
                    e.Node.Name = e.Label;
                }
                base.OnAfterLabelEdit(e);
            }
        }

        protected override void OnCreateControl()
        {
            if (base.LabelEdit)
            {
                base.ImageList = this.images;
            }
            else
            {
                base.ImageList = null;
            }
            base.OnCreateControl();
        }

        public void RenameItem()
        {
            if (base.LabelEdit && (base.SelectedNode != null))
            {
                base.SelectedNode.BeginEdit();
            }
        }

        public ParagraphData ToggleSelectedItemParagraph(Template template, out bool isAddOperation)
        {
            ParagraphData item = null;
            isAddOperation = false;
            if (base.LabelEdit && (base.SelectedNode != null))
            {
                if (base.SelectedNode.Tag != null)
                {
                    item = template.GetParagraphByName((string) base.SelectedNode.Tag);
                    template.Paragraphs.Remove(item);
                    base.SelectedNode.Tag = null;
                    base.SelectedNode.ImageIndex = 1;
                    base.SelectedNode.SelectedImageIndex = 1;
                    return item;
                }
                isAddOperation = true;
                base.SelectedNode.ImageIndex = 0;
                base.SelectedNode.SelectedImageIndex = 0;
                if (template.GetParagraphByName(base.SelectedNode.Name) != null)
                {
                    base.SelectedNode.Tag = base.SelectedNode.Name + "1";
                }
                else
                {
                    base.SelectedNode.Tag = base.SelectedNode.Name;
                }
                item = new ParagraphData((string) base.SelectedNode.Tag);
                template.Paragraphs.Add(item);
            }
            return item;
        }

        public delegate string GetDefaultParagraphNameEventHanlder(int level);
    }
}

