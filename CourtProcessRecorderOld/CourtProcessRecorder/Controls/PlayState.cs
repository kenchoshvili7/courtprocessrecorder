﻿namespace CourtProcessRecorder.Controls
{
    using System;

    public enum PlayState
    {
        Idle,
        Playing,
        Paused
    }
}

