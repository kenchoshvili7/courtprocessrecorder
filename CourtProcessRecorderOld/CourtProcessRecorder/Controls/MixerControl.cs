﻿using CourtRecorderCommon;

namespace CourtProcessRecorder.Controls
{
    using AudioConfiguration;
    using CourtProcessRecorder;
    using CourtProcessRecorder.DataTypes;
    using DeviceController;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    public class MixerControl : UserControl
    {
        private IContainer components = null;
        private DateTime currentDate;
        private int lockTrackBarScrollevent;
        private Timer timer;
        private TrackBar trackBar;
        private bool usbInterfaceButtonPressedNonSyncEventAssigned;
        private Modes viewMode;

        public MixerControl()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private VolumeControl GetControlFromChannelId(AudioSourceChannels channel)
        {
            foreach (Control control in base.Controls)
            {
                if ((control is VolumeControl) && (((AudioSourceChannels) control.Tag) == channel))
                {
                    return (control as VolumeControl);
                }
            }
            return null;
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            this.timer = new Timer(this.components);
            this.trackBar = new TrackBar();
            this.trackBar.BeginInit();
            base.SuspendLayout();
            this.timer.Enabled = true;
            this.timer.Interval = 300;
            this.timer.Tick += new EventHandler(this.timerTick);
            this.trackBar.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
            this.trackBar.AutoSize = false;
            this.trackBar.LargeChange = 10;
            this.trackBar.Location = new Point(-6, 0x79);
            this.trackBar.Maximum = 100;
            this.trackBar.Name = "trackBar";
            this.trackBar.Size = new Size(0x8e, 0x1b);
            this.trackBar.TabIndex = 1;
            this.trackBar.TickFrequency = 10;
            this.trackBar.Scroll += new EventHandler(this.trackBarScroll);
            base.Controls.Add(this.trackBar);
            this.MinimumSize = new Size(0x85, 150);
            base.Name = "MixerControl";
            base.Size = new Size(0x85, 150);
            this.trackBar.EndInit();
            base.ResumeLayout(false);
        }

        private void OnSelectChannelClicked(object sender, EventArgs e)
        {
            VolumeControl control = sender as VolumeControl;
            if (control != null)
            {
                AudioSourceChannels tag;
                AudioSourceChannels none = AudioSourceChannels.None;
                if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                {
                    foreach (Control control2 in base.Controls)
                    {
                        if (((control2 != control) && (control2 is VolumeControl)) && (control2 as VolumeControl).Selected)
                        {
                            none = (AudioSourceChannels) ((byte) (none | ((AudioSourceChannels) (control2 as VolumeControl).Tag)));
                        }
                    }
                }
                if ((this.ViewMode == Modes.Record) && (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.SingleChannel))
                {
                    foreach (Control control2 in base.Controls)
                    {
                        if ((control2 != control) && (control2 is VolumeControl))
                        {
                            (control2 as VolumeControl).Selected = false;
                        }
                    }
                }
                if ((AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel) && AudioIO.Apolo16Mixer.Connected)
                {
                    if (this.ViewMode == Modes.Record)
                    {
                        tag = (AudioSourceChannels) control.Tag;
                        if (!control.Selected)
                        {
                            //AudioIO.SelectedAudioChannels = (AudioSourceChannels) ((byte) (none & ((byte) ~tag))); //todo uncomment this
                        }
                        else
                        {
                            AudioIO.SelectedAudioChannels = (AudioSourceChannels) ((byte) (none | tag));
                        }
                    }
                }
                else
                {
                    tag = (AudioSourceChannels) control.Tag;
                    if (!control.Selected)
                    {
                        AudioIO.SelectedAudioChannels = AudioSourceChannels.None;
                    }
                    else if (this.ViewMode == Modes.Record)
                    {
                        AudioIO.SelectedAudioChannels = tag;
                    }
                    else if (this.ViewMode == Modes.Playback)
                    {
                        AudioIO.SelectedAudioChannels = AudioSourceChannels.ChannelPC;
                    }
                }
            }
        }

        public void SelectRecordingPriorityChannel()
        {
            VolumeControl controlFromChannelId;
            if (AudioIO.UsePriorityLogic)
            {
                foreach (Channel channel in AudioIO.Configuration.Channels)
                {
                    controlFromChannelId = this.GetControlFromChannelId(channel.ChannelNo);
                    if (controlFromChannelId != null)
                    {
                        if (channel.ChannelType == ChannelType.Judge)
                        {
                            controlFromChannelId.Selected = true;
                            AudioIO.SelectedAudioChannels = channel.ChannelNo;
                        }
                        else
                        {
                            controlFromChannelId.Selected = false;
                        }
                    }
                }
            }
            else
            {
                AudioSourceChannels none = AudioSourceChannels.None;
                foreach (Channel channel in AudioIO.Configuration.Channels)
                {
                    controlFromChannelId = this.GetControlFromChannelId(channel.ChannelNo);
                    if (controlFromChannelId != null)
                    {
                        if (channel.ChannelType != ChannelType.None)
                        {
                            controlFromChannelId.Selected = true;
                            none = (AudioSourceChannels) ((byte) (none | channel.ChannelNo));
                        }
                        else
                        {
                            controlFromChannelId.Selected = false;
                        }
                    }
                }
                AudioIO.SelectedAudioChannels = none;
            }
        }

        public void SetVolume(int volume)
        {
            this.lockTrackBarScrollevent++;
            try
            {
                this.trackBar.Value = volume;
            }
            finally
            {
                this.lockTrackBarScrollevent--;
            }
        }

        private void timerTick(object sender, EventArgs e)
        {
            foreach (Control control in base.Controls)
            {
                VolumeControl control2 = control as VolumeControl;
                if (control2 != null)
                {
                    switch (control2.ViewMode)
                    {
                        case Modes.Playback:
                            goto Label_00FD;

                        case Modes.Record:
                            goto Label_0050;
                    }
                }
                continue;
            Label_0050:
                if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.SingleChannel)
                {
                    if (((byte) (((AudioSourceChannels) control2.Tag) & AudioIO.SelectedAudioChannels)) > 0)
                    {
                        control2.Level = ((Recorder) AudioIO.Recorder).AmplitudeLevel;
                    }
                }
                else if (AudioIO.Apolo16Mixer.Connected)
                {
                    control2.Level = ((Apolo16Recorder) AudioIO.Recorder).AmplitudeLevel[(AudioSourceChannels) control2.Tag];
                }
                else
                {
                    control2.Level = ((MultiChannelRecorder) AudioIO.Recorder).AmplitudeLevel[(AudioSourceChannels) control2.Tag];
                }
                continue;
            Label_00FD:
                if (MainForm.CaseDocument.ProgramConfiguration == ProgramConfiguration.SingleChannel)
                {
                    if (((byte) (((AudioSourceChannels) control2.Tag) & AudioIO.SelectedAudioChannels)) > 0)
                    {
                        control2.Level = AudioIO.GetPlaybackLevel(AudioSourceChannels.ChannelPC);
                    }
                }
                else
                {
                    control2.Level = AudioIO.GetPlaybackLevel((AudioSourceChannels) control2.Tag);
                }
            }
        }

        private void trackBarScroll(object sender, EventArgs e)
        {
            if ((this.lockTrackBarScrollevent == 0) && (this.ViewMode == Modes.Playback))
            {
                AudioIO.SetPlaybackVolume(this.trackBar.Value);
            }
        }

        public void UpdateControlValues()
        {
            if (this.ViewMode == Modes.Playback)
            {
                AudioIO.SetPlaybackVolume(AudioIO.GetPlaybackVolume());
                this.trackBar.Value = AudioIO.GetPlaybackVolume();
            }
        }

        public void UpdateMixer()
        {
            int num2;
            VolumeControl control;
            AudioIO.IsInitialized(true);
            if ((AudioIO.ProgramMode == ProgramMode.Recording) && !this.usbInterfaceButtonPressedNonSyncEventAssigned)
            {
                if (AudioIO.Apolo16Mixer.Connected)
                {
                    AudioIO.Apolo16Mixer.ButtonsStateChanged += new DeviceController.ButtonPressedEvent(this.UsbInterfaceButtonPressedNonSync2);
                }
                else
                {
                    AudioIO.UsbInterface.ButtonPressed += new UsbInterface.ButtonPressedEvent(this.UsbInterfaceButtonPressedNonSync);
                }
                this.usbInterfaceButtonPressedNonSyncEventAssigned = true;
            }
            for (int i = base.Controls.Count - 1; i > 0; i--)
            {
                base.Controls.RemoveAt(i);
            }
            this.trackBar.Visible = this.ViewMode == Modes.Playback;
            CaseDocument caseDocument = MainForm.CaseDocument;
            if (this.ViewMode == Modes.Playback)
            {
                AudioIO.SelectedAudioChannels = AudioSourceChannels.ChannelPC;
                if ((caseDocument != null) && (caseDocument.Events.Count > 0))
                {
                    List<AudioSourceChannels> list = new List<AudioSourceChannels>();
                    num2 = 0;
                    if (caseDocument.ProgramConfiguration == ProgramConfiguration.SingleChannel)
                    {
                        foreach (Event event2 in caseDocument.Events)
                        {
                            if ((event2.Time.Date == this.CurrentDate) && (event2.RecordSet != null))
                            {
                                foreach (RecordSetData data in event2.RecordSet.RecordSetData)
                                {
                                    if (!((data.Channel == AudioSourceChannels.None) || list.Contains(data.Channel)))
                                    {
                                        list.Add(data.Channel);
                                        control = new VolumeControl();
                                        control.Location = new Point(0, num2);
                                        control.Tag = data.Channel;
                                        control.ChannelName = caseDocument.GetChannelName(event2.Time.Date, data.Channel);
                                        control.ViewMode = Modes.Playback;
                                        control.OnSelectClicked += new EventHandler(this.OnSelectChannelClicked);
                                        base.Controls.Add(control);
                                        num2 += control.Height;
                                    }
                                }
                            }
                        }
                    }
                    else if (caseDocument.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                    {
                        foreach (Channel channel in caseDocument.GetChannelNames(MainForm.CurrentDate))
                        {
                            list.Add(channel.ChannelNo);
                            control = new VolumeControl();
                            control.Location = new Point(0, num2);
                            control.Tag = channel.ChannelNo;
                            control.ChannelName = channel.Name;
                            control.ViewMode = Modes.Playback;
                            control.OnSelectClicked += new EventHandler(this.OnSelectChannelClicked);
                            base.Controls.Add(control);
                            num2 += control.Height;
                        }
                    }
                }
            }
            else
            {
                VolumeControl control2 = null;
                num2 = 0;
                caseDocument.ApplyChannelNames(this.CurrentDate);
                foreach (Channel channel2 in caseDocument.GetChannelNames(MainForm.CurrentDate))
                {
                    control = new VolumeControl();
                    control.Tag = channel2.ChannelNo;
                    control.Location = new Point(0, num2);
                    control.ChannelName = channel2.Name;
                    control.ViewMode = Modes.Record;
                    if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.MultiChannel)
                    {
                        if (AudioIO.Apolo16Mixer.Connected)
                        {
                            control.Selected = !((Apolo16Recorder) AudioIO.Recorder).GetChannelMuted(channel2.ChannelNo);
                        }
                        else
                        {
                            control.Selected = !((MultiChannelRecorder) AudioIO.Recorder).GetChannelMuted(channel2.ChannelNo);
                        }
                    }
                    control.OnSelectClicked += new EventHandler(this.OnSelectChannelClicked);
                    num2 += control.Height;
                    if (channel2.ChannelType == ChannelType.Judge)
                    {
                        control2 = control;
                    }
                    base.Controls.Add(control);
                }
            }
            this.UpdateControlValues();
        }

        private void UsbInterfaceButtonPressed(ChannelButtons channelButton, bool pressed)
        {
            VolumeControl control;
            if (((!AudioIO.IsInitialized(false) || !AudioIO.IsRecordingPossible()) || ((this.ViewMode != Modes.Record) || !pressed)) || !AudioIO.UsePriorityLogic)
            {
                return;
            }
            if (channelButton == ChannelButtons.Channel1)
            {
                if (((byte) (AudioIO.SelectedAudioChannels & AudioSourceChannels.Channel1)) == 1)
                {
                    AudioIO.SelectedAudioChannels = AudioSourceChannels.None;
                }
                else
                {
                    AudioIO.SelectedAudioChannels = AudioSourceChannels.Channel1;
                }
            }
            else if (((byte) (AudioIO.SelectedAudioChannels & AudioSourceChannels.Channel1)) != 1)
            {
                switch (channelButton)
                {
                    case ChannelButtons.Channel5:
                        AudioIO.SelectedAudioChannels = AudioSourceChannels.Channel5;
                        goto Label_00CD;

                    case ChannelButtons.Channel4:
                        AudioIO.SelectedAudioChannels = AudioSourceChannels.Channel4;
                        goto Label_00CD;

                    case ChannelButtons.Channel3:
                        AudioIO.SelectedAudioChannels = AudioSourceChannels.Channel3;
                        goto Label_00CD;

                    case ChannelButtons.Channel2:
                        AudioIO.SelectedAudioChannels = AudioSourceChannels.Channel2;
                        goto Label_00CD;

                    case ChannelButtons.Channel6:
                        AudioIO.SelectedAudioChannels = AudioSourceChannels.Channel6;
                        goto Label_00CD;
                }
                AudioIO.SelectedAudioChannels = AudioSourceChannels.None;
            }
        Label_00CD:
            control = this.GetControlFromChannelId(AudioIO.SelectedAudioChannels);
            foreach (Control control2 in base.Controls)
            {
                if (control2 is VolumeControl)
                {
                    if (control2 != control)
                    {
                        (control2 as VolumeControl).Selected = false;
                    }
                    else
                    {
                        control.Selected = true;
                    }
                }
            }
        }

        private void UsbInterfaceButtonPressedNonSync(ChannelButtons channelButton, bool pressed)
        {
            ButtonPressedEvent method = new ButtonPressedEvent(this.UsbInterfaceButtonPressed);
            if (base.IsHandleCreated)
            {
                base.Invoke(method, new object[] { channelButton, pressed });
            }
        }

        private void UsbInterfaceButtonPressedNonSync2(MixerChannels buttonStates, bool pressed)
        {
            ButtonPressedEvent method = new ButtonPressedEvent(this.UsbInterfaceButtonPressed);
            ChannelButtons none = ChannelButtons.None;
            if (((byte) (buttonStates & MixerChannels.Microphone1)) != 0)
            {
                none = ChannelButtons.Channel1;
            }
            else if (((byte) (buttonStates & MixerChannels.Microphone2)) != 0)
            {
                none = ChannelButtons.Channel2;
            }
            else if (((byte) (buttonStates & MixerChannels.Microphone3)) != 0)
            {
                none = ChannelButtons.Channel3;
            }
            else if (((byte) (buttonStates & MixerChannels.Microphone4)) != 0)
            {
                none = ChannelButtons.Channel4;
            }
            else if (((byte) (buttonStates & MixerChannels.Microphone5)) != 0)
            {
                none = ChannelButtons.Channel5;
            }
            else if (((byte) (buttonStates & MixerChannels.Microphone6)) != 0)
            {
                none = ChannelButtons.Channel6;
            }
            if (base.IsHandleCreated)
            {
                base.Invoke(method, new object[] { none, pressed });
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public DateTime CurrentDate
        {
            get
            {
                return this.currentDate;
            }
            set
            {
                if (this.currentDate != value)
                {
                    this.currentDate = value;
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Modes ViewMode
        {
            get
            {
                return this.viewMode;
            }
            set
            {
                if (this.viewMode != value)
                {
                    if (!((value != Modes.Record) || AudioIO.Recorder.IsRecording))
                    {
                        value = Modes.Playback;
                    }
                    this.viewMode = value;
                    this.UpdateMixer();
                }
            }
        }

        public delegate void ButtonPressedEvent(ChannelButtons channelButton, bool pressed);
    }
}

