﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class EditPersonDialog : BaseForm
    {
        private Button btnCancel;
        private Button btnOK;
        private ComboBox cbPersonTypes;
        private IContainer components = null;
        public DialogType dialogType;
        private TextBox edtName;
        private Label label1;
        private Label label2;

        public EditPersonDialog(PersonTypes personTypes, DialogType dialogType)
        {
            this.InitializeComponent();
            switch (dialogType)
            {
                case DialogType.Add:
                    this.Text = this.Text + " (დამატება)";
                    break;

                case DialogType.Edit:
                    this.Text = this.Text + " (შეცვლა)";
                    break;
            }
            this.dialogType = dialogType;
            this.cbPersonTypes.Items.AddRange(personTypes.ToArray());
            if (this.cbPersonTypes.Items.Count > 0)
            {
                this.cbPersonTypes.SelectedIndex = 0;
            }
            else
            {
                this.cbPersonTypes.Enabled = false;
                this.btnOK.Enabled = false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.label1 = new Label();
            this.edtName = new TextBox();
            this.label2 = new Label();
            this.cbPersonTypes = new ComboBox();
            this.btnOK = new Button();
            this.btnCancel = new Button();
            base.SuspendLayout();
            this.label1.AutoSize = true;
            this.label1.Location = new Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0xa8, 0x10);
            this.label1.TabIndex = 0;
            this.label1.Text = "სახელი, გვარი, მამის სახელი";
            this.edtName.Location = new Point(12, 0x1c);
            this.edtName.Name = "edtName";
            this.edtName.Size = new Size(0x139, 0x17);
            this.edtName.TabIndex = 1;
            this.label2.AutoSize = true;
            this.label2.Location = new Point(9, 0x36);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x2c, 0x10);
            this.label2.TabIndex = 2;
            this.label2.Text = "როლი";
            this.cbPersonTypes.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbPersonTypes.FormattingEnabled = true;
            this.cbPersonTypes.Location = new Point(12, 0x49);
            this.cbPersonTypes.Name = "cbPersonTypes";
            this.cbPersonTypes.Size = new Size(0x139, 0x18);
            this.cbPersonTypes.TabIndex = 3;
            this.btnOK.DialogResult = DialogResult.OK;
            this.btnOK.Location = new Point(0xa9, 0x67);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new Size(0x4b, 0x17);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Location = new Point(250, 0x67);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x4b, 0x17);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "გაუქმება";
            this.btnCancel.UseVisualStyleBackColor = true;
            base.AcceptButton = this.btnOK;
            base.AutoScaleDimensions = new SizeF(7f, 16f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x151, 0x87);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.btnOK);
            base.Controls.Add(this.cbPersonTypes);
            base.Controls.Add(this.edtName);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "EditPersonDialog";
            this.Text = "მონაწილე";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string PersonName
        {
            get
            {
                return this.edtName.Text;
            }
            set
            {
                this.edtName.Text = value;
            }
        }

        public string PersonType
        {
            get
            {
                return this.cbPersonTypes.Text;
            }
            set
            {
                this.cbPersonTypes.Text = value;
            }
        }

        public enum DialogType
        {
            Add,
            Edit,
            Delete
        }
    }
}

