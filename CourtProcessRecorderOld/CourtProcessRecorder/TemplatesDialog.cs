﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.Controls;
    using CourtProcessRecorder.DataTypes;
    using CourtProcessRecorder.Properties;
    using CourtRecorderCommon;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class TemplatesDialog : BaseForm
    {
        private ToolStrip actionsToolStrip;
        public const uint ActiveRichTextBoxColor = 0xffefefef;
        private string activeTemplateName;
        private string activeTemplateNameBeforeRename;
        private ToolStripButton btnActionParagraph;
        private ToolStripButton btnAddAction;
        private ToolStripButton btnAddPersonType;
        private ToolStripButton btnBold;
        private ToolStripButton btnBullet;
        private ToolStripButton btnCenterAlign;
        private ToolStripButton btnDecreaseActionLevel;
        private ToolStripButton btnDelete;
        private ToolStripButton btnDeleteAction;
        private ToolStripButton btnDeletePersonType;
        private ToolStripButton btnEditPersonType;
        private ToolStripButton btnIncreaseActionLevel;
        private ToolStripButton btnInsertPersonTypeVariable;
        private ToolStripButton btnInsertVariable;
        private ToolStripButton btnItalic;
        private ToolStripButton btnLeftAlign;
        private ToolStripButton btnLeftRightAlign;
        private ToolStripButton btnMoveActionDown;
        private ToolStripButton btnMoveActionUp;
        private ToolStripButton btnNew;
        private ToolStripButton btnRename;
        private ToolStripButton btnRenameAction;
        private ToolStripButton btnRightAlign;
        private ToolStripButton btnSave;
        private ToolStripButton btnSubscript;
        private ToolStripButton btnSuperscript;
        private ToolStripButton btnText;
        private ToolStripButton btnUnderline;
        private ToolStripComboBox cbFont;
        private ToolStripComboBox cbFontSize;
        private ToolStripComboBox cbParagraphFormats;
        private ToolStripComboBox cbTemplates;
        private ToolStripComboBox cbZoom;
        private ColumnHeader ch;
        private IContainer components = null;
        public Template currentTemplate;
        private RichTextBoxEx edtFooter;
        private RichTextBoxEx edtParagraph;
        private RichTextBoxEx edtProtocol;
        private ToolStrip footerToolStrip;
        private DataGridView gridSubsituations;
        private ToolStripLabel labelPossibleActions;
        private ToolStripLabel labelTemplateName;
        private RichTextBoxEx lastActiveRichTextBox;
        private SplitContainer leftBottomSplitter;
        private SplitContainer leftSplitter;
        private ListView listPersonTypes;
        private int lockEdtTextChanged;
        private int lockSelectedParagraphChange;
        private int lockSelectedTemplateChange;
        private SplitContainer mainSplitter;
        private ToolStrip mainToolStrip;
        private ToolStrip paragraphToolStrip;
        private ToolStrip personTypesToolStrip;
        private SplitContainer rightBottomSplitter;
        private SplitContainer rightSplitter;
        private ToolStrip subsituationsToolStrip;
        private ToolStripLabel toolStripLabel1;
        private ToolStripLabel toolStripLabel3;
        private ToolStripLabel toolStripLabel4;
        private ToolStripLabel toolStripLabel5;
        private ToolStripLabel toolStripLabel6;
        private ToolStripLabel toolStripLabel7;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripSeparator toolStripSeparator7;
        private ToolStripSeparator toolStripSeparator8;
        private StagesTreeControl treeCaseStage;

        public TemplatesDialog(string templateName)
        {
            this.activeTemplateNameBeforeRename = this.activeTemplateName = templateName;
            this.InitializeComponent();
        }

        private void AddVariable(string variable)
        {
            variable = " " + variable + " ";
            this.lastActiveRichTextBox.RemoveProtectedSelection();
            this.lastActiveRichTextBox.SelectedText = variable;
            int num = this.lastActiveRichTextBox.SelectionStart - variable.Length;
            this.lastActiveRichTextBox.SelectionStart = num + 1;
            this.lastActiveRichTextBox.SelectionLength = variable.Length - 2;
            this.lastActiveRichTextBox.SelectionColor = Color.Red;
            this.lastActiveRichTextBox.SelectionProtected = true;
            this.lastActiveRichTextBox.SelectionStart = (num + this.lastActiveRichTextBox.SelectionLength) + 1;
            this.lastActiveRichTextBox.SelectionLength = 0;
            this.edtLeave(this.lastActiveRichTextBox, null);
        }

        private void btnActionParagraphClick(object sender, EventArgs e)
        {
            if (this.treeCaseStage.SelectedNode != null)
            {
                bool flag;
                ParagraphData item = this.treeCaseStage.ToggleSelectedItemParagraph(this.currentTemplate, out flag);
                if (item != null)
                {
                    if (flag)
                    {
                        this.cbParagraphFormats.SelectedIndex = this.cbParagraphFormats.Items.Add(item);
                    }
                    else
                    {
                        this.cbParagraphFormats.Items.Remove(item);
                        this.cbParagraphFormats.Text = this.OnGetDefaultParagraphNameEventHanlder(this.treeCaseStage.SelectedNode.Level);
                    }
                }
                this.currentTemplate.Modified = true;
            }
        }

        private void btnAddActionClick(object sender, EventArgs e)
        {
            this.treeCaseStage.AddNewItem();
            this.currentTemplate.Modified = true;
        }

        private void btnAddPersonTypeClick(object sender, EventArgs e)
        {
            using (SetNewNameDialog dialog = new SetNewNameDialog(""))
            {
                dialog.Text = "მონაწილის ტიპის დამატება";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if (this.currentTemplate.PersonTypes.FindByName(dialog.TemplateName) != null)
                    {
                        throw new WarningException("ასეთი მონაწილე უკვე არსებობს!");
                    }
                    this.listPersonTypes.Items.Add(dialog.TemplateName);
                    this.currentTemplate.PersonTypes.Add(dialog.TemplateName);
                    this.currentTemplate.Modified = true;
                }
            }
        }

        private void btnBoldClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    bool? italic = null;
                    italic = null;
                    this.lastActiveRichTextBox.ApplyStyle(null, null, new bool?(!this.btnBold.Checked), italic, italic, null);
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnBulletClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    this.lastActiveRichTextBox.SelectionBullet = !this.lastActiveRichTextBox.SelectionBullet;
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnCenterAlignClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    this.lastActiveRichTextBox.SelectionAlignment = RichTextBoxEx.TextAlign.Center;
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnDecreaseActionLevelClick(object sender, EventArgs e)
        {
            this.treeCaseStage.DecreaseSelectedItemLevel();
            this.currentTemplate.Modified = true;
        }

        private void btnDeleteActionClick(object sender, EventArgs e)
        {
            this.treeCaseStage.DeleteSelectedItem();
            this.currentTemplate.Modified = true;
        }

        private void btnDeleteClick(object sender, EventArgs e)
        {
            int selectedIndex = this.cbTemplates.SelectedIndex;
            if (selectedIndex > -1)
            {
                if (this.cbTemplates.Text.EndsWith(" *"))
                {
                    MessageBox.Show(this, "გამოყენებული სხდომის ტიპის წაშლა არ შეიძლება!", "დაუშვებელი ოპერაცია", MessageBoxButtons.OK);
                }
                else
                {
                    if (this.currentTemplate != null)
                    {
                        this.currentTemplate.Dispose();
                    }
                    this.currentTemplate = null;
                    TemplatesManager.DeleteTemplate(this.cbTemplates.Text);
                    this.cbTemplates.Items.RemoveAt(selectedIndex);
                    if (selectedIndex > 0)
                    {
                        this.cbTemplates.SelectedIndex = selectedIndex - 1;
                    }
                    else if (this.cbTemplates.Items.Count > 0)
                    {
                        this.cbTemplates.SelectedIndex = 0;
                    }
                    if (this.cbTemplates.SelectedIndex == -1)
                    {
                        this.LoadFromTemplate(this.currentTemplate);
                        this.cbTemplates.ToolTipText = "სხდომის ტიპი: " + this.cbTemplates.Text;
                    }
                    else
                    {
                        this.cbTemplates.ToolTipText = "სხდომის ტიპი: (არ არის შექმნილი)";
                    }
                }
            }
        }

        private void btnDeletePersonTypeClick(object sender, EventArgs e)
        {
            for (int i = this.listPersonTypes.Items.Count - 1; i >= 0; i--)
            {
                if (this.listPersonTypes.Items[i].Selected)
                {
                    this.currentTemplate.PersonTypes.Remove(this.listPersonTypes.Items[i].Text);
                    this.listPersonTypes.Items.RemoveAt(i);
                    this.currentTemplate.Modified = true;
                }
            }
        }

        private void btnEditPersonTypeClick(object sender, EventArgs e)
        {
            if (this.listPersonTypes.SelectedItems.Count > 0)
            {
                int index = this.currentTemplate.PersonTypes.IndexOf(this.listPersonTypes.SelectedItems[0].Text);
                if (index > -1)
                {
                    using (SetNewNameDialog dialog = new SetNewNameDialog(this.currentTemplate.PersonTypes[index]))
                    {
                        dialog.Text = "მონაწილის ტიპის შეცვლა";
                        if (dialog.ShowDialog() == DialogResult.OK)
                        {
                            int num2 = this.currentTemplate.PersonTypes.IndexOf(dialog.TemplateName);
                            if ((num2 != -1) && (num2 != index))
                            {
                                throw new WarningException("ასეთი მონაწილე უკვე არსებობს!");
                            }
                            this.currentTemplate.PersonTypes[index] = dialog.TemplateName;
                            this.listPersonTypes.SelectedItems[0].Text = dialog.TemplateName;
                            this.currentTemplate.Modified = true;
                        }
                    }
                }
            }
        }

        private void btnIncreaseActionLevelClick(object sender, EventArgs e)
        {
            this.treeCaseStage.IncreaseSelectedItemLevel();
            this.currentTemplate.Modified = true;
        }

        private void btnInsertPersonTypeVariableClick(object sender, EventArgs e)
        {
            if ((this.lastActiveRichTextBox != null) && (this.listPersonTypes.SelectedItems.Count > 0))
            {
                this.AddVariable(this.listPersonTypes.SelectedItems[0].Text);
                this.currentTemplate.Modified = true;
            }
        }

        private void btnInsertVariableClick(object sender, EventArgs e)
        {
            if (((this.lastActiveRichTextBox != null) && (this.gridSubsituations.SelectedCells.Count > 0)) && (this.gridSubsituations.SelectedCells[0].Value != null))
            {
                string variable = this.gridSubsituations.SelectedCells[0].Value.ToString();
                this.AddVariable(variable);
                this.currentTemplate.Modified = true;
            }
        }

        private void btnItalicClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    bool? bold = null;
                    bold = null;
                    this.lastActiveRichTextBox.ApplyStyle(null, null, bold, new bool?(!this.btnItalic.Checked), bold, null);
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnLeftAlignClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    this.lastActiveRichTextBox.SelectionAlignment = RichTextBoxEx.TextAlign.Left;
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnLeftRightAlignClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    this.lastActiveRichTextBox.SelectionAlignment = RichTextBoxEx.TextAlign.Justify;
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnMoveActionDownClick(object sender, EventArgs e)
        {
            this.treeCaseStage.MoveSelectedItemDown();
            this.currentTemplate.Modified = true;
        }

        private void btnMoveActionUpClick(object sender, EventArgs e)
        {
            this.treeCaseStage.MoveSelectedItemUp();
            this.currentTemplate.Modified = true;
        }

        private void btnNewClick(object sender, EventArgs e)
        {
            this.lockSelectedTemplateChange++;
            try
            {
                string templateName = null;
                if (!((this.currentTemplate == null) || this.currentTemplate.Name.EndsWith(" *")))
                {
                    templateName = this.currentTemplate.Name;
                }
                switch (this.QuerySave())
                {
                    case DialogResult.Yes:
                        this.SaveTemplate();
                        break;

                    case DialogResult.No:
                        if (!this.currentTemplate.Name.EndsWith(" *"))
                        {
                            if (!TemplatesManager.HasTemplate(this.currentTemplate.Name))
                            {
                                this.cbTemplates.Items.Remove(this.currentTemplate.Name);
                            }
                            this.currentTemplate.Dispose();
                        }
                        this.currentTemplate = null;
                        break;

                    case DialogResult.Cancel:
                        return;
                }
                if (templateName != null)
                {
                    if (this.currentTemplate != null)
                    {
                        this.currentTemplate.Dispose();
                    }
                    this.currentTemplate = TemplatesManager.GetTemplateByName(templateName);
                }
                else
                {
                    this.currentTemplate = null;
                }
                if (this.currentTemplate != null)
                {
                    this.currentTemplate = (Template) this.currentTemplate.Clone();
                    this.currentTemplate.Name = PreferTemplateName(this.currentTemplate.Name);
                    this.cbTemplates.SelectedIndex = this.cbTemplates.Items.Add(this.currentTemplate.Name);
                }
                else
                {
                    this.currentTemplate = new Template(PreferTemplateName("ახალი სხდომის ტიპი"));
                    this.cbTemplates.SelectedIndex = this.cbTemplates.Items.Add(this.currentTemplate.Name);
                }
                this.cbTemplates.ToolTipText = "სხდომის ტიპი: " + this.cbTemplates.Text;
                this.LoadFromTemplate(this.currentTemplate);
                this.currentTemplate.Modified = true;
            }
            finally
            {
                this.lockSelectedTemplateChange--;
            }
        }

        private void btnRenameActionClick(object sender, EventArgs e)
        {
            this.treeCaseStage.RenameItem();
            this.currentTemplate.Modified = true;
        }

        private void btnRenameClick(object sender, EventArgs e)
        {
            using (SetNewNameDialog dialog = new SetNewNameDialog(this.currentTemplate.Name))
            {
                if ((dialog.ShowDialog() == DialogResult.OK) && !this.currentTemplate.Name.Equals(dialog.TemplateName))
                {
                    if (TemplatesManager.GetTemplateByName(dialog.TemplateName) != null)
                    {
                        throw new WarningException("ასეთი სახელი უკვე გამოყენებულია!");
                    }
                    if (!this.currentTemplate.Name.EndsWith(" *"))
                    {
                        TemplatesManager.RenameTemplate(this.currentTemplate.Name, dialog.TemplateName);
                    }
                    this.currentTemplate.Modified = true;
                    this.currentTemplate.Name = dialog.TemplateName;
                    this.lockSelectedTemplateChange++;
                    try
                    {
                        this.cbTemplates.Items[this.cbTemplates.SelectedIndex] = dialog.TemplateName;
                        this.cbTemplates.ToolTipText = "სხდომის ტიპი: " + this.cbTemplates.Text;
                    }
                    finally
                    {
                        this.lockSelectedTemplateChange--;
                    }
                }
            }
        }

        private void btnRightAlignClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    this.lastActiveRichTextBox.SelectionAlignment = RichTextBoxEx.TextAlign.Right;
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnSaveClick(object sender, EventArgs e)
        {
            this.SaveTemplate();
        }

        private void btnSubscriptClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    this.lastActiveRichTextBox.SelectionSubScript = !this.lastActiveRichTextBox.SelectionSubScript;
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnSuperscriptClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    this.lastActiveRichTextBox.SelectionSuperScript = !this.lastActiveRichTextBox.SelectionSuperScript;
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void btnTextClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                FontStyle regular = this.lastActiveRichTextBox.SelectionFont.Style;
                regular = FontStyle.Regular;
                this.lastActiveRichTextBox.BeginUpdate();
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    this.lastActiveRichTextBox.ApplyStyle(null, null, false, false, false, false);
                    this.lastActiveRichTextBox.SelectionColor = SystemColors.WindowText;
                    this.lastActiveRichTextBox.SelectionSubScript = true;
                    this.lastActiveRichTextBox.SelectionSubScript = false;
                    this.lastActiveRichTextBox.SelectionAlignment = RichTextBoxEx.TextAlign.Left;
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                }
                finally
                {
                    this.lastActiveRichTextBox.EndUpdate();
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
                this.currentTemplate.Modified = true;
            }
        }

        private void btnUnderlineClick(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    bool? bold = null;
                    bold = null;
                    this.lastActiveRichTextBox.ApplyStyle(null, null, bold, bold, new bool?(!this.btnUnderline.Checked), null);
                    this.edtSelectionChanged(this.lastActiveRichTextBox, null);
                    this.currentTemplate.Modified = true;
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void cbFontKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.cbFontSelectedIndexChanged(sender, e);
            }
        }

        private void cbFontSelectedIndexChanged(object sender, EventArgs e)
        {
            if ((this.lastActiveRichTextBox != null) && (this.cbFont.Items.IndexOf(this.cbFont.Text) > -1))
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    bool? bold = null;
                    bold = null;
                    bold = null;
                    this.lastActiveRichTextBox.ApplyStyle(this.cbFont.Text, null, bold, bold, bold, null);
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
                this.currentTemplate.Modified = true;
            }
        }

        private void cbFontSizeKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.cbFontSizeSelectedIndexChanged(sender, e);
            }
        }

        private void cbFontSizeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                this.lastActiveRichTextBox.SaveSelectionProtectedData();
                try
                {
                    float num;
                    if (float.TryParse(this.cbFontSize.Text, out num))
                    {
                        bool? bold = null;
                        bold = null;
                        bold = null;
                        this.lastActiveRichTextBox.ApplyStyle(null, new float?(num), bold, bold, bold, null);
                        this.currentTemplate.Modified = true;
                    }
                }
                finally
                {
                    this.lastActiveRichTextBox.RestoreProtectedData();
                }
            }
        }

        private void cbParagraphFormatsSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lockSelectedParagraphChange == 0)
            {
                this.lockEdtTextChanged++;
                try
                {
                    ParagraphData selectedItem = this.cbParagraphFormats.SelectedItem as ParagraphData;
                    if (selectedItem != null)
                    {
                        this.edtParagraph.Rtf = selectedItem.Data;
                    }
                    else
                    {
                        this.edtParagraph.Rtf = null;
                    }
                }
                finally
                {
                    this.lockEdtTextChanged--;
                }
            }
        }

        private void cbTemplatesSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lockSelectedTemplateChange == 0)
            {
                switch (this.QuerySave())
                {
                    case DialogResult.Yes:
                        this.SaveTemplate();
                        break;

                    case DialogResult.No:
                        if (!this.currentTemplate.Name.EndsWith(" *"))
                        {
                            if (!TemplatesManager.HasTemplate(this.currentTemplate.Name))
                            {
                                this.cbTemplates.Items.Remove(this.currentTemplate.Name);
                            }
                            this.currentTemplate.Dispose();
                        }
                        this.currentTemplate = null;
                        break;

                    case DialogResult.Cancel:
                        return;
                }
                this.activeTemplateNameBeforeRename = this.cbTemplates.Text;
                if (!this.cbTemplates.Text.EndsWith(" *"))
                {
                    this.currentTemplate = TemplatesManager.GetTemplateByName(this.cbTemplates.Text);
                    if (this.currentTemplate != null)
                    {
                        this.currentTemplate = (Template) this.currentTemplate.Clone();
                    }
                }
                else
                {
                    Template template = MainForm.CaseDocument.Templates.FindByName(this.cbTemplates.Text);
                    this.currentTemplate = (Template) template.Clone();
                }
                this.LoadFromTemplate(this.currentTemplate);
                if (this.currentTemplate != null)
                {
                    this.currentTemplate.Modified = false;
                }
                this.cbTemplates.ToolTipText = "სხდომის ტიპი: " + this.cbTemplates.Text;
            }
        }

        private void cbZoomKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.cbZoomSelectedIndexChanged(sender, e);
            }
        }

        private void cbZoomSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                float num;
                string text = this.cbZoom.Text;
                if (!string.IsNullOrEmpty(text))
                {
                    for (int i = text.Length - 1; i >= 0; i--)
                    {
                        if (!char.IsDigit(text[i]))
                        {
                            text = text.Remove(i, 1);
                        }
                    }
                }
                if (!float.TryParse(text, out num))
                {
                    num = this.lastActiveRichTextBox.ZoomFactor * 100f;
                }
                this.lastActiveRichTextBox.ZoomFactor = num / 100f;
                if (this.currentTemplate != null)
                {
                    this.currentTemplate.Modified = true;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void edtControlsEnter(object sender, EventArgs e)
        {
            this.lastActiveRichTextBox.BackColor = SystemColors.Window;
            this.lastActiveRichTextBox = (RichTextBoxEx) sender;
            this.lastActiveRichTextBox.BackColor = Color.FromArgb(-1052689);
            if (this.lastActiveRichTextBox == this.edtParagraph)
            {
                TemplatesManager.Subsituations.DefaultView.RowFilter = "TYPE_NAME='PARAGRAPH'";
                this.btnInsertPersonTypeVariable.Enabled = false;
            }
            else
            {
                TemplatesManager.Subsituations.DefaultView.RowFilter = "TYPE_NAME='PROTOCOL'";
                this.btnInsertPersonTypeVariable.Enabled = true;
            }
            this.gridSubsituations.DataSource = TemplatesManager.Subsituations.DefaultView;
            this.cbZoom.Text = ((((RichTextBoxEx) sender).ZoomFactor * 100f)).ToString() + "%";
        }

        private void edtLeave(object sender, EventArgs e)
        {
            if (sender == this.edtParagraph)
            {
                ParagraphData selectedItem = this.cbParagraphFormats.SelectedItem as ParagraphData;
                if (selectedItem != null)
                {
                    selectedItem.Data = this.edtParagraph.Rtf;
                }
            }
            this.currentTemplate.ProtocolHeader = this.edtProtocol.Rtf;
            this.currentTemplate.ProtocolFooter = this.edtFooter.Rtf;
        }

        private void edtProtected(object sender, EventArgs e)
        {
            if ((this.lastActiveRichTextBox != null) && (this.currentTemplate != null))
            {
                this.currentTemplate.Modified = true;
            }
        }

        private void edtSelectionChanged(object sender, EventArgs e)
        {
            if (this.lastActiveRichTextBox != null)
            {
                RichTextBoxEx lastActiveRichTextBox = this.lastActiveRichTextBox;
                this.lastActiveRichTextBox = null;
                try
                {
                    if (lastActiveRichTextBox.SelectionFont == null)
                    {
                        this.cbFont.SelectedIndex = -1;
                    }
                    else
                    {
                        this.cbFont.Text = lastActiveRichTextBox.SelectionFont.Name;
                    }
                    this.cbFontSize.Text = (lastActiveRichTextBox.SelectionFont == null) ? "" : lastActiveRichTextBox.SelectionFont.Size.ToString();
                    this.btnText.Checked = (lastActiveRichTextBox.SelectionFont != null) && (lastActiveRichTextBox.SelectionFont.Style == FontStyle.Regular);
                    this.btnBold.Checked = (lastActiveRichTextBox.SelectionFont != null) && lastActiveRichTextBox.SelectionFont.Bold;
                    this.btnItalic.Checked = (lastActiveRichTextBox.SelectionFont != null) && lastActiveRichTextBox.SelectionFont.Italic;
                    this.btnUnderline.Checked = (lastActiveRichTextBox.SelectionFont != null) && lastActiveRichTextBox.SelectionFont.Underline;
                    this.btnLeftAlign.Checked = (lastActiveRichTextBox.SelectionFont != null) && (lastActiveRichTextBox.SelectionAlignment == RichTextBoxEx.TextAlign.Left);
                    this.btnCenterAlign.Checked = (lastActiveRichTextBox.SelectionFont != null) && (lastActiveRichTextBox.SelectionAlignment == RichTextBoxEx.TextAlign.Center);
                    this.btnRightAlign.Checked = (lastActiveRichTextBox.SelectionFont != null) && (lastActiveRichTextBox.SelectionAlignment == RichTextBoxEx.TextAlign.Right);
                    this.btnLeftRightAlign.Checked = lastActiveRichTextBox.SelectionAlignment == RichTextBoxEx.TextAlign.Justify;
                    this.btnBullet.Checked = lastActiveRichTextBox.SelectionBullet;
                    this.btnSuperscript.Checked = lastActiveRichTextBox.SelectionSuperScript;
                    this.btnSubscript.Checked = lastActiveRichTextBox.SelectionSubScript;
                }
                finally
                {
                    this.lastActiveRichTextBox = lastActiveRichTextBox;
                }
            }
        }

        private void edtTextChanged(object sender, EventArgs e)
        {
            if ((this.lockEdtTextChanged == 0) && (this.currentTemplate != null))
            {
                this.currentTemplate.Modified = true;
            }
        }

        private void gridPersonTypesResize(object sender, EventArgs e)
        {
            this.ch.Width = this.ch.ListView.ClientRectangle.Width;
        }

        private void gridSubsituationsCellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.btnInsertVariableClick(sender, e);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(TemplatesDialog));
            this.mainToolStrip = new ToolStrip();
            this.labelTemplateName = new ToolStripLabel();
            this.cbTemplates = new ToolStripComboBox();
            this.btnNew = new ToolStripButton();
            this.btnDelete = new ToolStripButton();
            this.btnRename = new ToolStripButton();
            this.btnSave = new ToolStripButton();
            this.actionsToolStrip = new ToolStrip();
            this.labelPossibleActions = new ToolStripLabel();
            this.btnAddAction = new ToolStripButton();
            this.btnDeleteAction = new ToolStripButton();
            this.toolStripSeparator1 = new ToolStripSeparator();
            this.btnRenameAction = new ToolStripButton();
            this.toolStripSeparator2 = new ToolStripSeparator();
            this.btnIncreaseActionLevel = new ToolStripButton();
            this.btnDecreaseActionLevel = new ToolStripButton();
            this.toolStripSeparator3 = new ToolStripSeparator();
            this.btnMoveActionUp = new ToolStripButton();
            this.btnMoveActionDown = new ToolStripButton();
            this.toolStripSeparator4 = new ToolStripSeparator();
            this.btnActionParagraph = new ToolStripButton();
            this.toolStripLabel6 = new ToolStripLabel();
            this.toolStripLabel7 = new ToolStripLabel();
            this.btnSubscript = new ToolStripButton();
            this.btnSuperscript = new ToolStripButton();
            this.toolStripSeparator7 = new ToolStripSeparator();
            this.btnBullet = new ToolStripButton();
            this.toolStripSeparator5 = new ToolStripSeparator();
            this.btnLeftRightAlign = new ToolStripButton();
            this.btnRightAlign = new ToolStripButton();
            this.btnCenterAlign = new ToolStripButton();
            this.btnLeftAlign = new ToolStripButton();
            this.toolStripSeparator6 = new ToolStripSeparator();
            this.btnUnderline = new ToolStripButton();
            this.btnItalic = new ToolStripButton();
            this.btnBold = new ToolStripButton();
            this.btnText = new ToolStripButton();
            this.cbZoom = new ToolStripComboBox();
            this.cbFontSize = new ToolStripComboBox();
            this.cbFont = new ToolStripComboBox();
            this.mainSplitter = new SplitContainer();
            this.leftSplitter = new SplitContainer();
            this.treeCaseStage = new StagesTreeControl();
            this.leftBottomSplitter = new SplitContainer();
            this.personTypesToolStrip = new ToolStrip();
            this.toolStripLabel5 = new ToolStripLabel();
            this.btnInsertPersonTypeVariable = new ToolStripButton();
            this.toolStripSeparator8 = new ToolStripSeparator();
            this.btnDeletePersonType = new ToolStripButton();
            this.btnEditPersonType = new ToolStripButton();
            this.btnAddPersonType = new ToolStripButton();
            this.listPersonTypes = new ListView();
            this.ch = new ColumnHeader();
            this.gridSubsituations = new DataGridView();
            this.subsituationsToolStrip = new ToolStrip();
            this.toolStripLabel4 = new ToolStripLabel();
            this.btnInsertVariable = new ToolStripButton();
            this.rightSplitter = new SplitContainer();
            this.edtProtocol = new RichTextBoxEx();
            this.rightBottomSplitter = new SplitContainer();
            this.edtParagraph = new RichTextBoxEx();
            this.paragraphToolStrip = new ToolStrip();
            this.toolStripLabel1 = new ToolStripLabel();
            this.cbParagraphFormats = new ToolStripComboBox();
            this.edtFooter = new RichTextBoxEx();
            this.footerToolStrip = new ToolStrip();
            this.toolStripLabel3 = new ToolStripLabel();
            this.mainToolStrip.SuspendLayout();
            this.actionsToolStrip.SuspendLayout();
            this.mainSplitter.Panel1.SuspendLayout();
            this.mainSplitter.Panel2.SuspendLayout();
            this.mainSplitter.SuspendLayout();
            this.leftSplitter.Panel1.SuspendLayout();
            this.leftSplitter.Panel2.SuspendLayout();
            this.leftSplitter.SuspendLayout();
            this.leftBottomSplitter.Panel1.SuspendLayout();
            this.leftBottomSplitter.Panel2.SuspendLayout();
            this.leftBottomSplitter.SuspendLayout();
            this.personTypesToolStrip.SuspendLayout();
            ((ISupportInitialize) this.gridSubsituations).BeginInit();
            this.subsituationsToolStrip.SuspendLayout();
            this.rightSplitter.Panel1.SuspendLayout();
            this.rightSplitter.Panel2.SuspendLayout();
            this.rightSplitter.SuspendLayout();
            this.rightBottomSplitter.Panel1.SuspendLayout();
            this.rightBottomSplitter.Panel2.SuspendLayout();
            this.rightBottomSplitter.SuspendLayout();
            this.paragraphToolStrip.SuspendLayout();
            this.footerToolStrip.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this.mainToolStrip, "mainToolStrip");
            this.mainToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            this.mainToolStrip.Items.AddRange(new ToolStripItem[] { this.labelTemplateName, this.cbTemplates, this.btnNew, this.btnDelete, this.btnRename, this.btnSave });
            this.mainToolStrip.Name = "mainToolStrip";
            this.labelTemplateName.Name = "labelTemplateName";
            manager.ApplyResources(this.labelTemplateName, "labelTemplateName");
            manager.ApplyResources(this.cbTemplates, "cbTemplates");
            this.cbTemplates.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbTemplates.DropDownWidth = 640;
            this.cbTemplates.Name = "cbTemplates";
            this.cbTemplates.SelectedIndexChanged += new EventHandler(this.cbTemplatesSelectedIndexChanged);
            this.btnNew.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnNew.Image = Resources.new16_h;
            manager.ApplyResources(this.btnNew, "btnNew");
            this.btnNew.Name = "btnNew";
            this.btnNew.Click += new EventHandler(this.btnNewClick);
            this.btnDelete.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = Resources.delete16_h;
            manager.ApplyResources(this.btnDelete, "btnDelete");
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Click += new EventHandler(this.btnDeleteClick);
            this.btnRename.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnRename.Image = Resources.Rename;
            manager.ApplyResources(this.btnRename, "btnRename");
            this.btnRename.Name = "btnRename";
            this.btnRename.Click += new EventHandler(this.btnRenameClick);
            this.btnSave.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnSave.Image = Resources.save16_h;
            manager.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.Click += new EventHandler(this.btnSaveClick);
            manager.ApplyResources(this.actionsToolStrip, "actionsToolStrip");
            this.actionsToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            this.actionsToolStrip.Items.AddRange(new ToolStripItem[] { 
                this.labelPossibleActions, this.btnAddAction, this.btnDeleteAction, this.toolStripSeparator1, this.btnRenameAction, this.toolStripSeparator2, this.btnIncreaseActionLevel, this.btnDecreaseActionLevel, this.toolStripSeparator3, this.btnMoveActionUp, this.btnMoveActionDown, this.toolStripSeparator4, this.btnActionParagraph, this.toolStripLabel6, this.toolStripLabel7, this.btnSubscript, 
                this.btnSuperscript, this.toolStripSeparator7, this.btnBullet, this.toolStripSeparator5, this.btnLeftRightAlign, this.btnRightAlign, this.btnCenterAlign, this.btnLeftAlign, this.toolStripSeparator6, this.btnUnderline, this.btnItalic, this.btnBold, this.btnText, this.cbZoom, this.cbFontSize, this.cbFont
             });
            this.actionsToolStrip.Name = "actionsToolStrip";
            this.labelPossibleActions.Name = "labelPossibleActions";
            manager.ApplyResources(this.labelPossibleActions, "labelPossibleActions");
            this.btnAddAction.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnAddAction.Image = Resources.db_insert;
            manager.ApplyResources(this.btnAddAction, "btnAddAction");
            this.btnAddAction.Name = "btnAddAction";
            this.btnAddAction.Click += new EventHandler(this.btnAddActionClick);
            this.btnDeleteAction.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnDeleteAction.Image = Resources.db_delete;
            manager.ApplyResources(this.btnDeleteAction, "btnDeleteAction");
            this.btnDeleteAction.Name = "btnDeleteAction";
            this.btnDeleteAction.Click += new EventHandler(this.btnDeleteActionClick);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            manager.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            this.btnRenameAction.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnRenameAction.Image = Resources.rename16_h;
            manager.ApplyResources(this.btnRenameAction, "btnRenameAction");
            this.btnRenameAction.Name = "btnRenameAction";
            this.btnRenameAction.Click += new EventHandler(this.btnRenameActionClick);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            manager.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            this.btnIncreaseActionLevel.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnIncreaseActionLevel.Image = Resources.decrease_indent16_h;
            manager.ApplyResources(this.btnIncreaseActionLevel, "btnIncreaseActionLevel");
            this.btnIncreaseActionLevel.Name = "btnIncreaseActionLevel";
            this.btnIncreaseActionLevel.Click += new EventHandler(this.btnIncreaseActionLevelClick);
            this.btnDecreaseActionLevel.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnDecreaseActionLevel.Image = Resources.increase_indent16_h;
            manager.ApplyResources(this.btnDecreaseActionLevel, "btnDecreaseActionLevel");
            this.btnDecreaseActionLevel.Name = "btnDecreaseActionLevel";
            this.btnDecreaseActionLevel.Click += new EventHandler(this.btnDecreaseActionLevelClick);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            manager.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            this.btnMoveActionUp.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnMoveActionUp.Image = Resources.arrowup_blue16_h;
            manager.ApplyResources(this.btnMoveActionUp, "btnMoveActionUp");
            this.btnMoveActionUp.Name = "btnMoveActionUp";
            this.btnMoveActionUp.Click += new EventHandler(this.btnMoveActionUpClick);
            this.btnMoveActionDown.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnMoveActionDown.Image = Resources.arrowdown_blue16_h;
            manager.ApplyResources(this.btnMoveActionDown, "btnMoveActionDown");
            this.btnMoveActionDown.Name = "btnMoveActionDown";
            this.btnMoveActionDown.Click += new EventHandler(this.btnMoveActionDownClick);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            manager.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            this.btnActionParagraph.CheckOnClick = true;
            this.btnActionParagraph.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnActionParagraph.Image = Resources.paragraph_2;
            manager.ApplyResources(this.btnActionParagraph, "btnActionParagraph");
            this.btnActionParagraph.Name = "btnActionParagraph";
            this.btnActionParagraph.Click += new EventHandler(this.btnActionParagraphClick);
            this.toolStripLabel6.Name = "toolStripLabel6";
            manager.ApplyResources(this.toolStripLabel6, "toolStripLabel6");
            this.toolStripLabel7.Name = "toolStripLabel7";
            manager.ApplyResources(this.toolStripLabel7, "toolStripLabel7");
            this.btnSubscript.Alignment = ToolStripItemAlignment.Right;
            this.btnSubscript.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnSubscript.Image = Resources.subscript16_h;
            manager.ApplyResources(this.btnSubscript, "btnSubscript");
            this.btnSubscript.Name = "btnSubscript";
            this.btnSubscript.Click += new EventHandler(this.btnSubscriptClick);
            this.btnSuperscript.Alignment = ToolStripItemAlignment.Right;
            this.btnSuperscript.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnSuperscript.Image = Resources.superscript16_h;
            manager.ApplyResources(this.btnSuperscript, "btnSuperscript");
            this.btnSuperscript.Name = "btnSuperscript";
            this.btnSuperscript.Click += new EventHandler(this.btnSuperscriptClick);
            this.toolStripSeparator7.Alignment = ToolStripItemAlignment.Right;
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            manager.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
            this.btnBullet.Alignment = ToolStripItemAlignment.Right;
            this.btnBullet.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnBullet.Image = Resources.bullets16_h;
            manager.ApplyResources(this.btnBullet, "btnBullet");
            this.btnBullet.Name = "btnBullet";
            this.btnBullet.Click += new EventHandler(this.btnBulletClick);
            this.toolStripSeparator5.Alignment = ToolStripItemAlignment.Right;
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            manager.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            this.btnLeftRightAlign.Alignment = ToolStripItemAlignment.Right;
            this.btnLeftRightAlign.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnLeftRightAlign.Image = Resources.align_justify16_h;
            manager.ApplyResources(this.btnLeftRightAlign, "btnLeftRightAlign");
            this.btnLeftRightAlign.Name = "btnLeftRightAlign";
            this.btnLeftRightAlign.Click += new EventHandler(this.btnLeftRightAlignClick);
            this.btnRightAlign.Alignment = ToolStripItemAlignment.Right;
            this.btnRightAlign.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnRightAlign.Image = Resources.align_right16_h;
            manager.ApplyResources(this.btnRightAlign, "btnRightAlign");
            this.btnRightAlign.Name = "btnRightAlign";
            this.btnRightAlign.Click += new EventHandler(this.btnRightAlignClick);
            this.btnCenterAlign.Alignment = ToolStripItemAlignment.Right;
            this.btnCenterAlign.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnCenterAlign.Image = Resources.align_centre16_h;
            manager.ApplyResources(this.btnCenterAlign, "btnCenterAlign");
            this.btnCenterAlign.Name = "btnCenterAlign";
            this.btnCenterAlign.Click += new EventHandler(this.btnCenterAlignClick);
            this.btnLeftAlign.Alignment = ToolStripItemAlignment.Right;
            this.btnLeftAlign.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnLeftAlign.Image = Resources.align_left16_h;
            manager.ApplyResources(this.btnLeftAlign, "btnLeftAlign");
            this.btnLeftAlign.Name = "btnLeftAlign";
            this.btnLeftAlign.Click += new EventHandler(this.btnLeftAlignClick);
            this.toolStripSeparator6.Alignment = ToolStripItemAlignment.Right;
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            manager.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
            this.btnUnderline.Alignment = ToolStripItemAlignment.Right;
            this.btnUnderline.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnUnderline.Image = Resources.underline_blue16_h;
            manager.ApplyResources(this.btnUnderline, "btnUnderline");
            this.btnUnderline.Name = "btnUnderline";
            this.btnUnderline.Click += new EventHandler(this.btnUnderlineClick);
            this.btnItalic.Alignment = ToolStripItemAlignment.Right;
            this.btnItalic.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnItalic.Image = Resources.italic_blue16_h;
            manager.ApplyResources(this.btnItalic, "btnItalic");
            this.btnItalic.Name = "btnItalic";
            this.btnItalic.Click += new EventHandler(this.btnItalicClick);
            this.btnBold.Alignment = ToolStripItemAlignment.Right;
            this.btnBold.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnBold.Image = Resources.bold_blue16_h;
            manager.ApplyResources(this.btnBold, "btnBold");
            this.btnBold.Name = "btnBold";
            this.btnBold.Click += new EventHandler(this.btnBoldClick);
            this.btnText.Alignment = ToolStripItemAlignment.Right;
            this.btnText.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnText.Image = Resources.select_alltext16_h;
            manager.ApplyResources(this.btnText, "btnText");
            this.btnText.Name = "btnText";
            this.btnText.Click += new EventHandler(this.btnTextClick);
            this.cbZoom.Alignment = ToolStripItemAlignment.Right;
            manager.ApplyResources(this.cbZoom, "cbZoom");
            this.cbZoom.Items.AddRange(new object[] { manager.GetString("cbZoom.Items"), manager.GetString("cbZoom.Items1"), manager.GetString("cbZoom.Items2"), manager.GetString("cbZoom.Items3"), manager.GetString("cbZoom.Items4"), manager.GetString("cbZoom.Items5"), manager.GetString("cbZoom.Items6"), manager.GetString("cbZoom.Items7"), manager.GetString("cbZoom.Items8"), manager.GetString("cbZoom.Items9"), manager.GetString("cbZoom.Items10"), manager.GetString("cbZoom.Items11") });
            this.cbZoom.Name = "cbZoom";
            this.cbZoom.SelectedIndexChanged += new EventHandler(this.cbZoomSelectedIndexChanged);
            this.cbZoom.KeyDown += new KeyEventHandler(this.cbZoomKeyDown);
            this.cbFontSize.Alignment = ToolStripItemAlignment.Right;
            manager.ApplyResources(this.cbFontSize, "cbFontSize");
            this.cbFontSize.Items.AddRange(new object[] { manager.GetString("cbFontSize.Items"), manager.GetString("cbFontSize.Items1"), manager.GetString("cbFontSize.Items2"), manager.GetString("cbFontSize.Items3"), manager.GetString("cbFontSize.Items4"), manager.GetString("cbFontSize.Items5"), manager.GetString("cbFontSize.Items6"), manager.GetString("cbFontSize.Items7"), manager.GetString("cbFontSize.Items8"), manager.GetString("cbFontSize.Items9"), manager.GetString("cbFontSize.Items10"), manager.GetString("cbFontSize.Items11"), manager.GetString("cbFontSize.Items12"), manager.GetString("cbFontSize.Items13"), manager.GetString("cbFontSize.Items14"), manager.GetString("cbFontSize.Items15") });
            this.cbFontSize.Name = "cbFontSize";
            this.cbFontSize.SelectedIndexChanged += new EventHandler(this.cbFontSizeSelectedIndexChanged);
            this.cbFontSize.KeyDown += new KeyEventHandler(this.cbFontSizeKeyDown);
            this.cbFont.Alignment = ToolStripItemAlignment.Right;
            this.cbFont.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbFont.DropDownWidth = 200;
            this.cbFont.Name = "cbFont";
            manager.ApplyResources(this.cbFont, "cbFont");
            this.cbFont.Sorted = true;
            this.cbFont.SelectedIndexChanged += new EventHandler(this.cbFontSelectedIndexChanged);
            this.cbFont.KeyDown += new KeyEventHandler(this.cbFontKeyDown);
            manager.ApplyResources(this.mainSplitter, "mainSplitter");
            this.mainSplitter.Name = "mainSplitter";
            this.mainSplitter.Panel1.Controls.Add(this.leftSplitter);
            this.mainSplitter.Panel2.Controls.Add(this.rightSplitter);
            manager.ApplyResources(this.leftSplitter, "leftSplitter");
            this.leftSplitter.Name = "leftSplitter";
            this.leftSplitter.Panel1.Controls.Add(this.treeCaseStage);
            this.leftSplitter.Panel2.Controls.Add(this.leftBottomSplitter);
            manager.ApplyResources(this.treeCaseStage, "treeCaseStage");
            this.treeCaseStage.HideSelection = false;
            this.treeCaseStage.LabelEdit = true;
            this.treeCaseStage.Name = "treeCaseStage";
            this.treeCaseStage.AfterLabelEdit += new NodeLabelEditEventHandler(this.treeCaseStageAfterLabelEdit);
            this.treeCaseStage.AfterSelect += new TreeViewEventHandler(this.treeCaseStageAfterSelect);
            this.treeCaseStage.OnGetDefaultParagraphNameEventHanlder += new StagesTreeControl.GetDefaultParagraphNameEventHanlder(this.OnGetDefaultParagraphNameEventHanlder);
            this.treeCaseStage.BeforeSelect += new TreeViewCancelEventHandler(this.treeCaseStageBeforeSelect);
            manager.ApplyResources(this.leftBottomSplitter, "leftBottomSplitter");
            this.leftBottomSplitter.Name = "leftBottomSplitter";
            this.leftBottomSplitter.Panel1.Controls.Add(this.personTypesToolStrip);
            this.leftBottomSplitter.Panel1.Controls.Add(this.listPersonTypes);
            this.leftBottomSplitter.Panel2.Controls.Add(this.gridSubsituations);
            this.leftBottomSplitter.Panel2.Controls.Add(this.subsituationsToolStrip);
            manager.ApplyResources(this.personTypesToolStrip, "personTypesToolStrip");
            this.personTypesToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            this.personTypesToolStrip.Items.AddRange(new ToolStripItem[] { this.toolStripLabel5, this.btnInsertPersonTypeVariable, this.toolStripSeparator8, this.btnDeletePersonType, this.btnEditPersonType, this.btnAddPersonType });
            this.personTypesToolStrip.Name = "personTypesToolStrip";
            this.toolStripLabel5.Name = "toolStripLabel5";
            manager.ApplyResources(this.toolStripLabel5, "toolStripLabel5");
            this.btnInsertPersonTypeVariable.Alignment = ToolStripItemAlignment.Right;
            this.btnInsertPersonTypeVariable.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnInsertPersonTypeVariable.Image = Resources.redo_square16_h;
            manager.ApplyResources(this.btnInsertPersonTypeVariable, "btnInsertPersonTypeVariable");
            this.btnInsertPersonTypeVariable.Name = "btnInsertPersonTypeVariable";
            this.btnInsertPersonTypeVariable.Click += new EventHandler(this.btnInsertPersonTypeVariableClick);
            this.toolStripSeparator8.Alignment = ToolStripItemAlignment.Right;
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            manager.ApplyResources(this.toolStripSeparator8, "toolStripSeparator8");
            this.btnDeletePersonType.Alignment = ToolStripItemAlignment.Right;
            this.btnDeletePersonType.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnDeletePersonType.Image = Resources.user_drop16_h;
            manager.ApplyResources(this.btnDeletePersonType, "btnDeletePersonType");
            this.btnDeletePersonType.Name = "btnDeletePersonType";
            this.btnDeletePersonType.Click += new EventHandler(this.btnDeletePersonTypeClick);
            this.btnEditPersonType.Alignment = ToolStripItemAlignment.Right;
            this.btnEditPersonType.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnEditPersonType.Image = Resources.rename16_h;
            manager.ApplyResources(this.btnEditPersonType, "btnEditPersonType");
            this.btnEditPersonType.Name = "btnEditPersonType";
            this.btnEditPersonType.Click += new EventHandler(this.btnEditPersonTypeClick);
            this.btnAddPersonType.Alignment = ToolStripItemAlignment.Right;
            this.btnAddPersonType.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnAddPersonType.Image = Resources.user_add16_h1;
            manager.ApplyResources(this.btnAddPersonType, "btnAddPersonType");
            this.btnAddPersonType.Name = "btnAddPersonType";
            this.btnAddPersonType.Click += new EventHandler(this.btnAddPersonTypeClick);
            manager.ApplyResources(this.listPersonTypes, "listPersonTypes");
            this.listPersonTypes.Columns.AddRange(new ColumnHeader[] { this.ch });
            this.listPersonTypes.FullRowSelect = true;
            this.listPersonTypes.GridLines = true;
            this.listPersonTypes.HideSelection = false;
            this.listPersonTypes.MultiSelect = false;
            this.listPersonTypes.Name = "listPersonTypes";
            this.listPersonTypes.ShowGroups = false;
            this.listPersonTypes.UseCompatibleStateImageBehavior = false;
            this.listPersonTypes.View = View.Details;
            this.listPersonTypes.ItemActivate += new EventHandler(this.btnInsertPersonTypeVariableClick);
            this.listPersonTypes.Resize += new EventHandler(this.gridPersonTypesResize);
            manager.ApplyResources(this.ch, "ch");
            this.gridSubsituations.AllowUserToAddRows = false;
            this.gridSubsituations.AllowUserToDeleteRows = false;
            this.gridSubsituations.AllowUserToResizeColumns = false;
            this.gridSubsituations.AllowUserToResizeRows = false;
            this.gridSubsituations.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.gridSubsituations.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.gridSubsituations.BackgroundColor = SystemColors.Window;
            this.gridSubsituations.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSubsituations.ColumnHeadersVisible = false;
            manager.ApplyResources(this.gridSubsituations, "gridSubsituations");
            this.gridSubsituations.MultiSelect = false;
            this.gridSubsituations.Name = "gridSubsituations";
            this.gridSubsituations.ReadOnly = true;
            this.gridSubsituations.RowHeadersVisible = false;
            this.gridSubsituations.CellDoubleClick += new DataGridViewCellEventHandler(this.gridSubsituationsCellDoubleClick);
            manager.ApplyResources(this.subsituationsToolStrip, "subsituationsToolStrip");
            this.subsituationsToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            this.subsituationsToolStrip.Items.AddRange(new ToolStripItem[] { this.toolStripLabel4, this.btnInsertVariable });
            this.subsituationsToolStrip.Name = "subsituationsToolStrip";
            this.toolStripLabel4.Name = "toolStripLabel4";
            manager.ApplyResources(this.toolStripLabel4, "toolStripLabel4");
            this.btnInsertVariable.Alignment = ToolStripItemAlignment.Right;
            this.btnInsertVariable.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnInsertVariable.Image = Resources.redo_square16_h;
            manager.ApplyResources(this.btnInsertVariable, "btnInsertVariable");
            this.btnInsertVariable.Name = "btnInsertVariable";
            this.btnInsertVariable.Click += new EventHandler(this.btnInsertVariableClick);
            manager.ApplyResources(this.rightSplitter, "rightSplitter");
            this.rightSplitter.Name = "rightSplitter";
            this.rightSplitter.Panel1.Controls.Add(this.edtProtocol);
            this.rightSplitter.Panel2.Controls.Add(this.rightBottomSplitter);
            manager.ApplyResources(this.edtProtocol, "edtProtocol");
            this.edtProtocol.HideSelection = false;
            this.edtProtocol.Name = "edtProtocol";
            this.edtProtocol.SelectionAlignment = RichTextBoxEx.TextAlign.Left;
            this.edtProtocol.SelectionSubScript = false;
            this.edtProtocol.SelectionSuperScript = false;
            this.edtProtocol.SelectionChanged += new EventHandler(this.edtSelectionChanged);
            this.edtProtocol.Protected += new EventHandler(this.edtProtected);
            this.edtProtocol.Enter += new EventHandler(this.edtControlsEnter);
            this.edtProtocol.Leave += new EventHandler(this.edtLeave);
            this.edtProtocol.TextChanged += new EventHandler(this.edtTextChanged);
            manager.ApplyResources(this.rightBottomSplitter, "rightBottomSplitter");
            this.rightBottomSplitter.Name = "rightBottomSplitter";
            this.rightBottomSplitter.Panel1.Controls.Add(this.edtParagraph);
            this.rightBottomSplitter.Panel1.Controls.Add(this.paragraphToolStrip);
            this.rightBottomSplitter.Panel2.Controls.Add(this.edtFooter);
            this.rightBottomSplitter.Panel2.Controls.Add(this.footerToolStrip);
            manager.ApplyResources(this.edtParagraph, "edtParagraph");
            this.edtParagraph.HideSelection = false;
            this.edtParagraph.Name = "edtParagraph";
            this.edtParagraph.SelectionAlignment = RichTextBoxEx.TextAlign.Left;
            this.edtParagraph.SelectionSubScript = false;
            this.edtParagraph.SelectionSuperScript = false;
            this.edtParagraph.SelectionChanged += new EventHandler(this.edtSelectionChanged);
            this.edtParagraph.Protected += new EventHandler(this.edtProtected);
            this.edtParagraph.Enter += new EventHandler(this.edtControlsEnter);
            this.edtParagraph.Leave += new EventHandler(this.edtLeave);
            manager.ApplyResources(this.paragraphToolStrip, "paragraphToolStrip");
            this.paragraphToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            this.paragraphToolStrip.Items.AddRange(new ToolStripItem[] { this.toolStripLabel1, this.cbParagraphFormats });
            this.paragraphToolStrip.Name = "paragraphToolStrip";
            this.toolStripLabel1.Name = "toolStripLabel1";
            manager.ApplyResources(this.toolStripLabel1, "toolStripLabel1");
            this.cbParagraphFormats.Alignment = ToolStripItemAlignment.Right;
            manager.ApplyResources(this.cbParagraphFormats, "cbParagraphFormats");
            this.cbParagraphFormats.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbParagraphFormats.Name = "cbParagraphFormats";
            this.cbParagraphFormats.SelectedIndexChanged += new EventHandler(this.cbParagraphFormatsSelectedIndexChanged);
            manager.ApplyResources(this.edtFooter, "edtFooter");
            this.edtFooter.HideSelection = false;
            this.edtFooter.Name = "edtFooter";
            this.edtFooter.SelectionAlignment = RichTextBoxEx.TextAlign.Left;
            this.edtFooter.SelectionSubScript = false;
            this.edtFooter.SelectionSuperScript = false;
            this.edtFooter.SelectionChanged += new EventHandler(this.edtSelectionChanged);
            this.edtFooter.Protected += new EventHandler(this.edtProtected);
            this.edtFooter.Enter += new EventHandler(this.edtControlsEnter);
            this.edtFooter.Leave += new EventHandler(this.edtLeave);
            manager.ApplyResources(this.footerToolStrip, "footerToolStrip");
            this.footerToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            this.footerToolStrip.Items.AddRange(new ToolStripItem[] { this.toolStripLabel3 });
            this.footerToolStrip.Name = "footerToolStrip";
            this.toolStripLabel3.Name = "toolStripLabel3";
            manager.ApplyResources(this.toolStripLabel3, "toolStripLabel3");
            base.AutoScaleMode = AutoScaleMode.Inherit;
            manager.ApplyResources(this, "$this");
            base.Controls.Add(this.mainSplitter);
            base.Controls.Add(this.actionsToolStrip);
            base.Controls.Add(this.mainToolStrip);
            base.MinimizeBox = false;
            base.Name = "TemplatesDialog";
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.actionsToolStrip.ResumeLayout(false);
            this.actionsToolStrip.PerformLayout();
            this.mainSplitter.Panel1.ResumeLayout(false);
            this.mainSplitter.Panel2.ResumeLayout(false);
            this.mainSplitter.ResumeLayout(false);
            this.leftSplitter.Panel1.ResumeLayout(false);
            this.leftSplitter.Panel2.ResumeLayout(false);
            this.leftSplitter.ResumeLayout(false);
            this.leftBottomSplitter.Panel1.ResumeLayout(false);
            this.leftBottomSplitter.Panel2.ResumeLayout(false);
            this.leftBottomSplitter.ResumeLayout(false);
            this.personTypesToolStrip.ResumeLayout(false);
            this.personTypesToolStrip.PerformLayout();
            ((ISupportInitialize) this.gridSubsituations).EndInit();
            this.subsituationsToolStrip.ResumeLayout(false);
            this.subsituationsToolStrip.PerformLayout();
            this.rightSplitter.Panel1.ResumeLayout(false);
            this.rightSplitter.Panel2.ResumeLayout(false);
            this.rightSplitter.ResumeLayout(false);
            this.rightBottomSplitter.Panel1.ResumeLayout(false);
            this.rightBottomSplitter.Panel2.ResumeLayout(false);
            this.rightBottomSplitter.ResumeLayout(false);
            this.paragraphToolStrip.ResumeLayout(false);
            this.paragraphToolStrip.PerformLayout();
            this.footerToolStrip.ResumeLayout(false);
            this.footerToolStrip.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected void LoadFromTemplate(Template template)
        {
            if (template != null)
            {
                this.actionsToolStrip.Enabled = true;
                this.personTypesToolStrip.Enabled = true;
                this.paragraphToolStrip.Enabled = true;
                this.subsituationsToolStrip.Enabled = true;
                this.footerToolStrip.Enabled = true;
                this.treeCaseStage.Enabled = true;
                this.edtFooter.Enabled = true;
                this.edtParagraph.Enabled = true;
                this.edtProtocol.Enabled = true;
                this.listPersonTypes.Enabled = true;
                this.gridSubsituations.Enabled = true;
            }
            else
            {
                this.actionsToolStrip.Enabled = false;
                this.personTypesToolStrip.Enabled = false;
                this.paragraphToolStrip.Enabled = false;
                this.subsituationsToolStrip.Enabled = false;
                this.footerToolStrip.Enabled = false;
                this.treeCaseStage.Enabled = false;
                this.edtFooter.Enabled = false;
                this.edtParagraph.Enabled = false;
                this.edtProtocol.Enabled = false;
                this.listPersonTypes.Enabled = false;
                this.gridSubsituations.Enabled = false;
            }
            this.treeCaseStage.Nodes.Clear();
            this.edtFooter.Clear();
            this.edtFooter.ClearUndo();
            this.edtParagraph.Clear();
            this.edtParagraph.ClearUndo();
            this.edtProtocol.Clear();
            this.edtProtocol.ClearUndo();
            if (template != null)
            {
                this.treeCaseStage.BuildNodesFromDataTable(template.CaseStages);
                this.listPersonTypes.BeginUpdate();
                try
                {
                    this.listPersonTypes.Items.Clear();
                    foreach (string str in template.PersonTypes)
                    {
                        this.listPersonTypes.Items.Add(str);
                    }
                }
                finally
                {
                    this.listPersonTypes.EndUpdate();
                }
                if (this.lastActiveRichTextBox == this.edtParagraph)
                {
                    TemplatesManager.Subsituations.DefaultView.RowFilter = "TYPE_NAME='PARAGRAPH'";
                }
                else
                {
                    TemplatesManager.Subsituations.DefaultView.RowFilter = "TYPE_NAME='PROTOCOL'";
                }
                this.gridSubsituations.DataSource = TemplatesManager.Subsituations.DefaultView;
                if (this.gridSubsituations.Columns.Contains("TYPE_NAME"))
                {
                    this.gridSubsituations.Columns.Remove("TYPE_NAME");
                }
                if (this.gridSubsituations.Columns.Contains("SYS_NAME"))
                {
                    this.gridSubsituations.Columns.Remove("SYS_NAME");
                }
                this.lockSelectedParagraphChange++;
                try
                {
                    this.cbParagraphFormats.Items.Clear();
                    this.cbParagraphFormats.Items.AddRange(this.currentTemplate.Paragraphs.ToArray());
                }
                finally
                {
                    this.lockSelectedParagraphChange--;
                }
                if (this.treeCaseStage.SelectedNode != null)
                {
                    if (this.treeCaseStage.SelectedNode.Tag == null)
                    {
                        this.cbParagraphFormats.SelectedItem = template.GetParagraphByName(this.OnGetDefaultParagraphNameEventHanlder(this.treeCaseStage.SelectedNode.Level));
                    }
                    else
                    {
                        this.cbParagraphFormats.SelectedItem = template.GetParagraphByName((string) this.treeCaseStage.SelectedNode.Tag);
                    }
                }
                else
                {
                    this.cbParagraphFormats.SelectedIndex = 0;
                }
                this.lockEdtTextChanged++;
                try
                {
                    this.edtProtocol.Rtf = template.ProtocolHeader;
                    this.edtFooter.Rtf = template.ProtocolFooter;
                }
                finally
                {
                    this.lockEdtTextChanged--;
                }
            }
            else
            {
                this.listPersonTypes.Clear();
                this.gridSubsituations.DataSource = null;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!e.Cancel)
            {
                switch (this.QuerySave())
                {
                    case DialogResult.Yes:
                        this.SaveTemplate();
                        e.Cancel = false;
                        break;

                    case DialogResult.No:
                        e.Cancel = false;
                        break;

                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }

        private string OnGetDefaultParagraphNameEventHanlder(int level)
        {
            if (level == 0)
            {
                return Resources.ParagraphStage;
            }
            return Resources.ParagraphActionPersonNote;
        }

        protected override void OnLoad(EventArgs e)
        {
            this.lockSelectedTemplateChange++;
            try
            {
                base.OnLoad(e);
                foreach (Template template in MainForm.CaseDocument.Templates)
                {
                    if (this.activeTemplateName.EndsWith(" *"))
                    {
                        this.cbTemplates.Items.Add(template.Name);
                    }
                    else
                    {
                        this.cbTemplates.Items.Add(template.Name + " *");
                    }
                }
                foreach (Template template in TemplatesManager.Templates)
                {
                    this.cbTemplates.Items.Add(template.Name);
                }
                if (!string.IsNullOrEmpty(this.activeTemplateName))
                {
                    if (this.activeTemplateName.EndsWith(" *"))
                    {
                        Template template2 = MainForm.CaseDocument.Templates.FindByName(this.activeTemplateName);
                        this.currentTemplate = (Template) template2.Clone();
                    }
                    else
                    {
                        this.currentTemplate = (Template) TemplatesManager.GetTemplateByName(this.activeTemplateName).Clone();
                    }
                }
                else if (TemplatesManager.Templates.Count > 0)
                {
                    this.currentTemplate = (Template) TemplatesManager.Templates[0].Clone();
                }
                else
                {
                    this.currentTemplate = null;
                }
                if (this.currentTemplate == null)
                {
                    this.btnNewClick(null, null);
                }
                else
                {
                    this.cbTemplates.Text = this.currentTemplate.Name;
                    this.LoadFromTemplate(this.currentTemplate);
                    this.currentTemplate.Modified = false;
                    this.cbTemplates.ToolTipText = "სხდომის ტიპი: " + this.cbTemplates.Text;
                }
                this.lastActiveRichTextBox = this.edtProtocol;
                this.lastActiveRichTextBox.BackColor = Color.FromArgb(-1052689);
                foreach (FontFamily family in FontFamily.Families)
                {
                    this.cbFont.Items.Add(family.Name);
                }
            }
            finally
            {
                this.lockSelectedTemplateChange--;
            }
        }

        private static string PreferTemplateName(string prefferedName)
        {
            int num = 0;
            bool flag = false;
            while (!flag)
            {
                flag = true;
                foreach (Template template in TemplatesManager.Templates)
                {
                    if (template.Name == (prefferedName + ((num == 0) ? "" : (" (ეგზემპლარი " + num.ToString() + ")"))))
                    {
                        flag = false;
                        num++;
                        break;
                    }
                }
            }
            return (prefferedName + ((num == 0) ? "" : (" (ეგზემპლარი " + num.ToString() + ")")));
        }

        private DialogResult QuerySave()
        {
            if ((this.currentTemplate != null) && this.currentTemplate.Modified)
            {
                return MessageBox.Show(this, "სხდომის ტიპმა განიცადა ცვლილება. გსურთ შეინახოთ ცვლილება?", "შეტყობინება", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3);
            }
            return DialogResult.OK;
        }

        public bool SaveTemplate()
        {
            this.treeCaseStage.BuildStagesFromNodes(this.currentTemplate.CaseStages);
            this.edtLeave(this.edtParagraph, null);
            if (!this.currentTemplate.Name.EndsWith(" *"))
            {
                TemplatesManager.DeleteTemplate(this.currentTemplate.Name);
                string fileName = TemplatesManager.TemplatesPath + this.currentTemplate.Name + ".clpt";
                this.currentTemplate.SaveToFile(fileName);
                TemplatesManager.AddTemplate(this.currentTemplate, true);
                this.currentTemplate.Modified = false;
            }
            else
            {
                int num = MainForm.CaseDocument.Templates.FindIndexOfName(this.activeTemplateNameBeforeRename);
                if (num > -1)
                {
                    bool flag = this.currentTemplate.Name != this.activeTemplateNameBeforeRename;
                    while (flag)
                    {
                        flag = false;
                        foreach (KeyValuePair<DateTime, string> pair in MainForm.CaseDocument.TrialTemplates)
                        {
                            if (pair.Value == this.activeTemplateNameBeforeRename)
                            {
                                MainForm.CaseDocument.TrialTemplates[pair.Key] = this.currentTemplate.Name;
                                flag = true;
                                break;
                            }
                        }
                    }
                    MainForm.CaseDocument.Templates[num] = this.currentTemplate;
                }
                this.activeTemplateNameBeforeRename = this.currentTemplate.Name;
                this.currentTemplate = (Template) this.currentTemplate.Clone();
                this.currentTemplate.Modified = false;
            }
            return true;
        }

        private void treeCaseStageAfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            this.currentTemplate.Modified = true;
        }

        private void treeCaseStageAfterSelect(object sender, TreeViewEventArgs e)
        {
            if (((e.Action == TreeViewAction.ByKeyboard) || (e.Action == TreeViewAction.ByMouse)) || (e.Action == TreeViewAction.Unknown))
            {
                ParagraphData paragraphByName = null;
                if (e.Node.Tag == null)
                {
                    paragraphByName = this.currentTemplate.GetParagraphByName(this.OnGetDefaultParagraphNameEventHanlder(e.Node.Level));
                }
                else
                {
                    paragraphByName = this.currentTemplate.GetParagraphByName((string) e.Node.Tag);
                }
                this.cbParagraphFormats.SelectedIndex = this.cbParagraphFormats.Items.IndexOf(paragraphByName);
                this.btnActionParagraph.Checked = ParagraphData.IsCustom(this.cbParagraphFormats.Text);
            }
        }

        private void treeCaseStageBeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (((this.treeCaseStage.SelectedNode != null) && (((e.Action == TreeViewAction.ByKeyboard) || (e.Action == TreeViewAction.ByMouse)) || (e.Action == TreeViewAction.Unknown))) && (this.cbParagraphFormats.SelectedItem != null))
            {
                ((ParagraphData) this.cbParagraphFormats.SelectedItem).Data = this.edtParagraph.Rtf;
            }
        }
    }
}

