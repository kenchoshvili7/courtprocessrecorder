﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using CourtRecorderCommon;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class ImportPersonsDialog : BaseForm
    {
        private Button bCancel;
        private Button bOK;
        private ComboBox cbTrialDates;
        private IContainer components = null;
        private CaseDocument document;
        private Label label1;
        private ListBox lbSelectedPersons;

        public ImportPersonsDialog(CaseDocument document)
        {
            this.document = document;
            this.InitializeComponent();
        }

        private void cbTrialDatesSelectedIndexChanged(object sender, EventArgs e)
        {
            this.lbSelectedPersons.BeginUpdate();
            try
            {
                this.lbSelectedPersons.Items.Clear();
                if (this.document.TrialPersons.ContainsKey((DateTime) this.cbTrialDates.SelectedItem))
                {
                    foreach (Person person in this.document.TrialPersons[(DateTime) this.cbTrialDates.SelectedItem])
                    {
                        this.lbSelectedPersons.Items.Add(person);
                        this.lbSelectedPersons.SelectedItems.Add(person);
                    }
                }
            }
            finally
            {
                this.lbSelectedPersons.EndUpdate();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.bOK = new Button();
            this.bCancel = new Button();
            this.lbSelectedPersons = new ListBox();
            this.label1 = new Label();
            this.cbTrialDates = new ComboBox();
            base.SuspendLayout();
            this.bOK.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.bOK.DialogResult = DialogResult.OK;
            this.bOK.Location = new Point(0xfe, 0x114);
            this.bOK.Name = "bOK";
            this.bOK.Size = new Size(0x4b, 0x17);
            this.bOK.TabIndex = 0;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bCancel.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.bCancel.DialogResult = DialogResult.Cancel;
            this.bCancel.Location = new Point(0x14f, 0x114);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new Size(0x4b, 0x17);
            this.bCancel.TabIndex = 1;
            this.bCancel.Text = "გაუქმება";
            this.bCancel.UseVisualStyleBackColor = true;
            this.lbSelectedPersons.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.lbSelectedPersons.ItemHeight = 0x10;
            this.lbSelectedPersons.Location = new Point(12, 0x2c);
            this.lbSelectedPersons.Name = "lbSelectedPersons";
            this.lbSelectedPersons.SelectionMode = SelectionMode.MultiSimple;
            this.lbSelectedPersons.Size = new Size(0x18e, 0xd4);
            this.lbSelectedPersons.Sorted = true;
            this.lbSelectedPersons.TabIndex = 2;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x6c, 0x10);
            this.label1.TabIndex = 3;
            this.label1.Text = "სხდომის თარიღი";
            this.cbTrialDates.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.cbTrialDates.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbTrialDates.FormatString = "dd MMM yyyy";
            this.cbTrialDates.FormattingEnabled = true;
            this.cbTrialDates.Location = new Point(0x7e, 12);
            this.cbTrialDates.Name = "cbTrialDates";
            this.cbTrialDates.Size = new Size(0x11c, 0x18);
            this.cbTrialDates.TabIndex = 4;
            this.cbTrialDates.SelectedIndexChanged += new EventHandler(this.cbTrialDatesSelectedIndexChanged);
            base.AcceptButton = this.bOK;
            base.AutoScaleDimensions = new SizeF(7f, 16f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this.bCancel;
            base.ClientSize = new Size(0x1a6, 0x137);
            base.Controls.Add(this.cbTrialDates);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.lbSelectedPersons);
            base.Controls.Add(this.bCancel);
            base.Controls.Add(this.bOK);
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ImportPersonsDialog";
            this.Text = "მონაწილეების იმპორტი";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            foreach (DateTime time in this.document._TrialDates)
            {
                this.cbTrialDates.Items.Add(time);
            }
            this.cbTrialDates.SelectedItem = this.document.TrialDates[this.document.TrialDates.Length - 1];
        }

        public Person[] Persons
        {
            get
            {
                List<Person> list = new List<Person>();
                foreach (Person person in this.lbSelectedPersons.SelectedItems)
                {
                    list.Add(person);
                }
                return list.ToArray();
            }
        }
    }
}

