﻿namespace CourtProcessRecorder
{
    using AudioConfiguration;
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class ConfigPlaybackDeviceDialog : BaseForm
    {
        private Button btnCancel;
        private Button btnOK;
        private ComboBox cbDevices;
        private IContainer components = null;
        private Label label1;

        public ConfigPlaybackDeviceDialog()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.btnOK = new Button();
            this.btnCancel = new Button();
            this.label1 = new Label();
            this.cbDevices = new ComboBox();
            base.SuspendLayout();
            this.btnOK.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnOK.DialogResult = DialogResult.OK;
            this.btnOK.Enabled = false;
            this.btnOK.Font = new Font("Microsoft Sans Serif", 8.25f);
            this.btnOK.Location = new Point(0x123, 0x42);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new Size(0x4b, 0x17);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Font = new Font("Microsoft Sans Serif", 8.25f);
            this.btnCancel.Location = new Point(0x174, 0x42);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x4b, 0x17);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "გაუქმება";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x58, 0x10);
            this.label1.TabIndex = 2;
            this.label1.Text = "მოწყობილობა";
            this.cbDevices.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.cbDevices.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbDevices.FormattingEnabled = true;
            this.cbDevices.Location = new Point(12, 0x1c);
            this.cbDevices.Name = "cbDevices";
            this.cbDevices.Size = new Size(0x1b3, 0x18);
            this.cbDevices.TabIndex = 3;
            base.AcceptButton = this.btnOK;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x1cb, 0x65);
            base.Controls.Add(this.cbDevices);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.btnOK);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ConfigPlaybackDeviceDialog";
            this.Text = "სასმენი მოწყობილობის არჩევა";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (base.DialogResult == DialogResult.OK)
            {
                AudioIO.SetPlaybackDevice((DeviceInfo) this.cbDevices.SelectedItem);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.cbDevices.Items.AddRange(AudioIO.PlaybackDevices);
            if (this.cbDevices.Items.Count > 0)
            {
                this.cbDevices.SelectedIndex = this.cbDevices.Items.IndexOf(AudioIO.GetPlaybackDevice());
                this.btnOK.Enabled = true;
            }
        }
    }
}

