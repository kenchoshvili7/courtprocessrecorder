﻿namespace CourtProcessRecorder
{
    using System;

    public enum StartupAction
    {
        None,
        NewCaseDocument,
        OpenCaseDocument,
        OpenTodayCaseDocument
    }
}

