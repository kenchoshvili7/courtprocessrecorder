﻿namespace CourtProcessRecorder
{
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class StartupForm : BaseForm
    {
        private StartupAction action;
        private Button btnContinueTrial;
        private Button btnExit;
        private Button btnNewTrial;
        private Button btnOpenTrial;
        private IContainer components = null;

        public StartupForm()
        {
            if (!MainForm.MainFormInstance.Visible)
            {
                base.Icon = MainForm.MainFormInstance.Icon;
                base.ShowInTaskbar = true;
            }
            this.InitializeComponent();
            this.action = StartupAction.NewCaseDocument;
        }

        private void btnContinueTrial_Click(object sender, EventArgs e)
        {
            this.action = StartupAction.OpenTodayCaseDocument;
        }

        private void btnNewTrial_Click(object sender, EventArgs e)
        {
            this.action = StartupAction.NewCaseDocument;
        }

        private void btnOpenTrial_Click(object sender, EventArgs e)
        {
            this.action = StartupAction.OpenCaseDocument;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(StartupForm));
            this.btnNewTrial = new Button();
            this.btnOpenTrial = new Button();
            this.btnContinueTrial = new Button();
            this.btnExit = new Button();
            base.SuspendLayout();
            this.btnNewTrial.DialogResult = DialogResult.OK;
            manager.ApplyResources(this.btnNewTrial, "btnNewTrial");
            this.btnNewTrial.Name = "btnNewTrial";
            this.btnNewTrial.UseVisualStyleBackColor = true;
            this.btnNewTrial.Click += new EventHandler(this.btnNewTrial_Click);
            this.btnOpenTrial.DialogResult = DialogResult.OK;
            manager.ApplyResources(this.btnOpenTrial, "btnOpenTrial");
            this.btnOpenTrial.Name = "btnOpenTrial";
            this.btnOpenTrial.UseVisualStyleBackColor = true;
            this.btnOpenTrial.Click += new EventHandler(this.btnOpenTrial_Click);
            this.btnContinueTrial.DialogResult = DialogResult.OK;
            manager.ApplyResources(this.btnContinueTrial, "btnContinueTrial");
            this.btnContinueTrial.Name = "btnContinueTrial";
            this.btnContinueTrial.UseVisualStyleBackColor = true;
            this.btnContinueTrial.Click += new EventHandler(this.btnContinueTrial_Click);
            this.btnExit.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this.btnExit, "btnExit");
            this.btnExit.Name = "btnExit";
            this.btnExit.UseVisualStyleBackColor = true;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnExit;
            manager.ApplyResources(this, "$this");
            base.Controls.Add(this.btnExit);
            base.Controls.Add(this.btnContinueTrial);
            base.Controls.Add(this.btnOpenTrial);
            base.Controls.Add(this.btnNewTrial);
            base.FormBorderStyle = FormBorderStyle.None;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "StartupForm";
            base.ShowInTaskbar = true;
            base.ResumeLayout(false);
        }

        public StartupAction Action
        {
            get
            {
                return this.action;
            }
        }
    }
}

