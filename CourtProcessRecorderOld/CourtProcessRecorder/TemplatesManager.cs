﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Windows.Forms;

    public static class TemplatesManager
    {
        private static DataTable subsituations;
        private static List<Template> templates;

        public static void AddTemplate(Template template, bool setAsCurrent)
        {
            if (template != null)
            {
                Template item = (Template) template.Clone();
                templates.Add(item);
            }
        }

        public static void DeleteTemplate(string templateName)
        {
            Template templateByName = GetTemplateByName(templateName);
            if (templateByName != null)
            {
                Templates.Remove(templateByName);
                templateByName.Dispose();
                try
                {
                    File.Delete(TemplatesPath + templateName + ".CLPT");
                }
                catch
                {
                }
            }
        }

        public static Template GetTemplateByName(string templateName)
        {
            return Templates.Find(delegate (Template t) {
                return t.Name == templateName;
            });
        }

        public static bool HasTemplate(string templateName)
        {
            return Templates.Exists(delegate (Template t) {
                return t.Name == templateName;
            });
        }

        public static void RenameTemplate(string oldTemplateName, string newTemplateName)
        {
            Template templateByName = GetTemplateByName(oldTemplateName);
            if (templateByName != null)
            {
                templateByName.Name = newTemplateName;
                try
                {
                    File.Move(TemplatesPath + oldTemplateName + ".CLPT", TemplatesPath + newTemplateName + ".CLPT");
                }
                catch
                {
                }
            }
        }

        public static DataTable Subsituations
        {
            get
            {
                if (subsituations == null)
                {
                    subsituations = new DataTable("SUBSITUATIONS");
                    DataColumn column = subsituations.Columns.Add("TYPE_NAME", typeof(string));
                    column.MaxLength = 0x7fffffff;
                    column.AllowDBNull = false;
                    DataColumn column2 = subsituations.Columns.Add("SYS_NAME", typeof(string));
                    column2.MaxLength = 0x7fffffff;
                    column2.AllowDBNull = false;
                    subsituations.PrimaryKey = new DataColumn[] { column, column2 };
                    DataColumn column3 = subsituations.Columns.Add("NAME", typeof(string));
                    column3.MaxLength = 0x7fffffff;
                    column3.AllowDBNull = false;
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "number", "ნომერი" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "date", "თარიღი" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "name", "სახელი" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "numroom", "დარბაზის ნომერი" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "volume", "ციფრული მატარებელი" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "start", "დაწყების დრო" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "finish", "დასრულების დრო" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "audiorecording journal", "აუდიო ჟურნალი" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "actual persons", "მონაწილეები" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "signatures", "ციფრული ხელმოწერა" });
                    subsituations.Rows.Add(new object[] { "PROTOCOL", "serial number", "სერიული ნომერი" });
                    subsituations.Rows.Add(new object[] { "PARAGRAPH", "time", "დრო" });
                    subsituations.Rows.Add(new object[] { "PARAGRAPH", "action", "მოქმედება" });
                    subsituations.Rows.Add(new object[] { "PARAGRAPH", "person", "მონაწილე" });
                    subsituations.Rows.Add(new object[] { "PARAGRAPH", "note", "შენიშვნა" });
                }
                return subsituations;
            }
        }

        public static List<Template> Templates
        {
            get
            {
                if (templates == null)
                {
                    templates = new List<Template>();
                    foreach (string str in Directory.GetFiles(TemplatesPath))
                    {
                        if (Path.GetExtension(str).ToUpper() == ".CLPT")
                        {
                            Template item = Template.LoadFromFile(str);
                            if (item != null)
                            {
                                templates.Add(item);
                            }
                        }
                    }
                }
                return templates;
            }
        }

        public static string TemplatesPath
        {
            get
            {
                return (Path.GetDirectoryName(Application.ExecutablePath) + @"\Templates\");
            }
        }
    }
}

