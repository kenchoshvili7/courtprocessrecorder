﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class EnterCDSerialNo : BaseForm
    {
        private Button btnCancel;
        private Button btnOk;
        private CaseDocument caseDocument;
        private CheckBox cbSign;
        private IContainer components = null;
        private TextBox edtSerialNo;
        private Label label1;

        public EnterCDSerialNo(CaseDocument caseDocument, DateTime date)
        {
            this.caseDocument = caseDocument;
            this.InitializeComponent();
            this.edtSerialNo.Text = caseDocument.CDSerialNo;
            this.cbSign.Visible = false;
            foreach (DateTime time in caseDocument._TrialDates)
            {
                if (!caseDocument.Signature.ContainsKey(time))
                {
                    this.cbSign.Visible = true;
                    break;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.btnCancel = new Button();
            this.btnOk = new Button();
            this.edtSerialNo = new TextBox();
            this.label1 = new Label();
            this.cbSign = new CheckBox();
            base.SuspendLayout();
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Font = new Font("Sylfaen", 8.25f);
            this.btnCancel.Location = new Point(0x166, 0x45);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x4b, 0x17);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "გაუქმება";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnOk.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnOk.DialogResult = DialogResult.OK;
            this.btnOk.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.btnOk.Location = new Point(0x115, 0x45);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new Size(0x4b, 0x17);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.edtSerialNo.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.edtSerialNo.Location = new Point(12, 30);
            this.edtSerialNo.Name = "edtSerialNo";
            this.edtSerialNo.Size = new Size(0x1a5, 0x17);
            this.edtSerialNo.TabIndex = 1;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x33, 0x10);
            this.label1.TabIndex = 0;
            this.label1.Text = "ნომერი";
            this.cbSign.AutoSize = true;
            this.cbSign.Location = new Point(12, 0x47);
            this.cbSign.Name = "cbSign";
            this.cbSign.Size = new Size(0x99, 20);
            this.cbSign.TabIndex = 2;
            this.cbSign.Text = "ციფრული ხელმოწერა";
            this.cbSign.UseVisualStyleBackColor = true;
            base.AcceptButton = this.btnOk;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x1bd, 0x68);
            base.Controls.Add(this.cbSign);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.edtSerialNo);
            base.Controls.Add(this.btnOk);
            base.Controls.Add(this.btnCancel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "EnterCDSerialNo";
            this.Text = "დისკის სერიული ნომერი";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string CDSerialNo
        {
            get
            {
                return this.edtSerialNo.Text;
            }
        }

        public bool PerformSigning
        {
            get
            {
                return this.cbSign.Checked;
            }
        }
    }
}

