﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.Controls;
    using CourtProcessRecorder.DataTypes;
    using CourtProcessRecorder.Properties;
    using CourtRecorderCommon;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    public class MainForm : BaseForm
    {
        private AudioControl audioControl;
        private ToolStripButton btnAddLabel;
        private ToolStripButton btnBurnDisk;
        private ToolStripButton btnDecreaseTime;
        private ToolStripButton btnDelete;
        private ToolStripButton btnEditTemplate;
        private ToolStripButton btnIncreaseTime;
        private ToolStripButton btnManagePersons;
        private ToolStripSplitButton btnModes;
        private ToolStripButton btnNew;
        private ToolStripButton btnOpen;
        private ToolStripButton btnPriorityLogic;
        private ToolStripButton btnSave;
        private ToolStripButton btnSignature;
        private ToolStripButton btnViewOrder;
        private ToolStripButton btQA;
        private static CourtProcessRecorder.DataTypes.CaseDocument caseDocument;
        private ToolStripComboBox cbTemplates;
        private ColumnHeader ch;
        private ColumnHeader chAction;
        private ColumnHeader chNote;
        private ColumnHeader chPerson;
        private ColumnHeader chTime;
        private IContainer components;
        private static DateTime currentDate;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private TextBox edtNotes;
        private RichTextBox eOrderView;
        private ContextMenuStrip eventActionsContextMenu;
        private bool eventChangingByUser;
        private Timer freeSpaceWatcherTimer;
        private SplitContainer leftSplitContainer;
        private ListView listEvents;
        private ListView listPersons;
        private int lockBtnDateCheckedChanged;
        private int lockEdtNotesTextChanged;
        private bool lockTemplatesSelectedIndexChange;
        public static MainForm MainFormInstance;
        private SplitContainer mainSplitContainer;
        private ToolStrip mainToolStrip;
        private ToolStripMenuItem mnuAnnoteMode;
        private ToolStripMenuItem mnuProtocolMode;
        private SplitContainer rightBottomSplitter;
        private SplitContainer rightSplitContainer;
        private int savedSplitterDistance;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripSeparator toolStripSeparator7;
        private ToolStripSeparator toolStripSeparator9;
        private ToolStrip tpCaseDates;
        private NotifyIcon trayIcon;
        private StagesTreeControl treeCaseStage;
        private bool treeItemDoubleClicked;
        private ToolStripMenuItem ახალიჭდეToolStripMenuItem;
        private ToolStripMenuItem წამითუკანToolStripMenuItem;
        private ToolStripMenuItem წამითწინToolStripMenuItem;

        public MainForm()
        {
            bool flag2;
            this.components = null;
            MainFormInstance = this;
            AudioIO.Enabled = true;
            if ((AudioIO.ProgramMode == ProgramMode.Recording) && !(Directory.Exists(AudioIO.ArchiveDirectory) && (AudioIO.Configuration.Channels.Count != 0)))
            {
                MessageBox.Show(null, "პროგრამა არ არის სრულად დაკონფიგურირებული. გაუშვით კონფიგურაცია!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Process.GetCurrentProcess().Kill();
            }
            if (AudioIO.ProgramMode != ProgramMode.Playback)
            {
                while (!Directory.Exists(AudioIO.ApplicationPath + "Templates"))
                {
                    Process process = Process.Start(AudioIO.ApplicationPath + "TemplatesInstaller.exe");
                    process.WaitForExit();
                    if ((process.ExitCode != 0) && (MessageBox.Show(null, "პროგრამაში მუშაობისათვის საჭიროა შაბლონების დაყენება!", "შეტყობინება", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand) == DialogResult.Cancel))
                    {
                        Process.GetCurrentProcess().Kill();
                    }
                }
            }
            this.InitializeComponent();
            bool flag = AudioIO.ProgramMode != ProgramMode.Recording;
            if (flag)
            {
                this.eOrderView = new RichTextBox();
                this.eOrderView.Dock = DockStyle.Fill;
                this.eOrderView.ReadOnly = true;
                this.eOrderView.WordWrap = false;
                this.eOrderView.BackColor = SystemColors.Window;
                this.eOrderView.Text = "დოკუმენტი ცარიელია";
                this.mainSplitContainer.Panel1.Controls.Add(this.eOrderView);
            }
            if (AudioIO.ProgramMode == ProgramMode.Recording)
            {
                this.trayIcon = new NotifyIcon(this.components);
                this.trayIcon.Tag = 0;
                this.trayIcon.BalloonTipTitle = "გაფრთხილება!";
                this.trayIcon.BalloonTipIcon = ToolTipIcon.Warning;
                this.trayIcon.Icon = base.Icon;
                this.trayIcon.Visible = false;
                this.freeSpaceWatcherTimer.Enabled = true;
            }
            string[] commandLineArgs = Environment.GetCommandLineArgs();
            if (commandLineArgs.Length > 1)
            {
                string path = commandLineArgs[1];
                if (!File.Exists(path))
                {
                    path = Path.Combine(Directory.GetCurrentDirectory(), path);
                }
                if (File.Exists(path) && path.ToUpper().EndsWith(".CPD"))
                {
                    try
                    {
                        this.OpenCaseDocumentSafe(path);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(null, string.Format("ვერ ხერხდება '{0}' ფაილის გახსნა !\n{1}", path, exception.Message), "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        Process.GetCurrentProcess().Kill();
                    }
                    return;
                }
            }
            StartupAction action = (AudioIO.ProgramMode != ProgramMode.Recording) ? StartupAction.OpenCaseDocument : StartupAction.None;
        Label_02B8:
            switch (action)
            {
                case StartupAction.NewCaseDocument:
                    flag = this.NewCaseDocument();
                    break;

                case StartupAction.OpenCaseDocument:
                    flag = this.OpenCaseDocument(false);
                    break;

                case StartupAction.OpenTodayCaseDocument:
                    flag = this.OpenCaseDocument(true);
                    break;
            }
            if (flag)
            {
                return;
            }
            if (AudioIO.ProgramMode != ProgramMode.Recording)
            {
                Process.GetCurrentProcess().Kill();
                return;
            }
            using (StartupForm form = new StartupForm())
            {
                switch (form.ShowDialog())
                {
                    case DialogResult.OK:
                        action = form.Action;
                        goto Label_036E;

                    case DialogResult.Cancel:
                        Process.GetCurrentProcess().Kill();
                        return;
                }
            }
        Label_036E:
            flag2 = true;
            goto Label_02B8;
        }

        private ToolStripButton AddCaseDateButton(DateTime date)
        {
            ToolStripButton button = new ToolStripButton();
            button.CheckOnClick = false;
            button.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            button.Name = button.Text = date.ToShortDateString();
            button.ToolTipText = button.Text + " " + CaseDocument.TrialTemplates[date];
            button.Click += new EventHandler(this.btnCaseDateClick);
            if (caseDocument.Signature.ContainsKey(date))
            {
                button.Image = Resources.lock16;
                button.ImageTransparentColor = Resources.lock16.GetPixel(0, 0);
            }
            if (CaseDocument.IsTrialDamaged(date))
            {
                button.ForeColor = Color.Red;
            }
            button.Tag = date;
            return button;
        }

        private ListViewItem AddEvent(Event evnt)
        {
            ListViewItem item = new ListViewItem(new string[] { evnt.Time.ToLongTimeString(), evnt.Action, (evnt.Person == null) ? null : evnt.Person.ToString(), evnt.Note });
            item.Tag = evnt;
            this.listEvents.Items.Add(item);
            this.btnSignature.Enabled = true;
            return item;
        }

        private Event AddEvent(DateTime? dt, EventType eventType, string action, Person person, string note, RecordSet recordSet)
        {
            ToolStripItem current;
            ToolStripButton button;
            dt = new DateTime?(dt.GetValueOrDefault(DateTime.Now));
            if (CaseDocument.Signature.ContainsKey(dt.Value.Date))
            {
                MessageBox.Show(this, "ამ თარიღში სხომა ხელმოწერილია და ცვლილება დაუშვებელია!", "აკრძალული ოპერაცია", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return null;
            }
            Event event2 = new Event(dt.Value, eventType, action, person, note, recordSet);
            caseDocument.Events.Add(event2);
            bool flag = false;
            IEnumerator enumerator = this.tpCaseDates.Items.GetEnumerator();
            while (enumerator.MoveNext())
            {
                current = (ToolStripItem) enumerator.Current;
                if (current is ToolStripButton)
                {
                    button = current as ToolStripButton;
                    if (((DateTime) button.Tag) == event2.Time.Date)
                    {
                        flag = true;
                        if (button.Checked)
                        {
                            goto Label_0136;
                        }
                        this.btnCaseDateClick(button, new EventArgs());
                        return event2;
                    }
                }
            }
            Label_0136:
            if (flag)
            {
                ListViewItem item2 = this.AddEvent(event2);
                item2.Selected = true;
                item2.EnsureVisible();
                this.btnSignature.Enabled = true;
                return event2;
            }
            button = this.AddCaseDateButton(event2.Time.Date);
            int num = this.tpCaseDates.Items.Count - 1;
            while (num >= 0)
            {
                current = this.tpCaseDates.Items[num];
                if ((current is ToolStripButton) && (((DateTime) current.Tag) >= event2.Time.Date))
                {
                    break;
                }
                num--;
            }
            this.tpCaseDates.Items.Insert(num + 1, button);
            this.btnCaseDateClick(button, new EventArgs());
            return event2;
        }

        private void audioControlRecordEvent(object sender, RecordEventArgs e)
        {
            if (e.RecordEvent == RecordEvents.Start)
            {
                this.freeSpaceWatcherTimer.Enabled = false;
                if (this.AddEvent(null, EventType.RecordStart, "<<<ჩაწერის დაწყება>>>", null, null, e.RecordSet) == null)
                {
                    return;
                }
            }
            else
            {
                this.freeSpaceWatcherTimer.Enabled = true;
                if (this.AddEvent(null, EventType.RecordEnd, "<<<ჩაწერის შეჩერება>>>", null, null, null) == null)
                {
                    return;
                }
            }
            caseDocument.Modified = true;
            this.btnPriorityLogic.Enabled = this.btnSave.Enabled = this.btnOpen.Enabled = this.btnNew.Enabled = this.btnViewOrder.Enabled = this.btnSignature.Enabled = this.btnBurnDisk.Enabled = e.RecordEvent == RecordEvents.Stop;
            this.UpdateAudioControlSize();
        }

        private void AudioControlTimer(object sender, EventArgs e)
        {
            if (this.audioControl.State == AudioControlState.Playing)
            {
                int num = -1;
                foreach (ListViewItem item in this.listEvents.Items)
                {
                    if ((item.Tag as Event).Time > this.audioControl.Time)
                    {
                        break;
                    }
                    num++;
                }
                if (num == -1)
                {
                    num = 0;
                }
                if (num >= this.listEvents.Items.Count)
                {
                    num = this.listEvents.Items.Count - 1;
                }
                if (num > -1)
                {
                    this.listEvents.Items[num].Selected = true;
                    this.listEvents.Items[num].EnsureVisible();
                }
            }
        }

        private void btnAddLabelClick(object sender, EventArgs e)
        {
            if (caseDocument.RecordInitialized)
            {
                int num = this.listEvents.Items.Count - 1;
                int num2 = 0;
                if (num > 0)
                {
                    Event tag = (Event) this.listEvents.Items[num].Tag;
                    string note = tag.Note;
                    int num3 = 0;
                    if (!string.IsNullOrEmpty(note) && !string.IsNullOrEmpty(note = note.TrimStart(new char[0])))
                    {
                        foreach (char ch in note)
                        {
                            int result = 0;
                            if (int.TryParse(ch.ToString(), out result))
                            {
                                num3 = (num3 * 10) + result;
                            }
                            else
                            {
                                if (ch == ')')
                                {
                                    num2 = num3;
                                }
                                break;
                            }
                        }
                    }
                }
                num2++;
                if (this.AddEvent(new DateTime?(this.GetEventTime()), EventType.Note, null, null, num2.ToString() + ") ", null) != null)
                {
                    caseDocument.Modified = true;
                }
            }
        }

        private void btnBurnDiskClick(object sender, EventArgs e)
        {
            if (CaseDocument != null)
            {
                if (CaseDocument.Modified)
                {
                    if (MessageBox.Show(this, "სხდომის მონაცემები შეიცვალა სხდომის შექმნის (გახსნის) შემდგომ. გსურთ შეინახოთ ცვლილებები?", "შეტყობინება", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        this.SaveCaseDocument();
                    }
                    else
                    {
                        MessageBox.Show(this, "დისკე ჩაწერის ოპერაცია გაუქმებულია!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return;
                    }
                }
                using (EnterCDSerialNo no = new EnterCDSerialNo(CaseDocument, CurrentDate))
                {
                    if (no.ShowDialog() == DialogResult.OK)
                    {
                        CaseDocument.CDSerialNo = no.CDSerialNo;
                        if (no.PerformSigning)
                        {
                            using (SignDocumentDialog dialog = new SignDocumentDialog())
                            {
                                if (dialog.ShowDialog() != DialogResult.OK)
                                {
                                    return;
                                }
                                DialogResult result = MessageBox.Show(this, "თუ გსურთ რომ ხელმოწერა გაკეთდეს ყველა თარიღის სხდომებზე, მაშინ დაადასტურეთ Yes ღილაკით;" + Environment.NewLine + "თუ გსურთ ხელმოწერა გაკეთდეს მხოლოდ მიმდინარე თარიღის სხდომაზე, მაშინ დაადასტურეთ No ღილაკით;" + Environment.NewLine + "თუ გსურთ ოპერაცია გაგრძელდეს ხელმოწერის გარეშე დააჭირეთ Cancel ღილაკს.", "ხელმოწერა", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button3);
                                switch (result)
                                {
                                    case DialogResult.Yes:
                                    case DialogResult.No:
                                        this.audioControl.EnableRecording(false);
                                        foreach (DateTime time in CaseDocument._TrialDates)
                                        {
                                            if (!CaseDocument.Signature.ContainsKey(time) && ((result == DialogResult.Yes) || (CurrentDate == time)))
                                            {
                                                CaseDocument.Sign(time, new DateTime?(DateTime.Now), dialog.Signer, dialog.Organization);
                                                CaseDocument.Modified = true;
                                                this.UpdateSignButton(time);
                                            }
                                        }
                                        this.SaveCaseDocument();
                                        break;
                                }
                            }
                        }
                        using (OpticalBurnerForm form = new OpticalBurnerForm(CaseDocument.CDSerialNo))
                        {
                            form.ShowDialog();
                        }
                    }
                }
            }
        }

        private void btnCaseDateClick(object sender, EventArgs e)
        {
            if (this.lockBtnDateCheckedChanged == 0)
            {
                DateTime minValue = DateTime.MinValue;
                this.lockBtnDateCheckedChanged++;
                this.listEvents.BeginUpdate();
                try
                {
                    ToolStripButton button = sender as ToolStripButton;
                    if (button != null)
                    {
                        if (button.Checked)
                        {
                            return;
                        }
                        currentDate = minValue = (DateTime) button.Tag;
                        foreach (ToolStripItem item in this.tpCaseDates.Items)
                        {
                            if (item is ToolStripButton)
                            {
                                (item as ToolStripButton).Checked = false;
                            }
                        }
                        button.Checked = true;
                        this.listEvents.Items.Clear();
                        for (int i = 0; i < CaseDocument.Events.Count; i++)
                        {
                            if (CaseDocument.Events[i].Time.Date == currentDate)
                            {
                                this.AddEvent(CaseDocument.Events[i]);
                            }
                        }
                        if (AudioIO.ProgramMode == ProgramMode.Playback)
                        {
                            this.eOrderView.Rtf = CaseDocument.GenerateOrderRtf(currentDate);
                        }
                        else
                        {
                            this.lockTemplatesSelectedIndexChange = true;
                            try
                            {
                                this.cbTemplates.SelectedItem = CaseDocument.TrialTemplates[currentDate];
                                this.SetTemplateItems();
                            }
                            finally
                            {
                                this.lockTemplatesSelectedIndexChange = false;
                            }
                        }
                    }
                    else
                    {
                        this.listEvents.Items.Clear();
                    }
                    this.listPersons.BeginUpdate();
                    try
                    {
                        this.listPersons.Items.Clear();
                        if (caseDocument.TrialPersons.ContainsKey(currentDate))
                        {
                            foreach (Person person in caseDocument.TrialPersons[currentDate])
                            {
                                this.listPersons.Items.Add(person.ToString()).Tag = person;
                            }
                        }
                    }
                    finally
                    {
                        this.listPersons.EndUpdate();
                    }
                    this.audioControl.BeginUpdate();
                    try
                    {
                        this.audioControl.CurrentDate = currentDate;
                        this.audioControl.ViewMode = Modes.Playback;
                        if (AudioIO.ProgramMode != ProgramMode.Playback)
                        {
                            this.audioControl.EnableRecording((currentDate == DateTime.Now.Date) && !caseDocument.Signature.ContainsKey(currentDate));
                        }
                    }
                    finally
                    {
                        this.audioControl.EndUpdate();
                    }
                    this.UpdateSignatureDependedControls();
                }
                finally
                {
                    this.lockBtnDateCheckedChanged--;
                    this.listEvents.EndUpdate();
                }
            }
        }

        private void btnDecreaseTimeClick(object sender, EventArgs e)
        {
            if (caseDocument.RecordInitialized && (this.listEvents.SelectedItems.Count > 0))
            {
                Event tag = (Event) this.listEvents.SelectedItems[0].Tag;
                tag.Time = tag.Time.AddSeconds(-1.0);
                this.listEvents.SelectedItems[0].Text = tag.Time.ToLongTimeString();
                this.listEvents.Sort();
                caseDocument.Modified = true;
            }
        }

        private void btnDeleteClick(object sender, EventArgs e)
        {
            if (caseDocument.RecordInitialized && (this.listEvents.SelectedItems.Count > 0))
            {
                int index = this.listEvents.SelectedItems[0].Index;
                Event tag = (Event) this.listEvents.SelectedItems[0].Tag;
                bool flag = ((byte) (tag.EventType & EventType.RecordStart)) == 1;
                if (flag)
                {
                    switch (MessageBox.Show(this, "აუდიო <<<ჩაწერის დაწყება>>> წაშლა გამოიწვევს დაკავშირებული აუდიო ჩანაწერ(ებ)ის და მათი კავშირ(ებ)ის წაშლასაც, რომელის აღდგენაც შემდგომ შეუძლებელია. დარწმუნებული ხართ რომ გსურთ დაკავშირებული აუდიო ჩანაწერების წაშლა?", "გაფრთხილება", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2))
                    {
                        case DialogResult.No:
                            return;
                    }
                }
                caseDocument.Events.Remove(tag);
                this.listEvents.Items.RemoveAt(this.listEvents.SelectedItems[0].Index);
                caseDocument.Modified = true;
                if (this.listEvents.Items.Count > index)
                {
                    this.listEvents.Items[index].Selected = true;
                    this.listEvents.Items[index].EnsureVisible();
                }
                else if (this.listEvents.Items.Count > 0)
                {
                    this.listEvents.Items[this.listEvents.Items.Count - 1].Selected = true;
                    this.listEvents.Items[this.listEvents.Items.Count - 1].EnsureVisible();
                }
                if (flag)
                {
                    this.audioControl.BeginUpdate();
                    this.audioControl.EndUpdate();
                }
            }
        }

        private void btnEditTemplateClick(object sender, EventArgs e)
        {
            using (TemplatesDialog dialog = new TemplatesDialog(this.cbTemplates.Text))
            {
                dialog.ShowDialog();
                if (dialog.currentTemplate != null)
                {
                    this.lockTemplatesSelectedIndexChange = true;
                    try
                    {
                        this.cbTemplates.Items.Clear();
                        foreach (Template template in TemplatesManager.Templates)
                        {
                            this.cbTemplates.Items.Add(template.Name);
                        }
                        foreach (Template template in CaseDocument.Templates)
                        {
                            this.cbTemplates.Items.Insert(0, template.Name);
                        }
                        foreach (ToolStripItem item in this.tpCaseDates.Items)
                        {
                            if (item is ToolStripButton)
                            {
                                item.ToolTipText = item.Text + " " + CaseDocument.TrialTemplates[(DateTime) item.Tag];
                            }
                        }
                    }
                    finally
                    {
                        this.lockTemplatesSelectedIndexChange = false;
                    }
                    this.cbTemplates.SelectedItem = dialog.currentTemplate.Name;
                }
                else
                {
                    this.cbTemplates.SelectedIndex = -1;
                }
            }
        }

        private void btnIncreaseTimeClick(object sender, EventArgs e)
        {
            if (caseDocument.RecordInitialized && (this.listEvents.SelectedItems.Count > 0))
            {
                Event tag = (Event) this.listEvents.SelectedItems[0].Tag;
                tag.Time = tag.Time.AddSeconds(1.0);
                this.listEvents.SelectedItems[0].Text = tag.Time.ToLongTimeString();
                this.listEvents.Sort();
                caseDocument.Modified = true;
            }
        }

        private void btnManagePersonsClick(object sender, EventArgs e)
        {
            using (ManagePersonsDialog dialog = new ManagePersonsDialog())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    CaseDocument.Modified = true;
                    this.listPersons.BeginUpdate();
                    try
                    {
                        CaseDocument.TrialPersons.Remove(CurrentDate);
                        this.listPersons.Items.Clear();
                        CaseDocument.TrialPersons.Add(CurrentDate, dialog.Persons);
                        foreach (Person person in caseDocument.TrialPersons[CurrentDate])
                        {
                            this.listPersons.Items.Add(person.ToString()).Tag = person;
                        }
                    }
                    finally
                    {
                        this.listPersons.EndUpdate();
                    }
                }
            }
        }

        private void btnModesButtonClick(object sender, EventArgs e)
        {
            if (this.btnModes.Text == this.mnuProtocolMode.Text)
            {
                this.savedSplitterDistance = this.mainSplitContainer.SplitterDistance;
                this.mainSplitContainer.SplitterDistance = 0;
                this.btnModes.Image = Resources.goto_line;
                this.btnModes.Text = this.mnuAnnoteMode.Text;
                this.mnuProtocolMode.Checked = false;
                this.mnuAnnoteMode.Checked = true;
            }
            else if (this.btnModes.Text == this.mnuAnnoteMode.Text)
            {
                this.mainSplitContainer.SplitterDistance = this.savedSplitterDistance;
                this.btnModes.Image = Resources.goto_line___style_2;
                this.btnModes.Text = this.mnuProtocolMode.Text;
                this.mnuAnnoteMode.Checked = false;
                this.mnuProtocolMode.Checked = true;
            }
        }

        private void btnNewClick(object sender, EventArgs e)
        {
            this.NewCaseDocument();
        }

        private void btnOpenClick(object sender, EventArgs e)
        {
            switch (this.QuerySave())
            {
                case DialogResult.Yes:
                    this.SaveCaseDocument();
                    break;

                case DialogResult.Cancel:
                    return;
            }
            this.OpenCaseDocument(false);
        }

        private void btnPriorityLogicClick(object sender, EventArgs e)
        {
            AudioIO.UsePriorityLogic = this.btnPriorityLogic.Checked;
            AudioIO.SettingManager.WriteBoolean("UsePriorityLogic", this.btnPriorityLogic.Checked);
        }

        private void btnSaveClick(object sender, EventArgs e)
        {
            this.SaveCaseDocument();
        }

        private void btnSignatureClick(object sender, EventArgs e)
        {
            try
            {
                if (CaseDocument != null)
                {
                    if (!CaseDocument.Signature.ContainsKey(CurrentDate))
                    {
                        if (AudioIO.ProgramMode != ProgramMode.Playback)
                        {
                            if (MessageBox.Show(this, CurrentDate.ToLongDateString() + " თარიღის სხდომა არ არის ხელმოწერილი, გსურთ ციფრული ხელმოწერა!", "შეტყობინება", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                if (!CaseDocument.Modified || (MessageBox.Show(this, "სხდომის მონაცემები შეიცვალა სხდომის შექმნის (გახსნის) შემდგომ. გსურთ შეინახოთ ცვლილებები?", "შეტყობინება", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK))
                                {
                                    if (CaseDocument.Modified)
                                    {
                                        this.SaveCaseDocument();
                                    }
                                    using (SignDocumentDialog dialog = new SignDocumentDialog())
                                    {
                                        if (dialog.ShowDialog() == DialogResult.OK)
                                        {
                                            CaseDocument.Sign(CurrentDate, new DateTime?(DateTime.Now), dialog.Signer, dialog.Organization);
                                            this.audioControl.EnableRecording(false);
                                            this.UpdateSignButton(CurrentDate);
                                            CaseDocument.Modified = true;
                                            this.SaveCaseDocument();
                                            SignatureDialog.ShowDialog(CaseDocument, new DateTime?(CurrentDate));
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(this, "სხდომის ხელმოწერა უნდა მოხდეს მხოლოდ შენახულ სხდომაზე!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show(this, "სხდომა ხელმოწერილი არ არის!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                    }
                    else
                    {
                        SignatureDialog.ShowDialog(CaseDocument, new DateTime?(CurrentDate));
                    }
                }
            }
            catch (EAbortException)
            {
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void btnViewOrderClick(object sender, EventArgs e)
        {
            if (caseDocument != null)
            {
                if (CaseDocument.Modified)
                {
                    if (MessageBox.Show(this, "სხდომის მონაცემები შეიცვალა სხდომის შექმნის (გახსნის) შემდგომ. გსურთ შეინახოთ ცვლილებები?", "შეტყობინება", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        this.SaveCaseDocument();
                    }
                    else
                    {
                        MessageBox.Show(this, "ოქმის ნახვის ოპერაცია გაუქმებულია!", "შეტყობინება", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return;
                    }
                }
                caseDocument.GenerateOrderAndPreview(CurrentDate);
            }
        }

        private void btQAClick(object sender, EventArgs e)
        {
            if (this.listEvents.Items.Count > 0)
            {
                int num2;
                Event tag;
                string str = null;
                int num = 0;
                for (num2 = this.listEvents.Items.Count - 1; num2 >= 0; num2--)
                {
                    tag = (Event) this.listEvents.Items[num2].Tag;
                    if (tag.Person != null)
                    {
                        str = tag.Person.ToString();
                        num = num2;
                        break;
                    }
                }
                if (!string.IsNullOrEmpty(str))
                {
                    for (num2 = num - 1; num2 >= 0; num2--)
                    {
                        tag = (Event) this.listEvents.Items[num2].Tag;
                        if ((tag.Person != null) && (tag.Person.ToString() != str))
                        {
                            if (this.AddEvent(new DateTime?(this.GetEventTime()), EventType.Person, null, tag.Person, null, null) != null)
                            {
                                caseDocument.Modified = true;
                            }
                            break;
                        }
                    }
                }
            }
        }

        private void cbTemplatesSelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.lockTemplatesSelectedIndexChange)
            {
                this.InitializeFromSelectedTemplate();
                if ((AudioIO.ProgramMode != ProgramMode.Playback) && (caseDocument != null))
                {
                    caseDocument.Modified = true;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void edtNotesTextChanged(object sender, EventArgs e)
        {
            if ((this.lockEdtNotesTextChanged == 0) && (this.listEvents.SelectedItems.Count > 0))
            {
                Event tag = (Event) this.listEvents.SelectedItems[0].Tag;
                this.listEvents.SelectedItems[0].SubItems[3].Text = this.edtNotes.Text;
                tag.Note = this.edtNotes.Text;
                if (string.IsNullOrEmpty(this.edtNotes.Text))
                {
                    tag.EventType = (EventType) ((byte) (tag.EventType ^ EventType.Note));
                }
                else
                {
                    tag.EventType = (EventType) ((byte) (tag.EventType | EventType.Note));
                }
                caseDocument.Modified = true;
            }
        }

        private void freeSpaceWatcherTimerTick(object sender, EventArgs e)
        {
            //long freeSpace = Utils.GetFreeSpace(AudioIO.ArchiveDirectory);
            //if (freeSpace < AudioIO.Configuration.FreeSpaceLimitWarn)
            //{
            //    this.trayIcon.Text = this.Text.Substring(0, Math.Min(this.Text.Length, 0x3f));
            //    this.trayIcon.Visible = true;
            //    this.trayIcon.MouseClick += new MouseEventHandler(this.trayIconMouseClick);
            //    this.trayIcon.BalloonTipText = string.Format("თავისუფალი ადგილი სამუშაო დისკზე არის {0}-ზე ნაკლები {1}! გთხოვთ გაანთავისუფლოთ დამატებით!", Utils.FormatFileSize(freeSpace), Utils.FormatFileSize(AudioIO.Configuration.FreeSpaceLimitWarn));
            //    this.trayIcon.ShowBalloonTip(0xbb8);
            //    this.trayIcon.Tag = 1;
            //}
            //else
            //{
            //    this.trayIcon.Tag = 0;
            //    this.trayIcon.Visible = false;
            //}
        }

        private DateTime GetEventTime()
        {
            Predicate<Event> match = null;
            DateTime dt = DateTime.Now;
            if ((this.audioControl.ViewMode != Modes.Playback) || (AudioIO.ProgramMode == ProgramMode.Playback))
            {
                return dt;
            }
            dt = this.audioControl.playBackControl.Time;
        Label_005E:
            if (match == null)
            {
                match = delegate (Event e) {
                    return e.Time == dt;
                };
            }
            if (CaseDocument.Events.Find(match) != null)
            {
                dt = dt.AddTicks(10L);
                goto Label_005E;
            }
            return dt;
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(MainForm));
            this.mainToolStrip = new ToolStrip();
            this.btnNew = new ToolStripButton();
            this.btnOpen = new ToolStripButton();
            this.btnSave = new ToolStripButton();
            this.toolStripSeparator7 = new ToolStripSeparator();
            this.cbTemplates = new ToolStripComboBox();
            this.toolStripSeparator2 = new ToolStripSeparator();
            this.btnEditTemplate = new ToolStripButton();
            this.btnIncreaseTime = new ToolStripButton();
            this.toolStripSeparator9 = new ToolStripSeparator();
            this.btnManagePersons = new ToolStripButton();
            this.toolStripSeparator3 = new ToolStripSeparator();
            this.btnDecreaseTime = new ToolStripButton();
            this.toolStripSeparator4 = new ToolStripSeparator();
            this.btnDelete = new ToolStripButton();
            this.btnAddLabel = new ToolStripButton();
            this.btnBurnDisk = new ToolStripButton();
            this.btnViewOrder = new ToolStripButton();
            this.btQA = new ToolStripButton();
            this.toolStripSeparator1 = new ToolStripSeparator();
            this.btnModes = new ToolStripSplitButton();
            this.mnuProtocolMode = new ToolStripMenuItem();
            this.mnuAnnoteMode = new ToolStripMenuItem();
            this.btnSignature = new ToolStripButton();
            this.btnPriorityLogic = new ToolStripButton();
            this.mainSplitContainer = new SplitContainer();
            this.leftSplitContainer = new SplitContainer();
            this.treeCaseStage = new StagesTreeControl();
            this.listPersons = new ListView();
            this.ch = new ColumnHeader();
            this.rightSplitContainer = new SplitContainer();
            this.tpCaseDates = new ToolStrip();
            this.listEvents = new ListView();
            this.chTime = new ColumnHeader();
            this.chAction = new ColumnHeader();
            this.chPerson = new ColumnHeader();
            this.chNote = new ColumnHeader();
            this.rightBottomSplitter = new SplitContainer();
            this.edtNotes = new TextBox();
            this.audioControl = new AudioControl();
            this.eventActionsContextMenu = new ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new ToolStripMenuItem();
            this.deleteToolStripMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator5 = new ToolStripSeparator();
            this.წამითუკანToolStripMenuItem = new ToolStripMenuItem();
            this.წამითწინToolStripMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator6 = new ToolStripSeparator();
            this.ახალიჭდეToolStripMenuItem = new ToolStripMenuItem();
            this.freeSpaceWatcherTimer = new Timer(this.components);
            this.mainToolStrip.SuspendLayout();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.leftSplitContainer.Panel1.SuspendLayout();
            this.leftSplitContainer.Panel2.SuspendLayout();
            this.leftSplitContainer.SuspendLayout();
            this.rightSplitContainer.Panel1.SuspendLayout();
            this.rightSplitContainer.Panel2.SuspendLayout();
            this.rightSplitContainer.SuspendLayout();
            this.rightBottomSplitter.Panel1.SuspendLayout();
            this.rightBottomSplitter.Panel2.SuspendLayout();
            this.rightBottomSplitter.SuspendLayout();
            this.eventActionsContextMenu.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this.mainToolStrip, "mainToolStrip");
            this.mainToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            this.mainToolStrip.Items.AddRange(new ToolStripItem[] { 
                this.btnNew, this.btnOpen, this.btnSave, this.toolStripSeparator7, this.cbTemplates, this.toolStripSeparator2, this.btnEditTemplate, this.btnIncreaseTime, this.toolStripSeparator9, this.btnManagePersons, this.toolStripSeparator3, this.btnDecreaseTime, this.toolStripSeparator4, this.btnDelete, this.btnAddLabel, this.btnBurnDisk, 
                this.btnViewOrder, this.btQA, this.toolStripSeparator1, this.btnModes, this.btnSignature, this.btnPriorityLogic
             });
            this.mainToolStrip.Name = "mainToolStrip";
            this.btnNew.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnNew.Image = Resources.new16_h;
            manager.ApplyResources(this.btnNew, "btnNew");
            this.btnNew.Name = "btnNew";
            this.btnNew.Click += new EventHandler(this.btnNewClick);
            this.btnOpen.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnOpen.Image = Resources.open16_h;
            manager.ApplyResources(this.btnOpen, "btnOpen");
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Click += new EventHandler(this.btnOpenClick);
            this.btnSave.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnSave.Image = Resources.save16_h;
            manager.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.Click += new EventHandler(this.btnSaveClick);
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            manager.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
            this.cbTemplates.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbTemplates.DropDownWidth = 640;
            manager.ApplyResources(this.cbTemplates, "cbTemplates");
            this.cbTemplates.Name = "cbTemplates";
            this.cbTemplates.SelectedIndexChanged += new EventHandler(this.cbTemplatesSelectedIndexChanged);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            manager.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            this.btnEditTemplate.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnEditTemplate.Image = Resources.edit16_h;
            manager.ApplyResources(this.btnEditTemplate, "btnEditTemplate");
            this.btnEditTemplate.Name = "btnEditTemplate";
            this.btnEditTemplate.Click += new EventHandler(this.btnEditTemplateClick);
            this.btnIncreaseTime.Alignment = ToolStripItemAlignment.Right;
            this.btnIncreaseTime.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnIncreaseTime.Image = Resources.down16_h;
            manager.ApplyResources(this.btnIncreaseTime, "btnIncreaseTime");
            this.btnIncreaseTime.Name = "btnIncreaseTime";
            this.btnIncreaseTime.Click += new EventHandler(this.btnIncreaseTimeClick);
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            manager.ApplyResources(this.toolStripSeparator9, "toolStripSeparator9");
            this.btnManagePersons.DisplayStyle = ToolStripItemDisplayStyle.Image;
            manager.ApplyResources(this.btnManagePersons, "btnManagePersons");
            this.btnManagePersons.Name = "btnManagePersons";
            this.btnManagePersons.Click += new EventHandler(this.btnManagePersonsClick);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            manager.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            this.btnDecreaseTime.Alignment = ToolStripItemAlignment.Right;
            this.btnDecreaseTime.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnDecreaseTime.Image = Resources.up16_h;
            manager.ApplyResources(this.btnDecreaseTime, "btnDecreaseTime");
            this.btnDecreaseTime.Name = "btnDecreaseTime";
            this.btnDecreaseTime.Click += new EventHandler(this.btnDecreaseTimeClick);
            this.toolStripSeparator4.Alignment = ToolStripItemAlignment.Right;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            manager.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            this.btnDelete.Alignment = ToolStripItemAlignment.Right;
            this.btnDelete.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = Resources.delete16_h;
            manager.ApplyResources(this.btnDelete, "btnDelete");
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Click += new EventHandler(this.btnDeleteClick);
            this.btnAddLabel.Alignment = ToolStripItemAlignment.Right;
            this.btnAddLabel.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnAddLabel.Image = Resources.select16;
            manager.ApplyResources(this.btnAddLabel, "btnAddLabel");
            this.btnAddLabel.Name = "btnAddLabel";
            this.btnAddLabel.Click += new EventHandler(this.btnAddLabelClick);
            this.btnBurnDisk.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnBurnDisk.Image = Resources.cd16_h;
            manager.ApplyResources(this.btnBurnDisk, "btnBurnDisk");
            this.btnBurnDisk.Name = "btnBurnDisk";
            this.btnBurnDisk.Click += new EventHandler(this.btnBurnDiskClick);
            this.btnViewOrder.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnViewOrder.Image = Resources.print_preview;
            manager.ApplyResources(this.btnViewOrder, "btnViewOrder");
            this.btnViewOrder.Name = "btnViewOrder";
            this.btnViewOrder.Click += new EventHandler(this.btnViewOrderClick);
            this.btQA.Alignment = ToolStripItemAlignment.Right;
            this.btQA.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btQA.Image = Resources.help16_h;
            manager.ApplyResources(this.btQA, "btQA");
            this.btQA.Name = "btQA";
            this.btQA.Click += new EventHandler(this.btQAClick);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            manager.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            manager.ApplyResources(this.btnModes, "btnModes");
            this.btnModes.DropDownItems.AddRange(new ToolStripItem[] { this.mnuProtocolMode, this.mnuAnnoteMode });
            this.btnModes.Image = Resources.goto_line___style_2;
            this.btnModes.Name = "btnModes";
            this.btnModes.ButtonClick += new EventHandler(this.btnModesButtonClick);
            this.mnuProtocolMode.Checked = true;
            this.mnuProtocolMode.CheckOnClick = true;
            this.mnuProtocolMode.CheckState = CheckState.Checked;
            manager.ApplyResources(this.mnuProtocolMode, "mnuProtocolMode");
            this.mnuProtocolMode.Image = Resources.goto_line___style_2;
            this.mnuProtocolMode.MergeIndex = 0;
            this.mnuProtocolMode.Name = "mnuProtocolMode";
            this.mnuProtocolMode.Click += new EventHandler(this.btnModesButtonClick);
            this.mnuAnnoteMode.CheckOnClick = true;
            manager.ApplyResources(this.mnuAnnoteMode, "mnuAnnoteMode");
            this.mnuAnnoteMode.Image = Resources.goto_line;
            this.mnuAnnoteMode.MergeIndex = 0;
            this.mnuAnnoteMode.Name = "mnuAnnoteMode";
            this.mnuAnnoteMode.Click += new EventHandler(this.btnModesButtonClick);
            this.btnSignature.Alignment = ToolStripItemAlignment.Right;
            this.btnSignature.DisplayStyle = ToolStripItemDisplayStyle.Image;
            manager.ApplyResources(this.btnSignature, "btnSignature");
            this.btnSignature.Image = Resources.verify_document16;
            this.btnSignature.Name = "btnSignature";
            this.btnSignature.Click += new EventHandler(this.btnSignatureClick);
            this.btnPriorityLogic.Alignment = ToolStripItemAlignment.Right;
            this.btnPriorityLogic.CheckOnClick = true;
            this.btnPriorityLogic.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnPriorityLogic.Image = Resources.transfer_incoming16_h;
            manager.ApplyResources(this.btnPriorityLogic, "btnPriorityLogic");
            this.btnPriorityLogic.Name = "btnPriorityLogic";
            this.btnPriorityLogic.Click += new EventHandler(this.btnPriorityLogicClick);
            manager.ApplyResources(this.mainSplitContainer, "mainSplitContainer");
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Panel1.Controls.Add(this.leftSplitContainer);
            this.mainSplitContainer.Panel2.Controls.Add(this.rightSplitContainer);
            manager.ApplyResources(this.leftSplitContainer, "leftSplitContainer");
            this.leftSplitContainer.Name = "leftSplitContainer";
            this.leftSplitContainer.Panel1.Controls.Add(this.treeCaseStage);
            this.leftSplitContainer.Panel2.Controls.Add(this.listPersons);
            manager.ApplyResources(this.treeCaseStage, "treeCaseStage");
            this.treeCaseStage.Name = "treeCaseStage";
            this.treeCaseStage.BeforeCollapse += new TreeViewCancelEventHandler(this.treeCaseStageBeforeCollapseOrExpand);
            this.treeCaseStage.BeforeExpand += new TreeViewCancelEventHandler(this.treeCaseStageBeforeCollapseOrExpand);
            this.treeCaseStage.MouseClick += new MouseEventHandler(this.treeCaseStageMouseClick);
            this.treeCaseStage.MouseDown += new MouseEventHandler(this.treeCaseStage_MouseDown);
            manager.ApplyResources(this.listPersons, "listPersons");
            this.listPersons.Columns.AddRange(new ColumnHeader[] { this.ch });
            this.listPersons.FullRowSelect = true;
            this.listPersons.GridLines = true;
            this.listPersons.HideSelection = false;
            this.listPersons.MultiSelect = false;
            this.listPersons.Name = "listPersons";
            this.listPersons.ShowGroups = false;
            this.listPersons.UseCompatibleStateImageBehavior = false;
            this.listPersons.View = View.Details;
            this.listPersons.MouseClick += new MouseEventHandler(this.listPersonsMouseClick);
            manager.ApplyResources(this.ch, "ch");
            manager.ApplyResources(this.rightSplitContainer, "rightSplitContainer");
            this.rightSplitContainer.Name = "rightSplitContainer";
            this.rightSplitContainer.Panel1.Controls.Add(this.tpCaseDates);
            this.rightSplitContainer.Panel1.Controls.Add(this.listEvents);
            this.rightSplitContainer.Panel2.Controls.Add(this.rightBottomSplitter);
            this.tpCaseDates.GripStyle = ToolStripGripStyle.Hidden;
            manager.ApplyResources(this.tpCaseDates, "tpCaseDates");
            this.tpCaseDates.Name = "tpCaseDates";
            manager.ApplyResources(this.listEvents, "listEvents");
            this.listEvents.Columns.AddRange(new ColumnHeader[] { this.chTime, this.chAction, this.chPerson, this.chNote });
            this.listEvents.FullRowSelect = true;
            this.listEvents.GridLines = true;
            this.listEvents.HideSelection = false;
            this.listEvents.MultiSelect = false;
            this.listEvents.Name = "listEvents";
            this.listEvents.ShowGroups = false;
            this.listEvents.Sorting = SortOrder.Ascending;
            this.listEvents.UseCompatibleStateImageBehavior = false;
            this.listEvents.View = View.Details;
            this.listEvents.SelectedIndexChanged += new EventHandler(this.listEventsSelectedIndexChanged);
            this.listEvents.MouseDown += new MouseEventHandler(this.listEventsMouseDown);
            this.listEvents.MouseUp += new MouseEventHandler(this.listEventsMouseUp);
            manager.ApplyResources(this.chTime, "chTime");
            manager.ApplyResources(this.chAction, "chAction");
            manager.ApplyResources(this.chPerson, "chPerson");
            manager.ApplyResources(this.chNote, "chNote");
            manager.ApplyResources(this.rightBottomSplitter, "rightBottomSplitter");
            this.rightBottomSplitter.Name = "rightBottomSplitter";
            this.rightBottomSplitter.Panel1.Controls.Add(this.edtNotes);
            this.rightBottomSplitter.Panel2.Controls.Add(this.audioControl);
            this.rightBottomSplitter.SplitterMoved += new SplitterEventHandler(this.rightBottomSplitter_SplitterMoved);
            manager.ApplyResources(this.edtNotes, "edtNotes");
            this.edtNotes.Name = "edtNotes";
            this.edtNotes.TextChanged += new EventHandler(this.edtNotesTextChanged);
            manager.ApplyResources(this.audioControl, "audioControl");
            this.audioControl.MinimumSize = new Size(0x1b2, 120);
            this.audioControl.Name = "audioControl";
            this.audioControl.RecordEvent += new RecordEventHandler(this.audioControlRecordEvent);
            this.audioControl.Timer += new EventHandler(this.AudioControlTimer);
            this.audioControl.PlaybackCtrlClick += new CtrlClickEventHandler(this.PlaybackCtrlClick);
            this.eventActionsContextMenu.Items.AddRange(new ToolStripItem[] { this.toolStripMenuItem1, this.deleteToolStripMenuItem, this.toolStripSeparator5, this.წამითუკანToolStripMenuItem, this.წამითწინToolStripMenuItem, this.toolStripSeparator6, this.ახალიჭდეToolStripMenuItem });
            this.eventActionsContextMenu.Name = "eventActionsContextMenu";
            manager.ApplyResources(this.eventActionsContextMenu, "eventActionsContextMenu");
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            manager.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            manager.ApplyResources(this.deleteToolStripMenuItem, "deleteToolStripMenuItem");
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            manager.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            this.წამითუკანToolStripMenuItem.Name = "წამითუკანToolStripMenuItem";
            manager.ApplyResources(this.წამითუკანToolStripMenuItem, "წამითუკანToolStripMenuItem");
            this.წამითწინToolStripMenuItem.Name = "წამითწინToolStripMenuItem";
            manager.ApplyResources(this.წამითწინToolStripMenuItem, "წამითწინToolStripMenuItem");
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            manager.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
            this.ახალიჭდეToolStripMenuItem.Name = "ახალიჭდეToolStripMenuItem";
            manager.ApplyResources(this.ახალიჭდეToolStripMenuItem, "ახალიჭდეToolStripMenuItem");
            this.freeSpaceWatcherTimer.Interval = 0x7530;
            this.freeSpaceWatcherTimer.Tick += new EventHandler(this.freeSpaceWatcherTimerTick);
            base.AutoScaleMode = AutoScaleMode.Inherit;
            manager.ApplyResources(this, "$this");
            base.Controls.Add(this.mainSplitContainer);
            base.Controls.Add(this.mainToolStrip);
            base.Name = "MainForm";
            base.ShowIcon = true;
            base.ShowInTaskbar = true;
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            this.mainSplitContainer.ResumeLayout(false);
            this.leftSplitContainer.Panel1.ResumeLayout(false);
            this.leftSplitContainer.Panel2.ResumeLayout(false);
            this.leftSplitContainer.ResumeLayout(false);
            this.rightSplitContainer.Panel1.ResumeLayout(false);
            this.rightSplitContainer.Panel1.PerformLayout();
            this.rightSplitContainer.Panel2.ResumeLayout(false);
            this.rightSplitContainer.ResumeLayout(false);
            this.rightBottomSplitter.Panel1.ResumeLayout(false);
            this.rightBottomSplitter.Panel1.PerformLayout();
            this.rightBottomSplitter.Panel2.ResumeLayout(false);
            this.rightBottomSplitter.ResumeLayout(false);
            this.eventActionsContextMenu.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void InitializeFromSelectedTemplate()
        {
            if (AudioIO.ProgramMode == ProgramMode.Playback)
            {
                this.cbTemplates.Items.Add(CaseDocument.TrialTemplates[CurrentDate]);
                this.cbTemplates.SelectedIndex = 0;
            }
            else if (this.cbTemplates.SelectedIndex > -1)
            {
                this.lockTemplatesSelectedIndexChange = true;
                try
                {
                    if (!this.cbTemplates.Text.EndsWith(" *"))
                    {
                        Template item = (Template) TemplatesManager.GetTemplateByName(this.cbTemplates.Text).Clone();
                        item.Name = item.Name + " *";
                        if (caseDocument.Templates.FindByName(item.Name) == null)
                        {
                            caseDocument.Templates.Add(item);
                            caseDocument.TrialTemplates[CurrentDate] = item.Name;
                            this.cbTemplates.Items.Insert(0, item.Name);
                        }
                        else
                        {
                            switch (MessageBox.Show("დოკუმენტი უკვე შეიცავს '" + this.cbTemplates.Text + "' სხდომის ტიპს. შეიცვალოს ახალით?", "შეკითხვა", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                case DialogResult.Yes:
                                {
                                    int num = caseDocument.Templates.FindIndexOfName(item.Name);
                                    caseDocument.Templates[num] = item;
                                    caseDocument.TrialTemplates[CurrentDate] = item.Name;
                                    break;
                                }
                            }
                        }
                        this.cbTemplates.SelectedItem = item.Name;
                    }
                    else
                    {
                        caseDocument.TrialTemplates[CurrentDate] = this.cbTemplates.Text;
                    }
                }
                finally
                {
                    this.lockTemplatesSelectedIndexChange = false;
                }
            }
            this.SetTemplateItems();
        }

        private void listEventsMouseDown(object sender, MouseEventArgs e)
        {
            this.eventChangingByUser = true;
        }

        private void listEventsMouseUp(object sender, MouseEventArgs e)
        {
            this.eventChangingByUser = false;
        }

        private void listEventsSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listEvents.SelectedItems.Count > 0)
            {
                Event tag = (Event) this.listEvents.SelectedItems[0].Tag;
                this.lockEdtNotesTextChanged++;
                try
                {
                    this.edtNotes.Text = tag.Note;
                    this.edtNotes.SelectionStart = (tag.Note != null) ? tag.Note.Length : 0;
                }
                finally
                {
                    this.lockEdtNotesTextChanged--;
                }
                if (((this.audioControl.ViewMode == Modes.Playback) && (this.audioControl.State != AudioControlState.Recording)) && this.eventChangingByUser)
                {
                    this.audioControl.SeekToEvent(tag);
                }
            }
        }

        private void listPersonsMouseClick(object sender, MouseEventArgs e)
        {
            if ((((this.listPersons.HitTest(e.X, e.Y).Location & ListViewHitTestLocations.Label) == ListViewHitTestLocations.Label) && (this.listPersons.SelectedItems.Count > 0)) && (CaseDocument.RecordInitialized || (this.audioControl.State == AudioControlState.Recording)))
            {
                if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
                {
                    this.MergeEvent(EventType.Person, null, this.listPersons.SelectedItems[0].Tag as Person, null);
                    caseDocument.Modified = true;
                }
                else if (this.AddEvent(new DateTime?(this.GetEventTime()), EventType.Person, null, this.listPersons.SelectedItems[0].Tag as Person, null, null) != null)
                {
                    caseDocument.Modified = true;
                }
            }
        }

        private void LoadFromCaseDocument()
        {
            this.tpCaseDates.SuspendLayout();
            try
            {
                this.lockTemplatesSelectedIndexChange = true;
                try
                {
                    for (int i = this.cbTemplates.Items.Count - 1; i >= 0; i--)
                    {
                        string str = (string) this.cbTemplates.Items[i];
                        if (!(string.IsNullOrEmpty(str) || !str.EndsWith(" *")))
                        {
                            this.cbTemplates.Items.RemoveAt(i);
                        }
                    }
                    foreach (KeyValuePair<DateTime, string> pair in caseDocument.TrialTemplates)
                    {
                        if (!(!pair.Value.EndsWith(" *") || this.cbTemplates.Items.Contains(pair.Value)))
                        {
                            this.cbTemplates.Items.Insert(0, pair.Value);
                        }
                    }
                }
                finally
                {
                    this.lockTemplatesSelectedIndexChange = false;
                }
                this.tpCaseDates.Items.Clear();
                ToolStripButton button = null;
                ToolStripButton sender = null;
                foreach (DateTime time in caseDocument.TrialDates)
                {
                    button = this.AddCaseDateButton(time);
                    if (!caseDocument.IsTrialDamaged(time))
                    {
                        sender = button;
                    }
                    this.tpCaseDates.Items.Insert(0, button);
                }
                if (sender == null)
                {
                    sender = button;
                }
                this.lockTemplatesSelectedIndexChange = true;
                try
                {
                    this.cbTemplates.SelectedItem = caseDocument.TrialTemplates[CurrentDate];
                    this.SetTemplateItems();
                }
                finally
                {
                    this.lockTemplatesSelectedIndexChange = false;
                }
                this.audioControl.BeginUpdate();
                try
                {
                    this.btnCaseDateClick(sender, new EventArgs());
                    this.audioControl.CurrentDate = CurrentDate;
                    if ((CurrentDate == DateTime.Now.Date) && (this.listEvents.Items.Count == 0))
                    {
                        this.audioControl.ViewMode = Modes.Record;
                    }
                    else
                    {
                        this.audioControl.ViewMode = Modes.Playback;
                    }
                }
                finally
                {
                    this.audioControl.EndUpdate();
                }
                this.lockEdtNotesTextChanged++;
                try
                {
                    this.edtNotes.Clear();
                }
                finally
                {
                    this.lockEdtNotesTextChanged--;
                }
            }
            finally
            {
                this.tpCaseDates.ResumeLayout(true);
            }
        }

        private Event MergeEvent(EventType eventType, string action, Person person, string note)
        {
            if (this.listEvents.SelectedItems.Count > 0)
            {
                ListViewItem item = this.listEvents.SelectedItems[0];
                Event tag = (Event) item.Tag;
                if (CaseDocument.Signature.ContainsKey(tag.Time.Date))
                {
                    MessageBox.Show(this, "ამ თარიღში სხდომა ხელმოწერილია და ცვლილება დაუშვებელია!", "აკრძალული ოპერაცია", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return null;
                }
                if ((((byte) (tag.EventType & EventType.RecordStart)) == 1) || (((byte) (tag.EventType & EventType.RecordEnd)) == 2))
                {
                    return null;
                }
                tag.EventType = (EventType) ((byte) (tag.EventType | eventType));
                if (!string.IsNullOrEmpty(action))
                {
                    tag.Action = action;
                    item.SubItems[1].Text = action;
                }
                if (person != null)
                {
                    tag.Person = person;
                    item.SubItems[2].Text = person.ToString();
                }
                if (!string.IsNullOrEmpty(note))
                {
                    tag.Note = note;
                    item.SubItems[3].Text = note;
                }
                return tag;
            }
            return null;
        }

        private bool NewCaseDocument()
        {
            if (MainForm.caseDocument != null)
            {
                switch (this.QuerySave())
                {
                    case DialogResult.Yes:
                        this.SaveCaseDocument();
                        break;

                    case DialogResult.Cancel:
                        return false;
                }
            }
            using (NewTrialDialog dialog = new NewTrialDialog(NewTrialDialog.DialogType.New))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    AudioIO.WorkingDirectory = AudioIO.ArchiveDirectory;
                    CourtProcessRecorder.DataTypes.CaseDocument caseDocument = dialog.CaseDocument;
                    if (caseDocument != null)
                    {
                        if (MessageBox.Show(Form.ActiveForm, "ასეთი საქმის ნომერით სხდომა უკვე არსებობს! გსურთ გააგრძელოთ არსებული სხდომა?", "შენიშვნა", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                        {
                            MainForm.caseDocument = caseDocument;
                            currentDate = MainForm.caseDocument.TrialDates[MainForm.caseDocument.TrialDates.Length - 1];
                            this.cbTemplates.SelectedItem = MainForm.caseDocument.TrialTemplates[currentDate];
                            this.btnSignature.Enabled = true;
                            AudioIO.SetWorkingPath(AudioIO.WorkingDirectory + MainForm.caseDocument.TrialNo);
                            this.LoadFromCaseDocument();
                            MainForm.caseDocument.Modified = false;
                            return true;
                        }
                        return false;
                    }
                    MainForm.caseDocument = new CourtProcessRecorder.DataTypes.CaseDocument(dialog.TrialNo, dialog.CDSerialNo, dialog.TrialName, dialog.TrialDate);
                    currentDate = MainForm.caseDocument.TrialDates[MainForm.caseDocument.TrialDates.Length - 1];
                    this.btnSignature.Enabled = false;
                    AudioIO.SetWorkingPath(AudioIO.WorkingDirectory + MainForm.caseDocument.TrialNo);
                    this.LoadFromCaseDocument();
                    MainForm.caseDocument.Modified = false;
                    return true;
                }
            }
            return false;
        }

        protected override void OnClosed(EventArgs e)
        {
            this.freeSpaceWatcherTimer.Enabled = false;
            base.OnClosed(e);
            try
            {
                AudioIO.SettingManager.Persist();
            }
            catch
            {
                MessageBox.Show(null, "ვერ მოხერხდა კონფიგურაციის დამახსოვრება!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!e.Cancel)
            {
                switch (this.QuerySave())
                {
                    case DialogResult.Yes:
                        this.SaveCaseDocument();
                        e.Cancel = false;
                        break;

                    case DialogResult.No:
                        e.Cancel = false;
                        break;

                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
            if (this.audioControl.ViewMode == Modes.Playback)
            {
                this.audioControl.AbortRendering();
            }
            AudioIO.Dispose();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.rightSplitContainer.Panel2MinSize = this.audioControl.Size.Height;
            if (AudioIO.ProgramMode == ProgramMode.Playback)
            {
                this.btnManagePersons.Visible = false;
                this.toolStripSeparator9.Visible = false;
                this.btnAddLabel.Visible = false;
                this.btnDecreaseTime.Visible = false;
                this.btnDelete.Visible = false;
                this.btnEditTemplate.Visible = false;
                this.btnIncreaseTime.Visible = false;
                this.btnNew.Visible = false;
                this.btnSave.Visible = AudioIO.ProgramMode != ProgramMode.Playback;
                this.btQA.Visible = false;
                this.cbTemplates.Visible = false;
                this.btnPriorityLogic.Visible = false;
                this.toolStripSeparator2.Visible = false;
                this.toolStripSeparator3.Visible = false;
                this.toolStripSeparator4.Visible = false;
                this.edtNotes.ReadOnly = AudioIO.ProgramMode == ProgramMode.Playback;
                this.listPersons.Location = new Point(0, 0);
                this.listPersons.Size = new Size(this.leftSplitContainer.Panel2.Width, this.leftSplitContainer.Panel2.Height);
                this.leftSplitContainer.Dispose();
                this.btnBurnDisk.Visible = AudioIO.ProgramMode != ProgramMode.Playback;
            }
            else
            {
                AudioIO.UsePriorityLogic = this.btnPriorityLogic.Checked = AudioIO.SettingManager.ReadBoolean("UsePriorityLogic", false);
                this.btnPriorityLogic.Visible = (AudioIO.ProgramMode == ProgramMode.Recording) && AudioIO.Apolo16Mixer.Connected;
                if (TemplatesManager.Templates.Count > 0)
                {
                    foreach (Template template in TemplatesManager.Templates)
                    {
                        this.cbTemplates.Items.Add(template.Name);
                    }
                }
                this.cbTemplates.SelectedItem = CaseDocument.TrialTemplates[CurrentDate];
                if (caseDocument != null)
                {
                    caseDocument.Modified = false;
                }
            }
            this.UpdateControls();
            this.UpdateAudioControlSize();
            this.listEvents.ListViewItemSorter = new ListViewItemComparer();
        }

        public bool OpenCaseDocument(bool filterByCurrentDay)
        {
            using (OpenCaseDocumentDialog dialog = new OpenCaseDocumentDialog(filterByCurrentDay))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    caseDocument = dialog.CaseDocument;
                    currentDate = caseDocument.TrialDates[caseDocument.TrialDates.Length - 1];
                    this.cbTemplates.SelectedItem = caseDocument.TrialTemplates[currentDate];
                    AudioIO.SetWorkingPath(AudioIO.WorkingDirectory + caseDocument.TrialNo);
                    this.LoadFromCaseDocument();
                    this.btnSignature.Enabled = true;
                    caseDocument.Modified = false;
                    return true;
                }
            }
            return false;
        }

        public void OpenCaseDocumentSafe(string caseDocumentFile)
        {
            CourtProcessRecorder.DataTypes.CaseDocument document = CourtProcessRecorder.DataTypes.CaseDocument.Open(caseDocumentFile);
            DateTime time1 = document.TrialDates[document.TrialDates.Length - 1];
            if (document.TrialTemplates[document.TrialDates[document.TrialDates.Length - 1]] != null)
            {
                caseDocument = document;
                currentDate = document.TrialDates[caseDocument.TrialDates.Length - 1];
                this.cbTemplates.SelectedItem = document.TrialTemplates[currentDate];
                AudioIO.SetWorkingPath(AudioIO.WorkingDirectory + document.TrialNo);
                this.LoadFromCaseDocument();
                this.btnSignature.Enabled = true;
                document.Modified = false;
            }
        }

        private void PlaybackCtrlClick(object sender, CtrlClickEventArgs e)
        {
            if (this.listEvents.SelectedItems.Count > 0)
            {
                ListViewItem item = this.listEvents.SelectedItems[0];
                Event tag = (Event) item.Tag;
                if (((((byte) (tag.EventType & EventType.RecordStart)) != 1) && (((byte) (tag.EventType & EventType.RecordEnd)) != 2)) && !CaseDocument.Signature.ContainsKey(e.DateTime.Date))
                {
                    tag.Time = e.DateTime;
                    this.listEvents.SelectedItems[0].Text = tag.Time.ToLongTimeString();
                    this.listEvents.Sort();
                    caseDocument.Modified = true;
                    if ((this.audioControl.ViewMode == Modes.Playback) && (this.audioControl.State != AudioControlState.Recording))
                    {
                        this.audioControl.SeekToEvent(e.DateTime);
                    }
                }
            }
        }

        private DialogResult QuerySave()
        {
            if (AudioIO.ProgramMode == ProgramMode.Playback)
            {
                return DialogResult.No;
            }
            if ((CaseDocument != null) && CaseDocument.Modified)
            {
                return MessageBox.Show(this, "სხდომის მონაცემები შეიცვალა სხდომის შექმნის (გახსნის) შემდგომ. გსურთ შეინახოთ ცვლილებები?", "შეტყობინება", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3);
            }
            return DialogResult.OK;
        }

        private void rightBottomSplitter_SplitterMoved(object sender, SplitterEventArgs e)
        {
            this.UpdateAudioControlSize();
        }

        private bool SaveCaseDocument()
        {
            if (CaseDocument.Events.Count == 0)
            {
                MessageBox.Show("ცარიელი დოკუმენტის შენახვა არ შეიძლება!", "დაუშვებელი ოპერაცია", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
            for (int i = this.cbTemplates.Items.Count - 1; i >= 0; i--)
            {
                string str = (string) this.cbTemplates.Items[i];
                if (!(string.IsNullOrEmpty(str) || !str.EndsWith(" *")))
                {
                    this.cbTemplates.Items.RemoveAt(i);
                }
            }
            foreach (KeyValuePair<DateTime, string> pair in caseDocument.TrialTemplates)
            {
                if (!this.cbTemplates.Items.Contains(pair.Value))
                {
                    this.cbTemplates.Items.Insert(0, pair.Value);
                }
            }
            this.lockTemplatesSelectedIndexChange = true;
            this.cbTemplates.SelectedItem = CaseDocument.TrialTemplates[CurrentDate];
            this.lockTemplatesSelectedIndexChange = false;
            caseDocument.Save();
            return true;
        }

        private void SetTemplateItems()
        {
            if (!this.treeCaseStage.IsDisposed)
            {
                if (caseDocument.TrialTemplates.ContainsKey(CurrentDate))
                {
                    this.treeCaseStage.BuildNodesFromDataTable(caseDocument.Templates.FindByName(caseDocument.TrialTemplates[CurrentDate]).CaseStages);
                }
                else
                {
                    this.treeCaseStage.BuildNodesFromDataTable(null);
                }
            }
            if (caseDocument.TrialTemplates.ContainsKey(CurrentDate))
            {
                this.cbTemplates.ToolTipText = "სხდომის ტიპი: " + caseDocument.TrialTemplates[CurrentDate];
                if (this.tpCaseDates.Items.ContainsKey(currentDate.ToShortDateString()))
                {
                    this.tpCaseDates.Items[currentDate.ToShortDateString()].ToolTipText = currentDate.ToShortDateString() + " " + caseDocument.TrialTemplates[CurrentDate];
                }
            }
            else
            {
                this.cbTemplates.ToolTipText = "სხდომის ტიპი";
            }
            this.UpdateFormCaption();
        }

        private void trayIconMouseClick(object sender, MouseEventArgs e)
        {
            this.trayIcon.ShowBalloonTip(0xbb8);
        }

        private void treeCaseStage_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 2)
            {
                this.treeItemDoubleClicked = true;
            }
            else
            {
                this.treeItemDoubleClicked = false;
            }
        }

        private void treeCaseStageBeforeCollapseOrExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (this.treeItemDoubleClicked)
            {
                e.Cancel = true;
            }
            this.treeItemDoubleClicked = false;
        }

        private void treeCaseStageMouseClick(object sender, MouseEventArgs e)
        {
            TreeViewHitTestInfo info = this.treeCaseStage.HitTest(e.X, e.Y);
            if (((info.Location == TreeViewHitTestLocations.Label) && (info.Node != null)) && ((info.Node != null) && (CaseDocument.RecordInitialized || (this.audioControl.State == AudioControlState.Recording))))
            {
                if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
                {
                    this.MergeEvent(EventType.Stage, info.Node.Text, null, null);
                    caseDocument.Modified = true;
                }
                else if (this.AddEvent(new DateTime?(this.GetEventTime()), EventType.Stage, info.Node.Text, null, null, null) != null)
                {
                    caseDocument.Modified = true;
                }
            }
        }

        private void UpdateAudioControlSize()
        {
            this.rightBottomSplitter.Panel2MinSize = this.audioControl.CalcMinSize();
            this.rightSplitContainer.Panel2MinSize = ((this.rightSplitContainer.SplitterWidth + this.rightBottomSplitter.SplitterWidth) + this.rightBottomSplitter.Panel1MinSize) + this.rightBottomSplitter.Panel2MinSize;
        }

        private void UpdateControls()
        {
            if (this.cbTemplates.SelectedIndex == -1)
            {
                this.btnSave.Enabled = false;
                this.btnViewOrder.Enabled = false;
                this.btnBurnDisk.Enabled = false;
                this.audioControl.Enabled = false;
                this.btnManagePersons.Enabled = false;
            }
            else
            {
                this.btnSave.Enabled = true;
                this.btnViewOrder.Enabled = true;
                this.btnBurnDisk.Enabled = true;
                this.audioControl.Enabled = true;
                this.btnManagePersons.Enabled = true;
            }
        }

        public void UpdateFormCaption()
        {
            string str = (caseDocument == null) ? "'არ აქვს მინიჭებული'" : caseDocument.TrialNo;
            string str2 = (caseDocument == null) ? "'არ აქვს მინიჭებული'" : caseDocument.TrialDate.ToShortDateString();
            string str3 = ((caseDocument == null) || !caseDocument.TrialTemplates.ContainsKey(CurrentDate)) ? "'არ აქვს მინიჭებული'" : caseDocument.TrialTemplates[CurrentDate];
            this.Text = string.Format("{0} - საქმის №{1}, {2}-დან ({3})", new object[] { "სხდომის ჟურნალი", str, str2, str3 });
        }

        private void UpdateSignatureDependedControls()
        {
            bool flag = caseDocument.Signature.ContainsKey(CurrentDate);
            this.edtNotes.ReadOnly = flag;
            this.btnManagePersons.Visible = this.btnManagePersons.Enabled = !flag;
            this.btnEditTemplate.Visible = this.btnEditTemplate.Enabled = !flag;
            this.toolStripSeparator9.Visible = !flag;
            this.toolStripSeparator3.Visible = !flag;
            this.cbTemplates.Enabled = !flag;
            this.btQA.Visible = this.btQA.Enabled = !flag;
            this.btnAddLabel.Visible = this.btnAddLabel.Enabled = !flag;
            this.btnDelete.Visible = this.btnDelete.Enabled = !flag;
            this.btnDecreaseTime.Visible = this.btnDecreaseTime.Enabled = !flag;
            this.btnIncreaseTime.Visible = this.btnIncreaseTime.Enabled = !flag;
        }

        private void UpdateSignButton(DateTime date)
        {
            foreach (ToolStripItem item in this.tpCaseDates.Items)
            {
                if (item is ToolStripButton)
                {
                    ToolStripButton button = item as ToolStripButton;
                    if (((DateTime) button.Tag) == date)
                    {
                        button.Image = Resources.lock16;
                        button.ImageTransparentColor = Resources.lock16.GetPixel(0, 0);
                        break;
                    }
                }
            }
            this.UpdateSignatureDependedControls();
        }

        public static CourtProcessRecorder.DataTypes.CaseDocument CaseDocument
        {
            get
            {
                return caseDocument;
            }
        }

        public static DateTime CurrentDate
        {
            get
            {
                return currentDate;
            }
        }

        private class ListViewItemComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                Event tag = (Event) ((ListViewItem) x).Tag;
                Event event3 = (Event) ((ListViewItem) y).Tag;
                return DateTime.Compare(tag.Time, event3.Time);
            }
        }
    }
}

