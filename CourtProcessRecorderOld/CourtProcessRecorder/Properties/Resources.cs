﻿namespace CourtProcessRecorder.Properties
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.Resources;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
    internal class Resources
    {
        private static CultureInfo resourceCulture;
        private static System.Resources.ResourceManager resourceMan;

        internal Resources()
        {
        }

        internal static Bitmap _lock
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("lock", resourceCulture);
            }
        }

        internal static Bitmap address_book16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("address_book16_h", resourceCulture);
            }
        }

        internal static Bitmap align_center
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("align center", resourceCulture);
            }
        }

        internal static Bitmap align_centre16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("align_centre16_h", resourceCulture);
            }
        }

        internal static Bitmap align_full
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("align full", resourceCulture);
            }
        }

        internal static Bitmap align_justify16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("align_justify16_h", resourceCulture);
            }
        }

        internal static Bitmap align_left
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("align left", resourceCulture);
            }
        }

        internal static Bitmap align_left16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("align_left16_h", resourceCulture);
            }
        }

        internal static Bitmap align_right
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("align right", resourceCulture);
            }
        }

        internal static Bitmap align_right16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("align_right16_h", resourceCulture);
            }
        }

        internal static Bitmap arrowdown_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("arrowdown_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap arrowleft_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("arrowleft_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap arrowright_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("arrowright_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap arrowup_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("arrowup_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap bold_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("bold_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap bullets16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("bullets16_h", resourceCulture);
            }
        }

        internal static Bitmap cd16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("cd16_h", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static Bitmap db_delete
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("db delete", resourceCulture);
            }
        }

        internal static Bitmap db_insert
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("db insert", resourceCulture);
            }
        }

        internal static Bitmap decrease_indent16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("decrease_indent16_h", resourceCulture);
            }
        }

        internal static Bitmap delete16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("delete16_h", resourceCulture);
            }
        }

        internal static Bitmap down16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("down16_h", resourceCulture);
            }
        }

        internal static Bitmap edit16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("edit16_h", resourceCulture);
            }
        }

        internal static Bitmap edit16_h1
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("edit16_h1", resourceCulture);
            }
        }

        internal static Bitmap find_nextb24_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("find_nextb24_h", resourceCulture);
            }
        }

        internal static Bitmap goto_line
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("goto line", resourceCulture);
            }
        }

        internal static Bitmap goto_line___style_2
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("goto line - style 2", resourceCulture);
            }
        }

        internal static Bitmap help16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("help16_h", resourceCulture);
            }
        }

        internal static ImageListStreamer ImageStream16
        {
            get
            {
                return (ImageListStreamer) ResourceManager.GetObject("ImageStream16", resourceCulture);
            }
        }

        internal static Bitmap increase_indent16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("increase_indent16_h", resourceCulture);
            }
        }

        internal static Bitmap italic_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("italic_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap lock16
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("lock16", resourceCulture);
            }
        }

        internal static Bitmap minus16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("minus16_h", resourceCulture);
            }
        }

        internal static Bitmap minus24_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("minus24_h", resourceCulture);
            }
        }

        internal static Bitmap new_project24_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("new_project24_h", resourceCulture);
            }
        }

        internal static Bitmap new16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("new16_h", resourceCulture);
            }
        }

        internal static Bitmap number_pages
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("number pages", resourceCulture);
            }
        }

        internal static Bitmap open_project24_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("open_project24_h", resourceCulture);
            }
        }

        internal static Bitmap open16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("open16_h", resourceCulture);
            }
        }

        internal static Bitmap open24_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("open24_h", resourceCulture);
            }
        }

        internal static Bitmap paragraph_2
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("paragraph-2", resourceCulture);
            }
        }

        internal static string ParagraphAction
        {
            get
            {
                return ResourceManager.GetString("ParagraphAction", resourceCulture);
            }
        }

        internal static string ParagraphActionNote
        {
            get
            {
                return ResourceManager.GetString("ParagraphActionNote", resourceCulture);
            }
        }

        internal static string ParagraphActionPerson
        {
            get
            {
                return ResourceManager.GetString("ParagraphActionPerson", resourceCulture);
            }
        }

        internal static string ParagraphActionPersonNote
        {
            get
            {
                return ResourceManager.GetString("ParagraphActionPersonNote", resourceCulture);
            }
        }

        internal static string ParagraphNote
        {
            get
            {
                return ResourceManager.GetString("ParagraphNote", resourceCulture);
            }
        }

        internal static string ParagraphPerson
        {
            get
            {
                return ResourceManager.GetString("ParagraphPerson", resourceCulture);
            }
        }

        internal static string ParagraphPersonNote
        {
            get
            {
                return ResourceManager.GetString("ParagraphPersonNote", resourceCulture);
            }
        }

        internal static string ParagraphStage
        {
            get
            {
                return ResourceManager.GetString("ParagraphStage", resourceCulture);
            }
        }

        internal static Bitmap pause_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("pause_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap pause_orange16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("pause_orange16_h", resourceCulture);
            }
        }

        internal static Bitmap play_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("play_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap plus16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("plus16_h", resourceCulture);
            }
        }

        internal static Bitmap plus24
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("plus24", resourceCulture);
            }
        }

        internal static Bitmap print_preview
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("print preview", resourceCulture);
            }
        }

        internal static Bitmap record_blue24b_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("record_blue24b_h", resourceCulture);
            }
        }

        internal static Bitmap record_orange16b_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("record_orange16b_h", resourceCulture);
            }
        }

        internal static Bitmap redo_square16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("redo_square16_h", resourceCulture);
            }
        }

        internal static Bitmap Rename
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("Rename", resourceCulture);
            }
        }

        internal static Bitmap rename16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("rename16_h", resourceCulture);
            }
        }

        internal static Bitmap rename24_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("rename24_h", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager manager = new System.Resources.ResourceManager("CourtProcessRecorder.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }

        internal static Bitmap save16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("save16_h", resourceCulture);
            }
        }

        internal static Bitmap select_alltext16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("select_alltext16_h", resourceCulture);
            }
        }

        internal static Bitmap select16
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("select16", resourceCulture);
            }
        }

        internal static Bitmap soundnote_double16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("soundnote_double16_h", resourceCulture);
            }
        }

        internal static Bitmap speaker16
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("speaker16", resourceCulture);
            }
        }

        internal static Bitmap stop_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("stop_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap subscript16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("subscript16_h", resourceCulture);
            }
        }

        internal static Bitmap superscript16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("superscript16_h", resourceCulture);
            }
        }

        internal static Bitmap transfer_incoming16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("transfer_incoming16_h", resourceCulture);
            }
        }

        internal static Bitmap Treeview___Add
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("Treeview - Add", resourceCulture);
            }
        }

        internal static Bitmap Treeview___Delete
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("Treeview - Delete", resourceCulture);
            }
        }

        internal static Bitmap underline_blue16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("underline_blue16_h", resourceCulture);
            }
        }

        internal static Bitmap up16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("up16_h", resourceCulture);
            }
        }

        internal static Bitmap user_add16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("user_add16_h", resourceCulture);
            }
        }

        internal static Bitmap user_add16_h1
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("user_add16_h1", resourceCulture);
            }
        }

        internal static Bitmap user_drop16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("user_drop16_h", resourceCulture);
            }
        }

        internal static Bitmap users16_h
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("users16_h", resourceCulture);
            }
        }

        internal static Bitmap verify_document16
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("verify_document16", resourceCulture);
            }
        }

        internal static Bitmap wait1
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("wait1", resourceCulture);
            }
        }

        internal static Bitmap wizard24
        {
            get
            {
                return (Bitmap) ResourceManager.GetObject("wizard24", resourceCulture);
            }
        }
    }
}

