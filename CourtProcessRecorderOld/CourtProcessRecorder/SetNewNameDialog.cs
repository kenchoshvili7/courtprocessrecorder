﻿namespace CourtProcessRecorder
{
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class SetNewNameDialog : BaseForm
    {
        private Button btnCancel;
        private Button btnOk;
        private IContainer components = null;
        private TextBox edtName;
        private bool isSystem;
        private Label label1;

        public SetNewNameDialog(string name)
        {
            this.InitializeComponent();
            this.isSystem = false;
            if (!(string.IsNullOrEmpty(name) || !name.EndsWith(" *")))
            {
                name = name.Substring(0, name.Length - 2);
                this.isSystem = true;
            }
            this.edtName.Text = name;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.edtName.Text))
            {
                MessageBox.Show(this, "დასახელება არ შეიძლება იყოს ცარიელი!", "არასწორი მნიშვნელობა", MessageBoxButtons.OK);
            }
            else if (this.edtName.Text.EndsWith(" *"))
            {
                MessageBox.Show(this, "დასახელება არ შეიძლება მთავრდებოდეს ' *'!", "არასწორი მნიშვნელობა", MessageBoxButtons.OK);
            }
            else
            {
                base.DialogResult = DialogResult.OK;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(SetNewNameDialog));
            this.btnOk = new Button();
            this.btnCancel = new Button();
            this.edtName = new TextBox();
            this.label1 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new EventHandler(this.btnOk_Click);
            this.btnCancel.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.edtName, "edtName");
            this.edtName.Name = "edtName";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            base.AcceptButton = this.btnOk;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnCancel;
            manager.ApplyResources(this, "$this");
            base.Controls.Add(this.label1);
            base.Controls.Add(this.edtName);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.btnOk);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "SetNewNameDialog";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string TemplateName
        {
            get
            {
                if (this.isSystem)
                {
                    return (this.edtName.Text + " *");
                }
                return this.edtName.Text;
            }
        }
    }
}

