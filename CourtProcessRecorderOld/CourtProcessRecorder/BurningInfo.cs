﻿namespace CourtProcessRecorder
{
    using IMAPI2.Interop;
    using System;

    public class BurningInfo
    {
        public CourtProcessRecorder.BurnStage BurnStage;
        public IMAPI_FORMAT2_DATA_WRITE_ACTION CurrentAction;
        public int CurrentWriteSpeed;
        public int ElapsedTime;
        public int FreeSystemBuffer;
        public int LastReadLba;
        public int LastWrittenLba;
        public string ProgressText;
        public int RemainingTime;
        public int SectorCount;
        public int StartLba;
        public int TotalSystemBuffer;
        public int TotalTime;
        public int UsedSystemBuffer;
    }
}

