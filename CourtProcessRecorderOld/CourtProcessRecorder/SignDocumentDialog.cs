﻿namespace CourtProcessRecorder
{
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class SignDocumentDialog : BaseForm
    {
        private Button btnCancel;
        private Button btnOk;
        private IContainer components = null;
        private TextBox edtOrganization;
        private TextBox edtSigner;
        private Label label1;
        private Label label2;

        public SignDocumentDialog()
        {
            this.InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.edtSigner.Text))
            {
                MessageBox.Show("შეიყვანეთ ხელმომწერი!", "არასწორი მნიშვნელობა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else if (string.IsNullOrEmpty(this.edtOrganization.Text))
            {
                MessageBox.Show("შეიყვანეთ ორგანიზაცია!", "არასწორი მნიშვნელობა", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                base.DialogResult = DialogResult.OK;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.btnCancel = new Button();
            this.btnOk = new Button();
            this.edtSigner = new TextBox();
            this.label1 = new Label();
            this.edtOrganization = new TextBox();
            this.label2 = new Label();
            base.SuspendLayout();
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Font = new Font("Sylfaen", 8.25f);
            this.btnCancel.Location = new Point(0x166, 0x72);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x4b, 0x17);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "გაუქმება";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnOk.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnOk.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.btnOk.Location = new Point(0x115, 0x72);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new Size(0x4b, 0x17);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new EventHandler(this.btnOk_Click);
            this.edtSigner.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.edtSigner.Location = new Point(12, 30);
            this.edtSigner.Name = "edtSigner";
            this.edtSigner.Size = new Size(0x1a5, 0x17);
            this.edtSigner.TabIndex = 0;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new Size(80, 0x10);
            this.label1.TabIndex = 0;
            this.label1.Text = "ხელმომწერი";
            this.edtOrganization.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.edtOrganization.Location = new Point(12, 0x51);
            this.edtOrganization.Name = "edtOrganization";
            this.edtOrganization.Size = new Size(0x1a5, 0x17);
            this.edtOrganization.TabIndex = 1;
            this.label2.AutoSize = true;
            this.label2.Location = new Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x53, 0x10);
            this.label2.TabIndex = 0;
            this.label2.Text = "ორგანიზაცია";
            base.AcceptButton = this.btnOk;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x1bd, 0x95);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.edtOrganization);
            base.Controls.Add(this.edtSigner);
            base.Controls.Add(this.btnOk);
            base.Controls.Add(this.btnCancel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "SignDocumentDialog";
            this.Text = "ხელმოწერა";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string Organization
        {
            get
            {
                return this.edtOrganization.Text;
            }
        }

        public string Signer
        {
            get
            {
                return this.edtSigner.Text;
            }
        }
    }
}

