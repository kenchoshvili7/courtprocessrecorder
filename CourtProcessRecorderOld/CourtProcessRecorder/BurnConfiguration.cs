﻿namespace CourtProcessRecorder
{
    using IMAPI2.Interop;
    using System;

    public class BurnConfiguration
    {
        public IDiscRecorder2 DiscRecorder;
        public bool ForceMediaToBeClosed;
        public IWriteSpeedDescriptor WriteSpeedDescriptor;
    }
}

