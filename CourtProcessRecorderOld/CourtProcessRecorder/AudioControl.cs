﻿namespace CourtProcessRecorder
{
    using AudioConfiguration;
    using CourtProcessRecorder.Controls;
    using CourtProcessRecorder.DataTypes;
    using CourtProcessRecorder.Properties;
    using CourtRecorderCommon;
    using DeviceController;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    public class AudioControl : UserControl
    {
        private ToolStripButton btnConfigPlaybackDevice;
        private ToolStripButton btnListen;
        private ToolStripButton btnPlayPause;
        private ToolStripButton btnRecordPause;
        private ToolStripButton btnStopPlayback;
        private IContainer components = null;
        private DateTime currentDate;
        private bool enableRecording;
        private Label labelDate;
        private Label labelTime;
        private ToolStrip mainToolBar;
        private MixerControl mixerControl;
        public PlayBackControl playBackControl;
        private TrackBar tbDeviceVolume;
        private DateTime time;
        private System.Windows.Forms.Timer timer;
        private ToolTip toolTip;
        private int updateLock;
        private Modes viewMode;

        public event CtrlClickEventHandler PlaybackCtrlClick;

        public event RecordEventHandler RecordEvent;

        public event EventHandler Timer;

        public AudioControl()
        {
            this.InitializeComponent();
            this.ViewMode = Modes.Playback;
            this.playBackControl.UpdateTime += new EventHandler(this.TimerTick);
            this.playBackControl.OnStopped += new EventHandler(this.PlaybackStopped);
            if (AudioIO.ProgramMode != ProgramMode.Playback)
            {
                this.playBackControl.CtrlClickEvent += new CtrlClickEventHandler(this.PlaybackCtrlClicked);
            }
            this.tbDeviceVolume.Visible = (AudioIO.ProgramMode == ProgramMode.Recording) && ((AudioIO.UsbInterface.SpecifiedDevice != null) || AudioIO.Apolo16Mixer.Connected);
            if (this.tbDeviceVolume.Visible)
            {
                if (AudioIO.Apolo16Mixer.Connected)
                {
                    this.tbDeviceVolume.Maximum = 0xff;
                    this.tbDeviceVolume.TickFrequency = 0x20;
                    this.tbDeviceVolume.Value = AudioIO.Apolo16Mixer.GetVolume(MixerChannels.Monitor);
                }
                else
                {
                    this.tbDeviceVolume.Value = AudioIO.UsbInterface.GetVolume();
                }
            }
        }

        public void AbortRendering()
        {
            this.playBackControl.AbortRendering();
        }

        public void BeginUpdate()
        {
            this.updateLock++;
        }

        private void btnConfigPlaybackDeviceClick(object sender, EventArgs e)
        {
            using (ConfigPlaybackDeviceDialog dialog = new ConfigPlaybackDeviceDialog())
            {
                dialog.ShowDialog();
            }
        }

        private void btnListenClick(object sender, EventArgs e)
        {
            AudioIO.ListenRecorder(this.btnListen.Checked);
        }

        private void btnPlaybackStopClick(object sender, EventArgs e)
        {
            this.btnStopPlayback.Enabled = false;
            this.playBackControl.Stop();
            this.btnPlayPause.Image = Resources.play_blue16_h;
            this.btnPlayPause.Checked = false;
            this.btnRecordPause.Enabled = AudioIO.IsRecordingPossible() && this.enableRecording;
            this.btnConfigPlaybackDevice.Visible = (this.ViewMode == Modes.Playback) && (this.State == AudioControlState.Idle);
        }

        private void btnPlayPauseClick(object sender, EventArgs e)
        {
            this.btnStopPlayback.Enabled = true;
            if (this.btnPlayPause.Checked)
            {
                this.playBackControl.Play();
                this.btnPlayPause.Image = Resources.pause_blue16_h;
                this.btnRecordPause.Enabled = false;
                this.mixerControl.UpdateControlValues();
                this.btnConfigPlaybackDevice.Visible = (this.ViewMode == Modes.Playback) && (this.State == AudioControlState.Idle);
            }
            else
            {
                this.playBackControl.Pause();
                this.btnPlayPause.Image = Resources.play_blue16_h;
                this.btnConfigPlaybackDevice.Visible = (this.ViewMode == Modes.Playback) && (this.State == AudioControlState.Idle);
            }
        }

        private void btnRecordPauseClick(object sender, EventArgs e)
        {
            this.BeginUpdate();
            try
            {
                AudioIO.Enabled = true;
                RecordEventArgs recordEventArgs = new RecordEventArgs();
                if (this.btnRecordPause.Checked)
                {
                    AudioIO.SelectedAudioChannels = AudioSourceChannels.None;
                    if (!AudioIO.StartRecord(AudioIO.SelectedAudioChannels))
                    {
                        AudioIO.SelectedAudioChannels = AudioSourceChannels.ChannelPC;
                        this.btnRecordPause.Checked = false;
                        throw new ApplicationException("ვერ ხერხდება ჩაწერა, შეამოწმეთ კონფიგურაცია!");
                    }
                    recordEventArgs.RecordSet = AudioIO.CurrentRecordSet;
                    recordEventArgs.RecordEvent = RecordEvents.Start;
                    this.btnRecordPause.Image = Resources.pause_orange16_h;
                    this.ViewMode = Modes.Record;
                    if (AudioIO.Configuration.ProgramConfiguration == ProgramConfiguration.SingleChannel)
                    {
                        this.mixerControl.SelectRecordingPriorityChannel();
                    }
                    else if (AudioIO.Apolo16Mixer.Connected)
                    {
                        this.mixerControl.SelectRecordingPriorityChannel();
                        Apolo16Mixer mixer1 = AudioIO.Apolo16Mixer;
                        mixer1.Mute = (MixerChannels) ((byte) (((int) mixer1.Mute) & 0xbf));
                    }
                    this.OnRecordEvent(recordEventArgs);
                }
                else
                {
                    recordEventArgs.RecordEvent = RecordEvents.Stop;
                    if (recordEventArgs.RecordEvent == RecordEvents.Stop)
                    {
                        AudioIO.StopRecord();
                    }
                    this.ViewMode = Modes.Playback;
                    this.btnRecordPause.Image = Resources.record_orange16b_h;
                    this.btnRecordPause.Checked = false;
                    this.OnRecordEvent(recordEventArgs);
                }
            }
            finally
            {
                this.EndUpdate();
            }
        }

        public int CalcMinSize()
        {
            return (this.mixerControl.Controls.Count * 0x2a);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void EnableRecording(bool enable)
        {
            this.enableRecording = enable;
            this.btnRecordPause.Enabled = this.btnRecordPause.Visible = enable;
        }

        public void EndUpdate()
        {
            if (this.updateLock > 0)
            {
                this.updateLock--;
            }
            if (this.updateLock == 0)
            {
                this.btnPlaybackStopClick(null, null);
                this.UpdateControls();
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(AudioControl));
            this.labelTime = new Label();
            this.labelDate = new Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.mainToolBar = new ToolStrip();
            this.btnRecordPause = new ToolStripButton();
            this.btnListen = new ToolStripButton();
            this.btnPlayPause = new ToolStripButton();
            this.btnStopPlayback = new ToolStripButton();
            this.btnConfigPlaybackDevice = new ToolStripButton();
            this.toolTip = new ToolTip(this.components);
            this.tbDeviceVolume = new TrackBar();
            this.playBackControl = new PlayBackControl();
            this.mixerControl = new MixerControl();
            this.mainToolBar.SuspendLayout();
            this.tbDeviceVolume.BeginInit();
            base.SuspendLayout();
            this.labelTime.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.labelTime.Font = new Font("Tahoma", 21.75f, FontStyle.Bold, GraphicsUnit.Point, 0xcc);
            this.labelTime.Location = new Point(0x114, 0);
            this.labelTime.Margin = new Padding(0);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new Size(0x98, 0x23);
            this.labelTime.TabIndex = 0;
            this.labelTime.TextAlign = ContentAlignment.MiddleCenter;
            this.labelDate.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.labelDate.Font = new Font("Tahoma", 12f, FontStyle.Bold, GraphicsUnit.Point, 0xcc);
            this.labelDate.Location = new Point(0x11d, 0x23);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new Size(0x8f, 0x17);
            this.labelDate.TabIndex = 0;
            this.labelDate.TextAlign = ContentAlignment.MiddleCenter;
            this.timer.Enabled = true;
            this.timer.Interval = 0x3e8;
            this.timer.Tick += new EventHandler(this.TimerTick);
            this.mainToolBar.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.mainToolBar.AutoSize = false;
            this.mainToolBar.Dock = DockStyle.None;
            this.mainToolBar.GripStyle = ToolStripGripStyle.Hidden;
            this.mainToolBar.Items.AddRange(new ToolStripItem[] { this.btnRecordPause, this.btnListen, this.btnPlayPause, this.btnStopPlayback, this.btnConfigPlaybackDevice });
            this.mainToolBar.Location = new Point(0x123, 0x3a);
            this.mainToolBar.Name = "mainToolBar";
            this.mainToolBar.Padding = new Padding(0);
            this.mainToolBar.Size = new Size(0x8f, 0x1d);
            this.mainToolBar.TabIndex = 2;
            this.mainToolBar.Text = "toolStrip1";
            this.btnRecordPause.Alignment = ToolStripItemAlignment.Right;
            this.btnRecordPause.AutoSize = false;
            this.btnRecordPause.CheckOnClick = true;
            this.btnRecordPause.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnRecordPause.Image = Resources.record_orange16b_h;
            this.btnRecordPause.ImageTransparentColor = Color.Magenta;
            this.btnRecordPause.Name = "btnRecordPause";
            this.btnRecordPause.Size = new Size(0x20, 0x20);
            this.btnRecordPause.Text = "ჩაწერა";
            this.btnRecordPause.Click += new EventHandler(this.btnRecordPauseClick);
            this.btnListen.Alignment = ToolStripItemAlignment.Right;
            this.btnListen.AutoSize = false;
            this.btnListen.Checked = true;
            this.btnListen.CheckOnClick = true;
            this.btnListen.CheckState = CheckState.Checked;
            this.btnListen.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnListen.Image = (Image) manager.GetObject("btnListen.Image");
            this.btnListen.ImageTransparentColor = Color.Magenta;
            this.btnListen.Name = "btnListen";
            this.btnListen.Size = new Size(0x20, 0x20);
            this.btnListen.Text = "პირდაპირი მოსმენა";
            this.btnListen.Visible = false;
            this.btnListen.Click += new EventHandler(this.btnListenClick);
            this.btnPlayPause.AutoSize = false;
            this.btnPlayPause.CheckOnClick = true;
            this.btnPlayPause.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnPlayPause.Image = Resources.play_blue16_h;
            this.btnPlayPause.ImageTransparentColor = Color.Magenta;
            this.btnPlayPause.Name = "btnPlayPause";
            this.btnPlayPause.Size = new Size(0x20, 0x20);
            this.btnPlayPause.Text = "მოსმენა";
            this.btnPlayPause.Visible = false;
            this.btnPlayPause.Click += new EventHandler(this.btnPlayPauseClick);
            this.btnStopPlayback.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnStopPlayback.Enabled = false;
            this.btnStopPlayback.Image = Resources.stop_blue16_h;
            this.btnStopPlayback.ImageTransparentColor = Color.Magenta;
            this.btnStopPlayback.Name = "btnStopPlayback";
            this.btnStopPlayback.Size = new Size(0x17, 0x1a);
            this.btnStopPlayback.Text = "გაჩერება";
            this.btnStopPlayback.Visible = false;
            this.btnStopPlayback.Click += new EventHandler(this.btnPlaybackStopClick);
            this.btnConfigPlaybackDevice.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.btnConfigPlaybackDevice.Image = Resources.speaker16;
            this.btnConfigPlaybackDevice.ImageTransparentColor = Color.Magenta;
            this.btnConfigPlaybackDevice.Name = "btnConfigPlaybackDevice";
            this.btnConfigPlaybackDevice.Size = new Size(0x17, 20);
            this.btnConfigPlaybackDevice.Text = "სასმენი მოწყობილობა";
            this.btnConfigPlaybackDevice.Click += new EventHandler(this.btnConfigPlaybackDeviceClick);
            this.toolTip.AutoPopDelay = 0x1388;
            this.toolTip.InitialDelay = 0x3e8;
            this.toolTip.ReshowDelay = 100;
            this.tbDeviceVolume.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.tbDeviceVolume.AutoSize = false;
            this.tbDeviceVolume.Location = new Point(0x123, 90);
            this.tbDeviceVolume.Maximum = 4;
            this.tbDeviceVolume.Name = "tbDeviceVolume";
            this.tbDeviceVolume.Size = new Size(0x8f, 0x20);
            this.tbDeviceVolume.TabIndex = 5;
            this.toolTip.SetToolTip(this.tbDeviceVolume, "ხმა დარბაზში");
            this.tbDeviceVolume.ValueChanged += new EventHandler(this.tbDeviceVolumeValueChanged);
            this.playBackControl.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.playBackControl.CurrentDate = new DateTime(0L);
            this.playBackControl.Location = new Point(0x85, 0);
            this.playBackControl.Margin = new Padding(0);
            this.playBackControl.Name = "playBackControl";
            this.playBackControl.Size = new Size(0x98, 150);
            this.playBackControl.TabIndex = 4;
            this.playBackControl.Time = new DateTime(0L);
            this.mixerControl.Dock = DockStyle.Left;
            this.mixerControl.Location = new Point(0, 0);
            this.mixerControl.Margin = new Padding(0);
            this.mixerControl.MinimumSize = new Size(0x85, 150);
            this.mixerControl.Name = "mixerControl";
            this.mixerControl.Size = new Size(0x85, 150);
            this.mixerControl.TabIndex = 3;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.Controls.Add(this.tbDeviceVolume);
            base.Controls.Add(this.playBackControl);
            base.Controls.Add(this.mixerControl);
            base.Controls.Add(this.labelTime);
            base.Controls.Add(this.labelDate);
            base.Controls.Add(this.mainToolBar);
            this.MinimumSize = new Size(0x1b2, 150);
            base.Name = "AudioControl";
            base.Size = new Size(0x1b2, 150);
            this.mainToolBar.ResumeLayout(false);
            this.mainToolBar.PerformLayout();
            this.tbDeviceVolume.EndInit();
            base.ResumeLayout(false);
        }

        private void OnRecordEvent(RecordEventArgs recordEventArgs)
        {
            if (this.RecordEvent != null)
            {
                this.RecordEvent(this, recordEventArgs);
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.playBackControl.Left = this.mixerControl.Right;
            this.playBackControl.Width = this.labelDate.Left - this.playBackControl.Left;
            this.mainToolBar.Location = new Point(this.playBackControl.Right, this.labelDate.Bottom);
            this.mainToolBar.Size = new Size(base.Width - this.playBackControl.Right, 0x24);
            this.tbDeviceVolume.Location = new Point(this.playBackControl.Right, this.mainToolBar.Bottom);
            this.tbDeviceVolume.Size = new Size(base.Width - this.playBackControl.Right, this.tbDeviceVolume.Height);
        }

        private void OnTimer()
        {
            if (this.Timer != null)
            {
                this.Timer(this, new EventArgs());
            }
        }

        public void PlaybackCtrlClicked(object sender, CtrlClickEventArgs e)
        {
            if (this.PlaybackCtrlClick != null)
            {
                this.PlaybackCtrlClick(sender, e);
            }
        }

        private void PlaybackStopped(object sender, EventArgs e)
        {
            this.btnPlaybackStopClick(sender, e);
        }

        public void SeekToEvent(Event evt)
        {
            this.playBackControl.SeekToEvent(evt);
        }

        public void SeekToEvent(DateTime dt)
        {
            this.playBackControl.SeekToEvent(dt);
        }

        private void tbDeviceVolumeValueChanged(object sender, EventArgs e)
        {
            if (AudioIO.Apolo16Mixer.Connected)
            {
                AudioIO.Apolo16Mixer.SetVolume(MixerChannels.Monitor, (byte) this.tbDeviceVolume.Value);
            }
            else
            {
                AudioIO.UsbInterface.SetVolume((byte) this.tbDeviceVolume.Value);
            }
        }

        private void TimerTick(object sender, EventArgs e)
        {
            if (!(!this.btnRecordPause.Checked && this.playBackControl.HasData()))
            {
                this.Time = DateTime.Now;
            }
            else if (sender == this.playBackControl)
            {
                this.Time = this.playBackControl.Time;
            }
            this.OnTimer();
        }

        private void UpdateControls()
        {
            if (this.updateLock == 0)
            {
                this.playBackControl.Visible = (this.ViewMode == Modes.Playback) && this.playBackControl.HasData();
                this.btnPlayPause.Visible = (this.ViewMode == Modes.Playback) && this.playBackControl.HasData();
                this.btnStopPlayback.Visible = (this.ViewMode == Modes.Playback) && this.playBackControl.HasData();
                this.btnPlayPause.Enabled = this.playBackControl.HasData();
                this.timer.Enabled = this.ViewMode == Modes.Record;
                this.btnRecordPause.Visible = AudioIO.IsRecordingPossible() && this.enableRecording;
                if (this.ViewMode == Modes.Playback)
                {
                    this.mixerControl.UpdateMixer();
                }
                if (this.ViewMode == Modes.Playback)
                {
                    this.playBackControl.UpdateChannelData();
                }
                this.btnConfigPlaybackDevice.Visible = (this.ViewMode == Modes.Playback) && this.playBackControl.HasData();
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime CurrentDate
        {
            get
            {
                return this.currentDate;
            }
            set
            {
                if (this.currentDate != value)
                {
                    this.currentDate = value;
                    this.playBackControl.CurrentDate = value;
                    this.mixerControl.CurrentDate = value;
                    this.UpdateControls();
                }
            }
        }

        public AudioControlState State
        {
            get
            {
                if (this.btnRecordPause.Checked)
                {
                    return AudioControlState.Recording;
                }
                if (this.btnStopPlayback.Enabled)
                {
                    return AudioControlState.Playing;
                }
                return AudioControlState.Idle;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime Time
        {
            get
            {
                return this.time;
            }
            set
            {
                if (this.time != value)
                {
                    this.time = value;
                    this.labelTime.Text = this.time.ToLongTimeString();
                    this.labelDate.Text = this.time.ToShortDateString();
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Modes ViewMode
        {
            get
            {
                return this.viewMode;
            }
            set
            {
                if (AudioIO.ProgramMode != ProgramMode.Recording)
                {
                    value = Modes.Playback;
                }
                this.viewMode = value;
                this.btnPlaybackStopClick(null, null);
                this.mixerControl.ViewMode = value;
                this.UpdateControls();
            }
        }
    }
}

