﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    public class NewTrialDialog : BaseForm
    {
        private Button btnCancel;
        private Button btnOK;
        private DateTimePicker cbTrialDate;
        private ComboBox cbTrialNo;
        private IContainer components = null;
        private DialogType dialogType;
        private TextBox edtCDSerialNo;
        private TextBox edtTitle;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label labelCaseNo;

        public NewTrialDialog(DialogType dialogType)
        {
            this.dialogType = dialogType;
            if (!MainForm.MainFormInstance.Visible)
            {
                base.Icon = MainForm.MainFormInstance.Icon;
                base.ShowInTaskbar = true;
            }
            this.InitializeComponent();
            if (dialogType == DialogType.New)
            {
                this.edtCDSerialNo.Visible = this.label3.Visible = false;
                base.Height -= (this.edtCDSerialNo.Height + this.label3.Height) + 10;
                this.cbTrialNo.MaxLength = 0x10;
                this.cbTrialDate.Enabled = false;
                this.cbTrialDate.Value = DateTime.Today;
            }
        }

        private void btnOKClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.TrialNo) || string.IsNullOrEmpty(this.TrialNo.Trim()))
            {
                MessageBox.Show(this, "სხდომის № არ შეიძლება იყოს ცარიელი!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!((this.dialogType != DialogType.New) || CourtProcessRecorder.DataTypes.CaseDocument.IsValidTrialNo(this.TrialNo)))
            {
                MessageBox.Show(this, string.Format("სხდომის № არ შეიძლება შეიცავდეს სიმბოლოებს {0}{1} სიმბოლოების რაოდენობა არ უნდა აღემატებოდეს {2}!", ", \\ / : ; ? * < > | ~ \"", Environment.NewLine, 0x10), "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                base.DialogResult = DialogResult.OK;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.btnOK = new Button();
            this.btnCancel = new Button();
            this.edtCDSerialNo = new TextBox();
            this.cbTrialDate = new DateTimePicker();
            this.cbTrialNo = new ComboBox();
            this.edtTitle = new TextBox();
            this.labelCaseNo = new Label();
            this.label1 = new Label();
            this.label2 = new Label();
            this.label3 = new Label();
            base.SuspendLayout();
            this.btnOK.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnOK.Location = new Point(0x143, 12);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new Size(0x60, 0x1c);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new EventHandler(this.btnOKClick);
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Location = new Point(0x143, 0x2e);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x60, 0x1c);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "გაუქმება";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.edtCDSerialNo.Location = new Point(12, 0xca);
            this.edtCDSerialNo.Name = "edtCDSerialNo";
            this.edtCDSerialNo.Size = new Size(0x197, 0x17);
            this.edtCDSerialNo.TabIndex = 7;
            this.cbTrialDate.Format = DateTimePickerFormat.Short;
            this.cbTrialDate.Location = new Point(0xcd, 0x1d);
            this.cbTrialDate.Name = "cbTrialDate";
            this.cbTrialDate.Size = new Size(0x5f, 0x17);
            this.cbTrialDate.TabIndex = 3;
            this.cbTrialNo.FormattingEnabled = true;
            this.cbTrialNo.Location = new Point(12, 0x1d);
            this.cbTrialNo.Name = "cbTrialNo";
            this.cbTrialNo.Size = new Size(0xbb, 0x18);
            this.cbTrialNo.TabIndex = 1;
            this.edtTitle.AcceptsReturn = true;
            this.edtTitle.Location = new Point(12, 0x56);
            this.edtTitle.Multiline = true;
            this.edtTitle.Name = "edtTitle";
            this.edtTitle.Size = new Size(0x197, 90);
            this.edtTitle.TabIndex = 5;
            this.labelCaseNo.AutoSize = true;
            this.labelCaseNo.Location = new Point(12, 9);
            this.labelCaseNo.Name = "labelCaseNo";
            this.labelCaseNo.Size = new Size(0x48, 0x10);
            this.labelCaseNo.TabIndex = 0;
            this.labelCaseNo.Text = "სხდომის №";
            this.label1.AutoSize = true;
            this.label1.Location = new Point(0xca, 9);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x6c, 0x10);
            this.label1.TabIndex = 2;
            this.label1.Text = "სხდომის თარიღი";
            this.label2.AutoSize = true;
            this.label2.Location = new Point(12, 0x41);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x7f, 0x10);
            this.label2.TabIndex = 4;
            this.label2.Text = "სხდომის დასახელება";
            this.label3.AutoSize = true;
            this.label3.Location = new Point(12, 0xb5);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0xcb, 0x10);
            this.label3.TabIndex = 6;
            this.label3.Text = "კომპაქტ-დისკის სერიული ნომერი";
            base.AcceptButton = this.btnOK;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnCancel;
            base.ClientSize = new Size(0x1af, 0xfe);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.labelCaseNo);
            base.Controls.Add(this.edtTitle);
            base.Controls.Add(this.cbTrialNo);
            base.Controls.Add(this.cbTrialDate);
            base.Controls.Add(this.edtCDSerialNo);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.btnOK);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "NewTrialDialog";
            this.Text = "ახალი სხდომა";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            foreach (string str in Directory.GetFiles(AudioIO.ArchiveDirectory, "*.cpd"))
            {
                try
                {
                    CourtProcessRecorder.DataTypes.CaseDocument document = CourtProcessRecorder.DataTypes.CaseDocument.Open(str);
                    this.cbTrialNo.Items.Add(new CaseItem(document.TrialNo, document));
                }
                catch
                {
                }
            }
        }

        public CourtProcessRecorder.DataTypes.CaseDocument CaseDocument
        {
            get
            {
                string trialNo = this.TrialNo;
                foreach (CaseItem item in this.cbTrialNo.Items)
                {
                    if (item.TrialNo == trialNo)
                    {
                        return item.CaseDocument;
                    }
                }
                return null;
            }
        }

        public string CDSerialNo
        {
            get
            {
                return this.edtCDSerialNo.Text;
            }
        }

        public DateTime TrialDate
        {
            get
            {
                return this.cbTrialDate.Value.Date;
            }
        }

        public string TrialName
        {
            get
            {
                return this.edtTitle.Text;
            }
        }

        public string TrialNo
        {
            get
            {
                return this.cbTrialNo.Text;
            }
        }

        public enum DialogType
        {
            New,
            Search
        }
    }
}

