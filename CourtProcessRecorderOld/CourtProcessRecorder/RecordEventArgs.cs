﻿namespace CourtProcessRecorder
{
    using CourtRecorderCommon;
    using System;

    public class RecordEventArgs : EventArgs
    {
        public RecordEvents RecordEvent;
        public CourtRecorderCommon.RecordSet RecordSet;
    }
}

