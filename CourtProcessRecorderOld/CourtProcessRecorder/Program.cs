﻿namespace CourtProcessRecorder
{
    using CourtRecorderCommon;
    using System;
    using System.Globalization;
    using System.Threading;
    using System.Windows.Forms;

    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ka-GE");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ka-GE");
            Application.CurrentInputLanguage = InputLanguage.FromCulture(Thread.CurrentThread.CurrentCulture);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            AppExceptionHandler.EnableExceptionHandling();
            Application.Run(new MainForm());
            AppExceptionHandler.DisableExceptionHandling();
        }
    }
}

