﻿namespace CourtProcessRecorder
{
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class RestoreRecording : BaseForm
    {
        private Button btnCancel;
        private Button btnOK;
        private ComboBox cbTrackOwner;
        private ColumnHeader colCreationTime;
        private ColumnHeader colFile;
        private ColumnHeader colSize;
        private IContainer components = null;
        private ListView lvFiles;

        public RestoreRecording()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RestoreRecording));
            this.btnOK = new Button();
            this.btnCancel = new Button();
            this.lvFiles = new ListView();
            this.cbTrackOwner = new ComboBox();
            this.colFile = new ColumnHeader();
            this.colCreationTime = new ColumnHeader();
            this.colSize = new ColumnHeader();
            base.SuspendLayout();
            manager.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnCancel.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.lvFiles.Columns.AddRange(new ColumnHeader[] { this.colFile, this.colCreationTime, this.colSize });
            this.lvFiles.GridLines = true;
            manager.ApplyResources(this.lvFiles, "lvFiles");
            this.lvFiles.MultiSelect = false;
            this.lvFiles.Name = "lvFiles";
            this.lvFiles.UseCompatibleStateImageBehavior = false;
            this.lvFiles.View = View.Details;
            this.cbTrackOwner.FormattingEnabled = true;
            manager.ApplyResources(this.cbTrackOwner, "cbTrackOwner");
            this.cbTrackOwner.Name = "cbTrackOwner";
            manager.ApplyResources(this.colFile, "colFile");
            manager.ApplyResources(this.colCreationTime, "colCreationTime");
            manager.ApplyResources(this.colSize, "colSize");
            base.AcceptButton = this.btnOK;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnCancel;
            manager.ApplyResources(this, "$this");
            base.Controls.Add(this.cbTrackOwner);
            base.Controls.Add(this.lvFiles);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.btnOK);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.Name = "RestoreRecording";
            base.ResumeLayout(false);
        }
    }
}

