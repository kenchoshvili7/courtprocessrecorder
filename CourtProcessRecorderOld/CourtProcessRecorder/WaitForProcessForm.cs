﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.Properties;
    using CourtRecorderCommon;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class WaitForProcessForm : BaseForm
    {
        private BackgroundWorker backgroundWorker;
        private IContainer components = null;
        private Label label;
        private Panel panel;
        private object retVal;
        public Label status;
        public static string StatusText1;
        public static string StatusText2;
        private Timer timer;

        public WaitForProcessForm()
        {
            this.InitializeComponent();
            base.ShowInTaskbar = false;
        }

        private void backgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Job argument = (Job) e.Argument;
                e.Result = argument();
            }
            catch (Exception exception)
            {
                e.Result = exception;
            }
        }

        private void backgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled)
            {
                if (e.Error != null)
                {
                    this.retVal = e.Error;
                }
                else
                {
                    this.retVal = e.Result;
                }
            }
            base.Close();
            StatusText2 = null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            this.panel = new Panel();
            this.label = new Label();
            this.status = new Label();
            this.backgroundWorker = new BackgroundWorker();
            this.timer = new Timer(this.components);
            this.panel.SuspendLayout();
            base.SuspendLayout();
            this.panel.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.panel.BackColor = SystemColors.ButtonHighlight;
            this.panel.BorderStyle = BorderStyle.Fixed3D;
            this.panel.Controls.Add(this.label);
            this.panel.Location = new Point(2, 2);
            this.panel.Name = "panel";
            this.panel.Size = new Size(370, 0x39);
            this.panel.TabIndex = 1;
            this.label.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.label.Font = new Font("Sylfaen", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label.Image = Resources.wait1;
            this.label.ImageAlign = ContentAlignment.MiddleLeft;
            this.label.Location = new Point(9, 10);
            this.label.Name = "label";
            this.label.Size = new Size(0x15d, 0x2a);
            this.label.TabIndex = 0;
            this.label.Text = "მიმდინარეობს დამუშავება...";
            this.label.TextAlign = ContentAlignment.MiddleRight;
            this.status.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.status.AutoEllipsis = true;
            this.status.BorderStyle = BorderStyle.Fixed3D;
            this.status.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.status.Location = new Point(2, 0x3e);
            this.status.Name = "status";
            this.status.Size = new Size(0x170, 0x2d);
            this.status.TabIndex = 2;
            this.status.UseMnemonic = false;
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new DoWorkEventHandler(this.backgroundWorkerDoWork);
            this.backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.backgroundWorkerRunWorkerCompleted);
            this.timer.Tick += new EventHandler(this.timer_Tick);
            base.AutoScaleMode = AutoScaleMode.Inherit;
            this.BackgroundImageLayout = ImageLayout.None;
            base.ClientSize = new Size(0x177, 0x6f);
            base.Controls.Add(this.status);
            base.Controls.Add(this.panel);
            base.FormBorderStyle = FormBorderStyle.None;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "WaitForProcessForm";
            base.ShowInTaskbar = true;
            base.Deactivate += new EventHandler(this.WaitForProcessForm_Deactivate);
            base.Activated += new EventHandler(this.WaitForProcessForm_Deactivate);
            base.FormClosing += new FormClosingEventHandler(this.WaitForProcessFormFormClosing);
            this.panel.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            this.label.Text = StatusText1;
            this.status.Text = StatusText2;
        }

        public static object WaitFor(Job job)
        {
            using (WaitForProcessForm form = new WaitForProcessForm())
            {
                form.backgroundWorker.RunWorkerAsync(job);
                form.timer.Enabled = true;
                form.label.Text = StatusText1;
                form.status.Text = StatusText2;
                form.ShowDialog();
                if (form.backgroundWorker.IsBusy)
                {
                    form.backgroundWorker.CancelAsync();
                }
                return form.retVal;
            }
        }

        private void WaitForProcessForm_Deactivate(object sender, EventArgs e)
        {
            this.label.Refresh();
        }

        private void WaitForProcessFormFormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = this.backgroundWorker.IsBusy;
        }
    }
}

