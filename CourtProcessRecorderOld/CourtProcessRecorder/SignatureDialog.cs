﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using CourtRecorderCommon;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.Windows.Forms;

    public class SignatureDialog : BaseForm
    {
        private Button btnCheckSignature;
        private Button btnClose;
        private Button btnPrint;
        private CourtProcessRecorder.DataTypes.CaseDocument CaseDocument;
        private ComboBox cbCaseDates;
        private IContainer components = null;
        private DateTime? Date;
        private DateTimePicker edtCreationDate;
        private TextBox edtOrganization;
        private TextBox edtResponsiblePerson;
        private TextBox edtSignature;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;

        public SignatureDialog()
        {
            this.InitializeComponent();
        }

        private void btnCheckSignatureClick(object sender, EventArgs e)
        {
            if (this.cbCaseDates.SelectedItem != null)
            {
                if (this.CaseDocument.VerifySignature((DateTime) this.cbCaseDates.SelectedItem))
                {
                    MessageBox.Show(this, "მონაცემთა ხელმოწერის შემოწმება წარმატებით დასრულდა!", "ხელმოწერის შემოწმება", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show(this, "მონაცემთა ხელმოწერა არასწორია!", "ხელმოწერის შემოწმება", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                base.DialogResult = DialogResult.None;
            }
        }

        private void btnPrintClick(object sender, EventArgs e)
        {
            if (this.cbCaseDates.SelectedItem != null)
            {
                using (PrintPreviewDialog dialog = new PrintPreviewDialog())
                {
                    PrintDocument document = new PrintDocument();
                    document.PrintPage += new PrintPageEventHandler(this.printDocumentPrintPage);
                    dialog.Document = document;
                    dialog.WindowState = FormWindowState.Maximized;
                    dialog.ShowDialog();
                }
            }
        }

        private void cbCaseDatesSelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnPrint.Enabled = this.btnCheckSignature.Enabled = (this.cbCaseDates.SelectedIndex > -1) && this.CaseDocument.Signature.ContainsKey((DateTime) this.cbCaseDates.SelectedItem);
            this.RefreshSignature();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public static string FormatSignature(byte[] signature)
        {
            string str = null;
            if (signature != null)
            {
                int num = 0;
                foreach (byte num2 in signature)
                {
                    if ((num % 0x10) == 0)
                    {
                        string str3 = str;
                        str = str3 + num.ToString("X4") + ": " + num2.ToString("X2") + " ";
                    }
                    else
                    {
                        str = str + num2.ToString("X2") + " ";
                    }
                    num++;
                    if ((num % 0x10) == 0)
                    {
                        str = str + Environment.NewLine;
                    }
                }
            }
            return str;
        }

        private void InitializeComponent()
        {
            this.edtSignature = new TextBox();
            this.label1 = new Label();
            this.edtResponsiblePerson = new TextBox();
            this.label2 = new Label();
            this.edtOrganization = new TextBox();
            this.label3 = new Label();
            this.edtCreationDate = new DateTimePicker();
            this.btnClose = new Button();
            this.btnPrint = new Button();
            this.btnCheckSignature = new Button();
            this.label4 = new Label();
            this.cbCaseDates = new ComboBox();
            base.SuspendLayout();
            this.edtSignature.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.edtSignature.Font = new Font("Courier New", 9f);
            this.edtSignature.Location = new Point(12, 0x84);
            this.edtSignature.Multiline = true;
            this.edtSignature.Name = "edtSignature";
            this.edtSignature.ReadOnly = true;
            this.edtSignature.Size = new Size(470, 0xc9);
            this.edtSignature.TabIndex = 4;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(12, 0x2d);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x85, 0x10);
            this.label1.TabIndex = 1;
            this.label1.Text = "პასუხისმგებელი პირი";
            this.edtResponsiblePerson.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.edtResponsiblePerson.Location = new Point(0x97, 0x2a);
            this.edtResponsiblePerson.Name = "edtResponsiblePerson";
            this.edtResponsiblePerson.ReadOnly = true;
            this.edtResponsiblePerson.Size = new Size(0x14b, 0x17);
            this.edtResponsiblePerson.TabIndex = 1;
            this.label2.AutoSize = true;
            this.label2.Location = new Point(12, 0x4a);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x53, 0x10);
            this.label2.TabIndex = 1;
            this.label2.Text = "ორგანიზაცია";
            this.edtOrganization.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.edtOrganization.Location = new Point(0x97, 0x47);
            this.edtOrganization.Name = "edtOrganization";
            this.edtOrganization.ReadOnly = true;
            this.edtOrganization.Size = new Size(0x14b, 0x17);
            this.edtOrganization.TabIndex = 2;
            this.label3.AutoSize = true;
            this.label3.Location = new Point(12, 0x6a);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x51, 0x10);
            this.label3.TabIndex = 1;
            this.label3.Text = "შექმნის დრო";
            this.edtCreationDate.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.edtCreationDate.CustomFormat = "dd MMMM yyyy  HH:mm:ss";
            this.edtCreationDate.Enabled = false;
            this.edtCreationDate.Format = DateTimePickerFormat.Custom;
            this.edtCreationDate.Location = new Point(0x97, 0x67);
            this.edtCreationDate.Name = "edtCreationDate";
            this.edtCreationDate.Size = new Size(0x14b, 0x17);
            this.edtCreationDate.TabIndex = 3;
            this.edtCreationDate.Value = new DateTime(0x7d8, 8, 0x1c, 12, 0x30, 0, 0);
            this.btnClose.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnClose.DialogResult = DialogResult.Cancel;
            this.btnClose.Location = new Point(0x197, 0x153);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new Size(0x4b, 0x17);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "დახურვა";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnPrint.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnPrint.Enabled = false;
            this.btnPrint.Location = new Point(0x146, 0x153);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new Size(0x4b, 0x17);
            this.btnPrint.TabIndex = 6;
            this.btnPrint.Text = "ბეჭვდა";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new EventHandler(this.btnPrintClick);
            this.btnCheckSignature.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            this.btnCheckSignature.Enabled = false;
            this.btnCheckSignature.Location = new Point(0xf5, 0x153);
            this.btnCheckSignature.Name = "btnCheckSignature";
            this.btnCheckSignature.Size = new Size(0x4b, 0x17);
            this.btnCheckSignature.TabIndex = 5;
            this.btnCheckSignature.Text = "შემოწმება";
            this.btnCheckSignature.UseVisualStyleBackColor = true;
            this.btnCheckSignature.Click += new EventHandler(this.btnCheckSignatureClick);
            this.label4.AutoSize = true;
            this.label4.Location = new Point(12, 15);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x6c, 0x10);
            this.label4.TabIndex = 1;
            this.label4.Text = "სხდომის თარიღი";
            this.cbCaseDates.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbCaseDates.FormattingEnabled = true;
            this.cbCaseDates.Location = new Point(0x97, 12);
            this.cbCaseDates.Name = "cbCaseDates";
            this.cbCaseDates.Size = new Size(0x149, 0x18);
            this.cbCaseDates.TabIndex = 0;
            this.cbCaseDates.SelectedIndexChanged += new EventHandler(this.cbCaseDatesSelectedIndexChanged);
            base.AcceptButton = this.btnCheckSignature;
            base.AutoScaleMode = AutoScaleMode.Inherit;
            base.CancelButton = this.btnClose;
            base.ClientSize = new Size(0x1ee, 0x176);
            base.Controls.Add(this.cbCaseDates);
            base.Controls.Add(this.btnCheckSignature);
            base.Controls.Add(this.btnPrint);
            base.Controls.Add(this.btnClose);
            base.Controls.Add(this.edtCreationDate);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.edtOrganization);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.edtResponsiblePerson);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.edtSignature);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.Margin = new Padding(3, 4, 3, 4);
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            this.MinimumSize = new Size(500, 400);
            base.Name = "SignatureDialog";
            this.Text = "ხელმოწერა";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            foreach (KeyValuePair<DateTime, CourtProcessRecorder.DataTypes.CaseDocument.SignatureData> pair in this.CaseDocument.Signature)
            {
                this.cbCaseDates.Items.Add(pair.Key);
            }
            if (this.cbCaseDates.Items.Count > 0)
            {
                if (this.Date.HasValue)
                {
                    this.cbCaseDates.SelectedIndex = this.cbCaseDates.Items.IndexOf(this.Date.Value);
                }
            }
            else
            {
                this.RefreshSignature();
            }
            if ((this.cbCaseDates.SelectedIndex == -1) && (this.cbCaseDates.Items.Count > 0))
            {
                this.cbCaseDates.SelectedIndex = 0;
            }
        }

        private void printDocumentPrintPage(object sender, PrintPageEventArgs e)
        {
            SizeF ef;
            Font font;
            string text = ((((("პასუხისმგებელი  პირი: " + this.edtResponsiblePerson.Text + Environment.NewLine) + "ორგანიზაცია: " + this.edtOrganization.Text + Environment.NewLine) + "შექმნის  დრო: " + (this.edtCreationDate.Visible ? this.edtCreationDate.Value.ToString("dd MMMM yyyy HH:mm:ss") : "") + Environment.NewLine) + "სხდომი თარიღი: " + ((DateTime) this.cbCaseDates.SelectedItem).ToString("dd MMMM yyyy") + Environment.NewLine) + Environment.NewLine) + "ციფრული ხელმოწერა: " + Environment.NewLine;
            using (font = new Font("Sylfaen", 9f, FontStyle.Regular))
            {
                ef = e.Graphics.MeasureString(text, font);
                e.Graphics.DrawString(text, font, Brushes.Black, (PointF) e.MarginBounds.Location);
            }
            text = this.edtSignature.Text;
            using (font = new Font("Courier New", 9f, FontStyle.Regular))
            {
                e.Graphics.DrawString(text, font, Brushes.Black, new PointF((float) e.MarginBounds.Location.X, (e.MarginBounds.Location.Y + ef.Height) + 5f));
            }
        }

        private void RefreshSignature()
        {
            try
            {
                if (this.cbCaseDates.SelectedIndex > -1)
                {
                    CourtProcessRecorder.DataTypes.CaseDocument.SignatureData data = this.CaseDocument.Signature[(DateTime) this.cbCaseDates.SelectedItem];
                    this.edtSignature.Text = FormatSignature(data.Data);
                    this.edtResponsiblePerson.Text = data.ResponsiblePerson;
                    this.edtOrganization.Text = data.Organization;
                    if (data.SignTime.HasValue)
                    {
                        this.edtCreationDate.Value = data.SignTime.Value;
                    }
                    this.edtCreationDate.Visible = data.SignTime.HasValue;
                    this.btnCheckSignature.Enabled = this.cbCaseDates.SelectedIndex > -1;
                    this.btnPrint.Enabled = this.cbCaseDates.SelectedIndex > -1;
                }
            }
            catch (EAbortException)
            {
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void ShowDialog(CourtProcessRecorder.DataTypes.CaseDocument caseDocument, DateTime? date)
        {
            using (SignatureDialog dialog = new SignatureDialog())
            {
                dialog.CaseDocument = caseDocument;
                dialog.Date = date;
                dialog.ShowDialog();
            }
        }
    }
}

