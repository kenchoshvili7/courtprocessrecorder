﻿namespace CourtProcessRecorder.DataTypes
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class Templates : List<Template>
    {
        private object Clone()
        {
            List<Template> list = new List<Template>();
            foreach (Template template in this)
            {
                list.Add((Template) template.Clone());
            }
            return list;
        }

        public Template FindByName(string name)
        {
            return base.Find(delegate (Template t) {
                return t.Name == name;
            });
        }

        public int FindIndexOfName(string name)
        {
            return base.FindIndex(delegate (Template t) {
                return t.Name == name;
            });
        }
    }
}

