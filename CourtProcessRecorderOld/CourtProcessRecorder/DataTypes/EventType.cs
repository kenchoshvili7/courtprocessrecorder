﻿namespace CourtProcessRecorder.DataTypes
{
    using System;

    [Serializable, Flags]
    public enum EventType : byte
    {
        Action = 8,
        Note = 0x20,
        Person = 0x10,
        RecordEnd = 2,
        RecordStart = 1,
        Stage = 4
    }
}

