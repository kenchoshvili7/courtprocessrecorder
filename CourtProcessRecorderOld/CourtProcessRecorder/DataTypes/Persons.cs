﻿namespace CourtProcessRecorder.DataTypes
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class Persons : List<Person>, ICloneable
    {
        public object Clone()
        {
            Persons persons = new Persons();
            foreach (Person person in this)
            {
                persons.Add(new Person(person.Name.Substring(0), person.Type.Substring(0)));
            }
            return persons;
        }

        public Person FindByTypeAndName(string personType, string name)
        {
            return base.Find(delegate (Person p) {
                return (p.Type == personType) && (p.Name == name);
            });
        }
    }
}

