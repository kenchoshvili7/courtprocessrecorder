﻿namespace CourtProcessRecorder.DataTypes
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class Events : List<Event>
    {
        public void SortEvents()
        {
            base.Sort(delegate (Event x, Event y) {
                if (x.Time.Ticks < y.Time.Ticks)
                {
                    return -1;
                }
                if (x.Time.Ticks == y.Time.Ticks)
                {
                    return 0;
                }
                return 1;
            });
        }
    }
}

