﻿namespace CourtProcessRecorder.DataTypes
{
    using CourtRecorderCommon;
    using System;

    [Serializable]
    public class Event
    {
        public string Action;
        public CourtProcessRecorder.DataTypes.EventType EventType;
        public string Note;
        public CourtProcessRecorder.DataTypes.Person Person;
        public CourtRecorderCommon.RecordSet RecordSet;
        public DateTime Time;

        public Event(DateTime time, CourtProcessRecorder.DataTypes.EventType eventType, string action, CourtProcessRecorder.DataTypes.Person person, string note, CourtRecorderCommon.RecordSet recordSet)
        {
            this.EventType = eventType;
            this.Time = time;
            this.Action = action;
            this.Person = person;
            this.Note = note;
            this.RecordSet = recordSet;
        }
    }
}

