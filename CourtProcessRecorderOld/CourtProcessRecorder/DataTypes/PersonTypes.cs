﻿namespace CourtProcessRecorder.DataTypes
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class PersonTypes : List<string>, ICloneable
    {
        public object Clone()
        {
            PersonTypes types = new PersonTypes();
            foreach (string str in this)
            {
                types.Add(str);
            }
            return types;
        }

        public string FindByName(string name)
        {
            return base.Find(delegate (string p) {
                return p == name;
            });
        }
    }
}

