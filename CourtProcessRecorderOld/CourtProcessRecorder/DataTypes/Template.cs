﻿namespace CourtProcessRecorder.DataTypes
{
    using CourtProcessRecorder.Properties;
    using CourtRecorderCommon;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Xml.Serialization;

    [Serializable]
    public class Template : IDisposable, ICloneable
    {
        private string actualPersones;
        private Dictionary<string, CaseStage> caseStages;
        [NonSerialized, XmlIgnore]
        private bool modified;
        private string name;
        private List<ParagraphData> paragraphs;
        private CourtProcessRecorder.DataTypes.PersonTypes personTypes;
        private string protocolFooter;
        private string protocolHeader;
        public string[] UnparagraphedDefaultParagraphName;

        public Template()
        {
            this.UnparagraphedDefaultParagraphName = new string[] { "* სტადია", "* მოქმედება + მოქმედი პირი + დანართი" };
            this.Init();
            this.modified = false;
        }

        public Template(string name)
        {
            this.UnparagraphedDefaultParagraphName = new string[] { "* სტადია", "* მოქმედება + მოქმედი პირი + დანართი" };
            this.name = name;
            this.Init();
            this.modified = false;
        }

        public Template(SerializationInfo info, StreamingContext context)
        {
            this.UnparagraphedDefaultParagraphName = new string[] { "* სტადია", "* მოქმედება + მოქმედი პირი + დანართი" };
            this.modified = false;
        }

        public object Clone()
        {
            Template template = new Template(this.name);
            template.Modified = false;
            if (this.CaseStages != null)
            {
                template.caseStages = new Dictionary<string, CaseStage>();
                foreach (KeyValuePair<string, CaseStage> pair in this.CaseStages)
                {
                    template.caseStages.Add(pair.Key, (CaseStage) pair.Value.Clone());
                }
            }
            if (this.personTypes != null)
            {
                template.personTypes = (CourtProcessRecorder.DataTypes.PersonTypes) this.personTypes.Clone();
            }
            template.actualPersones = this.actualPersones;
            template.protocolHeader = this.protocolHeader;
            template.protocolFooter = this.protocolFooter;
            if (this.paragraphs != null)
            {
                template.paragraphs = new List<ParagraphData>(this.paragraphs.Count);
                foreach (ParagraphData data in this.paragraphs)
                {
                    template.paragraphs.Add((ParagraphData) data.Clone());
                }
            }
            return template;
        }

        public virtual void Dispose()
        {
            if (this.CaseStages != null)
            {
                this.caseStages = null;
            }
            if (this.personTypes != null)
            {
                this.personTypes = null;
            }
        }

        public ParagraphData GetParagraphByName(string name)
        {
            return this.Paragraphs.Find(delegate (ParagraphData pd) {
                return pd.Name == name;
            });
        }

        protected virtual void Init()
        {
            this.modified = false;
        }

        public static Template LoadFromFile(string fileName)
        {
            Template template2;
            try
            {
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Binder = new TemporarySerializationBinder();
                    Template template = (Template) formatter.Deserialize(stream);
                    if (template != null)
                    {
                        template.name = Path.GetFileNameWithoutExtension(fileName);
                    }
                    template2 = template;
                }
            }
            finally
            {
            }
            return template2;
        }

        public void SaveToFile(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                new BinaryFormatter().Serialize(stream, this);
            }
        }

        public string ActualPersones
        {
            get
            {
                return this.actualPersones;
            }
        }

        public Dictionary<string, CaseStage> CaseStages
        {
            get
            {
                if (this.caseStages == null)
                {
                    this.caseStages = new Dictionary<string, CaseStage>();
                }
                return this.caseStages;
            }
        }

        public bool Modified
        {
            get
            {
                return this.modified;
            }
            set
            {
                this.modified = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public List<ParagraphData> Paragraphs
        {
            get
            {
                if (this.paragraphs == null)
                {
                    this.paragraphs = new List<ParagraphData>();
                    this.paragraphs.Add(new ParagraphData(Resources.ParagraphStage));
                    this.paragraphs.Add(new ParagraphData(Resources.ParagraphAction));
                    this.paragraphs.Add(new ParagraphData(Resources.ParagraphPerson));
                    this.paragraphs.Add(new ParagraphData(Resources.ParagraphActionPerson));
                    this.paragraphs.Add(new ParagraphData(Resources.ParagraphActionPersonNote));
                    this.paragraphs.Add(new ParagraphData(Resources.ParagraphActionNote));
                    this.paragraphs.Add(new ParagraphData(Resources.ParagraphPersonNote));
                    this.paragraphs.Add(new ParagraphData(Resources.ParagraphNote));
                }
                return this.paragraphs;
            }
        }

        public CourtProcessRecorder.DataTypes.PersonTypes PersonTypes
        {
            get
            {
                if (this.personTypes == null)
                {
                    this.personTypes = new CourtProcessRecorder.DataTypes.PersonTypes();
                }
                return this.personTypes;
            }
        }

        public string ProtocolFooter
        {
            get
            {
                return this.protocolFooter;
            }
            set
            {
                this.protocolFooter = value;
            }
        }

        public string ProtocolHeader
        {
            get
            {
                return this.protocolHeader;
            }
            set
            {
                this.protocolHeader = value;
            }
        }
    }
}

