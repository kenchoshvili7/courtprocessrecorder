﻿namespace CourtProcessRecorder.DataTypes
{
    using AudioConfiguration;
    using CourtProcessRecorder;
    using CourtProcessRecorder.Properties;
    using CourtRecorderCommon;
    using DeviceController;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Security.Cryptography;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml.Serialization;

    [Serializable]
    public class CaseDocument
    {
        private _RSAParameters? _rsaParam;
        private string actualPersones;
        private Dictionary<string, CaseStage> caseStages;
        private string cdSerialNo;
        private Dictionary<DateTime, List<Channel>> channelNames;
        private CourtProcessRecorder.DataTypes.Events events;
        [NonSerialized, XmlIgnore]
        private string fileName;
        public const string InvalidTrialNoSymbols = ", \\ / : ; ? * < > | ~ \"";
        public const int MaxTrialNoLength = 0x10;
        [NonSerialized, XmlIgnore]
        private bool modified;
        private CourtProcessRecorder.DataTypes.Persons persons;
        [NonSerialized, XmlIgnore]
        private AudioConfiguration.ProgramConfiguration? programConfiguration;
        [NonSerialized, XmlIgnore]
        private bool recordingStarted;
        private Dictionary<DateTime, SignatureData> signature;
        private CourtProcessRecorder.DataTypes.Templates templates;
        private DateTime trialDate;
        private string trialName;
        private string trialNo;
        public Dictionary<DateTime, CourtProcessRecorder.DataTypes.Persons> trialPersons;
        private string trialRoom;
        private Template trialTemplate;
        private Dictionary<DateTime, string> trialTemplates;
        private string uniqueId;
        public int Version;

        public CaseDocument()
        {
            this.Init();
            this.AfterLoad();
            this.Version = 2;
        }

        public CaseDocument(string fileName)
        {
            this.Init();
            this.AfterLoad();
            this.Version = 2;
        }

        public CaseDocument(SerializationInfo info, StreamingContext context)
        {
            this.AfterLoad();
            this.channelNames = new Dictionary<DateTime, List<Channel>>();
        }

        public CaseDocument(string trialNo, string cdSerialNo, string title, DateTime trialDate)
        {
            this.Init();
            this.trialNo = trialNo;
            this.cdSerialNo = cdSerialNo;
            this.trialName = title;
            this.trialDate = trialDate;
            this.AfterLoad();
            this.Version = 2;
            Template item = (Template)TemplatesManager.Templates[0].Clone();
            item.Name = item.Name + " *";
            this.Templates.Add(item);
        }

        protected virtual void AfterLoad()
        {
        }

        public void ApplyChannelNames(DateTime trialDate)
        {
            if (this.channelNames == null)
            {
                this.channelNames = new Dictionary<DateTime, List<Channel>>();
            }
            if (!this.channelNames.ContainsKey(trialDate))
            {
                this.channelNames.Add(trialDate, AudioIO.Configuration.Channels);
            }
        }

        public long CalculateSize(DateTime? dateTime)
        {
            long length = new FileInfo(this.GetFileName()).Length;
            foreach (Event event2 in this.Events)
            {
                if ((!dateTime.HasValue || (event2.Time.Date == dateTime.Value.Date)) && (event2.RecordSet != null))
                {
                    string[] strArray = event2.RecordSet.FileName.Split(new char[] { ':' });
                    foreach (string str in strArray)
                    {
                        string str2 = str.Substring(str.IndexOf('?') + 1);
                        string directoryName = Path.GetDirectoryName(this.GetFileName());
                        if (!directoryName.EndsWith(@"\"))
                        {
                            directoryName = directoryName + @"\";
                        }
                        directoryName = directoryName + this.TrialNo + @"\" + str2;
                        try
                        {
                            length += new FileInfo(directoryName).Length;
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            return length;
        }

        public void GenerateOrderAndPreview(DateTime date)
        {
            string path = Path.GetTempPath() + this.GetFullTrialName() + "(" + date.Year.ToString() + date.Month.ToString() + date.Day.ToString() + ").rtf";
            string contents = this.GenerateOrderRtf(date);
            try
            {
                File.WriteAllText(path, contents, Encoding.ASCII);
                Process process = Process.Start(path);
            }
            catch
            {
                MessageBox.Show(Form.ActiveForm, "ვერ ხერხდება '" + path + "' ფაილის შექმნა! შეამოწმეთ რომ გაქვთ შესაბამისი უფლებ(ებ)ი ან/და ფაილი უკვე არსებობს და დაკავებულია რაიმე პროგრამისგან!", "ოქმის ფორმირება", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public string GenerateOrderRtf(DateTime date)
        {
            string str;
            using (RichTextBox box = new RichTextBox())
            {
                using (RichTextBox box2 = new RichTextBox())
                {
                    List<KeyValuePair<string, string>> variables = null;
                    Template template = this.Templates.FindByName(this.TrialTemplates[date]);
                    box.Rtf = template.ProtocolHeader;
                    box2.Rtf = template.ProtocolFooter;
                    box2.SelectAll();
                    foreach (Event event2 in this.Events)
                    {
                        ParagraphData paragraphByName = null;
                        if (date == event2.Time.Date)
                        {
                            switch (event2.EventType)
                            {
                                case EventType.Person:
                                    paragraphByName = template.GetParagraphByName(Resources.ParagraphPerson);
                                    break;

                                case (EventType.Person | EventType.Stage):
                                case EventType.Stage:
                                case (EventType.Note | EventType.Stage):
                                case (EventType.Note | EventType.Person | EventType.Stage):
                                    goto Label_010C;

                                case (EventType.Action | EventType.Person):
                                    paragraphByName = template.GetParagraphByName(Resources.ParagraphActionPerson);
                                    break;

                                case EventType.Action:
                                    goto Label_01EA;

                                case EventType.Note:
                                    goto Label_01B4;

                                case (EventType.Action | EventType.Note):
                                    goto Label_028F;

                                case (EventType.Note | EventType.Person):
                                    paragraphByName = template.GetParagraphByName(Resources.ParagraphPersonNote);
                                    break;

                                case (EventType.Action | EventType.Note | EventType.Person):
                                    paragraphByName = template.GetParagraphByName(Resources.ParagraphActionPersonNote);
                                    break;
                            }
                        }
                        goto Label_02BC;
                    Label_010C:
                        if (template.CaseStages.ContainsKey(event2.Action))
                        {
                            if (!string.IsNullOrEmpty(template.CaseStages[event2.Action].ParagraphName))
                            {
                                paragraphByName = template.GetParagraphByName(template.CaseStages[event2.Action].ParagraphName);
                            }
                            else
                            {
                                paragraphByName = template.GetParagraphByName(string.IsNullOrEmpty(template.CaseStages[event2.Action].ParentStage) ? Resources.ParagraphStage : Resources.ParagraphActionPersonNote);
                            }
                        }
                        else
                        {
                            paragraphByName = template.GetParagraphByName(Resources.ParagraphStage);
                        }
                        goto Label_02BC;
                    Label_01B4:
                        paragraphByName = template.GetParagraphByName(Resources.ParagraphNote);
                        goto Label_02BC;
                    Label_01EA:
                        if (template.CaseStages.ContainsKey(event2.Action))
                        {
                            if (!string.IsNullOrEmpty(template.CaseStages[event2.Action].ParagraphName))
                            {
                                paragraphByName = template.GetParagraphByName(template.CaseStages[event2.Action].ParagraphName);
                            }
                            else
                            {
                                paragraphByName = template.GetParagraphByName(string.IsNullOrEmpty(template.CaseStages[event2.Action].ParentStage) ? Resources.ParagraphAction : Resources.ParagraphActionPersonNote);
                            }
                        }
                        else
                        {
                            paragraphByName = template.GetParagraphByName(Resources.ParagraphAction);
                        }
                        goto Label_02BC;
                    Label_028F:
                        paragraphByName = template.GetParagraphByName(Resources.ParagraphActionNote);
                    Label_02BC:
                        if (paragraphByName != null)
                        {
                            box.SelectionStart = box.Text.Length;
                            box.SelectionLength = 0;
                            box.SelectedRtf = this.ReplaceSubsituationValue(date, paragraphByName.Data, event2, ref variables);
                        }
                    }
                    box.SelectionStart = box.Text.Length;
                    box.SelectionLength = 0;
                    box.SelectedRtf = box2.SelectedRtf;
                    str = this.ReplaceSubsituationValue(date, box.Rtf, null, ref variables);
                }
            }
            return str;
        }

        public SignatureData GenerateSignature(DateTime date, DateTime? signatureDate, string responsiblePerson, string organization)
        {
            SignatureData data;
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                using (SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider())
                {
                    if (!this._rsaParam.HasValue)
                    {
                        this._rsaParam = new _RSAParameters?(_RSAParameters.FromRSAParameters(rsa.ExportParameters(true)));
                    }
                    else
                    {
                        rsa.ImportParameters(this._rsaParam.Value.ToRSAParameters());
                    }
                    Job job = null;
                    using (DocumentStream s = DocumentStream.FromCaseDocument(date, this))
                    {
                        byte[] bytes = BitConverter.GetBytes(signatureDate.HasValue ? signatureDate.Value.ToBinary() : 0L);
                        s.Write(bytes, 0, bytes.Length);
                        bytes = Encoding.Unicode.GetBytes(responsiblePerson);
                        s.Write(bytes, 0, bytes.Length);
                        bytes = Encoding.Unicode.GetBytes(organization);
                        s.Write(bytes, 0, bytes.Length);
                        if (job == null)
                        {
                            job = delegate
                            {
                                return new SignatureData(signatureDate, responsiblePerson, organization, rsa.SignData(s, sha1));
                            };
                        }
                        object obj2 = WaitForProcessForm.WaitFor(job);
                        if (obj2 is Exception)
                        {
                            throw ((Exception)obj2);
                        }
                        data = (SignatureData)obj2;
                    }
                }
            }
            return data;
        }

        public string GetChannelName(DateTime trialDate, AudioSourceChannels channel)
        {
            Channel[] channelNames = this.GetChannelNames(trialDate);
            foreach (Channel channel2 in channelNames)
            {
                if (channel2.ChannelNo == channel)
                {
                    return channel2.Name;
                }
            }
            return "არ მოიძებნა";
        }

        public Channel[] GetChannelNames(DateTime trialDate)
        {
            if ((this.channelNames != null) && this.channelNames.ContainsKey(trialDate))
            {
                return this.channelNames[trialDate].ToArray();
            }
            return AudioIO.Configuration.Channels.ToArray();
        }

        public string GetFileName()
        {
            if (this.fileName == null)
            {
                this.fileName = AudioIO.WorkingDirectory + NormalizeFileName(this.GetFullTrialName()) + ".cpd";
            }
            return this.fileName;
        }

        public string GetFullTrialName()
        {
            return (this.TrialNo + " - " + this.TrialDate.ToString("yyyy-MM-dd"));
        }

        protected virtual void Init()
        {
            this.trialDate = DateTime.Today;
            this.signature = null;
            this.modified = false;
            this.actualPersones = null;
            this.trialRoom = null;
            this.trialNo = null;
            this.cdSerialNo = null;
            this.trialName = null;
            this.uniqueId = Guid.NewGuid().ToString();
            this.channelNames = new Dictionary<DateTime, List<Channel>>();
        }

        internal bool IsTrialDamaged(DateTime date)
        {
            foreach (Event event2 in this.Events)
            {
                if (((((byte)(event2.EventType & EventType.RecordStart)) == 1) && (event2.Time.Date == date)) && (event2.RecordSet != null))
                {
                    string[] strArray = event2.RecordSet.FileName.Split(new char[] { ':' });
                    foreach (string str in strArray)
                    {
                        string directoryName = Path.GetDirectoryName(this.GetFileName());
                        if (!directoryName.EndsWith(@"\"))
                        {
                            directoryName = directoryName + @"\";
                        }
                        if (!File.Exists(directoryName + this.TrialNo + @"\" + str.Substring(str.IndexOf('?') + 1)))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public static bool IsValidTrialNo(string trialNo)
        {
            if (string.IsNullOrEmpty(trialNo))
            {
                return false;
            }
            if (trialNo.Length > 0x10)
            {
                return false;
            }
            string str = ", \\ / : ; ? * < > | ~ \"".Replace(" ", "");
            foreach (char ch in str)
            {
                if (trialNo.Contains(ch.ToString()))
                {
                    return false;
                }
            }
            return true;
        }

        private static string NormalizeFileName(string fileName)
        {
            foreach (char ch in Path.GetInvalidPathChars())
            {
                byte num2 = (byte)ch;
                fileName = fileName.Replace(ch.ToString(), "$" + num2.ToString("X"));
            }
            foreach (char ch in Path.GetInvalidFileNameChars())
            {
                fileName = fileName.Replace(ch.ToString(), "$" + ((byte)ch).ToString("X"));
            }
            return fileName;
        }

        public static CaseDocument Open(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Binder = new TemporarySerializationBinder();
                CaseDocument document = (CaseDocument)formatter.Deserialize(stream);
                document.Events.SortEvents();
                document.fileName = fileName;
                return document;
            }
        }

        public int RecordCount()
        {
            int num = 0;
            foreach (Event event2 in this.Events)
            {
                if (event2.RecordSet != null)
                {
                    num++;
                }
            }
            return num;
        }

        private string ReplaceSubsituationValue(DateTime date, string rtf, Event ev, ref List<KeyValuePair<string, string>> variables)
        {
            Predicate<Event> match = null;
            Predicate<Event> predicate4 = null;
            KeyValuePair<string, string> pair2;
            if (variables == null)
            {
                variables = new List<KeyValuePair<string, string>>();
                foreach (DataRow row in TemplatesManager.Subsituations.Rows)
                {
                    string str = ((row["NAME"] == null) || (row["NAME"] is DBNull)) ? null : row["NAME"].ToString();
                    if (string.IsNullOrEmpty(str))
                    {
                        continue;
                    }
                    string trialNo = null;
                    switch (str)
                    {
                        case "ნომერი":
                            trialNo = this.TrialNo;
                            goto Label_0496;

                        case "თარიღი":
                            trialNo = date.ToLongDateString();
                            goto Label_0496;

                        case "სახელი":
                            trialNo = this.TrialName;
                            goto Label_0496;

                        case "დარბაზის ნომერი":
                            trialNo = this.TrialRoom;
                            goto Label_0496;

                        case "ციფრული მატარებელი":
                            trialNo = "კომპაქტ დისკი";
                            goto Label_0496;

                        case "დაწყების დრო":
                            break;

                        case "დასრულების დრო":
                            goto Label_025A;

                        case "აუდიო ჟურნალი":
                            foreach (Event event2 in this.Events)
                            {
                                if (event2.Time.Date == date)
                                {
                                    if (((byte)(event2.EventType & EventType.RecordStart)) == 1)
                                    {
                                        trialNo = trialNo + ((trialNo == null) ? "" : ", ") + event2.Time.ToLongTimeString() + "...";
                                    }
                                    if (((byte)(event2.EventType & EventType.RecordEnd)) == 2)
                                    {
                                        trialNo = trialNo + event2.Time.ToLongTimeString();
                                    }
                                }
                            }
                            goto Label_0496;

                        case "მონაწილეები":
                            if (this.TrialPersons.ContainsKey(date))
                            {
                                foreach (Person person in this.TrialPersons[date])
                                {
                                    trialNo = trialNo + ((trialNo == null) ? null : ", ") + person.ToString();
                                }
                            }
                            goto Label_0496;

                        case "ციფრული ხელმოწერა":
                            trialNo = ((this.Events.Count > 0) && this.Signature.ContainsKey(date)) ? SignatureDialog.FormatSignature(this.Signature[date].Data) : null;
                            goto Label_0496;

                        case "სერიული ნომერი":
                            trialNo = (this.CDSerialNo == null) ? null : this.CDSerialNo;
                            goto Label_0496;

                        case "დრო":
                            trialNo = (ev != null) ? ev.Time.ToLongTimeString() : null;
                            goto Label_0496;

                        case "მოქმედება":
                            trialNo = ((ev != null) && (ev.Action != null)) ? ev.Action : null;
                            goto Label_0496;

                        case "მონაწილე":
                            trialNo = ((ev != null) && (ev.Person != null)) ? ev.Person.ToString() : null;
                            goto Label_0496;

                        case "შენიშვნა":
                            trialNo = ((ev != null) && (ev.Note != null)) ? ev.Note : null;
                            goto Label_0496;

                        default:
                            goto Label_0496;
                            if (match == null)
                            {
                                match = delegate(Event e)
                                {
                                    return e.Time.Date == date;
                                };
                            }
                            break;
                    }
                    trialNo = (this.Events.Count > 0) ? this.Events.Find(match).Time.ToLongTimeString() : null;
                    goto Label_0496;
                    if (predicate4 == null)
                    {
                        predicate4 = delegate(Event e)
                        {
                            return e.Time.Date == date;
                        };
                    }
                Label_025A:
                    trialNo = (this.Events.Count > 0) ? this.Events.FindLast(predicate4).Time.ToLongTimeString() : null;
                Label_0496:
                    variables.Add(new KeyValuePair<string, string>(str, trialNo));
                }
                if (this.TrialPersons.ContainsKey(date))
                {
                    using (List<Person>.Enumerator enumerator3 = this.TrialPersons[date].GetEnumerator())
                    {
                        Predicate<KeyValuePair<string, string>> predicate = null;
                        Person person;
                        while (enumerator3.MoveNext())
                        {
                            person = enumerator3.Current;
                            if (!string.IsNullOrEmpty(person.Type) && !string.IsNullOrEmpty(person.Name))
                            {
                                if (predicate == null)
                                {
                                    predicate = delegate(KeyValuePair<string, string> i)
                                    {
                                        return i.Key == person.Type;
                                    };
                                }
                                int num = variables.FindIndex(predicate);
                                if (num > -1)
                                {
                                    pair2 = variables[num];
                                    variables[num] = new KeyValuePair<string, string>(person.Type, pair2.Value + ", " + person.Name);
                                }
                                else
                                {
                                    variables.Add(new KeyValuePair<string, string>(person.Type, person.Name));
                                }
                            }
                        }
                    }
                }
                using (List<string>.Enumerator enumerator4 = this.Templates.FindByName(this.TrialTemplates[date]).PersonTypes.GetEnumerator())
                {
                    Predicate<KeyValuePair<string, string>> predicate2 = null;
                    string person;
                    while (enumerator4.MoveNext())
                    {
                        person = enumerator4.Current;
                        if (!string.IsNullOrEmpty(person))
                        {
                            if (predicate2 == null)
                            {
                                predicate2 = delegate(KeyValuePair<string, string> i)
                                {
                                    return i.Key == person;
                                };
                            }
                            if (variables.FindIndex(predicate2) == -1)
                            {
                                variables.Add(new KeyValuePair<string, string>(person, ""));
                            }
                        }
                    }
                }
                variables.Sort(new VariableComparer());
            }
            int num2 = variables.FindIndex(delegate(KeyValuePair<string, string> i)
            {
                return i.Key == "დრო";
            });
            if (num2 > -1)
            {
                pair2 = variables[num2];
                variables[num2] = new KeyValuePair<string, string>(pair2.Key, (ev != null) ? ev.Time.ToLongTimeString() : null);
            }
            num2 = variables.FindIndex(delegate(KeyValuePair<string, string> i)
            {
                return i.Key == "მოქმედება";
            });
            if (num2 > -1)
            {
                pair2 = variables[num2];
                variables[num2] = new KeyValuePair<string, string>(pair2.Key, ((ev != null) && (ev.Action != null)) ? ev.Action : null);
            }
            num2 = variables.FindIndex(delegate(KeyValuePair<string, string> i)
            {
                return i.Key == "მონაწილე";
            });
            if (num2 > -1)
            {
                pair2 = variables[num2];
                variables[num2] = new KeyValuePair<string, string>(pair2.Key, ((ev != null) && (ev.Person != null)) ? ev.Person.ToString() : null);
            }
            num2 = variables.FindIndex(delegate(KeyValuePair<string, string> i)
            {
                return i.Key == "შენიშვნა";
            });
            if (num2 > -1)
            {
                pair2 = variables[num2];
                variables[num2] = new KeyValuePair<string, string>(pair2.Key, ((ev != null) && (ev.Note != null)) ? ev.Note : null);
            }
            RichTextBox box = null;
            foreach (KeyValuePair<string, string> pair in variables)
            {
                int num3 = -1;
                if (box == null)
                {
                    box = new RichTextBox();
                    box.Rtf = rtf;
                }
                while (true)
                {
                    num3 = box.Find(pair.Key, num3 + 1, -1, RichTextBoxFinds.None);
                    if (num3 == -1)
                    {
                        break;
                    }
                    box.SelectionStart = num3;
                    box.SelectionLength = pair.Key.Length;
                    if (box.SelectionProtected)
                    {
                        box.SelectionProtected = false;
                        box.SelectionColor = Color.Black;
                        box.SelectedText = (pair.Value == null) ? "" : pair.Value;
                        num3 = -1;
                    }
                }
            }
            if (box != null)
            {
                rtf = box.Rtf;
            }

            return rtf;
        }

        public void Save()
        {
            using (FileStream stream = new FileStream(this.GetFileName(), FileMode.Create, FileAccess.Write, FileShare.None))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                bool flag = false;
                Template item = null;
                string name = null;
                try
                {
                    foreach (Event event2 in this.Events)
                    {
                        if (event2.Time.Date == DateTime.Today)
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (!flag)
                    {
                        if (this.TrialTemplates.ContainsKey(DateTime.Today))
                        {
                            name = this.TrialTemplates[DateTime.Today];
                            item = this.Templates.FindByName(name);
                            this.trialTemplates.Remove(DateTime.Today);
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                    int index = this.Templates.Count - 1;
                    while (index >= 0)
                    {
                        if (!this.trialTemplates.ContainsValue(this.Templates[index].Name))
                        {
                            this.Templates.RemoveAt(index);
                        }
                        index--;
                    }
                    if (item != null)
                    {
                        for (index = 0; index < this.Templates.Count; index++)
                        {
                            if (item.Name == this.Templates[index].Name)
                            {
                                item = null;
                                break;
                            }
                        }
                    }
                    formatter.Serialize(stream, this);
                }
                finally
                {
                    if (item != null)
                    {
                        this.Templates.Add(item);
                    }
                    if (!flag)
                    {
                        this.trialTemplates.Add(DateTime.Today, name);
                    }
                }
                this.Modified = false;
            }
        }

        public void Sign(DateTime date, DateTime? signatureDate, string responsiblePerson, string organization)
        {
            this.Signature[date] = this.GenerateSignature(date, signatureDate, responsiblePerson, organization);
        }

        public bool VerifySignature(DateTime date)
        {
            if (this.Signature.ContainsKey(date) && this._rsaParam.HasValue)
            {
                SignatureData signature = this.Signature[date];
                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
                {
                    using (SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider())
                    {
                        rsa.ImportParameters(this._rsaParam.Value.ToRSAParameters());
                        string str = CryptoConfig.MapNameToOID(sha1.GetType().ToString());
                        try
                        {
                            Job job = null;
                            using (DocumentStream s = DocumentStream.FromCaseDocument(date, this))
                            {
                                byte[] bytes = BitConverter.GetBytes(signature.SignTime.HasValue ? signature.SignTime.Value.ToBinary() : 0L);
                                s.Write(bytes, 0, bytes.Length);
                                bytes = Encoding.Unicode.GetBytes(signature.ResponsiblePerson);
                                s.Write(bytes, 0, bytes.Length);
                                bytes = Encoding.Unicode.GetBytes(signature.Organization);
                                s.Write(bytes, 0, bytes.Length);
                                if (job == null)
                                {
                                    job = delegate
                                    {
                                        return rsa.VerifyHash(sha1.ComputeHash(s), str, signature.Data);
                                    };
                                }
                                object obj2 = WaitForProcessForm.WaitFor(job);
                                if (obj2 is Exception)
                                {
                                    throw ((Exception)obj2);
                                }
                                return (bool)obj2;
                            }
                        }
                        catch (EAbortException)
                        {
                            return false;
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
            }
            return false;
        }

        public DateTime[] _TrialDates
        {
            get
            {
                List<DateTime> list = new List<DateTime>();
                foreach (Event event2 in this.Events)
                {
                    if (!list.Contains(event2.Time.Date))
                    {
                        list.Add(event2.Time.Date);
                    }
                }
                return list.ToArray();
            }
        }

        public string ActualPersones
        {
            get
            {
                return this.actualPersones;
            }
        }

        public Dictionary<string, CaseStage> CaseStages
        {
            get
            {
                if (this.caseStages == null)
                {
                    this.caseStages = new Dictionary<string, CaseStage>();
                }
                return this.caseStages;
            }
        }

        public string CDSerialNo
        {
            get
            {
                return this.cdSerialNo;
            }
            set
            {
                this.cdSerialNo = value;
            }
        }

        public CourtProcessRecorder.DataTypes.Events Events
        {
            get
            {
                if (this.events == null)
                {
                    this.events = new CourtProcessRecorder.DataTypes.Events();
                }
                return this.events;
            }
        }

        public bool Modified
        {
            get
            {
                return this.modified;
            }
            set
            {
                this.modified = value;
            }
        }

        [Obsolete("Use TrialPersons instead", true)]
        public CourtProcessRecorder.DataTypes.Persons Persons
        {
            get
            {
                if (this.persons == null)
                {
                    this.persons = new CourtProcessRecorder.DataTypes.Persons();
                }
                return this.persons;
            }
        }

        public AudioConfiguration.ProgramConfiguration ProgramConfiguration
        {
            get
            {
                if (this.Events.Count == 0)
                {
                    return AudioIO.Configuration.ProgramConfiguration;
                }
                if (!this.programConfiguration.HasValue)
                {
                    foreach (Event event2 in this.Events)
                    {
                        if (((((byte)(event2.EventType & EventType.RecordStart)) == 1) && (event2.RecordSet != null)) && (event2.RecordSet.FileName.Contains("?") || event2.RecordSet.FileName.Contains(":")))
                        {
                            //this.programConfiguration = 1;
                            this.programConfiguration = ProgramConfiguration.MultiChannel;
                            return this.programConfiguration.Value;
                        }
                    }
                    this.programConfiguration = 0;
                }
                return this.programConfiguration.GetValueOrDefault(AudioConfiguration.ProgramConfiguration.SingleChannel);
            }
        }

        public bool RecordingStarted
        {
            get
            {
                return this.recordingStarted;
            }
            set
            {
                this.recordingStarted = value;
            }
        }

        public bool RecordInitialized
        {
            get
            {
                return (this.Events.Count > 0);
            }
        }

        public Dictionary<DateTime, SignatureData> Signature
        {
            get
            {
                if (this.signature == null)
                {
                    this.signature = new Dictionary<DateTime, SignatureData>();
                }
                return this.signature;
            }
        }

        public CourtProcessRecorder.DataTypes.Templates Templates
        {
            get
            {
                if (this.templates == null)
                {
                    this.templates = new CourtProcessRecorder.DataTypes.Templates();
                    if (this.Version < 1)
                    {
                        this.templates.Add(this.trialTemplate);
                    }
                }
                return this.templates;
            }
        }

        public DateTime TrialDate
        {
            get
            {
                return this.trialDate;
            }
        }

        public DateTime[] TrialDates
        {
            get
            {
                List<DateTime> list = new List<DateTime>();
                foreach (Event event2 in this.Events)
                {
                    if (!list.Contains(event2.Time.Date))
                    {
                        list.Add(event2.Time.Date);
                    }
                }
                if (!((AudioIO.ProgramMode != ProgramMode.Recording) || list.Contains(DateTime.Today)))
                {
                    list.Add(DateTime.Today);
                }
                return list.ToArray();
            }
        }

        public string TrialName
        {
            get
            {
                return this.trialName;
            }
        }

        public string TrialNo
        {
            get
            {
                return this.trialNo;
            }
            set
            {
                this.trialNo = value;
            }
        }

        public Dictionary<DateTime, CourtProcessRecorder.DataTypes.Persons> TrialPersons
        {
            get
            {
                if (this.trialPersons == null)
                {
                    this.trialPersons = new Dictionary<DateTime, CourtProcessRecorder.DataTypes.Persons>();
                    if (this.Version < 1)
                    {
                        foreach (DateTime time in this.TrialDates)
                        {
                            this.trialPersons.Add(time, this.persons);
                        }
                    }
                }
                return this.trialPersons;
            }
        }

        public string TrialRoom
        {
            get
            {
                return this.trialRoom;
            }
        }

        [Obsolete("Use TrialTemplates instead", true)]
        public Template TrialTemplate
        {
            get
            {
                return this.trialTemplate;
            }
            set
            {
                this.trialTemplate = value;
            }
        }

        public Dictionary<DateTime, string> TrialTemplates
        {
            get
            {
                if (this.trialTemplates == null)
                {
                    this.trialTemplates = new Dictionary<DateTime, string>();
                    if (this.Version < 1)
                    {
                        foreach (DateTime time in this.TrialDates)
                        {
                            this.trialTemplates.Add(time, this.trialTemplate.Name);
                        }
                    }
                }
                if (!this.trialTemplates.ContainsKey(DateTime.Today) && (AudioIO.ProgramMode != ProgramMode.Playback))
                {
                    if (this.TrialDates.Length > 1)
                    {
                        this.trialTemplates.Add(DateTime.Today, this.trialTemplates[this.TrialDates[this.TrialDates.Length - 2]]);
                    }
                    else
                    {
                        this.trialTemplates.Add(DateTime.Today, TemplatesManager.Templates[0].Name + " *");
                    }
                }
                return this.trialTemplates;
            }
        }

        public string UniqueId
        {
            get
            {
                return this.uniqueId;
            }
        }

        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct _RSAParameters
        {
            public byte[] D;
            public byte[] DP;
            public byte[] DQ;
            public byte[] Exponent;
            public byte[] InverseQ;
            public byte[] Modulus;
            public byte[] P;
            public byte[] Q;
            internal RSAParameters ToRSAParameters()
            {
                RSAParameters parameters = new RSAParameters();
                parameters.D = this.D;
                parameters.DP = this.DP;
                parameters.DQ = this.DQ;
                parameters.Exponent = this.Exponent;
                parameters.InverseQ = this.InverseQ;
                parameters.Modulus = this.Modulus;
                parameters.P = this.P;
                parameters.Q = this.Q;
                return parameters;
            }

            internal static CaseDocument._RSAParameters FromRSAParameters(RSAParameters rsaParameters)
            {
                CaseDocument._RSAParameters parameters = new CaseDocument._RSAParameters();
                parameters.D = rsaParameters.D;
                parameters.DP = rsaParameters.DP;
                parameters.DQ = rsaParameters.DQ;
                parameters.Exponent = rsaParameters.Exponent;
                parameters.InverseQ = rsaParameters.InverseQ;
                parameters.Modulus = rsaParameters.Modulus;
                parameters.P = rsaParameters.P;
                parameters.Q = rsaParameters.Q;
                return parameters;
            }
        }

        private class DocumentStream : Stream
        {
            private Stream caseDocumentStream;
            private List<Stream> fileInfos;
            private long position;

            private DocumentStream(DateTime date, CaseDocument caseDocument)
            {
                if (caseDocument == null)
                {
                    throw new Exception("დოკუმენტი ცარიელია!");
                }
                this.caseDocumentStream = new MemoryStream();
                SurrogateSelector selector = new SurrogateSelector();
                StreamingContext context = new StreamingContext();
                selector.AddSurrogate(typeof(CaseDocument), context, new CaseDocumentSerializationSurrogate(date));
                new BinaryFormatter(selector, context).Serialize(this.caseDocumentStream, caseDocument);
                this.caseDocumentStream.Position = 0L;
                this.fileInfos = new List<Stream>();
                foreach (Event event2 in caseDocument.Events)
                {
                    if ((event2.Time.Date == date) && (event2.RecordSet != null))
                    {
                        string[] strArray = event2.RecordSet.FileName.Split(new char[] { ':' });
                        foreach (string str in strArray)
                        {
                            string str2 = str.Substring(str.IndexOf('?') + 1);
                            string directoryName = Path.GetDirectoryName(caseDocument.GetFileName());
                            if (!directoryName.EndsWith(@"\"))
                            {
                                directoryName = directoryName + @"\";
                            }
                            directoryName = directoryName + caseDocument.TrialNo + @"\" + str2;
                            try
                            {
                                this.fileInfos.Add(new FileStream(directoryName, FileMode.Open, FileAccess.Read, FileShare.Read));
                            }
                            catch (Exception)
                            {
                                if (MessageBox.Show(Form.ActiveForm, "ვერ ხერხდება  '" + str2 + "' ფაილის გახსნა წასაკითხად! გავაგრძელო შემოწმება?", "შეცდომა", MessageBoxButtons.YesNo, MessageBoxIcon.Hand) == DialogResult.No)
                                {
                                    throw new EAbortException();
                                }
                            }
                        }
                    }
                }
            }

            public override void Flush()
            {
                throw new NotImplementedException();
            }

            internal static CaseDocument.DocumentStream FromCaseDocument(DateTime date, CaseDocument caseDocument)
            {
                return new CaseDocument.DocumentStream(date, caseDocument);
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                int num = 0;
                if (this.position < this.caseDocumentStream.Length)
                {
                    num = this.caseDocumentStream.Read(buffer, 0, count);
                    count -= num;
                    this.position += num;
                }
                if (count > 0)
                {
                    long length = this.caseDocumentStream.Length;
                    foreach (FileStream stream in this.fileInfos)
                    {
                        length += stream.Length;
                        if (this.position < length)
                        {
                            int num3 = 0;
                            try
                            {
                                num3 = stream.Read(buffer, num, count);
                                WaitForProcessForm.StatusText1 = string.Format("წაკითხვა ({0}/{1}) {2}%", this.fileInfos.IndexOf(stream), this.fileInfos.Count, (((stream.Position * 1.0) / ((double)stream.Length)) * 100.0).ToString("0.00"));
                                WaitForProcessForm.StatusText2 = stream.Name;
                            }
                            catch (Exception exception)
                            {
                                if (MessageBox.Show(null, "ვერ ხერხდება  '" + stream.Name + "' ფაილის წაკითხვა! გავაგრძელო შემოწმება?", "შეცდომა", MessageBoxButtons.YesNo, MessageBoxIcon.Hand) == DialogResult.No)
                                {
                                    throw new Exception("ვერ მოხერხდა '" + stream.Name + "' ფაილის წაკითხვა! ოპერაცია უარყოფილია.", exception);
                                }
                            }
                            num += num3;
                            this.position += num3;
                            count -= num3;
                        }
                        if (count == 0)
                        {
                            return num;
                        }
                    }
                }
                return num;
            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                throw new NotImplementedException();
            }

            public override void SetLength(long value)
            {
                throw new NotImplementedException();
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                this.caseDocumentStream.Write(buffer, offset, count);
            }

            public override bool CanRead
            {
                get
                {
                    return true;
                }
            }

            public override bool CanSeek
            {
                get
                {
                    return false;
                }
            }

            public override bool CanWrite
            {
                get
                {
                    return true;
                }
            }

            public override long Length
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public override long Position
            {
                get
                {
                    return this.position;
                }
                set
                {
                    this.position = value;
                }
            }

            internal class CaseDocumentSerializationSurrogate : ISerializationSurrogate
            {
                private DateTime date;

                public CaseDocumentSerializationSurrogate(DateTime date)
                {
                    this.date = date;
                }

                public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
                {
                    CaseDocument document = obj as CaseDocument;
                    if (document != null)
                    {
                        Events events = new Events();
                        foreach (Event event2 in document.Events)
                        {
                            if (event2.Time.Date == this.date)
                            {
                                events.Add(event2);
                            }
                        }
                        if (events.Count <= 0)
                        {
                            throw new Exception("ამ თარიღში სხდომა არ არსებობს!");
                        }
                        events.SortEvents();
                        info.AddValue("Events", events.ToArray(), typeof(Event[]));
                        info.AddValue("UniqueId", document.UniqueId, typeof(string));
                        info.AddValue("TrialName", document.TrialName, typeof(string));
                        info.AddValue("TrialNo", document.TrialNo, typeof(string));
                        info.AddValue("TrialDate", document.TrialDate, typeof(DateTime));
                        info.AddValue("TrialRoom", document.TrialRoom, typeof(string));
                        info.AddValue("ActualPersones", document.ActualPersones, typeof(string));
                        info.AddValue("TrialTemplate", document.TrialTemplates[this.date], typeof(string));
                        info.AddValue("Template", document.Templates.FindByName(document.TrialTemplates[this.date]), typeof(Template));
                        if (document.Version >= 2)
                        {
                            info.AddValue("channelNames", document.channelNames[this.date].ToArray(), typeof(Channel[]));
                        }
                        if (document.TrialPersons.ContainsKey(this.date))
                        {
                            info.AddValue("TrialPersones", document.TrialPersons[this.date].ToArray(), typeof(Person[]));
                        }
                    }
                }

                public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
                {
                    return null;
                }
            }
        }

        private class VariableComparer : IComparer<KeyValuePair<string, string>>
        {
            public int Compare(KeyValuePair<string, string> x, KeyValuePair<string, string> y)
            {
                if (x.Key.Length == y.Key.Length)
                {
                    return 0;
                }
                if (x.Key.Length < y.Key.Length)
                {
                    return 1;
                }
                return -1;
            }
        }
        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct SignatureData
        {
            public DateTime? SignTime;
            public string ResponsiblePerson;
            public string Organization;
            public byte[] Data;
            public SignatureData(DateTime? signTime, string responsiblePerson, string organization, byte[] data)
            {
                this.SignTime = signTime;
                this.ResponsiblePerson = responsiblePerson;
                this.Organization = organization;
                this.Data = data;
            }
        }
    }
}

