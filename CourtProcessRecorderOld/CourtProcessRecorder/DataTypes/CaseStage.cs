﻿namespace CourtProcessRecorder.DataTypes
{
    using System;

    [Serializable]
    public class CaseStage : ICloneable
    {
        public string ParagraphName;
        public string ParentStage;

        public CaseStage()
        {
        }

        public CaseStage(string paragraphName, string parentStage)
        {
            this.ParagraphName = paragraphName;
            this.ParentStage = parentStage;
        }

        public object Clone()
        {
            return new CaseStage(this.ParagraphName, this.ParentStage);
        }
    }
}

