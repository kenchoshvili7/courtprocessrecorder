﻿namespace CourtProcessRecorder.DataTypes
{
    using System;

    [Serializable]
    public class Person
    {
        public string Name;
        public string Type;

        public Person(string name, string type)
        {
            this.Name = name;
            this.Type = type;
        }

        public override string ToString()
        {
            return (this.Type + ": " + this.Name);
        }
    }
}

