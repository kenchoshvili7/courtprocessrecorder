﻿namespace CourtProcessRecorder.DataTypes
{
    using CourtProcessRecorder.Properties;
    using System;

    [Serializable]
    public class ParagraphData : ICloneable
    {
        public string Data;
        public const string DefaultParagraph = "* სტადია";
        public string Name;

        public ParagraphData()
        {
        }

        public ParagraphData(string paragraphName)
        {
            this.Name = paragraphName;
            if (paragraphName == Resources.ParagraphStage)
            {
                this.Data = "{\\rtf1\\ansi\\ansicpg1251\\deff0\\deflang1049{\\fonttbl{\\f0\\fnil\\fcharset204 Sylfaen;}{\\f1\\fnil\\fcharset0 Sylfaen;}}\r\n{\\colortbl ;\\red255\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\qc\\f0\\fs24  \\cf1\\f1  \\protect\\u4315?\\u4317?\\u4325?\\u4315?\\u4308?\\u4307?\\u4308?\\u4305?\\u4304?\\protect0  \\cf0  \\par\r\n\\pard\\f0\\par\r\n}\r\n";
            }
            else if (paragraphName == Resources.ParagraphAction)
            {
                this.Data = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Sylfaen;}}\r\n{\\colortbl ;\\red255\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\cf1\\protect\\f0\\fs24\\u4307?\\u4320?\\u4317?\\protect0 : \\protect\\u4315?\\u4317?\\u4325?\\u4315?\\u4308?\\u4307?\\u4308?\\u4305?\\u4304?\\protect0\\par\r\n\\cf0\\fs18\\par\r\n}\r\n";
            }
            else if (paragraphName == Resources.ParagraphPerson)
            {
                this.Data = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Sylfaen;}}\r\n{\\colortbl ;\\red255\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\cf1\\protect\\f0\\fs24\\u4307?\\u4320?\\u4317?\\cf0\\protect0 :  \\cf1\\protect\\u4315?\\u4317?\\u4316?\\u4304?\\u4332?\\u4312?\\u4314?\\u4308?\\cf0\\protect0  \\par\r\n\\fs18\\par\r\n}\r\n";
            }
            else if (paragraphName == Resources.ParagraphActionPerson)
            {
                this.Data = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Sylfaen;}}\r\n{\\colortbl ;\\red255\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\cf1\\protect\\f0\\fs24\\u4307?\\u4320?\\u4317?\\cf0\\protect0  \\cf1\\protect\\u4315?\\u4317?\\u4325?\\u4315?\\u4308?\\u4307?\\u4308?\\u4305?\\u4304?\\cf0\\protect0 :  \\cf1\\protect\\u4315?\\u4317?\\u4316?\\u4304?\\u4332?\\u4312?\\u4314?\\u4308?\\cf0\\protect0\\par\r\n\\par\r\n}\r\n";
            }
            else if (paragraphName == Resources.ParagraphActionPersonNote)
            {
                this.Data = "{\\rtf1\\ansi\\ansicpg1251\\deff0\\deflang1049{\\fonttbl{\\f0\\fnil\\fcharset0 Sylfaen;}{\\f1\\fnil\\fcharset204 Sylfaen;}}\r\n{\\colortbl ;\\red255\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\cf1\\lang1033\\protect\\f0\\fs24\\u4307?\\u4320?\\u4317?\\cf0\\protect0  \\cf1\\protect\\u4315?\\u4317?\\u4325?\\u4315?\\u4308?\\u4307?\\u4308?\\u4305?\\u4304?\\cf0\\protect0  \\cf1\\protect\\u4315?\\u4317?\\u4316?\\u4304?\\u4332?\\u4312?\\u4314?\\u4308?\\cf0\\protect0 :  \\cf1\\protect\\u4328?\\u4308?\\u4316?\\u4312?\\u4328?\\u4309?\\u4316?\\u4304?\\cf0\\protect0\\par\r\n\\lang1049\\f1\\fs18\\par\r\n}\r\n";
            }
            else if (paragraphName == Resources.ParagraphActionNote)
            {
                this.Data = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Sylfaen;}}\r\n{\\colortbl ;\\red255\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\cf1\\protect\\f0\\fs24\\u4307?\\u4320?\\u4317?\\cf0\\protect0  \\cf1\\protect\\u4315?\\u4317?\\u4325?\\u4315?\\u4308?\\u4307?\\u4308?\\u4305?\\u4304?\\cf0\\protect0 :  \\cf1\\protect\\u4328?\\u4308?\\u4316?\\u4312?\\u4328?\\u4309?\\u4316?\\u4304?\\cf0\\protect0  \\par\r\n\\fs18\\par\r\n}\r\n";
            }
            else if (paragraphName == Resources.ParagraphPersonNote)
            {
                this.Data = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Sylfaen;}{\\f1\\fnil Sylfaen;}}\r\n{\\colortbl ;\\red255\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\cf1\\protect\\f0\\fs24\\u4307?\\u4320?\\u4317?\\cf0\\protect0  \\f1  \\cf1\\protect\\f0\\u4315?\\u4317?\\u4316?\\u4304?\\u4332?\\u4312?\\u4314?\\u4308?\\cf0\\protect0 :  \\cf1\\protect\\u4328?\\u4308?\\u4316?\\u4312?\\u4328?\\u4309?\\u4316?\\u4304?\\cf0\\protect0\\par\r\n\\fs18\\par\r\n}\r\n";
            }
            else if (paragraphName == Resources.ParagraphNote)
            {
                this.Data = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Sylfaen;}}\r\n{\\colortbl ;\\red255\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\cf1\\protect\\f0\\fs24\\u4307?\\u4320?\\u4317?\\cf0\\protect0 :  \\cf1\\protect\\u4328?\\u4308?\\u4316?\\u4312?\\u4328?\\u4309?\\u4316?\\u4304?\\cf0\\protect0   \\par\r\n\\par\r\n}\r\n";
            }
        }

        public object Clone()
        {
            ParagraphData data = new ParagraphData();
            if (this.Name != null)
            {
                data.Name = this.Name.Substring(0);
            }
            if (this.Data != null)
            {
                data.Data = this.Data.Substring(0);
            }
            return data;
        }

        public static bool IsCustom(string paragraphName)
        {
            if (((((paragraphName == Resources.ParagraphStage) || (paragraphName == Resources.ParagraphAction)) || ((paragraphName == Resources.ParagraphPerson) || (paragraphName == Resources.ParagraphActionPerson))) || (((paragraphName == Resources.ParagraphActionPersonNote) || (paragraphName == Resources.ParagraphNote)) || (paragraphName == Resources.ParagraphPersonNote))) || (paragraphName == Resources.ParagraphNote))
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}

