﻿namespace CourtProcessRecorder
{
    using System;

    public enum BurnStage
    {
        InitializingFileSystem,
        WritingToTheDisc
    }
}

