﻿namespace CourtProcessRecorder
{
    using System;

    public enum AudioControlState
    {
        Idle,
        Recording,
        Playing
    }
}

