﻿namespace CourtProcessRecorder
{
    using CourtProcessRecorder.DataTypes;
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct CaseItem
    {
        public string TrialNo;
        public CourtProcessRecorder.DataTypes.CaseDocument CaseDocument;
        public override string ToString()
        {
            return this.TrialNo;
        }

        public CaseItem(string trialNo, CourtProcessRecorder.DataTypes.CaseDocument document)
        {
            this.TrialNo = trialNo;
            this.CaseDocument = document;
        }
    }
}

