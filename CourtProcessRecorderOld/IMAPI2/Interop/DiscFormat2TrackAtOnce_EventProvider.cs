﻿namespace IMAPI2.Interop
{
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;
    using System.Threading;

    [ClassInterface(ClassInterfaceType.None)]
    internal sealed class DiscFormat2TrackAtOnce_EventProvider : DiscFormat2TrackAtOnce_Event, IDisposable
    {
        private Hashtable m_aEventSinkHelpers = new Hashtable();
        private IConnectionPoint m_connectionPoint = null;

        public event DiscFormat2TrackAtOnce_EventHandler Update
        {
            add
            {
                lock (this)
                {
                    int num;
                    DiscFormat2TrackAtOnce_SinkHelper pUnkSink = new DiscFormat2TrackAtOnce_SinkHelper(value);
                    this.m_connectionPoint.Advise(pUnkSink, out num);
                    pUnkSink.Cookie = num;
                    this.m_aEventSinkHelpers.Add(pUnkSink.UpdateDelegate, pUnkSink);
                }
            }
            remove
            {
                lock (this)
                {
                    DiscFormat2TrackAtOnce_SinkHelper helper = this.m_aEventSinkHelpers[value] as DiscFormat2TrackAtOnce_SinkHelper;
                    if (helper != null)
                    {
                        this.m_connectionPoint.Unadvise(helper.Cookie);
                        this.m_aEventSinkHelpers.Remove(helper.UpdateDelegate);
                    }
                }
            }
        }

        public DiscFormat2TrackAtOnce_EventProvider(object pointContainer)
        {
            lock (this)
            {
                Guid gUID = typeof(DDiscFormat2TrackAtOnceEvents).GUID;
                (pointContainer as IConnectionPointContainer).FindConnectionPoint(ref gUID, out this.m_connectionPoint);
            }
        }

        private void Cleanup()
        {
            Monitor.Enter(this);
            try
            {
                foreach (DiscFormat2TrackAtOnce_SinkHelper helper in this.m_aEventSinkHelpers)
                {
                    this.m_connectionPoint.Unadvise(helper.Cookie);
                }
                this.m_aEventSinkHelpers.Clear();
                Marshal.ReleaseComObject(this.m_connectionPoint);
            }
            catch (SynchronizationLockException)
            {
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        public void Dispose()
        {
            this.Cleanup();
            GC.SuppressFinalize(this);
        }

        ~DiscFormat2TrackAtOnce_EventProvider()
        {
            this.Cleanup();
        }
    }
}

