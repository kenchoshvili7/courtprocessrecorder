﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    [ComImport, TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual), Guid("27354154-8F64-5B0F-8F00-5D77AFBE261E")]
    public interface IDiscFormat2TrackAtOnce
    {
        [DispId(0x800)]
        bool IsRecorderSupported(IDiscRecorder2 Recorder);
        [DispId(0x801)]
        bool IsCurrentMediaSupported(IDiscRecorder2 Recorder);
        [DispId(0x700)]
        bool MediaPhysicallyBlank { get; }
        [DispId(0x701)]
        bool MediaHeuristicallyBlank { get; }
        [DispId(0x702)]
        object[] SupportedMediaTypes { get; }
        [DispId(0x200)]
        void PrepareMedia();
        [DispId(0x201)]
        void AddAudioTrack(IStream data);
        [DispId(0x202)]
        void CancelAddTrack();
        [DispId(0x203)]
        void ReleaseMedia();
        [DispId(0x204)]
        void SetWriteSpeed(int RequestedSectorsPerSecond, bool RotationTypeIsPureCAV);
        [DispId(0x100)]
        IDiscRecorder2 Recorder { get; set; }
        [DispId(0x102)]
        bool BufferUnderrunFreeDisabled { get; set; }
        [DispId(0x103)]
        int NumberOfExistingTracks { get; }
        [DispId(260)]
        int TotalSectorsOnMedia { get; }
        [DispId(0x105)]
        int FreeSectorsOnMedia { get; }
        [DispId(0x106)]
        int UsedSectorsOnMedia { get; }
        [DispId(0x107)]
        bool DoNotFinalizeMedia { get; set; }
        [DispId(0x10a)]
        object[] ExpectedTableOfContents { get; }
        [DispId(0x10b)]
        IMAPI_MEDIA_PHYSICAL_TYPE CurrentPhysicalMediaType { get; }
        [DispId(270)]
        string ClientName { get; set; }
        [DispId(0x10f)]
        int RequestedWriteSpeed { get; }
        [DispId(0x110)]
        bool RequestedRotationTypeIsPureCAV { get; }
        [DispId(0x111)]
        int CurrentWriteSpeed { get; }
        [DispId(0x112)]
        bool CurrentRotationTypeIsPureCAV { get; }
        [DispId(0x113)]
        object[] SupportedWriteSpeeds { get; }
        [DispId(0x114)]
        object[] SupportedWriteSpeedDescriptors { get; }
    }
}

