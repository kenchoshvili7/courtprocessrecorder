﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, CoClass(typeof(MsftDiscFormat2EraseClass)), Guid("27354156-8F64-5B0F-8F00-5D77AFBE261E")]
    public interface MsftDiscFormat2Erase : IDiscFormat2Erase, DiscFormat2Erase_Event
    {
    }
}

