﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual), Guid("2C941FE1-975B-59BE-A960-9A2A262853A5")]
    public interface IFileSystemImage
    {
        [DispId(0)]
        IFsiDirectoryItem Root { get; }
        [DispId(1)]
        int SessionStartBlock { get; set; }
        [DispId(2)]
        int FreeMediaBlocks { get; set; }
        [DispId(0x24)]
        void SetMaxMediaBlocksFromDevice(IDiscRecorder2 discRecorder);
        [DispId(3)]
        int UsedBlocks { get; }
        [DispId(4)]
        string VolumeName { get; set; }
        [DispId(5)]
        string ImportedVolumeName { get; }
        [DispId(6)]
        IBootOptions BootImageOptions { get; set; }
        [DispId(7)]
        int FileCount { get; }
        [DispId(8)]
        int DirectoryCount { get; }
        [DispId(9)]
        string WorkingDirectory { get; set; }
        [DispId(10)]
        int ChangePoint { get; }
        [DispId(11)]
        bool StrictFileSystemCompliance { get; set; }
        [DispId(12)]
        bool UseRestrictedCharacterSet { get; set; }
        [DispId(13)]
        FsiFileSystems FileSystemsToCreate { get; set; }
        [DispId(14)]
        FsiFileSystems FileSystemsSupported { get; }
        [DispId(0x25)]
        int UDFRevision { get; set; }
        [DispId(0x1f)]
        object[] UDFRevisionsSupported { get; }
        [DispId(0x20)]
        void ChooseImageDefaults(IDiscRecorder2 discRecorder);
        [DispId(0x21)]
        void ChooseImageDefaultsForMediaType(IMAPI_MEDIA_PHYSICAL_TYPE value);
        [DispId(0x22)]
        int ISO9660InterchangeLevel { get; set; }
        [DispId(0x26)]
        object[] ISO9660InterchangeLevelsSupported { get; }
        [DispId(15)]
        IFileSystemImageResult CreateResultImage();
        [DispId(0x10)]
        FsiItemType Exists(string FullPath);
        [DispId(0x12)]
        string CalculateDiscIdentifier();
        [DispId(0x13)]
        FsiFileSystems IdentifyFileSystemsOnDisc(IDiscRecorder2 discRecorder);
        [DispId(20)]
        FsiFileSystems GetDefaultFileSystemForImport(FsiFileSystems fileSystems);
        [DispId(0x15)]
        FsiFileSystems ImportFileSystem();
        [DispId(0x16)]
        void ImportSpecificFileSystem(FsiFileSystems fileSystemToUse);
        [DispId(0x17)]
        void RollbackToChangePoint(int ChangePoint);
        [DispId(0x18)]
        void LockInChangePoint();
        [DispId(0x19)]
        IFsiDirectoryItem CreateDirectoryItem(string Name);
        [DispId(0x1a)]
        IFsiFileItem CreateFileItem(string Name);
        [DispId(0x1b)]
        string VolumeNameUDF { get; }
        [DispId(0x1c)]
        string VolumeNameJoliet { get; }
        [DispId(0x1d)]
        string VolumeNameISO9660 { get; }
        [DispId(30)]
        bool StageFiles { get; set; }
        [DispId(40)]
        object[] MultisessionInterfaces { get; set; }
    }
}

