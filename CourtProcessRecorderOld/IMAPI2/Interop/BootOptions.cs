﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, Guid("2C941FD4-975B-59BE-A960-9A2A262853A5"), CoClass(typeof(BootOptionsClass))]
    public interface BootOptions : IBootOptions
    {
    }
}

