﻿namespace IMAPI2.Interop
{
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Runtime.InteropServices;

    [ComImport, Guid("27354130-7F64-5B0F-8F00-5D77AFBE261E"), TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual)]
    public interface IDiscMaster2
    {
        [TypeLibFunc((short) 0x41), DispId(-4)]
        IEnumerator GetEnumerator();
        [DispId(0)]
        string this[int index] { get; }
        [DispId(1)]
        int Count { get; }
        [DispId(2)]
        bool IsSupportedEnvironment { get; }
    }
}

