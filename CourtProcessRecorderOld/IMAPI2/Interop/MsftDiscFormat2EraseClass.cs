﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, Guid("2735412B-7F64-5B0F-8F00-5D77AFBE261E"), ClassInterface(ClassInterfaceType.None), ComSourceInterfaces("DDiscFormat2EraseEvents"), TypeLibType(TypeLibTypeFlags.FCanCreate)]
    public class MsftDiscFormat2EraseClass
    {
    }
}

