﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, TypeLibType(TypeLibTypeFlags.FCanCreate), ClassInterface(ClassInterfaceType.None), Guid("27354129-7F64-5B0F-8F00-5D77AFBE261E"), ComSourceInterfaces("DDiscFormat2TrackAtOnceEvents")]
    public class MsftDiscFormat2TrackAtOnceClass
    {
    }
}

