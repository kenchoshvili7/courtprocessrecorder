﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, CoClass(typeof(MsftDiscRecorder2Class)), Guid("27354132-7F64-5B0F-8F00-5D77AFBE261E"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface MsftDiscRecorder2Ex : IDiscRecorder2Ex
    {
    }
}

