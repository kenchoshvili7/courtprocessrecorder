﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, TypeLibType(TypeLibTypeFlags.FCanCreate), ComSourceInterfaces("DiscFormat2DataEvents"), ClassInterface(ClassInterfaceType.None), Guid("2735412A-7F64-5B0F-8F00-5D77AFBE261E")]
    public class MsftDiscFormat2DataClass
    {
    }
}

