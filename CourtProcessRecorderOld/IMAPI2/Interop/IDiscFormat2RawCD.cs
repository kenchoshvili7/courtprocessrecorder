﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    [ComImport, Guid("27354155-8F64-5B0F-8F00-5D77AFBE261E"), TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual)]
    public interface IDiscFormat2RawCD
    {
        [DispId(0x800)]
        bool IsRecorderSupported(IDiscRecorder2 Recorder);
        [DispId(0x801)]
        bool IsCurrentMediaSupported(IDiscRecorder2 Recorder);
        [DispId(0x700)]
        bool MediaPhysicallyBlank { get; }
        [DispId(0x701)]
        bool MediaHeuristicallyBlank { get; }
        [DispId(0x702)]
        object[] SupportedMediaTypes { get; }
        [DispId(0x200)]
        void PrepareMedia();
        [DispId(0x201)]
        void WriteMedia(IStream data);
        [DispId(0x202)]
        void WriteMedia2(IStream data, int streamLeadInSectors);
        [DispId(0x203)]
        void CancelWrite();
        [DispId(0x204)]
        void ReleaseMedia();
        [DispId(0x205)]
        void SetWriteSpeed(int RequestedSectorsPerSecond, bool RotationTypeIsPureCAV);
        [DispId(0x100)]
        IDiscRecorder2 Recorder { get; set; }
        [DispId(0x102)]
        bool BufferUnderrunFreeDisabled { get; set; }
        [DispId(0x103)]
        int StartOfNextSession { get; }
        [DispId(260)]
        int LastPossibleStartOfLeadout { get; }
        [DispId(0x105)]
        IMAPI_MEDIA_PHYSICAL_TYPE CurrentPhysicalMediaType { get; }
        [DispId(0x108)]
        object[] SupportedSectorTypes { get; }
        [DispId(0x109)]
        IMAPI_FORMAT2_RAW_CD_DATA_SECTOR_TYPE RequestedSectorType { get; set; }
        [DispId(0x10a)]
        string ClientName { get; set; }
        [DispId(0x10b)]
        int RequestedWriteSpeed { get; }
        [DispId(0x10c)]
        bool RequestedRotationTypeIsPureCAV { get; }
        [DispId(0x10d)]
        int CurrentWriteSpeed { get; }
        [DispId(270)]
        bool CurrentRotationTypeIsPureCAV { get; }
        [DispId(0x10f)]
        object[] SupportedWriteSpeeds { get; }
        [DispId(0x110)]
        object[] SupportedWriteSpeedDescriptors { get; }
    }
}

