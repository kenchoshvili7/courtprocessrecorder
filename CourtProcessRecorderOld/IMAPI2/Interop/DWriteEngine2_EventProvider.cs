﻿namespace IMAPI2.Interop
{
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;
    using System.Threading;

    [ClassInterface(ClassInterfaceType.None)]
    internal sealed class DWriteEngine2_EventProvider : DWriteEngine2_Event, IDisposable
    {
        private Hashtable m_aEventSinkHelpers = new Hashtable();
        private IConnectionPoint m_connectionPoint = null;

        public event DWriteEngine2_EventHandler Update
        {
            add
            {
                lock (this)
                {
                    int num;
                    DWriteEngine2_SinkHelper pUnkSink = new DWriteEngine2_SinkHelper(value);
                    this.m_connectionPoint.Advise(pUnkSink, out num);
                    pUnkSink.Cookie = num;
                    this.m_aEventSinkHelpers.Add(pUnkSink.UpdateDelegate, pUnkSink);
                }
            }
            remove
            {
                lock (this)
                {
                    DWriteEngine2_SinkHelper helper = this.m_aEventSinkHelpers[value] as DWriteEngine2_SinkHelper;
                    if (helper != null)
                    {
                        this.m_connectionPoint.Unadvise(helper.Cookie);
                        this.m_aEventSinkHelpers.Remove(helper.UpdateDelegate);
                    }
                }
            }
        }

        public DWriteEngine2_EventProvider(object pointContainer)
        {
            lock (this)
            {
                Guid gUID = typeof(DWriteEngine2Events).GUID;
                (pointContainer as IConnectionPointContainer).FindConnectionPoint(ref gUID, out this.m_connectionPoint);
            }
        }

        private void Cleanup()
        {
            Monitor.Enter(this);
            try
            {
                foreach (DWriteEngine2_SinkHelper helper in this.m_aEventSinkHelpers)
                {
                    this.m_connectionPoint.Unadvise(helper.Cookie);
                }
                this.m_aEventSinkHelpers.Clear();
                Marshal.ReleaseComObject(this.m_connectionPoint);
            }
            catch (SynchronizationLockException)
            {
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        public void Dispose()
        {
            this.Cleanup();
            GC.SuppressFinalize(this);
        }

        ~DWriteEngine2_EventProvider()
        {
            this.Cleanup();
        }
    }
}

