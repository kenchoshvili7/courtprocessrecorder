﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComVisible(false), TypeLibType((short) 0x10), ComEventInterface(typeof(DDiscFormat2RawCDEvents), typeof(DiscFormat2RawCD_EventProvider))]
    public interface DiscFormat2RawCD_Event
    {
        event DiscFormat2RawCD_EventHandler Update;
    }
}

