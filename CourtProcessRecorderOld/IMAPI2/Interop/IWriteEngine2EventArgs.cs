﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComImport, Guid("27354136-7F64-5B0F-8F00-5D77AFBE261E"), TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual)]
    public interface IWriteEngine2EventArgs
    {
        [DispId(0x100)]
        int StartLba { get; }
        [DispId(0x101)]
        int SectorCount { get; }
        [DispId(0x102)]
        int LastReadLba { get; }
        [DispId(0x103)]
        int LastWrittenLba { get; }
        [DispId(0x106)]
        int TotalSystemBuffer { get; }
        [DispId(0x107)]
        int UsedSystemBuffer { get; }
        [DispId(0x108)]
        int FreeSystemBuffer { get; }
    }
}

