﻿namespace IMAPI2.Interop
{
    using System;

    public enum IMAPI_FORMAT2_RAW_CD_WRITE_ACTION
    {
        IMAPI_FORMAT2_RAW_CD_WRITE_ACTION_UNKNOWN,
        IMAPI_FORMAT2_RAW_CD_WRITE_ACTION_PREPARING,
        IMAPI_FORMAT2_RAW_CD_WRITE_ACTION_WRITING,
        IMAPI_FORMAT2_RAW_CD_WRITE_ACTION_FINISHING
    }
}

