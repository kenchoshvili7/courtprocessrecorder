﻿namespace IMAPI2.Interop
{
    using System;

    public enum FsiFileSystems
    {
        FsiFileSystemISO9660 = 1,
        FsiFileSystemJoliet = 2,
        FsiFileSystemNone = 0,
        FsiFileSystemUDF = 4,
        FsiFileSystemUnknown = 0x40000000
    }
}

