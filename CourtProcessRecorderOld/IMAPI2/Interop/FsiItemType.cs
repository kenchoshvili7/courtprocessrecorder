﻿namespace IMAPI2.Interop
{
    using System;

    public enum FsiItemType
    {
        FsiItemNotFound,
        FsiItemDirectory,
        FsiItemFile
    }
}

