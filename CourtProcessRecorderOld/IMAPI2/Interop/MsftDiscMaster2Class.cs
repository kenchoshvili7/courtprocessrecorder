﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, TypeLibType(TypeLibTypeFlags.FCanCreate), ClassInterface(ClassInterfaceType.None), ComSourceInterfaces("DDiscMaster2Events"), Guid("2735412E-7F64-5B0F-8F00-5D77AFBE261E")]
    public class MsftDiscMaster2Class
    {
    }
}

