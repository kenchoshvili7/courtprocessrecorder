﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, CoClass(typeof(MsftWriteEngine2Class)), Guid("27354135-7F64-5B0F-8F00-5D77AFBE261E")]
    public interface MsftWriteEngine2 : IWriteEngine2, DWriteEngine2_Event
    {
    }
}

