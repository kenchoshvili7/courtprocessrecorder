﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    [Guid("2C941FD8-975B-59BE-A960-9A2A262853A5"), TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual)]
    public interface IFileSystemImageResult
    {
        [DispId(1)]
        IStream ImageStream { get; }
        [DispId(2)]
        IProgressItems ProgressItems { get; }
        [DispId(3)]
        int TotalBlocks { get; }
        [DispId(4)]
        int BlockSize { get; }
        [DispId(5)]
        string DiscId { get; }
    }
}

