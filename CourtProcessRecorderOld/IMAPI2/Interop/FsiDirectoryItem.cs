﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, CoClass(typeof(FsiDirectoryItemClass)), Guid("2C941FDC-975B-59BE-A960-9A2A262853A5")]
    public interface FsiDirectoryItem : IFsiDirectoryItem
    {
    }
}

