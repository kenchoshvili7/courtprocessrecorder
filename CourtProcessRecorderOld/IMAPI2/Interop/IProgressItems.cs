﻿namespace IMAPI2.Interop
{
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Runtime.InteropServices;

    [Guid("2C941FD7-975B-59BE-A960-9A2A262853A5"), TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual)]
    public interface IProgressItems
    {
        [DispId(-4), TypeLibFunc((short) 0x41)]
        IEnumerator GetEnumerator();
        [DispId(0)]
        IProgressItem this[int Index] { get; }
        [DispId(1)]
        int Count { get; }
        [DispId(2)]
        IProgressItem ProgressItemFromBlock(uint block);
        [DispId(3)]
        IProgressItem ProgressItemFromDescription(string Description);
        [DispId(4)]
        IEnumProgressItems EnumProgressItems { get; }
    }
}

