﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, Guid("2735413F-7F64-5B0F-8F00-5D77AFBE261E"), TypeLibType((short) 0x1180)]
    public interface DDiscFormat2TrackAtOnceEvents
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime), DispId(0x200)]
        void Update([In, MarshalAs(UnmanagedType.IDispatch)] object sender, [In, MarshalAs(UnmanagedType.IDispatch)] object progress);
    }
}

