﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual), Guid("2735413D-7F64-5B0F-8F00-5D77AFBE261E")]
    public interface IDiscFormat2DataEventArgs
    {
        [DispId(0x100)]
        int StartLba { get; }
        [DispId(0x101)]
        int SectorCount { get; }
        [DispId(0x102)]
        int LastReadLba { get; }
        [DispId(0x103)]
        int LastWrittenLba { get; }
        [DispId(0x106)]
        int TotalSystemBuffer { get; }
        [DispId(0x107)]
        int UsedSystemBuffer { get; }
        [DispId(0x108)]
        int FreeSystemBuffer { get; }
        [DispId(0x300)]
        int ElapsedTime { get; }
        [DispId(0x301)]
        int RemainingTime { get; }
        [DispId(770)]
        int TotalTime { get; }
        [DispId(0x303)]
        IMAPI_FORMAT2_DATA_WRITE_ACTION CurrentAction { get; }
    }
}

