﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, CoClass(typeof(MsftDiscMaster2Class)), Guid("27354130-7F64-5B0F-8F00-5D77AFBE261E")]
    public interface MsftDiscMaster2 : IDiscMaster2, DiscMaster2_Event
    {
    }
}

