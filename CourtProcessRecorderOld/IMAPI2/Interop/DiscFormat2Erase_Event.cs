﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComVisible(false), TypeLibType((short) 0x10), ComEventInterface(typeof(DDiscFormat2EraseEvents), typeof(DiscFormat2Erase_EventProvider))]
    public interface DiscFormat2Erase_Event
    {
        event DiscFormat2Erase_EventHandler Update;
    }
}

