﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    [TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual), Guid("27354153-9F64-5B0F-8F00-5D77AFBE261E")]
    public interface IDiscFormat2Data
    {
        [DispId(0x800)]
        bool IsRecorderSupported(IDiscRecorder2 Recorder);
        [DispId(0x801)]
        bool IsCurrentMediaSupported(IDiscRecorder2 Recorder);
        [DispId(0x700)]
        bool MediaPhysicallyBlank { get; }
        [DispId(0x701)]
        bool MediaHeuristicallyBlank { get; }
        [DispId(0x702)]
        object[] SupportedMediaTypes { get; }
        [DispId(0x100)]
        IDiscRecorder2 Recorder { get; set; }
        [DispId(0x101)]
        bool BufferUnderrunFreeDisabled { get; set; }
        [DispId(260)]
        bool PostgapAlreadyInImage { get; set; }
        [DispId(0x106)]
        IMAPI_FORMAT2_DATA_MEDIA_STATE CurrentMediaStatus { get; }
        [DispId(0x107)]
        IMAPI_MEDIA_WRITE_PROTECT_STATE WriteProtectStatus { get; }
        [DispId(0x108)]
        int TotalSectorsOnMedia { get; }
        [DispId(0x109)]
        int FreeSectorsOnMedia { get; }
        [DispId(0x10a)]
        int NextWritableAddress { get; }
        [DispId(0x10b)]
        int StartAddressOfPreviousSession { get; }
        [DispId(0x10c)]
        int LastWrittenAddressOfPreviousSession { get; }
        [DispId(0x10d)]
        bool ForceMediaToBeClosed { get; set; }
        [DispId(270)]
        bool DisableConsumerDvdCompatibilityMode { get; set; }
        [DispId(0x10f)]
        IMAPI_MEDIA_PHYSICAL_TYPE CurrentPhysicalMediaType { get; }
        [DispId(0x110)]
        string ClientName { get; set; }
        [DispId(0x111)]
        int RequestedWriteSpeed { get; }
        [DispId(0x112)]
        bool RequestedRotationTypeIsPureCAV { get; }
        [DispId(0x113)]
        int CurrentWriteSpeed { get; }
        [DispId(0x114)]
        bool CurrentRotationTypeIsPureCAV { get; }
        [DispId(0x115)]
        object[] SupportedWriteSpeeds { get; }
        [DispId(0x116)]
        object[] SupportedWriteSpeedDescriptors { get; }
        [DispId(0x117)]
        bool ForceOverwrite { get; set; }
        [DispId(280)]
        object[] MultisessionInterfaces { get; }
        [DispId(0x200)]
        void Write(IStream data);
        [DispId(0x201)]
        void CancelWrite();
        [DispId(0x202)]
        void SetWriteSpeed(int RequestedSectorsPerSecond, bool RotationTypeIsPureCAV);
    }
}

