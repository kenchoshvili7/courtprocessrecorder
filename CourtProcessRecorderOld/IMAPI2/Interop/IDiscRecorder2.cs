﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComImport, Guid("27354133-7F64-5B0F-8F00-5D77AFBE261E"), TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual)]
    public interface IDiscRecorder2
    {
        [DispId(0x100)]
        void EjectMedia();
        [DispId(0x101)]
        void CloseTray();
        [DispId(0x102)]
        void AcquireExclusiveAccess(bool force, string clientName);
        [DispId(0x103)]
        void ReleaseExclusiveAccess();
        [DispId(260)]
        void DisableMcn();
        [DispId(0x105)]
        void EnableMcn();
        [DispId(0x106)]
        void InitializeDiscRecorder(string recorderUniqueId);
        [DispId(0)]
        string ActiveDiscRecorder { get; }
        [DispId(0x201)]
        string VendorId { get; }
        [DispId(0x202)]
        string ProductId { get; }
        [DispId(0x203)]
        string ProductRevision { get; }
        [DispId(0x204)]
        string VolumeName { get; }
        [DispId(0x205)]
        object[] VolumePathNames { [DispId(0x205)] get; }
        [DispId(0x206)]
        bool DeviceCanLoadMedia { [DispId(0x206)] get; }
        [DispId(0x207)]
        int LegacyDeviceNumber { [DispId(0x207)] get; }
        [DispId(520)]
        object[] SupportedFeaturePages { [DispId(520)] get; }
        [DispId(0x209)]
        object[] CurrentFeaturePages { [DispId(0x209)] get; }
        [DispId(0x20a)]
        object[] SupportedProfiles { [DispId(0x20a)] get; }
        [DispId(0x20b)]
        object[] CurrentProfiles { [DispId(0x20b)] get; }
        [DispId(0x20c)]
        object[] SupportedModePages { [DispId(0x20c)] get; }
        [DispId(0x20d)]
        string ExclusiveAccessOwner { [DispId(0x20d)] get; }
    }
}

