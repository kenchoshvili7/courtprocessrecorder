﻿namespace IMAPI2.Interop
{
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    [TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual), Guid("2C941FDC-975B-59BE-A960-9A2A262853A5")]
    public interface IFsiDirectoryItem
    {
        [DispId(11)]
        string Name { get; }
        [DispId(12)]
        string FullPath { get; }
        [DispId(13)]
        DateTime CreationTime { get; set; }
        [DispId(14)]
        DateTime LastAccessedTime { get; set; }
        [DispId(15)]
        DateTime LastModifiedTime { get; set; }
        [DispId(0x10)]
        bool IsHidden { get; set; }
        [DispId(0x11)]
        string FileSystemName(FsiFileSystems fileSystem);
        [DispId(0x12)]
        string FileSystemPath(FsiFileSystems fileSystem);
        [TypeLibFunc((short) 0x41), DispId(-4)]
        System.Collections.IEnumerator GetEnumerator();
        [DispId(0)]
        IFsiItem this[string path] { get; }
        [DispId(1)]
        int Count { get; }
        [DispId(2)]
        IEnumFsiItems EnumFsiItems { get; }
        [DispId(30)]
        void AddDirectory(string path);
        [DispId(0x1f)]
        void AddFile(string path, IStream fileData);
        [DispId(0x20)]
        void AddTree(string sourceDirectory, bool includeBaseDirectory);
        [DispId(0x21)]
        void Add(IFsiItem Item);
        [DispId(0x22)]
        void Remove(string path);
        [DispId(0x23)]
        void RemoveTree(string path);
    }
}

