﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComImport, Guid("27354150-7F64-5B0F-8F00-5D77AFBE261E"), TypeLibType((short) 0x1000)]
    public interface IMultisession
    {
        [DispId(0x100)]
        bool IsSupportedOnCurrentMediaState { get; }
        [DispId(0x65)]
        bool InUse { get; set; }
        [DispId(0x102)]
        MsftDiscRecorder2 ImportRecorder { get; }
    }
}

