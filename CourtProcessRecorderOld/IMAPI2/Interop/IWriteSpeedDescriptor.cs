﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComImport, Guid("27354144-7F64-5B0F-8F00-5D77AFBE261E"), TypeLibType((short) 0x1040)]
    public interface IWriteSpeedDescriptor
    {
        [DispId(0x101)]
        IMAPI_MEDIA_PHYSICAL_TYPE MediaType { get; }
        [DispId(0x102)]
        bool RotationTypeIsPureCAV { get; }
        [DispId(0x103)]
        int WriteSpeed { get; }
    }
}

