﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("27354132-7F64-5B0F-8F00-5D77AFBE261E")]
    public interface IDiscRecorder2Ex
    {
        void SendCommandNoData(ref byte Cdb, uint CdbSize, out byte[] SenseBuffer, uint Timeout);
        void SendCommandSendDataToDevice(ref byte Cdb, uint CdbSize, out byte[] SenseBuffer, uint Timeout, ref byte Buffer, uint BufferSize);
        void SendCommandGetDataFromDevice(ref byte Cdb, uint CdbSize, out byte[] SenseBuffer, uint Timeout, out byte Buffer, uint BufferSize, out uint BufferFetched);
        void ReadDvdStructure(uint format, uint address, uint layer, uint agid, out IntPtr data, out uint Count);
        void SendDvdStructure(uint format, ref byte data, uint Count);
        void GetAdapterDescriptor(out IntPtr data, out uint byteSize);
        void GetDeviceDescriptor(out IntPtr data, out uint byteSize);
        void GetDiscInformation(out IntPtr discInformation, out uint byteSize);
        void GetTrackInformation(uint address, IMAPI_READ_TRACK_ADDRESS_TYPE addressType, out IntPtr trackInformation, out uint byteSize);
        void GetFeaturePage(IMAPI_FEATURE_PAGE_TYPE requestedFeature, sbyte currentFeatureOnly, out IntPtr featureData, out uint byteSize);
        void GetModePage(IMAPI_MODE_PAGE_TYPE requestedModePage, IMAPI_MODE_PAGE_REQUEST_TYPE requestType, out IntPtr modePageData, out uint byteSize);
        void SetModePage(IMAPI_MODE_PAGE_REQUEST_TYPE requestType, ref byte data, uint byteSize);
        void GetSupportedFeaturePages(sbyte currentFeatureOnly, out IntPtr featureData, out uint byteSize);
        void GetSupportedProfiles(sbyte currentOnly, out IntPtr profileTypes, out uint validProfiles);
        void GetSupportedModePages(IMAPI_MODE_PAGE_REQUEST_TYPE requestType, out IntPtr modePageTypes, out uint validPages);
        uint GetByteAlignmentMask();
        uint GetMaximumNonPageAlignedTransferSize();
        uint GetMaximumPageAlignedTransferSize();
    }
}

