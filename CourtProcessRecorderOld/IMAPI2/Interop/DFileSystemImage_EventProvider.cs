﻿namespace IMAPI2.Interop
{
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;
    using System.Threading;

    [ClassInterface(ClassInterfaceType.None)]
    internal sealed class DFileSystemImage_EventProvider : DFileSystemImage_Event, IDisposable
    {
        private Hashtable m_aEventSinkHelpers = new Hashtable();
        private IConnectionPoint m_connectionPoint = null;

        public event DFileSystemImage_EventHandler Update
        {
            add
            {
                lock (this)
                {
                    int num;
                    DFileSystemImage_SinkHelper pUnkSink = new DFileSystemImage_SinkHelper(value);
                    this.m_connectionPoint.Advise(pUnkSink, out num);
                    pUnkSink.Cookie = num;
                    this.m_aEventSinkHelpers.Add(pUnkSink.UpdateDelegate, pUnkSink);
                }
            }
            remove
            {
                lock (this)
                {
                    DFileSystemImage_SinkHelper helper = this.m_aEventSinkHelpers[value] as DFileSystemImage_SinkHelper;
                    if (helper != null)
                    {
                        this.m_connectionPoint.Unadvise(helper.Cookie);
                        this.m_aEventSinkHelpers.Remove(helper.UpdateDelegate);
                    }
                }
            }
        }

        public DFileSystemImage_EventProvider(object pointContainer)
        {
            lock (this)
            {
                Guid gUID = typeof(DFileSystemImageEvents).GUID;
                (pointContainer as IConnectionPointContainer).FindConnectionPoint(ref gUID, out this.m_connectionPoint);
            }
        }

        private void Cleanup()
        {
            Monitor.Enter(this);
            try
            {
                foreach (object obj2 in this.m_aEventSinkHelpers)
                {
                    if (obj2 is DFileSystemImage_SinkHelper)
                    {
                        this.m_connectionPoint.Unadvise((obj2 as DFileSystemImage_SinkHelper).Cookie);
                    }
                }
                this.m_aEventSinkHelpers.Clear();
                Marshal.ReleaseComObject(this.m_connectionPoint);
            }
            catch (SynchronizationLockException)
            {
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        public void Dispose()
        {
            this.Cleanup();
            GC.SuppressFinalize(this);
        }

        ~DFileSystemImage_EventProvider()
        {
            this.Cleanup();
        }
    }
}

