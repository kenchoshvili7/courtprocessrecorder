﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComImport, TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual), Guid("27354156-8F64-5B0F-8F00-5D77AFBE261E")]
    public interface IDiscFormat2Erase
    {
        [DispId(0x800)]
        bool IsRecorderSupported(IDiscRecorder2 Recorder);
        [DispId(0x801)]
        bool IsCurrentMediaSupported(IDiscRecorder2 Recorder);
        [DispId(0x700)]
        bool MediaPhysicallyBlank { get; }
        [DispId(0x701)]
        bool MediaHeuristicallyBlank { get; }
        [DispId(0x702)]
        object[] SupportedMediaTypes { get; }
        [DispId(0x100)]
        IDiscRecorder2 Recorder { get; set; }
        [DispId(0x101)]
        bool FullErase { get; set; }
        [DispId(0x102)]
        IMAPI_MEDIA_PHYSICAL_TYPE CurrentPhysicalMediaType { get; }
        [DispId(0x103)]
        string ClientName { get; set; }
        [DispId(0x201)]
        void EraseMedia();
    }
}

