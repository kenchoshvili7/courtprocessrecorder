﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, CoClass(typeof(MsftMultisessionClass)), Guid("27354150-7F64-5B0F-8F00-5D77AFBE261E")]
    public interface MsftMultisession : IMultisession
    {
    }
}

