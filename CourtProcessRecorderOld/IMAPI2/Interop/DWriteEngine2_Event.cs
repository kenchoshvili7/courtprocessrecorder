﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComVisible(false), ComEventInterface(typeof(DWriteEngine2Events), typeof(DWriteEngine2_EventProvider)), TypeLibType((short) 0x10)]
    public interface DWriteEngine2_Event
    {
        event DWriteEngine2_EventHandler Update;
    }
}

