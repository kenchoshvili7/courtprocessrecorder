﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, ClassInterface(ClassInterfaceType.None), Guid("2C941FC5-975B-59BE-A960-9A2A262853A5"), TypeLibType(TypeLibTypeFlags.FCanCreate), ComSourceInterfaces("DFileSystemImageEvents")]
    public class MsftFileSystemImageClass
    {
    }
}

