﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, Guid("27354153-9F64-5B0F-8F00-5D77AFBE261E"), CoClass(typeof(MsftDiscFormat2DataClass))]
    public interface MsftDiscFormat2Data : IDiscFormat2Data, DiscFormat2Data_Event
    {
    }
}

