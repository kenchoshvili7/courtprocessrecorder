﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [TypeLibType((short) 0x10), ComVisible(false), ComEventInterface(typeof(DFileSystemImageEvents), typeof(DFileSystemImage_EventProvider))]
    public interface DFileSystemImage_Event
    {
        event DFileSystemImage_EventHandler Update;
    }
}

