﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    [TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual), Guid("2C941FDB-975B-59BE-A960-9A2A262853A5")]
    public interface IFsiFileItem
    {
        [DispId(11)]
        string Name { get; }
        [DispId(12)]
        string FullPath { get; }
        [DispId(13)]
        DateTime CreationTime { get; set; }
        [DispId(14)]
        DateTime LastAccessedTime { get; set; }
        [DispId(15)]
        DateTime LastModifiedTime { get; set; }
        [DispId(0x10)]
        bool IsHidden { get; set; }
        [DispId(0x11)]
        string FileSystemName(FsiFileSystems fileSystem);
        [DispId(0x12)]
        string FileSystemPath(FsiFileSystems fileSystem);
        [DispId(0x29)]
        long DataSize { get; }
        [DispId(0x2a)]
        int DataSize32BitLow { get; }
        [DispId(0x2b)]
        int DataSize32BitHigh { get; }
        [DispId(0x2c)]
        IStream Data { get; set; }
    }
}

