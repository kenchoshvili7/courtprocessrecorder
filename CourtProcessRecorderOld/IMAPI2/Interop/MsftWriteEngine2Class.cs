﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, ComSourceInterfaces("DWriteEngine2Events"), ClassInterface(ClassInterfaceType.None), Guid("2735412C-7F64-5B0F-8F00-5D77AFBE261E"), TypeLibType(TypeLibTypeFlags.FCanCreate)]
    public class MsftWriteEngine2Class
    {
    }
}

