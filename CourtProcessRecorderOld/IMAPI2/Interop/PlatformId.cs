﻿namespace IMAPI2.Interop
{
    using System;

    public enum PlatformId
    {
        PlatformX86,
        PlatformPowerPC,
        PlatformMac
    }
}

