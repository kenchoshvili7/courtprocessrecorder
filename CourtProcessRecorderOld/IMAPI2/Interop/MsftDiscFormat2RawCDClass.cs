﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, ComSourceInterfaces("DDiscFormat2RawCDEvents"), ClassInterface(ClassInterfaceType.None), TypeLibType(TypeLibTypeFlags.FCanCreate), Guid("27354128-7F64-5B0F-8F00-5D77AFBE261E")]
    public class MsftDiscFormat2RawCDClass
    {
    }
}

