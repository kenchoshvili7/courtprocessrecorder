﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComImport, TypeLibType((short) 0x1000), Guid("27354151-7F64-5B0F-8F00-5D77AFBE261E")]
    public interface IMultisessionSequential : IMultisession
    {
        [DispId(200)]
        bool IsFirstDataSession { get; }
        [DispId(0x201)]
        int StartAddressOfPreviousSession { get; }
        [DispId(0x202)]
        int LastWrittenAddressOfPreviousSession { get; }
        [DispId(0x203)]
        int NextWritableAddress { get; }
        [DispId(0x204)]
        int FreeSectorsOnMedia { get; }
    }
}

