﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [Guid("2C941FD9-975B-59BE-A960-9A2A262853A5"), TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual)]
    public interface IFsiItem
    {
        [DispId(11)]
        string Name { get; }
        [DispId(12)]
        string FullPath { get; }
        [DispId(13)]
        DateTime CreationTime { get; set; }
        [DispId(14)]
        DateTime LastAccessedTime { get; set; }
        [DispId(15)]
        DateTime LastModifiedTime { get; set; }
        [DispId(0x10)]
        bool IsHidden { get; set; }
        [DispId(0x11)]
        string FileSystemName(FsiFileSystems fileSystem);
        [DispId(0x12)]
        string FileSystemPath(FsiFileSystems fileSystem);
    }
}

