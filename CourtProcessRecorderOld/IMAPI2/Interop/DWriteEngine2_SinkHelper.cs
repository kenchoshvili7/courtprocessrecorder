﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [TypeLibType(TypeLibTypeFlags.FHidden), ClassInterface(ClassInterfaceType.None)]
    public sealed class DWriteEngine2_SinkHelper : DWriteEngine2Events
    {
        private int m_dwCookie;
        private DWriteEngine2_EventHandler m_UpdateDelegate;

        public DWriteEngine2_SinkHelper(DWriteEngine2_EventHandler eventHandler)
        {
            if (eventHandler == null)
            {
                throw new ArgumentNullException("Delegate (callback function) cannot be null");
            }
            this.m_dwCookie = 0;
            this.m_UpdateDelegate = eventHandler;
        }

        public void Update(object sender, object progress)
        {
            this.m_UpdateDelegate(sender, progress);
        }

        public int Cookie
        {
            get
            {
                return this.m_dwCookie;
            }
            set
            {
                this.m_dwCookie = value;
            }
        }

        public DWriteEngine2_EventHandler UpdateDelegate
        {
            get
            {
                return this.m_UpdateDelegate;
            }
            set
            {
                this.m_UpdateDelegate = value;
            }
        }
    }
}

