﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [TypeLibType((short) 0x10), ComEventInterface(typeof(DDiscFormat2DataEvents), typeof(DiscFormat2Data_EventProvider)), ComVisible(false)]
    public interface DiscFormat2Data_Event
    {
        event DiscFormat2Data_EventHandler Update;
    }
}

