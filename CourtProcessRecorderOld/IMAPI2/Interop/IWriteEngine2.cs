﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    [ComImport, TypeLibType(TypeLibTypeFlags.FDispatchable | TypeLibTypeFlags.FNonExtensible | TypeLibTypeFlags.FDual), Guid("27354135-7F64-5B0F-8F00-5D77AFBE261E")]
    public interface IWriteEngine2
    {
        [DispId(0x200)]
        void WriteSection(IStream data, int startingBlockAddress, int numberOfBlocks);
        [DispId(0x201)]
        void CancelWrite();
        [DispId(0x100)]
        IDiscRecorder2Ex Recorder { get; set; }
        [DispId(0x101)]
        bool UseStreamingWrite12 { get; set; }
        [DispId(0x102)]
        int StartingSectorsPerSecond { get; set; }
        [DispId(0x103)]
        int EndingSectorsPerSecond { get; set; }
        [DispId(260)]
        int BytesPerSector { get; set; }
        [DispId(0x105)]
        bool WriteInProgress { get; }
    }
}

