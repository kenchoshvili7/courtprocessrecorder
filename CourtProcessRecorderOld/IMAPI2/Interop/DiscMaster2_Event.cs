﻿namespace IMAPI2.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [ComVisible(false), TypeLibType((short) 0x10), ComEventInterface(typeof(DDiscMaster2Events), typeof(DiscMaster2_EventProvider))]
    public interface DiscMaster2_Event
    {
        event DiscMaster2_NotifyDeviceAddedEventHandler NotifyDeviceAdded;

        event DiscMaster2_NotifyDeviceRemovedEventHandler NotifyDeviceRemoved;
    }
}

