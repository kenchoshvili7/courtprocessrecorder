﻿namespace IMAPI2.Interop
{
    using System.Runtime.InteropServices;

    [ComImport, CoClass(typeof(ProgressItemsClass)), Guid("2C941FD7-975B-59BE-A960-9A2A262853A5")]
    public interface ProgressItems : IProgressItems
    {
    }
}

