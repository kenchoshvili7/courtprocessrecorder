﻿namespace BurnMedia
{
    using IMAPI2.Interop;
    using System;
    using System.Drawing;

    internal interface IMediaItem
    {
        bool AddToFileSystem(IFsiDirectoryItem rootItem);

        Icon FileIcon { get; }

        string Path { get; }

        long SizeOnDisc { get; }
    }
}

