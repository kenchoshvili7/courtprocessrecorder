﻿namespace BurnMedia
{
    using IMAPI2.Interop;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    internal class DirectoryItem : IMediaItem
    {
        private string displayName;
        private Icon fileIcon = null;
        private string m_directoryPath;
        private List<IMediaItem> mediaItems = new List<IMediaItem>();

        public DirectoryItem(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                throw new FileNotFoundException("The directory added to DirectoryItem was not found!", directoryPath);
            }
            this.m_directoryPath = directoryPath;
            FileInfo info = new FileInfo(this.m_directoryPath);
            this.displayName = info.Name;
            string[] files = Directory.GetFiles(this.m_directoryPath);
            foreach (string str in files)
            {
                this.mediaItems.Add(new FileItem(str));
            }
            string[] directories = Directory.GetDirectories(this.m_directoryPath);
            foreach (string str2 in directories)
            {
                this.mediaItems.Add(new DirectoryItem(str2));
            }
            SHFILEINFO psfi = new SHFILEINFO();
            IntPtr ptr = Win32.SHGetFileInfo(this.m_directoryPath, 0, ref psfi, (uint) Marshal.SizeOf(psfi), 0x101);
            this.fileIcon = Icon.FromHandle(psfi.hIcon);
        }

        public bool AddToFileSystem(IFsiDirectoryItem rootItem)
        {
            try
            {
                rootItem.AddTree(this.m_directoryPath, true);
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error adding folder", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
        }

        public override string ToString()
        {
            return this.displayName;
        }

        public Icon FileIcon
        {
            get
            {
                return this.fileIcon;
            }
        }

        public string Path
        {
            get
            {
                return this.m_directoryPath;
            }
        }

        public long SizeOnDisc
        {
            get
            {
                long num = 0L;
                foreach (IMediaItem item in this.mediaItems)
                {
                    num += item.SizeOnDisc;
                }
                return num;
            }
        }
    }
}

