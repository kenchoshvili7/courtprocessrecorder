﻿namespace BurnMedia
{
    using IMAPI2.Interop;
    using System;
    using System.Drawing;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;
    using System.Windows.Forms;

    internal class FileItem : IMediaItem
    {
        private string displayName;
        private const uint FILE_ATTRIBUTE_NORMAL = 0x80;
        private Icon fileIcon = null;
        private string filePath;
        private long m_fileLength = 0L;
        private const long SECTOR_SIZE = 0x800L;
        private const uint STGM_DELETEONRELEASE = 0x4000000;
        private const uint STGM_READ = 0;
        private const uint STGM_SHARE_DENY_NONE = 0x40;
        private const uint STGM_SHARE_DENY_WRITE = 0x20;

        public FileItem(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("The file added to FileItem was not found!", path);
            }
            this.filePath = path;
            FileInfo info = new FileInfo(this.filePath);
            this.displayName = info.Name;
            this.m_fileLength = info.Length;
            SHFILEINFO psfi = new SHFILEINFO();
            IntPtr ptr = Win32.SHGetFileInfo(this.filePath, 0, ref psfi, (uint) Marshal.SizeOf(psfi), 0x101);
            this.fileIcon = Icon.FromHandle(psfi.hIcon);
        }

        public bool AddToFileSystem(IFsiDirectoryItem rootItem)
        {
            try
            {
                System.Runtime.InteropServices.ComTypes.IStream stream = null;
                SHCreateStreamOnFile(this.filePath, 0x20, ref stream);
                if (stream != null)
                {
                    rootItem.AddFile(this.displayName, stream);
                    return true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error adding file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return false;
        }

        [DllImport("shlwapi.dll", EntryPoint="SHCreateStreamOnFileW", CharSet=CharSet.Unicode, ExactSpelling=true, PreserveSig=false)]
        private static extern void SHCreateStreamOnFile(string fileName, uint mode, ref System.Runtime.InteropServices.ComTypes.IStream stream);
        public override string ToString()
        {
            return this.displayName;
        }

        public Icon FileIcon
        {
            get
            {
                return this.fileIcon;
            }
        }

        public string Path
        {
            get
            {
                return this.filePath;
            }
        }

        public long SizeOnDisc
        {
            get
            {
                if (this.m_fileLength > 0L)
                {
                    return (((this.m_fileLength / 0x800L) + 1L) * 0x800L);
                }
                return 0L;
            }
        }
    }
}

