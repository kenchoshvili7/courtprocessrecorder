﻿namespace CourtProcessPlayer
{
    partial class AudioFilesContainersUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.audioFilesContainersGridControl = new DevExpress.XtraGrid.GridControl();
            this.audioFilesContainerBindingSource = new System.Windows.Forms.BindingSource();
            this.audioFilesContainersGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.colEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeSpanEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit();
            ((System.ComponentModel.ISupportInitialize)(this.audioFilesContainersGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioFilesContainerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioFilesContainersGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeSpanEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // audioFilesContainersGridControl
            // 
            this.audioFilesContainersGridControl.DataSource = this.audioFilesContainerBindingSource;
            this.audioFilesContainersGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.audioFilesContainersGridControl.Location = new System.Drawing.Point(0, 0);
            this.audioFilesContainersGridControl.MainView = this.audioFilesContainersGridView;
            this.audioFilesContainersGridControl.Name = "audioFilesContainersGridControl";
            this.audioFilesContainersGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemTimeSpanEdit1});
            this.audioFilesContainersGridControl.Size = new System.Drawing.Size(224, 311);
            this.audioFilesContainersGridControl.TabIndex = 3;
            this.audioFilesContainersGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.audioFilesContainersGridView});
            // 
            // audioFilesContainerBindingSource
            // 
            this.audioFilesContainerBindingSource.DataSource = typeof(JsonDatabaseModels.AudioFilesContainer);
            // 
            // audioFilesContainersGridView
            // 
            this.audioFilesContainersGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStartTime,
            this.colEndTime});
            this.audioFilesContainersGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.audioFilesContainersGridView.GridControl = this.audioFilesContainersGridControl;
            this.audioFilesContainersGridView.Name = "audioFilesContainersGridView";
            this.audioFilesContainersGridView.OptionsDetail.EnableMasterViewMode = false;
            this.audioFilesContainersGridView.OptionsNavigation.UseTabKey = false;
            this.audioFilesContainersGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.audioFilesContainersGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.audioFilesContainersGridView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.audioFilesContainersGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colStartTime
            // 
            this.colStartTime.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colStartTime.AppearanceCell.Options.UseFont = true;
            this.colStartTime.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colStartTime.AppearanceHeader.Options.UseFont = true;
            this.colStartTime.Caption = "დაწყება";
            this.colStartTime.ColumnEdit = this.repositoryItemTimeEdit1;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.OptionsFilter.AllowFilter = false;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 0;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTimeEdit1.EditFormat.FormatString = "T";
            this.repositoryItemTimeEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // colEndTime
            // 
            this.colEndTime.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colEndTime.AppearanceCell.Options.UseFont = true;
            this.colEndTime.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colEndTime.AppearanceHeader.Options.UseFont = true;
            this.colEndTime.Caption = "დასრულება";
            this.colEndTime.ColumnEdit = this.repositoryItemTimeEdit1;
            this.colEndTime.FieldName = "EndTime";
            this.colEndTime.Name = "colEndTime";
            this.colEndTime.OptionsColumn.AllowEdit = false;
            this.colEndTime.Visible = true;
            this.colEndTime.VisibleIndex = 1;
            // 
            // repositoryItemTimeSpanEdit1
            // 
            this.repositoryItemTimeSpanEdit1.AutoHeight = false;
            this.repositoryItemTimeSpanEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeSpanEdit1.Mask.EditMask = "dd.HH:mm:ss";
            this.repositoryItemTimeSpanEdit1.Name = "repositoryItemTimeSpanEdit1";
            // 
            // AudioFilesContainersUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.audioFilesContainersGridControl);
            this.Name = "AudioFilesContainersUserControl";
            this.Size = new System.Drawing.Size(224, 311);
            ((System.ComponentModel.ISupportInitialize)(this.audioFilesContainersGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioFilesContainerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioFilesContainersGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeSpanEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl audioFilesContainersGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView audioFilesContainersGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit repositoryItemTimeSpanEdit1;
        private System.Windows.Forms.BindingSource audioFilesContainerBindingSource;
    }
}
