﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AudioConfiguration;
using CourtProcessPlayer.HelperClasses;
using CourtRecorderCommon;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DeviceController;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.Gui;
using RecordProvider;
using RecordProvider.HelperClasses;

namespace CourtProcessPlayer
{
    public partial class PlayerUserControl : XtraUserControl
    {
        private PlayHelper _playHelper;
        private Session _session;
        private AudioDevice _audioDevice;
        private PlaybackSpeed _playbackSpeed;
        private readonly Timer _playbackPositionTimer = new Timer();
        private readonly List<CustomWaveViewer> _waveViewers;
        private readonly List<VolumeMeter> _volumeMeters;
        private readonly List<LabelControl> _labels;
        private LabelControl _currentTimelbl;
        //private SessionDbHelper _sessionDbHelper;
        private GridView _sessionEventGridView;
        private GridControl _audioFilesContainersGridControl;
        private GridView _audioFilesContainersGridView;
        private TimeSpan _durationSum;
        private IOrderedEnumerable<AudioFilesContainer> _audioFilesContainers;
        private readonly Dictionary<PictureBox, Bitmap> _pbBitmap;
        public event PlaybackStopped PlaybackStopped;
        private Dictionary<AudioSourceChannels?, bool> _audioFilesMuteVolumes;
        private string _sessionPath;

        public PlayerUserControl()
        {
            InitializeComponent();

            _waveViewers = new List<CustomWaveViewer>();
            _volumeMeters = new List<VolumeMeter>();
            _labels = new List<LabelControl>();
            _pbBitmap = new Dictionary<PictureBox, Bitmap>();

            // if in design mode do not execute code below
            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;

            this.Load += OnLoad;

            var audioDevices = AudioDevice.GetPlayBackDevices();
            _audioDevice = audioDevices.FirstOrDefault();

            _playbackPositionTimer.Interval = 1000;
            playbackVolumeTrackBarControl.Properties.Maximum = 100;
            playbackVolumeTrackBarControl.EditValue = 100;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            _currentTimelbl = FormsHelper.FindControlByTypeAndName<LabelControl>(this, "currentTimelbl");
            _currentTimelbl.Text = GetCurrentDateTime().ToString("HH:mm:ss");

            var eventsGridControl = FormsHelper.FindControlByTypeAndName<GridControl>(this, "eventsGridControl");
            _sessionEventGridView = eventsGridControl.MainView as GridView;

            _audioFilesContainersGridControl = FormsHelper.FindControlByTypeAndName<GridControl>(this, "audioFilesContainersGridControl");
            _audioFilesContainersGridView = _audioFilesContainersGridControl.MainView as GridView;
            _audioFilesContainersGridView.Click += AudioFilesContainersGridViewOnClick;

            _audioFilesContainersGridControl.DataSource = _audioFilesContainers;
        }

        private void OnPlaybackOnMeterStreamVolume(AudioSourceChannels? channels, float maxSampleValues)
        {
            var volumeMeter = _volumeMeters.FirstOrDefault(v => ((Channel)v.Tag).ChannelNo == channels);
            if (volumeMeter != null) volumeMeter.Amplitude = maxSampleValues;
        }

        public async void InitializePlayerControl(Session session)
        {
            _session = session;
            _sessionPath = SessionDbWrapper.SessionDbHelper.SessionPath;
            //_sessionDbHelper = new SessionDbHelper(session.Path);
            //_sessionDbHelper = SessionDbWrapper.SessionDbHelper;

            _playbackPositionTimer.Tick += PlaybackPositionTimerOnTick;

            playbackVolumeTrackBarControl.MouseUp += PlaybackVolumeTrackBarControlOnMouseUp;

            MeteringSampleProviderWrapper.PlaybackMeterStreamVolume += OnPlaybackOnMeterStreamVolume;
            if (Global.GlobalMode == GlobalMode.Recorder) AudioIO.PlaybackMeterStreamVolume += OnPlaybackOnMeterStreamVolume;

            _audioFilesContainers = SessionDbWrapper.SessionDbHelper.ReloadSession(_session).AudioFilesContainers.OrderBy(a => a.StartTime);

            InitializeAudioFilesMuteVolumes();

            CreatePlayHelper();

            if (_audioFilesContainersGridControl != null) _audioFilesContainersGridControl.DataSource = _audioFilesContainers;

            _durationSum = _audioFilesContainers.Where(s => s.Duration != null).SumTimeSpan(d => d.Duration.Value);
            timelineTrackBar.Properties.Maximum = (int)_durationSum.TotalSeconds;
            timelineTrackBar.EditValue = 0;

            try
            {
                RemoveCustomControls();
                await AddCustomControls();
            }
            catch
            {
                // ignored
            }

            AssignImagesToPictureBoxes();

            if (_currentTimelbl != null) _currentTimelbl.Text = GetCurrentDateTime().ToString("HH:mm:ss");

            playerTableLayoutPanel.Refresh();
        }

        private void CreatePlayHelper()
        {
            if (_playHelper != null) _playHelper.Dispose();

            _playHelper = new PlayHelper(SessionDbWrapper.SessionDbHelper.ReloadSession(_session), _audioDevice, _playbackSpeed, _audioFilesMuteVolumes);
            _playHelper.PlaybackChunkChangedEvent += PlayHelperOnPlaybackChunkChangedEvent;
            _playHelper.PlaybackStopped += PlayHelperOnPlaybackStopped;
        }

        private async Task AddCustomControls()
        {
            var count = _session.Channels.Count;

            var height = (Height - timelineTrackBar.Height - count * 10) / count;
            var size = new Size(120, height);

            //var y2Channel = new Dictionary<AudioSourceChannels, int>();
            var channel2Row = new Dictionary<AudioSourceChannels, int>();

            var volumeTrackbar = playerTableLayoutPanel.Controls.Find("volumeMeterAndPlaybackVolumePanel", true).FirstOrDefault();
            var timelineTrackbar = playerTableLayoutPanel.Controls.Find("trackbarAndWaveViewerPanel", true).FirstOrDefault();

            playerTableLayoutPanel.RowStyles.Clear();
            playerTableLayoutPanel.RowCount = _session.Channels.Count + 1;

            for (var i = 0; i < _session.Channels.Count; i++)
            {
                var ch = _session.Channels[i];
                playerTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, height));
                channel2Row.Add(ch.ChannelNo, i);

                //y2Channel.Add(ch.ChannelNo, y);
                //y += height + 10;
            }

            playerTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 42));
            playerTableLayoutPanel.Controls.Add(volumeTrackbar, 0, _session.Channels.Count);
            playerTableLayoutPanel.Controls.Add(timelineTrackbar, 1, _session.Channels.Count);

            var tasks = _session.Channels.Select(async channel =>
            {
                //var yy = y2Channel[channel.ChannelNo];
                //});

                //foreach (var channel in _session.Channels)
                //{
                //var point = new Point(0, yy);
                //var waveViewerLocation = new Point(timelineTrackBar.Location.X, yy);

                var volumeMeter = new VolumeMeter
                {
                    Tag = channel,
                    Amplitude = 0F,
                    AutoSize = true,
                    ForeColor = Color.FromArgb(0, 191, 255),
                    MaxDb = 0F,
                    MinDb = -60F,
                    Name = channel.Name + "volumeMeter",
                    Size = size,
                    Dock = DockStyle.Fill,
                    Orientation = Orientation.Horizontal
                };

                playerTableLayoutPanel.Controls.Add(volumeMeter, 0, channel2Row[channel.ChannelNo]);

                _volumeMeters.Add(volumeMeter);

                var label = new LabelControl
                {
                    Text = channel.Name,
                    Font = new Font("Arial", 10, FontStyle.Bold),
                    ForeColor = Color.Black,
                    BackColor = Color.FromArgb(40, 220, 20, 60),
                    Tag = channel,
                    AutoSize = true,
                    AutoSizeMode = LabelAutoSizeMode.None,
                    Dock = DockStyle.Fill
                };
                label.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
                label.Appearance.TextOptions.VAlignment = VertAlignment.Center;
                label.Click += LabelOnClick;

                volumeMeter.Controls.Add(label);
                _labels.Add(label);

                #region waveViewer

                //var waveViewer = new CustomWaveViewer();
                //waveViewer.Location = waveViewerLocation;
                //waveViewer.SamplesPerPixel = 128;
                //waveViewer.Size = new Size(trackbarAndWaveViewerPanel.Width, height);
                //waveViewer.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                //waveViewer.StartPosition = 0;

                //var channelToWaveStream = _playHelper.ChannelToWaveStreams.FirstOrDefault(c => c.Channel.ChannelNo == channel.ChannelNo);

                //if (channelToWaveStream != null)
                //    waveViewer.WaveStream = channelToWaveStream.WaveStream;

                //waveViewer.FitToScreen();
                //trackbarAndWaveViewerPanel.Controls.Add(waveViewer);
                //_waveViewers.Add(waveViewer);

                #endregion waveViewer

                Bitmap waveImage = null;

                foreach (var audioFilesContainer in _audioFilesContainers)
                {
                    double durationTotalSeconds = 0;
                    if (audioFilesContainer.Duration != null)
                        durationTotalSeconds = audioFilesContainer.Duration.Value.TotalSeconds;

                    var width =
                        Convert.ToInt32(trackbarAndWaveViewerPanel.Width * durationTotalSeconds / _durationSum.TotalSeconds);
                    if (width < 1) continue;

                    var audioFiless = audioFilesContainer.AudioFiles.Where(x => x.Channel == channel.ChannelNo);

                    foreach (var audioFile in audioFiless)
                    {
                        var wg = new WaveformGenerator
                        {
                            Direction = WaveformGenerator.WaveformDirection.LeftToRight,
                            Orientation = WaveformGenerator.WaveformSideOrientation.LeftSideOnTopOrLeft,
                            Detail = 1.5f,
                            LeftSideBrush = new SolidBrush(Color.DeepSkyBlue),
                            RightSideBrush = new SolidBrush(Color.DeepSkyBlue)
                        };

                        wg.Completed += WgOnCompleted;
                        try
                        {
                            if (string.IsNullOrWhiteSpace(_sessionPath)) continue;

                            wg.CreateStream(_sessionPath + audioFile.Path);
                        }
                        catch
                        {
                            continue;
                        }

                        await wg.DetectWaveformLevelsAsync();

                        var image = wg.CreateWaveform(width, height);

                        if (waveImage == null) waveImage = image;
                        else
                        {
                            var bitmap = new Bitmap(waveImage.Width + width, height);
                            using (var g = Graphics.FromImage(bitmap))
                            {
                                g.DrawImage(waveImage, 0, 0, waveImage.Width, height);
                                g.DrawImage(image, waveImage.Width, 0, width, height);
                            }

                            waveImage = bitmap;
                        }
                    }
                }

                var pictureBox = new PictureBox
                {
                    Name = channel.ChannelNo.ToString(),
                    Dock = DockStyle.Fill,
                    BorderStyle = BorderStyle.None,
                    SizeMode = PictureBoxSizeMode.StretchImage
                };

                playerTableLayoutPanel.Controls.Add(pictureBox, 1, channel2Row[channel.ChannelNo]);

                _pbBitmap.Add(pictureBox, waveImage);
            });

            await Task.Factory.ContinueWhenAll(tasks.ToArray(), _ => { });
        }

        private void WgOnCompleted(object sender, EventArgs e)
        {
            var wg = sender as WaveformGenerator;
            wg.CloseStream();
        }

        private void AssignImagesToPictureBoxes()
        {
            foreach (var pb in _pbBitmap)
            {
                pb.Key.Image = pb.Value;
                pb.Key.Update();
            }
        }

        private void RemoveCustomControls()
        {
            foreach (var volumeMeter in _volumeMeters)
                playerTableLayoutPanel.Controls.Remove(volumeMeter);

            foreach (var waveViewer in _waveViewers)
                playerTableLayoutPanel.Controls.Remove(waveViewer);

            foreach (var pb in _pbBitmap)
            {
                playerTableLayoutPanel.Controls.Remove(pb.Key);
                pb.Value.Dispose();
            }

            _volumeMeters.Clear();
            _waveViewers.Clear();
            _labels.Clear();
            _pbBitmap.Clear();
        }

        private void LabelOnClick(object sender, EventArgs e)
        {
            var control = sender as Control;
            var channel = control.Tag as Channel ?? new Channel();

            var isMuted = _playHelper.MuteChannel(channel.ChannelNo);
            control.BackColor = isMuted ? Color.FromArgb(100, 128, 128, 128) : Color.FromArgb(40, 220, 20, 60);

            _audioFilesMuteVolumes[channel.ChannelNo] = isMuted;
        }

        public void PlaybackDeviceLookUpEditOnEditValueChanged(AudioDevice audioDevice)
        {
            _audioDevice = audioDevice;

            StopAndRestorePreviousState();
        }

        public void ChangePlaybackSpeed(PlaybackSpeed playbackSpeed)
        {
            _playbackSpeed = playbackSpeed;

            StopAndRestorePreviousState();
        }

        private void StopAndRestorePreviousState()
        {
            var positionBefore = _playHelper.GetPlaybackCurrentPosition();
            var playerStatusBefore = PlayerStatus;

            _playHelper.StopPlayback();

            CreatePlayHelper();

            Seek(positionBefore);

            _playHelper.SetPlaybackVolume((int)playbackVolumeTrackBarControl.EditValue);

            if (playerStatusBefore != PlayerStatus.Playing) return;
            _playbackPositionTimer.Start();
            _playHelper.PauseResumeAudio();
        }

        private void PlaybackVolumeTrackBarControlOnMouseUp(object sender, MouseEventArgs e)
        {
            _playHelper.SetPlaybackVolume((int)playbackVolumeTrackBarControl.EditValue);
        }

        public DateTime GetCurrentDateTime()
        {
            return _playHelper?.GetCurrentDateTime() ?? DateTime.MinValue;
        }

        public void PlayPause()
        {
            ReloadSession();

            _playbackPositionTimer.Start();

            if (_playHelper.PlayerStatus == PlayerStatus.Stopped)
            {
                _playHelper.PlayAudio(new AudioDevice());
            }
            else
            {
                _playHelper.PauseResumeAudio();
            }
        }

        public void Stop()
        {
            _playbackPositionTimer.Stop();
            _playHelper.StopPlayback();

            SetPlaybackPosition();

            _audioFilesContainersGridView.FocusedRowHandle = 0;
            _audioFilesContainersGridView.SelectRow(0);

            ResetVolumeMetersAmplitude();
        }

        public PlayerStatus PlayerStatus { get { return _playHelper.PlayerStatus; } }

        private void PlaybackPositionTimerOnTick(object sender, EventArgs e)
        {
            SetPlaybackPosition();
        }

        private void SetPlaybackPosition(bool fromOut = false)
        {
            var pos = _playHelper.GetPlaybackCurrentPosition();

            timelineTrackBar.EditValue = Math.Round(pos.TotalSeconds, MidpointRounding.AwayFromZero);

            _currentTimelbl.Text = GetCurrentDateTime().ToString("HH:mm:ss");

            if (!fromOut) SelectSessionEventsValue();
        }

        public void SeekFromOutside(DateTime dateTime, bool fromOut = true)
        {
            ReloadSession();

            var audioFilesContainer = _session.AudioFilesContainers.FirstOrDefault(a => a.StartTime.HasValue && a.StartTime.Value.TrimMilliseconds() <= dateTime && a.EndTime > dateTime);
            if (audioFilesContainer == null) return;

            var timeSpan = dateTime - audioFilesContainer.StartTime + audioFilesContainer.From;

            Seek((TimeSpan)timeSpan, fromOut);
        }

        private void timelineTrackBar_MouseUp(object sender, MouseEventArgs e)
        {
            var timeSpan = new TimeSpan(0, 0, Convert.ToInt32(timelineTrackBar.EditValue));
            Seek(timeSpan);
        }

        private void AudioFilesContainersGridViewOnClick(object sender, EventArgs e)
        {
            var gridView = sender as GridView;
            var audioFilesContainer = gridView.GetFocusedRow() as AudioFilesContainer;
            if (audioFilesContainer == null) return;

            Seek(audioFilesContainer.From);
        }

        private void Seek(TimeSpan timeSpan, bool fromOut = false)
        {
            _playbackPositionTimer.Stop();

            ReloadSession();

            _playHelper.SeekPlayback(timeSpan);

            if (_playHelper.PlayerStatus == PlayerStatus.Playing)
                _playbackPositionTimer.Start();
            else
            {
                SetPlaybackPosition(fromOut);
            }
        }

        private void PlayHelperOnPlaybackChunkChangedEvent(AudioFilesContainer audioFilesContainer)
        {
            if (_audioFilesContainersGridControl.InvokeRequired)
                Invoke(new MethodInvoker(() => SelectAudioFilesContainersValue(audioFilesContainer)));
            else
                SelectAudioFilesContainersValue(audioFilesContainer);
        }

        private void SelectAudioFilesContainersValue(AudioFilesContainer audioFilesContainer)
        {
            int rowHandle = _audioFilesContainersGridView.LocateByValue("From", audioFilesContainer.From);
            _audioFilesContainersGridView.FocusedRowHandle = rowHandle;
            _audioFilesContainersGridView.SelectRow(rowHandle);
        }

        private void SelectSessionEventsValue()
        {
            var time = GetCurrentDateTime();

            var sessionEvents = _session.SessionEvents.OrderByDescending(o => o.Time);
            var currenSessionEvent = sessionEvents.FirstOrDefault(e => e.Time <= time);
            if (currenSessionEvent == null) currenSessionEvent = sessionEvents.OrderBy(o => o.Time).FirstOrDefault(e => e.Time > time);

            _sessionEventGridView.ClearSelection();

            var rowHandle = sessionEvents.OrderBy(o => o.Time).ToList().IndexOf(currenSessionEvent);

            _sessionEventGridView.FocusedRowHandle = rowHandle;
            _sessionEventGridView.SelectRow(rowHandle);
        }

        private void PlayHelperOnPlaybackStopped()
        {
            if (PlaybackStopped != null) PlaybackStopped.Invoke();

            ResetVolumeMetersAmplitude();
        }

        private void InitializeAudioFilesMuteVolumes()
        {
            _audioFilesMuteVolumes = new Dictionary<AudioSourceChannels?, bool>();
            foreach (var channel in _session.Channels)
                _audioFilesMuteVolumes.Add(channel.ChannelNo, false);
        }

        public void ReloadSession()
        {
            if (_session == null) return;
            _session = SessionDbWrapper.SessionDbHelper.ReloadSession(_session);
        }

        private void ResetVolumeMetersAmplitude()
        {
            _volumeMeters.ForEach(v => v.Amplitude = 0);
        }

        public void DisposePlayer()
        {
            foreach (var pb in _pbBitmap)
            {
                pb.Value?.Dispose();
            }

            _playbackPositionTimer.Stop();
            if (_playHelper != null)
            {
                _playHelper.StopPlayback();
                _playHelper.Dispose();
            }

            _playbackPositionTimer.Tick -= PlaybackPositionTimerOnTick;
            if (playbackVolumeTrackBarControl != null)
                playbackVolumeTrackBarControl.MouseUp -= PlaybackVolumeTrackBarControlOnMouseUp;
            MeteringSampleProviderWrapper.PlaybackMeterStreamVolume -= OnPlaybackOnMeterStreamVolume;
            AudioIO.PlaybackMeterStreamVolume -= OnPlaybackOnMeterStreamVolume;

            if (_playHelper != null)
            {
                _playHelper.PlaybackChunkChangedEvent -= PlayHelperOnPlaybackChunkChangedEvent;
                _playHelper.PlaybackStopped -= PlaybackStopped;
            }

            //SessionDbWrapper.SessionDbHelper?.Dispose();
            _playHelper = null;
            _session = null;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var parms = base.CreateParams;
                parms.Style &= ~0x02000000;  // Turn off WS_CLIPCHILDREN
                return parms;
            }
        }
    }
}
