﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit;
using HelperClasses;
using JsonDatabaseModels;

namespace CourtProcessPlayer
{
    public partial class SessionReportPlayerForm : XtraForm
    {
        public SessionReportPlayerForm()
        {
            InitializeComponent();
        }

        public bool LoadSessionReport(Session session)
        {
            if (string.IsNullOrEmpty(session.DocumentStream)) return false;

            var existingMs = new MemoryStream(Convert.FromBase64String(session.DocumentStream)) { Position = 0 };
            richEditControl.LoadDocument(existingMs, DocumentFormat.Rtf);

            return true;
        }
    }
}
