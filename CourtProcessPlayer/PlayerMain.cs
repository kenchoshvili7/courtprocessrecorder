﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Windows.Forms;
using AudioConfiguration;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using HelperClasses;
using JsonDatabaseModels;
using NAudio.Wave;
using RecordProvider;
using RecordProvider.HelperClasses;
using Un4seen.Bass;

namespace CourtProcessPlayer
{
    public partial class PlayerMain : XtraForm
    {
        private Session _session;
        private string _argsPath;

        public PlayerMain(string[] args)
        {
            InitializeComponent();

            Global.ProgramMode = ProgramMode.CourtProcessPlayer;

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;

            if (WaveOut.DeviceCount == 0)
            {
                XtraMessageBox.Show("ვერ მოიძებნა აუდიო მოწყობილობა!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }

            BassRegistration.Register();
            Bass.BASS_Init(0, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);

            var audioDevices = AudioDevice.GetPlayBackDevices();

            playbackDeviceLookUpEdit.Properties.DataSource = audioDevices;
            playbackDeviceLookUpEdit.EditValue = 0;
            playbackDeviceLookUpEdit.EditValueChanged += PlaybackDeviceLookUpEditOnEditValueChanged;

            playbackSpeedLookUpEdit.Properties.DataSource = PlaybackSpeed.GetPlaybackSpeeds;
            playbackSpeedLookUpEdit.EditValue = 16000;
            playbackSpeedLookUpEdit.EditValueChanged += PlaybackSpeedLookUpEditOnEditValueChanged;

            var grid = sessionEventUserControl.Controls.Find("eventsGridControl", true).FirstOrDefault() as GridControl;
            var eventsGrid = grid.MainView as GridView;
            eventsGrid.OptionsBehavior.Editable = false;

            _argsPath = args.FirstOrDefault();

            OpenSesion();

            playerUserControl.PlaybackStopped += PlayerUserControlOnPlaybackStopped;

            seekTextEdit.KeyDown += SeekTextEditOnKeyDown;
        }

        private void OpenSesion()
        {
            var path = ConstantStrings.LocalDatabasePathForPlayer;

            if (!Directory.Exists(path))
            {
                if (string.IsNullOrWhiteSpace(_argsPath))
                {
                    var openFileDialog = new OpenFileDialog
                    {
                        DefaultExt = "*.cpr",
                        Filter = @".cpr Files (*.cpr)|*.cpr"
                    };

                    var result = openFileDialog.ShowDialog();

                    if (result != DialogResult.OK) Environment.Exit(0);
                    
                    if (string.IsNullOrWhiteSpace(openFileDialog.FileName)) Environment.Exit(0);
                    
                    _argsPath = openFileDialog.FileName;
                }

                var tempPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
                ZipFile.ExtractToDirectory(_argsPath, tempPath);

                path = tempPath;
            }

            SessionDbWrapper.Initialize(path, true);

            _session = SessionDbWrapper.SessionDbHelper.GetSession();
            _session.Path = path;

            sessionEventUserControl.InitializeSession(_session);

            playerUserControl.InitializePlayerControl(_session);

            Text = _session.Number;

            participantsGridControl.DataSource = _session.Participants;

            seekTextEdit.EditValue = _session.AudioFilesContainers.OrderBy(o => o.StartTime).FirstOrDefault()?.StartTime;
        }

        private void PlaybackDeviceLookUpEditOnEditValueChanged(object sender, EventArgs e)
        {
            var audioDevice = playbackDeviceLookUpEdit.GetSelectedDataRow() as AudioDevice;
            playerUserControl.PlaybackDeviceLookUpEditOnEditValueChanged(audioDevice);

            if (audioDevice.DeviceId == 0x7ffffff1) playbackSpeedLookUpEdit.Enabled = false;
            else playbackSpeedLookUpEdit.Enabled = true;

            playbackSpeedLookUpEdit.EditValue = 16000;

            //TogglePlayerButton();
        }

        private void PlaybackSpeedLookUpEditOnEditValueChanged(object sender, EventArgs eventArgs)
        {
            var playbackSpeed = playbackSpeedLookUpEdit.GetSelectedDataRow() as PlaybackSpeed;
            playerUserControl.ChangePlaybackSpeed(playbackSpeed);
        }

        private void playPausebtn_Click(object sender, EventArgs e)
        {
            playerUserControl.PlayPause();

            TogglePlayerButton();
        }

        private void stopbtn_Click(object sender, EventArgs e)
        {
            playerUserControl.Stop();

            TogglePlayerButton();
        }

        private void seekSimpleButton_Click(object sender, EventArgs e)
        {
            Seek();
        }

        private void SeekTextEditOnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                Seek();
        }

        private void Seek()
        {
            var time = Convert.ToDateTime(seekTextEdit.Text);
            playerUserControl.SeekFromOutside(time, false);
        }

        private void TogglePlayerButton()
        {
            if (playerUserControl.PlayerStatus == PlayerStatus.Playing)
            {
                playPausebtn.Text = @"პაუზა";
                playPausebtn.ImageIndex = 1;
            }
            else
            {
                playPausebtn.Text = @"მოსმენა";
                playPausebtn.ImageIndex = 0;
            }
        }

        private void printSimpleButton_Click(object sender, EventArgs e)
        {
            var sessionReport = new SessionReportPlayerForm();
            var isValid = sessionReport.LoadSessionReport(_session);
            if (isValid) sessionReport.Show();
        }

        private void PlayerUserControlOnPlaybackStopped()
        {
            if (InvokeRequired)
                Invoke(new MethodInvoker(TogglePlayerButton));
            else
                TogglePlayerButton();
        }

        private void MainMouseDown(object sender, MouseEventArgs e)
        {
            if (e is DXMouseEventArgs dxMouseEventArgs) dxMouseEventArgs.Handled = true;
        }
    }
}
