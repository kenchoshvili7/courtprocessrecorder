﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;

namespace CourtProcessPlayer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Environment.SetEnvironmentVariable("PATH", Environment.GetEnvironmentVariable("PATH") + ";" + "Data");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += ApplicationOnThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

            if (!IsWin7OrHigher())
            {
                XtraMessageBox.Show("მხარდაჭერილი ოპერაციული სისტემის ვერსიაა Windows 7 და ზემოთ!", "შეცდომა", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }

            var culture = new CultureInfo("en-US")
            {
                DateTimeFormat =
                {
                    ShortDatePattern = "dd.MM.yyyy",
                    LongDatePattern = "dd.MM.yyyy",
                    LongTimePattern = "HH:mm:ss"
                },
                NumberFormat =
                {
                    CurrencySymbol = "",
                    NumberDecimalDigits = 2,
                    NumberDecimalSeparator = ".",
                    CurrencyDecimalSeparator = ".",
                    PercentDecimalSeparator = ".",
                    PercentDecimalDigits = 10,
                    NumberGroupSeparator = ""
                }
            };
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            UserLookAndFeel.Default.SetSkinStyle("Office 2010 Silver");
            Application.Run(new PlayerMain(args));
        }

        private static void ApplicationOnThreadException(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.ToString());
        }

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;

            if (exception != null) MessageBox.Show(exception.ToString());
        }

        private static bool IsWin7OrHigher()
        {
            var os = Environment.OSVersion;
            return (os.Version.Major > 6) || ((os.Version.Major == 6) && (os.Version.Minor >= 1));
        }
    }
}
