﻿namespace CourtProcessPlayer
{
    partial class PlayerMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerMain));
            this.sessionEventUserControl = new CourtProcessPlayer.SessionEventUserControl();
            this.playerUserControl = new CourtProcessPlayer.PlayerUserControl();
            this.mediaControllerButtonsPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.playbackSpeedLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.seekSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.fastForwardImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.seekTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.currentTimelbl = new DevExpress.XtraEditors.LabelControl();
            this.playbackDeviceLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.audioDeviceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stopbtn = new DevExpress.XtraEditors.SimpleButton();
            this.stopImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.playPausebtn = new DevExpress.XtraEditors.SimpleButton();
            this.playPauseImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.sessionEventPanel = new DevExpress.XtraEditors.PanelControl();
            this.playerPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.participantAudioFilesContainersXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.participantsTab = new DevExpress.XtraTab.XtraTabPage();
            this.participantsGridControl = new DevExpress.XtraGrid.GridControl();
            this.participantsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParticipantDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.audioFilesContainersTab = new DevExpress.XtraTab.XtraTabPage();
            this.audioFilesContainersUserControl1 = new CourtProcessPlayer.AudioFilesContainersUserControl();
            this.printSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.mediaControllerButtonsPanelControl)).BeginInit();
            this.mediaControllerButtonsPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playbackSpeedLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fastForwardImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seekTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackDeviceLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playPauseImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventPanel)).BeginInit();
            this.sessionEventPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerPanelControl)).BeginInit();
            this.playerPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.participantAudioFilesContainersXtraTabControl)).BeginInit();
            this.participantAudioFilesContainersXtraTabControl.SuspendLayout();
            this.participantsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridView)).BeginInit();
            this.audioFilesContainersTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // sessionEventUserControl
            // 
            this.sessionEventUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sessionEventUserControl.Location = new System.Drawing.Point(0, 0);
            this.sessionEventUserControl.Name = "sessionEventUserControl";
            this.sessionEventUserControl.Size = new System.Drawing.Size(1190, 379);
            this.sessionEventUserControl.TabIndex = 0;
            this.sessionEventUserControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainMouseDown);
            // 
            // playerUserControl
            // 
            this.playerUserControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playerUserControl.Location = new System.Drawing.Point(227, 32);
            this.playerUserControl.Name = "playerUserControl";
            this.playerUserControl.Size = new System.Drawing.Size(961, 310);
            this.playerUserControl.TabIndex = 1;
            // 
            // mediaControllerButtonsPanelControl
            // 
            this.mediaControllerButtonsPanelControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mediaControllerButtonsPanelControl.Controls.Add(this.playbackSpeedLookUpEdit);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.seekSimpleButton);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.seekTextEdit);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.currentTimelbl);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.playbackDeviceLookUpEdit);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.stopbtn);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.printSimpleButton);
            this.mediaControllerButtonsPanelControl.Controls.Add(this.playPausebtn);
            this.mediaControllerButtonsPanelControl.Location = new System.Drawing.Point(1, 2);
            this.mediaControllerButtonsPanelControl.Name = "mediaControllerButtonsPanelControl";
            this.mediaControllerButtonsPanelControl.Size = new System.Drawing.Size(1188, 30);
            this.mediaControllerButtonsPanelControl.TabIndex = 2;
            // 
            // playbackSpeedLookUpEdit
            // 
            this.playbackSpeedLookUpEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackSpeedLookUpEdit.Location = new System.Drawing.Point(903, 4);
            this.playbackSpeedLookUpEdit.Name = "playbackSpeedLookUpEdit";
            this.playbackSpeedLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.playbackSpeedLookUpEdit.Properties.Appearance.Options.UseFont = true;
            this.playbackSpeedLookUpEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.playbackSpeedLookUpEdit.Properties.AppearanceDropDown.Options.UseFont = true;
            this.playbackSpeedLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.playbackSpeedLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.playbackSpeedLookUpEdit.Properties.DisplayMember = "Description";
            this.playbackSpeedLookUpEdit.Properties.DropDownRows = 4;
            this.playbackSpeedLookUpEdit.Properties.ShowHeader = false;
            this.playbackSpeedLookUpEdit.Properties.ValueMember = "SampleRate";
            this.playbackSpeedLookUpEdit.Size = new System.Drawing.Size(100, 22);
            this.playbackSpeedLookUpEdit.TabIndex = 25;
            this.playbackSpeedLookUpEdit.ToolTip = "მოსმენის სიჩქარე";
            // 
            // seekSimpleButton
            // 
            this.seekSimpleButton.ImageIndex = 0;
            this.seekSimpleButton.ImageList = this.fastForwardImageCollection;
            this.seekSimpleButton.Location = new System.Drawing.Point(140, 2);
            this.seekSimpleButton.Name = "seekSimpleButton";
            this.seekSimpleButton.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.seekSimpleButton.Size = new System.Drawing.Size(80, 25);
            this.seekSimpleButton.TabIndex = 24;
            this.seekSimpleButton.Text = "გადახვევა";
            this.seekSimpleButton.Click += new System.EventHandler(this.seekSimpleButton_Click);
            // 
            // fastForwardImageCollection
            // 
            this.fastForwardImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("fastForwardImageCollection.ImageStream")));
            this.fastForwardImageCollection.Images.SetKeyName(0, "fastforward.png");
            // 
            // seekTextEdit
            // 
            this.seekTextEdit.EditValue = "";
            this.seekTextEdit.Location = new System.Drawing.Point(5, 4);
            this.seekTextEdit.Name = "seekTextEdit";
            this.seekTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.seekTextEdit.Properties.Appearance.Options.UseFont = true;
            this.seekTextEdit.Properties.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.seekTextEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.seekTextEdit.Properties.EditFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.seekTextEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.seekTextEdit.Properties.Mask.EditMask = "dd.MM.yyyy HH:mm:ss";
            this.seekTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.seekTextEdit.Size = new System.Drawing.Size(130, 22);
            this.seekTextEdit.TabIndex = 23;
            this.seekTextEdit.ToolTip = "გადახვევის დრო";
            // 
            // currentTimelbl
            // 
            this.currentTimelbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.currentTimelbl.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.currentTimelbl.Location = new System.Drawing.Point(578, 5);
            this.currentTimelbl.Name = "currentTimelbl";
            this.currentTimelbl.Size = new System.Drawing.Size(72, 19);
            this.currentTimelbl.TabIndex = 2;
            this.currentTimelbl.Text = "00:00:00";
            // 
            // playbackDeviceLookUpEdit
            // 
            this.playbackDeviceLookUpEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackDeviceLookUpEdit.Location = new System.Drawing.Point(773, 4);
            this.playbackDeviceLookUpEdit.Name = "playbackDeviceLookUpEdit";
            this.playbackDeviceLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.playbackDeviceLookUpEdit.Properties.Appearance.Options.UseFont = true;
            this.playbackDeviceLookUpEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.playbackDeviceLookUpEdit.Properties.AppearanceDropDown.Options.UseFont = true;
            this.playbackDeviceLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.playbackDeviceLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DeviceName", "Device Name", 72, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.playbackDeviceLookUpEdit.Properties.DataSource = this.audioDeviceBindingSource;
            this.playbackDeviceLookUpEdit.Properties.DisplayMember = "DeviceName";
            this.playbackDeviceLookUpEdit.Properties.DropDownRows = 3;
            this.playbackDeviceLookUpEdit.Properties.ShowHeader = false;
            this.playbackDeviceLookUpEdit.Properties.ValueMember = "DeviceId";
            this.playbackDeviceLookUpEdit.Size = new System.Drawing.Size(120, 22);
            this.playbackDeviceLookUpEdit.TabIndex = 1;
            this.playbackDeviceLookUpEdit.ToolTip = "მოსასმენი მოწყობილობა";
            this.playbackDeviceLookUpEdit.Visible = false;
            // 
            // audioDeviceBindingSource
            // 
            this.audioDeviceBindingSource.DataSource = typeof(RecordProvider.AudioDevice);
            // 
            // stopbtn
            // 
            this.stopbtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stopbtn.ImageIndex = 0;
            this.stopbtn.ImageList = this.stopImageCollection;
            this.stopbtn.Location = new System.Drawing.Point(1103, 2);
            this.stopbtn.Name = "stopbtn";
            this.stopbtn.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.stopbtn.Size = new System.Drawing.Size(80, 25);
            this.stopbtn.TabIndex = 0;
            this.stopbtn.Text = "გაჩერება";
            this.stopbtn.Click += new System.EventHandler(this.stopbtn_Click);
            // 
            // stopImageCollection
            // 
            this.stopImageCollection.ImageSize = new System.Drawing.Size(20, 20);
            this.stopImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("stopImageCollection.ImageStream")));
            this.stopImageCollection.Images.SetKeyName(0, "stop1.png");
            // 
            // playPausebtn
            // 
            this.playPausebtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playPausebtn.ImageIndex = 0;
            this.playPausebtn.ImageList = this.playPauseImageCollection;
            this.playPausebtn.Location = new System.Drawing.Point(1013, 2);
            this.playPausebtn.Name = "playPausebtn";
            this.playPausebtn.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.playPausebtn.Size = new System.Drawing.Size(80, 25);
            this.playPausebtn.TabIndex = 0;
            this.playPausebtn.Text = "მოსმენა";
            this.playPausebtn.Click += new System.EventHandler(this.playPausebtn_Click);
            // 
            // playPauseImageCollection
            // 
            this.playPauseImageCollection.ImageSize = new System.Drawing.Size(20, 20);
            this.playPauseImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("playPauseImageCollection.ImageStream")));
            this.playPauseImageCollection.Images.SetKeyName(0, "play.png");
            this.playPauseImageCollection.Images.SetKeyName(1, "pause.png");
            // 
            // sessionEventPanel
            // 
            this.sessionEventPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sessionEventPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.sessionEventPanel.Controls.Add(this.sessionEventUserControl);
            this.sessionEventPanel.Location = new System.Drawing.Point(1, 2);
            this.sessionEventPanel.Name = "sessionEventPanel";
            this.sessionEventPanel.Size = new System.Drawing.Size(1190, 379);
            this.sessionEventPanel.TabIndex = 3;
            // 
            // playerPanelControl
            // 
            this.playerPanelControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playerPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.playerPanelControl.Controls.Add(this.participantAudioFilesContainersXtraTabControl);
            this.playerPanelControl.Controls.Add(this.playerUserControl);
            this.playerPanelControl.Controls.Add(this.mediaControllerButtonsPanelControl);
            this.playerPanelControl.Location = new System.Drawing.Point(1, 380);
            this.playerPanelControl.Name = "playerPanelControl";
            this.playerPanelControl.Size = new System.Drawing.Size(1190, 344);
            this.playerPanelControl.TabIndex = 4;
            // 
            // participantAudioFilesContainersXtraTabControl
            // 
            this.participantAudioFilesContainersXtraTabControl.Location = new System.Drawing.Point(0, 32);
            this.participantAudioFilesContainersXtraTabControl.Name = "participantAudioFilesContainersXtraTabControl";
            this.participantAudioFilesContainersXtraTabControl.SelectedTabPage = this.participantsTab;
            this.participantAudioFilesContainersXtraTabControl.Size = new System.Drawing.Size(221, 312);
            this.participantAudioFilesContainersXtraTabControl.TabIndex = 22;
            this.participantAudioFilesContainersXtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.participantsTab,
            this.audioFilesContainersTab});
            // 
            // participantsTab
            // 
            this.participantsTab.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.participantsTab.Appearance.Header.Options.UseFont = true;
            this.participantsTab.Appearance.Header.Options.UseTextOptions = true;
            this.participantsTab.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.participantsTab.Controls.Add(this.participantsGridControl);
            this.participantsTab.Name = "participantsTab";
            this.participantsTab.Size = new System.Drawing.Size(215, 280);
            this.participantsTab.TabPageWidth = 87;
            this.participantsTab.Text = "მონაწილეები";
            // 
            // participantsGridControl
            // 
            this.participantsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.participantsGridControl.Location = new System.Drawing.Point(0, 0);
            this.participantsGridControl.MainView = this.participantsGridView;
            this.participantsGridControl.Name = "participantsGridControl";
            this.participantsGridControl.Size = new System.Drawing.Size(215, 280);
            this.participantsGridControl.TabIndex = 6;
            this.participantsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.participantsGridView});
            // 
            // participantsGridView
            // 
            this.participantsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPosition,
            this.colName,
            this.colParticipantDescription,
            this.colUId,
            this.colCreatedOn});
            this.participantsGridView.GridControl = this.participantsGridControl;
            this.participantsGridView.Name = "participantsGridView";
            this.participantsGridView.OptionsCustomization.AllowColumnMoving = false;
            this.participantsGridView.OptionsCustomization.AllowColumnResizing = false;
            this.participantsGridView.OptionsCustomization.AllowGroup = false;
            this.participantsGridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.participantsGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.participantsGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.participantsGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colPosition
            // 
            this.colPosition.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPosition.AppearanceCell.Options.UseFont = true;
            this.colPosition.FieldName = "Position";
            this.colPosition.Name = "colPosition";
            this.colPosition.Width = 74;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colName.AppearanceCell.Options.UseFont = true;
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.Width = 74;
            // 
            // colParticipantDescription
            // 
            this.colParticipantDescription.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colParticipantDescription.AppearanceCell.Options.UseFont = true;
            this.colParticipantDescription.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colParticipantDescription.AppearanceHeader.Options.UseFont = true;
            this.colParticipantDescription.Caption = "მონაწილე";
            this.colParticipantDescription.FieldName = "ParticipantDescription";
            this.colParticipantDescription.Name = "colParticipantDescription";
            this.colParticipantDescription.OptionsColumn.AllowEdit = false;
            this.colParticipantDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colParticipantDescription.OptionsColumn.ReadOnly = true;
            this.colParticipantDescription.Visible = true;
            this.colParticipantDescription.VisibleIndex = 0;
            this.colParticipantDescription.Width = 128;
            // 
            // colUId
            // 
            this.colUId.FieldName = "UId";
            this.colUId.Name = "colUId";
            this.colUId.Width = 47;
            // 
            // colCreatedOn
            // 
            this.colCreatedOn.FieldName = "CreatedOn";
            this.colCreatedOn.Name = "colCreatedOn";
            this.colCreatedOn.Width = 48;
            // 
            // audioFilesContainersTab
            // 
            this.audioFilesContainersTab.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.audioFilesContainersTab.Appearance.Header.Options.UseFont = true;
            this.audioFilesContainersTab.Appearance.Header.Options.UseTextOptions = true;
            this.audioFilesContainersTab.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.audioFilesContainersTab.Controls.Add(this.audioFilesContainersUserControl1);
            this.audioFilesContainersTab.Name = "audioFilesContainersTab";
            this.audioFilesContainersTab.Size = new System.Drawing.Size(215, 280);
            this.audioFilesContainersTab.TabPageWidth = 87;
            this.audioFilesContainersTab.Text = "დროები";
            // 
            // audioFilesContainersUserControl1
            // 
            this.audioFilesContainersUserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.audioFilesContainersUserControl1.Location = new System.Drawing.Point(0, 0);
            this.audioFilesContainersUserControl1.Name = "audioFilesContainersUserControl1";
            this.audioFilesContainersUserControl1.Size = new System.Drawing.Size(215, 280);
            this.audioFilesContainersUserControl1.TabIndex = 21;
            // 
            // printSimpleButton
            // 
            this.printSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.printSimpleButton.Image = ((System.Drawing.Image)(resources.GetObject("printSimpleButton.Image")));
            this.printSimpleButton.ImageIndex = 0;
            this.printSimpleButton.Location = new System.Drawing.Point(234, 2);
            this.printSimpleButton.Name = "printSimpleButton";
            this.printSimpleButton.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.printSimpleButton.Size = new System.Drawing.Size(109, 25);
            this.printSimpleButton.TabIndex = 0;
            this.printSimpleButton.Text = "სხდომის ოქმი";
            this.printSimpleButton.Click += new System.EventHandler(this.printSimpleButton_Click);
            // 
            // PlayerMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 723);
            this.Controls.Add(this.playerPanelControl);
            this.Controls.Add(this.sessionEventPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PlayerMain";
            this.Text = "Court Process Player";
            ((System.ComponentModel.ISupportInitialize)(this.mediaControllerButtonsPanelControl)).EndInit();
            this.mediaControllerButtonsPanelControl.ResumeLayout(false);
            this.mediaControllerButtonsPanelControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playbackSpeedLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fastForwardImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seekTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackDeviceLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playPauseImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventPanel)).EndInit();
            this.sessionEventPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playerPanelControl)).EndInit();
            this.playerPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.participantAudioFilesContainersXtraTabControl)).EndInit();
            this.participantAudioFilesContainersXtraTabControl.ResumeLayout(false);
            this.participantsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGridView)).EndInit();
            this.audioFilesContainersTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SessionEventUserControl sessionEventUserControl;
        private PlayerUserControl playerUserControl;
        private DevExpress.XtraEditors.PanelControl mediaControllerButtonsPanelControl;
        private DevExpress.XtraEditors.SimpleButton stopbtn;
        private DevExpress.XtraEditors.SimpleButton playPausebtn;
        private DevExpress.XtraEditors.LookUpEdit playbackDeviceLookUpEdit;
        private DevExpress.XtraEditors.LabelControl currentTimelbl;
        private DevExpress.XtraEditors.PanelControl sessionEventPanel;
        private DevExpress.XtraEditors.PanelControl playerPanelControl;
        private System.Windows.Forms.BindingSource audioDeviceBindingSource;
        private DevExpress.Utils.ImageCollection playPauseImageCollection;
        private DevExpress.Utils.ImageCollection stopImageCollection;
        private DevExpress.XtraEditors.SimpleButton seekSimpleButton;
        private DevExpress.XtraEditors.TextEdit seekTextEdit;
        private DevExpress.XtraEditors.LookUpEdit playbackSpeedLookUpEdit;
        private DevExpress.Utils.ImageCollection fastForwardImageCollection;
        private DevExpress.XtraTab.XtraTabControl participantAudioFilesContainersXtraTabControl;
        private DevExpress.XtraTab.XtraTabPage participantsTab;
        private DevExpress.XtraGrid.GridControl participantsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView participantsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colParticipantDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colUId;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedOn;
        private DevExpress.XtraTab.XtraTabPage audioFilesContainersTab;
        private AudioFilesContainersUserControl audioFilesContainersUserControl1;
        private DevExpress.XtraEditors.SimpleButton printSimpleButton;
    }
}

