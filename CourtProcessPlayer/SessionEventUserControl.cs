﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CourtProcessPlayer.HelperClasses;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using HelperClasses;
using JsonDatabaseModels;

namespace CourtProcessPlayer
{
    public partial class SessionEventUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        private PlayerUserControl _playerUserControl;
        //private readonly DatabaseHelper _databaseHelper;
        //private SessionDbHelper _sessionDbHelper;
        private Session _currentSession;

        public SessionEventUserControl()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            eventsGridView.Click += EventsGridViewOnClick;
            timeTextEdit.Click += EventsGridViewOnClick;
            noteMemoEdit.Click += EventsGridViewOnClick;

            eventsGridView.CellValueChanged += EventsGridViewOnCellValueChanged;

            _playerUserControl = FormsHelper.FindControlByTypeAndName<PlayerUserControl>(this, "playerUserControl");

            //if (Parent.Parent.Parent != null)
            //    _playerUserControl = Parent.Parent.Parent.Controls.Find("playerUserControl", true).FirstOrDefault() as PlayerUserControl;
            //else _playerUserControl = Parent.Parent.Controls.Find("playerUserControl", true).FirstOrDefault() as PlayerUserControl;

            repositoryItemDateEdit1.DisplayFormat.FormatString = "T";
        }

        private void EventsGridViewOnClick(object sender, EventArgs e)
        {
            if (!_playerUserControl.Visible) return;

            if (eventsGridView == null) return;

            if (eventsGridView.SelectedRowsCount > 1) return;

            var sessionEvent = eventsGridView.GetFocusedRow() as SessionEvent;
            if (sessionEvent == null) return;

            //gridView.ClearSelection();

            _playerUserControl.SeekFromOutside(sessionEvent.Time);
        }

        private void EventsGridViewOnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            _currentSession = SessionDbWrapper.SessionDbHelper.ReloadSession(_currentSession);

            var selectedSessionEvent = eventsGridView.GetFocusedRow() as SessionEvent;
            if (selectedSessionEvent == null) return;

            var sessionEvent = _currentSession.SessionEvents.FirstOrDefault(s => s.UId == selectedSessionEvent.UId);

            if (sessionEvent == null) return;

            if (IsStartStopEvent(selectedSessionEvent))
            {
                selectedSessionEvent.Time = sessionEvent.Time;
                selectedSessionEvent.Note = string.Empty;
                return;
            }

            var time = new DateTime(sessionEvent.Time.Year, sessionEvent.Time.Month, sessionEvent.Time.Day, selectedSessionEvent.Time.Hour, selectedSessionEvent.Time.Minute, selectedSessionEvent.Time.Second);

            sessionEvent.Time = time;
            sessionEvent.Note = selectedSessionEvent.Note;

            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            ReloadDataSource();

            ChangeFocusedRow(sessionEvent);

            _playerUserControl.ReloadSession();
        }

        public void InitializeSession(Session session)
        {
            _currentSession = ReloadSessionFromDatabase(session);
            if (_currentSession.SessionEvents != null)
                eventsGridControl.DataSource = _currentSession.SessionEvents.OrderBy(o => o.Time);
        }

        public void AddEvent(Session session, SessionEvent sessionEvent)
        {
            _currentSession = ReloadSessionFromDatabase(session);
            if (_currentSession.SessionEvents == null) _currentSession.SessionEvents = new List<SessionEvent>();

            _currentSession.SessionEvents.Add(sessionEvent);
            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            ChangeFocusedRow(sessionEvent);

            _playerUserControl.ReloadSession();
        }

        public void UpdateEvent(Session session, SessionEvent sessionEvent)
        {
            _currentSession = ReloadSessionFromDatabase(session);
            if (_currentSession.SessionEvents == null) return;

            var selectedSessionEvent = GetSelectedSessionEvent();

            if (IsStartStopEvent(selectedSessionEvent)) return;

            if (sessionEvent.Participant == null)
            {
                var se = _currentSession.SessionEvents.FirstOrDefault(s => s.UId == selectedSessionEvent.UId);
                if (se != null) se.SessionAction = sessionEvent.SessionAction;
            }
            else
            {
                var se = _currentSession.SessionEvents.FirstOrDefault(s => s.UId == selectedSessionEvent.UId);
                if (se != null) se.Participant = sessionEvent.Participant;
            }

            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            ChangeFocusedRow(selectedSessionEvent);

            _playerUserControl.ReloadSession();
        }

        //public void RemoveEvent(Session session, SessionEvent sessionEvent)
        //{
        //    if (IsStartStopEvent(sessionEvent)) return;

        //    _currentSession = ReloadSessionFromDatabase(session);
        //    _currentSession.SessionEvents.RemoveAll(s => s.UId == sessionEvent.UId);
        //    _databaseHelper.UpdateSession(_currentSession);

        //    ReloadDataSource();
        //    ChangeFocusedRow(sessionEvent);

        //    _playerUserControl.ReloadSession();
        //}

        public void RemoveEvents(Session session)
        {
            _currentSession = ReloadSessionFromDatabase(session);

            var focusedRow = eventsGridView.FocusedRowHandle - 1;

            var info = GetGridViewInfo(eventsGridView);
            var firstRow = info.RowsInfo?.FirstOrDefault();
            if (firstRow == null) return;
            var firstRowHandle = firstRow.RowHandle;

            foreach (var index in eventsGridView.GetSelectedRows())
            {
                var selectedSessionEvent = eventsGridView.GetRow(index) as SessionEvent;
                if (IsStartStopEvent(selectedSessionEvent)) continue;

                _currentSession.SessionEvents.RemoveAll(s => s.UId == selectedSessionEvent?.UId);
            }

            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            ReloadDataSource();

            eventsGridView.TopRowIndex = firstRowHandle;
            eventsGridView.FocusedRowHandle = focusedRow;

            eventsGridView.ClearSelection();
            eventsGridView.SelectRow(focusedRow);

            _playerUserControl.ReloadSession();
        }

        public void SecondsBefore()
        {
            ChangeSecondsInSessionEvent(-1);
        }

        public void SecondsAfter()
        {
            ChangeSecondsInSessionEvent(1);
        }

        private void ChangeSecondsInSessionEvent(int seconds)
        {
            var selectedSessionEvent = eventsGridView.GetFocusedRow() as SessionEvent;

            if (selectedSessionEvent == null) return;

            if (IsStartStopEvent(selectedSessionEvent)) return;

            var sessionEvent = _currentSession.SessionEvents.FirstOrDefault(s => s.UId == selectedSessionEvent.UId);

            sessionEvent.Time = selectedSessionEvent.Time.AddSeconds(seconds);
            SessionDbWrapper.SessionDbHelper.UpdateSession(_currentSession);

            ReloadDataSource();
            ChangeFocusedRow(sessionEvent);

            _playerUserControl.ReloadSession();
        }

        public SessionEvent GetSelectedSessionEvent()
        {
            var sessionEvent = eventsGridView.GetFocusedRow() as SessionEvent;
            return sessionEvent;
        }

        private void ChangeFocusedRow(SessionEvent sessionEvent)
        {
            var currentSessionEvent = _currentSession.SessionEvents.FirstOrDefault(s => s.UId == sessionEvent.UId);

            var index = _currentSession.SessionEvents.OrderBy(o => o.Time).ToList().IndexOf(currentSessionEvent);
            var rowHandle = eventsGridView.GetRowHandle(index);

            var info = GetGridViewInfo(eventsGridView);
            var firstRowHandle = info.RowsInfo.FirstOrDefault()?.RowHandle;

            ReloadDataSource();
            eventsGridView.ClearSelection();

            eventsGridView.TopRowIndex = firstRowHandle ?? rowHandle;
            eventsGridView.FocusedRowHandle = rowHandle;
            eventsGridView.SelectRow(rowHandle);
        }

        private void ReloadDataSource()
        {
            eventsGridControl.DataSource = _currentSession.SessionEvents?.OrderBy(o => o.Time).ThenBy(c => c.CreatedOn);
        }

        private Session ReloadSessionFromDatabase(Session session)
        {
            return SessionDbWrapper.SessionDbHelper.ReloadSession(session);
        }

        private static bool IsStartStopEvent(SessionEvent sessionEvent)
        {
            var description = sessionEvent?.SessionAction?.Description;
            return description != null && (description.Contains("<<<ჩაწერის დაწყება>>>") || description.Contains("<<<ჩაწერის დასრულება>>>"));
        }

        private static GridViewInfo GetGridViewInfo(GridView view)
        {
            var fi = typeof(GridView).GetField("fViewInfo", BindingFlags.NonPublic | BindingFlags.Instance);

            return fi.GetValue(view) as GridViewInfo;
        }
    }
}
