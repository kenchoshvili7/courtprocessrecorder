﻿using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;

namespace CourtProcessPlayer
{
    partial class PlayerUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.timelineTrackBar = new DevExpress.XtraEditors.TrackBarControl();
            this.trackbarAndWaveViewerPanel = new DevExpress.XtraEditors.PanelControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.playbackVolumeTrackBarControl = new DevExpress.XtraEditors.TrackBarControl();
            this.volumeMeterAndPlaybackVolumePanel = new DevExpress.XtraEditors.PanelControl();
            this.playerTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.audioFilesContainerBindingSource = new System.Windows.Forms.BindingSource();
            this.audioDeviceBindingSource = new System.Windows.Forms.BindingSource();
            ((System.ComponentModel.ISupportInitialize)(this.timelineTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timelineTrackBar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarAndWaveViewerPanel)).BeginInit();
            this.trackbarAndWaveViewerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackVolumeTrackBarControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackVolumeTrackBarControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeMeterAndPlaybackVolumePanel)).BeginInit();
            this.volumeMeterAndPlaybackVolumePanel.SuspendLayout();
            this.playerTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.audioFilesContainerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // timelineTrackBar
            // 
            this.timelineTrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timelineTrackBar.EditValue = null;
            this.timelineTrackBar.Location = new System.Drawing.Point(0, 0);
            this.timelineTrackBar.Name = "timelineTrackBar";
            this.timelineTrackBar.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.timelineTrackBar.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.timelineTrackBar.Properties.ShowLabels = true;
            this.timelineTrackBar.Size = new System.Drawing.Size(840, 42);
            this.timelineTrackBar.TabIndex = 1;
            this.timelineTrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.timelineTrackBar_MouseUp);
            // 
            // trackbarAndWaveViewerPanel
            // 
            this.trackbarAndWaveViewerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackbarAndWaveViewerPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.trackbarAndWaveViewerPanel.Controls.Add(this.timelineTrackBar);
            this.trackbarAndWaveViewerPanel.Location = new System.Drawing.Point(129, 306);
            this.trackbarAndWaveViewerPanel.Name = "trackbarAndWaveViewerPanel";
            this.trackbarAndWaveViewerPanel.Size = new System.Drawing.Size(840, 42);
            this.trackbarAndWaveViewerPanel.TabIndex = 8;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(621, 354);
            // 
            // playbackVolumeTrackBarControl
            // 
            this.playbackVolumeTrackBarControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playbackVolumeTrackBarControl.EditValue = null;
            this.playbackVolumeTrackBarControl.Location = new System.Drawing.Point(0, 0);
            this.playbackVolumeTrackBarControl.Name = "playbackVolumeTrackBarControl";
            this.playbackVolumeTrackBarControl.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.playbackVolumeTrackBarControl.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.playbackVolumeTrackBarControl.Size = new System.Drawing.Size(114, 42);
            this.playbackVolumeTrackBarControl.TabIndex = 5;
            this.playbackVolumeTrackBarControl.ToolTip = "მოსმენის ხმა";
            // 
            // volumeMeterAndPlaybackVolumePanel
            // 
            this.volumeMeterAndPlaybackVolumePanel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.volumeMeterAndPlaybackVolumePanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.volumeMeterAndPlaybackVolumePanel.Controls.Add(this.playbackVolumeTrackBarControl);
            this.volumeMeterAndPlaybackVolumePanel.Location = new System.Drawing.Point(6, 306);
            this.volumeMeterAndPlaybackVolumePanel.Name = "volumeMeterAndPlaybackVolumePanel";
            this.volumeMeterAndPlaybackVolumePanel.Size = new System.Drawing.Size(114, 42);
            this.volumeMeterAndPlaybackVolumePanel.TabIndex = 10;
            // 
            // playerTableLayoutPanel
            // 
            this.playerTableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playerTableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.playerTableLayoutPanel.ColumnCount = 2;
            this.playerTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.playerTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.playerTableLayoutPanel.Controls.Add(this.volumeMeterAndPlaybackVolumePanel, 0, 0);
            this.playerTableLayoutPanel.Controls.Add(this.trackbarAndWaveViewerPanel, 1, 0);
            this.playerTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.playerTableLayoutPanel.Name = "playerTableLayoutPanel";
            this.playerTableLayoutPanel.RowCount = 1;
            this.playerTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.playerTableLayoutPanel.Size = new System.Drawing.Size(975, 354);
            this.playerTableLayoutPanel.TabIndex = 11;
            // 
            // audioFilesContainerBindingSource
            // 
            this.audioFilesContainerBindingSource.DataSource = typeof(JsonDatabaseModels.AudioFilesContainer);
            // 
            // audioDeviceBindingSource
            // 
            this.audioDeviceBindingSource.DataSource = typeof(RecordProvider.AudioDevice);
            // 
            // PlayerUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.playerTableLayoutPanel);
            this.Name = "PlayerUserControl";
            this.Size = new System.Drawing.Size(975, 354);
            ((System.ComponentModel.ISupportInitialize)(this.timelineTrackBar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timelineTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarAndWaveViewerPanel)).EndInit();
            this.trackbarAndWaveViewerPanel.ResumeLayout(false);
            this.trackbarAndWaveViewerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackVolumeTrackBarControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackVolumeTrackBarControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeMeterAndPlaybackVolumePanel)).EndInit();
            this.volumeMeterAndPlaybackVolumePanel.ResumeLayout(false);
            this.volumeMeterAndPlaybackVolumePanel.PerformLayout();
            this.playerTableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.audioFilesContainerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioDeviceBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TrackBarControl timelineTrackBar;
        private System.Windows.Forms.BindingSource audioFilesContainerBindingSource;
        private DevExpress.XtraEditors.TrackBarControl playbackVolumeTrackBarControl;
        private System.Windows.Forms.BindingSource audioDeviceBindingSource;
        private PanelControl trackbarAndWaveViewerPanel;
        private DevExpress.XtraEditors.PanelControl volumeMeterAndPlaybackVolumePanel;
        private LayoutControlGroup Root;
        private TableLayoutPanel playerTableLayoutPanel;
    }
}
