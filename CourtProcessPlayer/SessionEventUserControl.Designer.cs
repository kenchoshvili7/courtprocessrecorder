﻿namespace CourtProcessPlayer
{
    partial class SessionEventUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.eventsGridControl = new DevExpress.XtraGrid.GridControl();
            this.sessionEventBindingSource = new System.Windows.Forms.BindingSource();
            this.eventsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.timeTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSessionAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParticipant = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.noteMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colUId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noteMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // eventsGridControl
            // 
            this.eventsGridControl.DataSource = this.sessionEventBindingSource;
            this.eventsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventsGridControl.Location = new System.Drawing.Point(0, 0);
            this.eventsGridControl.MainView = this.eventsGridView;
            this.eventsGridControl.Name = "eventsGridControl";
            this.eventsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2,
            this.timeTextEdit,
            this.repositoryItemCheckEdit1,
            this.noteMemoEdit});
            this.eventsGridControl.Size = new System.Drawing.Size(621, 410);
            this.eventsGridControl.TabIndex = 0;
            this.eventsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.eventsGridView});
            // 
            // sessionEventBindingSource
            // 
            this.sessionEventBindingSource.DataSource = typeof(JsonDatabaseModels.SessionEvent);
            // 
            // eventsGridView
            // 
            this.eventsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDate,
            this.colTime,
            this.colSessionAction,
            this.colParticipant,
            this.colNote,
            this.colUId,
            this.colCreatedOn});
            this.eventsGridView.GridControl = this.eventsGridControl;
            this.eventsGridView.Name = "eventsGridView";
            this.eventsGridView.OptionsCustomization.AllowColumnMoving = false;
            this.eventsGridView.OptionsCustomization.AllowGroup = false;
            this.eventsGridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.eventsGridView.OptionsCustomization.AllowSort = false;
            this.eventsGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.eventsGridView.OptionsSelection.EnableAppearanceHideSelection = false;
            this.eventsGridView.OptionsSelection.MultiSelect = true;
            this.eventsGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.eventsGridView.OptionsView.RowAutoHeight = true;
            this.eventsGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colDate
            // 
            this.colDate.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colDate.AppearanceCell.Options.UseFont = true;
            this.colDate.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colDate.AppearanceHeader.Options.UseFont = true;
            this.colDate.Caption = "თარიღი";
            this.colDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDate.FieldName = "Time";
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.Width = 88;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "dd.MMM.yyyy";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colTime
            // 
            this.colTime.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colTime.AppearanceCell.Options.UseFont = true;
            this.colTime.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colTime.AppearanceHeader.Options.UseFont = true;
            this.colTime.Caption = "დრო";
            this.colTime.ColumnEdit = this.timeTextEdit;
            this.colTime.FieldName = "Time";
            this.colTime.Name = "colTime";
            this.colTime.Visible = true;
            this.colTime.VisibleIndex = 0;
            this.colTime.Width = 60;
            // 
            // timeTextEdit
            // 
            this.timeTextEdit.AutoHeight = false;
            this.timeTextEdit.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeTextEdit.EditFormat.FormatString = "HH:mm:ss";
            this.timeTextEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeTextEdit.Mask.EditMask = "HH:mm:ss";
            this.timeTextEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.timeTextEdit.Name = "timeTextEdit";
            // 
            // colSessionAction
            // 
            this.colSessionAction.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colSessionAction.AppearanceCell.Options.UseFont = true;
            this.colSessionAction.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colSessionAction.AppearanceHeader.Options.UseFont = true;
            this.colSessionAction.Caption = "მოქმედება";
            this.colSessionAction.FieldName = "SessionAction.Description";
            this.colSessionAction.Name = "colSessionAction";
            this.colSessionAction.OptionsColumn.AllowEdit = false;
            this.colSessionAction.Visible = true;
            this.colSessionAction.VisibleIndex = 1;
            this.colSessionAction.Width = 148;
            // 
            // colParticipant
            // 
            this.colParticipant.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colParticipant.AppearanceCell.Options.UseFont = true;
            this.colParticipant.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colParticipant.AppearanceHeader.Options.UseFont = true;
            this.colParticipant.Caption = "მონაწილე";
            this.colParticipant.FieldName = "Participant.ParticipantDescription";
            this.colParticipant.Name = "colParticipant";
            this.colParticipant.OptionsColumn.AllowEdit = false;
            this.colParticipant.Visible = true;
            this.colParticipant.VisibleIndex = 2;
            this.colParticipant.Width = 173;
            // 
            // colNote
            // 
            this.colNote.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colNote.AppearanceCell.Options.UseFont = true;
            this.colNote.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colNote.AppearanceHeader.Options.UseFont = true;
            this.colNote.Caption = "შენიშვნა";
            this.colNote.ColumnEdit = this.noteMemoEdit;
            this.colNote.FieldName = "Note";
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 3;
            this.colNote.Width = 134;
            // 
            // noteMemoEdit
            // 
            this.noteMemoEdit.Name = "noteMemoEdit";
            // 
            // colUId
            // 
            this.colUId.FieldName = "UId";
            this.colUId.Name = "colUId";
            // 
            // colCreatedOn
            // 
            this.colCreatedOn.FieldName = "CreatedOn";
            this.colCreatedOn.Name = "colCreatedOn";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "HH:mm:ss";
            this.repositoryItemDateEdit1.Mask.EditMask = "HH:mm:ss";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // SessionEventUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.eventsGridControl);
            this.Name = "SessionEventUserControl";
            this.Size = new System.Drawing.Size(621, 410);
            ((System.ComponentModel.ISupportInitialize)(this.eventsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionEventBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noteMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl eventsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView eventsGridView;
        private System.Windows.Forms.BindingSource sessionEventBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSessionAction;
        private DevExpress.XtraGrid.Columns.GridColumn colParticipant;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colUId;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedOn;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit timeTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit noteMemoEdit;
    }
}
