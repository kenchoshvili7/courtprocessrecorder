﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CourtProcessPlayer.HelperClasses
{
    public class FormsHelper
    {
        public static T FindControlByTypeAndName<T>(Control control, string name) where T : Control
        {
            var baseControl = GetAllParentControls(control).LastOrDefault();
            var control2Return = Convert.ChangeType(baseControl?.Controls.Find(name, true).FirstOrDefault(), typeof(T));

            return (T)control2Return;
        }

        private static IEnumerable<Control> GetAllParentControls(Control control)
        {
            yield return control;

            if (control.Parent == null) yield break;
            var parent = control.Parent;

            while (parent != null)
            {
                yield return parent;
                parent = parent.Parent;
            }
        }
    }
}
